<?php
include_once "models/JobCategory.php";
include_once "models/JobWorkbusy.php";
include_once "models/JobEduType.php";

/** Created by phpWebStudio(c) 2013 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Резюме [resume] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.content", "resume.6.tpl");

define('NOINFO', '<i class="light_grey">нет информации</i>');
$arrIf['previous.works'] =
$arrIf['education'] = false;

if (!isset($objResume) || !is_object($objResume)) {
    $objResume = new Resume();
}
if (!isset($objSiteUser)) {
    $objSiteUser = new SiteUser();
}

/**
 * Если указан параметр записи
 */
if (isset($arrReqUri[1]) && !empty($arrReqUri[1])) {
    if (is_numeric($arrReqUri[1])) {
        $intRecordId = $arrReqUri[1];
        $strWhereSql = "jr_id=".$intRecordId;
        $flagUpdateUrl = true;
    } else {
        $strWhereSql = "jr_seo_url='".mysql_real_escape_string($arrReqUri[1])."'";
    }

    $strSqlQuery = "SELECT * FROM `job_resumes` WHERE $strWhereSql AND `jr_status` = 'Y'";
    $arrRecord = $objDb->fetch($strSqlQuery);

    if (is_array($arrRecord) && !empty($arrRecord)) {
        $arrTplVars['intResumeId'] = $intRecordId = $arrRecord['jr_id'];
        $arr_string_fields = array('strTitle'=>'jr_title', 'strFio'=>'jr_fio', 'strLinkedin'=>'jr_linkedin', 'strSkype'=>'jr_skype', 'strEmail'=>'jr_email', 'strPhone'=>'jr_phone', 'strCity'=>'jr_city', 'strAbout'=>'jr_about', 'strEduPlus'=>'jr_edu_plus', 'strMetaDesc'=>'jr_meta_description', 'strMetaKeywords'=>'jr_meta_keywords', 'strExperienceDescription' => 'jr_exp_desc');
        $arr_checked_fields = array('jr_sex', 'jr_marriage', 'jr_children', 'jr_trips', 'jr_driver');
        $arr_int_fields = array('strSalary'=>'jr_salary', 'strIcq'=>'jr_icq', 'strAge'=>'jr_age');

        foreach($arr_string_fields as $key => $field) {
            $arrRecord[$key] = (!empty($arrRecord[$field]) ? htmlspecialchars(trim($arrRecord[$field])) : NOINFO);
            $arrIf['is.'.$key] = $arrRecord[$key] != NOINFO;
        }

        if ($arrRecord['strPhone']==NOINFO || empty($arrRecord['strPhone'])) {
            if ($arrRecord['jr_grab_has_phone']=='Y' && $arrRecord['jr_grab_src_id'] > 0) {
                if (file_exists(PRJ_IMAGES.'resumes/contactimg/phone.'.$arrRecord['jr_grab_src_id'].'.jpg') && filesize(PRJ_IMAGES.'resumes/contactimg/phone.'.$arrRecord['jr_grab_src_id'].'.jpg') > 0) {
                    $arrRecord['strPhone'] = '<img src="'.$arrTplVars['cfgAllImg'].'resumes/contactimg/phone.'.$arrRecord['jr_grab_src_id'].'.jpg" class="phone_snap"/>';
                    $arrIf['is.strPhone'] = true;
                } elseif (file_exists(PRJ_IMAGES.'resumes/contactimg/phone.'.$arrRecord['jr_grab_src_id']) && filesize(PRJ_IMAGES.'resumes/contactimg/phone.'.$arrRecord['jr_grab_src_id']) > 0) {
                    $arrRecord['strPhone'] = '<img src="'.$arrTplVars['cfgAllImg'].'resumes/contactimg/phone.'.$arrRecord['jr_grab_src_id'].'" class="phone_snap"/>';
                    $arrIf['is.strPhone'] = true;
                }
            }
        } else {
            if (!is_object($objImage)) {
                $objImage = new clsImages();
            }
            $arrRecord['strPhone'] = '<img src="data:image/png;base64,'. base64_encode($objImage->text2img($arrRecord['strPhone'])) .'" />';
        }

        if ($arrRecord['strEmail']==NOINFO || empty($arrRecord['strEmail'])) {
            if ($arrRecord['jr_grab_has_email']=='Y' && $arrRecord['jr_grab_src_id'] > 0) {
                if (file_exists(PRJ_IMAGES.'resumes/contactimg/email.'.$arrRecord['jr_grab_src_id'].'.jpg') && filesize(PRJ_IMAGES.'resumes/contactimg/email.'.$arrRecord['jr_grab_src_id'].'.jpg') > 0) {
                    $arrRecord['strEmail'] = '<img src="'.$arrTplVars['cfgAllImg'].'resumes/contactimg/email.'.$arrRecord['jr_grab_src_id'].'.jpg" class="email_snap"/>';
                    $arrIf['is.strEmail'] = true;
                } elseif (file_exists(PRJ_IMAGES.'resumes/contactimg/email.'.$arrRecord['jr_grab_src_id']) && filesize(PRJ_IMAGES.'resumes/contactimg/email.'.$arrRecord['jr_grab_src_id']) > 0) {
                    $arrRecord['strEmail'] = '<img src="'.$arrTplVars['cfgAllImg'].'resumes/contactimg/email.'.$arrRecord['jr_grab_src_id'].'" class="email_snap"/>';
                    $arrIf['is.strEmail'] = true;
                }
            }
        } else {
            if (!is_object($objImage)) {
                $objImage = new clsImages();
            }
            $arrRecord['strEmail'] = '<img src="data:image/png;base64,'. base64_encode($objImage->text2img($arrRecord['strEmail'])) .'" />';
        }

        if (!empty($arrRecord['strAbout'])) $arrRecord['strAbout'] = nl2br($arrRecord['strAbout']);

        foreach($arr_checked_fields as $field) {
            $arrRecord[$field.'_'.$arrRecord[$field]] = ' checked';
        }

        foreach($arr_int_fields as $key => $field) {
            $arrRecord[$key] = (intval($arrRecord[$field]) ? $arrRecord[$field] : NOINFO);
        }

        $arrRecord['strTitle'] = mb_ucfirst($arrRecord['strTitle']);
        $arrTplVars['m_title'] = $arrRecord['strTitle'].' | '.$strProjectName;
        $arrTplVars['m_description'] = $arrIf['is.strMetaDesc'] ? $arrRecord['strMetaDesc'] : $arrTplVars['m_description'];
        $arrTplVars['m_keywords'] = $arrIf['is.strMetaKeywords'] ? $arrRecord['strMetaKeywords'] : $arrTplVars['m_keywords'];

        if ($arrRecord['strMetaDesc']==NOINFO || empty($arrRecord['strMetaDesc'])) {
            $arrRecord['strMetaDesc'] = "Газета Работа от А до Я, резюме - ".$arrRecord['strTitle'];

            $strSqlQuery = "UPDATE `job_resumes` SET"
                . " `jr_meta_description` = '".mysql_real_escape_string($arrRecord['strMetaDesc'])."'"
                . " WHERE `jr_id` = ".$intRecordId;
            if (!$objDb->query($strSqlQuery)) {
                # sql error
                die('Ошибка базы данных. Нажмите кнопку НАЗАД в Вашем браузере!');
            }
        }

        if ($arrRecord['strMetaKeywords']==NOINFO || empty($arrRecord['strMetaKeywords'])) {
            $arrRecord['strMetaKeywords'] = $arrRecord['strTitle'].','.implode(',', $arrKeyWords);

            $strSqlQuery = "UPDATE `job_resumes` SET"
                . " `jr_meta_keywords` = '".mysql_real_escape_string($arrRecord['strMetaKeywords'])."'"
                . " WHERE `jr_id` = ".$intRecordId;
            if (!$objDb->query($strSqlQuery)) {
                # sql error
                die('Ошибка базы данных. Нажмите кнопку НАЗАД в Вашем браузере!');
            }
        }

        if (empty($arrRecord['jr_age']) && !empty($arrRecord['jr_birthday'])) {
            // количество лет между датами
            $a = new DateTime($arrRecord['jr_birthday']);
            $b = new DateTime(date('Y-m-d'));
            $arrRecord['strAge'] = $a->diff($b)->format('%y');
        }
        $arrIf['is.strAge'] = !empty($arrRecord['strAge']) && $arrRecord['strAge'] != NOINFO;
        $arrIf['is.strSalary'] = !empty($arrRecord['strSalary']) && $arrRecord['strSalary'] != NOINFO;
        if ($arrIf['is.strSalary']) {
            $arrRecord['strSalary'] .= ' руб';
        }

        $arrRecord['strMarriage'] = $arrRecord['jr_marriage'] == 'Y' ? 'женат/замужем' : 'холост/не замужем';
        $arrRecord['strChildren'] = $arrRecord['jr_children'] == 'Y' ? 'есть' : 'нет';
        $arrRecord['strDriver'] = $arrRecord['jr_driver'] == 'Y' ? 'есть' : 'не имею';
        $arrRecord['strTrips'] = $arrRecord['jr_trips'] == 'Y' ? 'да, в любое время' : ($arrRecord['jr_trips'] == 'S' ? 'иногда / не часто': 'не готов(-а)');

        if ($arrRecord['jr_work_busy']) {

            $objWorkbusy = new JobWorkbusy($_db_config);
            $arrRecord['strWorkbusy'] = $objWorkbusy->getStatus($arrRecord['jr_work_busy']);
        } else {
            $arrRecord['strWorkbusy'] = NOINFO;
        }
        $arrIf['is.strWorkbusy'] = $arrRecord['strWorkbusy'] != NOINFO;

        if (date('Y', $arrRecord['jr_date_publish']) > 2012)
        $arrRecord['strDatePublish'] = date("d.m.Y", $arrRecord['jr_date_publish']);

        $objCategory = new JobCategory($_db_config);

        /**
         * Выборка о работе
         */
        $strSqlQuery = "SELECT * FROM `job_resumes_works` WHERE `jrw_jr_id` = ".$intRecordId;
        $arrWorksHistory = $objDb->fetchall($strSqlQuery);
        if (is_array($arrWorksHistory) && !empty($arrWorksHistory)) {
            $arrIf['previous.works'] = true;

            foreach ($arrWorksHistory as $k => $arr) {
                $arrWorksHistory[$k]['strHistoryCompany'] = (!empty($arr['jrw_company']) ? htmlspecialchars($arr['jrw_company']) : '&mdash;' );
                $arrWorksHistory[$k]['strHistoryCategory'] = $objCategory->getCategoryFieldById($arr['jrw_jc_id'], 'jc_title');
                $arrWorksHistory[$k]['strHistoryPosition'] = (!empty($arr['jrw_position']) ? htmlspecialchars($arr['jrw_position']) : '&mdash;' );
                $arrWorksHistory[$k]['strHistoryRegion'] = (!empty($arr['jrw_region']) ? htmlspecialchars($arr['jrw_region']) : '&mdash;' );
                $arrWorksHistory[$k]['strHistoryWeb'] = '&mdash;';
                if (!empty($arr['jrw_web'])) {
                    $arr['jrw_web'] = str_replace('http://', '', $arr['jrw_web']);
                    $arr['jrw_web'] = str_replace('https://', '', $arr['jrw_web']);
                    $arrWorksHistory[$k]['strHistoryWeb'] = $arr['jrw_web'];
                }
                $arrWorksHistory[$k]['strHistoryResponsibilities'] = (!empty($arr['jrw_responsibility']) ? htmlspecialchars($arr['jrw_responsibility']) : '&mdash;' );
                $arrWorksHistory[$k]['strHistoryDateFrom'] = $objUtils->workDate(5, $arr['jrw_date_start']);
                $arrWorksHistory[$k]['strHistoryDateTo'] = ($objUtils->dateValid($arr['jrw_date_fin']) ? $objUtils->workDate(5, $arr['jrw_date_fin']) : '&mdash;' );
            }
        }

        /**
         * Образование
         */
        $objEduType = new JobEduType();

        $intEduType = $objEduType->getIdByResume($intRecordId);

        if ($intEduType) {
            $arrRecord['strEducation'] = $objEduType->getStatus($intEduType);
            $arrIf['is.strEducation'] = true;
        } else {
            $arrRecord['strEducation'] = NOINFO;
        }

        $arr_string_fields = array('strInstitution'=>'jre_institution', 'strFaculty'=>'jre_faculty', 'strSpecialization'=>'jre_spec');
        $strSqlQuery = "SELECT * FROM `job_resumes_education` WHERE `jre_jr_id` = ".$intRecordId;
        $arrEdu = $objDb->fetch($strSqlQuery);
        if (is_array($arrEdu) && !empty($arrEdu)) {
            $arrIf['education'] = true;
            foreach($arr_string_fields as $key => $field) {
                $arrRecord[$key] = (!empty($arrEdu[$field]) ? htmlspecialchars($arrEdu[$field]) : NOINFO);
                $arrIf['is.'.$key] = $arrRecord[$key] != NOINFO;
            }

            if (!empty($arrEdu['jre_date_start']) && $objUtils->dateValid($arrEdu['jre_date_start'])) {
                $arrRecord['strEduDateStart'] = date('d.m.Y', strtotime($arrEdu['jre_date_start']));
            } else {
                $arrRecord['strEduDateStart'] = NOINFO;
            }

            if (!empty($arrEdu['jre_date_fin']) && $objUtils->dateValid($arrEdu['jre_date_fin'])) {
                $arrRecord['strEduDateFin'] = date('d.m.Y', strtotime($arrEdu['jre_date_fin']));
            } else {
                $arrRecord['strEduDateFin'] = 'по наст.вр.';
            }
        }

        if ($arrRecord['jr_city_id'] > 0) {
            $arrCity = $objSiteUser->getCityById($arrRecord['jr_city_id']);
            if (is_array($arrCity) && !empty($arrCity)) {
                $arrRecord['strCity'] = $arrCity['city'];
            }
            $arrIf['is.strCity'] = !empty($arrRecord['strCity']);
        }

        $objTpl->tpl_array("page.content", $arrRecord);
        $objResume->incViews($intRecordId);
    } else {
        header('location: /error');
        exit;
    }
}
$objTpl->tpl_loop("page.content", "resume.history", $arrWorksHistory);

$arrTplVars['strBackUri'] = SITE_URL.'index';
$arrTplVars['strBackTitle'] = 'вернуться на главную страницу';

if (isset($_SESSION['search'])) {
    $arrTplVars['strBackUri'] = SITE_URL.'search?p='.($_SESSION['search']['p']?:1);
    $arrTplVars['strBackTitle'] = 'вернуться к списку резюме';
}

$objTpl->tpl_if("page.content", $arrIf);
$objTpl->tpl_array("page.content", $arrTplVars);
