<?php
/** Created by phpWebStudio(c) 2014 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Тренинги [trainings] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.content", "trainings.53.tpl");

if (isset($arrReqUri[1]) && !empty($arrReqUri[1]) && $arrReqUri[1]!='group') {
    $arrIf['record'] = true;

    $arrTplVars['stPage'] = isset($_GET['p'])?$_GET['p']:1;

    $intId = intval($arrReqUri[1]);
    $strSqlQuery = "SELECT * FROM site_trainings WHERE str_status='Y' AND str_id=".$intId;

    $arrInfo = $objDb->fetch( $strSqlQuery );
    // Проверим, существует ли такая запись, и если да, то обработаем для вывода
    if (!empty($arrInfo)) {
        $intId = intval($arrInfo['str_id']);
        $arrInfo['strBody'] = nl2br($arrInfo['str_desc']);
        $arrInfo['strAddress'] = htmlspecialchars($arrInfo['str_address']);
        $arrInfo['strEmail'] = (htmlspecialchars($arrInfo['str_email']));
        $arrInfo['strPhone'] = (htmlspecialchars($arrInfo['str_phone']));

        if (!empty($arrInfo['str_web']) && !$objUtils->isValidUrl($arrInfo['str_web'])) {
            if (!preg_match('/http/', $arrInfo['str_web'])) {
                $arrInfo['str_web'] = 'http://'.$arrInfo['str_web'];
            }
        }

        //$arrInfo['strWeb'] = ($objUtils->isValidUrl($arrInfo['str_web'])?$objUtils->makeClickableLinks($arrInfo['str_web']):$arrInfo['str_web']);
        $arrInfo['strWeb'] = $arrInfo['str_web'];

        $objTpl->tpl_array("page.content", $arrInfo);

        $arrTplVars['m_title'] = htmlspecialchars(strip_tags($arrInfo['str_title'])).' | '.$arrTplVars['m_title'];
        $arrTplVars['m_description'] = $objUtils->substrText(htmlspecialchars(strip_tags($arrInfo['str_desc'])), 250);
    } else {
        header('Location: '.SITE_URL.'error');
        exit();
    }
} else {
    $arrIf['list'] = true;

    $arrTplVars['stPage'] = isset($_GET['stPage'])?$_GET['stPage']:1;

    $strSqlWhere = '';
    if ($arrReqUri[1]=='group' && $arrReqUri[2] > 0) {
        $strSqlWhere = ' AND `strl_str_id` = '.intval($arrReqUri[2]);

        if ($arrReqUri[3]=='subgroup' && $arrReqUri[4] > 0) {
            $strSqlWhere = ' AND `strl_str_id` = '.intval($arrReqUri[4]);
        }
    }

    if (isset($_GET['search']) && !empty($_GET['search'])) {
        $arrTplVars['searchParam'] = htmlspecialchars(trim($_GET['search']));
        $searchString = mysql_real_escape_string(trim($_GET['search']));
        if (strlen($searchString) > 2)
            $strSqlWhere .= " AND (`str_title` LIKE '%$searchString%' OR `str_desc` LIKE '%$searchString%')";
        else
            $strSqlWhere .= " AND (`str_title` LIKE '$searchString' OR `str_desc` LIKE '$searchString')";
    }

    // Всего записей в базе
    $strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM `site_trainings` LEFT JOIN `site_training_rubric_lnk` ON (`strl_st_id` = `str_id`) WHERE str_status = 'Y'".$strSqlWhere;
    $arrTplVars['intQuantitySelectRecords'] = $arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');
    // ***** BEGIN: Построение пейджинга для вывода списка
    $objPagination = new tplPaginator($arrTplVars['intQuantitySelectRecords'], SITE_TPL_TPL_DIR);
    /**
     * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
     * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
     */
    $objPagination->strPaginatorTpl = "site.global.4.tpl";
    $objPagination->strColorActiveStyle = "tab-bl-b";
    $objPagination->strColorActivePage = '8A0202';
    $objPagination->intQuantRecordPage = 25;
    // Создаем блок пэйджинга
    $objPagination->paCreate();
    // ***** END: Построение пейджинга для вывода списка
    // Запрос для выборки нужных записей
    $strSqlQuery = "SELECT * FROM `site_trainings`"
        ." LEFT JOIN `site_training_rubric_lnk` ON (`strl_st_id` = `str_id`)"
        ." WHERE str_status = 'Y'".$strSqlWhere
        ." ORDER BY str_date_create DESC, str_id DESC ".$objPagination->strSqlLimit;
    $arrLastTrainings = $objDb->fetchall( $strSqlQuery );
    // кол-во публикаций показанных на странице
    $arrTplVars['intQuantityShowRecOnPage'] = ( !empty($arrLastTrainings) ? count($arrLastTrainings) : 0 );
//    if ($arrTplVars['intQuantityShowRecOnPage'] >= $objPagination->intQuantRecordPage) {
        // Присвоение значения пэйджинга для списка
        $arrTplVars['blockPaginator'] = $objPagination->paShow();
//    }

    if ( is_array($arrLastTrainings) ) {
        foreach ( $arrLastTrainings as $key => $value ) {
            $arrLastTrainings[$key]['strDatePublishArticle'] = date('d.m.Y', $value['str_date_publish']);
        }
    }

    $objTpl->tpl_loop("page.content", "last.trainings", $arrLastTrainings);

    /**
     * Выбор рубрик
     */
    $strSqlQuery = "SELECT * FROM `site_training_rubrics` WHERE `str_parent_id` IS NULL ORDER BY `str_id`";
    $arrRubrics = $objDb->fetchall($strSqlQuery);

    if (is_array($arrRubrics) && !empty($arrRubrics)) {
        foreach ($arrRubrics as $k => $val) {
            $arrRubrics[$k]['rubric_active'] = (isset($arrReqUri[2]) && $val['str_id'] == $arrReqUri[2]) ? 'active' : '';
        }
    }

    if ($arrReqUri[1]=='group' && $arrReqUri[2] > 0) {
        $arrTplVars['mainRubricID'] = intval($arrReqUri[2]);
        $strSqlQuery = "SELECT * FROM `site_training_rubrics` WHERE `str_parent_id` = ".intval($arrReqUri[2])." ORDER BY `str_id`";
        $arrSubRubrics = $objDb->fetchall($strSqlQuery);
        if (is_array($arrSubRubrics) && !empty($arrSubRubrics)) {
            $arrIf['sub.categories'] = true;
            foreach ($arrSubRubrics as $k => $val) {
                $arrSubRubrics[$k]['subrubric_active'] = (isset($arrReqUri[4]) && $val['str_id'] == $arrReqUri[4]) ? 'active' : '';
            }

            $objTpl->tpl_loop("page.content", "list.subgroups", $arrSubRubrics);
        }
    }
    $objTpl->tpl_loop("page.content", "list.groups", $arrRubrics);
}


$objTpl->tpl_if("page.content", $arrIf);
$objTpl->tpl_array("page.content", $arrTplVars);
