<?php
/** Created by phpWebStudio(c) 2014 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Мои вакансии [my-vacancies] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.content", "my-vacancies.65.tpl");

$arrTplVars['msgErr'] = null;

if (isset($_POST['sss']) && strlen($_POST['sss'])==32) {
    $arrTplVars['strEmail'] = $_POST['strEmail'];
    if ($_POST['sss'] == $_SESSION['saveMySss']) {
        if (!empty($_POST['strEmail'])) {
            if ($objUtils->checkEmail($_POST['strEmail'])) {
                # Проверить, а не является ли данный E-mail уже зарегистрированным пользователем
                if (!isset($objSiteUser) || !is_object($objSiteUser)) {
                    $objSiteUser = new SiteUser();
                }

                $is_registered = $objSiteUser->isEmailExist($_POST['strEmail']); // Не важно по вакансии или по резюме

                if ($is_registered) {
                    # Если зарегистрирован, напишем ему чтобы он воспользовался функцией Восстановления пароля и дадим ссылку
                    $arrTplVars['msgWarn'] = 'Данный E-mail адрес уже зарегистрирован на сайте.<br>Воспользуйтесь <a href="/recovery">восстановлением пароля</a>, если Вы его забыли.';
                } else {
                    # Если пользователь не зарегистрирован, проверим, а есть ли у него вообще вакансии на сайте
                    $objVacancy = new Vacancy();
                    $has_vacancy_id = $objVacancy->isExistsByEmail($_POST['strEmail']);

                    if ($has_vacancy_id) {
                        # Отправить письмо
                        if (!is_object($objMail)) {
                            include_once("cls.mail.php");
                            $objMail = new clsMail();
                        }

                        $linkToLogin = $objSiteUser->getLinkToLogin($has_vacancy_id, 'V');
                        if (!is_null($linkToLogin)) {
                            $arrVacancy = $objVacancy->getData($has_vacancy_id);
                            $strFIO = $arrVacancy['jv_contact_fio'];
                            $sendTo = $_POST['strEmail'];
                            $sendFrom = "info@rabota-ya.ru";
                            $sendSubject = "Работа от А до Я | Доступ в Личный Кабинет";
                            $sendMessage = "Уважаемый/ая ".($strFIO?:'работодатель')."!\r\n";
                            $sendMessage .= "Для Вас автоматически создана ссылка для входа в Личный Кабинет:\r\n \r\n";
                            $sendMessage .= "$linkToLogin \r\n";
                            $sendMessage .= "\r\n";
                            $sendMessage .= "По всем вопросам обращайтесь по телефону или посредствам E-mail указаных на странице сайта Контакты\r\n";
                            $sendMessage .= "\r\n*****\r\n \r\n";
                            $sendMessage .= "Не отвечайте на данное письмо! Текст сформирован автоматически почтовым роботом.";

                            $sendHTMLMessage = file_get_contents(SITE_TPL_DIR.'emails/new.account.created.html');

                            $linkToContacts = SITE_URL.'contacts';
                            $sendHTMLMessage = preg_replace("/\{\%LINK_CONTACTS\%\}/", $linkToContacts, $sendHTMLMessage);

                            $sendHTMLMessage = preg_replace("/\{\%FIO\%\}/", ($strFIO?:'работодатель'), $sendHTMLMessage);
                            $sendHTMLMessage = preg_replace("/\{\%LINK\%\}/", $linkToLogin, $sendHTMLMessage);
                            $sendHTMLMessage = preg_replace("/\n/", "\r\n", $sendHTMLMessage);

                            if (isset($_SERVER['_IS_DEVELOPER_MODE']) && $_SERVER['_IS_DEVELOPER_MODE'] == 1) {
                                $sendTo = "jimmy.webstudio@gmail.com"; // DEBUG
                                $sendTo = "shirokovskij@yahoo.com"; // DEBUG
                            }
                            //$sendTo = 'info@rabota-ya.ru'; // DEBUG

                            $objMail->new_mail($sendFrom, $sendTo, $sendSubject, $sendMessage, $sendHTMLMessage);
                            if ($sendTo != "jimmy.webstudio@gmail.com") {
                                $objMail->add_bcc("jimmy.webstudio@gmail.com");
                            }

                            $objMail->add_attachment( PRJ_SITEIMG.'content/email-bg.jpg', 'email-bg.jpg', 'image' );

                            if (!$objMail->send()) {
                                write_file("Письмо не ушло на $sendTo!");
                                $arrTplVars['msgErr'] = 'Письмо не ушло. Свяжитесь с администрацией сайта или попытайтесь позже.';
                            } else {
                                $arrTplVars['msgOk'] = 'Вам отправлено письмо, в котором указана ссылка для входа в Личный Кабинет';
                            }
                        } else {
                            $arrTplVars['msgErr'] = 'Операция не удалась';
                        }
                    } else {
                        $arrTplVars['msgWarn'] = 'Данный E-mail адрес не прикреплен ни к одному резюме.<br>Чтобы создать резюме, пройдите на страницу <a href="/form_add_resume">Создать резюме</a>.';
                    }
                }
            } else {
                $arrTplVars['msgErr'] = 'Неправильно указан E-mail адрес. Неверный формат.';
            }
        } else {
            $arrTplVars['msgErr'] = 'Вы забыли указать Ваш E-mail адрес';
        }
    } else {
        $arrTplVars['msgErr'] = 'Что-то пошло не так';
    }
}
$arrIf['error'] = !empty($arrTplVars['msgErr']);
$arrIf['warning'] = !empty($arrTplVars['msgWarn']);
$arrIf['ok'] = !empty($arrTplVars['msgOk']);

$_SESSION['saveMySss'] = $arrTplVars['sss'] = md5(date('c'));

$objTpl->tpl_array("page.content", $arrTplVars);
$objTpl->tpl_if("page.content", $arrIf);
