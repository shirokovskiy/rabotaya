<?php
/** Created by phpWebStudio(c) 2013 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Резюме по профессиям [po-professiyam-resume] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.content", "po-professiyam-resume.24.tpl");

$strSqlQuery = "(SELECT COUNT(`jr_title`) AS iC, `jr_title` FROM  `job_resumes` WHERE `jr_status` = 'Y' AND `jr_title` REGEXP '^".implode('|^', $objUtils->arrRusChars)."' GROUP BY `jr_title` ORDER BY iC DESC LIMIT 0, 60) ORDER BY `jr_title`";
$arrRecords = $objDb->fetchall($strSqlQuery);

if (is_array($arrRecords)) {
    foreach ($arrRecords as $key => $value) {
        $arrRecords[$key]['strTitle'] = htmlspecialchars($value['jr_title']);
        $arrRecords[$key]['urlTitle'] = urlencode(trim($value['jr_title']));
        $arrRecords[$key]['strCss'] = ($value['iC'] > 100 ? 'item a_lot_of' : 'item');
    }
}

$objTpl->tpl_loop("page.content", "list", $arrRecords);
$objTpl->tpl_array("page.content", $arrTplVars);
