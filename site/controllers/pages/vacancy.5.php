<?php
/** Created by phpWebStudio(c) 2013 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Вакансия [vacancy] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.content", "vacancy.5.tpl");

define('NOINFO', '<i class="light_grey">нет информации</i>');

if (!isset($objVacancy) || !is_object($objVacancy)) {
    $objVacancy = new Vacancy();
}
if (!isset($objSiteUser)) {
    $objSiteUser = new SiteUser();
}

/**
 * Если указан параметр записи
 */
if (isset($arrReqUri[1]) && !empty($arrReqUri[1])) {
    if (is_numeric($arrReqUri[1])) {
        $strWhereSql = "jv_id=".intval($arrReqUri[1]);
        $flagUpdateUrl = true;
    } else {
        $strWhereSql = "jv_seo_url='".mysql_real_escape_string($arrReqUri[1])."'";
    }

    // Выбрать вакансию
    $strSqlQuery = "SELECT * FROM `job_vacancies`"
        ." LEFT JOIN `job_companies` ON (jc_id = jv_jc_id)"
        ." LEFT JOIN `site_images` ON (si_type = 'company' AND si_rel_id = jc_id)"
        ." WHERE $strWhereSql AND `jv_status` = 'Y'";
    $arrRecord = $objDb->fetch($strSqlQuery);
    // Если вакансия есть
    if (is_array($arrRecord) && !empty($arrRecord)) {
        $intRecordId = intval($arrRecord['jv_id']);
        /**
         * Если добавляем вакансию в Избранное
         */
        if ($_GET['status'] == 'fav' && $objUser->isAuthed()) {
            $objUser->addFavoriteVacancy($intRecordId);

            header('Location: /profile/favorites_vacancies');
            exit;
        }

        /**
         * Если добавляем вакансию в Черный список
         */
        if ($_GET['status'] == 'bl' && $objUser->isAuthed()) {
            $objUser->addBlacklistVacancy($intRecordId);

            header('Location: /profile/blacklist');
            exit;
        }

        $arrTplVars['intVacancyId'] = $intRecordId;
        $arr_string_fields = array('strTitle'=>'jv_title', 'strCompany'=>'jc_title', 'strFio'=>'jv_contact_fio', 'strEmail'=>'jv_email', 'strPhone'=>'jv_phone', 'strCity'=>'jv_city', 'strDescription'=>'jv_description', 'strMetaDesc'=>'jv_meta_description', 'strMetaKeywords'=>'jv_meta_keywords');

        foreach($arr_string_fields as $key => $field) {
            $arrRecord[$key] = (!empty($arrRecord[$field]) ? htmlspecialchars(trim($arrRecord[$field])) : NOINFO);
            $arrIf['is.'.$key] = $arrRecord[$key] != NOINFO;
        }
        $arrRecord['strTitle'] = mb_ucfirst($arrRecord['strTitle']);
        $arrTplVars['m_title'] = $arrRecord['strTitle'].' | '.$strProjectName;
        $arrTplVars['m_description'] = $arrIf['is.strMetaDesc'] ? $arrRecord['strMetaDesc'] : $arrTplVars['m_description'];
        $arrTplVars['m_keywords'] = $arrIf['is.strMetaKeywords'] ? $arrRecord['strMetaKeywords'] : $arrTplVars['m_keywords'];

        if ($arrRecord['strMetaDesc']==NOINFO || empty($arrRecord['strMetaDesc'])) {
            $arrRecord['strMetaDesc'] = "Газета Работа от А до Я, вакансия - ".$arrRecord['strTitle'];

            $strSqlQuery = "UPDATE `job_vacancies` SET"
                . " `jv_meta_description` = '".mysql_real_escape_string($arrRecord['strMetaDesc'])."'"
                . " WHERE `jv_id` = ".$intRecordId;
            if (!$objDb->query($strSqlQuery)) {
                # sql error
                die('Ошибка базы данных. Нажмите кнопку НАЗАД в Вашем браузере!');
            }
        }

        if ($arrRecord['strMetaKeywords']==NOINFO || empty($arrRecord['strMetaKeywords'])) {
            $arrRecord['strMetaKeywords'] = $arrRecord['strTitle'].','.implode(',', $arrKeyWords);

            $strSqlQuery = "UPDATE `job_vacancies` SET"
                . " `jv_meta_keywords` = '".mysql_real_escape_string($arrRecord['strMetaKeywords'])."'"
                . " WHERE `jv_id` = ".$intRecordId;
            if (!$objDb->query($strSqlQuery)) {
                # sql error
                die('Ошибка базы данных. Нажмите кнопку НАЗАД в Вашем браузере!');
            }
        }

        if (!empty($arrRecord['strDescription'])) $arrRecord['strDescription'] = nl2br($arrRecord['strDescription']);

        if(!empty($arrRecord['strCompany']) && intval($arrRecord['jc_id'])) {
            $arrRecord['strCompanyName'] = $arrRecord['strCompany'];
            $arrRecord['strCompany'] = '<a href="'.SITE_URL.'company-vacancies/'.$arrRecord['jc_id'].'">'.$arrRecord['strCompany'].'</a>';
        }

        $arrRecord['strDriver'] = $arrRecord['jv_driver'] == 'Y' ? 'требуется' : NOINFO;
        $arrIf['is.strDriver'] = $arrRecord['strDriver'] != NOINFO;

        $arrRecord['strTrips'] = $arrRecord['jv_trips'] == 'Y' ? 'требуется' : NOINFO;
        $arrIf['is.strTrips'] = $arrRecord['strTrips'] != NOINFO;

        $arrRecord['strWorkbusy'] = $arrRecord['strEdu'] = NOINFO;
        if(intval($arrRecord['jv_jw_id'])) {
            include_once "models/JobWorkbusy.php";
            $objWB = new JobWorkbusy($_db_config);
            $arrRecord['strWorkbusy'] = $objWB->getStatus($arrRecord['jv_jw_id']);
        }
        if(intval($arrRecord['jv_jet_id'])) {
            include_once "models/JobEduType.php";
            $objET = new JobEduType($_db_config);
            $arrRecord['strEdu'] = $objET->getStatus($arrRecord['jv_jet_id']);
        }
        $arrIf['is.strEdu'] = $arrRecord['strEdu'] != NOINFO;

        $arrRecord['strSex'] = $arrRecord['jv_sex'] == 'male' ? 'мужчина' : ($arrRecord['jv_sex'] == 'female' ? 'женщина' : NOINFO);
        $arrIf['is.strSex'] = $arrRecord['strSex'] != NOINFO;

        $arrIf['is.strSalary'] = true;
        $arrRecord['strSalary'] = (!empty($arrRecord['jv_salary_from']) ? 'от '.$arrRecord['jv_salary_from'] : '').(!empty($arrRecord['jv_salary_to']) ? (!empty($arrRecord['jv_salary_from'])?' до ':'').$arrRecord['jv_salary_to'] : '');
        if (!empty($arrRecord['strSalary']) && $arrRecord['strSalary'] != NOINFO) {
            $arrRecord['strSalary'] .= ' руб';
        } elseif (!empty($arrRecord['jv_salary'])) {
            $arrRecord['strSalary'] = $arrRecord['jv_salary'];
        } else {
            $arrRecord['strSalary'] = NOINFO;
            $arrIf['is.strSalary'] = false;
        }

        if ($arrRecord['strPhone']!=NOINFO && strlen($arrRecord['strPhone'])==10 && !preg_match('/\s/i', $arrRecord['strPhone'])) {
            $arrRecord['strPhone'] = '+7 '.substr($arrRecord['strPhone'],0,3).' '.substr($arrRecord['strPhone'],3,3).' '.substr($arrRecord['strPhone'],6,2).' '.substr($arrRecord['strPhone'],8,2);
        }

        if ($arrRecord['strPhone']!=NOINFO) {
            if (!is_object($objImage)) {
                $objImage = new clsImages();
            }
            $arrRecord['strPhone'] = '<img src="data:image/png;base64,'. base64_encode($objImage->text2img($arrRecord['strPhone'])) .'" />';
        }

        if ($arrRecord['strEmail']!=NOINFO) {
            if (!is_object($objImage)) {
                $objImage = new clsImages();
            }
            $arrRecord['strEmail'] = '<img src="data:image/png;base64,'. base64_encode($objImage->text2img($arrRecord['strEmail'])) .'" />';
        }

        if ( date('Y', $arrRecord['jv_date_publish']) > 2012 ) {
            $arrRecord['strDatePublish'] = date("d.m.Y", $arrRecord['jv_date_publish']);
        }

        if ($arrRecord['jv_city_id'] > 0) {
            $arrCity = $objSiteUser->getCityById($arrRecord['jv_city_id']);
            if (is_array($arrCity) && !empty($arrCity)) {
                $arrRecord['strCity'] = $arrCity['city'];
            }
            $arrIf['is.strCity'] = !empty($arrRecord['strCity']);
        }

        if (!empty($arrRecord['si_filename']) && file_exists(PRJ_IMAGES.'companies/'.$arrRecord['si_filename'])) {
            $arrIf['is.logo'] = true;
        }



        $objTpl->tpl_array("page.content", $arrRecord);
        $objVacancy->incViews($intRecordId);
    } else {
        header('location: /error');
        exit;
    }
}
$arrTplVars['strBackUri'] = SITE_URL.'index';
$arrTplVars['strBackTitle'] = 'вернуться на главную страницу';

if (isset($_SESSION['search'])) {
    $arrTplVars['strBackUri'] = SITE_URL.'search?p='.($_SESSION['search']['p']?:1);
    $arrTplVars['strBackTitle'] = 'вернуться к списку вакансий';
}

$objTpl->tpl_array("page.content", $arrTplVars);
$objTpl->tpl_if("page.content", $arrIf);
