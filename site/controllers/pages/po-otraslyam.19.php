<?php
/** Created by phpWebStudio(c) 2013 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Поиск по отраслям [po-otraslyam] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.content", "po-otraslyam.19.tpl");

include_once "models/JobCategory.php";
/**
 * Список категорий
 */
if (!isset($objJobCategory) && !is_object($objJobCategory)) {
    $objJobCategory = new JobCategory();
}

$arrCategories = $objJobCategory->getCategories();
if (is_array($arrCategories) && !empty($arrCategories)) {
    foreach ($arrCategories as $key => $arr) {
        $arrCategories[$key]['strMainCategory'] = htmlspecialchars($arr['jc_title']);
        $arrCategories[$key]['intMainCategoryId'] = $arr['jc_id'];
    }

    $objTpl->tpl_loop("page.content", "list.categories", $arrCategories);

    foreach ($arrCategories as $key => $arr) {
        $arrSubCategories = $objJobCategory->getSubCategories($arr['jc_id']);
        if (is_array($arrSubCategories) && !empty($arrSubCategories)) {
            foreach ($arrSubCategories as $k2 => $arr_sub) {
                $arrSubCategories[$k2]['intSubCatId'] = $arr_sub['jc_id'];
                $arrSubCategories[$k2]['strSubCatTitle'] = htmlspecialchars($arr_sub['jc_title']);
            }
        }

        $arrIf['is.subcategories.'.$arr['jc_id']] = is_array($arrSubCategories) && !empty($arrSubCategories);
        $objTpl->tpl_loop("page.content", "list.subcategories.".$arr['jc_id'], $arrSubCategories);
    }
}

$objTpl->tpl_array("page.content", $arrTplVars);
