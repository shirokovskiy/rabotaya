<?php
/** Created by phpWebStudio(c) 2013 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Вакансии по профессиям [po-professiyam] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.content", "po-professiyam.23.tpl");

if (!isset($objSiteUser)) {
    $objSiteUser = new SiteUser();
}

if (isset($_SESSION['myCity']) && !empty($_SESSION['myCity'])) {
    $cityId = $objSiteUser->getCityId($_SESSION['myCity']);
    if (!empty($cityId)) {
        $strSqlWhere = " AND (jv_city LIKE '%".$_SESSION['myCity']."%' OR jv_city_id = $cityId)";
    } else {
        $strSqlWhere = " AND jv_city LIKE '%".$_SESSION['myCity']."%'";
    }
}

$strSqlQuery = "(SELECT COUNT(`jv_title`) AS iC, `jv_title` FROM  `job_vacancies` WHERE `jv_status` = 'Y' AND `jv_title` REGEXP '^".implode('|^', $objUtils->arrRusChars)."'".$strSqlWhere." GROUP BY `jv_title` ORDER BY iC DESC LIMIT 0, 60) ORDER BY `jv_title`";
$arrRecords = $objDb->fetchall($strSqlQuery);

if (is_array($arrRecords)) {
    foreach ($arrRecords as $key => $value) {
        $arrRecords[$key]['strTitle'] = htmlspecialchars($value['jv_title']);
        $arrRecords[$key]['urlTitle'] = urlencode(trim($value['jv_title']));
        $arrRecords[$key]['strCss'] = ($value['iC'] > 100 ? 'item a_lot_of' : 'item');
    }
}

$objTpl->tpl_loop("page.content", "list", $arrRecords);
$objTpl->tpl_array("page.content", $arrTplVars);


/*UPDATE tb_Company
SET CompanyIndustry = CONCAT(UCASE(LEFT(CompanyIndustry, 1)),
    SUBSTRING(CompanyIndustry, 2));*/

/*UPDATE tb_Company
SET CompanyIndustry = CONCAT(UCASE(LEFT(CompanyIndustry, 1)),
    LCASE(SUBSTRING(CompanyIndustry, 2)));*/
