<?php
/** Created by phpWebStudio(c) 2013 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Восстановление пароля [recovery] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.content", "recovery.16.tpl");

include_once "models/SiteUser.php";
$objSiteUser = new SiteUser();

if ( isset( $_POST['recovery']) && $_POST['recovery'] == 'true' ) {
    $arrSqlData['strEmailToRecover'] = mysql_real_escape_string(trim($_REQUEST['strEmailToRecover']));
    $arrTplVars['strEmailToRecover'] = htmlspecialchars(trim($_REQUEST['strEmailToRecover']));

    if ( empty($arrSqlData['strEmailToRecover']) || !$objSiteUser->checkEmail($arrSqlData['strEmailToRecover']) ) {
        $arrIf['no.email'] = true;
    } else {
        /**
         * Поиск клиента
         */
//        $strSqlQuery = "SELECT su_id, su_login, su_passw FROM `site_users` WHERE su_email='".$arrSqlData['strEmailToRecover']."'";
//        $arrExistSuchUser = $objDb->fetch( $strSqlQuery );

        $userId = $objSiteUser->isEmailExist($arrSqlData['strEmailToRecover']);
        if ($userId > 0) {
            $arrExistSuchUser = $objSiteUser->setId($userId)->getData();
        }

        if ( is_array( $arrExistSuchUser ) && !empty( $arrExistSuchUser['su_login'] ) && !empty( $arrExistSuchUser['su_passw'] ) ) {
            # Логин и Пароль есть, осталось только отправить
            if ( !is_object( $objMail ) ) {
                include_once( "cls.mail.php" );
                $objMail = new clsMail();
            }

            $newPassword = $objSiteUser->getRandomPassw();

            if ( strlen( $newPassword ) == 7 ) {
                $strSqlQuery = "UPDATE `site_users` SET"
                    ." su_passw = md5('$newPassword')"
                    ." WHERE su_id='".$arrExistSuchUser['su_id']."'"
                ;
                if ( !$objDb->query( $strSqlQuery ) ) {
                    # sql error
                    $GLOBALS['manStatusError'] = 1;
                    $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
                }

                $sendTo = $arrSqlData['strEmailToRecover'];
                $sendFrom = "info@rabota-ya.ru";
                $sendSubject = "Работа от А до Я | Восстановление пароля";
                $sendMessage = "Уважаемый пользователь сайта Rabota-Ya.Ru!\n";
                $sendMessage .= "Вы, или кто-то другой, запросили напомнить пароль для авторизации на сайте:\n";
                $sendMessage .= "Логин: {$arrExistSuchUser['su_login']}\n";
                $sendMessage .= "Пароль: $newPassword\n";
                $sendMessage .= "\n";
                $sendMessage .= "По всем вопросам обращайтесь по телефону или посредствам E-mail указаных на странице Контакты\n";
                $sendMessage .= "\n\n";
                $sendMessage .= "Не отвечайте на данное письмо! Текст сформирован автоматически почтовым роботом.\n";

                $sendMessage .= "\n\n===============================================================\n\n";

                $sendMessage .= "Dear subscriber of Rabota-Ya.Ru!\n";
                $sendMessage .= "You, or someone else, requested password recovery:\n";
                $sendMessage .= "Login: {$arrExistSuchUser['su_login']}\n";
                $sendMessage .= "Password: $newPassword\n";
                $sendMessage .= "\n";
                $sendMessage .= "If any questions don't hesitate to call or Email us by contacts on http://www.rabota-ya.ru/contacts/\n";
                $sendMessage .= "\n\n";
                $sendMessage .= "Please, don't answer on this Email! This is automatically sent message.\n";

                $sendHTMLMessage = file_get_contents(SITE_TPL_DIR.'emails/password.recovery.html');

                $linkToLogin = SITE_URL.'profile';
                $linkToContacts = SITE_URL.'contacts';
                $sendHTMLMessage = preg_replace("/\{\%LINK_LOGIN\%\}/", $linkToLogin, $sendHTMLMessage);
                $sendHTMLMessage = preg_replace("/\{\%LINK_CONTACTS\%\}/", $linkToContacts, $sendHTMLMessage);

                $sendHTMLMessage = preg_replace("/\{\%login\%\}/", $arrExistSuchUser['su_login'], $sendHTMLMessage);
                $sendHTMLMessage = preg_replace("/\{\%password\%\}/", $newPassword, $sendHTMLMessage);

                $sendHTMLMessage = preg_replace("/\n/", "\r\n", $sendHTMLMessage);

                if (isset($_SERVER['_IS_DEVELOPER_MODE']) && $_SERVER['_IS_DEVELOPER_MODE'] == 1) {
                    $sendTo = "jimmy.webstudio@gmail.com"; // DEBUG
                }
                // $sendTo = 'info@rabota-ya.ru'; // DEBUG

                $objMail->new_mail( $sendFrom, $sendTo, $sendSubject, $sendMessage, $sendHTMLMessage );
                if ($sendTo != "jimmy.webstudio@gmail.com") {
                    $objMail->add_bcc( "jimmy.webstudio@gmail.com" );
                }

                $objMail->add_attachment( PRJ_SITEIMG.'content/email-bg.jpg', 'email-bg.jpg', 'image' );

                if ( !$objMail->send() ) {
                    $arrIf['not.sent.password'] = true;
                } else {
                    $arrIf['sent'] = true;
                }
            } else {
                $arrIf['no.new.password'] = true;
            }
        } else {
            $arrIf['no.such.email'] = true;
        }
    }
}

$objTpl->tpl_if("page.content", $arrIf);
$objTpl->tpl_array("page.content", $arrTplVars);
