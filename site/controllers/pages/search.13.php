<?php
/** Created by phpWebStudio(c) 2013 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Поиск [search] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.content", "search.13.tpl");

$pageSymbol = 'p';
$strSearchJoin = $strSearchTerm = '';

if (!isset($objSiteUser)) {
    $objSiteUser = new SiteUser();
}

if (!isset($objResume)) {
    $objResume = new Resume();
}

if (!isset($objVacancy)) {
    $objVacancy = new Vacancy();
}

// All search parameters
//$_POST['type']
//$_POST['search_city']
//$_POST['query']
//$_POST['categoryId']
//$_POST['search_salary']
//$_POST['searchWhere']
//$_POST['searchHow']
//$_POST['subQuery']
//$_POST['cbxSalaryNotFixed']
//$_POST['cbxEdu']
//$_POST['cbxDrive']
//$_POST['trips']
//$_POST['marriage']
//$_POST['searchGender']
//$_POST['cbxNoAge']
//$_POST['age_from']
//$_POST['age_to']
//$_POST['sbxWorkBusy']
//$_POST['sbxPeriod']
//$_POST['isNoSalary']

if (isset($arrReqUri[1])) {
    switch ($arrReqUri[1]) {
        case 'city':
            if (isset($arrReqUri[2]) && !empty($arrReqUri[2])) {
                $_POST['type'] = 'vacancies';
                $_POST['search_city'] = urldecode($arrReqUri[2]);
            }
            break;

        case 'category':
            if (isset($arrReqUri[2]) && !empty($arrReqUri[2])) {
                $_POST['type'] = 'vacancies';
                $_POST['categoryId'] = intval($arrReqUri[2]);
            }
            break;

        case 'category-resume':
            if (isset($arrReqUri[2]) && !empty($arrReqUri[2])) {
                $_POST['type'] = 'resumes';
                $_POST['categoryId'] = intval($arrReqUri[2]);
            }
            break;

        case 'profession':
            if (isset($arrReqUri[2]) && !empty($arrReqUri[2])) {
                $_POST['type'] = 'vacancies';
                $_POST['query'] = urldecode($arrReqUri[2]);

                /**
                 * Проверим что для данной профессии есть SEO текст
                 */
                $strSqlQuery = "SELECT * FROM `site_seo_texts` WHERE `sst_word` LIKE '".mysql_real_escape_string($_POST['query'])."'";
                $arrInfo = $objDb->fetch($strSqlQuery);
                if (is_array($arrInfo) && !empty($arrInfo)) {
                    $arrIf['show.seo.text'] = true;
                    $arrTplVars['seo.text'] = nl2br(htmlspecialchars($arrInfo['sst_text']));
                }
            }
            break;

        case 'profession-resume':
            if (isset($arrReqUri[2]) && !empty($arrReqUri[2])) {
                $_POST['type'] = 'resumes';
                $_POST['query'] = urldecode($arrReqUri[2]);
            }
            break;

        default:
            break;
    }
}

if (isset($_SESSION['search']) && isset($_GET[$pageSymbol])) { // saved search
    $_POST = $_SESSION['search'];

} else if (!empty($_POST)) { // new search
    if (isset($_SESSION['search'])) {
        unset($_SESSION['search'], $_SESSION['searchParameterSaved']);
    }
    $_SESSION['search'] = $_POST;
}

if (isset($_GET[$pageSymbol])) {
    $_SESSION['search']['p'] = $_GET[$pageSymbol];
}

//echo '<pre>';
//print_r($_POST);
//die('</pre><br />' . __FILE__ . ':' . __LINE__);

if ((isset($_POST['query']) && strlen($_POST['query']) > 2)
    || (isset($_POST['categoryId']) && !empty($_POST['categoryId']))
    || (isset($_POST['search_salary']) && !empty($_POST['search_salary']))
    || (isset($_POST['search_city']) && !empty($_POST['search_city']))
    || (isset($_POST['type']) && !empty($_POST['type']))
   ){

    $arrTplVars['query'] = htmlspecialchars($_POST['query']);
    $querySql = mysql_real_escape_string(trim(strip_tags($_POST['query'])));
    $arrTplVars['search_salary'] = htmlspecialchars($_POST['search_salary']);
    $_POST['search_city'] = !empty($_POST['search_city']) ? htmlspecialchars($_POST['search_city']) : (!empty($_SESSION['myCity'])?$_SESSION['myCity']:'');
    $arrTplVars['search_city'] = htmlspecialchars($_POST['search_city']);
    switch($_POST['type']) {
        case 'resumes':
            $arrTplVars['link'] = 'resume';
            $arrTplVars['strWhatSearched'] = 'резюме';
            // Всего записей в базе
            $strSqlQueryAll = "SELECT COUNT(*) AS intCountAllRecords FROM `job_resumes` WHERE jr_status = 'Y'";

            // по фразе
            if (strlen($querySql) >= 2) {
                if ($_POST['searchWhere'] == 1) {
                    if ($_POST['searchHow'] == 1 && preg_match('/\s/', $querySql)) {
                        $querySql = preg_replace("/[\s]+/", " ", $querySql);
                        $arrWords = explode(' ', $querySql);
                        $arrExpFields = array('jr_title', 'jr_about', 'jr_edu_plus', 'jr_city', 'jr_id');
                        if (is_array($arrWords) && !empty($arrWords)) {
                            foreach ($arrWords as $k => $word) {
                                if (!empty($word)) {
                                    foreach ($arrExpFields as $z => $field) {
                                        if ($field == 'jr_id') {
                                            if (intval($word)) {
                                                $arrWordsExp[] = "`$field` = '".intval($word)."'";
                                            }
                                        } else {
                                            if (strlen($word) > 1) {
                                                $arrWordsExp[] = "`$field` LIKE '%".$word."%'";
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        $strSearchTerm .= " AND (".implode(' OR ', $arrWordsExp).")";
                    } else {
                        $strSearchTerm .= " AND (jr_title LIKE '%".$querySql."%' OR jr_about LIKE '%".$querySql."%' OR jr_edu_plus LIKE '%".$querySql."%' OR jr_city LIKE '".$querySql."' OR jr_id = ".intval($querySql).")";
                    }
                } else {
                    if ($_POST['searchHow'] == 1 && preg_match('/\s/', $querySql)) {
                        $querySql = preg_replace("/[\s]+/", " ", $querySql);
                        $arrWords = explode(' ', $querySql);
                        if (is_array($arrWords) && !empty($arrWords)) {
                            foreach ($arrWords as $k => $word) {
                                if (!empty($word)) {
                                    $arrWordsExp[] = $word;
                                }
                            }
                        }

                        $strSearchTerm .= " AND jr_title REGEXP '".implode('|', $arrWordsExp)."'";
                    } else {
                        $strSearchTerm .= " AND jr_title LIKE '%".$querySql."%'";
                    }
                }
//                $strSearchTerm = " AND (jr_title LIKE '%".$querySql."%' OR jr_about LIKE '%".$querySql."%' OR jr_edu_plus LIKE '%".$querySql."%' OR jr_city LIKE '".$querySql."')";
            }

            // исключить слова
            if (!empty($_POST['subQuery']) && strlen($_POST['subQuery'])) {
                $excludeSql = mysql_real_escape_string(trim($_POST['subQuery']));
                if (preg_match("/\s/", $excludeSql)) {
                    $excludeSql = preg_replace("/[\s]+/", " ", $excludeSql);
                    $arrWords = explode(' ', $excludeSql);
                    if (is_array($arrWords) && !empty($arrWords)) {
                        foreach ($arrWords as $k => $word) {
                            if (!empty($word)) {
                                $strSearchTerm .= " AND jr_title NOT LIKE '%".$word."%' AND jr_about NOT LIKE '%".$word."%' AND jr_edu_plus NOT LIKE '%".$word."%' AND jr_city NOT LIKE '".$word."'";
                            }
                        }
                    }
                } else {
                    $strSearchTerm .= " AND jr_title NOT LIKE '%".$excludeSql."%' AND jr_about NOT LIKE '%".$excludeSql."%' AND jr_edu_plus NOT LIKE '%".$excludeSql."%' AND jr_city NOT LIKE '".$excludeSql."'";
                }
            }

            // по зарплате
            if (isset($_POST['search_salary']) && intval($_POST['search_salary'])) {
                $strSearchTerm .= " AND (jr_salary >= ".$_POST['search_salary'];
                $mustClose = true;
            } elseif (intval($_POST['salary_from']) && intval($_POST['salary_to'])) {
                $strSearchTerm .= " AND (jr_salary BETWEEN ".$_POST['salary_from']." AND ".$_POST['salary_to'];
                $mustClose = true;
            } elseif (intval($_POST['salary_from']) && !intval($_POST['salary_to'])) {
                $strSearchTerm .= " AND (jr_salary >= ".$_POST['salary_from'];
                $mustClose = true;
            } elseif (intval($_POST['salary_to']) && !intval($_POST['salary_from'])) {
                $strSearchTerm .= " AND (jr_salary <= ".$_POST['salary_to'];
                $mustClose = true;
            }
            if (isset($_POST['cbxSalaryNotFixed']) && !empty($_POST['cbxSalaryNotFixed'])) {
                $strSearchTerm .= ($mustClose ? ' OR' : ' AND')." (jr_salary IS NULL OR jr_salary LIKE '')";
            }
            if ($mustClose) {
                $strSearchTerm .= ")";
            }
            // по городу
            if (isset($_POST['search_city']) && strlen($_POST['search_city'])>2) {
                $cityId = $objSiteUser->getCityId(trim($_POST['search_city']));
                if ($cityId > 0) {
                    $strSearchTerm .= " AND (jr_city LIKE '%".$_POST['search_city']."%' OR jr_city_id = $cityId)";
                } else {
                    $strSearchTerm .= " AND jr_city LIKE '%".$_POST['search_city']."%'";
                }
            } elseif (isset($_SESSION['myCity']) && !empty($_SESSION['myCity'])) {
                $cityId = $objSiteUser->getCityId(trim($_SESSION['myCity']));
                if ($cityId > 0) {
                    $strSearchTerm .= " AND (jr_city LIKE '%".$_SESSION['myCity']."%' OR jr_city_id = $cityId)";
                } else {
                    $strSearchTerm .= " AND jr_city LIKE '".$_SESSION['myCity']."'";
                }
            }
            // по категории
            if ($_POST['categoryId'] > 0) {
                $strSearchJoin = " LEFT JOIN `job_category_link` ON (jcl_rel_id = jr_id AND jcl_type = 'resume')";
                $strSearchTerm .= " AND jcl_sjc_id = ".$_POST['categoryId'];
            }
            // по образованию
            if (isset($_POST['cbxEdu']) && ((is_array($_POST['cbxEdu']) || intval($_POST['cbxEdu'])) && !empty($_POST['cbxEdu'])) ) {
                $strSearchJoin = " LEFT JOIN `job_resumes_education` ON (jre_jr_id = jr_id)";
                if (is_array($_POST['cbxEdu']))
                    $strSearchTerm .= " AND jre_jet_id IN (".implode(',',$_POST['cbxEdu']).")";
                else
                    $strSearchTerm .= " AND jre_jet_id IN (".intval($_POST['cbxEdu']).")";
            }
            // по водительским правам
            if (isset($_POST['cbxDrive']) && !empty($_POST['cbxDrive'])) {
                $strSearchTerm .= " AND jr_driver = 'Y'";
            }
            // по командировкам
            if (isset($_POST['trips']) && !empty($_POST['trips'])) {
                $strSearchTerm .= " AND jr_trips = '".mysql_real_escape_string($_POST['trips'])."'";
            }
            // по семейному
            if (isset($_POST['marriage']) && !empty($_POST['marriage'])) {
                $strSearchTerm .= " AND jr_marriage = '".mysql_real_escape_string($_POST['marriage'])."'";
            }
            // по полу
            if (isset($_POST['searchGender']) && !empty($_POST['searchGender'])) {
                $strSearchTerm .= " AND jr_sex = '".mysql_real_escape_string($_POST['searchGender'])."'";
            }
            // по возрасту
            if (isset($_POST['cbxNoAge']) && !empty($_POST['cbxNoAge'])) {
                $strSearchTerm .= " AND jr_age IS NULL AND jr_birthday IS NULL";
            } else {
                $strDateAgeFrom = $strDateAgeTo = null;
                if (intval($_POST['age_from'])) {
                    $strDateAgeTo = date("Y-12-31", strtotime((date('Y') - $_POST['age_from']).date('-m-d')));
                }
                if (intval($_POST['age_to'])) {
                    $strDateAgeFrom = date("Y-01-01", strtotime((date('Y') - $_POST['age_to']).date('-m-d')));
                }

                if (intval($_POST['age_from']) && intval($_POST['age_to'])) {
                    $strSearchTerm .= " AND ((jr_birthday BETWEEN '$strDateAgeFrom' AND '$strDateAgeTo' AND jr_birthday IS NOT NULL) OR (jr_age IS NOT NULL AND jr_age BETWEEN ".intval($_POST['age_from'])." AND ".intval($_POST['age_to'])."))";
                } elseif (intval($_POST['age_from'])) {
                    $strSearchTerm .= " AND ((jr_birthday BETWEEN '".date("Y-01-01", strtotime('-100 years'))."' AND '$strDateAgeTo' AND jr_birthday IS NOT NULL) OR (jr_age IS NOT NULL AND jr_age >= ".intval($_POST['age_from'])."))";
                } elseif (intval($_POST['age_to'])) {
                    $strSearchTerm .= " AND ((jr_birthday BETWEEN '$strDateAgeFrom' AND CURDATE() AND jr_birthday IS NOT NULL) OR (jr_age IS NOT NULL AND jr_age <= ".intval($_POST['age_to'])."))";
                }
            }

            // по занятости
            if (isset($_POST['sbxWorkBusy']) && ((is_array($_POST['sbxWorkBusy']) || intval($_POST['sbxWorkBusy'])) && !empty($_POST['sbxWorkBusy'])) ) {
                if (is_array($_POST['sbxWorkBusy']))
                    $strSearchTerm .= " AND jr_work_busy IN (".implode(',',$_POST['sbxWorkBusy']).")";
                else
                    $strSearchTerm .= " AND jr_work_busy IN (".intval($_POST['sbxWorkBusy']).")";
            }

            // за период
            if (isset($_POST['sbxPeriod']) && !empty($_POST['sbxPeriod'])) {
                switch($_POST['sbxPeriod']) {
                    case 1: $strTimeFrom = strtotime("-1 day"); break;
                    case 2: $strTimeFrom = strtotime("-3 days"); break;
                    case 3: $strTimeFrom = strtotime("-1 week"); break;
                    case 4: $strTimeFrom = strtotime("-2 weeks"); break;
                    case 5: $strTimeFrom = strtotime("-1 month"); break;
                    case 6: $strTimeFrom = strtotime("-3 months"); break;
                    default:$strTimeFrom = null; break;
                }
                if (!empty($strTimeFrom))
                    $strSearchTerm .= " AND jr_date_publish BETWEEN ".$strTimeFrom." AND UNIX_TIMESTAMP()";
            }

            // Выбрано записей
            $strSqlQueryFilter = "SELECT COUNT(*) AS intQuantitySelectRecords FROM `job_resumes` $strSearchJoin WHERE jr_status = 'Y'".$strSearchTerm;
            echo '<!--', $strSqlQueryFilter, '-->';
            //die ('<!--'. $strSqlQueryFilter. '-->');
            break;

        case 'vacancies':
            $arrTplVars['link'] = 'vacancy';
            $arrTplVars['strWhatSearched'] = 'вакансий';

            // Всего записей в базе
            $strSqlQueryAll = "SELECT COUNT(*) AS intCountAllRecords FROM `job_vacancies` WHERE jv_status = 'Y'";

            // Поисковые фильтры
            if (strlen($querySql) >= 2) {
                if ($_POST['searchWhere'] == 1) {
                    if ($_POST['searchHow'] == 1 && preg_match('/\s/', $querySql)) {
                        $querySql = preg_replace("/[\s]+/", " ", $querySql);
                        $arrWords = explode(' ', $querySql);
                        $arrExpFields = array('jv_title', 'jv_description', 'jv_city', 'jv_id');
                        if (is_array($arrWords) && !empty($arrWords)) {
                            foreach ($arrWords as $k => $word) {
                                if (!empty($word)) {
                                    foreach ($arrExpFields as $z => $field) {
                                        if ($field == 'jv_id') {
                                            if (intval($word)) {
                                                $arrWordsExp[] = "`$field` = '".intval($word)."'";
                                            }
                                        } else {
                                            if (strlen($word) > 1) {
                                                $arrWordsExp[] = "`$field` LIKE '%".$word."%'";
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        $strSearchTerm .= " AND (".implode(' OR ', $arrWordsExp).")";
                    } else {
                        $strSearchTerm .= " AND (jv_title LIKE '%".$querySql."%' OR jv_description LIKE '%".$querySql."%' OR jv_city LIKE '".$querySql."' OR jv_id = ".intval($querySql).")";
                    }
                } else {
                    if ($_POST['searchHow'] == 1 && preg_match('/\s/', $querySql)) {
                        $querySql = preg_replace("/[\s]+/", " ", $querySql);
                        $arrWords = explode(' ', $querySql);
                        if (is_array($arrWords) && !empty($arrWords)) {
                            foreach ($arrWords as $k => $word) {
                                if (!empty($word)) {
                                    $arrWordsExp[] = $word;
                                }
                            }
                        }

                        $strSearchTerm .= " AND jv_title REGEXP '".implode('|', $arrWordsExp)."'";
                    } else {
                        $strSearchTerm .= " AND jv_title LIKE '%".$querySql."%'";
                    }
                }
            }

            // исключить слова
            if (!empty($_POST['subQuery']) && strlen($_POST['subQuery'])) {
                $excludeSql = mysql_real_escape_string(trim($_POST['subQuery']));
                if (preg_match("/\s/", $excludeSql)) {
                    $excludeSql = preg_replace("/[\s]+/", " ", $excludeSql);
                    $arrWords = explode(' ', $excludeSql);
                    if (is_array($arrWords) && !empty($arrWords)) {
                        foreach ($arrWords as $k => $word) {
                            if (!empty($word)) {
                                $strSearchTerm .= " AND jv_title NOT LIKE '%".$word."%' AND jv_description NOT LIKE '%".$word."%' AND jv_city NOT LIKE '".$word."'";
                            }
                        }
                    }
                } else {
                    $strSearchTerm .= " AND jv_title NOT LIKE '%".$excludeSql."%' AND jv_description NOT LIKE '%".$excludeSql."%' AND jv_city NOT LIKE '".$excludeSql."'";
                }
            }

            // зарплата
            if (isset($_POST['search_salary']) && intval($_POST['search_salary'])) {
                $strSearchTerm .= " AND jv_salary_from >= ".$_POST['search_salary'];
            }

            if (!empty($_POST['isNoSalary'])) {
                $strSearchTerm .= " AND (jv_salary_from IS NULL OR jv_salary_from = 0 OR jv_salary_from = '')";
            }

            // город
            if (isset($_POST['search_city']) && strlen($_POST['search_city'])>2) {
                $cityId = $objSiteUser->getCityId(trim($_POST['search_city']));
                if ($cityId > 0) {
                    $strSearchTerm .= " AND (jv_city LIKE '%".$_POST['search_city']."%' OR jv_city_id = $cityId)";
                } else {
                    $strSearchTerm .= " AND jv_city LIKE '%".$_POST['search_city']."%'";
                }
            } elseif (isset($_SESSION['myCity']) && !empty($_SESSION['myCity'])) {
                $cityId = $objSiteUser->getCityId(trim($_SESSION['myCity']));
                if ($cityId > 0) {
                    $strSearchTerm .= " AND (jv_city LIKE '%".$_SESSION['myCity']."%' OR jv_city_id = $cityId)";
                } else {
                    $strSearchTerm .= " AND jv_city LIKE '%".$_SESSION['myCity']."%'";
                }
            }
            // категория
            if ($_POST['categoryId'] > 0) {
                $strSearchJoin = " LEFT JOIN `job_category_link` ON (jcl_rel_id = jv_id AND jcl_type = 'vacancy')";
                $strSearchTerm .= " AND jcl_sjc_id = ".$_POST['categoryId'];
            }
            // по образованию
            if (isset($_POST['cbxEdu']) && ((is_array($_POST['cbxEdu']) || intval($_POST['cbxEdu'])) && !empty($_POST['cbxEdu']))) {
                if (is_array($_POST['cbxEdu']))
                    $strSearchTerm .= " AND jv_jet_id IN (".implode(',',$_POST['cbxEdu']).")";
                else
                    $strSearchTerm .= " AND jv_jet_id IN (".intval($_POST['cbxEdu']).")";
            }
            // по занятости
            if (isset($_POST['cbxWorkBusy']) && ((is_array($_POST['cbxWorkBusy']) || intval($_POST['cbxWorkBusy'])) && !empty($_POST['cbxWorkBusy'])) ) {
                if (is_array($_POST['cbxWorkBusy']))
                    $strSearchTerm .= " AND jv_jw_id IN (".implode(',',$_POST['cbxWorkBusy']).")";
                else
                    $strSearchTerm .= " AND jv_jw_id IN (".$_POST['cbxWorkBusy'].")";
            }
            // по водительским правам
            if (isset($_POST['cbxDrive']) && !empty($_POST['cbxDrive'])) {
                $strSearchTerm .= " AND jv_driver = 'Y'";
            }
            // по командировкам
            if (isset($_POST['cbxTrips']) && !empty($_POST['cbxTrips'])) {
                $strSearchTerm .= " AND jv_trips = 'Y'";
            }
            // за период
            if (isset($_POST['sbxPeriod']) && !empty($_POST['sbxPeriod'])) {
                switch($_POST['sbxPeriod']) {
                    case 1: $strTimeFrom = strtotime("-1 day"); break;
                    case 2: $strTimeFrom = strtotime("-3 days"); break;
                    case 3: $strTimeFrom = strtotime("-1 week"); break;
                    case 4: $strTimeFrom = strtotime("-2 weeks"); break;
                    case 5: $strTimeFrom = strtotime("-1 month"); break;
                    case 6: $strTimeFrom = strtotime("-3 months"); break;
                    default:$strTimeFrom = null; break;
                }
                if (!empty($strTimeFrom))
                    $strSearchTerm .= " AND jv_date_publish BETWEEN ".$strTimeFrom." AND UNIX_TIMESTAMP()";
            }

            // Выбрано записей
            $strSqlQueryFilter = "SELECT COUNT(*) AS intQuantitySelectRecords FROM `job_vacancies` $strSearchJoin WHERE jv_status = 'Y'".$strSearchTerm;
            echo '<!--', $strSqlQueryFilter, '-->';
            break;

        case 'companies':
            $arrTplVars['link'] = 'company-vacancies';
            $arrTplVars['strWhatSearched'] = 'компаний';
            // Всего записей в базе
            $strSqlQueryAll = "SELECT COUNT(*) AS intCountAllRecords FROM `job_companies` WHERE jc_status = 'Y'";

            // Выборка записей
            $strSearchTerm = " AND jc_title LIKE '%".mysql_real_escape_string($_POST['query'])."%'";

            /*if (isset($_POST['search_city']) && strlen($_POST['search_city'])>2) {
                $cityId = $objSiteUser->getCityId(trim($_POST['search_city']));
                if ($cityId > 0) {
                    $strSearchTerm .= " AND (jc_city LIKE '%".$_POST['search_city']."%' OR jc_city_id = $cityId)";
                } else {
                    $strSearchTerm .= " AND jc_city LIKE '%".$_POST['search_city']."%'";
                }
            } elseif (isset($_SESSION['myCity']) && !empty($_SESSION['myCity'])) {
                $cityId = $objSiteUser->getCityId(trim($_SESSION['myCity']));
                if ($cityId > 0) {
                    $strSearchTerm .= " AND (jc_city LIKE '%".$_SESSION['myCity']."%' OR jc_city_id = $cityId OR (jc_city = '' AND jc_city_id IS NULL))";
                } else {
                    $strSearchTerm .= " AND jc_city LIKE '".$_SESSION['myCity']."'";
                }
            }*/
            $strSqlQueryFilter = "SELECT COUNT(*) AS intQuantitySelectRecords FROM `job_companies` $strSearchJoin WHERE jc_status = 'Y'".$strSearchTerm;
            break;
        default:
            break;
    }

//    echo '<pre>';
//    print_r($strSqlQueryFilter);
//    die('</pre><br />' . __FILE__ . ':' . __LINE__);

//    if ($_SERVER['REMOTE_ADDR'] == '188.134.80.143') {
//        echo '<!--'.$strSqlQueryFilter.'-->';
//        echo '<br />';
//        die(__FILE__ .':' . __LINE__);
//    }

    $arrTplVars['intCountAllRecords'] = $objDb->fetch( $strSqlQueryAll, 'intCountAllRecords');
    $arrTplVars['intQuantitySelectRecords'] = $objDb->fetch( $strSqlQueryFilter, 'intQuantitySelectRecords');

    // ***** BEGIN: Построение пейджинга для вывода списка
    $objPagination = new tplPaginator($arrTplVars['intQuantitySelectRecords'], SITE_TPL_TPL_DIR, $pageSymbol);

    /**
     * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
     * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
     */
    $objPagination->strPaginatorTpl = "site.global.4.tpl";
    $objPagination->intQuantRecordPage = 25;
    $objPagination->strColorActivePage = '8A0202';
    $objPagination->strColorLinkStyle = "link-b";
    $objPagination->strColorActiveStyle = "tab-bl-b";
    $objPagination->strNameImageBack = "arrow_one_left.gif";
    $objPagination->strNameImageForward = "arrow_one_right.gif";
    // Создаем блок пэйджинга
    $objPagination->paCreate();
    // ***** END: Построение пейджинга для вывода списка

    // Запрос для выборки нужных записей
    switch($_POST['type']) {
        case 'resumes':
            $arrTplVars['strSearchAnotherCat'] = '-resume';
            $arrFields['jr_date_publish'] = 'date_publish';
            $arrFields['jr_salary'] = 'salary';
            $arrFields['jr_id'] = 'id';
            $arrFields['jr_about'] = 'description';
            $arrFields['jr_title'] = 'title';
//            $arrFields[''] = '';
            foreach ($arrFields as $field => $new) {
                $arrFieldParams[] = "`$field` AS $new";
            }
            $strFields = implode(',',$arrFieldParams);
            $strSqlQuery = "SELECT $strFields FROM `job_resumes` ".$strSearchJoin
                ." WHERE jr_status = 'Y'".$strSearchTerm
                ." ORDER BY jr_date_publish DESC, jr_id DESC ".$objPagination->strSqlLimit;

            break;

        case 'vacancies':
            $arrFields['jv_salary_from'] = 'salary_from';
            $arrFields['jv_salary_to'] = 'salary_to';
            $arrFields['jv_salary'] = 'salary';
            $arrFields['jv_id'] = 'id';
            $arrFields['jv_description'] = 'description';
            $arrFields['jv_title'] = 'title';
            foreach ($arrFields as $field => $new) {
                $arrFieldParams[] = "`$field` AS $new";
            }
            $strFields = implode(',',$arrFieldParams);
            $strSqlQuery = "SELECT $strFields FROM `job_vacancies` ".$strSearchJoin
                ." WHERE jv_status = 'Y'".$strSearchTerm
                ." ORDER BY jv_date_publish DESC, jv_id DESC ".$objPagination->strSqlLimit;
            break;

        case 'companies':
            $arrFields['jc_id'] = 'id';
            $arrFields['jc_description'] = 'description';
            $arrFields['jc_title'] = 'title';
            foreach ($arrFields as $field => $new) {
                $arrFieldParams[] = "`$field` AS $new";
            }
            $strFields = implode(',',$arrFieldParams);
            $strSqlQuery = "SELECT $strFields FROM `job_companies` ".$strSearchJoin
                ." WHERE jc_status = 'Y'".$strSearchTerm
                ." ORDER BY jc_date_create DESC, jc_id DESC ".$objPagination->strSqlLimit;
            break;
        default:
            break;
    }
    $arrRecords = $objDb->fetchall( $strSqlQuery );

    // кол-во публикаций показанных на странице
    $arrTplVars['intQuantityShowRecOnPage'] = ( !empty($arrRecords) ? count($arrRecords) : 0 );
    // Присвоение значения пэйджинга для списка
    if ($arrTplVars['intQuantityShowRecOnPage'] > 0) {
        $arrTplVars['blockPaginator'] = $objPagination->paShow();
    }

    if ( is_array($arrRecords) ) {
        foreach ( $arrRecords as $key => $value ) {
            $arrRecords[$key]['strDatePublish'] = date('d.m.y', $value['date_publish']);
            $arrRecords[$key]['strSalary'] = (!empty($value['salary_from']) ? 'от '.$value['salary_from'] : '').(!empty($value['salary_to']) ? (!empty($value['salary_from'])?' до ':'').$value['salary_to'] : '');
            if (!empty($arrRecords[$key]['strSalary'])) {
                $arrRecords[$key]['strSalary'] .= ' руб';
            } elseif (!empty($value['salary'])) {
                $arrRecords[$key]['strSalary'] = $value['salary'].' руб';
            }

            $arrRecords[$key]['strDescription'] = $objUtils->substrText($value['description'], 220, true);
            $arrRecords[$key]['strTitle'] = mb_ucfirst($value['title']);

            $arrRecords[$key]['strSeoUrl'] = $value['id'];

            if ($_POST['type'] == 'resumes') {
                $arrRecords[$key]['strSeoUrl'] = strtolower( $objUtils->translitBadChars($objUtils->translitKyrToLat(trim($value['title'])), array(" ", '.')))."_".$value['id'];
                $arrRecords[$key]['strSeoUrl'] = str_replace('__', '_', $arrRecords[$key]['strSeoUrl']);
                $objResume->setId($value['id'])->saveSeoUrl($arrRecords[$key]['strSeoUrl']);
            }

            if ($_POST['type'] == 'vacancies') {
                $arrRecords[$key]['strSeoUrl'] = strtolower( $objUtils->translitBadChars($objUtils->translitKyrToLat(trim($value['title'])), array(" ", '.')))."_".$value['id'];
                $arrRecords[$key]['strSeoUrl'] = str_replace('__', '_', $arrRecords[$key]['strSeoUrl']);
                $objVacancy->setId($value['id'])->saveSeoUrl($arrRecords[$key]['strSeoUrl']);
            }
        }
    }
}

include_once "models/JobCategory.php";
if (!isset($objJobCategory) && !is_object($objJobCategory)) {
    $objJobCategory = new JobCategory();
}
$arrTplVars['intPreselectedCategoryId'] = 0;
if (isset($_POST['categoryId']) && !empty($_POST['categoryId'])) {
    $arrTplVars['strPreselectedCategoryTitle'] = $objJobCategory->getCategoryFieldById($_POST['categoryId'], 'jc_title');
    $arrTplVars['intPreselectedCategoryId'] = $_POST['categoryId'];
    $arrTplVars['strWhatSearched'] = 'по отрасли: <span>'.$arrTplVars['strPreselectedCategoryTitle'].'</span>';
    $arrIf['cat.search'] = true;
}

$arrIf['no.results'] = empty($arrRecords);
$objTpl->tpl_loop("page.content", "list.results", $arrRecords);
$objTpl->tpl_array("page.content", $arrTplVars);

$arrIf['authed.and.not.saved'] = (!$arrIf['no.results'] && $arrIf['authed'] && !$_SESSION['searchParameterSaved'] && !(isset($arrReqUri[1]) && (in_array($arrReqUri[1], array('profession', 'category')))));

$objTpl->tpl_if("page.content", $arrIf);
