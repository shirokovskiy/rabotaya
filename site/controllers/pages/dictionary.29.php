<?php
/** Created by phpWebStudio(c) 2013 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Словарь профессий [dictionary] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.content", "dictionary.29.tpl");

$arrRusChars = array();
if (is_array($objUtils->arrRusChars) && !empty($objUtils->arrRusChars)) {
    foreach ($objUtils->arrRusChars as $k => $char) {
        $arrRusChars[$k]['char'] = $char;
        $arrRusChars[$k]['charUrl'] = urlencode($char);
    }
}
$objTpl->tpl_loop("page.content", "rus.chars", $arrRusChars);

$arrEngChars = array();
if (is_array($objUtils->arrEngChars) && !empty($objUtils->arrEngChars)) {
    foreach ($objUtils->arrEngChars as $k => $char) {
        $arrEngChars[$k]['char'] = $char;
        $arrEngChars[$k]['charUrl'] = urlencode($char);
    }
}
$objTpl->tpl_loop("page.content", "eng.chars", $arrEngChars);

$arrTplVars['showParams'] = 'false';
$strSearchTerm = null;
if (isset($arrReqUri[1]) && !empty($arrReqUri[1])) {
    /**
     * Проверка ввода параметров
     */
    switch ($arrReqUri[1]) {
        case 'words-by-first-char':
            if (isset($arrReqUri[2]) && !empty($arrReqUri[2])) {
                $strSearchTerm = " AND sa_title LIKE '".mysql_real_escape_string(urldecode($arrReqUri[2]))."%'";
                $arrTplVars['showParams'] = 'true';
                $arrTplVars['charSelected'] = urldecode($arrReqUri[2]);
            }
            break;

        default:
            break;
    }
}


if (isset($arrReqUri[1]) && !empty($arrReqUri[1]) && empty($strSearchTerm)) {
    /**
     * Проверить если есть третий параметр
     */
    if(isset($arrReqUri[2]) && !empty($arrReqUri[2])) {
        $nDate = $arrReqUri[1];
        $nUrl = trim($arrReqUri[2]);
        $strSqlQuery = "SELECT * FROM site_articles WHERE sa_status='Y' AND sa_type='dict' AND sa_url='".$nUrl."' AND sa_date_publish=".$nDate;
    } else {
        /**
         * Если нет, тогда это просто слово
         */
        $intId = intval($arrReqUri[1]);
        $strSqlQuery = "SELECT * FROM site_articles WHERE sa_status='Y' AND sa_type='dict' AND sa_id=".$intId;
        $flagUpdate = true;
    }
    $arrWordInfo = $objDb->fetch( $strSqlQuery );
    // Проверим, существует ли такая запись, и если да, то обработаем для вывода
    if (!empty($arrWordInfo)) {
        $arrIf['record'] = true;
        if(!$flagUpdate) {
            $intId = intval($arrWordInfo['sa_id']);
        }
        $arrWordInfo['strDatePublishArticle'] = $objUtils->workDate(2, date('Y-m-d',$arrWordInfo['sa_date_publish']));
        $arrWordInfo['strArticleBody'] = nl2br($arrWordInfo['sa_content']);
        $arrIf['picture'] = ( file_exists(PRJ_IMAGES."articles/articles.photo.".$arrWordInfo['sa_id'].".jpg") && $arrWordInfo['sa_image'] == 'Y');
        $objTpl->tpl_array("page.content", $arrWordInfo);

        $arrTplVars['m_title'] = htmlspecialchars(strip_tags($arrWordInfo['sa_title'])).' | '.$arrTplVars['m_title'];
        $arrTplVars['m_description'] = $objUtils->substrText(htmlspecialchars(strip_tags($arrWordInfo['sa_content'])), 250);

        $arrTplVars['strUniqueMainPhoto'] = $arrIf['picture'] ? '?pic='.time() : '';

        // Показать картинки, если они есть
        if ($arrWordInfo['sa_image']=='Y') {
            # Дополнительные картинки есть только в том случае, если есть оснавная
            $strSqlQuery = "SELECT si_id,si_desc FROM site_images WHERE si_status='Y' AND si_rel_id = ".$intId." AND si_type = 'article'";
            $arrMoreImages = $objDb->fetchall( $strSqlQuery );
            $arrIf['pic_group'] = is_array($arrMoreImages) && !empty($arrMoreImages);
        }
    } else {
        header('Location: '.SITE_URL.'error');
        exit();
    }
} else {
    // Всего записей в базе
    $strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM `site_articles` WHERE sa_status = 'Y' AND sa_type='dict' AND sa_date_publish <= NOW()".$strSearchTerm;
    $arrTplVars['intQuantitySelectRecords'] = $arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');

    $arrIf['no.records'] = $arrTplVars['intQuantitySelectRecords'] == 0;

    // Выбрано записей
    //$strSqlQuery = "SELECT COUNT(*) AS intQuantitySelectRecords FROM `site_articles` WHERE sa_status = 'Y' AND sa_date_publish <= DATE_FORMAT(NOW(), '%Y-%m-%d')";
    //$arrTplVars['intQuantitySelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantitySelectRecords');
    // ***** BEGIN: Построение пейджинга для вывода списка
    $objPagination = new tplPaginator($arrTplVars['intQuantitySelectRecords'], SITE_TPL_TPL_DIR);
    /**
     * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
     * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
     */
    $objPagination->strPaginatorTpl = "site.global.4.tpl";
    $objPagination->intQuantRecordPage = 15;
    $objPagination->strColorLinkStyle = "link-b";
    $objPagination->strColorActiveStyle = "tab-bl-b";
    $objPagination->strNameImageBack = "arrow_one_left.gif";
    $objPagination->strNameImageForward = "arrow_one_right.gif";
    // Создаем блок пэйджинга
    $objPagination->paCreate();
    // ***** END: Построение пейджинга для вывода списка
    // Запрос для выборки нужных записей
    $strSqlQuery = "SELECT * FROM `site_articles`"
        ." WHERE sa_status = 'Y' AND sa_type = 'dict' AND sa_date_publish <= NOW()".$strSearchTerm
        ." ORDER BY sa_title ".$objPagination->strSqlLimit;
    $arrWords = $objDb->fetchall( $strSqlQuery );
    // кол-во публикаций показанных на странице
    $arrTplVars['intQuantityShowRecOnPage'] = ( !empty($arrWords) ? count($arrWords) : 0 );
    if ($arrTplVars['intQuantityShowRecOnPage'] >= 1) {
        // Присвоение значения пэйджинга для списка
        $arrTplVars['blockPaginator'] = $objPagination->paShow();
    }

    if ( is_array($arrWords) ) {
        foreach ( $arrWords as $key => $value ) {
            $arrWords[$key]['strDatePublishArticle'] = date('d.m.Y', $value['sa_date_publish']);
            $arrWords[$key]['urlParam'] = (!empty($value['sa_url']) ? ((!empty($value['sa_date_publish']) ? $value['sa_date_publish'].'/' : '').$value['sa_url']) : $value['sa_id'] );
            $arrIf['dictionary.picture.'.$value['sa_id']] = ( file_exists(DROOT."storage/images/dictionary/dictionary.photo.".$value['sa_id'].".jpg") && $value['sa_image'] == 'Y');
//            $arrWords[$key]['strUniquePhoto'] = $arrIf['articles.picture.'.$value['sa_id']] ? '?pic='.time() : '';
        }
    }

    $objTpl->tpl_loop("page.content", "dictionary.words", $arrWords);
    $arrIf['list'] = true;
}

$objTpl->tpl_if("page.content", $arrIf);
$objTpl->tpl_array("page.content", $arrTplVars);
