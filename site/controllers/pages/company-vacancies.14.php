<?php
include_once "models/Company.php";

/** Created by phpWebStudio(c) 2013 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Вакансии ведущей компании [company-vacancies] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.content", "company-vacancies.14.tpl");

if (isset($arrReqUri[1]) && !empty($arrReqUri[1])) {
    $intRecordId = intval($arrReqUri[1]);

    $objCompany = new Company($_db_config);
    if (!isset($objSiteUser)) {
        $objSiteUser = new SiteUser();
    }

    // Выборка по компании
    $arrCompany = $objCompany->getCompany($intRecordId);

    $arr_string_fields = array('strCompanyName'=>'jc_title', 'strCity'=>'jc_city', 'strWeb'=>'jc_web', 'strEmail'=>'jc_email', 'strPhone'=>'jc_phone', 'strCompanyDescription'=>'jc_description');
    foreach($arr_string_fields as $key => $field) {
        $arrTplVars[$key] = (!empty($arrCompany[$field]) ? htmlspecialchars($arrCompany[$field]) : '');
        $arrIf['is.'.$key] = !empty($arrTplVars[$key]);
    }

    $arrTplVars['strCompanyDescription'] = nl2br($arrTplVars['strCompanyDescription']);

    $arrTplVars['strLogo'] = $objCompany->getLogo($intRecordId);
    $arrIf['is.strLogo'] = !empty($arrTplVars['strLogo']);
    $arrTplVars['strWeb'] = (!empty($arrTplVars['strWeb'])?$arrTplVars['strWeb']:'');
    if ($arrCompany['jc_city_id'] > 0) {
        $arrCity = $objSiteUser->getCityById($arrCompany['jc_city_id']);
        if (is_array($arrCity) && !empty($arrCity)) {
            $arrTplVars['strCity'] = $arrCity['city'];
        }
        $arrIf['is.strCity'] = !empty($arrTplVars['strCity']);
    }


    // Запрос для выборки нужных записей
    $strSqlQuery = "SELECT * FROM `job_vacancies`"
        ." WHERE jv_status = 'Y' AND `jv_jc_id` = ".$intRecordId
        ." ORDER BY jv_date_publish DESC, jv_id DESC";
    $arrVacancies = $objDb->fetchall( $strSqlQuery );

    if ( is_array($arrVacancies) ) {
        foreach ( $arrVacancies as $key => $value ) {
            $arrVacancies[$key]['jv_title'] = mb_ucfirst($value['jv_title']);
            $arrVacancies[$key]['strDatePublish'] = date('d.m.y', $value['jv_date_publish']);
            $arrVacancies[$key]['strUniquePhoto'] = $arrIf['news.picture.'.$value['jv_id']] ? '?pic='.time() : '';
            $arrVacancies[$key]['urlParam'] = (!empty($value['jv_url']) ? ((!empty($value['jv_date_publish']) ? $value['jv_date_publish'].'/' : '').$value['jv_url']) : $value['jv_id'] );

            $arrVacancies[$key]['strSalary'] = (!empty($value['jv_salary_from']) ? 'от '.$value['jv_salary_from'] : '').(!empty($value['jv_salary_to']) ? (!empty($value['jv_salary_from'])?' до ':'').$value['jv_salary_to'] : '');
            if (!empty($arrVacancies[$key]['strSalary'])) {
                $arrVacancies[$key]['strSalary'] .= ' руб';
            } elseif (!empty($value['jv_salary'])) {
                $arrVacancies[$key]['strSalary'] = $value['jv_salary'];
            }

            $arrVacancies[$key]['strDescription'] = $objUtils->substrText(strip_tags($value['jv_description']), 220, true);
            $arrIf['is.strDescription.'.$value['jv_id']] = !empty($arrVacancies[$key]['strDescription']);

            $arrIf['is.list'] = true;
        }
    }
} else {
    header('location: /error'); exit();
}

$objTpl->tpl_loop("page.content", "list.vacancies", $arrVacancies); unset($arrVacancies);
$objTpl->tpl_array("page.content", $arrTplVars);
$objTpl->tpl_if("page.content", $arrIf);
