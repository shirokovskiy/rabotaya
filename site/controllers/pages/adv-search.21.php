<?php
/** Created by phpWebStudio(c) 2013 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Расширеный поиск [adv-search] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.content", "adv-search.21.tpl");

// Категории - Сфера деятельности
$strSqlQuery = "SELECT * FROM `job_categories` WHERE `jc_parent` IS NULL ORDER BY `jc_order`";
$arrCats = $objDb->fetchall($strSqlQuery);
if (is_array($arrCats) && !empty($arrCats)) {
    foreach ($arrCats as $k => $arr) {
        $arrCats[$k]['strOptGroupName'] = htmlspecialchars($arr['jc_title']);
        $arrCats[$k]['intGroupID'] = $arr['jc_id'];
    }
}
$objTpl->tpl_loop("page.content", "option.group.category", $arrCats);


if (is_array($arrCats) && !empty($arrCats)) {
    foreach ($arrCats as $k => $arr) {
        $strSqlQuery = "SELECT * FROM `job_categories` WHERE `jc_parent` = ".$arr['jc_id']." ORDER BY `jc_order`";
        $arrSubCats = $objDb->fetchall($strSqlQuery);

        if (is_array($arrSubCats) && !empty($arrSubCats)) {
            foreach ($arrSubCats as $kk => $varr) {
                $arrSubCats[$kk]['intOptionValue'] = intval($varr['jc_id']);
                $arrSubCats[$kk]['strOptionTitle'] = htmlspecialchars($varr['jc_title']);
            }
        }

        $objTpl->tpl_loop("page.content", "group.options.".$arr['jc_id'], $arrSubCats);
    }
}

// Тип занятости
$strSqlQuery = "SELECT * FROM `job_workbusy` ORDER BY jw_id";
$arrWorkbusyTypes = $objDb->fetchall($strSqlQuery);
if (is_array($arrWorkbusyTypes) && !empty($arrWorkbusyTypes)) {
    foreach ($arrWorkbusyTypes as $k => $val) {
        $arrWorkbusyTypes[$k]['checked'] = (isset($sbxWorkbusy) && $val['jw_id']==$sbxWorkbusy ? ' selected' : '');
    }
}
$objTpl->tpl_loop("page.content", "workbusy.types", $arrWorkbusyTypes);

// Образование
$strSqlQuery = "SELECT * FROM `job_education_types` ORDER BY jet_id";
$arrEduTypes = $objDb->fetchall($strSqlQuery);
if (is_array($arrEduTypes) && !empty($arrEduTypes)) {
    foreach ($arrEduTypes as $k => $val) {
        $arrEduTypes[$k]['checked'] = (isset($rbEdu) && $val['jet_id']==$rbEdu ? ' checked' : '');
    }
}
$objTpl->tpl_loop("page.content", "education.types", $arrEduTypes);

$objTpl->tpl_array("page.content", $arrTplVars);
