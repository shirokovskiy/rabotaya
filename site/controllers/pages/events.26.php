<?php
/** Created by phpWebStudio(c) 2013 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Событие [events] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.content", "events.26.tpl");

include_once "models/Events.php";
$objEvents = new Events($_db_config);

/**
 * Если указана конкретная новость - покажем её
 */
if (isset($arrReqUri[1]) && !empty($arrReqUri[1])) {
    if(isset($arrReqUri[2]) && !empty($arrReqUri[2])) {
        $nDate = $arrReqUri[1];
        $nUrl = trim($arrReqUri[2]);
        $strSqlQuery = "SELECT * FROM site_events WHERE se_status='Y' AND se_url='".$nUrl."' AND se_date_publ='".$nDate."'";
    } else {
        $intId = intval($arrReqUri[1]);
        $strSqlQuery = "SELECT * FROM site_events WHERE se_status='Y' AND se_id=".$intId;
        $flagUpdate = true;
    }
    $arrEventsInfo = $objDb->fetch( $strSqlQuery );
    // Проверим, существует ли такая новость, и если да, то обработаем для вывода
    if (!empty($arrEventsInfo)) {
        if(!$flagUpdate) {
            $intId = intval($arrEventsInfo['se_id']);
        }
        $arrEventsInfo['strDatePublEvents'] = $objUtils->workDate(2, $arrEventsInfo['se_date_publ']);
        $arrEventsInfo['strEventsBody'] = nl2br($arrEventsInfo['se_body']);
        $arrEventsImage = $objEvents->getEventImage($arrEventsInfo['se_id']);
        $arrEventsInfo['strImageTitle'] = htmlspecialchars($arrEventsImage['si_title']);
        $arrIf['picture'] = ( file_exists(DROOT."storage/files/images/events/events.photo.".$arrEventsInfo['se_id'].".jpg") && $arrEventsImage['si_id']);
        $objTpl->tpl_array("page.content", $arrEventsInfo);

        $arrTplVars['m_title'] = htmlspecialchars(strip_tags($arrEventsInfo['se_title'])).' | '.$arrTplVars['m_title'];
        $arrTplVars['m_description'] = $objUtils->substrText(htmlspecialchars(strip_tags($arrEventsInfo['se_body'])));

        $arrTplVars['strUniqueMainPhoto'] = $arrIf['picture'] ? '?pic='.time() : '';

        // Показать картинки, если они есть
//        if ($arrEventsInfo['se_image']=='Y') {
            # Дополнительные картинки есть только в том случае, если есть оснавная
//            $strSqlQuery = "SELECT si_id,si_desc FROM site_images WHERE si_status='Y' AND si_rel_id = ".$intId." AND si_type = 'events'";
//            $arrMoreImages = $objDb->fetchall( $strSqlQuery );
//            $arrIf['pic_group'] = is_array($arrMoreImages) && !empty($arrMoreImages);
//        }

        // Check attachments
//        $eventsAttachedFiles = PRJ_FILES.'events_attachments/'.$intId.'/';
//        if (is_dir($eventsAttachedFiles)) {
//            $exclude_list = array(".", "..");
//            $arrAttachedFiles = array_diff(scandir($eventsAttachedFiles), $exclude_list);
//            if (is_array($arrAttachedFiles)) {
//                $arrIf['is.attached.files'] = !empty($arrAttachedFiles);
//                foreach ($arrAttachedFiles as $k => $v) {
//                    $arrAttachedFilesList[$k]['name'] = $v;
//                    $arrAttachedFilesList[$k]['at.url'] = $arrTplVars['cfgFilesUrl'].'events_attachments/'.$intId.'/'.$v;
//                }
//            }
//        }

        /**
         * Обновим URL новости, если её нет
         */
        if ($flagUpdate && empty($arrEventsInfo['se_url']) && $intId > 0){
            $strUrl = strtolower( $objUtils->translitBadChars($objUtils->translitKyrToLat(trim($arrEventsInfo['se_title']))) );
            $strSqlQuery = "UPDATE `site_events` SET `se_url` = '$strUrl' WHERE `se_id` = $intId";
            if (!$objDb->query($strSqlQuery)) {
                # sql error
            }
        }
    } else {
        header('Location: '.SITE_URL.'error');
        exit();
    }

    $objTpl->tpl_loop( "page.content", "list", $arrMoreImages );
    $objTpl->tpl_loop( "page.content", "attachments", $arrAttachedFilesList );
    $arrIf['event'] = true;

} else {
    /**
     * Список событий
     */

    // Всего записей в базе
    $strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM `site_events` WHERE se_status = 'Y' AND se_date_publ <= DATE_FORMAT(NOW(), '%Y-%m-%d')";
    $arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');
    // Выбрано записей
    $strSqlQuery = "SELECT COUNT(*) AS intQuantitySelectRecords FROM `site_events` WHERE se_status = 'Y' AND se_date_publ <= DATE_FORMAT(NOW(), '%Y-%m-%d')";
    $arrTplVars['intQuantitySelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantitySelectRecords');
    // ***** BEGIN: Построение пейджинга для вывода списка
    $objPagination = new tplPaginator($arrTplVars['intQuantitySelectRecords'], SITE_TPL_TPL_DIR);
    /**
     * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
     * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
     */
    $objPagination->strPaginatorTpl = "site.global.4.tpl";
    $objPagination->intQuantRecordPage = 15;
    $objPagination->strColorLinkStyle = "link-b";
    $objPagination->strColorActiveStyle = "tab-bl-b";
    $objPagination->strNameImageBack = "arrow_one_left.gif";
    $objPagination->strNameImageForward = "arrow_one_right.gif";
    // Создаем блок пэйджинга
    $objPagination->paCreate();
    // ***** END: Построение пейджинга для вывода списка
    // Запрос для выборки нужных записей
    $strSqlQuery = "SELECT * FROM `site_events`"
        ." WHERE se_status = 'Y' AND se_date_publ <= DATE_FORMAT(NOW(), '%Y-%m-%d')"
        ." ORDER BY se_date_publ DESC, se_id DESC ".$objPagination->strSqlLimit;
    $arrLastEvents = $objDb->fetchall( $strSqlQuery );
    // кол-во публикаций показанных на странице
    $arrTplVars['intQuantityShowRecOnPage'] = ( !empty($arrLastEvents) ? count($arrLastEvents) : 0 );

    if ($arrTplVars['intQuantityShowRecOnPage'] >= $objPagination->intQuantRecordPage) {
        // Присвоение значения пэйджинга для списка
        $arrTplVars['blockPaginator'] = $objPagination->paShow();
    }

    if ( is_array($arrLastEvents) ) {
        foreach ( $arrLastEvents as $key => $value ) {
            $arrLastEvents[$key]['strDatePublicEvents'] = date('d.m.y', strtotime($value['se_date_publ']));
            $arrLastEvents[$key]['urlParam'] = (!empty($value['se_url']) ? ((!empty($value['se_date_publ']) ? $value['se_date_publ'].'/' : '').$value['se_url']) : $value['se_id'] );
        }
    }

    $objTpl->tpl_loop("page.content", "last.events", $arrLastEvents);
    $arrIf['events.list'] = true;
}
$objTpl->tpl_if("page.content", $arrIf);
$objTpl->tpl_array("page.content", $arrTplVars);
