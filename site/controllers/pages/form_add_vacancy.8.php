<?php
/** Created by phpWebStudio(c) 2013 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Создать вакансию [form_add_vacancy] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.content", "form_add_vacancy.8.tpl");

include_once "models/Company.php";
$objCompany = new Company($_db_config);

include_once "models/Vacancy.php";
$objVacancy = new Vacancy($_db_config);

$intStatusError=0;

if(isset($_POST) && $_POST['add'] == 'vacancy') {
    $arrSqlData['strTitle'] = mysql_real_escape_string(trim($_POST['strTitle']));
    $arrTplVars['strTitle'] = htmlspecialchars(stripslashes(trim($_POST['strTitle'])));

    if (empty($arrSqlData['strTitle']) ) {
        $intStatusError=1;
        $arrCodeError[] = 'Вы не заполнили поле Должность';
    }

    $arrSqlData['strSalaryFrom'] = mysql_real_escape_string(trim($_POST['strSalaryFrom']));
    $arrTplVars['strSalaryFrom'] = htmlspecialchars(stripslashes(trim($_POST['strSalaryFrom'])));

    $arrSqlData['strSalaryTo'] = mysql_real_escape_string(trim($_POST['strSalaryTo']));
    $arrTplVars['strSalaryTo'] = htmlspecialchars(stripslashes(trim($_POST['strSalaryTo'])));

    $arrSqlData['strCity'] = mysql_real_escape_string(trim($_POST['strCity']));
    $arrTplVars['strCity'] = htmlspecialchars(stripslashes(trim($_POST['strCity'])));

    if (empty($arrSqlData['strCity']) ) {
        $intStatusError=1;
        $arrCodeError[] = 'Вы не заполнили поле Город';
    }

    $rbSex = $_POST['rbSex']?:'N';
    $arrTplVars['jv_sex_'.$rbSex] = " checked";

    $rbEdu = $_POST['rbEdu']?:'N';
    $sbxWorkbusy = $_POST['sbxWorkbusy'];

    $rbAgency = $_POST['rbAgency']?:'N';
    $arrTplVars['rbAgency_'.$rbAgency] = ' checked';

    $cbxDriver = $_POST['cbxDriver']?:'N';
    $arrTplVars['cbxDriver'.$cbxDriver] = ' checked';

    $cbxTrips = $_POST['cbxTrips']?:'N';
    $arrTplVars['cbxTrips'.$cbxTrips] = ' checked';

    $arrSqlData['strDescription'] = mysql_real_escape_string(trim($_POST['strDescription']));
    $arrTplVars['strDescription'] = htmlspecialchars(stripslashes(trim($_POST['strDescription'])));

    $arrSqlData['strContactPerson'] = mysql_real_escape_string(trim($_POST['strContactPerson']));
    $arrTplVars['strContactPerson'] = htmlspecialchars(stripslashes(trim($_POST['strContactPerson'])));

    $arrSqlData['strPhoneCode'] = mysql_real_escape_string(trim($_POST['strPhoneCode']));
    $arrTplVars['strPhoneCode'] = htmlspecialchars(stripslashes(trim($_POST['strPhoneCode'])));

    $arrSqlData['strPhone'] = mysql_real_escape_string(trim($_POST['strPhone']));
    $arrTplVars['strPhone'] = htmlspecialchars(stripslashes(trim($_POST['strPhone'])));

    if (empty($arrSqlData['strPhone']) ) {
        $intStatusError=1;
        $arrCodeError[] = 'Вы не заполнили Телефон';
    }

    $arrSqlData['strEmail'] = mysql_real_escape_string(trim($_POST['strEmail']));
    $arrTplVars['strEmail'] = htmlspecialchars(stripslashes(trim($_POST['strEmail'])));

    /*if (empty($arrSqlData['strEmail']) ) {
        $intStatusError=1;
        $arrCodeError[] = 'Вы не заполнили поле Email';
    }*/

    $arrSqlData['strCompany'] = mysql_real_escape_string(trim($_POST['strCompany']));
    $arrTplVars['strCompany'] = htmlspecialchars(stripslashes(trim($_POST['strCompany'])));

    if (empty($arrSqlData['strCompany']) ) {
        $intStatusError=1;
        $arrCodeError[] = 'Вы не заполнили Наименование Компании';
    }

    $arrSqlData['strCompanyCity'] = mysql_real_escape_string(trim($_POST['strCompanyCity']));
    $arrTplVars['strCompanyCity'] = htmlspecialchars(stripslashes(trim($_POST['strCompanyCity'])));

    $arrSqlData['strAddress'] = mysql_real_escape_string(trim($_POST['strAddress']));
    $arrTplVars['strAddress'] = htmlspecialchars(stripslashes(trim($_POST['strAddress'])));

    $arrSqlData['strWeb'] = mysql_real_escape_string(trim($_POST['strWeb']));
    $arrTplVars['strWeb'] = htmlspecialchars(stripslashes(trim($_POST['strWeb'])));

    $arrSqlData['strCompanyEmail'] = mysql_real_escape_string(trim($_POST['strCompanyEmail']));
    $arrTplVars['strCompanyEmail'] = htmlspecialchars(stripslashes(trim($_POST['strCompanyEmail'])));

    $arrSqlData['strCompPhoneCode'] = mysql_real_escape_string(trim($_POST['strCompPhoneCode']));
    $arrTplVars['strCompPhoneCode'] = htmlspecialchars(stripslashes(trim($_POST['strCompPhoneCode'])));

    $arrSqlData['strCompanyPhone'] = mysql_real_escape_string(trim($_POST['strCompanyPhone']));
    $arrTplVars['strCompanyPhone'] = htmlspecialchars(stripslashes(trim($_POST['strCompanyPhone'])));

    $arrSqlData['strCompanyDescription'] = mysql_real_escape_string(trim($_POST['strCompanyDescription']));
    $arrTplVars['strCompanyDescription'] = htmlspecialchars(stripslashes(trim($_POST['strCompanyDescription'])));

    if ($intStatusError!=1) {
        $strSqlFields = ""
            ." jv_title = '{$arrSqlData['strTitle']}'"
            .", jv_salary_from = '{$arrSqlData['strSalaryFrom']}'"
            .", jv_salary_to = '{$arrSqlData['strSalaryTo']}'"
            .", jv_driver = '$cbxDriver'"
            .", jv_trips = '$cbxTrips'"
            .", jv_city = '{$arrSqlData['strCity']}'"
            .", jv_description = '{$arrSqlData['strDescription']}'"
            .", jv_contact_fio = '{$arrSqlData['strContactPerson']}'"
            .", jv_phone = '".(!empty($arrSqlData['strPhoneCode'])?'+7'.$arrSqlData['strPhoneCode']:'').$arrSqlData['strPhone']."'"
            . (!empty($arrSqlData['strEmail'])?", jv_email = '{$arrSqlData['strEmail']}'":'')
            .", jv_jet_id = '$rbEdu'"
            .", jv_jw_id = '$sbxWorkbusy'"
            .", jv_status = 'Y'"
        ;
        $strSqlQuery = "INSERT INTO job_vacancies SET".$strSqlFields
            .", jv_date_publish = UNIX_TIMESTAMP()";
        if ( !$objDb->query( $strSqlQuery ) ) {
            $intStatusError=1;
            $arrCodeError[] = 'Ошибка базы данных';
        } else {
            $intRecordId = $objDb->insert_id();
            if ( $intRecordId > 0 ) {
                // Проверим, нет ли такой компании уже
                $arrCompany = $objCompany->getCompanyByName($arrSqlData['strCompany']);

                if (is_array($arrCompany) && !empty($arrCompany)) {
                    $intCompanyId = (int)$arrCompany['jc_id'];
                } else {
                    // Добавляем компанию
                    $strSqlFields = " jc_title = '".$arrSqlData['strCompany']."'"
                        .", jc_type = '$rbAgency'"
                        .", jc_city = '{$arrSqlData['strCompanyCity']}'"
                        .", jc_address = '{$arrSqlData['strAddress']}'"
                        .", jc_web = '{$arrSqlData['strWeb']}'"
                        .", jc_email = '{$arrSqlData['strCompanyEmail']}'"
                        .", jc_phone = '".(!empty($arrSqlData['strCompPhoneCode'])?'+7'.$arrSqlData['strCompPhoneCode']:'').$arrSqlData['strCompanyPhone']."'"
                        .", jc_description = '{$arrSqlData['strCompanyDescription']}'"
                    ;
                    $strSqlQuery = "INSERT INTO job_companies SET $strSqlFields, jc_date_create = UNIX_TIMESTAMP()";
                    if ( !$objDb->query( $strSqlQuery ) ) {
                        $intStatusError=1;
                        $arrCodeError[] = 'Ошибка базы данных! Возможно такая Компания уже существует в нашей базе данных. Если Вы представитель этой компании, Войдите в Личный кабинет и добавьте вакансию.';
                    } else {
                        $intCompanyId = $objDb->insert_id();
                    }
                }

                if ($intCompanyId > 0) {
                    $objVacancy->setId($intRecordId)->setCompanyId($intCompanyId);
                    if (is_object($objUser) && $objUser->isAuthed())
                        $objVacancy->setUserID($objUser->getId());
                }

                if (!$intStatusError) {
                    $_SESSION['status'] = 'ok';
                    $_SESSION['position'] = $arrTplVars['strTitle'];
                    header('Location: '.SITE_URL.'form_add_vacancy');
                    exit();
                }
            } else {
                $intStatusError=1;
                $arrCodeError[] = 'Что-то пошло не так! Данные не записаны =(';
            }
        }
    }

    if($intStatusError==1) {
        $arrTplVars['strMessage'] = implode('<br>', $arrCodeError);
        $arrIf['is.msg'] = true;
    }
}

$arrTplVars['msgStatus'] = intval($intStatusError);

if ($_SESSION['status']=='ok') {
    $arrTplVars['strMessage'] = nl2br("Поздравляем!!!\nВаша вакансия\n<b>".$_SESSION['position']."</b>\nуспешно добавлена!");
    $arrIf['is.msg'] = true;
    unset($_SESSION['status']);
    unset($_SESSION['position']);
}

// Тип занятости
$strSqlQuery = "SELECT * FROM `job_workbusy` ORDER BY jw_id";
$arrWorkbusyTypes = $objDb->fetchall($strSqlQuery);
if (is_array($arrWorkbusyTypes) && !empty($arrWorkbusyTypes)) {
    foreach ($arrWorkbusyTypes as $k => $val) {
        $arrWorkbusyTypes[$k]['checked'] = (isset($sbxWorkbusy) && $val['jw_id']==$sbxWorkbusy ? ' selected' : '');
    }
}
$objTpl->tpl_loop("page.content", "workbusy.types", $arrWorkbusyTypes);

// Образование
$strSqlQuery = "SELECT * FROM `job_education_types` ORDER BY jet_id";
$arrEduTypes = $objDb->fetchall($strSqlQuery);
if (is_array($arrEduTypes) && !empty($arrEduTypes)) {
    foreach ($arrEduTypes as $k => $val) {
        $arrEduTypes[$k]['checked'] = (isset($rbEdu) && $val['jet_id']==$rbEdu ? ' checked' : '');
    }
}
$objTpl->tpl_loop("page.content", "education.types", $arrEduTypes);

// Города
$strSqlQuery = "SELECT COUNT(`jv_city_id`) AS `citiesMax`, `city` AS `strCity` FROM `job_vacancies` LEFT JOIN `site_cities` ON (`jv_city_id` = `id`) WHERE `jv_city_id` > 0 AND `country_id` > 0 AND `city` NOT LIKE '%область%' AND `city` NOT LIKE '%край%' AND `city` NOT REGEXP ',' AND `city` NOT REGEXP '[.]' GROUP BY `jv_city_id` ORDER BY `citiesMax` DESC LIMIT 50";
$arrCities = $objDb->fetchcol($strSqlQuery, 1);
if (is_array($arrCities) && !empty($arrCities)) {
    foreach ($arrCities as &$city) {
        $city = addslashes($city);
    }
}
$arrTplVars['strCities'] = "'".implode("','", $arrCities)."'";// Санкт-Петербург','Москва','Лабытнанги','Салехард'";

if (is_object($objUser) && $objUser->isAuthed()){
    $arrUserData = $objUser->getData();
    $arrTplVars['strContactPerson'] = $arrUserData['su_lname'].' '.$arrUserData['su_fname'].' '.$arrUserData['su_mname'];
    $arrTplVars['strEmail'] = $arrUserData['email'];
}

$objTpl->tpl_if("page.content", $arrIf);
$objTpl->tpl_array("page.content", $arrTplVars);
