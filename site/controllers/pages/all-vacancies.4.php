<?php
/** Created by phpWebStudio(c) 2013 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Все вакансии [all-vacancies] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.content", "all-vacancies.4.tpl");

$objVacancy = new Vacancy();

/**
 * Список вакансий
 */

// Всего записей в базе
$strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM `job_vacancies` WHERE jv_status = 'Y'";
$arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');

// Выбрано записей
$strSqlQuery = "SELECT COUNT(*) AS intQuantitySelectRecords FROM `job_vacancies` WHERE jv_status = 'Y'";
$arrTplVars['intQuantitySelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantitySelectRecords');
// ***** BEGIN: Построение пейджинга для вывода списка
$objPagination = new tplPaginator($arrTplVars['intQuantitySelectRecords'], SITE_TPL_TPL_DIR, 'p');

/**
 * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
 * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
 */
$objPagination->strPaginatorTpl = "site.global.4.tpl";
$objPagination->intQuantRecordPage = 25;
$objPagination->strColorActivePage = '8A0202';
$objPagination->strColorLinkStyle = "link-b";
$objPagination->strColorActiveStyle = "tab-bl-b";
$objPagination->strNameImageBack = "arrow_one_left.gif";
$objPagination->strNameImageForward = "arrow_one_right.gif";
// Создаем блок пэйджинга
$objPagination->paCreate();
// ***** END: Построение пейджинга для вывода списка

// Запрос для выборки нужных записей
$strSqlQuery = "SELECT jv_id,jv_title,jv_date_publish,jv_salary_from,jv_salary_to,jv_salary,jv_description FROM `job_vacancies`"
    ." WHERE `jv_status` = 'Y'"
    ." ORDER BY `jv_date_publish` DESC, `jv_id` DESC ".$objPagination->strSqlLimit;
$arrVacancies = $objDb->fetchall( $strSqlQuery );

// кол-во публикаций показанных на странице
$arrTplVars['intQuantityShowRecOnPage'] = ( !empty($arrVacancies) ? count($arrVacancies) : 0 );
// Присвоение значения пэйджинга для списка
$arrTplVars['blockPaginator'] = $objPagination->paShow();

if ( is_array($arrVacancies) ) {
    foreach ( $arrVacancies as $key => $value ) {
        $arrVacancies[$key]['strTitle'] = mb_ucfirst($value['jv_title']);
        $arrVacancies[$key]['strDatePublish'] = date('d.m.y', $value['jv_date_publish']);
        $arrVacancies[$key]['strUniquePhoto'] = $arrIf['news.picture.'.$value['jv_id']] ? '?pic='.time() : '';
        $arrVacancies[$key]['strSalary'] = (!empty($value['jv_salary_from']) ? 'от '.$value['jv_salary_from'] : '').(!empty($value['jv_salary_to']) ? (!empty($value['jv_salary_from'])?' до ':'').$value['jv_salary_to'] : '');
        if (!empty($arrVacancies[$key]['strSalary'])) {
            $arrVacancies[$key]['strSalary'] .= ' руб';
        } elseif (!empty($value['jv_salary'])) {
            $arrVacancies[$key]['strSalary'] = $value['jv_salary'];
        }

        $arrVacancies[$key]['strDescription'] = $objUtils->substrText(strip_tags($value['jv_description']), 200, true);
        $arrIf['is.strDescription.'.$value['jv_id']] = !empty($arrVacancies[$key]['strDescription']);
        $arrVacancies[$key]['strSeoUrl'] = strtolower( $objUtils->translitBadChars($objUtils->translitKyrToLat(trim($value['jv_title'])), array(" ", '.')))."_".$value['jv_id'];
        $arrVacancies[$key]['strSeoUrl'] = str_replace('__', '_', $arrVacancies[$key]['strSeoUrl']);
        $objVacancy->setId($value['jv_id'])->saveSeoUrl($arrVacancies[$key]['strSeoUrl']);
    }
}

$objTpl->tpl_loop("page.content", "list.vacancies", $arrVacancies);
$objTpl->tpl_array("page.content", $arrTplVars);
$objTpl->tpl_if("page.content", $arrIf);

