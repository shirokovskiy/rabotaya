<?php
/** Created by phpWebStudio(c) 2014 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Добавить тренинг [add.training] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.content", "add.training.54.tpl");

$intStatusError=0;

if(isset($_POST) && $_POST['add'] == 'training') {
    // CAPTCHA >>>
    if ( strtoupper($_SESSION['captchaTrain']) != strtoupper($_POST['strCodeString']) || empty($_POST['strCodeString']) ) {
        $intStatusError=1;
        $arrCodeError[] = 'Визуальный код введён неверно';
        // *** CAPTCHA
    } else {
        $arrSqlData['strTitle'] = mysql_real_escape_string(trim($_POST['strTitle']));
        $arrTplVars['strTitle'] = htmlspecialchars(stripslashes(trim($_POST['strTitle'])));

        if (empty($arrSqlData['strTitle']) ) {
            $intStatusError=1;
            $arrCodeError[] = 'Вы не заполнили поле Название тренинга';
        }

//    $arrSqlData['strCity'] = mysql_real_escape_string(trim($_POST['strCity']));
//    $arrTplVars['strCity'] = htmlspecialchars(stripslashes(trim($_POST['strCity'])));

//    if (empty($arrSqlData['strCity']) ) {
//        $intStatusError=1;
//        $arrCodeError[] = 'Вы не заполнили поле Город';
//    }

        $arrSqlData['strDescription'] = mysql_real_escape_string(trim($_POST['strDescription']));
        $arrTplVars['strDescription'] = htmlspecialchars(stripslashes(trim($_POST['strDescription'])));

        $arrSqlData['strPhone'] = mysql_real_escape_string(trim($_POST['strPhone']));
        $arrTplVars['strPhone'] = htmlspecialchars(stripslashes(trim($_POST['strPhone'])));

        $arrSqlData['strEmail'] = mysql_real_escape_string(trim($_POST['strEmail']));
        $arrTplVars['strEmail'] = htmlspecialchars(stripslashes(trim($_POST['strEmail'])));

        if (empty($arrSqlData['strEmail']) && empty($arrSqlData['strPhone'])) {
            $intStatusError=1;
            $arrCodeError[] = 'Вы не заполнили поле Email или Телефон (заполните хотя бы одно)';
        }

//    $arrSqlData['strCompany'] = mysql_real_escape_string(trim($_POST['strCompany']));
//    $arrTplVars['strCompany'] = htmlspecialchars(stripslashes(trim($_POST['strCompany'])));

//    if (empty($arrSqlData['strCompany']) ) {
//        $intStatusError=1;
//        $arrCodeError[] = 'Вы не заполнили Наименование Компании';
//    }


        $arrSqlData['strAddress'] = mysql_real_escape_string(trim($_POST['strAddress']));
        $arrTplVars['strAddress'] = htmlspecialchars(stripslashes(trim($_POST['strAddress'])));

        $arrSqlData['strWeb'] = mysql_real_escape_string(trim($_POST['strWeb']));
        $arrTplVars['strWeb'] = htmlspecialchars(stripslashes(trim($_POST['strWeb'])));
    }

    if ($intStatusError!=1) {
        $strSqlFields = ""
            ." str_title = '{$arrSqlData['strTitle']}'"
//            .", str_city = '{$arrSqlData['strCity']}'"
            .", str_desc = '{$arrSqlData['strDescription']}'"
            . (!empty($arrSqlData['strAddress'])?", str_address = '{$arrSqlData['strAddress']}'":'')
            . (!empty($arrSqlData['strWeb'])?", str_web = '{$arrSqlData['strWeb']}'":'')
            . (!empty($arrSqlData['strPhone'])?", str_phone = '{$arrSqlData['strPhone']}'":'')
            . (!empty($arrSqlData['strEmail'])?", str_email = '{$arrSqlData['strEmail']}'":'')
            .", str_status = 'Y'"
            .", str_alias = UNIX_TIMESTAMP()"
        ;
        $strSqlQuery = "INSERT INTO site_trainings SET".$strSqlFields
            .", str_date_create = UNIX_TIMESTAMP()";
        if ( !$objDb->query( $strSqlQuery ) ) {
            $intStatusError=1;
            $arrCodeError[] = 'Ошибка базы данных';
        } else {
            $intRecordId = $objDb->insert_id();
            if ( $intRecordId > 0 ) {
                if (!$intStatusError) {
                    $_SESSION['status'] = 'ok';
                    $_SESSION['position'] = $arrTplVars['strTitle'];
                    header('Location: '.SITE_URL.'add.training');
                    exit();
                }
            } else {
                $intStatusError=1;
                $arrCodeError[] = 'Что-то пошло не так! Данные не записаны!';
            }
        }
    }

    if($intStatusError==1) {
        $arrTplVars['strMessage'] = implode('<br>', $arrCodeError);
        $arrIf['is.msg'] = true;
    }
}

$arrTplVars['msgStatus'] = intval($intStatusError);

if ($_SESSION['status']=='ok') {
    $arrTplVars['strMessage'] = nl2br("Поздравляем!!!\nВаш тренинг\n<b>".$_SESSION['position']."</b>\nуспешно добавлен!");
    $arrIf['is.msg'] = true;
    unset($_SESSION['status']);
    unset($_SESSION['position']);
}

$objTpl->tpl_if("page.content", $arrIf);
$objTpl->tpl_array("page.content", $arrTplVars);
