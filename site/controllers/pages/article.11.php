<?php
/** Created by phpWebStudio(c) 2013 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Статья [article] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.content", "article.11.tpl");

if (isset($arrReqUri[1]) && !empty($arrReqUri[1])) {
    if(isset($arrReqUri[2]) && !empty($arrReqUri[2])) {
        $nDate = $arrReqUri[1];
        $nUrl = trim($arrReqUri[2]);
        $strSqlQuery = "SELECT * FROM site_articles WHERE sa_status='Y' AND sa_url='".$nUrl."' AND sa_date_publish=".$nDate;
    } else {
        $intId = intval($arrReqUri[1]);
        $strSqlQuery = "SELECT * FROM site_articles WHERE sa_status='Y' AND sa_id=".$intId;
        $flagUpdate = true;
    }
    $arrArticlesInfo = $objDb->fetch( $strSqlQuery );
    // Проверим, существует ли такая запись, и если да, то обработаем для вывода
    if (!empty($arrArticlesInfo)) {
        if(!$flagUpdate) {
            $intId = intval($arrArticlesInfo['sa_id']);
        }
        $arrArticlesInfo['strDatePublishArticle'] = $objUtils->workDate(2, date('Y-m-d',$arrArticlesInfo['sa_date_publish']));
        $arrArticlesInfo['strArticleBody'] = nl2br($arrArticlesInfo['sa_content']);
        $arrIf['picture'] = ( file_exists(PRJ_IMAGES."articles/articles.photo.".$arrArticlesInfo['sa_id'].".jpg") && $arrArticlesInfo['sa_image'] == 'Y');
        $objTpl->tpl_array("page.content", $arrArticlesInfo);

        $arrTplVars['m_title'] = htmlspecialchars(strip_tags($arrArticlesInfo['sa_title'])).' | '.$arrTplVars['m_title'];
        $arrTplVars['m_description'] = $objUtils->substrText(htmlspecialchars(strip_tags($arrArticlesInfo['sa_content'])), 250);

        $arrTplVars['strUniqueMainPhoto'] = $arrIf['picture'] ? '?pic='.time() : '';

        // Показать картинки, если они есть
        if ($arrArticlesInfo['sa_image']=='Y') {
            # Дополнительные картинки есть только в том случае, если есть оснавная
            $strSqlQuery = "SELECT si_id,si_desc FROM site_images WHERE si_status='Y' AND si_rel_id = ".$intId." AND si_type = 'article'";
            $arrMoreImages = $objDb->fetchall( $strSqlQuery );
            $arrIf['pic_group'] = is_array($arrMoreImages) && !empty($arrMoreImages);
        }
    } else {
        header('Location: '.SITE_URL.'error');
        exit();
    }
}

$objTpl->tpl_loop( "page.content", "list", $arrMoreImages );
$objTpl->tpl_if("page.content", $arrIf);
$objTpl->tpl_array("page.content", $arrTplVars);
