<?php
/** Created by phpWebStudio(c) 2013 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Создать резюме [form_add_resume] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.content", "form_add_resume.7.tpl");

$intStatusError=0;
$arrCodeError[] = 'Возникла ошибка:';

if (!isset($objSiteUser)) {
    $objSiteUser = new SiteUser();
}

if(isset($_POST) && $_POST['add'] == 'resume') {
    $intResumeId = intval($_POST['resumeID']);

    $arrSqlData['strTitle'] = mysql_real_escape_string(trim($_POST['strTitle']));
    $arrTplVars['strTitle'] = htmlspecialchars(stripslashes(trim($_POST['strTitle'])));

    if (empty($arrSqlData['strTitle']) ) {
        $intStatusError=1;
        $arrCodeError[] = 'Вы не заполнили поле Претендую на должность';
    }

    $arrSqlData['strFio'] = mysql_real_escape_string(trim($_POST['strFio']));
    $arrTplVars['strFio'] = htmlspecialchars(stripslashes(trim($_POST['strFio'])));

    if (empty($arrSqlData['strFio']) ) {
        $intStatusError=1;
        $arrCodeError[] = 'Вы не заполнили поле Имя';
    }

    $arrSqlData['strBirthday'] = mysql_real_escape_string(trim($_POST['strBirthday']));
    $arrTplVars['strBirthday'] = htmlspecialchars(stripslashes(trim($_POST['strBirthday'])));

    $cbxAgeOnly = ($_POST['cbxAgeOnly'] == 'Y' ? 'Y' : 'N');
    $arrTplVars['cbxAgeOnly'] = ($cbxAgeOnly == 'Y' ? ' checked' : '');

    if ($cbxAgeOnly == 'Y') {
        // количество лет между датами
        $a = new DateTime($arrSqlData['strBirthday']);
        $b = new DateTime(date('Y-m-d'));
        $age = $a->diff($b)->format('%y');
    }

    $cbxMarriage = ($_POST['cbxMarriage'] == 'Y' ? 'Y' : 'N');
    $arrTplVars['cbxMarriage'] = ($cbxMarriage == 'Y' ? ' checked' : '');

    $arrSqlData['strSalary'] = intval(trim($_POST['strSalary']));
    $arrTplVars['strSalary'] = htmlspecialchars(stripslashes(trim($_POST['strSalary'])));

    if (intval($arrSqlData['strSalary']) <= 100) {
        $intStatusError=1;
        $arrCodeError[] = 'Проверьте правильно ли Вы ввели ожидаемую Зряплату';
    }

    $arrSqlData['strCity'] = mysql_real_escape_string(trim($_POST['strCity']));
    $arrTplVars['strCity'] = htmlspecialchars(stripslashes(trim($_POST['strCity'])));

    if (empty($arrSqlData['strCity']) ) {
        $intStatusError=1;
        $arrCodeError[] = 'Вы не заполнили поле Город';
    } else {
        $cityId = $objSiteUser->getCityId($arrSqlData['strCity']);
    }

    $rbSex = $_POST['rbSex'];
    $arrTplVars['rb_'.$rbSex] = " checked";

    if(empty($rbSex)) {
        $intStatusError=1;
        $arrCodeError[] = 'Вы не указали пол';
    }

    $rbChildren = $_POST['rbChildren'];
    $arrTplVars['rbChildren_'.$rbChildren] = ' checked';

    $cbxTrips = $_POST['rbTrips'];
    $arrTplVars['rbTrips_'.$cbxTrips] = " checked";

    $arrSqlData['strAbout'] = mysql_real_escape_string(trim($_POST['strAbout']));
    $arrTplVars['strAbout'] = htmlspecialchars(stripslashes(trim($_POST['strAbout'])));

    $arrSqlData['strExperienceDescription'] = mysql_real_escape_string(trim($_POST['strExperienceDescription']));
    $arrTplVars['strExperienceDescription'] = htmlspecialchars(stripslashes(trim($_POST['strExperienceDescription'])));

    $arrSqlData['strPhone'] = mysql_real_escape_string(trim($_POST['strPhone']));
    $arrTplVars['strPhone'] = htmlspecialchars(stripslashes(trim($_POST['strPhone'])));

    if (empty($arrSqlData['strPhone']) ) {
        if (is_array($_POST['mobPhone']) && !empty($_POST['mobPhone'])) {
            $arrSqlData['strPhone'] = implode(' ',$_POST['mobPhone']);
            $arrSqlData['strPhone'] = mysql_real_escape_string(trim($arrSqlData['strPhone']));
        } elseif (is_array($_POST['homePhone']) && !empty($_POST['homePhone'])) {
            $arrSqlData['strPhone'] = implode(' ',$_POST['homePhone']);
            $arrSqlData['strPhone'] = mysql_real_escape_string(trim($arrSqlData['strPhone']));
        } elseif (is_array($_POST['workPhone']) && !empty($_POST['workPhone'])) {
            $arrSqlData['strPhone'] = implode(' ',$_POST['workPhone']);
            $arrSqlData['strPhone'] = mysql_real_escape_string(trim($arrSqlData['strPhone']));
        } else {
            $intStatusError=1;
            $arrCodeError[] = 'Вы не заполнили Телефон';
        }

        // todo: надо записать не "один из", а "все"
    }

    $arrSqlData['strEmail'] = mysql_real_escape_string(trim($_POST['strEmail']));
    $arrTplVars['strEmail'] = htmlspecialchars(stripslashes(trim($_POST['strEmail'])));

    if(!empty($arrSqlData['strEmail']) && !$objUtils->checkEmail($arrSqlData['strEmail'])) {
        $intStatusError=1;
        $arrCodeError[] = 'Поле Email содержит не правильного формата адрес';
    }

    $sbxWorkbusy = intval($_POST['sbxWorkbusy']);

    $arrSqlData['strSkype'] = mysql_real_escape_string(trim($_POST['strSkype']));
    $arrTplVars['strSkype'] = htmlspecialchars(stripslashes(trim($_POST['strSkype'])));

    $arrSqlData['strICQ'] = mysql_real_escape_string(trim($_POST['strICQ']));
    $arrTplVars['strICQ'] = htmlspecialchars(stripslashes(trim($_POST['strICQ'])));

    $arrSqlData['strLinkedin'] = mysql_real_escape_string(trim($_POST['strLinkedin']));
    $arrTplVars['strLinkedin'] = htmlspecialchars(stripslashes(trim($_POST['strLinkedin'])));

    $cbxDriver = ($_POST['cbxDriver'] == 'Y' ? 'Y' : 'N');
    $arrTplVars['cbxDriver'] = ($cbxDriver == 'Y' ? ' checked' : '');

    $arrSqlData['strInstitution'] = mysql_real_escape_string(trim($_POST['strInstitution']));
    $arrTplVars['strInstitution'] = htmlspecialchars(stripslashes(trim($_POST['strInstitution'])));

    $arrSqlData['strFaculty'] = mysql_real_escape_string(trim($_POST['strFaculty']));
    $arrTplVars['strFaculty'] = htmlspecialchars(stripslashes(trim($_POST['strFaculty'])));

    $arrSqlData['strSpecialization'] = mysql_real_escape_string(trim($_POST['strSpecialization']));
    $arrTplVars['strSpecialization'] = htmlspecialchars(stripslashes(trim($_POST['strSpecialization'])));

    $arrSqlData['strEduDateFin'] = mysql_real_escape_string(trim($_POST['strEduDateFin']));
    $arrTplVars['strEduDateFin'] = htmlspecialchars(stripslashes(trim($_POST['strEduDateFin'])));

    $arrSqlData['strEduPlus'] = mysql_real_escape_string(trim($_POST['strEduPlus']));
    $arrTplVars['strEduPlus'] = htmlspecialchars(stripslashes(trim($_POST['strEduPlus'])));

    $arrSqlData['strCitizenship'] = mysql_real_escape_string(trim($_POST['strCitizenship']));
    $arrTplVars['strCitizenship'] = htmlspecialchars(stripslashes(trim($_POST['strCitizenship'])));

    if ($intStatusError!=1) {
        $strSqlFields = ""
            ." jr_title = '{$arrSqlData['strTitle']}'"
            .", jr_salary = '{$arrSqlData['strSalary']}'"
            .", jr_fio = '{$arrSqlData['strFio']}'"
            . ($age>0?", jr_age = $age":'')
            . (is_object($a) ? ", jr_birthday = '".$a->format('Y-m-d')."'" : '' )
            .", jr_sex = '".$rbSex."'"
            .", jr_children = '".$rbChildren."'"
            .", jr_marriage = '".$cbxMarriage."'"
            .", jr_city = '".$arrSqlData['strCity']."'"
            .", jr_city_id = ".(intval($cityId)>0?$cityId:'NULL')
            .", jr_driver = '$cbxDriver'"
            .", jr_work_busy = '{$sbxWorkbusy}'"
            .", jr_trips = '{$cbxTrips}'"
            .", jr_phone = '{$arrSqlData['strPhone']}'"
            .", jr_email = '{$arrSqlData['strEmail']}'"
            .", jr_skype = '{$arrSqlData['strSkype']}'"
            .", jr_icq = '{$arrSqlData['strICQ']}'"
            .", jr_linkedin = '{$arrSqlData['strLinkedin']}'"
            .", jr_edu_plus = '{$arrSqlData['strEduPlus']}'"
            .", jr_exp_desc = '{$arrSqlData['strExperienceDescription']}'"
            .", jr_about = '{$arrSqlData['strAbout']}'"
            .", jr_grazhdanstvo = '{$arrSqlData['strCitizenship']}'"
            .", jr_status = '".($objUser->isAuthed()?'Y':'N')."'"
            .", jr_date_publish = UNIX_TIMESTAMP()"
            .($objUser->isAuthed() ? ", jr_su_id = ".$objUser->getId() : '' )
        ;
        if ($intResumeId > 0) {
            $strSqlQuery = "UPDATE job_resumes SET ".$strSqlFields." WHERE jr_id = ".$intResumeId;
        } else {
            $strSqlQuery = "INSERT INTO job_resumes SET ".$strSqlFields;
        }
//        $objDb->setDebugModeOn();
        if ( !$objDb->query( $strSqlQuery ) ) {
            $intStatusError=1;
            $arrCodeError[] = 'Ошибка базы данных: '.$strSqlQuery;
        } else {
            if ($intResumeId > 0) {
                if ($objUser->isAuthed()) {
                    header('Location: /profile/resumes');
                    exit;
                }
            } else {
                $intResumeId = $objDb->insert_id();
                if ( $intResumeId > 0 ) {
                    /**
                     * Work history save
                     */
                    $arrSqlData['strCompany'] = mysql_real_escape_string(trim($_POST['strCompany']));
                    $arrTplVars['strCompany'] = htmlspecialchars(stripslashes(trim($_POST['strCompany'])));

                    $arrSqlData['strPosition'] = mysql_real_escape_string(trim($_POST['strPosition']));
                    $arrTplVars['strPosition'] = htmlspecialchars(stripslashes(trim($_POST['strPosition'])));

                    $arrSqlData['strDateFrom'] = mysql_real_escape_string(trim($_POST['strDateFrom']));
                    $arrSqlData['strDateFrom'] = $objUtils->workDate(13, $arrSqlData['strDateFrom']);
                    $arrTplVars['strDateFrom'] = htmlspecialchars(stripslashes(trim($_POST['strDateFrom'])));

                    $arrSqlData['strDateTo'] = mysql_real_escape_string(trim($_POST['strDateTo']));
                    $arrSqlData['strDateTo'] = $objUtils->workDate(13, $arrSqlData['strDateTo']);
                    $arrTplVars['strDateTo'] = htmlspecialchars(stripslashes(trim($_POST['strDateTo'])));

                    $arrSqlData['strRegion'] = mysql_real_escape_string(trim($_POST['strRegion']));
                    $arrTplVars['strRegion'] = htmlspecialchars(stripslashes(trim($_POST['strRegion'])));

                    $arrSqlData['strWeb'] = mysql_real_escape_string(trim($_POST['strWeb']));
                    $arrTplVars['strWeb'] = htmlspecialchars(stripslashes(trim($_POST['strWeb'])));

                    $arrSqlData['strResponsibility'] = mysql_real_escape_string(trim($_POST['strResponsibility']));
                    $arrTplVars['strResponsibility'] = htmlspecialchars(stripslashes(trim($_POST['strResponsibility'])));

                    $arrSqlData['intCategoryId'] = $intCategoryId = intval(trim($_POST['intCategoryId']));
                    $arrTplVars['intCategoryId'] = ($arrSqlData['intCategoryId'] > 0 ? $arrSqlData['intCategoryId'] : '');

                    if (!empty($arrSqlData['strCompany']) && !empty($arrSqlData['strDateFrom']) && !empty($arrSqlData['strPosition'])) {
                        $strSqlFields = " jrw_jr_id = ".$intResumeId
                            .", jrw_jc_id = ".$arrSqlData['intCategoryId']
                            .", jrw_position = '{$arrSqlData['strPosition']}'"
                            .", jrw_company = '{$arrSqlData['strCompany']}'"
                            .", jrw_region = '{$arrSqlData['strRegion']}'"
                            .", jrw_web = '{$arrSqlData['strWeb']}'"
                            . ($objUtils->dateValid($arrSqlData['strDateFrom']) ? ", jrw_date_start = '{$arrSqlData['strDateFrom']}'" : '')
                            . ($objUtils->dateValid($arrSqlData['strDateTo']) ? ", jrw_date_fin = '{$arrSqlData['strDateTo']}'" : '')
                            .", jrw_responsibility = '{$arrSqlData['strResponsibility']}'"
                        ;
                        $strSqlQuery = "INSERT INTO job_resumes_works SET ".$strSqlFields;
                        if ( !$objDb->query( $strSqlQuery ) ) {
                            $intStatusError=1;
                            $arrCodeError[] = 'Ошибка базы данных: '.$strSqlQuery;
                        }
                    }


                    /**
                     * Education save
                     */
                    $rbEdu = intval($_POST['rbEdu']);

                    $arrSqlData['strInstitution'] = mysql_real_escape_string(trim($_POST['strInstitution']));
                    $arrTplVars['strInstitution'] = htmlspecialchars(stripslashes(trim($_POST['strInstitution'])));

                    $arrSqlData['strFaculty'] = mysql_real_escape_string(trim($_POST['strFaculty']));
                    $arrTplVars['strFaculty'] = htmlspecialchars(stripslashes(trim($_POST['strFaculty'])));

                    $arrSqlData['strSpecialization'] = mysql_real_escape_string(trim($_POST['strSpecialization']));
                    $arrTplVars['strSpecialization'] = htmlspecialchars(stripslashes(trim($_POST['strSpecialization'])));

                    $arrSqlData['strEduDateStart'] = mysql_real_escape_string(trim($_POST['strEduDateStart']));
                    $arrTplVars['strEduDateStart'] = htmlspecialchars(stripslashes(trim($_POST['strEduDateStart'])));

                    $arrSqlData['strEduDateFin'] = mysql_real_escape_string(trim($_POST['strEduDateFin'])); // обычная дата, не timestamp
                    $arrTplVars['strEduDateFin'] = htmlspecialchars(stripslashes(trim($_POST['strEduDateFin'])));

                    if (!empty($rbEdu) && !empty($arrSqlData['strInstitution']) && !empty($arrTplVars['strEduDateFin'])) {
                        $strSqlFields = " jre_jr_id = ".$intResumeId
                            .", jre_jet_id = ".$rbEdu
                            .", jre_institution = '{$arrSqlData['strInstitution']}'"
                            .", jre_faculty = '{$arrSqlData['strFaculty']}'"
                            .", jre_spec = '{$arrSqlData['strSpecialization']}'"
                            . ($objUtils->dateValid(date('Y-m-d',strtotime($arrSqlData['strEduDateStart']))) ? ", jre_date_start = '".$objUtils->workDate(13, $arrSqlData['strEduDateStart'])."'" : '')
                            . ($objUtils->dateValid(date('Y-m-d',strtotime($arrSqlData['strEduDateFin']))) ? ", jre_date_fin = '".$objUtils->workDate(13, $arrSqlData['strEduDateFin'])."'" : '')
                        ;
                        $strSqlQuery = "INSERT INTO job_resumes_education SET ".$strSqlFields;
                        if ( !$objDb->query( $strSqlQuery ) ) {
                            $intStatusError=1;
                            $arrCodeError[] = 'Ошибка базы данных: '.$strSqlQuery;
                        }
                    }

                    if (!$intStatusError) {
                        $_SESSION['status'] = 'ok';
                        $_SESSION['position'] = $arrTplVars['strTitle'];
                        header('Location: '.SITE_URL.'form_add_resume');
                        exit();
                    }
                }
            }
        }
    }

    if($intStatusError==1) {
        $arrTplVars['strMessage'] = implode('<br>', $arrCodeError);
        $arrIf['is.msg'] = true;
    }
} else {
    $arrTplVars['strSalary'] = '35000';
}
$arrTplVars['msgStatus'] = intval($intStatusError);

if ($_SESSION['status']=='ok') {
    $arrTplVars['strMessage'] = nl2br("Поздравляем! Ваше резюме\n<b>".$_SESSION['position']."</b>\nуспешно добавлено!".($arrIf['authed']?"\nВы можете просмотреть его в Личном кабинете.":''));
    $arrIf['is.msg'] = true;
    unset($_SESSION['status'], $_SESSION['position']);
}

// Сфера деятельности
$strSqlQuery = "SELECT * FROM `job_categories` WHERE `jc_parent` = 0 OR `jc_parent` IS NULL ORDER BY `jc_order`";
$arrCats = $objDb->fetchall($strSqlQuery);
if (is_array($arrCats) && !empty($arrCats)) {
    foreach ($arrCats as $k => $arr) {
        $arrCats[$k]['strOptGroupName'] = htmlspecialchars($arr['jc_title']);
        $arrCats[$k]['intGroupID'] = $arr['jc_id'];
    }
}
$objTpl->tpl_loop("page.content", "option.group.category", $arrCats);
if (is_array($arrCats) && !empty($arrCats)) {
    foreach ($arrCats as $k => $arr) {
        $strSqlQuery = "SELECT * FROM `job_categories` WHERE `jc_parent` = ".$arr['jc_id']." ORDER BY `jc_order`";
        $arrSubCats = $objDb->fetchall($strSqlQuery);

        if (is_array($arrSubCats) && !empty($arrSubCats)) {
            foreach ($arrSubCats as $kk => $varr) {
                $arrSubCats[$kk]['intOptionValue'] = intval($varr['jc_id']);
                $arrSubCats[$kk]['strOptionTitle'] = htmlspecialchars($varr['jc_title']);
                $arrSubCats[$kk]['selected'] = ($varr['jc_id']==$intCategoryId ? ' selected' : '');
            }
        }

        $objTpl->tpl_loop("page.content", "group.options.".$arr['jc_id'], $arrSubCats);
    }
}

// Города
$strSqlQuery = "SELECT `city` FROM `site_cities` ORDER BY `pos`, `id`";
$arrCities = $objDb->fetchcol($strSqlQuery);
if (is_array($arrCities) && !empty($arrCities)) {
    foreach ($arrCities as $k => $city) {
        $arrCities[$k] = htmlspecialchars($city);
    }
}
$arrTplVars['strCities'] = '"'.implode('","', $arrCities).'"';
//$arrTplVars['strCities'] = '"Санкт-Петербург","Москва","Лабытнанги","Салехард"';

// Тип занятости
$strSqlQuery = "SELECT * FROM `job_workbusy` ORDER BY jw_id";
$arrWorkbusyTypes = $objDb->fetchall($strSqlQuery);
$objTpl->tpl_loop("page.content", "workbusy.types", $arrWorkbusyTypes);

// Образование
$strSqlQuery = "SELECT * FROM `job_education_types` ORDER BY jet_id";
$arrEduTypes = $objDb->fetchall($strSqlQuery);
$objTpl->tpl_loop("page.content", "education.types", $arrEduTypes);

// Если заполнить резюме хочет зарегистрированный клиент, подставим известные поля
if ($objUser->isAuthed()) {
    // Если пытаемся отредактировать резюме
    if (isset($arrReqUri[1]) && intval($arrReqUri[1])) {
        // вытянем данные по резюме
        $intResumeId = $arrReqUri[1];
        $Resume = new Resume();

        $arrResume = $Resume->getData($intResumeId);

        if (is_array($arrResume) && !empty($arrResume)) {
            # form
            $arrTplVars['resumeID'] = intval($intResumeId);
            $arrTplVars['strFio'] = htmlspecialchars($arrResume['jr_fio']);
            $arrTplVars['strTitle'] = htmlspecialchars($arrResume['jr_title']);
            $arrTplVars['strEmail'] = htmlspecialchars($arrResume['jr_email']);
            $arrTplVars['strSkype'] = htmlspecialchars($arrResume['jr_skype']);
            $arrTplVars['strICQ'] = htmlspecialchars($arrResume['jr_icq']);
            $arrTplVars['strLinkedin'] = htmlspecialchars($arrResume['jr_linkedin']);
            $arrTplVars['strSalary'] = htmlspecialchars($arrResume['jr_salary']);
            $arrTplVars['strAbout'] = htmlspecialchars($arrResume['jr_about']);
            $arrTplVars['strEduPlus'] = htmlspecialchars($arrResume['jr_edu_plus']);
            $arrTplVars['strExperienceDescription'] = htmlspecialchars($arrResume['jr_exp_desc']);
            $arrTplVars['strCitizenship'] = htmlspecialchars($arrResume['jr_grazhdanstvo']);

            $strCityById = '';
            if ($arrResume['jr_city_id'] > 0) {
                $arrCity = $objSiteUser->getCityById($arrResume['jr_city_id']);
                if (is_array($arrCity) && !empty($arrCity)) {
                    $strCityById = $arrCity['city'];
                }
                unset($arrCity);
            }

            $arrTplVars['strCity'] = (!empty($strCityById) ? $strCityById : (!empty($arrResume['jr_city']) ? htmlspecialchars($arrResume['jr_city']) : ''));
            if (!empty($arrResume['jr_birthday'])) {
                $arrTplVars['strBirthday'] = $objUtils->workDate(3, $arrResume['jr_birthday']);
            } else {
                $ages = $arrResume['jr_age'] > 0 ? $arrResume['jr_age'] : 30;
                $arrTplVars['strBirthday'] = date('d.m.Y', strtotime("-$ages years"));
            }

            $arrTplVars['cbxAgeOnly'] = $arrResume['jr_age'] > 0 ? ' checked' : '';
            $arrTplVars['cbxMarriage'] = $arrResume['jr_marriage'] == 'Y' ? ' checked' : '';
            $arrTplVars['cbxDriver'] = $arrResume['jr_driver'] == 'Y' ? ' checked' : '';
            $arrTplVars['rbChildren_'.$arrResume['jr_children']] = ' checked';
            $arrTplVars['rbTrips_'.$arrResume['jr_trips']] = ' checked';
            $arrTplVars['rb_'.$arrResume['jr_sex']] = ' checked';

            if (!empty($arrResume['jr_phone'])) {
                $arrPhoneData = explode(' ', $arrResume['jr_phone']);

                if (is_array($arrPhoneData) && !empty($arrPhoneData) && $arrPhoneData[0] == '+7') {
                    $arrTplVars['mobECode'] = isset($arrPhoneData[1]) ? $arrPhoneData[1] : '';
                    $arrTplVars['mobPhone'] = isset($arrPhoneData[2]) ? $arrPhoneData[2] : '';
                    $arrTplVars['mobPhoneComment'] = isset($arrPhoneData[3]) ? $arrPhoneData[3] : '';
                }
            }
        }
    } else {
        $arrTplVars['strFio'] = !empty($arrTplVars['strFio']) ? $arrTplVars['strFio'] : $objUser->getFullName();
        $arrTplVars['strCity'] = !empty($arrTplVars['strCity']) ? $arrTplVars['strCity'] : $objUser->getCity();
        $arrTplVars['strEmail'] = !empty($arrTplVars['strEmail']) ? $arrTplVars['strEmail'] : $objUser->getEmail();

        $phone = $objUser->getPhone();

        $phone = preg_replace("/\s+/", '', $phone);
        $phone = preg_replace("/\-/", '', $phone);
        $phone = preg_replace("/^\+7/", '', $phone);

        $phone_code = '';
        if (strlen($phone) == 10) {
            $phone_code = substr($phone, 0, 3);
            $phone = substr($phone, 3);
        }

        $arrTplVars['mobECode'] = !empty($arrTplVars['mobECode']) ? $arrTplVars['mobECode'] : $phone_code;
        $arrTplVars['mobPhone'] = !empty($arrTplVars['mobPhone']) ? $arrTplVars['mobPhone'] : $phone;
    }
}

$arrIf['no.image'] = !$arrIf['exist.image'];
$arrTplVars['strCity'] = (!empty($arrTplVars['strCity'])?$arrTplVars['strCity']:'Санкт-Петербург');
$arrTplVars['strExpCity'] = (!empty($arrTplVars['strCity'])?$arrTplVars['strCity']:'Санкт-Петербург');
$arrTplVars['rbTrips_N'] = (!isset($arrTplVars['rbTrips_Y']) && !isset($arrTplVars['rbTrips_S'])?' checked':'');
$arrTplVars['intExp'] = 1;

$objTpl->tpl_if("page.content", $arrIf);
$objTpl->tpl_array("page.content", $arrTplVars);
