<?php
/** Created by phpWebStudio(c) 2013 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Статьи [articles] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.content", "articles.10.tpl");

// Всего записей в базе
$strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM `site_articles` WHERE sa_status = 'Y' AND sa_type='article' AND sa_date_publish <= NOW()";
$arrTplVars['intQuantitySelectRecords'] = $arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');
// Выбрано записей
//$strSqlQuery = "SELECT COUNT(*) AS intQuantitySelectRecords FROM `site_articles` WHERE sa_status = 'Y' AND sa_date_publish <= DATE_FORMAT(NOW(), '%Y-%m-%d')";
//$arrTplVars['intQuantitySelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantitySelectRecords');
// ***** BEGIN: Построение пейджинга для вывода списка
$objPagination = new tplPaginator($arrTplVars['intQuantitySelectRecords'], SITE_TPL_TPL_DIR);
/**
 * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
 * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
 */
$objPagination->strPaginatorTpl = "site.global.4.tpl";
$objPagination->intQuantRecordPage = 15;
$objPagination->strColorLinkStyle = "link-b";
$objPagination->strColorActiveStyle = "tab-bl-b";
$objPagination->strNameImageBack = "arrow_one_left.gif";
$objPagination->strNameImageForward = "arrow_one_right.gif";
// Создаем блок пэйджинга
$objPagination->paCreate();
// ***** END: Построение пейджинга для вывода списка
// Запрос для выборки нужных записей
$strSqlQuery = "SELECT * FROM `site_articles`"
    ." WHERE sa_status = 'Y' AND sa_type='article' AND sa_date_publish <= NOW()"
    ." ORDER BY sa_date_publish DESC, sa_id DESC ".$objPagination->strSqlLimit;
$arrLastArticles = $objDb->fetchall( $strSqlQuery );
// кол-во публикаций показанных на странице
$arrTplVars['intQuantityShowRecOnPage'] = ( !empty($arrLastArticles) ? count($arrLastArticles) : 0 );
if ($arrTplVars['intQuantityShowRecOnPage'] >= $objPagination->intQuantRecordPage) {
    // Присвоение значения пэйджинга для списка
    $arrTplVars['blockPaginator'] = $objPagination->paShow();
}

if ( is_array($arrLastArticles) ) {
    foreach ( $arrLastArticles as $key => $value ) {
        $arrLastArticles[$key]['strDatePublishArticle'] = date('d.m.Y', $value['sa_date_publish']);
//      $arrLastArticles[$key]['urlParam'] = (!empty($value['sa_url']) ? ((!empty($value['sa_date_publish']) ? $value['sa_date_publish'].'/' : '').$value['sa_url']) : $value['sa_id'] );
//      $arrIf['articles.picture.'.$value['sa_id']] = ( file_exists(DROOT."storage/images/articles/articles.photo.".$value['sa_id'].".jpg") && $value['sa_image'] == 'Y');
//      $arrLastArticles[$key]['strUniquePhoto'] = $arrIf['articles.picture.'.$value['sa_id']] ? '?pic='.time() : '';
    }
}

$objTpl->tpl_loop("page.content", "last.articles", $arrLastArticles);
$objTpl->tpl_if("page.content", $arrIf);
$objTpl->tpl_array("page.content", $arrTplVars);
