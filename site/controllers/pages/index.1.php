<?php
/** Created by WMBM(c) 2006 (Shirokovskiy D. aka Jimmy™).
 * Страница: Главная страница сайта (обложка) [index] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.content", "index.1.tpl");

$countVac =
$countRes = 22;
$strSqlWhere = null;

if (!isset($objSiteUser)) {
    $objSiteUser = new SiteUser();
}

$objVacancy = new Vacancy();
$objResume = new Resume();

if (isset($_SESSION['myCity']) && !empty($_SESSION['myCity'])) {
    # показать на главной только с учётом города
    $cityId = $objSiteUser->getCityId($_SESSION['myCity']);
    if (!empty($cityId)) {
        $strSqlWhereV = " AND (jv_city LIKE '%".$_SESSION['myCity']."%' OR jv_city_id = $cityId)";
        $strSqlWhereR = " AND (jr_city LIKE '%".$_SESSION['myCity']."%' OR jr_city_id = $cityId)";
    } else {
        $strSqlWhereV = " AND jv_city LIKE '%".$_SESSION['myCity']."%'";
        $strSqlWhereR = " AND jr_city LIKE '%".$_SESSION['myCity']."%'";
    }
}

// Последние вакансии
$strSqlQuery = "SELECT jv_id, jv_title, jv_salary_from, jv_salary_to FROM `job_vacancies` WHERE `jv_status` = 'Y' $strSqlWhereV ORDER BY `jv_id` DESC LIMIT ".$countVac;
$arrVac = $objDb->fetchall($strSqlQuery);
if (is_array($arrVac) && !empty($arrVac)) {
    foreach ($arrVac as $k => $vacancy) {
        $arrVac[$k]['jv_title'] = mb_ucfirst($objUtils->substrText($vacancy['jv_title'], 60, true));
        $arrVac[$k]['strSalary'] = (!empty($vacancy['jv_salary_from']) ? 'от '.$vacancy['jv_salary_from'] : '').(!empty($vacancy['jv_salary_to']) ? (!empty($vacancy['jv_salary_from'])?' до ':'').$vacancy['jv_salary_to'] : '');
        $arrVac[$k]['strSeoUrl'] = strtolower( $objUtils->translitBadChars($objUtils->translitKyrToLat(trim($vacancy['jv_title'])), array(" ", '.')))."_".$vacancy['jv_id'];
        $arrVac[$k]['strSeoUrl'] = str_replace('__', '_', $arrVac[$k]['strSeoUrl']);
        $objVacancy->setId($vacancy['jv_id'])->saveSeoUrl($arrVac[$k]['strSeoUrl']);

        if (!empty($arrVac[$k]['strSalary'])) {
            $arrVac[$k]['strSalary'] .= ' руб';
        } elseif (!empty($vacancy['jv_salary'])) {
            $arrVac[$k]['strSalary'] = $vacancy['jv_salary'];
        }

        if (empty($arrVac[$k]['strSalary'])) {
            $arrVac[$k]['strSalary'] = 'зарплата не указана';
        }
    }
}
$arrIf['no.list.top.vacancies'] = !is_array($arrVac) || empty($arrVac);
$objTpl->tpl_loop("page.content", "list.top.vacancies", $arrVac);
unset($arrVac);

// Последние резюме
$strSqlQuery = "SELECT jr_id, jr_title, jr_salary FROM `job_resumes` WHERE `jr_status` = 'Y' AND `jr_salary` != '' AND `jr_salary` IS NOT NULL $strSqlWhereR ORDER BY `jr_id` DESC LIMIT ".$countRes;
$arrRes = $objDb->fetchall($strSqlQuery);
if (is_array($arrRes) && !empty($arrRes)) {
    foreach ($arrRes as $k => $resume) {
        $arrRes[$k]['jr_title'] = mb_ucfirst($objUtils->substrText($resume['jr_title'], 80, true));

        $arrRes[$k]['strSeoUrl'] = strtolower( $objUtils->translitBadChars($objUtils->translitKyrToLat(trim($resume['jr_title'])), array(" ", '.')))."_".$resume['jr_id'];
        $arrRes[$k]['strSeoUrl'] = str_replace('__', '_', $arrRes[$k]['strSeoUrl']);
        $objResume->setId($resume['jr_id'])->saveSeoUrl($arrRes[$k]['strSeoUrl']);
    }
}
$arrIf['no.list.top.resumes'] = !is_array($arrRes) || empty($arrRes);
$objTpl->tpl_loop("page.content", "list.top.resumes", $arrRes);
$arrTplVars['strMyCity'] = $_SESSION['myCity'] ? $_SESSION['myCity'] : '';
$objTpl->tpl_array("page.content", $arrTplVars);
$objTpl->tpl_if("page.content", $arrIf);
