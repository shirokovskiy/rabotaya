<?php
/** Created by phpWebStudio(c) 2013 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Все резюме [all-resumes] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.content", "all-resumes.3.tpl");

/**
 * Список всех активных резюме
 */
$objResume = new Resume();

// Всего записей в базе
$strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM `job_resumes` WHERE jr_status = 'Y' /*AND jr_date_publish <= UNIX_TIMESTAMP()*/";
$arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');

// Выбрано записей
$strSqlQuery = "SELECT COUNT(*) AS intQuantitySelectRecords FROM `job_resumes` WHERE jr_status = 'Y' /*AND jr_date_publish <= UNIX_TIMESTAMP()*/";
$arrTplVars['intQuantitySelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantitySelectRecords');
// ***** BEGIN: Построение пейджинга для вывода списка
$objPagination = new tplPaginator($arrTplVars['intQuantitySelectRecords'], SITE_TPL_TPL_DIR, 'p');

/**
 * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
 * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
 */
$objPagination->strPaginatorTpl = "site.global.4.tpl";
$objPagination->intQuantRecordPage = 25;
$objPagination->strColorActivePage = '8A0202';
$objPagination->strColorLinkStyle = "link-b";
$objPagination->strColorActiveStyle = "tab-bl-b";
$objPagination->strNameImageBack = "arrow_one_left.gif";
$objPagination->strNameImageForward = "arrow_one_right.gif";
// Создаем блок пэйджинга
$objPagination->paCreate();
// ***** END: Построение пейджинга для вывода списка

// Запрос для выборки нужных записей
$strSqlQuery = "SELECT * FROM `job_resumes`"
    ." WHERE jr_status = 'Y' /*AND jr_date_publish <= UNIX_TIMESTAMP()*/"
    ." ORDER BY jr_date_publish DESC, jr_id DESC ".$objPagination->strSqlLimit;
$arrResumes = $objDb->fetchall( $strSqlQuery );

// кол-во публикаций показанных на странице
$arrTplVars['intQuantityShowRecOnPage'] = ( !empty($arrResumes) ? count($arrResumes) : 0 );
// Присвоение значения пэйджинга для списка
$arrTplVars['blockPaginator'] = $objPagination->paShow();

if ( is_array($arrResumes) ) {
    foreach ( $arrResumes as $key => $value ) {
        $arrResumes[$key]['strTitle'] = /*($value['jr_grab_src_id']>0?'*** ':'').*/ mb_ucfirst($value['jr_title']);
        $arrResumes[$key]['strDatePublish'] = date('d.m.y', $value['jr_date_publish']);
        //$arrIf['news.picture.'.$value['jr_id']] = ( file_exists(DROOT."storage/images/news/news.photo.".$value['jr_id'].".jpg") && $value['jr_image'] == 'Y');
        $arrResumes[$key]['strUniquePhoto'] = $arrIf['news.picture.'.$value['jr_id']] ? '?pic='.time() : '';
        $arrResumes[$key]['urlParam'] = (!empty($value['jr_url']) ? ((!empty($value['jr_date_publish']) ? $value['jr_date_publish'].'/' : '').$value['jr_url']) : $value['jr_id'] );

        $arrResumes[$key]['strSalary'] = (!empty($value['jr_salary_from']) ? 'от '.$value['jr_salary_from'] : '').(!empty($value['jr_salary_to']) ? (!empty($value['jr_salary_from'])?' до ':'').$value['jr_salary_to'] : '');

        if (!empty($arrResumes[$key]['strSalary'])) {
            $arrResumes[$key]['strSalary'] .= ' р.';
        } elseif (!empty($value['jr_salary'])) {
            if (intval($value['jr_salary']) > 0) {
                $arrResumes[$key]['strSalary'] = 'от '.$value['jr_salary'].' р.';
            } else {
                $arrResumes[$key]['strSalary'] = $value['jr_salary']; // в этом случае возможно введён просто текст в ячейке
            }
        }

        $arrResumes[$key]['strDescription'] = $objUtils->substrText($value['jr_about'], 220, true);
        $arrIf['is.strDescription.'.$value['jr_id']] = !empty($arrResumes[$key]['strDescription']);

        $arrResumes[$key]['strSeoUrl'] = strtolower( $objUtils->translitBadChars($objUtils->translitKyrToLat(trim($value['jr_title'])), array(" ", '.')))."_".$value['jr_id'];
        $arrResumes[$key]['strSeoUrl'] = str_replace('__', '_', $arrResumes[$key]['strSeoUrl']);
        $objResume->setId($value['jr_id'])->saveSeoUrl($arrResumes[$key]['strSeoUrl']);
    }
}

$objTpl->tpl_loop("page.content", "list.resumes", $arrResumes);
$objTpl->tpl_if("page.content", $arrIf);
$objTpl->tpl_array("page.content", $arrTplVars);
