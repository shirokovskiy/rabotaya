<?php
/** Created by phpWebStudio(c) 2013 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Поиск по компаниям [po-kompaniyam] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.content", "po-kompaniyam.20.tpl");

$arrTplVars['link'] = 'company-vacancies';
$pageSymbol = 'p';
$strSearchJoin = '';

$arrRusChars = array();
if (is_array($objUtils->arrRusChars) && !empty($objUtils->arrRusChars)) {
    foreach ($objUtils->arrRusChars as $k => $char) {
        $arrRusChars[$k]['char'] = $char;
        $arrRusChars[$k]['charUrl'] = urlencode($char);
    }
}
$objTpl->tpl_loop("page.content", "rus.chars", $arrRusChars);

$arrEngChars = array();
if (is_array($objUtils->arrEngChars) && !empty($objUtils->arrEngChars)) {
    foreach ($objUtils->arrEngChars as $k => $char) {
        $arrEngChars[$k]['char'] = $char;
        $arrEngChars[$k]['charUrl'] = urlencode($char);
    }
}
$objTpl->tpl_loop("page.content", "eng.chars", $arrEngChars);


/**
 * Проверка ввода параметров
 */
if (isset($arrReqUri[1])) {
    switch ($arrReqUri[1]) {
        case 'company-by-first-char':
            if (isset($arrReqUri[2]) && !empty($arrReqUri[2])) {
                $strSearchTerm = " AND jc_title LIKE '".mysql_real_escape_string(urldecode($arrReqUri[2]))."%'";
            }
            break;

        default:
            break;
    }
}

if (!isset($objSiteUser)) {
    $objSiteUser = new SiteUser();
}

if (isset($_SESSION['myCity']) && !empty($_SESSION['myCity'])) {
    $cityId = $objSiteUser->getCityId($_SESSION['myCity']);
    if (!empty($cityId)) {
        $strSqlWhere = " AND (jc_city LIKE '%".$_SESSION['myCity']."%' OR jc_city_id = $cityId)";
    } else {
        $strSqlWhere = " AND jc_city LIKE '%".$_SESSION['myCity']."%'";
    }
}

// Всего записей в базе
$strSqlQueryAll = "SELECT COUNT(*) AS intCountAllRecords FROM `job_companies` WHERE jc_status = 'Y'" . $strSqlWhere;

// Выборка записей
$strSqlQueryFilter = "SELECT COUNT(*) AS intQuantitySelectRecords FROM `job_companies` $strSearchJoin WHERE jc_status = 'Y'".$strSearchTerm.$strSqlWhere;

$arrTplVars['intCountAllRecords'] = $objDb->fetch( $strSqlQueryAll, 'intCountAllRecords');
$arrTplVars['intQuantitySelectRecords'] = $objDb->fetch( $strSqlQueryFilter, 'intQuantitySelectRecords');

// ***** BEGIN: Построение пейджинга для вывода списка
$objPagination = new tplPaginator($arrTplVars['intQuantitySelectRecords'], SITE_TPL_TPL_DIR, $pageSymbol);

/**
 * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
 * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
 */
$objPagination->strPaginatorTpl = "site.global.4.tpl";
$objPagination->intQuantRecordPage = 25;
$objPagination->strColorActivePage = '8A0202';
$objPagination->strColorLinkStyle = "link-b";
$objPagination->strColorActiveStyle = "tab-bl-b";
$objPagination->strNameImageBack = "arrow_one_left.gif";
$objPagination->strNameImageForward = "arrow_one_right.gif";
// Создаем блок пэйджинга
$objPagination->paCreate();
// ***** END: Построение пейджинга для вывода списка


$arrFields['jc_id'] = 'id';
$arrFields['jc_description'] = 'description';
$arrFields['jc_title'] = 'title';
foreach ($arrFields as $field => $new) {
    $arrFieldParams[] = "`$field` AS `$new`";
}
$strFields = implode(',',$arrFieldParams);
$strSqlQuery = "SELECT $strFields FROM `job_companies` ".$strSearchJoin
    ." WHERE jc_status = 'Y'".$strSearchTerm.$strSqlWhere
    ." ORDER BY jc_date_create DESC, jc_id DESC ".$objPagination->strSqlLimit;
$arrRecords = $objDb->fetchall( $strSqlQuery );

// кол-во публикаций показанных на странице
$arrTplVars['intQuantityShowRecOnPage'] = ( !empty($arrRecords) ? count($arrRecords) : 0 );
// Присвоение значения пэйджинга для списка
if ($arrTplVars['intQuantityShowRecOnPage'] >= $objPagination->intQuantRecordPage || isset($_GET[$pageSymbol])) {
    $arrTplVars['blockPaginator'] = $objPagination->paShow();
}

if ( is_array($arrRecords) ) {
    foreach ( $arrRecords as $key => $value ) {
        $arrRecords[$key]['strDescription'] = $objUtils->substrText($value['description'], 220, true);
        $arrRecords[$key]['strTitle'] = mb_ucfirst($value['title']);
    }
}


$arrIf['no.results'] = empty($arrRecords);
$objTpl->tpl_if("page.content", $arrIf);
$objTpl->tpl_loop("page.content", "list.results", $arrRecords);
$objTpl->tpl_array("page.content", $arrTplVars);
