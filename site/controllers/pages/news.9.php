<?php
/** Created by phpWebStudio(c) 2013 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Новости [news] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.content", "news.9.tpl");

include_once "models/News.php";
$objNews = new News($_db_config);

/**
 * Если указана конкретная новость - покажем её
 */
if (isset($arrReqUri[1]) && !empty($arrReqUri[1])) {
    if(isset($arrReqUri[2]) && !empty($arrReqUri[2])) {
        $nDate = $arrReqUri[1];
        $nUrl = trim($arrReqUri[2]);
        $strSqlQuery = "SELECT * FROM site_news WHERE sn_status='Y' AND sn_url='".$nUrl."' AND sn_date_publ='".$nDate."'";
    } else {
        $intId = intval($arrReqUri[1]);
        $strSqlQuery = "SELECT * FROM site_news WHERE sn_status='Y' AND sn_id=".$intId;
        $flagUpdate = true;
    }
    $arrNewsInfo = $objDb->fetch( $strSqlQuery );
    // Проверим, существует ли такая новость, и если да, то обработаем для вывода
    if (!empty($arrNewsInfo)) {
        if(!$flagUpdate) {
            $intId = intval($arrNewsInfo['sn_id']);
        }
        $arrNewsInfo['strDatePublNews'] = $objUtils->workDate(2, $arrNewsInfo['sn_date_publ']);
        $arrNewsInfo['strNewsBody'] = $arrNewsInfo['sn_body'];
        $arrNewsImage = $objNews->getNewsImage($arrNewsInfo['sn_id']);
        $arrNewsInfo['strImageTitle'] = htmlspecialchars($arrNewsImage['si_title']);

        $arrImages = $objNews->getNewsImage($arrNewsInfo['sn_id']);
        if (is_array($arrImages) && !empty($arrImages)) {
            $arrIf['picture'] = true;
            foreach ($arrImages as $k => $image) {
                $arrImages[$k]['small_filename'] = str_replace('.b.', '.', $image['si_filename']);
            }
        }
        $objTpl->tpl_loop("page.content", "images", $arrImages);
        $objTpl->tpl_array("page.content", $arrNewsInfo);

        $arrTplVars['m_title'] = htmlspecialchars(strip_tags($arrNewsInfo['sn_title'])).' | '.$arrTplVars['m_title'];
        $arrTplVars['m_description'] = $objUtils->substrText(htmlspecialchars(strip_tags($arrNewsInfo['sn_body'])));

        $arrTplVars['strUniqueMainPhoto'] = $arrIf['picture'] ? '?pic='.time() : '';

        // Показать картинки, если они есть
        if ($arrNewsInfo['sn_image']=='Y') {
            # Дополнительные картинки есть только в том случае, если есть оснавная
            $strSqlQuery = "SELECT si_id,si_desc FROM site_images WHERE si_status='Y' AND si_rel_id = ".$intId." AND si_type = 'photorep'";
            $arrMoreImages = $objDb->fetchall( $strSqlQuery );
            $arrIf['pic_group'] = is_array($arrMoreImages) && !empty($arrMoreImages);
        }

        // Check attachments
        $newsAttachedFiles = PRJ_FILES.'news_attachments/'.$intId.'/';
        if (is_dir($newsAttachedFiles)) {
            $exclude_list = array(".", "..");
            $arrAttachedFiles = array_diff(scandir($newsAttachedFiles), $exclude_list);
            if (is_array($arrAttachedFiles)) {
                $arrIf['is.attached.files'] = !empty($arrAttachedFiles);
                foreach ($arrAttachedFiles as $k => $v) {
                    $arrAttachedFilesList[$k]['name'] = $v;
                    $arrAttachedFilesList[$k]['at.url'] = $arrTplVars['cfgFilesUrl'].'news_attachments/'.$intId.'/'.$v;
                }
            }
        }

        /**
         * Обновим URL новости, если её нет
         */
        if ($flagUpdate && empty($arrNewsInfo['sn_url']) && $intId > 0){
            $strUrl = strtolower( $objUtils->translitBadChars($objUtils->translitKyrToLat(trim($arrNewsInfo['sn_title']))) );
            $strSqlQuery = "UPDATE `site_news` SET `sn_url` = '$strUrl' WHERE `sn_id` = $intId";
            if (!$objDb->query($strSqlQuery)) {
                # sql error
            }
        }
    } else {
        header('Location: '.SITE_URL.'error');
        exit();
    }

    $objTpl->tpl_loop( "page.content", "list", $arrMoreImages );
    $objTpl->tpl_loop( "page.content", "attachments", $arrAttachedFilesList );
    $arrIf['novelty'] = true;

} else {
    /**
     * Список новостей
     */

    // Всего записей в базе
    $strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM `site_news` WHERE sn_status = 'Y' AND sn_date_publ <= DATE_FORMAT(NOW(), '%Y-%m-%d')";
    $arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');
    // Выбрано записей
    $strSqlQuery = "SELECT COUNT(*) AS intQuantitySelectRecords FROM `site_news` WHERE sn_status = 'Y' AND sn_date_publ <= DATE_FORMAT(NOW(), '%Y-%m-%d')";
    $arrTplVars['intQuantitySelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantitySelectRecords');
    // ***** BEGIN: Построение пейджинга для вывода списка
    $objPagination = new tplPaginator($arrTplVars['intQuantitySelectRecords'], SITE_TPL_TPL_DIR);
    /**
     * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
     * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
     */
    $objPagination->strPaginatorTpl = "site.global.4.tpl";
    $objPagination->intQuantRecordPage = 15;
    $objPagination->strColorLinkStyle = "link-b";
    $objPagination->strColorActiveStyle = "tab-bl-b";
    $objPagination->strNameImageBack = "arrow_one_left.gif";
    $objPagination->strNameImageForward = "arrow_one_right.gif";
    // Создаем блок пэйджинга
    $objPagination->paCreate();
    // ***** END: Построение пейджинга для вывода списка
    // Запрос для выборки нужных записей
    $strSqlQuery = "SELECT * FROM `site_news`"
        ." WHERE sn_status = 'Y' AND sn_date_publ <= DATE_FORMAT(NOW(), '%Y-%m-%d')"
        ." ORDER BY sn_date_publ DESC, sn_id DESC ".$objPagination->strSqlLimit;
    $arrLastNews = $objDb->fetchall( $strSqlQuery );
    // кол-во публикаций показанных на странице
    $arrTplVars['intQuantityShowRecOnPage'] = ( !empty($arrLastNews) ? count($arrLastNews) : 0 );

    if ($arrTplVars['intQuantityShowRecOnPage'] >= $objPagination->intQuantRecordPage) {
        // Присвоение значения пэйджинга для списка
        $arrTplVars['blockPaginator'] = $objPagination->paShow();
    }

    if ( is_array($arrLastNews) ) {
        foreach ( $arrLastNews as $key => $value ) {
            $arrLastNews[$key]['strDatePublicNews'] = date('d.m.y', strtotime($value['sn_date_publ']));
            $arrLastNews[$key]['urlParam'] = (!empty($value['sn_url']) ? ((!empty($value['sn_date_publ']) ? $value['sn_date_publ'].'/' : '').$value['sn_url']) : $value['sn_id'] );


            //$arrIf['news.picture.'.$value['sn_id']] = ( file_exists(DROOT."storage/images/news/news.photo.".$value['sn_id'].".jpg") && $value['sn_image'] == 'Y');
//            $arrLastNews[$key]['strUniquePhoto'] = $arrIf['news.picture.'.$value['sn_id']] ? '?pic='.time() : '';
        }
    }

    $objTpl->tpl_loop("page.content", "last.news", $arrLastNews);
    $arrIf['news.list'] = true;
}

$objTpl->tpl_if("page.content", $arrIf);

$objTpl->tpl_array("page.content", $arrTplVars);
