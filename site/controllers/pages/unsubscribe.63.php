<?php
/** Created by phpWebStudio(c) 2014 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Отписаться от рассылки [unsubscribe] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.content", "unsubscribe.63.tpl");

if (isset($arrReqUri[1])) {
    switch ($arrReqUri[1]) {
        case 'post':
            $email_unsubscribe = trim($_POST['email']);
            if (isset($arrReqUri[2]) && $arrReqUri[2] == md5(date('Y-m-d H')) && $objUtils->isValidEmail($email_unsubscribe)) {
                // let's check
                $Spamer = new Spamer();
                // проверим что такой Email существует
                $arrEmail = $Spamer->getSubscriberByEmail($email_unsubscribe);
                if (is_array($arrEmail) && !empty($arrEmail)) {
                    if ($Spamer->removeSubscriber($email_unsubscribe)) {
                        $arrIf['success'] = true;
                        $arrTplVars['strSuccessMsg'] = 'Адрес '.$email_unsubscribe.' успешно отписан от дальнейших рассылок!';
                    }
                } else {
                    $arrIf['error'] = true;
                    $arrTplVars['strErrorMsg'] = 'возможно Вы не правильно ввели адрес!';
                }
            } else {
                $arrIf['error'] = true;
                $arrTplVars['strErrorMsg'] = 'попробуйте ещё раз';
            }
            break;

        case 'email':
            if (isset($arrReqUri[2]) && isset($arrReqUri[3])) {
                // let's check
                $email_unsubscribe = urldecode($arrReqUri[2]);
                if (!$objUtils->isValidEmail($email_unsubscribe)) {
                    trace_log($email_unsubscribe, 'wrong.emails.to.unsubscribe.txt');
                }
                $secret_token = $arrReqUri[3];
                $check_secret_token = null;
                $Spamer = new Spamer();
                $arrEmail = $Spamer->getSubscriberByEmail($email_unsubscribe);
                if (is_array($arrEmail) && !empty($arrEmail)) {
                    $check_secret_token = md5( md5($arrEmail['sus_email']) . $arrEmail['sus_id'] );
                }
                if ($secret_token==$check_secret_token && !empty($check_secret_token)) {
                    # тогда тоукены равны, можно email удалять
                    if ($Spamer->removeSubscriber($email_unsubscribe)) {
                        $arrIf['success'] = true;
                        $arrTplVars['strSuccessMsg'] = 'Адрес '.$email_unsubscribe.' успешно удалён!';
                    }
                }
            } else {
                $arrIf['error'] = true;
                $arrTplVars['strErrorMsg'] = 'Error***';
            }
            break;

        default:
            break;
    }
}

/**
 * Правило для формирования UnSID
 * md5( md5( sus_email ). sus_id )
 */
$arrTplVars['UnSID'] = md5(date('Y-m-d H'));
$objTpl->tpl_if("page.content", $arrIf);
$objTpl->tpl_array("page.content", $arrTplVars);
