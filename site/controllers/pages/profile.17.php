<?php
/** Created by phpWebStudio(c) 2013 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Личный кабинет [profile] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.content", "profile.17.tpl");

$arrTplVars['strMessage'] = "";

$arrTplVars['strSID'] = md5(time().':solyanka');

if (!isset($objResume) || !is_object($objResume)) {
    $objResume = new Resume();
}
if (!isset($objVacancy) || !is_object($objVacancy)) {
    $objVacancy = new Vacancy();
}

/**
 * Сохраняем профайл
 */
if (isset($_SESSION['strSID']) && isset($_POST['strSID'])) {
    // принять форму
    if ($_SESSION['strSID']==$_POST['strSID']) {
        $arrSqlData['lname'] = mysql_real_escape_string(trim($_POST['strLName']));
        $arrTplVars['strLName'] = htmlspecialchars(stripslashes(trim($_POST['strLName'])));

        $arrSqlData['fname'] = mysql_real_escape_string(trim($_POST['strFName']));
        $arrTplVars['strFName'] = htmlspecialchars(stripslashes(trim($_POST['strFName'])));

        $arrSqlData['mname'] = mysql_real_escape_string(trim($_POST['strMName']));
        $arrTplVars['strMName'] = htmlspecialchars(stripslashes(trim($_POST['strMName'])));

        $arrSqlData['email'] = mysql_real_escape_string(trim($_POST['strEmail']));
        $arrTplVars['strEmail'] = htmlspecialchars(stripslashes(trim($_POST['strEmail'])));

        $arrSqlData['cbxSubsribe'] = ($_POST['cbxSubsribe'] == 'on' ? 'Y' : 'N');
        $arrTplVars['cbxSubsribe'] = ($_POST['cbxSubsribe'] == 'on' ? ' checked' : '');

        $arrSqlData['curpassword'] = mysql_real_escape_string(trim($_POST['strOldPassword']));
        $arrTplVars['strOldPassword'] = htmlspecialchars(stripslashes(trim($_POST['strOldPassword'])));

        $arrSqlData['password'] = mysql_real_escape_string(trim($_POST['strNewPassword']));
        $arrTplVars['strNewPassword'] = htmlspecialchars(stripslashes(trim($_POST['strNewPassword'])));

        $arrSqlData['passwordrepeat'] = mysql_real_escape_string(trim($_POST['strConfirmPassword']));
        $arrTplVars['strConfirmPassword'] = htmlspecialchars(stripslashes(trim($_POST['strConfirmPassword'])));

        if ($objUser->setData($arrSqlData)->save()) {
            $_SESSION['status'] = 'ok';
        }

        header('location: /profile');
        exit;
    }
}

$_SESSION['strSID'] = $arrTplVars['strSID'];

if ($_SESSION['status']=='ok') {
    $arrTplVars['msgStatus'] = 0;
    $arrTplVars['strMessage'] = nl2br("Поздравляем!!!\nВаши данные успешно обновлены!");
    $arrIf['is.msg'] = true;
    unset($_SESSION['status']);
}

if ($_SESSION['status']=='new_user') {
    $arrTplVars['msgStatus'] = 0;
    $arrTplVars['strMessage'] = nl2br("Рады приветствовать Вас!\nДля Вас создан Личный Кабинет!\nВаш сгенерированный пароль <b>".$_SESSION['new_pass']."</b> и выслан на Email.\nВы можете сменить его в Личном Кабинете на более запоминающийся для Вас прямо сейчас.");
    $arrIf['is.msg'] = true;
    $arrIf['is.new.user'] = true;
    $arrTplVars['strNewPassword'] = $newPassword = $_SESSION['new_pass'];

    if (isset($_SESSION['new_user_resume_id']) && $_SESSION['new_user_resume_id'] > 0) {
        $objResume->setIsInvited($_SESSION['new_user_resume_id']);
    }

    if (isset($_SESSION['new_user_vacancy_id']) && $_SESSION['new_user_vacancy_id'] > 0) {
        $objVacancy->setIsInvited($_SESSION['new_user_vacancy_id']);
    }

    if (!is_object($objMail)) {
        include_once("cls.mail.php");
        $objMail = new clsMail();
    }

    $sendTo = $_SESSION['new_login'];
    $sendFrom = "info@rabota-ya.ru";
    $sendSubject = "Работа от А до Я | Новый пароль в Личный Кабинет";

    $sendMessage = "Уважаемый пользователь сайта Rabota-Ya.Ru!\r\n";
    $sendMessage .= "Для Вас автоматически создан Личный Кабинет на сайте www.rabota-ya.ru/profile :\r\n";
    $sendMessage .= "Логин: $sendTo \r\n";
    $sendMessage .= "Пароль: $newPassword \r\n";
    $sendMessage .= "\r\n";
    $sendMessage .= "По всем вопросам обращайтесь по телефону или посредствам E-mail указаных на странице сайта Контакты \r\n";
    $sendMessage .= "\r\n";
    $sendMessage .= "Не отвечайте на данное письмо! Текст сформирован автоматически почтовым роботом.\r\n";
    $sendMessage .= "\r\n===============================================================\r\n";


    $sendHTMLMessage = file_get_contents(SITE_TPL_DIR.'emails/new.password.html');

    $linkToLogin = SITE_URL.'profile';
    $linkToContacts = SITE_URL.'contacts';
    $sendHTMLMessage = preg_replace("/\{\%LINK_LOGIN\%\}/", $linkToLogin, $sendHTMLMessage);
    $sendHTMLMessage = preg_replace("/\{\%LINK_CONTACTS\%\}/", $linkToContacts, $sendHTMLMessage);

    $sendHTMLMessage = preg_replace("/\{\%login\%\}/", $sendTo, $sendHTMLMessage);
    $sendHTMLMessage = preg_replace("/\{\%password\%\}/", $newPassword, $sendHTMLMessage);
    $sendHTMLMessage = preg_replace("/\n/", "\r\n", $sendHTMLMessage);

    if (isset($_SERVER['_IS_DEVELOPER_MODE']) && $_SERVER['_IS_DEVELOPER_MODE'] == 1) {
        $sendTo = "jimmy.webstudio@gmail.com"; // DEBUG
    }

//    $sendTo = 'info@rabota-ya.ru'; // DEBUG

    $objMail->new_mail($sendFrom, $sendTo, $sendSubject, $sendMessage, $sendHTMLMessage);
    if ($sendTo != "jimmy.webstudio@gmail.com") {
        $objMail->add_bcc("jimmy.webstudio@gmail.com");
    }

    $objMail->add_attachment( PRJ_SITEIMG.'content/email-bg.jpg', 'email-bg.jpg', 'image' );

    if (!$objMail->send()) {
        write_file("Письмо с паролем не ушло на $sendTo! =(");
    }

    unset($_SESSION['status'], $_SESSION['new_pass']);
}

/**
 * Проверить автологин по ссылке
 */
if (isset($arrReqUri[1]) && $arrReqUri[1] == 'autologin') {
    # Проверим наличие хеша
    if (isset($arrReqUri[2]) && strlen($arrReqUri[2]) == 32) {
        # проверим валидность хеша
        if ($objResume->isValidLink2Profile($arrReqUri[2])) {
            # произвести автоматическую авторизацию здесь для соискателя!!!
            $arrUserInfo = $objResume->getRecordDataByHash($arrReqUri[2]);

            if (is_array($arrUserInfo) && !empty($arrUserInfo)) {
                # есть резюме, а есть ли пользователь?
                if (isset($arrUserInfo['su_id']) && $arrUserInfo['su_id'] > 0) {
                    # пользователь существует и его создавать не надо
                } else {
                    # необходимо создать пользователя
                    $arrSqlData['lname'] = 'Unknown';
                    $arrSqlData['fname'] = 'Unknown';
                    $arrSqlData['mname'] = 'Unknown';

                    if (isset($arrUserInfo['jr_fio']) && !empty($arrUserInfo['jr_fio'])) {
                        $arrFIO = explode(' ', $arrUserInfo['jr_fio']);

                        $arrSqlData['lname'] = (isset($arrFIO[0]) ? $arrFIO[0] : '');
                        $arrSqlData['fname'] = (isset($arrFIO[1]) ? $arrFIO[1] : '');
                        $arrSqlData['mname'] = (isset($arrFIO[2]) ? $arrFIO[2] : '');

                        unset($arrFIO);
                    }

                    $arrSqlData['email'] = $arrUserInfo['jr_email'];
                    $arrSqlData['phone'] = $arrUserInfo['jr_phone'];

                    if (isset($arrUserInfo['jr_city_id']) && intval($arrUserInfo['jr_city_id'])) {
                        $arrSqlData['city_id'] = $arrUserInfo['jr_city_id'];
                    } elseif (isset($arrUserInfo['jr_city']) && !empty($arrUserInfo['jr_city'])) {
                        $arrSqlData['city'] = $arrUserInfo['jr_city'];
                    }

                    if (isset($arrUserInfo['jr_title']) && !empty($arrUserInfo['jr_title'])) {
                        $arrSqlData['position'] = $arrUserInfo['jr_title'];
                    }

                    $password = $objUtils->getRandomPassw(7);

                    $arrSqlData['password'] = mysql_real_escape_string(trim($password));
                    $arrSqlData['passwordrepeat'] = mysql_real_escape_string(trim($password));

                    if ($objUser->setData($arrSqlData)->save()) {
                        $_SESSION['status'] = 'new_user';
                        $_SESSION['new_login'] = $arrUserInfo['jr_email'];
                        $_SESSION['new_pass'] = $password;
                        $_SESSION['new_user_resume_id'] = $arrUserInfo['jr_id'];

                        # авторизовать нового пользователя
                        $objUser->passAuth($arrUserInfo['jr_email'], $password);

                        header('location: /profile');
                        exit;
                    }

                    unset($password, $arrSqlData);
                }
            }

        } elseif ($objVacancy->isValidLink2Profile($arrReqUri[2])) {
            # произвести автоматическую авторизацию здесь для работодателя!!!
            $arrUserInfo = $objVacancy->getRecordDataByHash($arrReqUri[2]);

            if (is_array($arrUserInfo) && !empty($arrUserInfo)) {
                # есть вакансия, а есть ли пользователь?
                if (isset($arrUserInfo['su_id']) && $arrUserInfo['su_id'] > 0) {
                    # пользователь существует и его создавать не надо
                } else {
                    # необходимо создать пользователя
                    $arrSqlData['type'] = 'jur';
                    $arrSqlData['lname'] = 'Unknown';
                    $arrSqlData['fname'] = 'Unknown';
                    $arrSqlData['mname'] = 'Unknown';

                    if (isset($arrUserInfo['jr_contact_fio']) && !empty($arrUserInfo['jr_contact_fio'])) {
                        $arrFIO = explode(' ', $arrUserInfo['jr_contact_fio']);

                        $arrSqlData['lname'] = (isset($arrFIO[0]) ? $arrFIO[0] : '');
                        $arrSqlData['fname'] = (isset($arrFIO[1]) ? $arrFIO[1] : '');
                        $arrSqlData['mname'] = (isset($arrFIO[2]) ? $arrFIO[2] : '');

                        unset($arrFIO);
                    }

                    $arrSqlData['email'] = $arrUserInfo['jv_email'];
                    $arrSqlData['phone'] = $arrUserInfo['jv_phone'];

                    if (isset($arrUserInfo['jv_city_id']) && intval($arrUserInfo['jv_city_id'])) {
                        $arrSqlData['city_id'] = $arrUserInfo['jv_city_id'];
                    } elseif (isset($arrUserInfo['jv_city']) && !empty($arrUserInfo['jv_city'])) {
                        $arrSqlData['city'] = $arrUserInfo['jv_city'];
                    }

                    $password = $objUtils->getRandomPassw(7);

                    $arrSqlData['password'] = mysql_real_escape_string(trim($password));
                    $arrSqlData['passwordrepeat'] = mysql_real_escape_string(trim($password));

                    if ($objUser->setData($arrSqlData)->save()) {
                        $_SESSION['status'] = 'new_user';
                        $_SESSION['new_login'] = $arrUserInfo['jv_email'];
                        $_SESSION['new_pass'] = $password;
                        $_SESSION['new_user_vacancy_id'] = $arrUserInfo['jv_id'];

                        # авторизовать нового пользователя
                        $objUser->passAuth($arrUserInfo['jv_email'], $password);

                        header('location: /profile');
                        exit;
                    }

                    unset($password, $arrSqlData);
                }
            }
        }

        unset($arrUserInfo);
    }
}


/**
 * Пропустить юзера только если он авторизован
 */
if (is_object($objUser) && $objUser->isAuthed()) {
    $tmp = $objUser->getData();
    $arrIf['fiz'] = $tmp['su_type'] == 'fiz';
    $arrIf['jur'] = $tmp['su_type'] == 'jur';
    unset($tmp);

    /**
     * Сохраняем шаблон письма
     */
    if (isset($_POST['type']) && $_POST['type'] == 'customTemplate' && isset($_POST['templateID']) && intval($_POST['templateID'])) {
        if ($objUser->saveCustomTemplate($_POST['templateID'], $_POST['strTemplate'])) {
            header('Location: /'.$arrTplVars['cfgCurrentUri']); exit;
        }
    }

    /**
     * Удаляем резюме, если запрошено
     */
    if ($_GET['rid'] > 0) {
        $rID = intval($_GET['rid']);

        $isResumeMy = $objResume->checkResumeByUser($rID, $objUser->getId());
        $isResumeMyByEmail = $objResume->checkResumeByEmail($rID, $objUser->getEmail());

        if ($isResumeMy || $isResumeMyByEmail) {
            switch ($_GET['status']) {
                case 'D':
                    $objResume->delete($rID);
                    break;

                case 'N':
                    $objResume->setStatus($rID, 'N');
                    break;

                case 'Y':
                    $objResume->setStatus($rID, 'Y');
                    break;

                default:;
            }

            header('Location: /profile/resumes');
            exit;
        }
    }

    /**
     * Удалить из избранных
     */
    if (isset($_GET['status']) && $_GET['status'] == 'rmf' && $_GET['vid'] > 0) {
        $objUser->removeFavoriteVacancy(intval($_GET['vid']));

        header('Location: /profile/favorites_vacancies');
        exit;
    }

    /**
     * Удалить из черных списков
     */
    if (isset($_GET['status']) && $_GET['status'] == 'rmbl' && $_GET['vid'] > 0) {
        $objUser->removeBlacklistVacancy(intval($_GET['vid']));

        header('Location: /profile/blacklist');
        exit;
    }

    /**
     * Удаляем вакансию, если запрошено
     */
    if ($_GET['vacid'] > 0) {
        $vID = intval($_GET['vacid']);

        $isVacancyMy = $objVacancy->checkVacancyByUser($vID, $objUser->getId());
        if ($isVacancyMy) {
            switch ($_GET['status']) {
                case 'D':
                    $objVacancy->delete($vID);
                    break;

                case 'N':
                    $objVacancy->setStatus($vID, 'N');
                    break;

                case 'Y':
                    $objVacancy->setStatus($vID, 'Y');
                    break;

                default:;
            }

            header('Location: /profile/vacancy');
            exit;
        }
    }
    /**
     * Удалить резюме из избранных
     */
    if (isset($_GET['status']) && $_GET['status'] == 'rmf' && $_GET['resid'] > 0) {
        //die( $_GET['status'].' *** '.$_GET['rid']);
        $objUser->removeFavoriteResume(intval($_GET['resid']));
        header('Location: /profile/favorite_resume');
        exit;
    }
    /**
     * Удалить резюме из черных списков
     */
    if (isset($_GET['status']) && $_GET['status'] == 'rmbl' && $_GET['resid'] > 0) {
        $objUser->removeBlacklistResume(intval($_GET['resid']));
        header('Location: /profile/blacklist_resume');
        exit;
    }

    $arrUserInfo = $objUser->getData();

    $arrTplVars['strLName'] = $arrUserInfo['su_lname'];
    $arrTplVars['strFName'] = $arrUserInfo['su_fname'];
    $arrTplVars['strMName'] = $arrUserInfo['su_mname'];
    $arrTplVars['strEmail'] = $arrUserInfo['su_login'];
    //$arrTplVars['idCity'] = (isset($arrUserInfo['su_city'])? $arrUserInfo['su_city'] : 0 );
    $arrTplVars['numVacancies'] = $objVacancy->getTotalCount();
    $arrIf['no.resumes'] = true;
    $arrIf['no.vacancy'] = true;

    switch ($arrReqUri[1]) {
        case 'subscriptions':
            /**
             * Удаление фильтра
             */
            if ($_GET['status'] == 'rm' && $_GET['svid'] > 0) {
                if ($objUser->deleteVacancySearchFilter($_GET['svid'])) {
                    header('location: /profile/subscriptions'); exit;
                }
            }

            /**
             * Подписки физ.лица
             */
            $arrList = $objUser->getVacanciesSearches();
            if (is_array($arrList) && !empty($arrList)) {
                foreach ($arrList as $k => $vacancySearch) {
                    $arrList[$k]['strParams'] = $objUser->translateVacancySearches( $vacancySearch['sus_params'] );
                }

                $objTpl->tpl_loop("page.content", "list.search.vacancies", $arrList);
                #echo '<pre>';var_dump($arrList);die('<hr />');
            } else {
                $arrIf['no.searches'] = true;
            }
            break;
        case 'subscriptions_resume':
            /**
             * Удаление фильтра
             */
            if ($_GET['status'] == 'rm' && $_GET['srid'] > 0) {
                if ($objUser->deleteResumeSearchFilter($_GET['srid'])) {
                    header('location: /profile/subscriptions_resume'); exit;
                }
            }

            /**
             * Подписки юр.лица
             */
            $arrList = $objUser->getResumeSearches();
            if (is_array($arrList) && !empty($arrList)) {
                foreach ($arrList as $k => $resumeSearch) {
                    $arrList[$k]['strParams'] = $objUser->translateResumeSearches( $resumeSearch['sus_params'] );
                }

                $objTpl->tpl_loop("page.content", "list.search.resume", $arrList);
                #echo '<pre>';var_dump($arrList);die('<hr />');
            } else {
                $arrIf['no.searches'] = true;
            }
            break;
        default: break;
    }

    /**
     * Try to find my resumes
     */
    $strSqlQuery = "SELECT `jr_id`, `jr_title`, `jr_city`, `jr_date_publish`,`jr_status`,`jr_views` FROM `job_resumes` WHERE `jr_su_id` = ".$objUser->getId()." OR `jr_email` = '".mysql_real_escape_string(trim($arrTplVars['strEmail']))."'";
    $arrList = $objDb->fetchall($strSqlQuery);
    if (is_array($arrList) && !empty($arrList)) {
        $arrIf['no.resumes'] = false;
        foreach ($arrList as $k => $arr) {
            $arrList[$k]['date_publish'] = $objUtils->workDate(4, date('Y-m-d H:i:s', $arr['jr_date_publish']));
            $arrList[$k]['strStatus'] = $arr['jr_status']=='Y'?'Опубликовано':'Снято с публикации';
            $arrIf['published.'.$arr['jr_id']] = $arr['jr_status']=='Y';
        }
    }
    $objTpl->tpl_loop("page.content", "list.resumes", $arrList);

    /**
     * Output favorites vacancies
     */
    $arrFavVacs = $objUser->getFavoriteVacancies();
    if (is_array($arrFavVacs) && !empty($arrFavVacs)) {
        foreach ($arrFavVacs as $k => $vacancy) {
            $arrFavVacs[$k]['strDate'] = $objUtils->workDate(1, date('Y-m-d',$vacancy['jv_date_publish']));
            $arrFavVacs[$k]['strSalary'] = $objUtils->workDate(1, date('Y-m-d',$vacancy['jv_date_publish']));
            $arrFavVacs[$k]['strSalary'] = (!empty($vacancy['jv_salary_from']) ? 'от '.$vacancy['jv_salary_from'] : '').(!empty($vacancy['jv_salary_to']) ? (!empty($vacancy['jv_salary_from'])?' до ':'').$vacancy['jv_salary_to'] : '');
            if (!empty($arrFavVacs[$k]['strSalary'])) {
                $arrFavVacs[$k]['strSalary'] .= ' руб';
            } elseif (!empty($vacancy['jv_salary'])) {
                $arrFavVacs[$k]['strSalary'] = $vacancy['jv_salary'];
            }
            $arrFavVacs[$k]['strWhen'] = str_replace(' ', '<br/>', date('d.m.Y H:i', $vacancy['linkCreated']));
        }
    } else $arrIf['no.favorite.vacancies'] = true;
    $objTpl->tpl_loop("page.content", "list.favorite.vacancies", $arrFavVacs);




    /**
     * Чёрный список
     */
    $arrBL = $objUser->getBlacklistVacancies();
    if (is_array($arrBL) && !empty($arrBL)) {
        foreach ($arrBL as $k => $vacancy) {
            $arrBL[$k]['strDate'] = $objUtils->workDate(1, date('Y-m-d',$vacancy['jv_date_publish']));
            $arrBL[$k]['strSalary'] = $objUtils->workDate(1, date('Y-m-d',$vacancy['jv_date_publish']));
            $arrBL[$k]['strSalary'] = (!empty($vacancy['jv_salary_from']) ? 'от '.$vacancy['jv_salary_from'] : '').(!empty($vacancy['jv_salary_to']) ? (!empty($vacancy['jv_salary_from'])?' до ':'').$vacancy['jv_salary_to'] : '');
            if (!empty($arrBL[$k]['strSalary'])) {
                $arrBL[$k]['strSalary'] .= ' руб';
            } elseif (!empty($vacancy['jv_salary'])) {
                $arrBL[$k]['strSalary'] = $vacancy['jv_salary'];
            }
            $arrBL[$k]['strWhen'] = str_replace(' ', '<br/>', date('d.m.Y H:i', $vacancy['linkCreated']));
        }
    } else $arrIf['no.blacklist.vacancies'] = true;
    $objTpl->tpl_loop("page.content", "list.blacklist.vacancies", $arrBL);

    /**
     * Output list of default templates
     */
    $strSqlQuery = "SELECT * FROM `site_users_templates` ORDER BY `sut_id`";
    $arrList = $objDb->fetchall($strSqlQuery);
    if (is_array($arrList) && !empty($arrList)) {
        foreach ($arrList as $k => $arr) {
            $arrCustomTemplate = $objUser->getCustomTemplate($arr['sut_id']);
            if (is_array($arrCustomTemplate) && !empty($arrCustomTemplate)) {
                $arrList[$k]['strTemplateBody'] = nl2br(htmlspecialchars($arrCustomTemplate['suct_body']));
                $arrList[$k]['sut_body'] = htmlspecialchars($arrCustomTemplate['suct_body']);
            } else {
                $arrList[$k]['strTemplateBody'] = nl2br(htmlspecialchars($arr['sut_body']));
                $arrList[$k]['sut_body'] = htmlspecialchars($arr['sut_body']);
            }
        }
    }
    $objTpl->tpl_loop("page.content", "user.templates", $arrList);

    //Избранные резюме
    $arrFavRess = $objUser->getFavoriteResumes();
    if (is_array($arrFavRess) && !empty($arrFavRess)) {
        foreach ($arrFavRess as $k => $resume) {
            $arrFavRess[$k]['strDate'] = $objUtils->workDate(1, date('Y-m-d',$resume['jr_date_publish']));
            $arrFavRess[$k]['strSalary'] = $resume['jr_salary'].' руб' ;
            $arrFavRess[$k]['strWhen'] = str_replace(' ', '<br/>', date('d.m.Y H:i', $resume['jrbl_date']));
        }
    } else $arrIf['no.favorite.resume'] = true;
    $objTpl->tpl_loop("page.content", "list.favorite.resume", $arrFavRess);

//Резюме в черном списке
    $arrBlRess = $objUser->getBlacklistResumes();
    if (is_array($arrBlRess) && !empty($arrBlRess)) {
        foreach ($arrBlRess as $k => $resume) {
            $arrBlRess[$k]['strDate'] = $objUtils->workDate(1, date('Y-m-d',$resume['jr_date_publish']));
            $arrBlRess[$k]['strSalary'] = $resume['jr_salary'].' руб' ;
            $arrBlRess[$k]['strWhen'] = str_replace(' ', '<br/>', date('d.m.Y H:i', $resume['jrbl_date']));
            $arrBlRess[$k]['countViews'] =  (($resume['jr_views']==0)? 'не просматривалось' : $resume['jr_views'] );
        }
    } else $arrIf['no.blacklist.resume'] = true;
    $objTpl->tpl_loop("page.content", "list.blacklist.resume", $arrBlRess);
}

$arrIf['profile'] = true;

if (isset($arrReqUri[1])) {
    unset($arrIf['profile']);
    $arrIf[$arrReqUri[1]] = true;
    switch($arrReqUri[1]) {
        case 'resumes':

            break;

        default:break;
    }
}

// Категории - Сфера деятельности
$strSqlQuery = "SELECT * FROM `job_categories` WHERE `jc_parent` IS NULL ORDER BY `jc_order`";
$arrCats = $objDb->fetchall($strSqlQuery);
if (is_array($arrCats) && !empty($arrCats)) {
    foreach ($arrCats as $k => $arr) {
        $arrCats[$k]['strOptGroupName'] = htmlspecialchars($arr['jc_title']);
        $arrCats[$k]['intGroupID'] = $arr['jc_id'];
    }
}
$objTpl->tpl_loop("page.content", "option.group.category", $arrCats);
$objTpl->tpl_loop("page.content", "option.group.category.informer", $arrCats);
$objTpl->tpl_loop("page.content", "option.group.category.resume", $arrCats);
$objTpl->tpl_loop("page.content", "option.group.category.resume.sent", $arrCats);


if (is_array($arrCats) && !empty($arrCats)) {
    foreach ($arrCats as $k => $arr) {
        $strSqlQuery = "SELECT * FROM `job_categories` WHERE `jc_parent` = ".$arr['jc_id']." ORDER BY `jc_order`";
        $arrSubCats = $objDb->fetchall($strSqlQuery);

        if (is_array($arrSubCats) && !empty($arrSubCats)) {
            foreach ($arrSubCats as $kk => $varr) {
                $arrSubCats[$kk]['intOptionValue'] = intval($varr['jc_id']);
                $arrSubCats[$kk]['strOptionTitle'] = htmlspecialchars($varr['jc_title']);
            }
        }

        $objTpl->tpl_loop("page.content", "group.options.".$arr['jc_id'], $arrSubCats);
        $objTpl->tpl_loop("page.content", "group.options.resume.".$arr['jc_id'], $arrSubCats);
        $objTpl->tpl_loop("page.content", "group.options.resume.sent.".$arr['jc_id'], $arrSubCats);
    }
}

// Тип занятости
$strSqlQuery = "SELECT * FROM `job_workbusy` ORDER BY jw_id";
$arrWorkbusyTypes = $objDb->fetchall($strSqlQuery);
if (is_array($arrWorkbusyTypes) && !empty($arrWorkbusyTypes)) {
    foreach ($arrWorkbusyTypes as $k => $val) {
        $arrWorkbusyTypes[$k]['checked'] = (isset($sbxWorkbusy) && $val['jw_id']==$sbxWorkbusy ? ' selected' : '');
    }
}
$objTpl->tpl_loop("page.content", "workbusy.types", $arrWorkbusyTypes);
$objTpl->tpl_loop("page.content", "workbusy.types.informer", $arrWorkbusyTypes);
$objTpl->tpl_loop("page.content", "workbusy.types.resume", $arrWorkbusyTypes);

// Образование
$strSqlQuery = "SELECT * FROM `job_education_types` ORDER BY jet_id";
$arrEduTypes = $objDb->fetchall($strSqlQuery);
if (is_array($arrEduTypes) && !empty($arrEduTypes)) {
    foreach ($arrEduTypes as $k => $val) {
        $arrEduTypes[$k]['checked'] = (isset($rbEdu) && $val['jet_id']==$rbEdu ? ' checked' : '');
    }
}
$objTpl->tpl_loop("page.content", "education.types", $arrEduTypes);
$objTpl->tpl_loop("page.content", "education.types.resume", $arrEduTypes);




/**
 * Try to find my vacancy
 */
$strSqlQuery = "SELECT `jv_id`, `jv_title`, `jv_city`, `jv_date_publish`,`jv_status`,`jv_views`, IFNULL(`jv_contact_fio`,'') AS cont_fio FROM `job_vacancies` "
                ." WHERE `jv_email` = '".mysql_real_escape_string(trim($arrTplVars['strEmail']))."' ORDER BY  `jv_date_publish` DESC";
$arrList = $objDb->fetchall($strSqlQuery);
if (is_array($arrList) && !empty($arrList)) {
    $arrIf['no.vacancy'] = false;
    foreach ($arrList as $k => $arr) {
        $arrList[$k]['date_publish'] = $objUtils->workDate(4, date('Y-m-d H:i:s', $arr['jv_date_publish']));
        $arrList[$k]['strStatus'] = $arr['jv_status']=='Y'?'Опубликовано':'Снято с публикации';
        $arrIf['published.'.$arr['jv_id']] = $arr['jv_status']=='Y';
    }
}
$objTpl->tpl_loop("page.content", "list.vacancies", $arrList);

// Города
$strSqlQuery = "SELECT * FROM `site_cities` ORDER BY id";
$arrCity = $objDb->fetchall($strSqlQuery);
/*if (is_array($arrCity) && !empty($arrCity)) {
    foreach ($arrCity as $k => $val) {
        $arrCity[$k]['checked'] = (isset($arrTplVars['idCity']) && $val['jet_id']==$arrTplVars['idCity'] ? ' checked' : '');
    }
}*/
$objTpl->tpl_loop("page.content", "city.names", $arrCity);
$objTpl->tpl_loop("page.content", "city.names.informer", $arrCity);
$objTpl->tpl_loop("page.content", "city.names.resume", $arrCity);

// Рекрутеры
$strSqlQuery = "SELECT su_id, CONCAT(IFNULL(su_lname,' '), ' ', IFNULL(su_fname,' '), ' ', IFNULL(su_mname,' ')) as fio FROM `site_users` WHERE su_type = 'fiz' ORDER BY su_id";
$arrRecruits = $objDb->fetchall($strSqlQuery);
$objTpl->tpl_loop("page.content", "recruits", $arrRecruits);

$objTpl->tpl_if("page.content", $arrIf);
$objTpl->tpl_array("page.content", $arrTplVars);
