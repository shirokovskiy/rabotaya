<?php
/** Created by phpWebStudio(c) 2013 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Резюме по отраслям [po-otraslyam-resume] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.content", "po-otraslyam-resume.25.tpl");
include_once "models/JobCategory.php";
/**
 * Список категорий
 */
if (!isset($objJobCategory) && !is_object($objJobCategory)) {
    $objJobCategory = new JobCategory();
}

if (!isset($objResume) && !is_object($objResume)) {
    $objResume = new Resume();
}

$arrCategories = $objJobCategory->getCategories();
if (is_array($arrCategories) && !empty($arrCategories)) {
    foreach ($arrCategories as $key => $arr) {
        $arrCategories[$key]['strMainCategory'] = htmlspecialchars($arr['jc_title']);
        $arrCategories[$key]['intMainCategoryId'] = $arr['jc_id'];
    }

    $objTpl->tpl_loop("page.content", "list.categories", $arrCategories);

    foreach ($arrCategories as $key => $arr) {
        $arrSubCategories = $objJobCategory->getSubCategories($arr['jc_id']);
        if (is_array($arrSubCategories) && !empty($arrSubCategories)) {
            foreach ($arrSubCategories as $k2 => $arr_sub) {
                $arrSubCategories[$k2]['intSubCatId'] = $arr_sub['jc_id'];
                $arrSubCategories[$k2]['intCountResumes'] = (int) $objResume->getCountByCategory($arr_sub['jc_id']);
                $arrSubCategories[$k2]['strSubCatTitle'] = htmlspecialchars($arr_sub['jc_title']);
            }
        }

        $arrIf['is.subcategories.'.$arr['jc_id']] = is_array($arrSubCategories) && !empty($arrSubCategories);
        $objTpl->tpl_loop("page.content", "list.subcategories.".$arr['jc_id'], $arrSubCategories);
    }
}
$objTpl->tpl_array("page.content", $arrTplVars);
