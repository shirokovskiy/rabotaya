<?php /* CMS - PhpWebStudio (Dmitry Shirokovskiy) Copyright 2006.10 - 2013.07 */
///+++ Обработчик фрагмента: Статьи [fragment.sidebar.articles]
$arrTplVars['name.fragment'] = 'fragment.sidebar.articles';
$objTpl->tpl_load($arrTplVars['name.fragment'], "fragment.sidebar.articles.frg");

/**
 * Выборка по статьям
 */
$strSqlQuery = "SELECT * FROM `site_articles` WHERE `sa_status` = 'Y' AND `sa_date_publish` <= NOW() AND sa_type='article' ORDER BY `sa_date_publish` DESC, sa_id DESC LIMIT 5";
$arrList = $objDb->fetchall($strSqlQuery);

if (is_array($arrList) && !empty($arrList)) {
    foreach ($arrList as $key => $value) {
        $arrList[$key]['strArticleTitle'] = htmlspecialchars($value['sa_title']);
        $arrList[$key]['strArticleDate'] = $objUtils->workDate(8, $value['sa_date_publish']);
    }
}
$objTpl->tpl_loop($arrTplVars['name.fragment'], "sidebar.list.articles", $arrList);
unset($arrList);

/**
 * Выборка по словарю
 */
$strSqlQuery = "SELECT * FROM `site_articles` WHERE `sa_status` = 'Y' AND sa_type='dict' ORDER BY `sa_date_publish` DESC, sa_id DESC LIMIT 5";
$arrList = $objDb->fetchall($strSqlQuery);

if (is_array($arrList) && !empty($arrList)) {
    foreach ($arrList as $key => $value) {
        $arrList[$key]['strArticleTitle'] = htmlspecialchars($value['sa_title']);
        $arrList[$key]['strArticleDate'] = $objUtils->workDate(8, $value['sa_date_publish']);
    }
}
$objTpl->tpl_loop($arrTplVars['name.fragment'], "sidebar.list.dictionary", $arrList);
unset($arrList);

$objTpl->tpl_array($arrTplVars['name.fragment'], $arrTplVars);
