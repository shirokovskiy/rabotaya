<?php /* CMS - PhpWebStudio (Dmitry Shirokovskiy) Copyright 2006.10 - 2013.10 */
///+++ Обработчик фрагмента: События [fragment.sidebar.events]
$arrTplVars['name.fragment'] = 'fragment.sidebar.events';
$objTpl->tpl_load($arrTplVars['name.fragment'], "fragment.sidebar.events.frg");

/**
 * Выборка новостей
 */
$strSqlQuery = "SELECT * FROM `site_events` WHERE `se_status` = 'Y' AND `se_date_publ` <= DATE_FORMAT(NOW(), '%Y-%m-%d') ORDER BY `se_date_publ` DESC, se_id DESC LIMIT 5";
$arrList = $objDb->fetchall($strSqlQuery);

if (is_array($arrList) && !empty($arrList)) {
    foreach ($arrList as $key => $value) {
        $arrList[$key]['strTitle'] = htmlspecialchars($value['se_title']);
        $arrList[$key]['strDate'] = $objUtils->workDate(8, $value['se_date_publ']);
        $arrList[$key]['strLink'] = $value['se_url'];
    }
}
$objTpl->tpl_loop($arrTplVars['name.fragment'], "sidebar.list.events", $arrList);
$objTpl->tpl_array($arrTplVars['name.fragment'], $arrTplVars);
