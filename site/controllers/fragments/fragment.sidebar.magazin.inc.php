<?php /* CMS - PhpWebStudio (Dmitry Shirokovskiy) Copyright 2006.10 - 2013.07 */
///+++ Обработчик фрагмента: Блок распространения Журнала [fragment.sidebar.magazin]
$arrTplVars['name.fragment'] = 'fragment.sidebar.magazin';
$objTpl->tpl_load($arrTplVars['name.fragment'], "fragment.sidebar.magazin.frg");

$arrDays = array('tu','we','th');

foreach ($arrDays as $day) {
    // Взять данные за день недели
    $strSqlQuery = "SELECT jm_url, jm_year, jm_week FROM `job_magazines` WHERE `jm_dow` = '".$day."' AND `jm_url` > 0 ORDER BY `jm_id` DESC LIMIT 1";
    $arrInfo = $objDb->fetch($strSqlQuery);

    $arrTplVars['strUrlMagazine_'.$day] = 'http://issuu.com/rabotayaru/docs/'.$arrInfo['jm_url'];
    $arrTplVars['intWeekMagazine_'.$day] = $arrInfo['jm_week'];
    $arrTplVars['intYearMagazine_'.$day] = $arrInfo['jm_year'];
}

$arrTplVars['strUrlPathDistribution'] = SITE_URL.'storage/siteimg/content/distribution-full.png';
$strImageName = 'distribution-full';
$ext = null;
if (file_exists(PRJ_IMAGES.'zone/'.$strImageName.'.jpg')) {
    $ext = 'jpg';
} elseif (file_exists(PRJ_IMAGES.'zone/'.$strImageName.'.png')) {
    $ext = 'png';
}
if (!empty($ext)) { // File exist
    $arrTplVars['strUrlPathDistribution'] = $arrTplVars['cfgAllImg'].'zone/'.$strImageName.'.'.$ext.'?q='.rand(1,10000);
}

$objTpl->tpl_array($arrTplVars['name.fragment'], $arrTplVars);
