<?php /* CMS - PhpWebStudio (Dmitry Shirokovskiy) Copyright 2006.10 - 2013.07 */
///+++ Обработчик фрагмента: Блок "Работа в других городах" [fragment.sidebar.job.in.cities]
$arrTplVars['name.fragment'] = 'fragment.sidebar.job.in.cities';
$objTpl->tpl_load($arrTplVars['name.fragment'], "fragment.sidebar.job.in.cities.frg");

$strSqlQuery = "SELECT COUNT(`jv_city_id`) AS `citiesMax`, `jv_city_id`, `city` AS `strCity` FROM `job_vacancies` LEFT JOIN `site_cities` ON (`jv_city_id` = `id`) WHERE `jv_city_id` > 0 AND `jv_city_id` NOT IN (1,3) AND `country_id` > 0 AND `city` NOT LIKE '%область%' AND `city` NOT LIKE '%край%' AND `city` NOT REGEXP ',' AND `city` NOT REGEXP '[.]' GROUP BY `jv_city_id` ORDER BY `citiesMax` DESC LIMIT 50";
$arrList = $objDb->fetchall($strSqlQuery);

if (is_array($arrList) && !empty($arrList)) {
    foreach ($arrList as &$arr) {
        $arr['strCityUrl'] = urlencode($arr['strCity']);
    }
    $arrIf['is.list'] = true;
}

$objTpl->tpl_loop($arrTplVars['name.fragment'], "list", $arrList);
$objTpl->tpl_if($arrTplVars['name.fragment'], $arrIf);
$objTpl->tpl_array($arrTplVars['name.fragment'], $arrTplVars);
