<?php /* CMS - PhpWebStudio (Dmitry Shirokovskiy) Copyright 2006.10 - 2013.07 */
///+++ Обработчик фрагмента: Рекламное место под навигацией [fragment.top.banner]
$arrTplVars['name.fragment'] = 'fragment.top.banner';
$objTpl->tpl_load($arrTplVars['name.fragment'], "fragment.top.banner.frg");

/**
 * Покажем верхний баннер
 */
$strSqlQuery = "SELECT * FROM site_images WHERE si_status = 'Y' AND si_type = 'banner' AND si_banner_type = 'top' AND si_project = ".SITE_ID_PROJECT." ORDER BY si_id DESC";
$arrTopBanners = $objDb->fetchall( $strSqlQuery );

if ( is_array( $arrTopBanners ) && !empty( $arrTopBanners ) ) {
    if(count($arrTopBanners) == 1) {
        $arrIf['show.top.banner'] = true;

        $arrTopBanner = $arrTopBanners[0];

        if ( empty( $arrTopBanner['si_banner_content'] ) ) {
            $arrTplVars['contentTopBanner'] = "<a href='http://{$arrTopBanner['si_url']}' target='_blank'><img src='{$arrTplVars['cfgAllImg']}banners/".$arrTopBanner['si_filename']."' border=0 title='".$arrTopBanner['si_desc']."' /></a>";
        } else {
            $arrTplVars['contentTopBanner'] = $arrTopBanner['si_banner_content'];
        }
    } elseif (count($arrTopBanners) > 1) {
        $arrIf['show.top.banners'] = true;

        foreach ($arrTopBanners as $key => $value) {
            $arrTopBanners[$key]['strUrl'] = "http://".$value['si_url'];
        }
    }
} else {
    $arrIf['show.top.banner.dummy'] = true;
}
$objTpl->tpl_loop($arrTplVars['name.fragment'], "top.banners", $arrTopBanners);
$objTpl->tpl_loop($arrTplVars['name.fragment'], "top.banners.tabs", $arrTopBanners);
$objTpl->tpl_array($arrTplVars['name.fragment'], $arrTplVars);
$objTpl->tpl_if($arrTplVars['name.fragment'], $arrIf);
