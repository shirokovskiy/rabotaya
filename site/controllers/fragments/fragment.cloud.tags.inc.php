<?php /* CMS - PhpWebStudio (Dmitry Shirokovskiy) Copyright 2006.10 - 2013.08 */
///+++ Обработчик фрагмента: Облако тэгов [fragment.cloud.tags]
$arrTplVars['name.fragment'] = 'fragment.cloud.tags';
$objTpl->tpl_load($arrTplVars['name.fragment'], "fragment.cloud.tags.frg");

$arrIf['index.seo.text'] = in_array($arrReqUri[0], array('index', '')) || !isset($arrReqUri[0]);

// Выборка вакансий для облака тэгов
$strSqlQuery = "SELECT * FROM (SELECT @cnt := COUNT( * ) +1, @lim :=10 FROM `job_vacancies`) vars STRAIGHT_JOIN (SELECT jv_id, `jv_title`, `jv_salary_to`, @lim := @lim -1 FROM `job_vacancies` WHERE (@cnt := @cnt -1) AND RAND( UNIX_TIMESTAMP() ) < @lim / @cnt) i";
$arrList = $objDb->fetchall($strSqlQuery);
if (is_array($arrList) && !empty($arrList)) {
    foreach ($arrList as $k => $val) {
        $arrList[$k]['strTitle'] = $val['jv_title'].(!empty($val['jv_salary_to']) ? ' '.$val['jv_salary_to'].'р.' : '');
        $arrList[$k]['strLink'] = SITE_URL.'vacancy/'.$val['jv_id'];
    }
}
$objTpl->tpl_loop($arrTplVars['name.fragment'], "list.vacancies.in.cloud", $arrList); unset($arrList);


// Выборка ведущих компаний
$strSqlQuery = "SELECT jc_id, jc_title, si_filename FROM `job_companies` LEFT JOIN `site_images` ON (si_type = 'company' AND si_rel_id = jc_id) WHERE `jc_top` = 'Y' AND `jc_status` = 'Y'";
$arrComps = $objDb->fetchall($strSqlQuery);
$arrIf['a.lot.of.companies'] = false;
if (is_array($arrComps) && !empty($arrComps)) {
    $arrIf['a.lot.of.companies'] = count($arrComps) > 8;
    foreach ($arrComps as $key => $value) {
        $arrComps[$key]['jc_title'] = htmlspecialchars($value['jc_title'], ENT_QUOTES);
        if (empty($value['si_filename']) || !file_exists(PRJ_IMAGES.'companies/'.$value['si_filename'])) {
            unset($arrComps[$key]);
        }
    }
}
$objTpl->tpl_loop($arrTplVars['name.fragment'], "list.top.companies", $arrComps); unset($arrComps);

/*
 * http://www.goat1000.com/tagcanvas-options.php
 *
 * Случайная выборка из БД
 *
 * SELECT name
  FROM random AS r1 JOIN
(SELECT (RAND() *
(SELECT MAX(id)
                        FROM random)) AS id)
        AS r2
 WHERE r1.id >= r2.id
 ORDER BY r1.id ASC
 LIMIT 1*/

$objTpl->tpl_array($arrTplVars['name.fragment'], $arrTplVars);
$objTpl->tpl_if($arrTplVars['name.fragment'], $arrIf);
