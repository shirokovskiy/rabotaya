<?php /* CMS - PhpWebStudio (Dmitry Shirokovskiy) Copyright 2006.10 - 2015.01 */
///+++ Обработчик фрагмента: Рекламное место в правой колонке, верх [fragment.sidebar.right.top.banner]
$arrTplVars['name.fragment'] = 'fragment.sidebar.right.top.banner';
$objTpl->tpl_load($arrTplVars['name.fragment'], "fragment.sidebar.right.top.banner.frg");

/**
 * Покажем правый верхний баннер
 */
$strSqlQuery = "SELECT * FROM site_images WHERE si_status = 'Y' AND si_type = 'banner' AND si_banner_type = 'right' AND si_project = ".SITE_ID_PROJECT." ORDER BY si_id DESC";
$arrRightBanners = $objDb->fetchall( $strSqlQuery );

if ( is_array( $arrRightBanners ) && !empty( $arrRightBanners ) ) {
    if(count($arrRightBanners) == 1) {
        $arrIf['show.right.banner'] = true;

        $arrRightBanner = $arrRightBanners[0];

        if ( empty( $arrRightBanner['si_banner_content'] ) ) {
            $arrTplVars['contentRightBanner'] = "<a href='http://{$arrRightBanner['si_url']}' target='_blank'><img src='{$arrTplVars['cfgAllImg']}banners/".$arrRightBanner['si_filename']."' border=0 title='".$arrRightBanner['si_desc']."' /></a>";
        } else {
            $arrTplVars['contentRightBanner'] = $arrRightBanner['si_banner_content'];
        }
    } elseif (count($arrRightBanners) > 1) {
        $arrIf['show.right.banners'] = true;

        foreach ($arrRightBanners as $key => $value) {
            $arrRightBanners[$key]['strUrl'] = "http://".$value['si_url'];
        }
    }
} else {
    $arrIf['show.right.banner.dummy'] = true;
}
$objTpl->tpl_loop($arrTplVars['name.fragment'], "right.banners", $arrRightBanners);
$objTpl->tpl_loop($arrTplVars['name.fragment'], "right.banners.tabs", $arrRightBanners);
$objTpl->tpl_array($arrTplVars['name.fragment'], $arrTplVars);
$objTpl->tpl_if($arrTplVars['name.fragment'], $arrIf);
