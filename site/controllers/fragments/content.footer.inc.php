<?php /* CMS - PhpWebStudio (Dmitry Shirokovskiy) Copyright 2006.10 - 2013.08 */
///+++ Обработчик фрагмента: Нижний блок страниц сайта [content.footer]
$arrTplVars['name.fragment'] = 'content.footer';
$objTpl->tpl_load($arrTplVars['name.fragment'], "content.footer.frg");
$objTpl->tpl_array($arrTplVars['name.fragment'], $arrTplVars);
$objTpl->tpl_if($arrTplVars['name.fragment'], $arrIf);
