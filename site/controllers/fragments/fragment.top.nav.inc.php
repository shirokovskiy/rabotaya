<?php /* CMS - PhpWebStudio (Dmitry Shirokovskiy) Copyright 2006.10 - 2013.07 */
include_once "models/Resume.php";
include_once "models/Vacancy.php";
include_once "models/SiteUser.php";
include_once "GeoData/GeoIP/geoip.inc";
include_once "GeoData/GeoIP/geoipcity.inc";
include_once "GeoData/GeoIP/geoipregionvars.php";
///+++ Обработчик фрагмента: Навигация по сайту [fragment.top.nav]
$arrTplVars['name.fragment'] = 'fragment.top.nav';
$objTpl->tpl_load($arrTplVars['name.fragment'], "fragment.top.nav.frg");

/**
 * Проверить если JavaScript не включен и вывести предупреждение
 */

$arrTplVars['active_color'] = 'color1';
$arrTplVars['preSelectedExtension'] = '';

if($arrReqUri[0] == 'form_add_resume')
    $arrTplVars['active_color'] = 'color3';
if($arrReqUri[0] == 'form_add_vacancy')
    $arrTplVars['active_color'] = 'color4';

if((isset($_POST['type']) && $_POST['type']=='resumes')
    || $arrReqUri[0] == 'adr-search'
    || $arrReqUri[0] == 'po-otraslyam-resume'
    || $arrReqUri[0] == 'po-professiyam-resume'
    || $arrReqUri[0] == 'resume'
) {
    $arrTplVars['active_color'] = 'color2';
}

if ($arrReqUri[0] == 'po-otraslyam' || $arrReqUri[0] == 'po-otraslyam-resume') {

    $arrTplVars['preSelectedExtension'] = 'categories_selection_trigger';

} elseif ($arrReqUri[0] == 'po-kompaniyam') {

    $arrTplVars['preSelectedExtension'] = 'companies_selection_trigger';

} elseif ($arrReqUri[0] == 'po-professiyam' || $arrReqUri[0] == 'po-professiyam-resume') {

    $arrTplVars['preSelectedExtension'] = 'profession_selection_trigger';

} elseif ($arrReqUri[0] == 'adr-search' || $arrReqUri[0] == 'adv-search') {

    $arrTplVars['preSelectedExtension'] = 'extended_search_trigger';
}

if (!isset($_POST['type'])) {
    $arrTplVars['type'] = 'vacancies';
} else {
    $arrTplVars['type'] = $_POST['type'];
}

$objVacancy = new Vacancy();
$arrTplVars['strTotalCountVacancies'] = $objVacancy->getTotalCount();

if($_subdomen == 'spb') {
    $arrIf['spb'] = true;
}

$objResume = new Resume();
$arrTplVars['strTotalCountResumes'] = $objResume->getTotalCount();

if (!isset($objSiteUser) || !is_object($objSiteUser)) {
    $objSiteUser = new SiteUser();
}

//unset($_SESSION['myCityId'], $_SESSION['myCity']);

/**
 * Geo-Location
 */
if (!isset($_SESSION['myCityId'])) {
    $myIP = $_SERVER['REMOTE_ADDR']=='127.0.0.1' ? '188.134.80.143' : $_SERVER['REMOTE_ADDR'];
    $myIP = $_SERVER['REMOTE_ADDR']=='127.0.0.1' ? '66.249.78.29' : $_SERVER['REMOTE_ADDR']; // Beverly Hills
//    $myIP = $_SERVER['REMOTE_ADDR']=='127.0.0.1' ? '208.115.111.75' : $_SERVER['REMOTE_ADDR'];
    $objGeoIP = geoip_open(GEOIPCITY_DAT, GEOIP_STANDARD);
    $rcGeoIP = geoip_record_by_addr($objGeoIP, $myIP);
    if ( !is_null($rcGeoIP) ) {
        $intCityId = $objSiteUser->saveIPCityCountry($myIP, $rcGeoIP);
        if ($intCityId > 0) {
            $_SESSION['myCityId'] = $intCityId;
            $city = $objSiteUser->getCityById($intCityId);
            $_SESSION['myCity'] = $city['city']; // get Russian name (city_eng is English name)
            unset($city);
        }
    }

    geoip_close($objGeoIP);
}

/**
 * Выбор регионов (городов)
 */
$strSqlQuery = "SELECT COUNT(`jv_city_id`) AS `citiesMax`, `jv_city_id` AS `cityID`, `city` AS `strCity` FROM `job_vacancies` LEFT JOIN `site_cities` ON (`jv_city_id` = `id`) WHERE `jv_city_id` > 0 AND `jv_city_id` NOT IN (1,3) AND `country_id` > 0 AND `city` NOT LIKE '%область%' AND `city` NOT LIKE '%край%' AND `city` NOT REGEXP ',' AND `city` NOT REGEXP '[.]' GROUP BY `jv_city_id` ORDER BY `citiesMax` DESC LIMIT 40";
$arrList = $objDb->fetchall($strSqlQuery);

if (is_array($arrList) && !empty($arrList)) {
    foreach ($arrList as $k => $arr) {
        $arrCityList[$arr['cityID']]['strCityUrl'] = urlencode($arr['strCity']);
        $arrCityList[$arr['cityID']]['citiesMax'] = $arr['citiesMax'];
        $arrCityList[$arr['cityID']]['strCity'] = $arr['strCity'];
    }
}

$strSqlQuery = "SELECT COUNT(`jr_city_id`) AS `citiesMax`, `jr_city_id` AS `cityID`, `city` AS `strCity` FROM `job_resumes` LEFT JOIN `site_cities` ON (`jr_city_id` = `id`) WHERE `jr_city_id` > 0 AND `jr_city_id` NOT IN (1,3) AND `country_id` > 0 AND `city` NOT LIKE '%область%' AND `city` NOT LIKE '%край%' AND `city` NOT REGEXP ',' AND `city` NOT REGEXP '[.]' GROUP BY `jr_city_id` ORDER BY `citiesMax` DESC LIMIT 40";
$arrList = $objDb->fetchall($strSqlQuery);

//$arrList = array_merge($arrList, $arrList2);
//$arrList = array_unique($arrList);

if (is_array($arrList) && !empty($arrList)) {
    foreach ($arrList as $k => $arr) {
        $arrCityList[$arr['cityID']]['strCityUrl'] = urlencode($arr['strCity']);
        $arrCityList[$arr['cityID']]['citiesMax'] += $arr['citiesMax'];
        $arrCityList[$arr['cityID']]['strCity'] = $arr['strCity'];
    }
}
$sorted = array();
if (is_array($arrCityList) && !empty($arrCityList)){
    foreach ($arrCityList as $cityID => $arr) {
        if ($arr['citiesMax'] > 3) {
            $sorted[$cityID] = $arr['citiesMax'];
        } else {
            unset($arrCityList[$cityID]);
        }
    }
    array_multisort($sorted, SORT_DESC, $arrCityList);
}
//echo '<pre>';
//print_r(count($arrList));
//print_r($arrCityList);
//die('</pre><br />' . __FILE__ . ':' . __LINE__);

$objTpl->tpl_loop($arrTplVars['name.fragment'], "list.cities", $arrCityList);

//echo '<pre>';
//print_r($_SESSION);
//die('<hr />');

//if (isset($_SESSION['myCityId'])) {
//    $arrCity = $objSiteUser->getCityById($_SESSION['myCityId']);
//    if (is_array($arrCity) && !empty($arrCity)) {
//        $arrTplVars['strMyCity'] = $arrCity['city'];
//    }
//}

/**
 * Get Informer data
 */
include_once "models/Informer.php";
$objInformer = new Informer();

$arr = $objInformer->getInformer('w_max');
$arrTplVars['strWeatherMax'] = (int)$arr['si_value'];
$arr = $objInformer->getInformer('w_min');
$arrTplVars['strWeatherMin'] = (int)$arr['si_value'];
$arr = $objInformer->getInformer('eur');
$arrTplVars['strInformerDate'] = date('d.m.y', strtotime($arr['si_date']));
$arrTplVars['strCurrencyEur'] = $arr['si_value'];
$arr = $objInformer->getInformer('usd');
$arrTplVars['strCurrencyUsd'] = $arr['si_value'];
unset($arr);

if (empty($arrTplVars['strMyCity'])) {
    if (!empty($_SESSION['myCity'])) {
        $arrTplVars['strMyCity'] = $_SESSION['myCity'];
    } else
        $arrTplVars['strMyCity'] = "Санкт-Петербург";
}

$arrTplVars['isUnknownRegion'] = 'false'; // it means region is known
if ($arrTplVars['strMyCity'] != "Санкт-Петербург" && $arrTplVars['strMyCity'] != "Москва" && !$_SESSION['isCityConfirmed'] && !isset($arrReqUri[1])) {
    $arrTplVars['isUnknownRegion'] = 'true';
    $arrTplVars['strDetectedRegionName'] = $arrTplVars['strMyCity'];
}

$arrProfessions[] = 'Менеджер';
$arrProfessions[] = 'Программист';
$arrProfessions[] = 'Водитель';
$arrProfessions[] = 'Оператор ПК';
$arrProfessions[] = 'Брокер';
$arrProfessions[] = 'Секретарь';
$arrProfessions[] = 'Охранник';
$arrProfessions[] = 'Слесарь';
$arrProfessions[] = 'Реставратор';
$arrProfessions[] = 'Швея';
$arrProfessions[] = 'Художник-оформитель';
$arrProfessions[] = 'Дизайнер';
$arrProfessions[] = 'Администратор';
$arrProfessions[] = 'Сторож';
$arrProfessions[] = 'Инспектор';

$key = array_rand($arrProfessions);
$arrTplVars['strRandomProfession'] = $arrProfessions[$key];
unset($key);

$arrTplVars['rand'] = rand(10000, 100000000);

$objTpl->tpl_if($arrTplVars['name.fragment'], $arrIf);
$objTpl->tpl_array($arrTplVars['name.fragment'], $arrTplVars);
