<div class="paginator">
    <div class="inl legend">
        <span>Всего записей: <b>{intQuantitySelectRecords}</b>| Показано записей: <b>{intQuantityShowRecOnPage}</b>|</span>
    </div>
    <div class="inl pages">
        <span>Страницы:</span>
<if pa.prev.arrow.pages>
    <a href="{strLinkBack}"><img src="{cfgAdminImg}{strNameImageBack}" border="0" alt="Назад" hspace="2" vspace="3"></a>
</if pa.prev.arrow.pages>
<loop pa.lst.pages>
    {strLink}
</loop pa.lst.pages>
<if pa.next.arrow.pages>
    <a href="{strLinkForward}"><img src="{cfgAdminImg}{strNameImageForward}" border="0" alt="Вперёд" hspace="2" vspace="3"></a>
</if pa.next.arrow.pages>
    </div>
</div>