<frg page.header>

<div id="page_wrapper">
<frg fragment.top.nav>
<div class="content_columns clearer">
    <div class="left_sidebar">
        <frg fragment.sidebar.magazin>
        <frg fragment.sidebar.news>
    </div><!-- END LEFT SIDEBAR -->
    <div class="content_block">
        <tpl page.content>
    </div>
</div>
<frg fragment.cloud.tags>
</div><!-- END OF WRAPPER -->

<frg content.footer>
<frg page.footer>