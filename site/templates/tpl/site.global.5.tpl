<frg page.header>

<div id="page_wrapper" class="col-2r">
<frg fragment.top.nav>
<div class="content_columns">
    <div class="content_block">
        <tpl page.content>
    </div>

    <div class="right_sidebar">
        <frg fragment.sidebar.events>
        <frg fragment.sidebar.articles>
        <frg fragment.sidebar.job.in.cities>
    </div><!-- END RIGHT SIDEBAR -->
</div>

<frg fragment.cloud.tags>
</div><!-- END OF WRAPPER -->

<frg content.footer>
<frg page.footer>