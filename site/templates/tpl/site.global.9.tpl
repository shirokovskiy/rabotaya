<frg page.header>

<div id="page_wrapper" class="col-1">
    <frg fragment.top.nav>
    <div class="content_columns clearer">
        <div class="content_block">
            <tpl page.content>
        </div>
    </div>

    <frg fragment.cloud.tags>
</div>

<frg content.footer>
<frg page.footer>