<div id="job-in-cities" class="sidebar_block" style="display: none">
    <h3>Работа в других городах</h3>
<if is.list>
    <ul>
<loop list>
        <li><a href="{cfgSiteUrl}search/city/{strCityUrl}">работа в г.{strCity} <span>({citiesMax})</span></a></li>
</loop list>
    </ul>
</if is.list>

    <p><a href="#" class="link-darkred" id="expand-cities-list">раскрыть список городов</a></p>
</div>

<script type="text/javascript">
jQuery(document).ready(function ($) {
    var citiesBoxHeight = $('#job-in-cities').height();
    var delta = 55;
    var H3h = $('#job-in-cities h3').height();
    var Ah = $('#expand-cities-list').height();
    var linkStringI = 'раскрыть список городов';
    var linkStringD = 'скрыть список городов';

    $('#expand-cities-list').toggle(function(){
        // increase
        var hh = citiesBoxHeight;

        $('#job-in-cities').animate({
            'height' : hh + 'px'
        }, 500);
        $('#job-in-cities ul').css({
            'height' : (hh - delta) + 'px'
        });
        $(this).html(linkStringD);
//        console.log('height 1 =' + $('#job-in-cities').height());

    }, function(){
        // decrease
        var hh = delta + H3h + Ah + 56;

        $('#job-in-cities').animate({
            'height' : hh + 'px'
        }, 500);
        $('#job-in-cities ul').css({
            'height' : (hh - delta) + 'px'
        });
        $(this).html(linkStringI);
//        console.log('height 2 =' + $('#job-in-cities').height());
    });

    $('#job-in-cities').css({'height' : '85px', 'display' : 'block'});
    $('#job-in-cities ul').css({'height' : '55px'});
});
</script>
