<div class="hidden">
    <div id="vAgreement" class="block_agreement">
        <h2>Правила размещения вакансий <br/>
            портала Rabota-Ya.ru (Работа от А до Я)</h2>

        <ul>
            <li>На Портале Rabota-Ya.ru производится строгий контроль качества размещаемых объявлений о вакансиях. Контроль производится как автоматически, с помощью специальных фильтров, так и вручную, модераторами.</li>
            <li>1. К публикации НЕ допускаются объявления следующего вида:</li>
            <li>1.1 содержащие нецензурную лексику;</li>
            <li>1.2 не являющиеся предложениями работы;</li>
            <li>1.3 предлагающие участие в различного рода финансовых пирамидах, сетевом маркетинге, а также других организациях, предлагающих заработок исключительно за счет получения процентов; объявления, содержащие предложение стать бизнес-партнером также не допускаются;</li>
            <li>1.4 предлагающие для записи на собеседование платную отправку SMS или платный звонок менеджеру организации;</li>
            <li>1.5 предлагающие работу в сети Интернет и на дому сомнительного характера;</li>
            <li>1.6 предлагающие перейти на какой-либо сайт или написать на какой-либо e-mail для получения информации о вакансии;</li>
            <li>1.7 подразумевающие оказание интимных услуг;</li>
            <li>1.8 содержащие рекламу, в том числе скрытую;</li>
            <li>1.9 содержащие ссылки, не имеющие отношения к организации-работодателю;</li>
            <li>1.10 противоречащие ст. 4 ФЗ «О средствах массовой информации» «Недопустимость злоупотребления свободой массовой информации» («…не допускается использование средств массовой информации в целях совершения уголовно наказуемых деяний, для разглашения сведений, составляющих государственную или иную специально охраняемую законом тайну, для распространения материалов, содержащих публичные призывы к осуществлению террористической деятельности или публично оправдывающих терроризм, других экстремистских материалов, а также материалов, пропагандирующих порнографию, культ насилия и жестокости...»);</li>
            <li>1.11 требующие заключения договора обязательного пенсионного страхования с каким-либо НПФ для устройства на работу или для бесплатного оказания услуг по содействию в трудоустройстве.</li>
            <li>1.12 Администрация Портала оставляет за собой право отказать в публикации вакансии без объяснения причин.</li>
            <li>2. Объявление о вакансии должно содержать в себе предложение работы и соответствовать следующим требованиям:</li>
            <li>2.1 объявление о вакансии должно содержать в себе предложение одной конкретной позиции;</li>
            <li>2.2 объявление должно быть размещено в соответствующих вакансии специализациях, объявление не может быть одновременно размещено более чем в 5 специализациях;</li>
            <li>2.3 текст объявления должен быть составлен на русском или английском языке, не допускаются объявления, написанные прописными буквами, транслитерацией и другими способами, затрудняющими чтение;</li>
            <li>2.4 Каждое поле должно быть заполнено в соответствии с его названием («условия работы», «должностные обязанности», «профессиональные навыки» и т.д.), не допускается дублирование одного и того же текста во все поля формы вакансии;</li>
            <li>2.5 в заголовке вакансии должно содержаться только название одной конкретной должности (в единственном числе, именительном падеже), другой информации в заголовке быть не должно;</li>
            <li>2.6 контактные данные работодателя должны быть указаны только в специально отведенных для этого полях.</li>
            <li>2.7 запрещено указывать в вакансии ограничения по полу / возрасту кандидатов и иным признакам, не связанным с деловыми качествами работников.</li>
            <li>3. Объявление о вакансии может быть размещено бессрочно, но при этом может быть снято работодателем в любое время по его желанию.</li>
            <li>4. Объявление о вакансии может быть обновлено для поднятия его в выдаче не чаще одного раза в неделю. Запрещается удалять вакансию и создавать заново такую же с целью поднятия её в результатах выдачи.</li>
            <li>5. Одновременное размещение копий одной и той же вакансии расценивается как спам.</li>
            <li>6. В случае обнаружения на Портале опубликованной вакансии, не соответствующей Правилам размещения, любой пользователь имеет право отправить жалобу модераторам Портала, нажав на соответствующую ссылку на странице вакансии.</li>
            <li>7. Объявления о вакансих, не соответстующие данным правилам, будут удаляться с Портала.</li>
            <li>8. Администрация Портала оставляет за собой право в одностороннем порядке, без согласования с кем-либо, принимать решения о публикации тех или иных вакансий и другой информации.</li>
            <li>9. Администрация Портала не несет ответственности за содержащуюся в вакансиях информацию.</li>
        </ul>
    </div>
</div>