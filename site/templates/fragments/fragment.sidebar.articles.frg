<div id="sidebar-dictionary" class="sidebar_block">
    <a href="{cfgSiteUrl}dictionary"><h3>СЛОВАРЬ ПРОФЕССИЙ</h3></a>

    <loop sidebar.list.dictionary>
        <div class="row">
            <a href="{strSiteUrl}dictionary/{sa_id}">{sa_title}</a>
        </div>
    </loop sidebar.list.dictionary>

    <div class="block">
        <a href="{cfgSiteUrl}dictionary"><i>Весь словарь</i></a>
    </div>
</div>