<if show.right.banner>
    <!-- RIGHT BANNER-->
    <div id="right-banner">
        {contentRightBanner}
    </div>
    <!-- // RIGHT BANNER-->
</if show.right.banner>


<if show.right.banners>
    <!-- RIGHT BANNERS -->
    <ul id="right_banner" class="jcarousel-skin-raya">
        <loop right.banners>
            <li><a href="{strUrl}" target="_blank"><img src="{cfgAllImg}banners/{si_filename}" alt="{si_desc}" border="0"/></a></li>
        </loop right.banners>
    </ul>
    <!-- // RIGHT BANNERS -->
</if show.right.banners>
