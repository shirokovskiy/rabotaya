<!DOCTYPE html>
<html>
<head>
    <title>{m_title}</title>
    <meta http-equiv="Content-Type" content="text/html; charset={METACHARSET}" />
    <meta name="keywords" content="{m_keywords}" />
    <meta name="description" content="{m_description}" />
    <meta name="distribution" content="global" />
    <meta name="robots" content="all" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta name="classification" content="Auto" />
    <meta name="Document-state" content="Dynamic" />
    <meta name="author" content="Dimitry Shirokovskiy, phpwebstudio.com" />
    <link rel="icon" href="{cfgSiteImg}favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="{cfgSiteImg}favicon.ico" type="image/x-icon" />
    <script type="text/javascript" src="{cfgSiteUrl}ext/js/jq.js"></script>
    <script type="text/javascript" src="{cfgSiteUrl}ext/js/jq.mw.js"></script>
    <script type="text/javascript" src="{cfgSiteUrl}ext/js/autocolumn.min.js"></script>
    <script type="text/javascript" src="{cfgSiteUrl}ext/js/jq.ui.js"></script>
    <script type="text/javascript" src="{cfgSiteUrl}ext/js/jq.ui.ru.js"></script>
    <script type="text/javascript" src="{cfgSiteUrl}ext/js/jq.jcarousel.js"></script>
    <script type="text/javascript" src="{cfgSiteUrl}ext/js/gui.js"></script>
    <script type="text/javascript" src="{cfgSiteUrl}ext/js/jq.jqz.js"></script>
    <script type="text/javascript" src="{cfgSiteUrl}ext/js/jq.z.js"></script>

    <link rel="stylesheet" type="text/css" href="{cfgSiteUrl}ext/css/general.css" />
    <link rel="stylesheet" type="text/css" href="{cfgSiteUrl}ext/css/ui/custom.css" />
    <link rel="stylesheet" type="text/css" href="{cfgSiteUrl}ext/css/jq.tools.css" />
    <link rel="stylesheet" type="text/css" href="{cfgSiteUrl}ext/css/jq.fb.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="{cfgSiteUrl}ext/css/jq.jqz.css" />
    <!--[if IE]>
    <link rel="stylesheet" type="text/css" href="{cfgSiteUrl}ext/css/win-os.css" />
    <![endif]-->

    <script type="text/javascript" src="{cfgSiteUrl}ext/js/jq.fb.js"></script>
    <script type="text/javascript" src="{cfgSiteUrl}ext/js/jq.t.js"></script>

    <!--[if lt IE 9]><script type="text/javascript" src="{cfgSiteUrl}ext/js/excanvas.js"></script><![endif]-->
    <script type="text/javascript" src="{cfgSiteUrl}ext/js/jq.tc.js"></script>
    <script type="text/javascript" src="{cfgSiteUrl}ext/js/jq.mi.js"></script>

    <script type="text/javascript" src="{cfgSiteUrl}ext/js/funcs.js"></script>
    <script type="text/javascript">
        $(function(){
            //
        });
    </script>
<if production>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter22257050 = new Ya.Metrika({id:22257050,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true});
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () { n.parentNode.insertBefore(s, n); }
            s.type = "text/javascript";
            s.async = true;
            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="//mc.yandex.ru/watch/22257050" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->


    <script type="text/javascript">
    /* GA */
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-43267427-1']);
    _gaq.push(['_trackPageview']);

    (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
    </script>
</if production>
</head>

<body bgcolor="#F5F3F7" text="#2E2E2E" link="#777777" vlink="#696969" alink="#4F4F4F" leftmargin="0" topmargin="15" marginheight="0" marginwidth="0">
