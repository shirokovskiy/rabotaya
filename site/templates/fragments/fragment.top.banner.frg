<if show.top.banner>
<!-- TOP BANNER-->
<div id="top-banner">
    {contentTopBanner}
</div>
<!-- // TOP BANNER-->
</if show.top.banner>


<if show.top.banners>
<!-- TOP BANNERS -->
<ul id="top_banner" class="jcarousel-skin-raya">
<loop top.banners>
    <li><a href="{strUrl}" target="_blank"><img src="{cfgAllImg}banners/{si_filename}" alt="{si_desc}" border="0"/></a></li>
</loop top.banners>
</ul>
<!-- // TOP BANNERS -->
</if show.top.banners>


<div class="img-weather">
    <img src="{cfgSiteImg}umbrella.png">
</div>
<table class="weather_block informer">
    <tr><td><span class="title">Погода: <br>днем: <span id="weather-max">{strWeatherMin} &#8451;</span></span></td></tr>
    <tr><td> </span>ночью: <span id="weather-min">{strWeatherMax} &#8451;</span></td></tr>
</table>

<div class="img-currency">
    <img src="{cfgSiteImg}currency.png">
</div>
<table class="currency_block informer">
    <caption class="title">Курсы валют<br/><span id="currency-date">на {strInformerDate}</span></caption>
    <tr><td class="ac"><a target="_blank" href="http://www.cbr.ru/">(USD) {strCurrencyUsd}</a></td></tr>
    <tr><td class="ac"><a target="_blank" href="http://www.cbr.ru/">(EUR) {strCurrencyEur}</a></td></tr>
</table>