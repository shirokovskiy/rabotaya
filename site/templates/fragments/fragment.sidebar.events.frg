<div id="events-brief" class="sidebar_block">
    <a href="{cfgSiteUrl}events"><h3>СОБЫТИЯ</h3></a>
    <loop sidebar.list.events>
        <div class="novelty">
            <a href="{cfgSiteUrl}events/{se_id}">{strTitle}</a>
        </div>
    </loop sidebar.list.events>

    <div class="block">
        <a href="{cfgSiteUrl}events"><i>Все события</i></a>
    </div>
</div>