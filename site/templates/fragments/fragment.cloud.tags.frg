<!--Ведущие компании-->
<div id="best-companies" class="">
    <h1>Вакансии ведущих компаний</h1>
    <div class="separator"></div>

    <div id="cloudTags" class="block">
        <ul id="company_tags" class="jcarousel jcarousel-skin-brands">
            <loop list.top.companies>
                <li><a href="{cfgSiteUrl}company-vacancies/{jc_id}"><img class="brand" src="{cfgAllImg}companies/{si_filename}" alt="{jc_title}"/></a></li>
            </loop list.top.companies>
        </ul>
    </div>
</div>

<if index.seo.text>
<div class="box-seo-text">
<strong>Работа, вакансии и анкеты соискателей в Санкт-Петербурге в газете вакансий «Работа от А до Я» (<a title="Работа в Санкт-Петербурге, ищу работу в Санкт-Петербурге, газета вакансий" href="http://rabota-ya.ru">rabota-ya.ru</a>). </strong>
<br>Мы рады предложить найти работу в Санкт-Петербурге соискателям через удобный сервис поиска работы. <p>Для работодателей мы предлагаем обширнейшую базу резюме соискателей.</p><p>Найти работу в Петербурге теперь гораздо проще используя газету вакансий Работа от А до Я. Большинство доступных у нас вакансий с приглашениями на работу от прямых работодателей. Хорошая работа в Петербурге и отличные резюме соискателей на сайте rabota-ya.ru – газета вакансий.</p> <p>Чтобы узнать где найти работу – обращайтесь  на сайт rabota-ya.ru, мы  предлагаем информацию  по всему Северо-Западному Федеральному округу.<br>Используйте нашу газету вакансий и веб-сайт для поиска работы, <a href="http://http://rabota-ya.ru/form_add_resume" title="Создать резюме соискателя, поиск работы в Санкт-Петербурге">публикуйте свои резюме соискателя</a>, ищите сотрудников, желаем удачи Вам!</p>
</div>
</if index.seo.text>

<div id="box_footer_links">
    <ul class="in_row">
        <li>
            <p><a href="{cfgSiteUrl}portal">О портале</a></p>
            <p><a href="{cfgSiteUrl}rules">Правила работы на портале</a></p>
            <p><a href="#form_adv_website_request" class="uc">Реклама на сайте</a></p>
            <p><a href="{cfgSiteUrl}trainings" class="important">ТРЕНИНГИ</a></p>
            <p><a href="{cfgSiteUrl}contacts">Контактная информация</a></p>
        </li>
        <li>
            <p><a href="{cfgSiteUrl}for_employers">Работодателям</a></p>
            <p><a href="#form_adv_website_request" class="uc">Реклама в газете</a></p>
            <p><a href="{cfgSiteUrl}sovety_rabotodatelyam">Советы для работодателей</a></p>
            <p><a href="{cfgSiteUrl}all-resumes" class="important">ВСЕ РЕЗЮМЕ</a></p>
            <p><a href="{cfgSiteUrl}informacionnoe-partnerstvo">Информационное партнерство</a></p>
        </li>
        <li>
            <p><a href="{cfgSiteUrl}soiskatelyam" class="uc">Соискателям</a></p>
            <p><a href="{cfgSiteUrl}usloviya_razmesheniya_resume" class="uc">Условия размещения резюме</a></p>
            <p><a href="{cfgSiteUrl}sovety_dlya_soiskateley" class="uc">Советы для соискателей</a></p>
            <p><a href="{cfgSiteUrl}all-vacancies" class="important">ВСЕ ВАКАНСИИ</a></p>
            <p><a href="{cfgSiteUrl}articles">Статьи</a></p>
        </li>
    </ul>
</div>

<!--Вакансии в облаке-->
<div id="tag-vacancy-cloud">
    <canvas width="1200" height="320" id="vacanciesCanvas">
        <p>К сожалению Ваш браузер не поддерживает HTML5 и поэтому Облако Тэгов не может быть отображено здесь! =(</p>
    </canvas>
</div>
<div id="vacancies_tags">
    <loop list.vacancies.in.cloud>
        <a href="{strLink}">{strTitle}</a>
    </loop list.vacancies.in.cloud>
</div>

<script type="text/javascript">
jQuery(document).ready(function ($) {
    $("#company_tags").jcarousel({
        auto: 10,
        scroll: 6,
        wrap: 'last',
        initCallback: mycarousel_initCallback
    });
});
</script>
