<div class="top_navigation">
    <h1 class="logo fl"><a href="{cfgSiteUrl}" title="{strPageTitle}">Работа от А до Я</a></h1>

    <div class="top_actions">
        <div class="fl">
            <ul class="adv_pass in_row">
                <li><a href="#form_adv_website_request" class="uc">Реклама <span>в ГАЗЕТЕ</span></a></li>
                <li><a href="#form_adv_website_request" class="uc">Реклама <span>на САЙТЕ</span></a></li>
            </ul>
        </div>
        <div class="phone_box fr finger">
            <h2>мы вам перезвоним!</h2>
            оставьте ваш номер телефона
        </div>
    </div>

    <div class="top_links">
        <ul id="items">
            <if spb>
            <li><a class="flow" href="{cfgSiteUrl}about">О газете</a></li>
            <li><a class="flow uc" href="{cfgSiteUrl}contacts">Контакты</a></li>
            </if spb>
            <if authed><li><a class="flow" href="{cfgSiteUrl}profile">Личный кабинет</a></li></if authed>
            <!--<ifelse authed><li><a class="flow auth_form_trigger" rel="#overlay_login_box" href="#">Войти</a></li></ifelse authed>-->

            <if authed><li class="last"><a class="flow" href="{cfgSiteCurrentUri}logout=1">Выход</a></li></if authed>
            <!--<ifelse authed><li class="last"><a class="flow" href="{cfgSiteUrl}registration">Зарегистрироваться</a></li></ifelse authed>-->
        </ul>
    </div>

    <div class="clearer"></div>
    <div class="phone_number_block">тел.: +7 (812) <span><span>305-305</span>-7</span></div>

    <div class="region_set">
        <p class="tr">Регион: <a href="javascript: void(0)" class="select_region">{strMyCity}</a></p>
    </div>

    <div class="search_box">
        <div class="bar">
            <div class="tabs">
                <ul>
                    <li id="tab1" class="color1"><a class="flow change-field" href="javascript:void(0)">Поиск вакансий</a></li>
                    <li id="tab2" class="color2"><a class="flow change-field" href="javascript:void(0)">Поиск сотрудников</a></li>
                    <li id="tab3" class="color3"><a class="flow change-field" href="javascript:void(0); setSearchForm('resumes')">Создать резюме</a></li>
                    <li id="tab4" class="color4"><a class="flow change-field" href="javascript:void(0); setSearchForm('vacancies')">Создать вакансию</a></li>
                </ul>
            </div>
            <div id="search_field" class="{active_color} flow">
                <div><form action="{cfgSiteUrl}search" method="post" id="searh_form">
                    <input type="hidden" id="categoryId" name="categoryId" value=""/>
                    <input type="hidden" id="type" name="type" value="{type}"/>
                    <div class="query">
                        <input type="text" id="query" name="query" value="{query}" class="plane fl" placeholder="Введите название должности или город или ID вакансии"/>
                        <input type="submit" value="Найти" class="plane-b"/>
                    </div>

                    <div class="precision">
                        <span class="fl">Поиск по:</span>
                        <ul>
                            <li><a href="/po-otraslyam" id="categories_selection_trigger">отраслям</a></li>
                            <li><a href="/po-kompaniyam" id="companies_selection_trigger">компаниям</a></li>
                            <li><a href="/po-professiyam" id="profession_selection_trigger">профессиям</a></li>
                            <li class="last"><a href="#" id="extended_search_trigger">расширенный</a></li>
                        </ul>
                    </div>

                    <div class="search_where clearer">
                        <ul class="fl">
                            <li><span>Искать в</span></li>
                            <li><input type="text" id="search_city" name="search_city" value="{search_city}" placeholder="во всей России" class="plane"/></li>
                            <li class="tr"><span>з/п от</span></li>
                            <li><input type="text" id="search_salary" name="search_salary" value="{search_salary}" placeholder="10000" class="plane" maxlength="7"/></li>
                            <li>руб.</li>
                        </ul>

                        <div class="total-count fr">Всего вакансий: <span>{strTotalCountVacancies}</span></div>
                        <div class="total-count fr">Всего резюме: <span>{strTotalCountResumes}</span></div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
    <div class="clearer"></div>

    <div id="regions_box">
        <p>Выберите Ваш регион:</p>
        <ul>
            <li><a href="#" style="font-weight: bold">Санкт-Петербург</a></li>
            <li><a href="#" style="font-weight: bold">Москва</a></li>
        <loop list.cities>
            <li><a href="#" title="записей {citiesMax}">{strCity}</a></li>
        </loop list.cities>
        </ul>
    </div>
</div>
<!-- END OF TOP NAVIGATION  -->

<div id="block_nav_top">
    <div class="wrapper">
        <table border="0">
            <tr>
                <td><a href="{cfgSiteUrl}"><img class="logo" src="/storage/siteimg/logo_s.png" alt="Работа от А до Я"/></a></td>
                <td><div class="links_box"><a href="#form_adv_website_request" class="uc"><b>Реклама в ГАЗЕТЕ или на САЙТЕ</b></a></div></td>
                <td><div class="phone_number_block">тел.: +7 (812) <span><span>305-305</span>-7</span></div></td>
                <td><div class="phone_box finger">
                    <h2>мы вам перезвоним!</h2>
                    оставьте ваш номер телефона
                </div></td>
                <td class="tr">
                    <p class="total-count">Всего вакансий: <a href="/all-vacancies"><span>{strTotalCountVacancies}</span></a></p>
                    <p class="total-count">Всего резюме: <a href="/all-resumes"><span>{strTotalCountResumes}</span></a></p>
                </td>
            </tr>
        </table>
    </div>
</div>


<script type="text/javascript">
var $window    = $(window);
$(function(){
    var $blockNav = $("#block_nav_top"), is_shown = false, topView = 90;
    $window.scroll(function(){
        if ($window.scrollTop() > topView && !is_shown) {
            $blockNav.stop().slideDown(1000);
            is_shown = true;
        }
        if ($window.scrollTop() <= topView && is_shown) {
            $blockNav.stop().slideUp();
            is_shown = false;
        }
    });

    var preSelectedExtension = '{preSelectedExtension}';
    if (preSelectedExtension != '') {
        $('.precision a').css({'color':'#4E4E4E', 'font-weight':'normal'});
        $('#'+preSelectedExtension).css({'color':'black', 'font-weight':'bold'});

        if (preSelectedExtension=='companies_selection_trigger') {
            $('#type').val( 'companies' );
            $('#query').attr('placeholder', 'Введите название компании');
        }
    }

    var unknownRegion = {isUnknownRegion};

    if (unknownRegion) {
        $('#detected_region_box span').html('{strDetectedRegionName}');
        $('#detected_region_box #city_confirm_yes').attr('city', '{strDetectedRegionName}');
        $('#detected_region_box').css({top: ($(window).height() / 5), left: ($(window).width() / 2 - 100)}).show();
    }

    $('#detected_region_box a').click(function () {
        $('#detected_region_box').hide();
        var clickedID = $(this).attr('id');
        console.log('clickedID = ' + clickedID);
        $.post("/lib/api/region.php", { city: $(this).attr('city') }, function(data){
            if (data.status == 0)
            {
                $('.select_region').text( data.city );
                var myHost = getMyHost('/{cfgCurrentUri}');
                if (clickedID == 'city_confirm_no')
                    gotoUrl( myHost );
            }
        }, "json");
    });
    // www.phpwebstudio.com
});
</script>
