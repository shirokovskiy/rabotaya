<div class="under-construction" id="uc">
    <img src="{cfgSiteImg}content/under-construction.gif" alt="Under Construction"/>
    <h3>Раздел в стадии разработки!</h3> Приносим свои извинения за временные неудобства.
</div>

<div id="form_adv_website_request">
    <form id="adv_website_request" method="POST" action="{cfgSiteUrl}adv_ad.php">
        <input type="hidden" name="adv" value="web"/>
    <h4>Email</h4>
        <input type="text" id="strEmail" name="strEmail" class="plane" maxlength="255" placeholder="your@email.here"/>
    <h4>Телефон</h4>
        <input type="text" id="strPhone" name="strPhone" class="plane" maxlength="255" placeholder="контактный телефон"/>
    <h4>Контактное лицо</h4>
        <input type="text" id="strContact" name="strContact" class="plane" maxlength="255" placeholder="как к Вам обращаться (ФИО)"/>
    <h4>Текст обращения</h4>
        <textarea id="strText" name="strText" style="overflow-y:auto;word-wrap:break-word"
                  class="plane" placeholder="HTML-тэги не допускаются">{strText}</textarea>
        <div>
        <!--CAPTCHA-->
        <img src="?get_img=captcha" border="0" class="fl inl">
        <input type="text" name="strCodeString" class="plane" maxlength="4" style="width:56px;letter-spacing:5px" onKeyUp="if(this.value.length >= this.maxLength) this.blur(); return false;">
            <span>введите буквы/цифры с картинки</span>
        <!--//CAPTCHA-->
        </div>
    </form>
    <div><button>Отправить</button></div>
</div>

<div id="overlay_login_box" class="overlay">
    <div class="contentWrap">
        <form action="/profile" method="POST">
            <input type="hidden" name="auth" value="true"/>
            <h1 class="title">Личный кабинет</h1>
            <h4>Email</h4>
            <input type="text" id="strAuthLogin" name="strAuthLogin" class="plane" maxlength="255" placeholder="ваш-адрес@email.ru"/>
            <h4>Пароль</h4>
            <input type="password" id="strAuthPassword" name="strAuthPassword" class="plane" maxlength="255" />
            <button>Сим сим откройся</button>
        </form>
        <p><a href="javascript: void(0)" id="closeLoginForm">Восстановить пароль</a><a href="#" class="recovery_form_trigger" rel="#overlay_recovery_box"></a></p>
    </div>
</div>

<div id="overlay_recovery_box" class="overlay">
    <div class="contentWrap">
        <form action="/lib/api/recovery.php" method="post">
            <input type="hidden" name="type" value="recovery"/>
            <h1 class="title">Выслать новый пароль</h1>
            <h4>Зарегистрированный ранее Email</h4>
            <input type="text" id="strRecoveryLogin" name="strRecoveryLogin" class="plane" maxlength="255" placeholder="ваш-адрес@email.ru"/>
            <button>Получить пароль</button>
        </form>
    </div>
</div>

<div id="detected_region_box">
    <h4 class="ac">Мы определили что Вы из <span></span>.</h4>
    <p class="ac">Это верно?</p>
    <p class="ac"><a href="javascript:void(0)" city="" id="city_confirm_yes">Да</a> / <a href="javascript:void(0)" city="Санкт-Петербург" id="city_confirm_no">Нет, показать Санкт-Петербург</a></p>
    <p class="note">P.S. Список резюме и вакансий на первой странице, а также при поиске отображается с учётом выбранного города!</p>
</div>

<script type="text/javascript">
    jQuery(document).ready(function($) {
        var sizeW = 265; /* also see in jq.tools.css for #overlay rules */
        var sizeH = 160;

        // Overlay
        function authOverlay() {
            // вычисление размеров для всплывающего слоя
            $('#overlay_login_box').css('width',sizeW);
            // overlay: loading external pages - if the function argument is given to overlay, it is assumed to be the onBeforeLoad event listener
            $(".auth_form_trigger").overlay({
                top: ($(window).height() - sizeH)/2,
                left: ($(window).width() - sizeW)/2 - 50,
                mask: '#939393',
                fixed: false,
                closeOnClick: true
            });

            $('#overlay_recovery_box').css('width',sizeW);

            $(".recovery_form_trigger").overlay({
                top: ($(window).height() - sizeH)/2,
                left: ($(window).width() - sizeW)/2 - 50,
                mask: '#939393',
                fixed: false,
                closeOnClick: true,
                onBeforeLoad: function () {
                    //$("a[rel]").overlay().close();
                }
            });
        }

        authOverlay();

        $('#closeLoginForm').click(function () {
            $(".auth_form_trigger").overlay().close();
            setTimeout(function () {
                $(".recovery_form_trigger").overlay().load();
            }, 500);
        });

        $(window).resize(function() {
            //authOverlay();
        });

        $("#overlay_recovery_box form").submit(function(event) {
            /* stop form from submitting normally */
            event.preventDefault();

            /* get some values from elements on the page: */
            var $form = $( this ),
                url = $form.attr( 'action'),
                validation = true,
                type = $form.find('input[name=type]').val(),
                strEmail = $( '#strRecoveryLogin' ).val();

            if (isEmpty(strEmail)) {
                validation = false;
                alert('Ошибка: Вы не заполнили поле Email.');
                $( '#strRecoveryLogin').addClass('err').change(function(){
                    $(this).removeClass('err');
                });
                return false;
            }

            if (!isEmail(strEmail)) {
                validation = false;
                alert('Ошибка: Вы не правильно заполнили поле Email.');
                $( '#strRecoveryLogin').addClass('err').change(function(){
                    $(this).removeClass('err');
                });
                return false;
            }

            if (validation) {
                /* Send the data using post */
                var posting = $.post( url, $form.serialize(), null, 'json' );

                /* Put the results in a div */
                posting.done(function( data ) {
                    if (data.status == 1) {
//                      console.log('OK');
                        $form.find("input[type=text]").val('');

                        $(".recovery_form_trigger").overlay().close();
                    } else {
                        console.log('NO RESULT. ERRORS!');
                    }

                    setTimeout(function () {
                        $.fancybox(data.message);
                    }, 500)
                });
            }
        });

        //$.fancybox("<p>На указанный Вами адрес выслан пароль.</p><p>В ближайшее будущее проверьте папку Входящие Вашего почтового ящика.</p><p>Убедитесь что письмо не попало в Спам.</p><p>Удачной авторизации!</p>");
    });
</script>

<img src="{cfgSiteImg}ajax-circle.gif" width="200" alt="ajax-loader" id="ajax-loader"/>
</body>
</html>