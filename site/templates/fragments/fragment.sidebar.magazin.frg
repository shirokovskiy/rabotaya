<div id="box_magazines" class="section">
    <div id="week_magazines" class="brd_tu">
        <div id="look_of_magazine">
            <img id="weekThumb" src="{cfgAllImg}magazines/magazine.thumb.tu.{intWeekMagazine_tu}.{intYearMagazine_tu}.jpg" />
            <button id="butExpand" class="butExpand"><span>Раскрыть</span></button>
        </div>
    </div>
    <div class="tabs">
        <a id="tu" href="javascript:void(0)" data-url="{strUrlMagazine_tu}" data-week="{intWeekMagazine_tu}" data-year="{intYearMagazine_tu}" class="current">ВТ</a>
        <a id="we" href="javascript:void(0)" data-url="{strUrlMagazine_we}" data-week="{intWeekMagazine_we}" data-year="{intYearMagazine_we}">СР</a>
        <a id="th" href="javascript:void(0)" data-url="{strUrlMagazine_th}" data-week="{intWeekMagazine_th}" data-year="{intYearMagazine_th}">ЧТ</a>
    </div>
</div>

<frg fragment.sidebar.news>

<div class="section">
    <div class="block">
        <div class="title">Зона распространения</div>
        <a href="{strUrlPathDistribution}" id="distribution"><img src="{cfgSiteImg}content/distribution.png" alt="Распространение"/></a>
    </div>
</div>