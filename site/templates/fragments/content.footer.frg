<div id="content_footer">
    <div id="copyright">
        <em>Внимание!</em> При обращении в организацию, просьба ссылаться на сайт газеты «Работа от А до Я» - <a class="link_grey" href="{cfgSiteUrl}">www.rabota-ya.ru</a>
        <br>При перепечатывании материалов, активная ссылка на сайт обязательна.
        <br>© Все права защищены.
        <br>Новая версия сайта находится в стадии бета-тестирования.
        <br>Возможны проблемы с работой некоторых страниц. Если Вы обнаружили ошибку или неточность, просим написать об этом в
        <a href="#">службу поддержки</a><br>
        <a href="{cfgSiteUrl}low">Закон о СМИ</a>
    </div>
</div>

<if production>
<div class="ac">
    <if dimitriy>
    <div class="fl box">
        <!-- begin of Top100 code -->

        <script id="top100Counter" type="text/javascript" src="http://counter.rambler.ru/top100.jcn?3010859"></script>
        <noscript>
            <a href="http://top100.rambler.ru/navi/3010859/">
                <img src="http://counter.rambler.ru/top100.cnt?3010859" alt="Rambler's Top100" border="0" />
            </a>

        </noscript>
        <!-- end of Top100 code -->
    </div>
    </if dimitriy>

    <div class="fl box">
        <!-- Rating@Mail.ru counter -->
        <script type="text/javascript">//<![CDATA[
        var _tmr = _tmr || [];
        _tmr.push({id: "2497310", type: "pageView", start: (new Date()).getTime()});
        (function (d, w) {
            var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true;
            ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
            var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
            if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
        })(document, window);
        //]]></script><noscript><div style="position:absolute;left:-10000px;">
        <img src="//top-fwz1.mail.ru/counter?id=2497310;js=na" style="border:0;" height="1" width="1" alt="Рейтинг@Mail.ru" />
        </div></noscript>
        <!-- //Rating@Mail.ru counter -->

        <!-- Rating@Mail.ru logo -->
        <a href="http://top.mail.ru/jump?from=2497310">
            <img src="//top-fwz1.mail.ru/counter?id=2497310;t=290;l=1"
                 style="border:0;" height="31" width="38" alt="Рейтинг@Mail.ru" /></a>
        <!-- //Rating@Mail.ru logo -->
    </div>

    <div class="fl box">
        <!--LiveInternet counter-->
        <script type="text/javascript"><!--
        document.write("<a href='http://www.liveinternet.ru/click' "+
            "target=_blank><img src='//counter.yadro.ru/hit?t44.17;r"+
            escape(document.referrer)+((typeof(screen)=="undefined")?"":
            ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
                    screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
            ";"+Math.random()+
            "' alt='' title='LiveInternet' "+
            "border='0' width='31' height='31'><\/a>")
        //--></script>
        <!--/LiveInternet-->
    </div>
</div>
</if production>
