<div class="central-block">
    <h3>Получить доступ к моим вакансиям используя E-mail</h3>

    <if error>
        <p class="err">Ошибка: {msgErr}</p>
    </if error>

    <if warning>
        <p class="warning"><span>Внимание</span>: {msgWarn}</p>
    </if warning>

    <if ok>
        <p class="success">{msgOk}</p>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $('#submitBtn').hide();
                $('#strEmail').prop('disabled', true);
            });
        </script>
    </if ok>

    <form action="" method="post">
        <input type="hidden" id="sss" name="sss" value="{sss}"/>
        <input type="text" id="strEmail" name="strEmail" value="{strEmail}" class="plane" maxlength="255" placeholder="введите E-mail здесь"/>
        <br/>
        <input type="submit" id="submitBtn" class="plane-b" value="Отправить мне ссылку для входа в Личный Кабинет"/>
    </form>

    <div class="block_a">
        <p>Получить доступ к своим вакансиям, используя только адрес электронной почты, можно только в том случае, если ранее Вы <b>не</b> заводили Личный Кабинет на сайте.</p>
        <p>Нажав кнопку «Отправить мне ссылку для входа в Личный Кабинет» Вы подтверждаете своё согласие на автоматическое создание Личного Кабинета на сайте Rabota-Ya.ru, отправки Вам дополнительных писем с логином и паролем, а также заполнение личной информации из Вашей вакансии.</p>
        <p>Личный Кабинет является закрытой страницей, для входа на которую Вам потребуется Ваш E-mail и высланый Вам вторым письмом пароль.</p>
        <p>Пароль, а также личные данные можно в дальнейшем изменить в Личном Кабинете.</p>
    </div>
</div>
