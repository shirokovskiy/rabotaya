<a name="search"></a>
<h1 class="title">Список отраслей</h1>

<script type="text/javascript">
    $(function (){
        $('.list_extensible .parent > a').click( function(){
            $('.list_extensible .sublist').hide();
            $(this).parent().find('.sublist').show();
        });
    });
</script>

<div id="categories_selection">
    <ul class="list_extensible">
        <loop list.categories>
            <li class="parent"><a href="#{intMainCategoryId}">{strMainCategory}</a>
                <if is.subcategories.{intMainCategoryId}>
                    <ul class="sublist">
                        <loop list.subcategories.{intMainCategoryId}>
                            <li><a href="/search/category/{intSubCatId}" class="link_grey" cat_id="{intSubCatId}">{strSubCatTitle}</a></li>
                        </loop list.subcategories.{intMainCategoryId}>
                    </ul>
                </if is.subcategories.{intMainCategoryId}>
            </li>
        </loop list.categories>
    </ul>
</div>