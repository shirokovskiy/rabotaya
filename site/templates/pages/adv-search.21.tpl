<h3>Расширенный поиск вакансий</h3>

<form action="/search" id="adv_searh_form" method="post">
    <input type="hidden" id="type" name="type" value="vacancies"/>
<div class="advanced_search_box vacancies">
    <div class="block">
        <input type="text" id="strAdvTitle" name="query" class="plane" maxlength="255" placeholder="Введите поисковую фразу: название должности или города или ID вакансии"/>
    </div>

    <div class="block">
        <div class="half">
            <span>Искать фразу:</span>
            <select name="searchWhere" class="plane">
                <option value="0">только в названии</option>
                <option value="1">везде</option>
            </select>
        </div>
        <div class="half">
            <span>и:</span>
            <select name="searchHow" class="plane">
                <option value="0">все слова</option>
                <option value="1">хотя бы одно слово</option>
            </select>
        </div>
        <div class="clearer"></div>
    </div>

    <div class="block">
        <select name="categoryId" class="catSelector plane">
            <option value="">любое направление деятельности</option>
            <loop option.group.category>
                <optgroup label="{strOptGroupName}">
                    <loop group.options.{intGroupID}>
                        <option value="{intOptionValue}">{strOptionTitle}</option>
                    </loop group.options.{intGroupID}>
                </optgroup>
            </loop option.group.category>
        </select>
    </div>

    <div class="block">
        <input type="text" name="subQuery" class="plane" maxlength="255" placeholder="исключить слова"/>
    </div>

    <div class="block">
        <div class="half">
            <span>Зарплата от:</span>
            <input type="text" id="strSalaryFrom" name="search_salary" class="small money" value="0" maxlength="9" placeholder="от" /> <span>руб.</span>
        </div>
        <div class="half">
            <input type="checkbox" id="isNoSalary" name="isNoSalary" class="plane fl"/> <label for="isNoSalary">- выводить без указанной з/п</label>
        </div>
        <div class="clearer"></div>
    </div>

    <div class="block">
        <div class="half">
            <span>Образование:</span>
            <div>
                <select name="cbxEdu" id="sbxEdu" class="plane">
                    <option value="">любое</option>
                    <loop education.types>
                        <option value="{jet_id}">{jet_title}</option>
                    </loop education.types>
                </select>
            </div>
        </div>
        <div class="half">
            <span>График работы:</span>
            <div>
                <select name="cbxWorkBusy" class="plane">
                    <option value="">любой</option>
                    <loop workbusy.types>
                        <option value="{jw_id}">{jw_title}</option>
                    </loop workbusy.types>
                </select>
            </div>
        </div>
        <div class="clearer"></div>
    </div>

    <div class="block">
        <div class="half">
            <div>
                <select name="sbxCompanyType" class="plane">
                    <option value="">прямые работодатели и КА</option>
                    <option value="1">только прямые работодатели</option>
                    <option value="2">только кадровые агенства</option>
                </select>
            </div>
        </div>
        <div class="half">
            <div>
                <select name="sbxPeriod" class="plane">
                    <option value="">за всё время</option>
                    <option value="1">за прошедшие 24 часа</option>
                    <option value="2">за 3 дня</option>
                    <option value="3">за неделю (7 дней)</option>
                    <option value="4">за 2 недели (14 дней)</option>
                    <option value="5">за месяц</option>
                    <option value="6">за 3 месяца</option>
                </select>
            </div>
        </div>
        <div class="clearer"></div>
    </div>

    <div class="block">
        <div class="clearer">
            <input type="checkbox" id="cbxDrive" name="cbxDrive" class="plane fl"/> <label for="cbxDrive">- Наличие водительского удостоверения</label>
        </div>
        <div class="clearer">
            <input type="checkbox" id="cbxTrips" name="cbxTrips" class="plane fl"/> <label for="cbxTrips">- Готовность к командировкам</label>
        </div>
    </div>

    <div class="block">
        <input type="submit" value="Найти" class="plane-b"/>
    </div>
</div>
</form>

<div class="box-seo-text">
    Быстрый способ найти сотрудника — это разместить вакансию на Rabota-Ya.ru Здесь ваши вакансии увидят тысячи соискателей и на вашу почту будут отправлены сотни откликов - резюме. Подробное описание вакансии, где ясно изложены не только требования и обязанности, указана зарплата и другие условия работы, очень привлекательно для соискателя.
    Рынок труда очень изменчив, но есть такие сферы, где всегда наблюдается определенный дефицит кадров — например, продажи и сервис. Чтобы вакансии вашей компании привлекли внимание наибольшего количества кандидатов, они должны быть на первых строчках результатов поиска.
    Еще один способ подбора персонала — выбрать подходящих кандидатов из общей базы резюме на сайте Rabota-Ya.ru. Резюме очень много, и на сайте можно воспользоваться и  быстрым поиском по названию должности и более точный поиск по параметрам.
</div>

<script type="text/javascript">
    $(function (){
        $('#strSalaryFrom').spinner({
            min: 0,
            step: 5000
        });

        //Обработка нажатия на кнопку "Вверх"
        $("#btn_up").click(function(){
            //Необходимо прокрутить в начало страницы
            var curPos=$(document).scrollTop();
            var scrollTime=curPos/1.73;
            $("body,html").animate({"scrollTop":0},scrollTime);
        });

        //Обработка нажатия на кнопку "Вниз"
        $("#btn_down").click(function(){
            //Необходимо прокрутить в конец страницы
            var curPos=$(document).scrollTop();
            var height=$("body").height();
            var scrollTime=(height-curPos)/1.73;
            $("body,html").animate({"scrollTop":height},scrollTime);
        });

        $("body,html").animate({"scrollTop": 119}, 500);
    });
</script>