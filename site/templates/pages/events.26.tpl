<if events.list>
    <h3>Все события</h3>

    <div class="cnt">
        {blockPaginator}
    </div>

    <div class="events-list">
        <loop last.events>
            <div class="news-cell">

                <div class="news-box">
                    <div class="news-text">
                        <p class="txt-block"><a href="{cfgSiteUrl}events/{se_id}" class="link-blue">{se_title}</a></p>
                    </div>
                    <if events.picture.{se_id}>
                        <div class="fr events-image"><a href="{cfgAllImg}events/events.photo.{se_id}.b.jpg{strUniquePhoto}" id="hrefImg{se_id}"><img src="{cfgAllImg}events/events.photo.{se_id}.jpg{strUniquePhoto}" border="0" width="200" /></a></div>
                    </if events.picture.{se_id}>
                    <div class="clearer"></div>
                </div>

            </div>
            <div class="clearer"></div>
        </loop last.events>
    </div>

    <div class="cnt">
        {blockPaginator}
    </div>
</if events.list><if event>
    <h3>{se_title}</h3>

    <script type="text/javascript">
        $(document).ready(function() {
            <if picture>
                    $("a#pictureMain").fancybox({
                        'titleShow'		: true
                        , 'titlePosition'	: 'over'
                    });
            </if picture>
        });
    </script>
    <div class="cnt">
        <if picture>
            <a href="{cfgAllImg}events/events.photo.{se_id}.b.jpg{strUniqueMainPhoto}" id="pictureMain"><img src="{cfgAllImg}events/events.photo.{se_id}.jpg{strUniqueMainPhoto}" width="200" border="0" align="right" style="margin:0 0 5px 10px" alt="{strImageTitle}"></a>
        </if picture>

        <span class="date"><i>{strDatePublEvents}</i></span>
        <p style="margin:12px 0 4px 0">{strEventsBody}</p>

        <p>« <a href="{cfgSiteUrl}events?stPage={stPage}">к списку событий</a></p>
    </div><!-- // cnt -->
</if event>