<h1>Резюме по профессиям</h1>

<ul class="list_in_columns clearer">
    <loop list>
        <li class="{strCss}"><a href="/search/profession-resume/{urlTitle}">{strTitle}</a></li>
    </loop list>
</ul>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('.list_in_columns').columnize({columns: 3});
    });
</script>
