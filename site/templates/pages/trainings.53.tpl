<if list>
<h1>Тренинги</h1>

<div id="box_categories" class="block_rubrics">
    <ul class="in_row">
        <li>Отрасль: </li>
        <li><a href="{cfgSiteUrl}trainings" class="main">ВСЕ</a></li>
        <loop list.groups>
            <li><a href="{cfgSiteUrl}trainings/group/{str_id}" class="{rubric_active}">{str_rubricname}</a></li>
        </loop list.groups>
    </ul>
</div>
<if sub.categories>
<div id="box_subcategories" class="block_rubrics">
    <ul class="in_row">
        <li>Подгруппа: </li>
        <loop list.subgroups>
            <li><a href="{cfgSiteUrl}trainings/group/{mainRubricID}/subgroup/{str_id}" class="{subrubric_active}">{str_rubricname}</a> |</li>
        </loop list.subgroups>
    </ul>
</div>
</if sub.categories>

<div class="cnt">{blockPaginator}</div>

<div class="search_field" style="margin: 10px 0">
    <form>
    Поиск тренинга:
    <input type="text" name="search" value="{searchParam}" style="width: 300px"/>
    <button>Искать</button>
    </form>
</div>

<div class="records-list">
<loop last.trainings>
    <div class="record-cell">

        <div class="record-box">
            <div class="news-text">
                <p class="txt-block"><a href="{cfgSiteUrl}trainings/{str_id}?p={stPage}" class="link-blue">{str_title}</a></p>
            </div>
            <div class="clearer"></div>
        </div>

    </div>
    <div class="clearer"></div>
</loop last.trainings>
</div>

<div class="cnt">{blockPaginator}</div>

<div style="margin-top: 40px; text-align: center">
    <a href="{cfgSiteUrl}add.training" style="font-size: 20px; font-weight: bold">Добавить тренинг</a>
</div>

</if list>

<if record>
<h3>{str_title}</h3>

<div class="cnt" id="training">
    <p class="description">{strBody}</p>
    <p class="box_small_text txt_grey">{strAddress}</p>
    <p class="box_small_text txt_grey">{strPhone}</p>
    <p class="box_small_text txt_grey">{strEmail}</p>
    <p class="box_small_text txt_grey">{strWeb}</p>

    <p style="margin-top: 30px">« <a href="{cfgSiteUrl}trainings?stPage={stPage}">к списку тренингов</a></p>
</div><!-- // cnt -->
</if record>
