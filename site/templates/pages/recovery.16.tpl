<div class="central-block">
    <h3>Восстановление пароля</h3>

    <p>Укажите E-mail адрес, который Вы использовали при регистрации и Вам будет отправлен новый сгенерированый пароль для авторизации на сайте.</p>

    <if sent>
        <span class="success"><b>Напоминание отправлено на указаный E-mail</b></span>
    </if sent>

    <if no.such.email>
        <span class="err"><b>Ошибка! Нет такого E-mail адреса в нашей БД сайта</b></span>
    </if no.such.email>

    <if no.email>
        <span class="err"><b>Ошибка! Поле E-mail осталось пустым или набран адрес неверного формата.</b></span>
        <a href="http://ru.wikipedia.org/wiki/%D0%AD%D0%BB%D0%B5%D0%BA%D1%82%D1%80%D0%BE%D0%BD%D0%BD%D0%B0%D1%8F_%D0%BF%D0%BE%D1%87%D1%82%D0%B0" target="_blank">Что такое E-mail?</a>
    </if no.email>

    <if no.new.password>
        <span class="err"><b>Ошибка! Новый пароль небыл сгенерирован</b></span>
    </if no.new.password>

    <if not.sent.password>
        <span class="err"><b>Ошибка! Не могу отправить E-mail. Попробуйте позже.</b></span>
    </if not.sent.password>

    <form method="POST">
        <input type="hidden" name="recovery" value="true"/>
        <input type="text" id="strEmail" name="strEmailToRecover" class="plane" value="{strEmailToRecover}" maxlength=255 style="width:300px" placeholder="введите E-mail здесь" />
        <input type="submit" name="submit" class="plane-b" value="Получить пароль" />
    </form>
</div>
