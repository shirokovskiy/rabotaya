<if news.list>
    <h3>Все новости</h3>

    <div class="cnt">
        {blockPaginator}
    </div>

    <div class="news-list">
        <loop last.news>
            <div class="news-cell clearer">

                <div class="news-box">
                    <div class="news-text">
                        <span class="date">{strDatePublicNews}</span>
                        <p class="txt-block"><a href="{cfgSiteUrl}news/{sn_id}" class="link-blue">{sn_title}</a></p>
                    </div>
                    <if news.picture.{sn_id}>
                        <div class="fr news-image"><a href="{cfgAllImg}news/news.photo.{sn_id}.b.jpg{strUniquePhoto}" id="hrefImg{sn_id}"><img src="{cfgAllImg}news/news.photo.{sn_id}.jpg{strUniquePhoto}" border="0" width="200" /></a></div>
                    </if news.picture.{sn_id}>
                    <div class="clearer"></div>
                </div>

            </div>
        </loop last.news>
    </div>

    <div class="cnt">
        {blockPaginator}
    </div>
</if news.list>









<if novelty>
    <h3>{sn_title}</h3>

    <script type="text/javascript">
        $(document).ready(function() {
            <if picture>
                $("a#pictureMain").fancybox({
                    'titleShow'		: true
                    , 'titlePosition'	: 'over'
                });
            </if picture>
        });
    </script>
    <div class="cnt">
        <if picture>
        <table style="width: 100%">
            <tr>
                <td style="width: 110px; vertical-align: top">
                <loop images>
                    <div>
                        <a href="{cfgAllImg}news/{sn_id}/{si_filename}" id="pictureMain"><img src="{cfgAllImg}news/{sn_id}/{small_filename}" width="100" border="0" align="right" style="margin-bottom: 5px" alt=""></a>
                    </div>
                </loop images>
                </td>
                <td style="padding: 0 10px; vertical-align: top">
        </if picture>
                    <span class="date"><i>{strDatePublNews}</i></span>
                    <p style="margin:12px 0 4px 0">{strNewsBody}</p>
        <if picture>
                </td>
            </tr>
        </table>
        </if picture>

        <p>« <a href="{cfgSiteUrl}news?stPage={stPage}">к списку новостей</a></p>
    </div><!-- // cnt -->
</if novelty>
