<div class="block profile">
<if authed>
    <if jur>
        <div class="left_sidebar">
            <div class="resumes block">
                <h3>Вакансии</h3>
                <ul>
                    <li><a href="/form_add_vacancy">Разместить вакансию</a></li>
                    <li><a href="/profile/vacancy">Мои вакансии</a></li>
                    <!--<li><a href="/profile/informer">Информер</a></li>-->
                </ul>
            </div>
            <!--<div class="longlist block">-->
                <!--<h3>Лонглисты</h3>-->

                <!--<ul>-->
                    <!--<li><a href="/profile/longlists">Лонглисты</a></li>-->
                    <!--<li><a href="/profile/shedule">Календарь</a></li>-->
                <!--</ul>-->
            <!--</div>-->
            <div class="vacancies block">
                <h3>Резюме</h3>

                <ul>
                    <li><a href="/profile/search_resume">Поиск резюме</a></li>
                    <li><a href="/profile/favorite_resume">Избранные</a></li>
                    <!--<li><a href="/profile/subscriptions_resume">Подписки</a></li>-->
                    <!--<li><a href="/profile/blacklist_resume">Черный список</a></li>-->
                    <!--<li><a href="/profile/sent_resume">Присланные резюме</a></li>-->
                </ul>
            </div>
            <!--<div class="pay_service block">-->
                <!--<h3>Платные услуги</h3>-->
                <!--<ul>-->
                    <!--<li><a href="/profile/description_service">Описание и заказ услуг</a></li>-->
                    <!--<li><a href="/profile/paying">Пополнение счета</a></li>-->
                    <!--<li><a href="/profile/invoicing">Выставление счета</a></li>-->
                    <!--<li><a href="/profile/ordered_service">Заказанные услуги</a></li>-->
                <!--</ul>-->
            <!--</div>-->
            <!--<div class="useful block">-->
                <!--<h3>Полезное</h3>-->
                <!--<ul>-->
                    <!--<li><a href="/profile/free_consulting">Бесплатные консультации по трудовому праву</a></li>-->
                    <!--<li><a href="/profile/prod_shedule">Производственный календарь</a></li>-->
                <!--</ul>-->
            <!--</div>-->
            <div class="settings block">
                <h3>Настройки</h3>

                <ul>
                    <li><a href="/profile">Мой профиль</a></li>
                    <!--<li><a href="/profile/msg_template">Шаблоны сообщений</a></li>-->
                    <!--<li><a href="/profile/comp_profile">Профиль компании</a></li>-->
                    <!--<li><a href="/profile/comp_offices">Офисы компании</a></li>-->
                    <!--<li><a href="/profile/users">Пользователи</a></li>-->
                    <!--<li><a href="/profile/feedback">Обратная связь</a></li>-->
                </ul>
            </div>
        </div>
        <div class="content_jur">
            <if vacancy>
                <h1>Мои вакансии</h1>
                <p class="important note">Максимальное одновременное количество вакансий <span>{vac_intime}</span>.</p>
                <div class="yellow_box">
                    <div class="block disp-inline">
                        <p class="title">Город</p>
                        <select name="cityId" class="red-border width150">
                            <option value="">Любой город</option>
                            <loop city.names>
                                <option value="{id}">{city}</option>
                            </loop city.names>
                        </select>
                    </div>
                    <div class="block disp-inline">
                        <p class="title">Статус</p>
                        <select name="stateId" class="red-border width150">
                            <option value="">Любой статус</option>
                            <loop states>
                                <option value="{intOptionValue}">{strOptionTitle}</option>
                            </loop states>
                        </select>
                    </div>
                    <div class="block disp-inline">
                        <p class="title">Рекрутер</p>
                        <select name="recruitId" class="red-border width150">
                            <option value="">Любой рекрутер</option>
                            <loop recruits>
                                <option value="{su_id}">{fio}</option>
                            </loop recruits>
                        </select>
                    </div>
                    <div class="block disp-inline">
                        <p class="title">Выводить</p>
                        <input type="checkbox" name="isOnOnePage" class="red-border" value="{isOnOnePage}" />на одной странице
                    </div>
                </div>
                <div class="clr"></div>
                    <if no.vacancy><p>Список пуст. Пока Вы не создали ни одной вакансии. <a href="/form_add_vacancy">Разместите</a> свою первую вакансию.</p></if no.vacancy>
                    <ifelse no.vacancy>
                        <p>Найдено вакансий: <span>{numVacancies}</span></p>
                        <table border="0" cellspacing="2" cellpadding="5" class="tbl-yellow-white">
                            <tr>
                                <th>ID</th>
                                <th class="col-title">Название</th>
                                <th class="col-title">Местоположение</th>
                                <th class="col-date">Дата</th>
                                <th>Статус</th>
                                <th>Просмотров</th>
                            </tr>
                            <loop list.vacancies>
                                <tr>
                                    <td><b>{jv_id}</b></td>
                                    <td><a href="{cfgSiteUrl}vacancy/{jv_id}"><span class="title">{jv_title}</span></a><br/>
                                        <if published.{jv_id}><a href="{cfgSiteUrl}vacancy/{jv_id}">Посмотреть</a> | </if published.{jv_id}><a href="#uc" class="uc">Редактировать</a> | <a href="#uc" class="uc">Повысить эффективность</a>
                                        <p class="smallgray">{cont_fio}</p>
                                    </td>
                                    <td><span class="date_pub">{jv_city}</span></td>
                                    <td><span class="date_pub color-darkred">{date_publish}</span><br/>
                                        <a href="#">Обновить дату</a>
                                    </td>
                                    <td><span class="color-green">{strStatus}</span><br/>
                                        <a href="{strCurrentUrl}?vacid={jv_id}&status=N">Снять с публикации</a>
                                    </td>
                                    <td><span>{jv_views}</span></td>
                                </tr>
                            </loop list.vacancies>
                        </table>
                    </ifelse no.vacancy>
            </if vacancy>
            <if informer>
                <h1>Получите бесплатно Информер для Вашего сайта</h1>
                <div class="block disp-inline block_informer">
                    <p><span class="color-red">R</span>abota-<span class="color-red">Ya</span>.Ru предоставляет новый сервис - Информер для Вашего сайта.
                        Информер - это блок с информацией о свежих и актуальных вакансиях, размещенных на портале <span class="color-red">R</span>abota-<span class="color-red">Ya</span>.Ru,
                        которые интересны поситителям именно Вашего сайта! В Информер могут быть включены вакансии с отбором по какому-либо фильтру иливакансиям именно вашей компании: таким образом,
                        Вы можете легко создать раздел с актуальным ивакансиями на Вашем сайте всего за пару минут.</p>
                    <p>Особенно удобно использовать Информер компаниям, которые регулярно размещают вакансиина нашем портале: теперь Вам нет неоходимости размещать вакансию вручную на Вашем сайте -
                    Информер сделает это за Вас автоматически!</p>
                    <p>Для Вашего удобства мы разработали два вида Информера: Краткий (включает дату, название вакансии и уровень заработной платы) и Расширенный (включает дату, название вакансии и
                        уровень заработной платы, город, график работы, а также требование по образованию).</p>
                    <p>Создать Информер очень легко: ...... ......</p>
                    <h2>Условия выбора вакансии</h2>
                    <div class=" block yellow_box">
                        <p><input type="text" name="informerKeyWord" class="red-border width320" value="{informer_keyword}" placeholder="Ключевые слова"/>
                            <select name="informerKeyWords" class="red-border width150">
                                <option value="allWords">Все слова</option>
                            </select>
                        </p>
                        <p>
                            <select name="informerKindActivity" class="red-border width515">
                                <option value="0">Любое направление деятельности</option>
                                <loop option.group.category.informer>
                                    <option value="{jc_id}">{jc_title}</option>
                                </loop option.group.category.informer>
                            </select>
                        </p>
                        <p>
                            <select name="informerWorkSchedule" class="red-border width320">
                                <option value="0">Любой график работы (кр. вахты и удаленной )</option>
                                <loop workbusy.types.informer>
                                    <option value="{jw_id}">{jw_title}</option>
                                </loop workbusy.types.informer>
                            </select>
                            З/п. от <input type="text" name="informerSalaryFrom" class="red-border width50" value="{informer_salary_from}"/> руб
                        </p>
                        <p>
                            <select name="informerCityId" class="red-border width320">
                                <option value="">Любой город</option>
                                <loop city.names.informer>
                                    <option value="{id}">{city}</option>
                                </loop city.names.informer>
                            </select>
                            <input type="text" name="informerCompanyID" class="red-border width130" value="{informer_companyID}" placeholder=" ID компании"/>
                        </p>
                        <p>
                            <a href="#" id="insertCompID">Вставить ID моей компании </a>
                        </p>
                    </div>
                    <h2>Внешний вид информера</h2>
                        <div class=" block yellow_box">
                            <table>
                            <tr>
                                <td><p>Ключевые слова </p></td>
                                <td align="right"><select name="informerKeyWords2" class="red-border width320">
                                        <option value="5">5</option>
                                        </select></td>
                            </tr>
                            <tr>
                                <td><p>Тип </p></td>
                                <td align="right"><select name="informerType" class="red-border width320">
                                        <option value="big" selected>расширенный</option>
                                        <option value="small">компактный</option>
                                    </select></td>
                            </tr>
                            <tr>
                                <td><p>Кодировка сайта </p></td>
                                <td  align="right"><select name="informerEncoding" class="red-border width320">
                                        <option value="">UTF-8</option>
                                    </select></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td align="right"><input type = 'button' id="btnGetInformer" value="Получить информер"/></td>
                            </tr>
                            </table>

                        </div>
                </div>
                <div class="disp-inline  block-informer-pic">
                    <h2>Так выглядит Расширенный Информер</h2>
                        <div class="block"><br/>
                            <img src="{cfgSiteImg}/content/informer-big.png"/>
                        </div>
                    <h2>Так выглядит Компактный Информер</h2>
                        <div class="block"><br/>
                            <img src="{cfgSiteImg}/content/informer-kompact.png"/>
                        </div>
                </div>
                <div class="clr"></div>

            </if informer>
            <if longlists>
                <h1>LongList соискателей</h1>
                <p>Для повышения безопасности Вы можете <b>включить шифрование</b> данных о соискателях в Лонглисте на странице <a  href="/profile"> Мой профиль</a>.</p>
                <div class=" block yellow_box">
                    <div class="block_left">
                        <table>
                        <tr>
                        <td><p>Вакансия</p></td>
                        <td>
                            <select name="longlistVacancy" class="red-border width320">
                                <option value="0">По всем открытым вакансиям</option>
                                <loop vacancy.longlist>
                                    <option value="{id}">{city}</option>
                                </loop vacancy.longlist>
                            </select>
                        </td></tr>
                        <tr>
                        <td><p>Статус</p></td>
                        <td>
                            <select name="longlistState" class="red-border width150">
                                <option value="0">Все</option>
                                <loop states.longlist>
                                    <option value="{id}">{city}</option>
                                </loop states.longlist>
                            </select>
                            <a href="#" class="radio-btn"> <div class="radio-btn-active"></div></a> <span> Текущий</span>
                            <a href="#" class="radio-btn"> <div class="radio-btn-active" style="display: none"></div></a> <span> Любой</span>

                        </td></tr>
                        <tr>
                            <td><p>Даты</p></td>
                            <td><input type="text" class="red-border width100" id = "longlistDateFrom" name="longlistDateFrom"/>  <input type="text" class="red-border width100" id="longlistDateTill" name="longlistDateTill"/></td>
                        </tr>
                        <tr>
                            <td>ФИО</td>
                            <td><input type="text" class="red-border width320" id = "lnglistFio" name="lnglistFio"/> </td>
                        </tr>
                        </table>
                    </div>
                    <div class="block_right">
                        <br/>
                        <img src="{cfgSiteImg}content/man-in-coat.png" class="img-ico"><a href="#">Добавить соискателя</a>
                        <p class="small-gray">для добавления соискателя извне</p>

                        <div class="clr"></div>
                        <br/>
                        <img src="{cfgSiteImg}content/excel.png" class="img-ico"><a href="#">Экспорт в Excel</a>
                        <p class="small-gray">Выгрузка лонглиста в Excel</p>
                    </div>
                    <div class="clr"></div>
                </div>
                <if no.llCandidate><p>Нет ниодного соискателя, удовлетворяющего указанным условиям.</p></if no.llCandidate>
                <ifelse no.llCandidate>
                    <p>В лонглисте <span>2{numLlCandidate}</span> соискателя</p>
                    <table border="0" cellspacing="2" cellpadding="5" class="tbl-yellow-white">
                        <tr>
                            <th>Добавлен в лист</th>
                            <th>ФИО</th>
                            <th>Телефоны</th>
                            <th>Текущий результат</th>
                            <th>Комментарий</th>
                        </tr>
                        <tr>
                            <td>15 мая 2014 10:00</td>
                            <td><p><span class="color-darkred">Петров Петр Петрович</span></p>
                                <p class="smallgray">3456:Менеджер (Спб)</p>
                            </td>
                            <td>+7(812)999-09-09</td>
                            <td><p><span class="bg-darkred">Отправили вакансию. Ожидаем ответа</span></p>
                                <p><img src="{cfgSiteImg}buttons/btn-clock.png" class="img-ico"><a class="block" href="#">История</a> <a href="#" class="block" >  <img src="{cfgSiteImg}buttons/btn-override.png" class="img-ico">Изменить</a></p>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>15 мая 2014 10:00</td>
                            <td><p><span class="color-darkred">Сидоров Петр Михайлович</span></p>
                                <p class="smallgray">3456:Менеджер (Спб)</p>
                            </td>
                            <td>+7(812)998-56-23</td>
                            <td><p><span class="bg-darkred">Отправили вакансию. Ожидаем ответа</span></p></td>
                            <td></td>
                        </tr>
                    </table>
            </ifelse no.llCandidate>
            </if longlists>
            <if search_resume>
                <h1>Расширенный поиск резюме</h1>
                <form action="/search" id="adv_searh_form" method="post">
                    <input type="hidden" id="type" name="type" value="resumes"/>
                <div class=" block yellow_box">
                    <table class="tbl-flex bottom30" >
                        <tr>
                            <td><select name="search_city" class="red-border width710">
                                    <option value="0">Любой город</option>
                                    <loop city.names.resume>
                                        <option value="{id}">{city}</option>
                                    </loop city.names.resume>
                            </select></td><td></td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" name="query" class="red-border width695" value="" placeholder="Ключевые слова или ID резюме"/>
                            </td><td></td>
                        </tr>
                        <tr>
                            <td >
                                <select name="searchWhere" class="red-border width320">
                                    <option value="0">только в названии</option>
                                    <option value="1">везде</option>
                                </select>
                            </td>

                            <td>
                                <select name="searchHow" class="red-border width320">
                                    <option value="0">все слова</option>
                                    <option value="1">хотя бы одно слово</option>
                                </select>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <input type="text" name="subQuery" class="red-border width695" value="" placeholder="исключить слова"/>
                            </td><td></td>
                        </tr>
                        <tr>
                            <td><select name="categoryId" class="red-border width710">
                                    <option value="0">любое направление деятельности</option>
                                    <loop option.group.category.resume>
                                        <optgroup label="{strOptGroupName}">
                                            <loop group.options.resume.{intGroupID}>
                                                <option value="{intOptionValue}">{strOptionTitle}</option>
                                            </loop group.options.resume.{intGroupID}>
                                        </optgroup>
                                    </loop option.group.category.resume>
                                </select></td><td></td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" name="trips" class="red-border" value = '' /><span class="top10">отображать готовых переехать</span>
                            </td><td></td>
                        </tr>
                        <tr>
                            <td><p class="left20">З/п от <input type="text" name="salary_from" class="red-border width70" value = '' /> до <input type="text" name="salary_to" class="red-border width70" value = '' /> руб.</p>
                            </td>
                            <td><input type="checkbox" name="resumeReadyGo" class="red-border" value = '' /><span class="top10">отображать готовых переехать</span></td>
                        </tr>
                        <tr>
                            <td><select name="cbxEdu" class="red-border width320">
                                    <option value="0">образование не важно</option>
                                    <loop education.types.resume>
                                        <option value="{id}">{jet_title}</option>
                                    </loop education.types.resume>
                                </select>
                            </td>
                            <td><select name="sbxWorkBusy" class="red-border width320">
                                    <option value="0">график работы не важен</option>
                                    <loop workbusy.types.resume>
                                        <option value="{id}">{jw_title}</option>
                                    </loop workbusy.types.resume>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><select name="searchGender" class="red-border width320">
                                    <option value="0">пол не важен</option>
                                    <option value="1">мужской</option>
                                    <option value="0">женский</option>
                                </select>
                            </td>
                            <td><input type="checkbox" name="resumeNoSex" class="red-border" value = '' /><span class="top10">выводить без указанного пола</span></td>
                        </tr>
                        <tr>
                            <td> <p class="left20">Возраст от <input type="text" name="age_from" class="red-border width50" value = '' /> до <input type="text" name="age_to" class="red-border width50" value = '' /> руб.</p>
                            </td>
                            <td><input type="checkbox" name="cbxNoAge" class="red-border" value = '' /><span class="top10">выводить без указанного возраста</span></td>
                        </tr>
                        <tr>
                            <td><select name="resumeLang" class="red-border width320">
                                    <option value="0">знание языка не обязательно</option>
                                </select>
                            </td>
                            <td><select name="resumeLevel" class="red-border width320">
                                    <option value="0">уровень не важен</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><select name="sbxPeriod" class="red-border width710">
                                    <option value="0">за все время</option>
                                    <option value="1">за прошедшие 24 часа</option>
                                    <option value="2">за 3 дня</option>
                                    <option value="3">за неделю (7 дней)</option>
                                    <option value="4">за 2 недели (14 дней)</option>
                                    <option value="5">за месяц</option>
                                    <option value="6">за 3 месяца</option>
                                </select></td><td></td>
                        </tr>
                    </table>
                    <input type="submit" value="Найти" name="btnResumeSearch" id = "btnResumeSearch" class="disp-right"/>
                </div>
                </form>
            </if search_resume>

            <if favorite_resume>
                <h1>Избранные резюме</h1>
                <div class=" block yellow_box red-border padding20 min-height50">
                <p class="inform"> На данной странице обображаются резюме, которые Вы добавили с список Избранных. Если Вы более не желаете видеть какое-либо резюме в данном списке,
                    нажмите на иконку в виде звездочки слева от названия резюме.</p>
                </div>
                <div class=" block">
                    <if no.favorite.resume><p>Вы пока не добавили ни одно резюме в список избранных. </p></if no.favorite.resume>
                    <ifelse no.favorite.resume>
                        <table  border="0" cellspacing="2" cellpadding="5" class="tbl-yellow-white">
                            <loop list.favorite.resume>
                                <tr>
                                    <td class="star"><a href="javascript:void(0)" onclick="confirmDelete('{strCurrentUrl}?resid={jr_id}&status=rmf')"><img src="{cfgSiteImg}star.png" title="удалить из Избранных"/></a></td>
                                    <td class="aln-left"><a href="{cfgSiteUrl}resume/{jr_id}"><span class="title aln-left">{jr_fio}</span></a><br />
                                        <span class="txt_grey txt_12 aln-left">{jr_title}</span></td>
                                    <td><span class="black">{jr_city}</span></td>
                                    <td><span class="salary">{strSalary}</span><br /><span class="txt_darkgrey txt_12">{strDate}</span></td>
                                    <td><span class="txt_12">{strWhen}</span></td>
                                    <td><a href="#uc" class="uc"><img src="{cfgSiteImg}envelop2.png" title="написать обращение" /></a></td>
                                </tr>
                            </loop list.favorite.resume>
                        </table>
                    </ifelse no.favorite.resume>
                </div>
            </if favorite_resume>

            <if subscriptions_resume>
                <h1>Подписки на резюме</h1>

                <if no.searches><p>У Вас пока нет ни одной подписки на резюме.</p></if no.searches>
                <ifelse no.searches>
                    <table border="0" cellspacing="2" cellpadding="5" class="tbl-yellow-white">
                        <tr>
                            <th>Название</th>
                            <th>Статус</th>
                            <th>Фильтры</th>
                        </tr>
                        <loop list.search.resume>
                            <tr>
                                <td><a href="{cfgSiteUrl}search/params/{sus_id}"><span class="title">{sus_title}</span></a></td>
                                <td><span class="color-red">Активна</span><br /><a href="javascript:void(0)" onclick="confirmDelete('{strCurrentUrl}?srid={sus_id}&status=rm')">Отменить</a></td>
                                <td><span class="txt_12 txt_darkgrey">{strParams}</span></td>
                            </tr>
                        </loop list.search.resume>
                    </table>
                </ifelse no.searches>
            </if subscriptions_resume>

            <if blacklist_resume>
                <h1>Черный список резюме</h1>
                <div class=" block yellow_box red-border padding20 min-height50">
                    <p class="inform"> На данной странице представлен черный список резюме. Резюме, присутствующие в этом списке,не отображаются на сайте.
                        Если Вы случайно поместили сюда какое-либо резюме, то можете удалить его, нажав на кнопку слева от названия резюме.</p>
                </div>
                <div class=" block">
                    <if no.blacklist.resume><p>Вы пока не добавили ни одно резюме в чёрный список. </p></if no.blacklist.resume>
                    <ifelse no.blacklist.resume>

                        <table  border="0" cellspacing="2" cellpadding="5" class="tbl-yellow-white"
                        <loop list.blacklist.resume>
                            <tr>

                                <td class="star"><a href="javascript:void(0)" onclick="confirmDelete('{strCurrentUrl}?resid={jr_id}&status=rmbl')"><img src="{cfgSiteImg}envelop2.png" title="Удалить из черного списка" /></a></td>
                                <td class="aln-left"><a href='{cfgSiteUrl}resume/{jr_id}'><span class="black">{jr_fio}, <br />{jr_age}</span></a></td>
                                <td class="aln-left"><span class="color-darkred">Должность:</span><span class="black"> {jr_title}</span><br/>
                                    <span class="color-darkred">Обязанности:</span><span class="black"></span>
                                </td>
                                <td><span class="black">{jr_city}</span></td>
                                <td><span class="salary">{strSalary}</span><br /><span class="txt_darkgrey txt_12">{strDate}</span></td>
                                <td><span class="txt_12">{jr_views}</span></td>

                            </tr>
                        </loop list.blacklist.resume>
                        </table>
                    </ifelse no.blacklist.resume>
                </div>
            </if blacklist_resume>





            <if sent_resume>
                <h1>Присланные резюме</h1>
                <div class=" block yellow_box padding20 min-height50">
                    <p> Уточнения по вакансиям</p>
                    <select name="categoryId" class="red-border width320">
                        <option value="0">любое направление деятельности</option>
                        <loop option.group.category.resume.sent>
                            <optgroup label="{strOptGroupName}">
                                <loop group.options.resume.sent.{intGroupID}>
                                    <option value="{intOptionValue}">{strOptionTitle}</option>
                                </loop group.options.resume.sent.{intGroupID}>
                            </optgroup>
                        </loop option.group.category.resume.sent>
                    </select>
                </div>
                <div class=" block">

                        <table  border="0" cellspacing="2" cellpadding="5" class="tbl-yellow-white">
                            <tr>
                                <th></th>
                                <th>Дата</th>
                                <th>Резюме</th>
                                <th>Вакансия</th>
                                <th>Мой статус в ЛЛ</th>
                                <th>    </th>
                            </tr>

                        <loop list.sent.resume>
                            <tr>

                                <td class="star"><a href="javascript:void(0)" onclick="confirmDelete('{strCurrentUrl}?resid={jr_id}&status=rmbl')"><img src="{cfgSiteImg}envelop2.png" title="" /></a></td>
                                <td></td>
                                <td class="aln-left"></td>
                                <td><span class="black"></span></td>
                                <td><span class="salary"></span></td>

                                <td class="star"><a href="javascript:void(0)" onclick="confirmDelete('{strCurrentUrl}?resid={jr_id}&status=rmsnt')"><img src="{cfgSiteImg}envelop2.png" title="Удалить" /></a></td>

                            </tr>
                        </loop list.sent.resume>
                        </table>

                </div>
            </if sent_resume>







            <if profile>
                <h1>Мой профиль</h1>

                <form action="" method="post">
                    <input type="hidden" id="strSID" name="strSID" value="{strSID}"/>
                    <table>
                        <colgroup>
                            <col class="col_titles"/>
                            <col class="col_fields"/>
                        </colgroup>
                        <tr>
                            <td class="section_title">Личные данные</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Фамилия</td>
                            <td><input type="text" id="strLName" name="strLName" value="{strLName}" class="plane w240"
                                       maxlength="255"/></td>
                        </tr>
                        <tr>
                            <td>Имя</td>
                            <td><input type="text" id="strFName" name="strFName" value="{strFName}" class="plane w240"
                                       maxlength="255"/></td>
                        </tr>
                        <tr>
                            <td>Отчество</td>
                            <td><input type="text" id="strMName" name="strMName" value="{strMName}" class="plane w240"
                                       maxlength="255"/></td>
                        </tr>
                        <tr>
                            <td class="section_title">Изменение пароля</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Действующий пароль</td>
                            <td><input type="password" id="strOldPassword" name="strOldPassword" class="plane w240" maxlength="255"/></td>
                        </tr>
                        <tr>
                            <td>Новый пароль</td>
                            <td><input type="password" id="strNewPassword" name="strNewPassword" class="plane w240" maxlength="255"/></td>
                        </tr>
                        <tr>
                            <td>Повторите новый пароль</td>
                            <td><input type="password" id="strConfirmPassword" name="strConfirmPassword" class="plane w240" maxlength="255"/><br/>
                                <span class="note">Если Вы не собираетесь менять пароль, просто оставьте поля пустыми.</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="section_title">Контактный E-mail</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>E-mail / Логин</td>
                            <td><input type="text" id="strEmail" name="strEmail" value="{strEmail}" class="plane w240"
                                       maxlength="255"/><br />
                                <span class="note">Данный E-mail может использоваться для восстановления пароля или же для контактов администрации портала с Вами. Если Вы создадите подписки, то они будут поступать на этот адрес.</span>
                            </td>
                        </tr>
                        <tr>
                            <td>Уведомления</td>
                            <td><input type="checkbox" id="cbxSubsribe" name="cbxSubsribe" class="plane"/><label for="cbxSubsribe">Присылать мне уведомления о сообщениях от других пользователей</label></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type = 'button' value = "Сохранить"/></td>
                        </tr>
                    </table>
                </form>
            </if profile>
        </div>
    </if jur>
<!--SEP 777-->
    <if fiz>
        <div class="left_sidebar">
            <div class="resumes block">
                <h3>Резюме</h3>

                <ul>
                    <li><a href="/profile">Мой профиль</a></li>
                    <li><a href="/profile/resumes">Мои резюме</a></li>
                    <li><a href="/form_add_resume">Разместить резюме</a></li>

                    <if hidden>
                    <li><a href="/profile/vip">VIP резюме</a></li>
                    </if hidden>
                    <li><a href="{cfgSiteCurrentUri}logout=1">Выход</a></li>
                </ul>
            </div>
            <div class="vacancies block">
                <h3>Вакансии</h3>

                <ul>
                    <li><a href="/profile/search_vacancies">Поиск вакансий</a></li>
                    <if hidden>
                    <li><a href="/profile/favorites_vacancies">Избранные</a></li>
                    <li><a href="/profile/subscriptions">Подписки</a></li>
                    <li><a href="/profile/blacklist">Чёрный список</a></li>
                    </if hidden>
                </ul>
            </div>
            <if hidden>
            <div class="messages block">
                <h3>Внутренняя почта</h3>

                <ul>
                    <li><a href="/profile/invitations">Приглашения и отклики</a></li>
                    <li><a href="/profile/msg_templates">Шаблоны сообщений</a></li>
                </ul>
            </div>
            </if hidden>
        </div>

        <div class="content">
            <if profile>
                <h1>Мой профиль</h1>

                <form action="" method="post">
                    <input type="hidden" id="strSID" name="strSID" value="{strSID}"/>
                <table>
                    <colgroup>
                        <col class="col_titles"/>
                        <col class="col_fields"/>
                    </colgroup>
                    <tr>
                        <td class="section_title">Личные данные</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Фамилия</td>
                        <td><input type="text" id="strLName" name="strLName" value="{strLName}" class="plane w240"
                                   maxlength="255"/></td>
                    </tr>
                    <tr>
                        <td>Имя</td>
                        <td><input type="text" id="strFName" name="strFName" value="{strFName}" class="plane w240"
                                   maxlength="255"/></td>
                    </tr>
                    <tr>
                        <td>Отчество</td>
                        <td><input type="text" id="strMName" name="strMName" value="{strMName}" class="plane w240"
                                   maxlength="255"/></td>
                    </tr>
                    <tr>
                        <td class="section_title">Изменение пароля</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Действующий пароль</td>
                        <td><input type="password" id="strOldPassword" name="strOldPassword" class="plane w240" maxlength="255" <if is.new.user>value="{strNewPassword}"</if is.new.user>/> <if is.new.user>Ваш пароль: {strNewPassword}</if is.new.user></td>
                    </tr>
                    <tr>
                        <td>Новый пароль</td>
                        <td><input type="password" id="strNewPassword" name="strNewPassword" class="plane w240" maxlength="255"/></td>
                    </tr>
                    <tr>
                        <td>Повторите новый пароль</td>
                        <td><input type="password" id="strConfirmPassword" name="strConfirmPassword" class="plane w240" maxlength="255"/><br/>
                            <span class="note">Если Вы не собираетесь менять пароль, просто оставьте поля пустыми.</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="section_title">Контактный E-mail</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>E-mail / Логин</td>
                        <td><input type="text" id="strEmail" name="strEmail" value="{strEmail}" class="plane w240"
                                   maxlength="255"/><br />
                            <span class="note">Данный E-mail может использоваться для восстановления пароля или же для контактов администрации портала с Вами. Если Вы создадите подписки, то они будут поступать на этот адрес.</span>
                        </td>
                    </tr>
                    <tr>
                        <td>Уведомления</td>
                        <td><input type="checkbox" id="cbxSubsribe" name="cbxSubsribe" class="plane"/><label for="cbxSubsribe">Присылать мне уведомления о сообщениях от других пользователей</label></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><button>Сохранить</button></td>
                    </tr>
                </table>
                </form>
            </if profile>

            <if resumes>
                <h1>Мои резюме</h1>

                <if no.resumes><p>Список пуст. Пока Вы не создали ни одного резюме. <a href="/form_add_resume">Разместите</a> своё первое резюме.</p></if no.resumes>
                <ifelse no.resumes>
                <table border="0" cellspacing="2" cellpadding="5" class="profile_list">
                    <tr>
                        <th>ID</th>
                        <th class="col-title">Название</th>
                        <th class="col-date">Дата</th>
                        <th>Статус</th>
                        <th>Просмотров</th>
                    </tr>
                    <loop list.resumes>
                    <tr>
                        <td class="ac"><b>{jr_id}</b></td>
                        <td><span class="title">{jr_title}</span><br />
                            <if published.{jr_id}><a href="{cfgSiteUrl}resume/{jr_id}">Просмотреть</a> | </if published.{jr_id}><a href="{cfgSiteUrl}form_add_resume/{jr_id}">Редактировать</a><!-- | <a href="#uc" class="uc">Повысить эффективность</a-->
                        </td>
                        <td><span class="date_pub">{date_publish}</span></td>
                        <td>{strStatus}<br />
                            <if published.{jr_id}><a href="{strCurrentUrl}?rid={jr_id}&status=N">Снять с публикации</a></if published.{jr_id}><ifelse published.{jr_id}><a href="{strCurrentUrl}?rid={jr_id}&status=Y">Опубликовать</a></ifelse published.{jr_id}><!-- | <a href="javascript:void(0)" onclick="confirmDelete('{strCurrentUrl}?rid={jr_id}&status=D')">Удалить резюме</a-->
                        </td>
                        <td class="ac"><span>{jr_views}</span></td>
                    </tr>
                    </loop list.resumes>
                </table>
                </ifelse no.resumes>
            </if resumes>

            <if vip>
                <h1>VIP резюме</h1>

                <p>Эта услуга позволит найти работу в кратчайшие сроки! В течение <b>7 суток</b> Ваше резюме будет находиться на <b>первых местах</b> в результатах поиска и в каталоге резюме и будет <b>выделено цветом</b>! </p>
            </if vip>

            <if search_vacancies>
                <h1>Расширенный поиск вакансии</h1>

                <form action="/search" id="adv_searh_form" method="post">
                    <input type="hidden" id="type" name="type" value="vacancies"/>
                    <div class="advanced_search_box vacancies">
                        <div class="block">
                            <input type="text" id="strAdvTitle" name="query" class="plane" maxlength="255" placeholder="Введите поисковую фразу: название должности или города или ID вакансии"/>
                        </div>

                        <div class="block">
                            <div class="half first-child">
                                <select name="searchWhere" class="plane">
                                    <option value="0">только в названии</option>
                                    <option value="1">везде</option>
                                </select>
                            </div>
                            <div class="half">
                                <select name="searchHow" class="plane">
                                    <option value="0">все слова</option>
                                    <option value="1">хотя бы одно слово</option>
                                </select>
                            </div>
                            <div class="clearer"></div>
                        </div>

                        <div class="block">
                            <select name="categoryId" class="catSelector plane">
                                <option value="">любое направление деятельности</option>
                                <loop option.group.category>
                                    <optgroup label="{strOptGroupName}">
                                        <loop group.options.{intGroupID}>
                                        <option value="{intOptionValue}">{strOptionTitle}</option>
                                </loop group.options.{intGroupID}>
                                </optgroup>
                                </loop option.group.category>
                            </select>
                        </div>

                        <div class="block">
                            <input type="text" name="subQuery" class="plane" maxlength="255" placeholder="исключить слова"/>
                        </div>

                        <div class="block">
                            <div class="half">
                                <span>Зарплата от:</span>
                                <input type="text" id="strSalaryFrom" name="search_salary" class="small money" value="0" maxlength="9" placeholder="от" /> <span>руб.</span>
                            </div>
                            <div class="half">
                                <input type="checkbox" id="isNoSalary" name="isNoSalary" class="plane fl"/> <label for="isNoSalary">- выводить без указанной з/п</label>
                            </div>
                            <div class="clearer"></div>
                        </div>

                        <div class="block">
                            <div class="half first-child">
                                <span>Образование:</span>
                                <div>
                                    <select name="cbxEdu" id="sbxEdu" class="plane">
                                        <option value="">любое</option>
                                        <loop education.types>
                                            <option value="{jet_id}">{jet_title}</option>
                                        </loop education.types>
                                    </select>
                                </div>
                            </div>
                            <div class="half">
                                <span>График работы:</span>
                                <div>
                                    <select name="cbxWorkBusy" class="plane">
                                        <option value="">любой</option>
                                        <loop workbusy.types>
                                            <option value="{jw_id}">{jw_title}</option>
                                        </loop workbusy.types>
                                    </select>
                                </div>
                            </div>
                            <div class="clearer"></div>
                        </div>

                        <div class="block">
                            <div class="half first-child">
                                <div>
                                    <select name="sbxCompanyType" class="plane">
                                        <option value="">прямые работодатели и КА</option>
                                        <option value="1">только прямые работодатели</option>
                                        <option value="2">только кадровые агенства</option>
                                    </select>
                                </div>
                            </div>
                            <div class="half">
                                <div>
                                    <select name="sbxPeriod" class="plane">
                                        <option value="">за всё время</option>
                                        <option value="1">за прошедшие 24 часа</option>
                                        <option value="2">за 3 дня</option>
                                        <option value="3">за неделю (7 дней)</option>
                                        <option value="4">за 2 недели (14 дней)</option>
                                        <option value="5">за месяц</option>
                                        <option value="6">за 3 месяца</option>
                                    </select>
                                </div>
                            </div>
                            <div class="clearer"></div>
                        </div>

                        <div class="block">
                            <div class="clearer">
                                <input type="checkbox" id="cbxDrive" name="cbxDrive" class="plane fl"/> <label for="cbxDrive">- Наличие водительского удостоверения</label>
                            </div>
                            <div class="clearer">
                                <input type="checkbox" id="cbxTrips" name="cbxTrips" class="plane fl"/> <label for="cbxTrips">- Готовность к командировкам</label>
                            </div>
                        </div>

                        <div class="block">
                            <input type="submit" value="Найти" class="plane-b"/>
                        </div>
                    </div>
                </form>

                <script type="text/javascript">
                    $(function (){
                        $('#strSalaryFrom').spinner({
                            min: 0,
                            step: 5000
                        });
                    });
                </script>
            </if search_vacancies>

            <if favorites_vacancies>
                <h1>Избранные вакансии</h1>

                <div class="block-note">На данной странице отображаются вакансии, которые Вы добавили в список Избранных. Если Вы не желаете более видеть какую-либо вакансию в данном списке, нажмите на иконку в виде звездочки слева от названия вакансии.</div>

                <if no.favorite.vacancies><p>Вы пока не добавили ни одну вакансию в список избранных. </p></if no.favorite.vacancies>
                <ifelse no.favorite.vacancies>
                <table border="0" cellspacing="2" cellpadding="5" class="favorite_vacancies_list profile_list">
                    <loop list.favorite.vacancies>
                    <tr>
                        <td class="star"><a href="javascript:void(0)" onclick="confirmDelete('{strCurrentUrl}?vid={jv_id}&status=rmf')"><img src="{cfgSiteImg}star.png" title="удалить из Избранных"/></a></td>
                        <td><a href="{cfgSiteUrl}vacancy/{jv_id}" class="title">{jv_title}</a><br /><span class="txt_grey txt_12">{jc_title}</span></td>
                        <td><span class="black">{jv_city}</span></td>
                        <td><span class="salary">{strSalary}</span><br /><span class="txt_darkgrey txt_12">{strDate}</span></td>
                        <td><span class="txt_12">{strWhen}</span></td>
                        <td><a href="#uc" class="uc"><img src="{cfgSiteImg}envelop2.png" title="написать обращение" /></a></td>
                    </tr>
                    </loop list.favorite.vacancies>
                </table>
                </ifelse no.favorite.vacancies>
            </if favorites_vacancies>

            <if subscriptions>
                <h1>Подписка на вакансии</h1>

                <if no.searches><p>У Вас пока нет ни одной подписки на вакансии.</p></if no.searches>
                <ifelse no.searches>
                <table border="0" cellspacing="2" cellpadding="5" class="profile_list">
                    <tr>
                        <th>Название</th>
                        <th>Статус</th>
                        <th>Фильтры</th>
                    </tr>
                <loop list.search.vacancies>
                    <tr>
                        <td><a href="{cfgSiteUrl}search/params/{sus_id}" class="title">{sus_title}</a></td>
                        <td><span>Активна</span><br /><a href="javascript:void(0)" onclick="confirmDelete('{strCurrentUrl}?svid={sus_id}&status=rm')">Отменить</a></td>
                        <td><span class="txt_12 txt_darkgrey">{strParams}</span></td>
                    </tr>
                </loop list.search.vacancies>
                </table>
                </ifelse no.searches>
            </if subscriptions>

            <if blacklist>
                <h1>Чёрный список</h1>

                <div class="block-note">На этой странице представлен черный список вакансии. Вакансии, присутствующие в этом списке, не отображаются на сайте при условии когда Вы авторизованы. Если Вы случайно поместили сюда какую-либо вакансию, то можете удалить ее, нажав на кнопку слева от названия вакансии. </div>

                <if no.blacklist.vacancies><p>Ваш Черный список пока пуст. </p></if no.blacklist.vacancies>
                <ifelse no.blacklist.vacancies>
                <table border="0" cellspacing="2" cellpadding="5" class="favorite_vacancies_list profile_list">
                    <loop list.blacklist.vacancies>
                        <tr>
                            <td class="star"><a href="javascript:void(0)" onclick="confirmDelete('{strCurrentUrl}?vid={jv_id}&status=rmbl')"><img src="{cfgSiteImg}finger_down.png" title="удалить из черых списков"/></a></td>
                            <td><a href="{cfgSiteUrl}vacancy/{jv_id}" class="title">{jv_title}</a><br /><span class="txt_grey txt_12">{jc_title}</span></td>
                            <td><span class="black">{jv_city}</span></td>
                            <td><span class="salary">{strSalary}</span><br /><span class="txt_darkgrey txt_12">{strDate}</span></td>
                            <td><span class="txt_12">{strWhen}</span></td>
                        </tr>
                    </loop list.blacklist.vacancies>
                </table>
                </ifelse no.blacklist.vacancies>
            </if blacklist>

            <if invitations>
                <h1>Приглашения и отклики</h1>

                <p>У Вас нет непрочитанных сообщений.</p>
            </if invitations>

            <if msg_templates>
                <h1>Шаблоны сообщений</h1>

                <loop user.templates>
                <div class="row">
                    <div class="clearer">
                        <p class="title fl">{sut_title}</p>
                        <p class="fr"><a href="#" rel="#edit_template{sut_id}" class="edit_template_trigger">редактировать</a></p>
                    </div>

                    <p>{strTemplateBody}</p>
                </div>

                <div id="edit_template{sut_id}" class="overlay edit_template">
                    <h1>{sut_title}</h1>

                    <form action="" method="post" id="form_edit_template">
                        <input type="hidden" name="type" value="customTemplate"/>
                        <input type="hidden" name="templateID" value="{sut_id}"/>
                        <textarea id="strTemplate" name="strTemplate" class="plane">{sut_body}</textarea>

                        <div>
                            <span>@FIO@</span> - имя получателя сообщения <br/>
                            <span>@MyFIO@</span> - Ваше ФИО.
                        </div>

                        <div class="tr">
                            <input type="submit" name="submit" value="Сохранить" class="plane-b finger"/>
                        </div>
                    </form>
                </div>
                </loop user.templates>
            </if msg_templates>
        </div>
    </if fiz>
</if authed>
<ifelse authed>
    <p>К сожалению Вы не авторизованы!</p>
    <p>Чтобы воспользоваться инструментами Личного кабинета, пожалуйста введите свой логин и пароль в <a href="#" rel="#overlay_login_box" class="auth_form_trigger">форме авторизации</a>.</p>
    <p>Если у Вас ещё нет пароля для входа в Личный кабинет, Вы можете получить его заполнив <a href="{cfgSiteUrl}registration#new">форму регистрации</a>.</p>
    <p>Если Вы забыли или потеряли пароль, воспользуйтесь <a href="#" rel="#overlay_recovery_box" class="recovery_form_trigger">формой восстановления пароля</a>.</p>
</ifelse authed>
</div>


<if is.msg>
    <div id="profile_msg" class="warn">{strMessage}</div>
    <script type="text/javascript">
        $(function(){
            var status = {msgStatus};
            if (status>0) {
                $('#profile_msg').removeClass().addClass('err');
            } else {
                $('#profile_msg').removeClass().addClass('success');
            }
            $.fancybox('#profile_msg');
        });
    </script>
</if is.msg>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        var sizeW = 565;
        var sizeH = 260;

        $(".edit_template_trigger").overlay({
            top: 0
            , left: ($(window).width() - sizeW)/2 - 50
            , mask: '#939393'
            , fixed: false
            , closeOnClick: true
        });

        $('.radio-btn').click(function(){
            $('.radio-btn > div').hide();
            $(this).children('div').show();
        });
        $('#longlistDateFrom, #longlistDateTill').datepicker();

    });
</script>
