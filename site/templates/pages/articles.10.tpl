<h1>Статьи</h1>

<div class="cnt">
    {blockPaginator}
</div>

<div class="records-list">
    <loop last.articles>
        <div class="record-cell">

            <div class="record-box">
                <div class="news-text">
                    <span class="date ">{strDatePublishArticle}</span>
                    <p class="txt-block"><a href="{cfgSiteUrl}article/{sa_id}" class="link-blue">{sa_title}</a></p>
                </div>
                <if articles.picture.{sa_id}>
                    <div class="fr news-image"><a href="{cfgAllImg}articles/articles.photo.{sa_id}.b.jpg{strUniquePhoto}" id="hrefImg{sa_id}"><img src="{cfgAllImg}articles/articles.photo.{sa_id}.jpg{strUniquePhoto}" border="0" width="200" /></a></div>
                </if articles.picture.{sa_id}>
                <div class="clearer"></div>
            </div>

        </div>
        <div class="clearer"></div>
    </loop last.articles>
</div>

<div class="cnt">
    {blockPaginator}
</div>
