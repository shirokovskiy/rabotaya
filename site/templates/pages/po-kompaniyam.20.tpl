<a name="search"></a>
<h1 class="title">Список компаний</h1>

<div id="rus_abc" class="clearer">
    <ul class="in_row">
        <loop rus.chars>
            <li><a href="/po-kompaniyam/company-by-first-char/{charUrl}">{char}</a></li>
        </loop rus.chars>
    </ul>
</div>
<div id="eng_abc" class="clearer">
    <ul class="in_row">
        <loop eng.chars>
            <li><a href="/po-kompaniyam/company-by-first-char/{charUrl}">{char}</a></li>
        </loop eng.chars>
    </ul>
</div>
<div id="num_abc" class="clearer">
    <ul class="in_row">
        <li><a href="/po-kompaniyam/company-by-first-char/0">0 - 9</a></li>
    </ul>
</div>

<div class="block">{blockPaginator}</div>
<div id="search_results" class="list">
    <loop list.results>
        <div class="row">
            <div class="cell col1"><a href="{cfgSiteUrl}{link}/{id}"><b>{strTitle}</b></a> <p>{strDescription}</p></div>
            <div class="cell clearer"><span class="salary">{strSalary}</span></div>
        </div>
    </loop list.results>
    <if no.results>
        <p>По заданным критериям ничего не найдено!</p>
    </if no.results>
</div>