<h1 class="title">Отписаться от рассылки</h1>

<if error>
    <div class="err unsubscribe_msg">Ошибка: {strErrorMsg}</div>
</if error>

<if success>
    <div class="success unsubscribe_msg">{strSuccessMsg}</div>
</if success>

<form action="/unsubscribe/post/{UnSID}" method="post">
<div class="block-note" id="unsubscribe">
    Для того чтобы отменить информационную рассылку уведомлений на Ваш электронный ящик, укажите его адрес в этом поле:
    <div class="input">
        <input type="email" required id="email" name="email" value="{email}" class="plane" maxlength="255" placeholder="Здесь введите Ваш E-mail"/>
    </div>
    <input type="submit" name="submit" value="Я не хочу получать рассылку" class="plane-b finger"/>
</div>
</form>
