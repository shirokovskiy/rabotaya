<h3>{sa_title}</h3>

<div class="cnt">
    <if picture>
        <a href="{cfgAllImg}articles/articles.photo.{sa_id}.b.jpg{strUniqueMainPhoto}" id="pictureMain"><img src="{cfgAllImg}articles/articles.photo.{sa_id}.jpg{strUniqueMainPhoto}" width="200" border="0" align="right" style="margin:0 0 5px 10px" alt="{sa_img_desc}"></a>
    </if picture>

    <span class="date"><i>{strDatePublishArticle}</i></span>
    <p style="margin:12px 0 4px 0">{strArticleBody}</p>

    <p>« <a href="{cfgSiteUrl}articles?stPage={stPage}">к списку статей</a></p>
</div><!-- // cnt -->
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('a#pictureMain').fancybox({
            'titleShow'		: true
            , 'titlePosition'	: 'over'
        });
    });
</script>