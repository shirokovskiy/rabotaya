<h3>Регистрация на сайте</h3>
<a name="new"></a>
<script type="text/javascript">
//<![CDATA[
jQuery(function(){
    $('#fizlitso').click(function(){
        $('.big_list').toggle();
        $('#fizlitso_box').toggle();
    });
    $('#jurlitso').click(function(){
        $('.big_list').toggle();
        $('#jurlitso_box').toggle();
    });

    /**
     * Нажата кнопка регистрации
     */
    $('#fizlitso_form #submit').click(function(){
        alert('Test-1');
    });

    /*$('#jurlitso_form #submit').click(function(){
        sendForm('#jurlitso_form', '/lib/api/registration.php');
    });*/

    /* attach a submit handler to the form */
    $(".reg_form form").submit(function(event) {
        /* stop form from submitting normally */
        event.preventDefault();

        /* get some values from elements on the page: */
        var $form = $( this ),
            url = $form.attr( 'action'),
            validation = true,
            type = $form.find('input[name=strType]').val(),
            strEmail = $( '#'+type+'_strEmail' ).val();

        if (isEmpty(strEmail)) {
            validation = false;
            $.fancybox('Ошибка: Вы не заполнили поле Email.');
            $( '#'+type+'_strEmail').addClass('err').change(function(){
                $(this).removeClass('err');
            });
            return false;
        }

        /**
         * Проверка обязательных полей
         */
        var errors = 0;
        $(".reg_form form#"+$form.id+" input.required").map(function(){
            if( !$(this).val() ) {
                $(this).addClass('err').change(function(){
                    $(this).removeClass('err');
                });
                errors++;
            } else if ($(this).val()) {
                $(this).removeClass('err');
            }
        });
        if(errors > 0){
            $.fancybox('Ошибка: поля, отмеченные красной снежинкой, обязательны для заполнения.');
            return false;
        }

        var strPassword = $( '#'+type+'_strPassword' ).val();
        var strPasswordRepeat = $( '#'+type+'_strPasswordRepeat' ).val();

        if ((strPasswordRepeat != strPassword && !isEmpty(strPassword)) || isEmpty(strPasswordRepeat) || isEmpty(strPassword)) {
            // Если пароли не совпадают
            validation = false;
            $.fancybox('Ошибка: Вы неверно заполнили поля для паролей. Пароль должен быть введён дважды и одинаково.');
            $( '#'+type+'_strPassword').addClass('err').change(function(){
                $(this).removeClass('err');
                $('#'+type+'_strPasswordRepeat').removeClass('err');
            });
            $( '#'+type+'_strPasswordRepeat').addClass('err').change(function(){
                $(this).removeClass('err');
                $('#'+type+'_strPassword').removeClass('err');
            });
            return false;
        }

        if (validation) {
            /* Send the data using post */
            var posting = $.post( url, $form.serialize(), null, 'json' );

            /* Put the results in a div */
            posting.done(function( data ) {
                if (data.status == 1) {
//                    console.log('OK');
                    $form.find("input[type=text]").val('');
                    $('.big_list').toggle();
                    $form.parent().toggle();

                    // todo: перебросить человека в Личный кабинет (автоматическая авторизация)
                } else {
                    console.log('NO RESULT. ERRORS!');
                }

                $.fancybox(data.message);
            });
        }
    });

    /**
     * Нажата кнопка Назад
     */
    $('.reg_form .block_a>a').click(function(){
        $('.big_list').toggle();
        $(this).parent().parent().toggle();
    });
});
//]]>
</script>

<ul class="big_list">
    <li><a href="javascript:void(0)" id="fizlitso">Я соискатель, ищу работу</a></li>
    <li><a href="javascript:void(0)" id="jurlitso">Я представитель компании, ищу сотрудника, или провожу тренинги</a></li>
</ul>

<div id="fizlitso_box" class="reg_form">
    <form id="fizlitso_form" action="/lib/api/registration.php">
        <input type="hidden" name="strType" value="fiz"/>
        <div class="clearer">
            <div class="fl col1">Email <span class="redstar">*</span></div>
            <div class="fl ui-widget"><input type="text" required id="fiz_strEmail" name="strEmail" class="plane required" maxlength="255" placeholder="ваш@email"/></div>
        </div>
        <div class="clearer">
            <div class="fl col1">Придумайте пароль <span class="redstar">*</span></div>
            <div class="fl ui-widget"><input type="text" required id="fiz_strPassword" name="strPassword" class="plane required" maxlength="255"/></div>
        </div>
        <div class="clearer">
            <div class="fl col1">Подтвердите пароль <span class="redstar">*</span></div>
            <div class="fl ui-widget"><input type="text" required id="fiz_strPasswordRepeat" name="strPasswordRepeat" class="plane required" maxlength="255"/></div>
        </div>
        <div class="clearer">
            <div class="fl col1">Фамилия</div>
            <div class="fl ui-widget"><input type="text" id="fiz_strLname" name="strLname" class="plane" maxlength="255"/></div>
        </div>
        <div class="clearer">
            <div class="fl col1">Имя</div>
            <div class="fl ui-widget"><input type="text" id="fiz_strFname" name="strFname" class="plane" maxlength="255"/></div>
        </div>
        <div class="clearer">
            <div class="fl col1">Отчество</div>
            <div class="fl ui-widget"><input type="text" id="fiz_strMname" name="strMname" class="plane" maxlength="255"/></div>
        </div>
        <div class="clearer">
            <div class="fl col1">Телефон</div>
            <div class="fl ui-widget"><input type="text" id="fiz_strPhone" name="strPhone" class="plane" maxlength="255"/></div>
        </div>
        <div class="clearer">
            <div class="fl col1"></div>
            <div class="fl ui-widget"><input type="submit" id="fiz_submit" name="submit" value="Зарегистрироваться" class="ui-button" /></div>
        </div>
        <div class="agreement">
            <input type="checkbox" id="fiz_cbxAgree" name="cbxAgree" value="Y" class="plane" />
            <label for="fiz_cbxAgree"> Я прочитал(а) и полностью согласен(на) с <a href="#fizAgreement" class="fb">Пользовательским соглашением</a></label>
        </div>
    </form>

    <div class="block_a ac"><a href="javascript:void(0)">Назад</a></div>
</div>
<div id="jurlitso_box" class="reg_form">
    <form id="jurlitso_form" action="/lib/api/registration.php">
        <input type="hidden" name="strType" value="jur"/>
        <div class="clearer">
            <div class="fl col1">Email <span class="redstar">*</span></div>
            <div class="fl ui-widget"><input type="text" required id="jur_strEmail" name="strEmail" class="plane required" maxlength="255" placeholder="ваш@email"/></div>
        </div>
        <div class="clearer">
            <div class="fl col1">Придумайте пароль <span class="redstar">*</span></div>
            <div class="fl ui-widget"><input type="text" required id="jur_strPassword" name="strPassword" class="plane required" maxlength="255"/></div>
        </div>
        <div class="clearer">
            <div class="fl col1">Подтвердите пароль <span class="redstar">*</span></div>
            <div class="fl ui-widget"><input type="text" required id="jur_strPasswordRepeat" name="strPasswordRepeat" class="plane required" maxlength="255"/></div>
        </div>
        <div class="clearer">
            <div class="fl col1">Фамилия</div>
            <div class="fl ui-widget"><input type="text" id="jur_strLname" name="strLname" class="plane" maxlength="255"/></div>
        </div>
        <div class="clearer">
            <div class="fl col1">Имя</div>
            <div class="fl ui-widget"><input type="text" id="jur_strFname" name="strFname" class="plane" maxlength="255"/></div>
        </div>
        <div class="clearer">
            <div class="fl col1">Отчество</div>
            <div class="fl ui-widget"><input type="text" id="jur_strMname" name="strMname" class="plane" maxlength="255"/></div>
        </div>
        <div class="clearer">
            <div class="fl col1">Должность</div>
            <div class="fl ui-widget"><input type="text" id="jur_strPosition" name="strPosition" class="plane" maxlength="255"/></div>
        </div>
        <div class="clearer">
            <div class="fl col1">Телефон <span class="redstar">*</span></div>
            <div class="fl ui-widget"><input type="text" required id="jur_strPhone" name="strPhone" class="plane required" maxlength="255"/></div>
        </div>
        <div class="clearer">
            <div class="fl col1">ИНН компании <span class="redstar">*</span></div>
            <div class="fl ui-widget"><input type="text" required id="jur_strInn" name="strInn" class="plane required" maxlength="255"/></div>
        </div>
        <!--<div class="clearer">
            <div class="fl col1"></div>
            <div class="fl ui-widget"><img id="captcha" src="{secureImage}" alt="CAPTCHA Image" /></div>
        </div>-->

        <div class="clearer">
            <div class="fl col1"></div>
            <div class="fl ui-widget"><input type="submit" id="jur_submit" name="submit" value="Зарегистрироваться" class="ui-button" /></div>
        </div>
        <div class="agreement">
            <input type="checkbox" id="jur_cbxAgree" name="cbxAgree" value="Y" class="plane" />
            <label for="jur_cbxAgree"> Я прочитал(а) и полностью согласен(на) с <a href="#jurAgreement" class="fb">Пользовательским соглашением</a></label>
        </div>
    </form>

    <div class="block_a ac"><a href="javascript:void(0)">Назад</a></div>
</div>


<div class="hidden">
    <div id="fizAgreement" class="block_agreement">
        <h2>Правила при регистрации на <a href="{cfgSiteUrl}">Rabota-ya.ru</a></h2>
        <ul class="discs">
        <li>Регистрация включает в себя комплекс информационных услуг по содействию в трудоустройстве, а именно:<br/>
            −размещение, редактирование и удаление анкеты Соискателя на <a href="{cfgSiteUrl}">Rabota-ya.ru</a>;<br/>
            −обеспечение доступа потенциальных работодателей к анкете Соискателя;<br/>
            −отправка Соискателю вакансий через <a href="{cfgSiteUrl}">Rabota-ya.ru</a>. посредством различных каналов связи( интернет, смс,);<br/>
            −доступ ко всем открытым возможностям сайта <a href="{cfgSiteUrl}">Rabota-ya.ru</a>.<br/>
            −консультации Соискателя по вопросам работы с <a href="{cfgSiteUrl}">Rabota-ya.ru</a>., включая консультации по составлению анкеты;<br/>
            −консультации предоставляются по электронной почте, по телефону и онлайн чат на сайте <a href="{cfgSiteUrl}">Rabota-ya.ru</a>.
        </li>
        <li>Акцепт оферты осуществляется путем регистрации Соискателя на <a href="{cfgSiteUrl}">Rabota-ya.ru</a> любым из способов:<br/>
            −через сайт <a href="{cfgSiteUrl}">Rabota-ya.ru</a>;<br/>
            −звонком на номер 305-305-7.
        </li>
        <li>Доступ Соискателя к Личной регистрации осуществляется при активации его логина и пароля, которые присваиваются Соискателю в момент его регистрации на <a href="{cfgSiteUrl}">Rabota-ya.ru</a>.</li>
        <li>Анкета Соискателя должна оформляться в соответствии с шаблоном и Правилами размещения резюме на <a href="{cfgSiteUrl}">Rabota-ya.ru</a>, которые могут быть установлены и изменены <a href="{cfgSiteUrl}">Rabota-ya.ru</a> в любое время. Правила публикуются на сайте <a href="{cfgSiteUrl}">Rabota-ya.ru</a>. Новые правила вступают в силу с момента публикации и не распространяются на уже размещенные на сайте анкеты. Соискатель в полном объеме несет ответственность за соблюдение указанных Правил. В случае нарушения Соискателем Правил <a href="{cfgSiteUrl}">Rabota-ya.ru</a> оставляет за собой право удалить личную анкету Соискателя без предупреждения.</li>
        <li>Мобильный телефон, который пользователь указывает в подписке на рассылку вакансий используется только для связи с пользователем и обеспечения доступа пользователя к параметрам подписки.</li>
        <li><a href="{cfgSiteUrl}">Rabota-ya.ru</a> не несет ответственности при использовании Соискателем <a href="{cfgSiteUrl}">Rabota-ya.ru</a>: за косвенные/непрямые убытки и/или упущенную выгоду Соискателя и/или третьих лиц</li>
        <li><a href="{cfgSiteUrl}">Rabota-ya.ru</a> не несет ответственности при ее использовании за достоверность информации, указанной в вакансиях, размещенных на <a href="{cfgSiteUrl}">Rabota-ya.ru</a>.</li>
        <li>В случае, если размещение информации Соискателем явилось основанием для предъявления к <a href="{cfgSiteUrl}">Rabota-ya.ru</a> претензий, исков третьих лиц и/или предписаний по уплате штрафных санкций со стороны государственных органов в связи с нарушением прав третьих лиц и/или законодательства, Соискатель обязуется незамедлительно по требованию Биржи предоставить всю запрашиваемую информацию, касающуюся размещения и содержания информации, содействовать <a href="{cfgSiteUrl}">Rabota-ya.ru</a> в урегулировании таких претензий и исков, а также возместить все убытки, причиненные <a href="{cfgSiteUrl}">Rabota-ya.ru</a> вследствие предъявления ему таких претензий, исков, предписаний.</li>
        <li>Споры подлежат рассмотрению в Арбитражном суде г. Санкт-Петербург.</li>
        <li>Соискатель понимает, что при размещении на <a href="{cfgSiteUrl}">Rabota-ya.ru</a> его персональные данные будут являться общедоступными, и <a href="{cfgSiteUrl}">Rabota-ya.ru</a> не несет ответственности за сохранение их конфиденциальности или использование третьими лицами.</li>
        <li>Соискатель выражает свое согласие на право <a href="{cfgSiteUrl}">Rabota-ya.ru</a> использовать размещенную им персональную информацию анонимно и в обобщенном виде для статистических целей, а также для таргетинга рекламы, размещаемой на <a href="{cfgSiteUrl}">Rabota-ya.ru</a>.</li>
        <li>Портал <a href="{cfgSiteUrl}">Rabota-ya.ru</a> не несет ответственности за отказ работодателей в трудоустройстве клиента.</li>
        <li>Обстоятельствами, исключающими ответственность Сторон за невыполнение обязательств, являются обстоятельства непреодолимой силы или иные не зависящие от воли Сторон обстоятельства <a href="{cfgSiteUrl}">Rabota-ya.ru</a> не несет ответственности за убытки и другие последствия, наступившие в связи с использованием или невозможностью использования услуг по вине Соискателя.<br/>
            <a href="{cfgSiteUrl}">Rabota-ya.ru</a> обязуется не предоставлять никакой персональной информации о Соискателях третьим лицам, заявляющим о возможном нецелевом использовании подобной информации.<br/>
            <a href="{cfgSiteUrl}">Rabota-ya.ru</a> не является представителем ни Соискателей, публикующих на <a href="{cfgSiteUrl}">Rabota-ya.ru</a> свои анкеты, ни работодателей, размещающих вакансии. Любые договоренности между Соискателями и работодателями, являются двусторонними, и <a href="{cfgSiteUrl}">Rabota-ya.ru</a> не имеет к ним отношения.
        </li>
        <li>Строго запрещено размещение открытой рекламы, а так же объявлений содержащих информацию интимного характера.</li>
        </ul>
    </div>

    <div id="jurAgreement">
        <h2>На Портале <a href="{cfgSiteUrl}">Rabota-ya.ru</a> производится строгий контроль качества размещаемых объявлений о вакансиях. Контроль производится как автоматически, с помощью специальных фильтров, так и вручную, модераторами.</h2>
        <ul>
        <li>1. К публикации НЕ допускаются объявления следующего вида:</li>
        <li>1.1 содержащие нецензурную лексику;</li>
        <li>1.2 не являющиеся предложениями работы;</li>
        <li>1.3 предлагающие участие в различного рода финансовых пирамидах, сетевом маркетинге, а также других организациях, предлагающих заработок исключительно за счет получения процентов; объявления, содержащие предложение стать бизнес-партнером также не допускаются;</li>
        <li>1.4 предлагающие для записи на собеседование платную отправку SMS или платный звонок менеджеру организации;</li>
        <li>1.5 предлагающие работу в сети Интернет и на дому сомнительного характера;</li>
        <li>1.6 предлагающие перейти на какой-либо сайт или написать на какой-либо e-mail для получения информации о вакансии;</li>
        <li>1.7 подразумевающие оказание интимных услуг;</li>
        <li>1.8 содержащие рекламу, в том числе скрытую;</li>
        <li>1.9 содержащие ссылки, не имеющие отношения к организации-работодателю;</li>
        <li>1.10 противоречащие ст. 4 ФЗ «О средствах массовой информации» «Недопустимость злоупотребления свободой массовой информации» («…не допускается использование средств массовой информации в целях совершения уголовно наказуемых деяний, для разглашения сведений, составляющих государственную или иную специально охраняемую законом тайну, для распространения материалов, содержащих публичные призывы к осуществлению террористической деятельности или публично оправдывающих терроризм, других экстремистских материалов, а также материалов, пропагандирующих порнографию, культ насилия и жестокости...»);</li>
        <li>1.11 требующие заключения договора обязательного пенсионного страхования с каким-либо НПФ для устройства на работу или для бесплатного оказания услуг по содействию в трудоустройстве.</li>
        <li>1.12 Администрация Портала оставляет за собой право отказать в публикации вакансии без объяснения причин.</li>
        <li>2. Объявление о вакансии должно содержать в себе предложение работы и соответствовать следующим требованиям:</li>
        <li>2.1 объявление о вакансии должно содержать в себе предложение одной конкретной позиции;</li>
        <li>2.2 объявление должно быть размещено в соответствующих вакансии специализациях, объявление не может быть одновременно размещено более чем в 5 специализациях;</li>
        <li>2.3 текст объявления должен быть составлен на русском или английском языке, не допускаются объявления, написанные прописными буквами, транслитерацией и другими способами, затрудняющими чтение;</li>
        <li>2.4 Каждое поле должно быть заполнено в соответствии с его названием («условия работы», «должностные обязанности», «профессиональные навыки» и т.д.), не допускается дублирование одного и того же текста во все поля формы вакансии;</li>
        <li>2.5 в заголовке вакансии должно содержаться только название одной конкретной должности (в единственном числе, именительном падеже), другой информации в заголовке быть не должно;</li>
        <li>2.6 контактные данные работодателя должны быть указаны только в специально отведенных для этого полях.</li>
        <li>2.7 запрещено указывать в вакансии ограничения по полу / возрасту кандидатов и иным признакам, не связанным с деловыми качествами работников.</li>
        <li>3. Объявление о вакансии может быть размещено бессрочно, но при этом может быть снято работодателем в любое время по его желанию.</li>
        <li>4. Объявление о вакансии может быть обновлено для поднятия его в выдаче не чаще одного раза в неделю. Запрещается удалять вакансию и создавать заново такую же с целью поднятия её в результатах выдачи.</li>
        <li>5. Одновременное размещение копий одной и той же вакансии расценивается как спам.</li>
        <li>6. В случае обнаружения на Портале опубликованной вакансии, не соответствующей Правилам размещения, любой пользователь имеет право отправить жалобу модераторам Портала, нажав на соответствующую ссылку на странице вакансии.</li>
        <li>7. Объявления о вакансих, не соответстующие данным правилам, будут удаляться с Портала.</li>
        <li>8. Администрация Портала оставляет за собой право в одностороннем порядке, без согласования с кем-либо, принимать решения о публикации тех или иных вакансий и другой информации.</li>
        <li>9. Администрация Портала не несет ответственности за содержащуюся в вакансиях информацию.</li>
        </ul>
    </div>
</div>
