<if list>
<h1>Словарь профессий</h1>

<div id="rus_abc" class="clearer">
    <ul class="in_row">
        <loop rus.chars>
            <li><a href="/dictionary/words-by-first-char/{charUrl}">{char}</a></li>
        </loop rus.chars>
    </ul>
</div>
<div id="eng_abc" class="clearer">
    <ul class="in_row">
        <loop eng.chars>
            <li><a href="/dictionary/words-by-first-char/{charUrl}">{char}</a></li>
        </loop eng.chars>
    </ul>
</div>

<div id="allWords" class="parameters">
    <a href="/dictionary">Весь словарь</a>
</div>

<div id="selectedChar" class="parameters">
    <p>Выбрана буква: <b>{charSelected}</b></p>
</div>

<script type="text/javascript">
    var showParams = {showParams};
    jQuery(document).ready(function ($) {
        if (showParams) {
            $('#allWords').show();
            $('#selectedChar').show();
        }
    });
</script>

<div class="cnt">
    {blockPaginator}
</div>

<div class="records-list">
    <loop dictionary.words>
        <div class="record-cell clearer">
            <div class="record-box">
                <div class="news-text">
                    <p class="txt-block"><a href="{cfgSiteUrl}dictionary/{urlParam}" class="link-blue">{sa_title}</a></p>
                </div>
                <if dictionary.picture.{sa_id}>
                <div class="fr news-image"><a href="{cfgAllImg}dictionary/dictionary.photo.{sa_id}.b.jpg{strUniquePhoto}" id="hrefImg{sa_id}"><img src="{cfgAllImg}dictionary/dictionary.photo.{sa_id}.jpg{strUniquePhoto}" border="0" width="200" /></a></div>
                </if dictionary.picture.{sa_id}>
                <div class="clearer"></div>
            </div>
        </div>
    </loop dictionary.words>
    <if no.records>
    <p class="err">Нет записей для заданных параметров!</p>
    </if no.records>
</div>

<div class="cnt">
    {blockPaginator}
</div>
</if list>

<if record>
    <h3>{sa_title}</h3>

    <div class="cnt">
        <if picture>
            <a href="{cfgAllImg}dictionary/dictionary.photo.{sa_id}.b.jpg{strUniqueMainPhoto}" id="pictureMain"><img src="{cfgAllImg}dictionary/dictionary.photo.{sa_id}.jpg{strUniqueMainPhoto}" width="200" border="0" align="right" style="margin:0 0 5px 10px" alt="{sa_img_desc}"></a>
        </if picture>

        <p style="margin:12px 0 4px 0">{strArticleBody}</p>

        <p>« <a href="{cfgSiteUrl}dictionary?stPage={stPage}">к списку слов</a></p>
    </div><!-- // cnt -->
</if record>
