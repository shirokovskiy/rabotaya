<a name="results"></a>
<h3>Результаты поиска {strWhatSearched}</h3>
<if authed.and.not.saved><div class="box-subscribe"><a href="#" rel="#overlay_searchname_box" id="profile-rss" class="link-darkred">Подписаться на выбранные параметры поиска</a></div></if authed.and.not.saved>
<div class="block">{blockPaginator}</div>

<div id="search_results" class="list">
<loop list.results>
    <div class="row">
        <div class="cell col1"><a href="{cfgSiteUrl}{link}/{strSeoUrl}"><b>{strTitle}</b></a> <p>{strDescription}</p></div>
        <div class="cell clearer"><span class="salary">{strSalary}</span></div>
    </div>
</loop list.results>
<if no.results>
    <p>По заданным критериям ничего не найдено!</p>

    <if cat.search>
        <p><a href="/po-otraslyam{strSearchAnotherCat}">выбрать другую отрасль</a></p>
    </if cat.search>
</if no.results>
</div>

<div class="block">{blockPaginator}</div>

<if show.seo.text>
<div class="box-seo-text">{seo.text}</div>
</if show.seo.text>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $("body,html").animate({"scrollTop": 119}, 500);

        /*$('#profile-rss').click(function(){
            // Send ajax
            var url = '/lib/api/profile_rss.php?r=' + dounique();
            var form = {
                action: 'setRss'
            };
            $.post(url, form, function(data){ // do it on answer
                console.log('JSON data', data);
                if (data.status == 1) {
                    $('#profile-rss').hide();
                    $.fancybox('Вы успешно подписались на указанные параметры поиска.');
                } else {
                    // Error output
                    $.fancybox('Ошибка: ' + data.message);
                }
            }, 'json');
        });*/
        var sizeW = 385; /* also see in jq.tools.css for #overlay rules */
        var sizeH = 140;

        $('#profile-rss').overlay({
            top: ($(window).height() - sizeH)/2,
            left: ($(window).width() - sizeW)/2 - 50,
            mask: '#939393',
            fixed: false,
            closeOnClick: true
        });

        $('#overlay_searchname_box').css('width',sizeW);
        $("#overlay_searchname_box form").submit(function(event) {
            /* stop form from submitting normally */
            event.preventDefault();

            var $form = $( this ),
                url = $form.attr( 'action'),
                validation = true,
                strSearchName = $( '#strSearchName' ).val();

            if (isEmpty(strSearchName)) {
                validation = false;
                alert('Ошибка: Вы не заполнили поле Наименование фильтра');
                $( '#strSearchName').addClass('err').change(function(){
                    $(this).removeClass('err');
                });
                return false;
            }

            if (validation) {
                /* Send the data using post */
                var posting = $.post( url, $form.serialize(), null, 'json' );

                /* Put the results in a div */
                posting.done(function( data ) {
//                    console.log('JSON data', data);
                    if (data.status == 1) {
                        $('#profile-rss').overlay().close();
                        $('#profile-rss').hide();
                        $.fancybox('Вы успешно подписались на указанные параметры поиска.');
                    } else {
                        // Error output
                        $.fancybox('Ошибка: ' + data.message);
                    }
                });
            }
        });
    });
</script>

<div id="overlay_searchname_box" class="overlay">
    <div class="contentWrap">
        <form action="/lib/api/profile_rss.php" method="post">
            <input type="hidden" name="action" value="setRss"/>
            <h1 class="title">Присвойте имя фильтру</h1>
            <input type="text" id="strSearchName" name="strSearchName" class="plane" maxlength="64" />
            <button>Сохранить</button>
        </form>
    </div>
</div>
