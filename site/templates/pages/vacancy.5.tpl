<h3>Вакансия: {strTitle} (<span>ID: {intVacancyId}</span>)</h3>
<if not.authed>
<div class="right-top-block-link">
    <a href="{cfgSiteUrl}my-vacancies">Редактировать / Добавить в архив</a>
</div>
</if not.authed>

<table class="info-table" border="0">
    <tr>
        <td id="logo-cell">
            <if is.logo>
                <img class="company-logo" src="{cfgAllImg}companies/{si_filename}" alt="{strCompanyName}" title="{strCompanyName}"/>
            </if is.logo>
            <if is.web>
                <p>Сайт: <a href="{strCompanyWeb}" target="_blank" class="link-darkred">{strCompanyWeb}</a></p>
            </if is.web>
            <if authed>
                <div class="authorized-box">
                <frg fragment.sidebar.profile.menu>
                </div>
            </if authed>
        </td>
        <td id="content-cell">
            <if is.strCompany>
            <h1 class="title">{strCompany}</h1>
            </if is.strCompany>

            <div class="box-contacts-btn">
                <button>Контактная информация</button>
            </div>

            <table class="list">
                <colgroup>
                    <col class="first">
                    <col>
                </colgroup>

                <if is.strSalary>
                <tr>
                    <td>Заработная плата</td>
                    <td class="salary-cell">{strSalary}</td>
                </tr>
                </if is.strSalary>

                <tr>
                    <td>Город</td>
                    <td>{strCity}</td>
                </tr>

                <if is.strEdu>
                <tr>
                    <td>Образование</td>
                    <td>{strEdu}</td>
                </tr>
                </if is.strEdu>

                <if is.strWorkbusy>
                    <tr>
                        <td>Предстоящая занятость</td>
                        <td>{strWorkbusy}</td>
                    </tr>
                </if is.strWorkbusy>

                <if is.strDriver>
                    <tr>
                        <td>Наличие водительского удостоверения</td>
                        <td>{strDriver}</td>
                    </tr>
                </if is.strDriver>

                <if is.strTrips>
                    <tr>
                        <td>Готовность к командировкам</td>
                        <td>{strTrips}</td>
                    </tr>
                </if is.strTrips>
            </table>

            <div id="contact-info">
                <div class="small-title">Контактная информация</div>
                <table class="list">
                    <colgroup>
                        <col class="first">
                        <col>
                    </colgroup>

                    <if is.strFio>
                    <tr>
                        <td>Контактное лицо</td>
                        <td>{strFio}</td>
                    </tr>
                    </if is.strFio>

                    <if is.strSex>
                    <tr>
                        <td>Пол кандидата</td>
                        <td>{strSex}</td>
                    </tr>
                    </if is.strSex>

                    <if is.strPhone>
                    <tr>
                        <td>Телефон</td>
                        <td>{strPhone}</td>
                    </tr>
                    </if is.strPhone>

                    <if is.strEmail>
                    <tr>
                        <td>Email</td>
                        <td>{strEmail}</td>
                    </tr>
                    </if is.strEmail>
                </table>

                <div class="note">
                    Сообщите, пожалуйста,<br/>что информацию о вакансии<br/>Вы узнали на сайте газеты
                    <div><span>РАБОТА ОТ А ДО Я</span><div style="clear: both"></div></div>
                    Спасибо.
                </div>
            </div>

            <if is.strDescription>
            <div class="small-title">Дополнительная информация</div>
            <div class="description">{strDescription}</div>
            </if is.strDescription>

            <br/><br/>

            <table class="list">
                <colgroup>
                    <col class="first">
                    <col>
                </colgroup>
                <tr>
                    <td>Дата публикации</td>
                    <td>{strDatePublish}</td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('.box-contacts-btn button').click(function() {
            $('#contact-info').show();
            if (!$(this).hasClass('inactive')) {
                $(this).addClass('inactive');
            }
        });
    });
</script>


<div class="ac block_m20"><a href="{strBackUri}" class="link-darkred">{strBackTitle}</a></div>
