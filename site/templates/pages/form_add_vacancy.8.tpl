<h3>Создать вакансию</h3><a class="light_grey fb link-inline-note" href="#vAgreement">Правила размещения вакансий</a>
<a name="form"></a>
<script type="text/javascript">
    $(function(){
        $('.picker').datepicker({
            showOn: "button",
            buttonImage: "{cfgAdminImg}calendar_icon.png",
            buttonImageOnly: true,
            dateFormat: "dd.mm.yy",
            changeYear: true
        });
        $('#strDateFrom').datepicker({
            showOn: "button",
            buttonImage: "{cfgAdminImg}calendar_icon.png",
            buttonImageOnly: true,
            dateFormat: "dd.mm.yy",
            changeYear: true,
            onClose: function( selectedDate ) {
                $( "#strDateTo" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $('#strDateTo').datepicker({
            showOn: "button",
            buttonImage: "{cfgAdminImg}calendar_icon.png",
            buttonImageOnly: true,
            dateFormat: "dd.mm.yy",
            changeYear: true,
            onClose: function( selectedDate ) {
                $( "#strDateFrom" ).datepicker( "option", "maxDate", selectedDate );
            }
        });

        $('#strSalaryFrom').spinner({
            min: 0,
            step: 5000,
            change: function( event, ui ) {
                $('#strSalaryTo').spinner({
                    min: $('#strSalaryFrom').val(),
                    step: 5000
                });
            },
            spin: function( event, ui ) {
                $('#strSalaryTo').spinner({
                    min: $('#strSalaryFrom').val(),
                    step: 5000
                });
            }
        });
        $('#strSalaryTo').spinner({
            min: 0,
            step: 5000,
            change: function( event, ui ) {
                $('#strSalaryFrom').spinner({
                    max: $('#strSalaryTo').val(),
                    step: 5000
                });
            },
            spin: function( event, ui ) {
                $('#strSalaryFrom').spinner({
                    max: $('#strSalaryTo').val(),
                    step: 5000
                });
            }
        });
        $( "#rbEdu" ).buttonset();
        $( "#rbSex" ).buttonset();
        $( "#cbxTrips" ).button().change(function(){
            if($(this).is(":checked")) {
                $( "#lTrips>span" ).text("да");
            } else {
                $( "#lTrips>span" ).text("нет");
            }
        });
        if($("#cbxTrips").is(":checked")) {
            $( "#lTrips>span" ).text("да");
        } else {
            $( "#lTrips>span" ).text("нет");
        }
        $( "#cbxDriver" ).button().change(function(){
            if($(this).is(":checked")) {
                $( "#lDriver>span" ).text("обязательно");
            } else {
                $( "#lDriver>span" ).text("неважно");
            }
        });
        if($("#cbxDriver").is(":checked")) {
            $( "#lDriver>span" ).text("обязательно");
        } else {
            $( "#lDriver>span" ).text("неважно");
        }

        var citiesSource = [{strCities}];
        $('#strCity').autocomplete({source: citiesSource, minLength: 3});
        $('#strCompanyCity').autocomplete({source: citiesSource, minLength: 3});
        $('#strPhone').input_mask("9999999");
        //$('#strPhoneCode').input_mask("99999");
        //$('#strPhoneExt').input_mask("99999");
        $('#strCompanyPhone').input_mask("9999999");
        //$('#strCompPhoneCode').input_mask("99999");
        //$('#strCompPhoneExt').input_mask("99999");
        $("body,html").animate({"scrollTop": 106}, 1500);
    });
</script>

<if is.msg>
    <script type="text/javascript">
        $(function(){
            var status = {msgStatus};
            if (status>0) {
                $('#add_vacancy_msg').removeClass().addClass('err');
            } else {
                $('#add_vacancy_msg').removeClass().addClass('success');
            }
            $.fancybox('#add_vacancy_msg');
        });
    </script>
</if is.msg>

<div id="add_vacancy_msg" class="warn">{strMessage}</div>

<form id="add_vacancy_form" class="create_form" method="post">
    <input type="hidden" name="add" value="vacancy"/>
    <table class="table-fixed-col">
        <col class="col1">
        <col class="col2">

        <tr>
            <td><p><span class="after-star">Должность</span></p></td>
            <td><input type="text" class="width450" name="strTitle" required value="{strTitle}"  /></td>
        </tr>
        <tr>
            <td><p>Предлагаемая зарплата: </p></td>
            <td><p>от <input type="text" class="width100" name="strSalaryFrom" id="strSalaryFrom" value="{strSalaryFrom}" maxlength="9" placeholder="от" title="от" />
                до <input type="text" class="width100" name="strSalaryTo" id="strSalaryTo" value="{strSalaryTo}" maxlength="9" placeholder="до" title="до" /></p></td>
        </tr>
        <tr>
            <td><p><span class="after-star">Город</span></p></td>
            <td><input type="text" class="width450" id="strCity" name="strCity" value="{strCity}" maxlength="128" placeholder="введите три первых буквы для подсказки" title="введите три первых буквы для подсказки" required /></td>
        </tr>
        <tr>
            <td class="tdL">Требуемое образование:</td>
            <td class="tdR">
                <div id="rbEdu">
                    <loop education.types>
                        <input type="radio" name="rbEdu" id="rbEdu{jet_id}" value="{jet_id}"{checked}/><label for="rbEdu{jet_id}">{jet_title}</label>
                    </loop education.types>
                </div>
            </td>
        </tr>
        <tr>
            <td class="tdL">Занятость: </td>
            <td class="tdR">
                <select name="sbxWorkbusy"  class="width200">
                    <loop workbusy.types>
                        <option value="{jw_id}" {checked}>{jw_title}</option>
                    </loop workbusy.types>
                </select>
            </td>
        </tr>
        <tr>
            <td class="tdL">Наличие водительского удостоверения:</td>
            <td class="tdR"><input type="checkbox" name="cbxDriver" id="cbxDriver" value="Y"{cbxDriverY}/><label id="lDriver" for="cbxDriver">неважно</label></td>
        </tr>

        <tr>
            <td class="tdL">Готовность к командировкам:</td>
            <td class="tdR"><input type="checkbox" id="cbxTrips" name="cbxTrips" value="Y"{cbxTripsY}/><label id="lTrips" for="cbxTrips">нет</label></td>
        </tr>
        <tr>
            <td class="tdL">Дополнительная информация:</td>
            <td class="tdR"><textarea name="strDescription" class="plane">{strDescription}</textarea></td>
        </tr>
        <tr><td colspan="2"><hr></td></tr>
        <tr><td colspan="2"><b>Информация о компании</b></td></tr>

        <tr>
            <td class="tdL">Наименование компании:&nbsp;<span class="redstar">*</span></td>
            <td class="tdR"><input type="text" name="strCompany" value="{strCompany}" maxlength="128" /></td>
        </tr>
        <tr>
            <td class="tdL">Наниматель:</td>
            <td class="tdR">
                <ul>
                    <li class="clearer"><label><input type="radio" name="rbAgency" value="direct"{rbAgency_direct} class="fl"/> &mdash; прямой работодатель</label></li>
                    <li class="clearer"><label><input type="radio" name="rbAgency" value="recrut"{rbAgency_recrut} class="fl"/> &mdash; кадровое агентство</label></li>
                </ul>
            </td>
        </tr>

        <tr>
            <td class="tdL">Город:</td>
            <td class="tdR"><input type="text" id="strCompanyCity" name="strCompanyCity" value="{strCompanyCity}" class="plane" maxlength="128" placeholder="введите три первых буквы для подсказки" title="введите три первых буквы для подсказки"/></td>
        </tr>
        <tr>
            <td class="tdL">Адрес:</td>
            <td class="tdR"><textarea id="strAddress" name="strAddress" class="plane">{strAddress}</textarea></td>
        </tr>
        <tr>
            <td class="tdL">Web-сайт компании:</td>
            <td class="tdR"><input type="text" id="strWeb" name="strWeb" value="{strWeb}" class="plane" maxlength="255" placeholder="http://www.ваш.сайт" title="http://www.ваш.сайт"/></td>
        </tr>
        <tr>
            <td class="tdL">Email компании:</td>
            <td class="tdR"><input type="text" id="strCompanyEmail" name="strCompanyEmail" value="{strCompanyEmail}" class="plane" maxlength="255" placeholder="укажите@email" title="укажите@email"/></td>
        </tr>
        <tr>
            <td class="tdL">Телефон компании:</td>
            <td class="tdR">
                <table>
                    <tr>
                        <td><input type="text" id="strCompPhoneCode" name="strCompPhoneCode" value="{strCompPhoneCode}" class="width60" maxlength="5"></td>
                        <td><input type="text" id="strCompanyPhone" name="strCompanyPhone" value="{strCompanyPhone}" class="width100" required maxlength="7" /></td>
                        <td><input type="text" id="strCompPhoneExt" name="strCompPhoneExt" value="{strCompPhoneExt}" class="width60" maxlength="5"></td>
                    </tr>
                    <tr>
                        <td align="center">код</td>
                        <td align="center">телефон</td>
                        <td align="center">доб.</td>
                    </tr>
                </table>
        </tr>
        <tr>
            <td class="tdL">Описание компании:</td>
            <td class="tdR"><textarea id="strCompanyDescription" name="strCompanyDescription" class="plane">{strCompanyDescription}</textarea></td>
        </tr>

    </table>

    <p class="header-form-create">Контактные данные работодателя</p>
    <table class="table-fixed-col">
        <col class="col1">
        <col class="col2">
        <tr>
            <td><p><span class="after-star">Ф.И.О.</span></p></td>
            <td><input type="text" nclass="width450" name="strContactPerson" value="{strContactPerson}" class="plane" maxlength="255" placeholder="ФИО" title="ФИО"  /></td>
        </tr>

        <tr>
            <td><p><span class="after-star">E-mail</span></p></td>
            <td><input type="text"  class="width450" name="strEmail" value="{strEmail}" class="plane" maxlength="128" placeholder="ваш@email" title="ваш@email"  /></td>
        </tr>
        <tr>
            <td><p><span class="after-star">Телефон</span></p></td>
            <td>
                <table>
                    <tr>
                        <td><input type="text" name="strPhoneCode" class="width60" id="telCode" maxlength="5"></td>
                        <td><input type="text" name="strPhone"  class="width100" id="strPhone" required maxlength="7" /></td>
                        <td><input type="text" name="strPhoneExt" class="width60" id="telExt" maxlength="5"></td>
                    </tr>
                    <tr>
                        <td align="center">код</td>
                        <td align="center">телефон</td>
                        <td align="center">доб.</td>
                    </tr>
                </table>
            </td>
        </tr>

    </table>
    <button class="create-vacancy">Создать</button>
</form>
<frg vacancy.agreement>
