<if is.msg>
<script type="text/javascript">
$(function(){
    var status = {msgStatus}
    if (status>0) {
        $('#add_training_msg').removeClass().addClass('err');
    } else {
        $('#add_training_msg').removeClass().addClass('success');
    }
    $.fancybox('#add_training_msg');
});
</script>
</if is.msg>
<div id="add_training_msg" class="warn">{strMessage}</div>
<div id="form_add_training" style="padding-left: 120px">
    <h1>Добавить тренинг</h1>

    <form method="post">
        <input type="hidden" name="add" value="training"/>
        <div>Название тренинга <span class="redstar">*</span></div>
        <div><input type="text" id="strTitle" name="strTitle" value="{strTitle}" class="plane" maxlength="255"/></div>
        <br/>
        <div>Описание</div>
        <div><textarea id="strDescription" name="strDescription" style="height:50px;overflow-y:auto;word-wrap:break-word"
                       class="plane">{strDescription}</textarea></div>
        <br/>
        <div>Адрес</div>
        <div><input type="text" id="strAddress" name="strAddress" value="{strAddress}" class="plane" maxlength="255"/></div>
        <br/>
        <div>Телефон</div>
        <div><input type="text" id="strPhone" name="strPhone" value="{strPhone}" class="plane w120" maxlength="255"/></div>
        <br/>
        <div>E-mail</div>
        <div><input type="text" id="strEmail" name="strEmail" value="{strEmail}" class="plane" maxlength="255" placeholder="например: ivanoff@kmail.zom"/></div>
        <br/>
        <div>Сайт (web-страница)</div>
        <div><input type="text" id="strWeb" name="strWeb" value="{strWeb}" class="plane" maxlength="255"/></div>

        <div style="margin: 10px 0">
            <!--CAPTCHA-->
            <img src="?get_img=captchaTrain" border="0" class="fl inl">
            <input type="text" name="strCodeString" class="plane" maxlength="4" style="width:56px;letter-spacing:5px" onKeyUp="if(this.value.length >= this.maxLength) this.blur(); return false;">
            <span>введите буквы и цифры с картинки</span>
            <!--//CAPTCHA-->
        </div>

        <div class="ac" style="margin-top: 30px">
            <button style="padding: 3px 7px; height: 34px">Отправить</button>
        </div>
    </form>
</div>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('#strPhone').input_mask("(999) 999-9999");
    });
</script>
