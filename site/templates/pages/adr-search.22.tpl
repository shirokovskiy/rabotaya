<h3>Расширенный поиск резюме</h3>

<form action="/search" id="adv_searh_form" method="post">
    <input type="hidden" id="type" name="type" value="resumes"/>
    <div class="advanced_search_box resumes">
        <div class="block">
            <input type="text" id="strAdvTitle" name="query" class="plane" maxlength="255" placeholder="Введите поисковую фразу: должность или город или ID резюме"/>
        </div>

        <div class="block">
            <div class="half">
                <span>Искать фразу:</span>
                <select name="searchWhere">
                    <option value="0">только в названии</option>
                    <option value="1">везде</option>
                </select>
            </div>
            <div class="half">
                <span>и:</span>
                <select name="searchHow">
                    <option value="0">все слова</option>
                    <option value="1">хотя бы одно слово</option>
                </select>
            </div>
            <div class="clearer"></div>
        </div>

        <div class="block">
            <select name="categoryId" class="catSelector">
                <option value="">любое направление деятельности</option>
                <loop option.group.category>
                    <optgroup label="{strOptGroupName}">
                        <loop group.options.{intGroupID}>
                            <option value="{intOptionValue}">{strOptionTitle}</option>
                        </loop group.options.{intGroupID}>
                    </optgroup>
                </loop option.group.category>
            </select>
        </div>

        <div class="block">
            <input type="text" name="subQuery" class="plane" maxlength="255" placeholder="исключить слова"/>
        </div>

        <div class="block">
                <ul class="n3col">
                    <li class="col_s">
                        <span>Зарплата от:</span>
                        <div>
                            <input type="text" id="strSalaryFrom" name="salary_from" class="small money" value="0" maxlength="9" placeholder="от" />
                        </div>
                    </li>
                    <li class="col_s">
                        <span>Зарплата до:</span>
                        <div>
                            <input type="text" id="strSalaryTo" name="salary_to" class="small money" maxlength="9" placeholder="укажите" />
                        </div>
                    </li>
                    <li>
                        <div class="clearer">
                            <input type="checkbox" id="cbxSalaryNotFixed" name="cbxSalaryNotFixed" class="plane fl"/> <label for="cbxSalaryNotFixed">- с з/п по договоренности</label>
                        </div>
                    </li>
                </ul>
        </div>

        <div class="block">
            <div class="half">
                <span>Образование:</span>
                <div>
                    <select name="cbxEdu" id="sbxEdu">
                        <option value="">любое</option>
                        <loop education.types>
                            <option value="{jet_id}">{jet_title}</option>
                        </loop education.types>
                    </select>
                </div>
            </div>
            <div class="half">
                <span>График работы:</span>
                <div>
                    <select name="sbxWorkBusy">
                        <option value="">любой</option>
                        <loop workbusy.types>
                            <option value="{jw_id}">{jw_title}</option>
                        </loop workbusy.types>
                    </select>
                </div>
            </div>
            <div class="clearer"></div>
        </div>

        <div class="block">
            <div class="half">
                <select name="searchGender">
                    <option value="">Пол кандидата не важен</option>
                    <option value="male">мужской</option>
                    <option value="female">женский</option>
                </select>
            </div>
            <div class="half">
                <select name="marriage" id="sbxMarriage">
                    <option value="">Семейное положение не важно</option>
                    <option value="Y">женат / замужем</option>
                    <option value="N">холост / не замужем</option>
                </select>
            </div>
            <div class="clearer"></div>
        </div>

        <div class="block">
            <div class="half">
                <ul class="in_row">
                    <li><p>Возраст от </p></li>
                    <li><input type="text" id="strAgeFrom" name="age_from" class="x-small age" value="0" maxlength="9" placeholder="от" /></li>
                    <li><p>до:</p></li>
                    <li><input type="text" id="strAgeTo" name="age_to" class="x-small age" maxlength="9" placeholder="укажите" /></li>
                </ul>
            </div>
            <div class="half">
                <div class="clearer par">
                    <input type="checkbox" id="cbxNoAge" name="cbxNoAge" class="plane fl"/> <label for="cbxNoAge">- без указанного возраста</label>
                </div>
            </div>
            <div class="clearer"></div>
        </div>

        <div class="block">
            <div class="clearer">
                <input type="checkbox" id="cbxDrive" name="cbxDrive" class="plane fl"/> <label for="cbxDrive">- наличие водительских прав</label>
            </div>
        </div>

        <div class="block">
            <div class="half">
                <select name="trips" id="sbxTrips">
                    <option value="">Командировки не важны</option>
                    <option value="Y">готов(-а) к командировкам</option>
                    <option value="N">не готов(-а) к командировкам</option>
                    <option value="S">не частые командировки</option>
                </select>
            </div>
            <div class="half">
                <div class="clearer">
                    <input type="checkbox" id="cbxTrips" name="cbxTrips" class="plane fl"/> <label for="cbxTrips">- отображать готовых переехать</label>
                </div>
            </div>
            <div class="clearer"></div>
        </div>

        <div class="block">
            <div class="half">
                <div>
                    <select name="sbxPeriod">
                        <option value="">за всё время</option>
                        <option value="1">за прошедшие 24 часа</option>
                        <option value="2">за 3 дня</option>
                        <option value="3">за неделю (7 дней)</option>
                        <option value="4">за 2 недели (14 дней)</option>
                        <option value="5">за месяц</option>
                        <option value="6">за 3 месяца</option>
                    </select>
                </div>
            </div>
            <div class="clearer"></div>
        </div>

        <div class="block">
            <input type="submit" value="Найти" class="plane-b"/>
        </div>
    </div>
</form>

<script type="text/javascript">
    $(function (){
        $('#strSalaryFrom').spinner({
            min: 0,
            step: 5000,
            change: function( event, ui ) {
                $('#strSalaryTo').spinner({
                    min: $('#strSalaryFrom').val(),
                    step: 5000
                });
            },
            spin: function( event, ui ) {
                $('#strSalaryTo').spinner({
                    min: $('#strSalaryFrom').val(),
                    step: 5000
                });
            }
        });
        $('#strSalaryTo').spinner({
            min: 0,
            step: 5000,
            change: function( event, ui ) {
                $('#strSalaryFrom').spinner({
                    max: $('#strSalaryTo').val(),
                    step: 5000
                });
            },
            spin: function( event, ui ) {
                $('#strSalaryFrom').spinner({
                    max: $('#strSalaryTo').val(),
                    step: 5000
                });
            }
        });
        $('#strAgeFrom').spinner({
            min: 14,
            step: 1,
            change: function( event, ui ) {
                $('#strAgeTo').spinner({
                    min: $('#strAgeFrom').val(),
                    step: 1
                });
            },
            spin: function( event, ui ) {
                $('#strAgeTo').spinner({
                    min: $('#strAgeFrom').val(),
                    step: 1
                });
            }
        });
        $('#strAgeTo').spinner({
            min: 14,
            step: 1,
            change: function( event, ui ) {
                $('#strAgeFrom').spinner({
                    max: $('#strAgeTo').val(),
                    step: 1
                });
            },
            spin: function( event, ui ) {
                $('#strAgeFrom').spinner({
                    max: $('#strAgeTo').val(),
                    step: 1
                });
            }
        });
        $("body,html").animate({"scrollTop": 119}, 500);
    });
</script>
