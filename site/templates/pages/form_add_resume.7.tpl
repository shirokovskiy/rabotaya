<h3>Создать резюме </h3><a class="light_grey fb link-inline-note" href="#rAgreement">Правила размещения резюме</a>
<a name="form"></a>
<script type="text/javascript">
$(function(){
        $('.picker').datepicker({
            showOn: "button",
            buttonImage: "{cfgAdminImg}calendar_icon.png",
            buttonImageOnly: true,
            dateFormat: "dd.mm.yy",
            changeYear: true
        });
        $('#strDateFrom').datepicker({
            showOn: "button",
            buttonImage: "{cfgAdminImg}calendar_icon.png",
            buttonImageOnly: true,
            dateFormat: "dd.mm.yy",
            changeYear: true,
            onClose: function( selectedDate ) {
                $( "#strDateTo" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $('#strDateTo').datepicker({
            showOn: "button",
            buttonImage: "{cfgAdminImg}calendar_icon.png",
            buttonImageOnly: true,
            dateFormat: "dd.mm.yy",
            changeYear: true,
            onClose: function( selectedDate ) {
                $( "#strDateFrom" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
        $('#strBirthday').datepicker({
            showOn: "button",
            buttonImage: "{cfgAdminImg}calendar_icon.png",
            buttonImageOnly: true,
            dateFormat: "dd.mm.yy",
            changeYear: true,
            showAnim: 'fold',
            defaultDate: '-30Y'
        });

        $('#strSalary').spinner({
            min: 0,
            step: 5000
        });
        $('#rbSex').buttonset();
        $('#rbChildren').buttonset();
        $('#rbMarriage').buttonset();
        $('#rbDriver').buttonset();

        var citiesSource = [{strCities}];
        $('#strCity').autocomplete({source: citiesSource, minLength: 3});
        $('#strRegion').autocomplete({source: citiesSource, minLength: 3});

        var categoriesSource = [{strCategories}];
        $('#strCategory').autocomplete({source: categoriesSource, minLength: 3});
        $('#strPhone').input_mask("(999) 999-9999");
        $('#strICQ').input_mask("9999?999999");
        $("body,html").animate({"scrollTop": 106}, 1500);
    });
</script>

<if is.msg>
<script type="text/javascript">
$(function(){
        var status = {msgStatus};
        if (status>0) {
            $('#add_resume_msg').removeClass().addClass('err');
        } else {
            $('#add_resume_msg').removeClass().addClass('success');
        }
        $.fancybox('#add_resume_msg');
    });
</script>
</if is.msg>


<nav class="center-align clearer">
    <ul class="steps-resume">
        <li><a class="steps-resume-active" id="but-step1" href="#"><span class="big-letter">1</span>Основная информация</a></li>
        <li><a class="steps-resume" id="but-step2" href="javascript:void(0)"><span class="big-letter">2</span>Пожелания к работе</a></li>
        <li><a class="steps-resume" id="but-step3" href="javascript:void(0)"><span class="big-letter">3</span>Опыт <br/> работы</a></li>
        <li><a class="steps-resume" id="but-step4" href="javascript:void(0)"><span class="big-letter">4</span>Образование</a></li>
        <li><a class="steps-resume" id="but-step5" href="javascript:void(0)"><span class="big-letter">5</span>Дополнительная информация</a> </li>
    </ul>
</nav>

<div id="add_resume_msg" class="warn">{strMessage}</div>


<form id="add_resume_form" class="create_form" method="post" enctype="multipart/form-data">
<input type="hidden" name="add" value="resume"/>
<input type="hidden" name="resumeID" value="{resumeID}"/>
<div id="main-info" class="active-block">
    <h3 class="header-form-create">Личные данные</h3>
    <table class="table-fixed-col">
        <col class="col1">
        <col class="col2">
        <tr>
            <td class="right-align"><p><span class="after-star">Ф.И.О.</span></p></td>
            <td><input type="text" class="width450" required id="contact-fio" name="strFio" value="{strFio}" maxlength="128" /></td>
        </tr>
        <tr>
            <td class="right-align"><p><span class="after-star">Пол</span></p></td>
            <td><div id="rbSex">
                <input type="radio" name="rbSex" id="rbSexF" value="female"{rb_female}/><label for="rbSexF">женский</label></li>
                <input type="radio" name="rbSex" id="rbSexM" value="male"{rb_male}/><label for="rbSexM">мужской</label></li>
            </div></td>
        </tr>
        <tr>
            <td class="right-align"><p>Дата рождения</p></td>
            <td><p><input type="text" class="width100" id="contact-birth" name="strBirthday" size='10' maxlength="10" placeholder="ДД.ММ.ГГГГ" value="{strBirthday}"/></p></td>
        </tr>
        <tr>
            <td class="tdL">Возраст:</td>
            <td class="tdR"><label><input type="checkbox" id="cbxAgeOnly" name="cbxAgeOnly" value="Y"{cbxAgeOnly} class="plane"/> &mdash; отображать только возраст / скрыть дату (чтобы вычислить возраст, необходимо ввести дату рождения)</label></td>
        </tr>
        <tr>
            <td class="tdL">Семейное положение:</td>
            <td class="tdR"><input type="checkbox" id="cbxMarriage" name="cbxMarriage" value="Y"{cbxMarriage} class="plane"/><label for="cbxMarriage"> &mdash; женат / замужем</label></td>
        </tr>
        <tr>
            <td class="tdL">Дети:</td>
            <td class="tdR">
                <div id="rbChildren">
                    <input type="radio" name="rbChildren" id="rbChildrenY" value="Y"{rbChildren_Y}/><label for="rbChildrenY">да (один или более)</label>
                    <input type="radio" name="rbChildren" id="rbChildrenN" value="N"{rbChildren_N}/><label for="rbChildrenN">нет</label>
                </div>
            </td>
        </tr>
        <!--tr>
            <td class="right-align"><p>Фотография</p></td>
            <td><img src="{cfgSiteImg}no-photo.gif" width="250px" height="320px" id="img-photo" />
                <button class="blue-button" type="button" id="btnUploadFile">Выбрать файл</button>
                <input type="file" class="width450" id="but-file"/>

                <p>*.jpg, *.png, *.tiff, *.bmp (не более 2MB). </p>
                <p>На фотографии должно быть четко видно лицо кандидата,снимок не должен содержать рекламы (например, логотипа соц.сети) и контактов соискателя. </p>
            </td>
        </tr-->
        <tr>
            <td class="right-align"><p><span class="after-star">Город проживания</span></p></td>
            <td><p><input id="contact-place-live" name="strCity" value="{strCity}" type="text" class="width450" maxlength="128" placeholder="введите три первых буквы для подсказки"></p></td>
        </tr>
    </table>

    <h3 class="header-form-create">Контактные данные</h3>

    <table class="table-fixed-col">
        <col class="col1">
        <col class="col2">
        <tr>
            <td class="right-align"><p><span class="after-star">E-mail</span></p></td>
            <td><input id="contact-mail" name="strEmail" value="{strEmail}" type="text" class="width450" maxlength="128" placeholder="например: ivanoff.ivan_999@mail.ru"/></td>
        </tr>
        <tr>
            <td class="right-align"><p><span class="after-star">Мобильный телефон</span></p></td>
            <td><p><input type="text" class="width60" placeholder="+7" maxlength="4" name="mobPhone[]" id="mobCCode" value="+7" /> <input type="text" class="width60" placeholder="код" maxlength="4" name="mobPhone[]" id="mobECode" value="{mobECode}"/> <input type="text" class="width100" placeholder="номер" maxlength="7" name="mobPhone[]" id="mobPhone" value="{mobPhone}"/> <input type="text" class="width140" placeholder="комментарий" maxlength="255" name="mobPhone[]" value="{mobPhoneComment}"/></p></td>
        </tr>
        <tr>
            <td class="right-align"><p>Домашний телефон</p></td>
            <td><p><input type="text" class="width60" placeholder="+7" maxlength="4" name="homePhone[]" id="homeCCode"/> <input type="text" class="width60" placeholder="код" maxlength="4" name="homePhone[]" id="homeECode"/> <input type="text" class="width100" placeholder="номер" maxlength="7" name="homePhone[]" id="homePhone"/> <input type="text" class="width140" placeholder="комментарий" maxlength="255" name="homePhone[]"/></p></td>
        </tr>
        <tr>
            <td class="right-align"><p>Рабочий телефон</p></td>
            <td><p><input type="text" class="width60" placeholder="+7" maxlength="4" name="workPhone[]" id="workCCode"/> <input type="text" class="width60" placeholder="код" maxlength="4" name="workPhone[]" id="workECode"/> <input type="text" class="width100" placeholder="номер" maxlength="7" name="workPhone[]" id="workPhone"/> <input type="text" class="width140" placeholder="комментарий" maxlength="255" name="workPhone[]"/></p></td>
        </tr>
         <tr>
                <td class="tdL">Skype: </td>
                <td class="tdR"><input type="text" name="strSkype" value="{strSkype}" class="plane medium" maxlength="96" placeholder="например: ivan"/></td>
            </tr>
            <tr>
                <td class="tdL">ICQ#: </td>
                <td class="tdR"><input type="text" id="strICQ" name="strICQ" value="{strICQ}" class="plane small" maxlength="12" placeholder="до 10 цифр"/></td>
            </tr>
            <tr>
                <td class="tdL">LinkedIn страница: </td>
                <td class="tdR"><input type="text" name="strLinkedin" value="{strLinkedin}" class="plane" maxlength="255" placeholder="например: http://www.linkedin.com/in/shirokovskiy или только shirokovskiy"/></td>
            </tr>
            <tr>
                <td class="tdL">Водительское удостоверение:</td>
                <td class="tdR"><label><input type="checkbox" id="cbxDriver" name="cbxDriver"{cbxDriver} value="Y" class="plane"/> &mdash; (отметить если есть действующие водительские права)</label></td>
            </tr>
    </table>

    <br/><br/>

    <div class="center-align">
        <button class="create-vacancy" id="next-step1">Следующий шаг</button>
    </div>
    <br/>
</div>

<div id="wish-job-info" style="display: none">
    <h3 class="header-form-create">Пожелания к работе</h3>
    <table class="table-fixed-col">
        <col class="col1">
        <col class="col2">
        <tr>
            <td class="right-align"><p><span class="after-star">Должность</span></p></td>
            <td><input type="text" class="width450" id="contact-position" name="strTitle" value="{strTitle}" maxlength="255"/></td>
        </tr>

        <tr>
            <td class="right-align"><p><span class="after-star">Профессиональная сфера</span></p></td>
            <td>
                <select class="width450" name="intCategoryProffId" id="intCategoryProffId">
                    <option value="">выберите...</option>
                    <loop option.group.category>
                        <optgroup label="{strOptGroupName}">
                            <loop group.options.{intGroupID}>
                            <option value="{intOptionValue}"{selected}>{strOptionTitle}</option>
                            </loop group.options.{intGroupID}>
                        </optgroup>
                    </loop option.group.category>
                </select>
            </td>
        </tr>
        <tr>
            <td class="right-align"><p><span class="after-star">Заработная плата</span></p></td>
            <td><p><input type="text" class="width100" id="contact-salary" name="strSalary" value="30000" value="{strSalary}"  maxlength="7"/> руб./месяц</p></td>
        </tr>
        <tr>
            <td class="right-align"><p>График работы</p></td>
            <td>
                <p><input type="radio" name="choose-schedule-work" value="any" checked id="schedule-any"> Любой</p>
                <p><input type="radio" name="choose-schedule-work" value="ch" id="schedule-choose"/> Выбрать</p>
                <p class="padding-left-15"> <input type="checkbox" name="type-work[]" value="full" checked disabled/> полный рабочий день</p>
                <p class="padding-left-15"> <input type="checkbox" name="type-work[]" value="change" disabled/> сменный</p>
                <p class="padding-left-15"> <input type="checkbox" name="type-work[]" value="free" disabled/> свободный</p>
                <p class="padding-left-15"> <input type="checkbox" name="type-work[]" value="part" disabled/> частичная занятость</p>
                <p class="padding-left-15"> <input type="checkbox" name="type-work[]" value="remote" disabled/> удаленная работа</p>
            </td>
        </tr>

        <tr>
            <td class="right-align"><p><span class="after-star">Место работы (город)</span></p></td>
            <td><p><input type="text" class="width450" name="strExpCity" id="contact-job-place"  maxlength="128" placeholder="введите три первых буквы для подсказки" value="{strExpCity}"/></p></td>
        </tr>
        <tr>
            <td class="right-align"><p>Командировки</p></td>
            <td>
                <p><input type="radio" name="rbTrips" value="N"{rbTrips_N} > Не готов к командировкам</p>
                <p><input type="radio" name="rbTrips" value="Y"{rbTrips_Y} checked> Готов к командировкам (в любое время)</p>
                <p><input type="radio" name="rbTrips" value="S"{rbTrips_S} > Готов к командировкам (иногда/не часто)</p>
            </td>
        </tr>
    </table>
    <br/><br/>
    <div class="center-align">
        <!--<a href="#"> Предыдущий шаг</a> -->
        <button class="create-vacancy" id="prev-step2">Предыдущий шаг</button>
        <button class="create-vacancy" id="next-step2">Следующий шаг</button>
    </div>
    <br/>
</div>

<div id="experience-info" style="display: none">
    <h3 class="header-form-create">Работа в должности</h3>
    <table class="table-fixed-col border-bottom1">
        <col class="col1">
        <col class="col2">
        <tr>
            <td class="right-align"><p><span class="after-star">Опыт работы в данной должности</span></p></td>
            <td><p><input type="text" class="width100" name="intExp" id="intExp" value="{intExp}"/>лет</p>
                <p><input type="checkbox" name="no-experience" id='no-experience' value="yes"/> нет опыта работы</p></td>
        </tr>
    </table>

    <div id="form_experience">
    <h3 class="header-form-create">Опыт работы</h3>
    <table class="table-fixed-col">
        <col class="col1">
        <col class="col2">
        <tr>
            <td class="right-align"><p>Название компании</p></td>
            <td><p><input type="text" class="width450" id="contact-comp-name" name="strCompany" maxlength="128" value="{strCompany}"/></p></td>
        </tr>
        <tr>
            <td class="right-align"><p>Должность</p></td>
            <td><p><input type="text" class="width450" id="contact-position-now" name="strPosition" maxlength="128" value="{strPosition}"/></p></td>
        </tr>
        <tr>
            <td class="right-align"><p>Период работы</p></td>
            <td><p>с <input type="text" class="width60" name="strDateFrom" id="job-from" value="{strDateFrom}"> по <input type="text" class="width60" name="strDateTo" id="job-till" value="{strDateTo}"></p>
                <p class="under-text color-gray font-small">Если Вы еще работаете в данной организации, поле "по" заполнять не нужно</p>
            </td>
        </tr>
        <tr>
            <td class="right-align">Сфера деятельности:</td>
            <td class="tdR">
                <select class="width450" name="intCategoryId" id="intCategoryId">
                    <option value="">выберите...</option>
                    <loop option.group.category>
                        <optgroup label="{strOptGroupName}">
                            <loop group.options.{intGroupID}>
                                <option value="{intOptionValue}"{selected}>{strOptionTitle}</option>
                            </loop group.options.{intGroupID}>
                        </optgroup>
                    </loop option.group.category>
                </select>
            </td>
        </tr>
        <tr>
                <td class="right-align">Регион / город:</td>
                <td class="tdR"><input class="width450" type="text" id="strRegion" name="strRegion" class="plane" value="{strRegion}" maxlength="128" placeholder="введите три первых буквы для подсказки"/></td>
            </tr>

            <tr>
                <td class="right-align">Web-сайт компании:</td>
                <td class="tdR"><input class="width450" type="text" id="strWeb" name="strWeb" class="plane" value="{strWeb}" maxlength="255" placeholder="укажите доменное имя, напр.: www.flickr.com"/></td>
            </tr>
        <tr>
            <td class="right-align"><p>Описание работы</p></td>
            <td><textarea type="text" class="width450" id="contact-about-job" name="strResponsibility"></textarea></td>
        </tr>
    </table>
    </div>

    <br/><br/>
    <div class="center-align">
        <!--<a href="#"> Предыдущий шаг</a> -->
        <button class="create-vacancy" id="prev-step3">Предыдущий шаг</button>
        <button class="create-vacancy" id="next-step3">Следующий шаг</button>
    </div>
    <br/>
</div>


<div id="education-info" style="display: none">
    <h3 class="header-form-create">Образование</h3>
    <table class="table-fixed-col">
        <col class="col1">
        <col class="col2">
        <tr>
            <td class="right-align"></td>
            <td><p><input type="radio" name="education-type" value="fh" checked> высшее</p>
                <p><input type="radio" name="education-type" value="ph" > неполное высшее</p>
                <p><input type="radio" name="education-type" value="ms" > среднее специальное</p>
                <p><input type="radio" name="education-type" value="m" > среднее</p>
            </td>
        </tr>
        <tr>
            <td class="right-align"><p>Название учебного заведения</p></td>
            <td><p><input type="text" class="width450" id="contact-school" maxlength="255" name="strInstitution" value="{strInstitution}"/></p></td>
        </tr>
        <!--<tr>-->
            <!--<td class="right-align"><p><span class="after-star">Полученное образование</span></p></td>-->
            <!--<td><p><input type="text" class="width450" name="education" id="contact-education" maxlength="255"/></p></td>-->
        <!--</tr>-->
        <tr>
            <td class="right-align"><p>Годы обучения</p></td>
            <td><p>с <input type="text" class="width60" name="strEduDateStart" id="contact-school-from" maxlength="4"/> по <input type="text" class="width60" name="strEduDateFin" id="contact-school-till" maxlength="4"/></p>
                <p class="under-text color-gray font-small">Если Вы еще учитесь в данном учебном заведении, поле "по" заполнять не нужно</p>
            </td>
        </tr>
        <tr>
                <td class="right-align">Факультет</td>
                <td><input type="text" id="strFaculty" name="strFaculty" class="plane" value="{strFaculty}" maxlength="255" /></td>
            </tr>
        <tr>
            <td class="right-align"><p>Специальность</p></td>
            <td><p><input type="text" class="width450" name="strSpecialization" id="contact-speciality"></p></td>
        </tr>
        <tr>
            <td class="right-align"><p>Комментарий</p></td>
            <td><textarea type="text" class="width450" name="strEduPlus">{strEduPlus}</textarea><br/>
                <p class="under-text color-gray font-small">Награды, темы диплома, средний балл и т.п.</p>
            </td>
        </tr>
    </table>

    <br/><br/>

    <div class="center-align">
        <!--<a href="#"> Предыдущий шаг</a> -->
        <button class="create-vacancy" id="prev-step4">Предыдущий шаг</button>
        <button class="create-vacancy" id="next-step4">Следующий шаг</button>
    </div>
    <br/>
</div>


<div id="additional-info" style="display: none">
    <h3 class="header-form-create">Дополнительная информация</h3>
    <table class="table-fixed-col">
        <col class="col1">
        <col class="col2">
        <tr>
            <td class="right-align"><p>Родной язык</p></td>
            <td><p><input name="strLang" id="strLang" type="text" class="width120" placeholder="введите хотя бы две буквы" value="Русский" maxlength="32"/></p></td>
        </tr>
        <tr>
            <td class="right-align"><p>Другие языки</p></td>
            <td><p><input type="checkbox" name="no-lang" value="no" /> не владею другими языками</p>

                <div id="lang-box">
                    <p><a href="#" id="but-add-lang">добавить язык</a></p>
                    <p><input type="text" class="width120" name="moreLang[]" placeholder="укажите язык" maxlength="32"/><input type="text" class="width120" placeholder="уровень владения" maxlength="32" name="moreLangLevel[]"/></p>
                </div>
            </td>
        </tr>
        <tr>
            <td class="right-align"><p>Профессиональные навыки</p></td>
            <td><textarea type="text" class="width450" name="strExperienceDescription">{strExperienceDescription}</textarea>
                <p class="under-text color-gray font-small">Опишите свои знания и навыки, которые соответствуют выбранной Вами должности</p></td>
        </tr>
        <tr>
            <td class="right-align"><p>О себе</p></td>
            <td><textarea type="text" class="width450" name="strAbout">{strAbout}</textarea>
                <p class="under-text color-gray font-small">Укажите информацию о себе, которая может заинтересовать потенциального работодателя</p></td>
        </tr>

        <tr>
            <td class="right-align"><p>Гражданство</p></td>
            <td><p><input type="text" class="width450" maxlength="64" name="strCitizenship" value="{strCitizenship}" placeholder="например: РФ"/></p></td>
        </tr>
        <!--<tr>-->
            <!--<td class="right-align"><p> </p></td>-->
            <!--<td><p><a href="#"> Добавить портфолио</a></p></td>-->
        <!--</tr>-->
    </table>

    <br/><br/>

    <div class="center-align">
        <!--<a href="#"> Предыдущий шаг</a> -->
        <button class="create-vacancy" id="prev-step5">Предыдущий шаг</button>
        <button class="create-vacancy" id="add-resume" type="submit">Разместить резюме</button>
    </div>
    <br/>
</div>
</form>

<script type="text/javascript">
var strErrorNote = 'Пожалуйста, заполните обязательные поля, помеченные символом (*)!';
jQuery(document).ready(function ($) {
    $('#contact-fio').focus();
    $('#contact-salary').spinner({
        min: 0,
        step: 5000
    });
    $('a.ui-spinner-button').css('border','none');
    $('#contact-birth').datepicker({
        showOn: "button",
        buttonImage: "{cfgSiteImg}calendar_icon.png",
        buttonImageOnly: true,
        dateFormat: "dd.mm.yy",
        changeYear: true,
        showAnim: 'fold',
        defaultDate: '-30Y'
    });

    $('#contact-birth').blur(function(){
        if ($('#contact-birth').val().length>0) {
            var rBirth=/^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$/;
            if (!rBirth.test($('#contact-birth').val()))
            {
                alert('Вы ввели не корректную дату рождения!(Формат даты: DD.MM.YYYY)');
                $('#contact-birth').focus();
            }
        }
        return false;
    });
    $('#contact-mail').blur(function(){
        if ($('#contact-mail').val().length>0) {
            var rMail=/^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/;
            if (!rMail.test($('#contact-mail').val()))
            {
                alert('Введите корректный e-mail!');
                $('#contact-mail').focus();
            }
        }
        return false;
    });
    var citiesSource = [{strCities}];
    $('#contact-place-live').autocomplete({source: citiesSource, minLength: 3});
    $('#contact-job-place').autocomplete({source: citiesSource, minLength: 3});
    var arrLangs = ['Русский','Английский','Французкий','Японский','Итальянский','Испанский','Немецкий'];
    $('#strLang').autocomplete({source: arrLangs, minLength: 1});

    $('#but-step1').click(function(){
        $('.active-block').hide().removeClass('active-block');
        $('#main-info').show().addClass('active-block');
        $('.steps-resume-active').removeClass('steps-resume-active');
        $('#but-step1').addClass('steps-resume-active');
        return false;
    });
    $('#but-step2').click(function(){
        if( isStep1OK() )
        {
            $('.active-block').hide().removeClass('active-block');
            $('#wish-job-info').show().addClass('active-block');
            $('.steps-resume-active').removeClass('steps-resume-active');
            $('#but-step2').addClass('steps-resume-active');
        }
        else alert(strErrorNote);
        $('#contact-position').focus();
        return false;
    });
    $('#btnUploadFile').click(function(){
        $('#but-file').trigger('click');
    });
    $('#but-step3').click(function(){
        if( isStep1OK() && isStep2OK() )
        {
            $('.active-block').hide().removeClass('active-block');
            $('#experience-info').show().addClass('active-block');
            $('.steps-resume-active').removeClass('steps-resume-active');
            $('#but-step3').addClass('steps-resume-active');
            $('#contact-comp-name').focus();
        }
        else alert(strErrorNote);
        return false;
    });
    $('#but-step4').click(function(){
        if ( isStep1OK() && isStep2OK() && isStep3OK() )
        {
            $('.active-block').hide().removeClass('active-block');
            $('#education-info').show().addClass('active-block');
            $('.steps-resume-active').removeClass('steps-resume-active');
            $('#but-step4').addClass('steps-resume-active');
        }
        else alert(strErrorNote);
        $('#contact-school').focus();
        return false;
    });
    $('#but-step5').click(function(){
        if( isStep1OK() && isStep2OK() && isStep3OK() )
        {
            $('.active-block').hide().removeClass('active-block');
            $('#additional-info').show().addClass('active-block');
            $('.steps-resume-active').removeClass('steps-resume-active');
            $('#but-step5').addClass('steps-resume-active');
            $('#strLang').focus();
        }
        else alert(strErrorNote);
        return false;
    });

    $('#schedule-choose').click(function(){
        $('.padding-left-15 input').removeAttr('disabled');
    });
    $('#schedule-any').click(function(){
        $('.padding-left-15 input').attr('disabled', 'disabled');
    });

    $('#next-step1').click( function(){
        $('#but-step2').trigger('click');
        return false;
    });
    $('#next-step2').click(function(){
        $('#but-step3').trigger('click');
        return false;
    });
    $('#prev-step2').click(function(){
        $('.active-block').hide().removeClass('active-block');
        $('#main-info').show().addClass('active-block');
        $('.steps-resume-active').removeClass('steps-resume-active');
        $('#but-step1').addClass('steps-resume-active');
        return false;
    });

    $('#next-step3').click(function(){
        $('#but-step4').trigger('click');
        return false;
    });
    $('#prev-step3').click(function(){
        $('.active-block').hide().removeClass('active-block');
        $('#wish-job-info').show().addClass('active-block');
        $('.steps-resume-active').removeClass('steps-resume-active');
        $('#but-step2').addClass('steps-resume-active');
        return false;
    });

    $('#next-step4').click(function(){
        $('#but-step5').trigger('click');
        return false;
    });
    $('#prev-step4').click(function(){
        $('.active-block').hide();
        $('#experience-info').show();
        $('.steps-resume-active').removeClass('steps-resume-active');
        $('#but-step3').addClass('steps-resume-active');
        $('.active-block').removeClass('active-block');
        $('#experience-info').addClass('active-block');
        return false;
    });

    $('#prev-step5').click(function(){
        $('.active-block').hide();
        $('#education-info').show();
        $('.steps-resume-active').removeClass('steps-resume-active');
        $('#but-step4').addClass('steps-resume-active');
        $('.active-block').removeClass('active-block');
        $('#education-info').addClass('active-block');
        return false;
    });

    $('#add-resume').click(function () {
        console.log('Prevent default');
        event.preventDefault();

        if (isStep1OK() && isStep2OK() && isStep3OK()) {
            $('#add_resume_form').submit();
        }

        return false;
    });

    function isStep1OK () {
        return ( $('#contact-fio').val().length>0
                && $('#contact-place-live').val().length>0
                && $('#contact-mail').val().length>0
                && $('input[name="rbSex"]').is(':checked')
                &&  (   ($('#mobCCode').val().length>0 && $('#mobECode').val().length>0 && $('#mobPhone').val().length>0)
                    ||  ($('#homeCCode').val().length>0 && $('#homeECode').val().length>0 && $('#homePhone').val().length>0)
                    ||  ($('#workCCode').val().length>0 && $('#workECode').val().length>0 && $('#workPhone').val().length>0)
                    )
        );
    }

    function isStep2OK () {
        return ( $('#contact-position').val().length>0
                && $('#intCategoryProffId').val().length>0
                && $('#contact-salary').val().length>0
                && $('#contact-job-place').val().length>0
        );
    }

    function isStep3OK () {
        return ($('#no-experience').is(':checked') || $('#intExp').val().length>0);
    }

    $('#but-file').change(function(){
        var str = $('#but-file').get(0).files[0].name;
        $('#img-photo').attr('src',$('#but-file').val())
    });

    $('#but-add-lang').click(function(){
        var str='<p class="margin5-0"><input type="text" class="width120" placeholder="укажите язык"><input type="text" class="width120" placeholder="уровень владения"></p>';
        $('#lang-box').append(str);
        return false;
    });
    $('input[name="no-lang"]').change(function(){
        if ($(this).is(":checked"))
            $('#lang-box').hide('medium');
        else
            $('#lang-box').show('medium');
    });

    $('#no-experience').click(function () {
        if ($(this).is(':checked')) {
            $('#intExp').attr('value', '0');
            $('#form_experience').hide();
        } else {
            $('#form_experience').show();
        }
    });

    $('#contact-salary').input_mask("9?999999", {placeholder:" "});
    $('#intExp').input_mask("9?9", {placeholder:" "});
    $('#contact-school-from').input_mask("9999");
    $('#contact-school-till').input_mask("9999");
});
</script>
<frg resume.agreement>