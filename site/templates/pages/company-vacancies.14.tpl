<h3>Информация о компании <span>{strCompanyName}</span></h3>
<if is.strLogo><div class="cell-logo"><img src="{cfgAllImg}companies/{strLogo}" alt="{strCompanyName}"/></div></if is.strLogo>

<table class="record-card company-info">
    <colgroup>
        <col class="first">
        <col>
    </colgroup>

    <if is.strCity>
        <tr>
            <td>Город</td>
            <td>{strCity}</td>
        </tr>
    </if is.strCity>
    <if is.strAddress>
        <tr>
            <td>Адрес</td>
            <td>{strAddress}</td>
        </tr>
    </if is.strAddress>
    <if is.strWeb>
        <tr>
            <td>Web-site</td>
            <td>{strWeb}</td>
        </tr>
    </if is.strWeb>
    <if is.strEmail>
        <tr>
            <td>Email</td>
            <td>{strEmail}</td>
        </tr>
    </if is.strEmail>
    <if is.strPhone>
        <tr>
            <td>Телефон</td>
            <td>{strPhone}</td>
        </tr>
    </if is.strPhone>
    <if is.strCompanyDescription>
        <tr>
            <td>Описание</td>
            <td>{strCompanyDescription}</td>
        </tr>
    </if is.strCompanyDescription>
</table>

<if is.list>
<h4 class="company-info">Вакансии компании:</h4>
<div class="vacancies list">
<loop list.vacancies>
    <div class="row">
        <div class="cell col1"><a href="{cfgSiteUrl}vacancy/{jv_id}">{jv_title}</a>
            <p>{strDescription} <if is.strDescription.{jv_id}><a href="{cfgSiteUrl}vacancy/{jv_id}">&raquo;</a></if is.strDescription.{jv_id}></p>
        </div>
        <div class="cell clearer"><span class="salary">{strSalary}</span></div>
    </div>
</loop list.vacancies>
</div>
</if is.list>
