<div class="wrapper_block">
    <div class="fl column first">
        <h1>НОВЫЕ ВАКАНСИИ</h1>

        <loop list.top.vacancies>
        <div class="row">
            <a href="{cfgSiteUrl}vacancy/{strSeoUrl}">{jv_title}</a>
            <p class="salary">{strSalary}</p>
        </div>
        </loop list.top.vacancies>

        <if no.list.top.vacancies>
        <p>Нет свежих вакансий в городе {strMyCity}</p>
        </if no.list.top.vacancies>
    </div>
    <div class="fl column last">
        <h1>НОВЫЕ РЕЗЮМЕ</h1>

        <loop list.top.resumes>
        <div class="row">
                <a href="{cfgSiteUrl}resume/{strSeoUrl}">{jr_title}</a>
            <p class="salary">{jr_salary} руб.</p>
        </div>
        </loop list.top.resumes>

        <if no.list.top.resumes>
        <p>Нет свежих резюме в городе {strMyCity}</p>
        </if no.list.top.resumes>
    </div>
    <div class="clearer"></div>
</div>
