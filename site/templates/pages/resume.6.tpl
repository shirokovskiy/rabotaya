<h3>Резюме: {strTitle} (<span>ID: {intResumeId}</span>)</h3>
<div class="right-top-block-link">
    <a href="{cfgSiteUrl}my-resumes">Редактировать / Добавить в архив</a>
</div>

<table class="record-card">
    <colgroup>
        <col class="first" />
        <col />
    </colgroup>

    <if is.strCity>
        <tr>
            <td>Город</td>
            <td>{strCity}</td>
        </tr>
    </if is.strCity>

    <tr>
        <td>Кандидат</td>
        <td>{strFio}</td>
    </tr>

    <if is.strSalary>
        <tr>
            <td>Ожидаемая з/п</td>
            <td>{strSalary}</td>
        </tr>
    </if is.strSalary>


    <if is.strWorkbusy>
        <tr>
            <td>Занятость</td>
            <td>{strWorkbusy}</td>
        </tr>
    </if is.strWorkbusy>

    <tr>
        <td>Телефон</td>
        <td>{strPhone}</td>
    </tr>

    <if is.strAbout>
        <tr>
            <td>О себе</td>
            <td>{strAbout}</td>
        </tr>
    </if is.strAbout>

    <if is.strAge>
        <tr>
            <td>Возраст</td>
            <td>{strAge}</td>
        </tr>
    </if is.strAge>

    <tr>
        <td>Семейное положение</td>
        <td>{strMarriage}</td>
    </tr>
    <tr>
        <td>Наличие детей</td>
        <td>{strChildren}</td>
    </tr>

    <if is.strEmail>
        <tr>
            <td>Email</td>
            <td>{strEmail}</td>
        </tr>
    </if is.strEmail>

    <if is.strSkype>
        <tr>
            <td>Skype</td>
            <td>{strSkype}</td>
        </tr>
    </if is.strSkype>
    <if is.strIcq>
        <tr>
            <td>ICQ #</td>
            <td>{strIcq}</td>
        </tr>
    </if is.strIcq>
    <if is.strLinkedin>
        <tr>
            <td>LinkedIn</td>
            <td>{strLinkedin}</td>
        </tr>
    </if is.strLinkedin>

    <tr>
        <td>Водительское удостоверение</td>
        <td>{strDriver}</td>
    </tr>
    <tr>
        <td>Готовность к командировкам</td>
        <td>{strTrips}</td>
    </tr>
    <if previous.works>
    <tr>
        <td colspan="2">Предыдущие места работы</td>
    </tr>
    <tr>
        <td colspan="2" class="txt-norm">
            <ol class="work_history">
                <loop resume.history>
                    <li class="company">
                        <div class="clearer">
                            <div class="fl">Компания:</div>
                            <div class="">{strHistoryCompany}</div>
                        </div>
                        <div class="clearer">
                            <div class="fl">Категория:</div>
                            <div class="">{strHistoryCategory}</div>
                        </div>
                        <div class="clearer">
                            <div class="fl">Должность:</div>
                            <div class="">{strHistoryPosition}</div>
                        </div>
                        <div class="clearer">
                            <div class="fl">Период работы:</div>
                            <div class="">{strHistoryDateFrom} &mdash; {strHistoryDateTo}</div>
                        </div>
                        <div class="clearer">
                            <div class="fl">Регион / Город:</div>
                            <div class="">{strHistoryRegion}</div>
                        </div>
                        <div class="clearer">
                            <div class="fl">Web-site</div>
                            <div class="">{strHistoryWeb}</div>
                        </div>
                        <div class="clearer">
                            <div class="fl">Достижения:</div>
                            <div class="">{strHistoryResponsibilities}</div>
                        </div>
                    </li>
                </loop resume.history>
            </ol>
        </td>
    </tr>
    </if previous.works>

    <if is.strEduPlus>
        <tr>
            <td>Дополнительные курсы</td>
            <td>{strEduPlus}</td>
        </tr>
    </if is.strEduPlus>

    <if is.strExperienceDescription>
        <tr>
            <td>Профессиональные навыки</td>
            <td>{strExperienceDescription}</td>
        </tr>
    </if is.strExperienceDescription>

    <tr>
        <td>Дата публикации</td>
        <td>{strDatePublish}</td>
    </tr>
</table>

<if education>
<br>
<h3>Образование</h3>
<br>
<table class="record-card">
    <colgroup>
        <col class="first">
        <col>
    </colgroup>

    <if is.strEducation>
        <tr>
            <td>Образование</td>
            <td>{strEducation}</td>
        </tr>
    </if is.strEducation>

    <tr>
        <td>Учебное заведение</td>
        <td>{strInstitution}</td>
    </tr>

    <if is.strFaculty>
        <tr>
            <td>Факультет</td>
            <td>{strFaculty}</td>
        </tr>
    </if is.strFaculty>
    <if is.strSpecialization>
        <tr>
            <td>Специализация / специальность</td>
            <td>{strSpecialization}</td>
        </tr>
    </if is.strSpecialization>

    <tr>
        <td>Даты обучения</td>
        <td>{strEduDateStart} &mdash; {strEduDateFin}</td>
    </tr>
</table>
</if education>

<div class="ac block_m20"><a href="{strBackUri}" class="link-darkred">{strBackTitle}</a></div>
<if authed>
<div class="ac block_m20"><a href="/profile" class="link-darkred">перейти в личный кабинет</a></div>
</if authed>
