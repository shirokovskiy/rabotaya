<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 7/31/13
 * Time         : 2:30 AM
 * Description  : for CLI only
 *
 *  Convert old resumes
 */
include_once('cfg/web.cfg.php');

$strSqlQuery = "SELECT * FROM `resume` ORDER BY id";
$arrRes = $objDb->fetchall($strSqlQuery);

if (is_array($arrRes) && !empty($arrRes)) {
    foreach ($arrRes as $k => $resume) {
        $strSqlQuery = "REPLACE INTO `job_resumes` SET"
            ." jr_id = ".$resume['id']
            .", jr_status = 'Y'"
            .", jr_title = '".mysql_real_escape_string(strip_tags($resume['post']))."'"
            .", jr_fio = '".mysql_real_escape_string(strip_tags($resume['name']))."'"
            . (!empty($resume['age']) ? ", jr_age = '".$resume['age']."'" : '' )
            .", jr_salary = '".mysql_real_escape_string(strip_tags($resume['salary']))."'"
            .", jr_about = 'Образование: ".mysql_real_escape_string(strip_tags($resume['education']))."\nУчебное заведение: ".mysql_real_escape_string(strip_tags($resume['university']))
                        ."\nОпыт работы: ".mysql_real_escape_string(strip_tags($resume['experience']))."\nО себе: ".mysql_real_escape_string(strip_tags($resume['message']))."'"
            . (!empty($resume['date']) ? ", jr_date_publish = '".$resume['date']."'" : '' )
            . (!empty($resume['additional_education']) ? ", jr_edu_plus = '".mysql_real_escape_string(strip_tags($resume['additional_education']))."'" : '' )
            . (!empty($resume['email']) ? ", jr_email = '".mysql_real_escape_string(strip_tags($resume['email']))."'" : '' )
            . (!empty($resume['phone']) ? ", jr_phone = '".mysql_real_escape_string(strip_tags($resume['phone']))."'" : '' )
            .", jr_driver = '".($resume['driver_license']>0?'Y':'N')."'"
            .", jr_children = '".($resume['children']>0?'Y':'N')."'"
            .", jr_trips = '".($resume['mission']>0?'Y':'N')."'"
            .", jr_marriage = '".($resume['family']>0?'Y':'N')."'"
        ;
        if (!$objDb->query($strSqlQuery)) {
            # sql error
            die('Ошибка БД в строке '.__LINE__."\n");
        } else {
            if (!empty($resume['section'])) {
                $strSqlQuery = "REPLACE INTO `job_category_link` SET"
                    ." jcl_rel_id = ".$resume['id']
                    .", jcl_sjc_id = ".$resume['section']
                    .", jcl_type = 'resume'"
                ;
                if (!$objDb->query($strSqlQuery)) {
                    # sql error
                    die('Ошибка БД в строке '.__LINE__."\n");
                }
            }
        }
    }
}

echo "Successfully done!\n";
