<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 4/10/14
 * Time         : 12:56 AM
 * Description  : script for crontab (every hour)
 */
include_once("cfg/web.cfg.php");

$where[] = 'jv_date_publish >= '.strtotime('-1 month');
$where[] = 'jv_date_publish IS NOT NULL';
$where[] = 'jv_phone IS NOT NULL';
$where[] = 'jv_phone != ""';
$where[] = 'jv_phone NOT LIKE "0000000%"';
$order[] = 'jv_id DESC';

$objVacancy = new Vacancy();
$list = $objVacancy->getList($where, $order);
$objCat = new JobCategory();
$objCompany = new Company();

//header("Accept: application/xml");
//header("Accept-Charset: utf-8");
//header("Content-Type: application/xml; charset=utf-8");

$xml = '<?xml version="1.0" encoding="utf-8"?>'."\n";
$xml .= '<source creation-time="'.date('Y-m-d H:i:s').' GMT+3" host="www.rabota-ya.ru">'."\n";
$xml .= '  <vacancies>'."\n";

#echo count($list)."<br/>\n";

// LOOP
foreach ($list as $vac):
$xml .= '   <vacancy>'."\n";
$xml .= '     <url>'.SITE_URL.'vacancy/'.$vac['jv_id'].'</url>'."\n";
$xml .= '     <creation-date>'.date('Y-m-d H:i:s', $vac['jv_date_publish']).' GMT+3'.'</creation-date>'."\n";
$salary = $objVacancy->getSalary($vac['jv_id']);
if (!empty($salary)) {
    $xml .= '     <salary>'.$salary.'</salary>'."\n";
}
$xml .= '     <category>'."\n";
$industry = $objCat->getCategoryByVacancy($vac['jv_id']);
if (empty($industry)) {
    $industry = $objCat->getCategoryFieldById(18);
}
$xml .= '       <industry>'.$industry.'</industry>'."\n";
$xml .= '     </category>'."\n";
$xml .= '     <job-name>'.cleanString($vac['jv_title']).'</job-name>'."\n";
$xml .= '     <description>'.cleanString($vac['jv_description']).'</description>'."\n";
$xml .= '     <addresses>'."\n";
$xml .= '       <address>'."\n";
$location = cleanString($vac['jv_city']);
if (empty($location)) $location = 'Санкт-Петербург';
$xml .= '         <location>'.$location.'</location>'."\n";
$xml .= '       </address>'."\n";
$xml .= '     </addresses>'."\n";

if ($vac['jv_jc_id'] > 0) {
    $company = $objCompany->getCompany($vac['jv_jc_id']);
    if (is_array($company) && !empty($company)) {
        $xml .= '     <company>'."\n";
        $xml .= '       <name>'.cleanString($company['jc_title']).'</name>'."\n";
        if (!empty($company['jc_description']))
        $xml .= '       <description>'.cleanString($company['jc_description']).'</description>'."\n";
        $logo = $objCompany->getLogo($vac['jv_jc_id']);
        if (!empty($logo))
        $xml .= '       <logo>'.$arrTplVars['cfgAllImg'].'companies/'.$logo.'</logo>'."\n";
        if (!empty($company['jc_web']))
        $xml .= '       <site>'.$company['jc_web'].'</site>'."\n";
        if ($objUtils->isValidEmail($company['jc_email'])) {
            $xml .= '       <email>'.$company['jc_email'].'</email>'."\n";
        } elseif($objUtils->isValidEmail($vac['jv_email'])) {
            $xml .= '       <email>'.$vac['jv_email'].'</email>'."\n";
        }
        if (!empty($company['jc_phone'])) {
            $xml .= '       <phone>'.$company['jc_phone'].'</phone>'."\n";
        } elseif(!empty($vac['jv_phone'])) {
            $xml .= '       <phone>'.$vac['jv_phone'].'</phone>'."\n";
        }
        if (!empty($vac['jv_contact_fio'])) {
            $xml .= '     <contact-name>'.$vac['jv_contact_fio'].'</contact-name>'."\n";
        }
        $xml .= '       <hr-agency>'.($company['jc_type']=='direct' ? 'false':'true').'</hr-agency>'."\n";
        $xml .= '     </company>'."\n";
    } else {
        $xml .= '     <anonymous-company>'."\n";
        $xml .= '       <description>Имя компании работодателя неизвестно</description>'."\n";
        $xml .= '     </anonymous-company>'."\n";
    }
} else {
    $xml .= '     <anonymous-company>'."\n";
    $xml .= '       <description>Имя компании скрыто или неизвестно.</description>'."\n";
    $xml .= '     </anonymous-company>'."\n";
}
$xml .= '   </vacancy>'."\n";
endforeach;
// END:LOOP

$xml .= '  </vacancies>'."\n";
$xml .= '</source>'."\n";

$fp = fopen("ya-vacancies.xml", "w");
fwrite($fp, $xml);
fclose($fp);

function cleanString($str) {
    $str = trim($str);
    $str = str_replace("''",'"', $str);
    $str = htmlspecialchars($str);
    $str = str_replace("'",'&apos;', $str);
    return $str;
}
