<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 8/1/13
 * Time         : 6:58 PM
 * Description  : for CLI only
 *
 *  Convert old vacancies
 */
include_once('cfg/web.cfg.php');

$strSqlQuery = "SELECT * FROM `vacancy` ORDER BY id";
$arrRes = $objDb->fetchall($strSqlQuery);
$i = 0;
if (is_array($arrRes) && !empty($arrRes)) {
    foreach ($arrRes as $k => $vacancy) {
        $i++;
        $strSqlQuery = "REPLACE INTO `job_vacancies` SET"
            ." jv_id = ".$vacancy['id']
            .", jv_status = 'Y'"
            .", jv_title = '".mysql_real_escape_string(strip_tags($vacancy['post']))."'"
            .", jv_description = '".(!empty($vacancy['message']) ? mysql_real_escape_string(strip_tags($vacancy['message']))."\n\n" : '')
            . (!empty($vacancy['contacts']) ? "Контактная информация: ".mysql_real_escape_string(strip_tags($vacancy['contacts']))."\n\n" : '' )."'"
            . (!empty($vacancy['salary']) ? ", jv_salary = '".mysql_real_escape_string(strip_tags(trim($vacancy['salary'])))."'" : '')
            . (!empty($vacancy['date']) ? ", jv_date_publish = '".$vacancy['date']."'" : '' )
            . (!empty($vacancy['email']) ? ", jv_email = '".mysql_real_escape_string(strip_tags($vacancy['email']))."'" : '' )
            . (!empty($vacancy['phone']) ? ", jv_phone = '".mysql_real_escape_string(strip_tags($vacancy['phone']))."'" : '' )
        ;

        $vacancy['salary'] = str_replace("т.р", '', $vacancy['salary']);
        $vacancy['salary'] = str_replace("руб", '', $vacancy['salary']);
        $vacancy['salary'] = str_replace(".", '', $vacancy['salary']);
        $vacancy['salary'] = str_replace("от", '', $vacancy['salary']);
        $vacancy['salary'] = trim($vacancy['salary']);
//        $vacancy['salary'] = preg_replace("/[^\d]/", '', $vacancy['salary']);

        if (!empty($vacancy['salary']) && !strpos($vacancy['salary'], '$')) {
            if (strchr($vacancy['salary'], '-')) {
                $arrS = explode('-', $vacancy['salary']);
                if (is_array($arrS) && !empty($arrS)) {
                    $arrS[0] = preg_replace("/[^\d]/", '', $arrS[0]);
                    if (!empty($arrS[0]))
                        $strSqlQuery .= ", jv_salary_from = ".$arrS[0];

                    $arrS[1] = preg_replace("/[^\d]/", '', $arrS[1]);
                    if (!empty($arrS[1]))
                        $strSqlQuery .= ", jv_salary_to = ".$arrS[1];
                }
            } else {
                $trSalary = preg_replace("/[^\d]/", '', $vacancy['salary']);
                if (intval($trSalary)>0)
                    $strSqlQuery .= ", jv_salary_to = ".$trSalary;
                unset($trSalary);
            }
        }
        if (!$objDb->query($strSqlQuery)) {
            # sql error
            die('Ошибка БД в строке '.__LINE__."\n");
        } else {
            if (!empty($vacancy['section'])) {
                // Добавление сферы деятельности
                $strSqlQuery = "REPLACE INTO `job_category_link` SET"
                    ." jcl_rel_id = ".$vacancy['id']
                    .", jcl_sjc_id = ".$vacancy['section']
                    .", jcl_type = 'vacancy'"
                ;
                if (!$objDb->query($strSqlQuery)) {
                    # sql error
                    die('Ошибка БД в строке '.__LINE__."\n");
                }
            }

            // Добавление компании
            if (!empty($vacancy['name']) && strlen($vacancy['name']) > 2) {
                $strSqlQuery = $d= "SELECT jc_id FROM `job_companies` WHERE `jc_title` = '".mysql_real_escape_string(strip_tags(stripslashes(trim($vacancy['name'])))) ."' ORDER BY jc_id LIMIT 1";
                $arrComp = $objDb->fetch($strSqlQuery);
                if (is_array($arrComp) && !empty($arrComp) && $arrComp['jc_id']) {
                    # компания существует
                    $jc_id = $arrComp['jc_id'];
                } else {
                    $strSqlQuery = "INSERT INTO `job_companies` SET"
                        ." jc_title = '".mysql_real_escape_string(strip_tags(stripslashes(trim($vacancy['name']))))."'"
                        .", jc_status = 'Y'"
                        .", jc_city = 'Санкт-Петербург'"
                        .", jc_date_create = ".time()
                    ;
                    if (!$objDb->query($strSqlQuery)) {
                        # sql error
                        die('Ошибка БД в строке '.__LINE__."\nпосле\n".$d);
                    } else {
                        $jc_id = $objDb->insert_id();
                    }
                }

                if ($jc_id > 0) {
                    $strSqlQuery = "UPDATE `job_vacancies` SET `jv_jc_id` = $jc_id WHERE `jv_id` = ".$vacancy['id'];
                    if (!$objDb->query($strSqlQuery)) {
                        # sql error
                        die('Ошибка БД в строке '.__LINE__."\n");
                    }
                } else {
                    echo "Нет инфо о компании вообще!\n";
                }
            }
        }
    }
}

echo "Successfully done!\n";
