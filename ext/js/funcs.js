/**
 * Здесь все функции для фронт и бэк-ендов
 */

/**
 * Шаблонозависимая функция для CMS
 * Включает кнопку сохранения контента (шаблона, страницы, фрагмента)
 */
function doAccept() {
	$('#smbSaveDoc').attr('disabled', false);
	$('#cbxPublish').attr('checked', true);
	alert("Вы соглашаетесь сохранить то, что видите в поле контента!\nСохраните Ваши изменения, или вернитесь к списку.");
}

/**
 * For the JCarousel
 * @param carousel
 */
function mycarousel_initCallback(carousel)
{
	// Disable autoscrolling if the user clicks the prev or next button.
	carousel.buttonNext.bind('click', function() {
		carousel.startAuto(0);
	});

	carousel.buttonPrev.bind('click', function() {
		carousel.startAuto(0);
	});

	// Pause autoscrolling if the user moves with the cursor over the clip.
	/*carousel.clip.hover(function() {
		carousel.stopAuto();
	}, function() {
		carousel.startAuto();
	});*/
}

function setSearchForm(type) {
	if(!isEmpty(type)) {
		$('#type').attr('value', type);
	}
}

$(function(){
	$.datepicker.setDefaults( $.datepicker.regional[ "ru" ] );

    /**
     * Tabs in top-navigation
     */
	$('.bar .tabs a.change-field').click(function(){
		var selectedStyle = $(this).parent().attr('class');
		$('#search_field').removeClass().addClass(selectedStyle).addClass('flow');
        $('.precision a').css({'color':'#4E4E4E', 'font-weight':'normal'});
		if(selectedStyle == 'color1') {
            setSearchForm('vacancies');
            $('#companies_selection_trigger').parent().show();
            $('#extended_search_trigger').attr('href', getMyHost() + '/adv-search');
            $('#categories_selection_trigger').attr('href', getMyHost() + '/po-otraslyam').show();
            $('#profession_selection_trigger').attr('href', getMyHost() + '/po-professiyam');
            $('#query').attr('placeholder', 'Введите название должности или город или ID вакансии');
        }
		if(selectedStyle == 'color2') {
            setSearchForm('resumes');
            $('#companies_selection_trigger').parent().hide();
            $('#extended_search_trigger').attr('href', getMyHost() + '/adr-search');
            $('#categories_selection_trigger').attr('href', getMyHost() + '/po-otraslyam-resume').hide();
            $('#profession_selection_trigger').attr('href', getMyHost() + '/po-professiyam-resume');
            $('#query').attr('placeholder', 'Введите название должности или город или ID резюме');
        }
		if(selectedStyle == 'color3') {setTimeout(function(){window.location.href = getMyHost() + '/form_add_resume';}, 500);}
		if(selectedStyle == 'color4') {setTimeout(function(){window.location.href = getMyHost() + '/form_add_vacancy';}, 500);}
	});

    /**
     * Open to read newspaper
     */
	$('#butExpand').click(function(){
		window.location = $('.section .tabs a').eq(0).attr('data-url');
	});

    /**
     * Week newspapers
     */
	$('.section .tabs a').click(function(){
		var tabID = $(this).attr('id');
		var tabWeek = $(this).attr('data-week');
		var tabYear = $(this).attr('data-year');
		$('#weekThumb').attr('src', getMyHost('/storage/files/images') + '/magazines/magazine.thumb.'+tabID+'.'+tabWeek+'.'+tabYear+'.jpg');
		var tabHref = $(this).attr('data-url');
		$('#week_magazines').removeClass().addClass('brd_'+tabID);
		$('.section .tabs a').removeClass();
		$(this).addClass('current').blur();
		$('#butExpand').click(function(){
			window.location = tabHref;
		});
	});

	if ($('a.uc')) // Under Construction
		$('a.uc').fancybox();
	if ($('a.fb')) // FancyBoxed Layer
		$('a.fb').fancybox();
	if ($('#distribution'))
		$('#distribution').fancybox({
			width       : 900
//			fitToView    : false
			, afterLoad : function () {
				//
			}
			, afterShow : function () {
				$('img.fancybox-image').wrap('<span style="display:inline-block"></span>').css({'display':'block'}).parent().zoom();
			}
		});
	$('.phone_box').click(function(){
		$.fancybox("#form_adv_website_request");
	});

    /**
     * Order advertisement
     */
	$('#form_adv_website_request button').click(function(){
		$('#ajax-loader').show();
		var url = $("#adv_website_request").attr( 'action' );
		try {
			$.post(url, $("#adv_website_request").serialize(), function(data){
				$('#ajax-loader').hide();
				if (data.status == 0) {
					alert(data.message);
				} else {
					$.fancybox.close();
				}
			});
		} catch(e) {
			$('#ajax-loader').hide();
		}
	});


    /**
     * Top-banners in Carousel
     */
	$('#top_banner').jcarousel({
		auto: 4,
        scroll: 1,
		wrap: 'last',
		initCallback: mycarousel_initCallback
	});

	if( ! $('#vacanciesCanvas').tagcanvas({
		textColour : '#000000',
		textHeight: 30,
		outlineThickness : 2,
		outlineColour : '#757575',
		maxSpeed : 0.15,
		depth : 0.75
	}, 'vacancies_tags')) {
		// TagCanvas failed to load
		$('#tag-vacancy-cloud').hide();
	}

	if( $('#brandsCanvas') ) {
		if( ! $('#brandsCanvas').tagcanvas({
			shape : "vring",
			lock : "y",
			stretchX : 11,
			outlineThickness : 2,
			outlineColour : '#757575',
			maxSpeed : 0.15,
			depth : 0.75
		}, 'company_tags')) {
			// TagCanvas failed to load
			$('#tag-icons-cloud').hide();
		}
	}

	/**
	 * Кликаем по ссылке "профессиям"
	 */
	$('#profession_selection_trigger').click(function(){
		$('.precision a').css({'color':'#4E4E4E', 'font-weight':'normal'});
		$(this).css({'color':'black', 'font-weight':'bold'});
		$('#query').attr('placeholder', 'Введите название профессии или часть слова');
	});

	/**
	 * Кликаем по ссылке "компаниям"
	 */
	$('#companies_selection_trigger').click(function(){
		$('.precision a').css({'color':'#4E4E4E', 'font-weight':'normal'});
		$(this).css({'color':'black', 'font-weight':'bold'});
		$('#type').val( 'companies' );
		$('#query').attr('placeholder', 'Введите название компании или часть слова');
	});

	/**
	 * Тыкаем по ссылке "отраслям"
	 */
	$('#categories_selection_trigger').click(function () {
		$('.precision a').css({'color':'#4E4E4E', 'font-weight':'normal'});
		$(this).css({'color':'black', 'font-weight':'bold'});
	});

	/**
	 * Показ блока Выбор региона
	 */
	$('.select_region').click(function () {
		$('#regions_box').show();
	});

	/**
	 * Выбор региона
	 */
	$('#regions_box a').click(function () {
		$('#regions_box').hide();
		$.post("/lib/api/region.php", { city: $(this).text() }, function(data){
//            console.log(data);
			if (data.status == 0)
			{
				$('.select_region').text( data.city );
				var myHost = getMyHost(window.location.pathname,data.citySubDomain);
//				console.log(myHost);
				gotoUrl( myHost );
			}
		}, "json");
	});

	if ( $('#type').val() == 'resumes' ) {
		$('#companies_selection_trigger').parent().hide();
	}

	if ($('#search_field').hasClass('color2')) {
		$('#tab2 > a').click();
	} else {
		$('#extended_search_trigger').attr('href', getMyHost() + '/adv-search');
	}
});

