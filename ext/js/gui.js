/********************************************************
 * Created by Shirokovskiy D. jimmy.webstudio@gmail.com
 *
 */


/** ********** Created by Jimmy 2008.02.07 17:11 **************************
* Определить браузер
**************************************************************************/
var isDOM = document.getElementById; //DOM1 browser (MSIE 5+, Netscape 6, Opera 5+)
var isOpera = isOpera5 = window.opera && isDOM; //Opera 5+
var isOpera6 = isOpera && window.print; //Opera 6+
var isOpera7 = isOpera && document.readyState; //Opera 7+
var isMSIE4 = document.all && document.all.item && !isOpera; //Microsoft Internet Explorer 4+
var isMSIE = isDOM && isMSIE4; //MSIE 5+
var isNetscape4 = document.layers; //Netscape 4.*
var isMozilla = isDOM && navigator.appName == "Netscape"; //Mozilla или Netscape 6.*



function jsShowModal(strPagePath, intWidth, intHeight, stScroll, stResize, strWinName) {
  var scroll = stScroll;
  var resize = stResize;

  if ( strWinName == '' )
    strWinName = 'fcmModal';

  if ( resize == '' )
    resize = 'yes';

  var top=0, left=0;
  if(intWidth > screen.width-10 || intHeight > screen.height-28) {
    scroll = 'yes';
  }

  if(intHeight < screen.height-28)
    top = Math.floor((screen.height - intHeight)/2-14);

  if(intWidth < screen.width-10)
    left = Math.floor((screen.width - intWidth)/2);

  intWidth = Math.min(intWidth, screen.width-10);
  intHeight = Math.min(intHeight, screen.height-28);
  window.open(strPagePath, strWinName, 'menubar=no, scrollbars='+scroll+', resizable='+resize+', width='+intWidth+', height='+intHeight+', left='+left+', top='+top);
}

/** ********** Created by Jimmy 2006.11.25 22:45 **************************
* Закрывание модального окна с предварительной перезагрузкой родительского
**************************************************************************/
function doClose( iTime, obj ) {
  //var objHrefReload;

  iTime = parseInt(iTime);

  if( !iTime ) {
    iTime = 1000;
  }

  //objHrefReload = obj.location.href;
  obj.location.reload();
  setTimeout(window.close(), iTime);
}

/** ********** Created by Jimmy 2006.04.23 16:35 **************************
* Подтверждение на удаление, с переходом по ссылке
**************************************************************************/
function confirmDelete(url, msgtxt) {
  if( !msgtxt ) {
    msgtxt = '';
  }
  if(confirm("Вы действительно хотите УДАЛИТЬ запись?"+"\n"+msgtxt)) {
    if ( url != '' ) {
      location.href = ''+url;
    }
  }
}

/** ********** Created by Jimmy 2008.05.06 15:23 **************************
* Подтверждение действия, с переходом по ссылке
**************************************************************************/
function confirmAction(url, msgtxt) {
  if( !msgtxt ) {
    msgtxt = '';
  }
  if( confirm(msgtxt+"\n"+"Вы уверены?") ) {
    if ( url != '' ) {
      location.href = ''+url;
    }
  }
}

/** ********** Created by Jimmy 2009.03.31 16:29 **************************
* Переход по ссылке
**************************************************************************/
function gotoUrl( url ) {
  window.location.href=url;
}

/**
 * Get current hostname
 */
function getMyHost(path,subdomen) {
    var host = window.location.hostname;
    var r = new RegExp("^.+\\..+\\..+");

    if(r.test(host)) {
        host = host.substr(host.indexOf(("."))+1,host.length);
    }

	return window.location.protocol + '//' + (subdomen ? subdomen + "." : '') + host + (path ? path : '');
}

/* Поддержка регулярных выражений */
var reSupport = 0;
function winInit() {
	if (window.RegExp) {
		var tempStr = "a";
		var tempReg = new RegExp(tempStr);
		if (tempReg.test(tempStr)) { reSupport = 1; }
	}
}

/* Проверка правильно написанного Email */
function isEmail(str) {
  if (!reSupport) { return (str.indexOf(".") > 2) && (str.indexOf("@") > 0); }
  var r1 = new RegExp("(@.*@)|(\\.\\.)|(@\\.)|(^\\.)");
  var r2 = new RegExp("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$");
  return (!r1.test(str) && r2.test(str));
}

/**
 * Checks if variable is empty
 *
 * @param el
 * @return {Boolean}
 * @author Dimitry Shirokovskiy
 */
function isEmpty(el) {
	if (typeof(el) === 'undefined' || el == '' || el == null || el == false) {
		return true;
	}
	return false;
}


/* Очистить блок. Как правило применяется к DIV и подобным тэгам, имеющим парам. innerHTML */
function clearBlock( name ) {
  if ( document.getElementById(name) ) {
    var obj = document.getElementById(name);
    obj.innerHTML = '';
    obj.style.display = 'none';
  }
}

/* Очистка textarea */
function clearTextA( id ) {
  var o = getObj( id );

  o.innerText = o.innerHTML = '';
}

function setInnerHTML ( id, val ) {
  var o = getObj( id );

  o.innerText = o.innerHTML = val;
}

/* Отметить все CheckBox-ы в форме, переданные через запятую */
function checkUncheck( theElements, tagControllerID ) {
  var formElements = theElements.split(',');
  if ( tagControllerID != '' && tagControllerID != undefined ) {
    var theController = document.getElementById(tagControllerID);
  }

  for( var z=0; z<formElements.length; z++ ) {
    theItem = document.getElementById(formElements[z]);

    if ( theItem.type ) {
      if ( theItem.type == 'checkbox' ) {
        if ( theController && theController.type == 'checkbox') {
          theItem.checked = theController.checked;
        } else {
          theItem.checked = true;
        }
      }
    } else {
      theInputs = theItem.getElementsByTagName('input');

      for( var y=0; y<theInputs.length; y++) {
        if( theInputs[y].type == 'checkbox' && theInputs[y].id != theController.id ) {
          if ( theController && theController.type == 'checkbox') {
            theInputs[y].checked = theController.checked;
          } else {
            theInputs[y].checked = true;
          }
        }
      }
    }
  }
}

/* Отметить все CheckBox-ы в форме. objForm - ссылка на объект, ch - TRUE/FALSE (выделить / снять выделение) */
function checkAll( objForm, ch ) {
  if (objForm && objForm.tagName == 'FORM') {
    var formElements = objForm.elements;
    for( var i=0; i < formElements.length; i++) {
      if (formElements[i].tagName == 'td' || formElements[i].tagName == 'div' || formElements[i].style.display == 'none') {
        continue;
      }
      if (formElements[i].type && formElements[i].type == 'checkbox') {
        formElements[i].checked = ch;
      }
    }
  }
}

/** ********** Created by Jimmy 2008.02.06 15:43 **************************
* Скрыть объект. Показать объект
**************************************************************************/
function hideBlock( obj_id ) {
  if ( document.getElementById( obj_id ) ) {
    document.getElementById( obj_id ).style.display = 'none';
  }
}

function showBlock( obj_id, type ) {
  if (type == undefined) type = 'block';
  if ( document.getElementById( obj_id ) ) {
    document.getElementById( obj_id ).style.display = type;
  }
}

/**
* Реверсивно показываем и скрываем
*/
function HideShow ( el ) {
  if (document.getElementById(el).style.display != 'none') {
    document.getElementById(el).style.display = 'none';
  } else {
    document.getElementById(el).style.display = 'block';
  }
}


/**
* Специфичная функция
*/
function HideShowDisEnabled ( el ) {
  var objEl = document.getElementById(el);
  var objColl = objEl.getElementsByTagName("input");

  if (objColl.item(0)) {
    var objElements = objColl.item(0).form.elements;
  }

  if (objEl.style.display != 'none') {
    objEl.style.display = 'none';
    if (objElements && objElements.length > 0) {
      for( var i=0; i < objElements.length; i++) {
        if (objElements[i].tagName == 'INPUT' || objElements[i].tagName == 'SELECT' || objElements[i].tagName == 'TEXTAREA' || objElements[i].tagName == 'BUTTON') {
          for ( var s = 0; s < objColl.length; s++ ) {
            if ( objColl[s].name == objElements[i].name ) {
              objElements[i].disabled = true;
            }
          }
        }
      }
    }
  } else {
    objEl.style.display = 'block';
    if (objElements && objElements.length > 0) {
      for( var i=0; i < objElements.length; i++) {
        if (objElements[i].tagName == 'INPUT' || objElements[i].tagName == 'SELECT' || objElements[i].tagName == 'TEXTAREA' || objElements[i].tagName == 'BUTTON') {
          for ( var s=0; s < objColl.length; s++ ) {
            if ( objColl[s].name == objElements[i].name ) {
              objElements[i].disabled = false;
            }
          }
        }
      }
    }
  }
}

function reloadPage() {
  window.location.reload();
}

function setFocusNext( obj, i, obj_id ) {
  if (obj.value.length >= i)
    document.getElementById(obj_id).focus();
}

function setValue( id, val ) {
  document.getElementById( id ).value = val;
}

function ElementDisabled( obj, status ) {
  obj.disabled = status;
}

function ReturnValueCbx( obj ) {
  obj.checked = !obj.checked;
}

function getObj ( obj_id ) {
  return document.getElementById(obj_id);
}

function getSelectBoxValue ( tagID ) {
  if (document.getElementById( tagID ))
    return document.getElementById( tagID ).options[document.getElementById( tagID ).selectedIndex].value;
  else
    alert ('Объект не найден!');
  return false;
}

function getSBValue( tagID ) {
  return getSelectBoxValue ( tagID );
}

function getValue( tagID ) {
  if (document.getElementById( tagID ))
    return document.getElementById( tagID ).value;
  else
    alert ('Объект не найден!');
  return false;
}

function chImg ( obj, turn, target, replace ) {
  if (obj && turn && target && replace) {
    if ( turn == 'on' ) {
      obj.src = str_replace( target, replace, obj.src );
    } else {
      obj.src = str_replace( replace, target, obj.src );
    }
  }
}

function str_replace(search, replace, subject) {
  return subject.split(search).join(replace);
}

function okExec( obj, evt ) {
  evt = (evt) ? evt : ((window.event) ? window.event : "")
  if (evt) {
    if (evt.keyCode) strKeyCode = evt.keyCode;
    else
    if (evt.which) strKeyCode = evt.which;

    if (strKeyCode) {
      if (strKeyCode == 13 || strKeyCode == 27 || strKeyCode == 16 || strKeyCode == 17 || strKeyCode == 18 ||
          strKeyCode == 37 || strKeyCode == 38 || strKeyCode == 39 || strKeyCode == 40 ) {
        return false;
      }
    }
  }

  return true;
}

function convertDate( obj_tag_name_id, convert_type ) {
  var date = getValue( obj_tag_name_id );
  var new_date = '';

  if(convert_type == undefined) convert_type = 1;

  if (convert_type == 1) { // DD.MM.YYYY => YYYY-MM-DD
    arr = date.split('.');
    new_date = arr[2]+'-'+arr[1]+'-'+arr[0];
  } else if (convert_type == 2) {  // YYYY-MM-DD => DD.MM.YYYY
    arr = date.split('-');
    new_date = arr[2]+'.'+arr[1]+'.'+arr[0];
  } else {
    alert('Unknown convert type!');
  }

  return new_date;
}

/*********************************************************/
/* Копирование текста из поля в буфер                    */
/*********************************************************/
function CopyClipboard(obj){
  obj = findObj(obj);
  if (obj) {
    window.clipboardData.setData('Text', obj.value);
  }
}

/*********************************************************/
/* Все свойства объекта                                  */
/*********************************************************/
function fnShowProps(obj, objName){
  var result = "";
  for (var i in obj) // обращение к свойствам объекта по индексу
    result += objName + "." + i + " = " + obj[i] + "<br />\n";
  document.write(result);
}

function getShowProps(obj, objName) {
	var result = "";
	for (var i in obj) // обращение к свойствам объекта по индексу
		result += objName + "." + i + " = " + obj[i] + "\n";
	return result;
}

function findObj(n, d) {
  var p,i,x;
  if(!d) d=document;
  if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);
  }

  if(!(x=d[n])&&d.all) x=d.all[n];
  for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function getClickURL() {
	if (self.clickURL) return clickURL;
	return document.location.href;
}

function getClickTitle() {
	if (self.clickTitle) return clickTitle;
	return document.title;
}

function _getTime() {
  now = new Date();
  hours = now.getHours();
  minuts = now.getMinutes();
  if (hours<10) hours="0"+hours;
  if (minuts<10) minuts="0"+minuts;
  curTime = hours+':'+minuts;
  return curTime;
}

function _getDate() {
  now = new Date();
  day = now.getDate();
  month = now.getMonth()+1;
  year = now.getYear();
  if (day<10) day="0"+day;
  if (month<10) month="0"+month;
  curDate = day+'-'+month+'-'+year;
  return curDate;
}

function dounique() { var date = new Date(); return date.getTime(); }

function isArray(obj) {
	return obj instanceof Array;
}

/**
 * Extract hostname
 * @returns {*}
 */
String.prototype.getHostname = function() {
	var re = new RegExp('^(?:f|ht)tp(?:s)?\://([^/]+)', 'im');
	return this.match(re)[1].toString();
}
