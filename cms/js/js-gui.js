var ns = document.layers;
var ie = document.all;

function showLayer(layerName){
	eval(layerRef+'["'+layerName+'"]'+styleSwitch+'.visibility="visible"');
}

function hideLayer(layerName){
	eval(layerRef+'["'+layerName+'"]'+styleSwitch+'.visibility="hidden"');
}

function findObj(n, d) {
	var p,i,x;
	if(!d) d=document;
	if((p=n.indexOf("?"))>0&&parent.frames.length) {
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);
	}

	if(!(x=d[n])&&d.all) x=d.all[n];

	for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

	for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=findObj(n,d.layers[i].document);

	if(!x && document.getElementById) x=document.getElementById(n); return x;
}

/*********************************************************/
/* Копирование текста из поля в буфер                    */
/*********************************************************/
function CopyClipboard(obj){
	obj = findObj(obj);
	if (obj) {
		window.clipboardData.setData('Text', obj.value);
	}
}


/*********************************************************/
/* Все свойства объекта                                  */
/*********************************************************/
function fnShowProps(obj, objName){
	var result = "";
	for (var i in obj) // обращение к свойствам объекта по индексу
		result += objName + "." + i + " = " + obj[i] + "<br />\n";
	document.write(result);
}


/*********************************************************/
/* Created by Jimmy (shirokovskij@mail.ru)                        */
/*********************************************************/
function jsShowModal(strPagePath, intWidth, intHeight, stScroll, stResize, strWinName) {
	var scroll = stScroll;
	var resize = stResize;

	if ( strWinName == '' )
		strWinName = 'winModal';

	if ( resize == '' )
		resize = 'yes';

	var top=0, left=0;
	if(intWidth > screen.width-10 || intHeight > screen.height-28) {
		scroll = 'yes';
	}

	if(intHeight < screen.height-28)
		top = Math.floor((screen.height - intHeight)/2-14);

	if(intWidth < screen.width-10)
		left = Math.floor((screen.width - intWidth)/2);

	intWidth = Math.min(intWidth, screen.width-10);
	intHeight = Math.min(intHeight, screen.height-28);
	window.open(strPagePath, strWinName, 'scrollbars='+scroll+', resizable='+resize+', width='+intWidth+', height='+intHeight+', left='+left+', top='+top+', menubar=no, directories=no, location=no, toolbar=no, status=no');
}
/*****/

function jsKeyPress() {
	if(window.event.keyCode == 27)
		window.close();
}

/** ********** Created by Jimmy 2006.04.23 16:35 **************************
 * Подтверждение на удаление, с переходом по ссылке
 **************************************************************************/
function confirmDelete(url, msgtxt)
{
	if( !msgtxt ) {
		msgtxt = '';
	}
	if(confirm("Вы действительно хотите УДАЛИТЬ запись?"+"\n"+msgtxt)) {
		if ( url != '' ) {
			location.href = ''+url;
		}
	}
}

/** ********** Created by Jimmy 2006.11.25 22:45 **************************
 * Закрывание модального окна с предварительной перезагрузкой родительского
 **************************************************************************/
function doClose( iTime, obj ) {
	iTime = parseInt(iTime);

	if( !iTime ) {
		iTime = 1000;
	}

	obj.location.reload();
	setTimeout(window.close(), iTime);
}

/**
 * Checks if variable is empty
 *
 * @param el
 * @return {Boolean}
 * @author Dimitry Shirokovskiy
 */
function isEmpty(el) {
	if (typeof(el) === 'undefined' || el == '' || el == null || el == false) {
		return true;
	}
	return false;
}

/**
 * Checkout amount of days in month
 * @param iMonth
 * @param iYear
 * @returns {number}
 */
function daysInMonth(iMonth, iYear)
{
	return 32 - new Date(iYear, iMonth, 32).getDate();
}

/** ********** Created  Wed Nov 25 07:05:16 GMT 2009 *************
 * Get unique value
 **************************************************************************/
function getUnique() { var date = new Date(); return (Math.round(Math.random() * date.getTime()) +1) }

function isArray(obj) {
	return obj instanceof Array;
}

/**
 * Шаблонозависимая функция для CMS
 * Включает кнопку сохранения контента (шаблона, страницы, фрагмента)
 */
function doAccept() {
    $('#smbSaveDoc').attr('disabled', false);
    $('#cbxPublish').attr('checked', true);
    alert("Этим выбором Вы соглашаетесь сохранить то, что видите в поле контента!\nСохраните Ваши изменения, или вернитесь к списку.");
}