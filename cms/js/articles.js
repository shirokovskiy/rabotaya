/**
 * Created by Dmitry Sirokovskiy.
 * Email: info@phpwebstudio.com
 * Date: 4/28/14
 * Time: 4:27 PM
 * Description:
 */
Ext.Loader.setConfig({enabled: true});
Ext.Loader.setPath('Ext.ux', '/cms/js/extjs/resources/ux');
Ext.require([
	'Ext.grid.*',
	'Ext.data.*',
	'Ext.util.*',
	'Ext.toolbar.Paging',
	'Ext.ux.PreviewPlugin',
	'Ext.ModelManager',
	'Ext.tip.QuickTipManager'
]);
Ext.ns('Articles');

// application main entry point
Ext.onReady(function() {
	Ext.QuickTips.init();

	Ext.define('Article.Window', {
		extend: 'Ext.window.Window',
		width: 680,
		height: 320,
		layout: 'fit',
		autoDestroy: true,
		closeAction: 'destroy'
	});

	Ext.define('ArticleModel', {
		extend: 'Ext.data.Model',
		idProperty: 'sa_id', // см.таблицу модели и имя первичного ключа (поля)
		fields: [
			{name:'sa_id', type: 'int'},
			'sa_title',
			'sa_content',
			{name:'sa_date_publish', type: 'int'},
			{name:'sa_date_add', type:'date', dateFormat: 'Y-m-d H:i:s'},
			{name:'sa_date_change', type:'date', dateFormat: 'Y-m-d H:i:s'}
		]
	});

	Ext.define('Article.Form', {
		extend: 'Ext.form.Panel',
		id: 'ArticleForm',
		url: '/lib/api/cms/article-edit.php',
		layout: 'anchor', // ???
		autoScroll: true,
		defaultType: 'textfield',
		defaults: {
			anchor: '98%',
			labelWidth: 100,
			labelStyle: 'padding-left: 10px',
			padding: '5px'
		},
		items: [
			{
				xtype: 'hiddenfield',
				name: 'sa_id'
			},{
				fieldLabel: 'Заголовок',
				name: 'sa_title',
				allowBlank: false
			},{
				xtype: 'htmleditor',// 'textareafield',
				fieldLabel: 'Статья',
				name: 'sa_content',
				grow      : true,
				allowBlank: false,
				height: 200
			}
		],
		buttons: [
			{
				text: 'Сохранить',
				handler: function(){
					var form = this.up('form').getForm();
					if(form.isValid()) {
						form.submit({
							success: function(){
								Articles.Store.reload();
								this.up('window').close();
							},
							failure: function(form, action){
								Ext.Msg.alert('Failed', action.result.message);
							},
							scope: this
						});
					}
				}
			},{
				text: 'Отмена',
				handler: function(){
					this.up('window').close();
				}
			}
		]
	});

	Articles.Toolbar = Ext.create('Ext.toolbar.Toolbar',{
		items: [
			{
				text: 'Новая статья',
				scope: this,
				handler: function() {
					var window = Article.Window.create();
					var form = Article.Form.create();

					window.setTitle('Новая статья');
					window.add(form);
					window.show();
				}
			},'-',{
				text: 'Удалить',
				id: 'removeButton',
				action: 'all',
				disabled: true,
				handler: function() {
					var record = Articles.Grid.getSelectionModel().getSelection()[0];
					Ext.Msg.show({
						title: 'Удалить статью',
						msg: 'Вы действительно хотите удалить?',
						buttons: Ext.Msg.YESNO,
						fn: function(btn){
							if (btn=='yes') {
								Ext.Ajax.request({
									url: '/lib/api/cms/delete.php',
									params: {act: 'a', id: record.data.sa_id},
									success: function() {
										Articles.Store.reload();
									}
								});
							}
						}
					});
				}
			}
		]
	});

	var limitPageSize = 15;

	Articles.Store = Ext.create('Ext.data.Store', {
		storeId: 'ArticlesStore',
		model: 'ArticleModel',
		autoSync: false, // ???
		pageSize: limitPageSize,
		remoteSort: true, // параметры сортировки будут посылаться на сервер
		sorters: [
			{
				property: 'sa_id',
				direction: 'DESC'
			}
		],
		listeners: {
			'load': function(store, records, success) {
				var msg = store.getProxy().getReader().rawData.message;
				if (success) {
					if (!isEmpty(msg)) {
						Ext.Msg.show({
							modal  : true,
							title  : 'Операция выполнена успешно',
							msg    : msg,
							buttons: Ext.Msg.OK
						});
					}

				} else {
					Ext.Msg.show({
						modal  : true,
						title  : 'Ошибка!',
						msg    : msg,
						icon   : Ext.Msg.ERROR,
						buttons: Ext.Msg.OK
					});
				}
			}
		},
		proxy: {
			type: 'ajax',
			url: '/lib/api/cms/articles.php',
			reader: {
				id: 'sa_id',
				type: 'json',
				root: 'data',
				totalProperty: 'total',
				messageProperty: 'message',
				successProperty: 'success'
			}
			, simpleSortMode: true // если включить, тогда будет посылаться простой вариант параметров sorters, в противном случае аяксом уходят json-параметры sorters
		}
		, autoLoad: true
	});

	Articles.Grid = Ext.create('Ext.grid.Panel', {
		tbar: Articles.Toolbar,
		store: Articles.Store,
		layout: 'fit',
		autoExpandColumn: 'sa_title',
		width: 900,
		renderTo: 'articlesStore', // идентификатор объекта (тэга) куда будем выводить
		columns: [
			{
				text: 'ID',
				width: 50,
				align: 'center',
				sortable: true,
				dataIndex: 'sa_id'
			},{
				text: 'Заголовок',
				width: 200,
				sortable: true,
				dataIndex: 'sa_title',
				flex: 1
			},{
				text: 'Дата созд.',
				width: 120,
				sortable: true,
				dataIndex: 'sa_date_add',
				renderer: function(value){
					return Ext.util.Format.date(value, 'd.m.Y H:i:s');
				}
			} // TODO: добавить поля статуса и изображения
		],
		listeners: {
			itemdblclick: function(grid, record) {
				var form = Article.Form.create(),
					window = Article.Window.create();

				// предзаполнение хранилищ организатора/лидера значениями из записи
				form.loadRecord(record);

				window.setTitle('ID: #' + record.get("sa_id"));
				window.add(form);
				window.show();
			},
			'select': function() {
				var buttonRemove = Ext.getCmp('removeButton');
				buttonRemove.setDisabled(false);
			}
		},
		bbar: Ext.create("Ext.PagingToolbar", {
			store: Articles.Store,
			displayInfo: true,
			pageSize: limitPageSize,
			displayMsg: "Показано {0} - {1} из {2}",
			emptyMsg: 'Нет записей'
		})
	});
}); // eo function onReady

