/**
 * Created by Dmitry Sirokovskiy.
 * Email: info@phpwebstudio.com
 * Date: 5/28/14
 * Time: 3:27 PM
 * Description:
 */
Ext.Loader.setConfig({enabled: true});
Ext.Loader.setPath('Ext.ux', '/cms/js/extjs/resources/ux');
Ext.require([
	'Ext.grid.*',
	'Ext.data.*',
	'Ext.util.*',
	'Ext.toolbar.Paging',
	'Ext.ux.PreviewPlugin',
	'Ext.ModelManager',
	'Ext.tip.QuickTipManager',
	'Ext.ux.grid.FiltersFeature'
]);
Ext.ns('Cities');

// application main entry point
Ext.onReady(function() {
	Ext.QuickTips.init();

	Ext.define('City.Window', {
		extend: 'Ext.window.Window',
		width: 680,
		height: 320,
		layout: 'fit',
		autoDestroy: true,
		closeAction: 'destroy'
	});

	Ext.define('CityModel', {
		extend: 'Ext.data.Model',
		idProperty: 'sa_id', // см.таблицу модели и имя первичного ключа (поля)
		fields: [
			{name:'id', type: 'int'},
			'city',
			'city_eng',
			{name:'country_id', type:'int'},
			{name:'pos', type:'int'}
		]
	});

	Cities.MainSelectedStore = Ext.create('Ext.data.Store', {
		fields: ['abbr', 'name'],
		data: [{
			abbr:'N',
			name:'Обычный город'
		},{
			abbr:'Y',
			name:'Топ-город'
		}]
	});

	Ext.define('City.Form', {
		extend: 'Ext.form.Panel',
		id: 'CityForm',
		url: '/lib/api/cms/city-edit.php',
		layout: 'anchor', // ???
		autoScroll: true,
		defaultType: 'textfield',
		defaults: {
			anchor: '98%',
			labelWidth: 100,
			labelStyle: 'padding-left: 10px',
			padding: '5px'
		},
		items: [
			{
				xtype: 'hiddenfield',
				name: 'id'
			},{
				fieldLabel: 'Город',
				name: 'city',
				allowBlank: false
			},{
				fieldLabel: 'По англ.',
				name: 'city_eng',
				allowBlank: true
			},{
				fieldLabel: 'Позиция',
				name: 'pos',
				allowBlank: true
			},{
				xtype: 'combo',
				fieldLabel: 'Выводить как',
				name: 'main_selected',
				store: Cities.MainSelectedStore,
				editable: false,
				allowBlank: false,
				forceSelection: true,
				displayField: 'name',
				valueField: 'abbr'
			}
		],
		buttons: [
			{
				text: 'Сохранить',
				handler: function(){
					var form = this.up('form').getForm();
					if(form.isValid()) {
						form.submit({
							success: function(){
								Cities.Store.reload();
								this.up('window').close();
							},
							failure: function(form, action){
								Ext.Msg.alert('Failed', action.result.message);
							},
							scope: this
						});
					}
				}
			},{
				text: 'Отмена',
				handler: function(){
					this.up('window').close();
				}
			}
		]
	});

	Cities.Toolbar = Ext.create('Ext.toolbar.Toolbar',{
		items: [
			{
				text: 'Добавить город',
				scope: this,
				handler: function() {
					var window = City.Window.create();
					var form = City.Form.create();

					window.setTitle('Новый город');
					window.add(form);
					window.show();
				}
			},'-',{
				text: 'Удалить',
				id: 'removeButton',
				action: 'all',
				disabled: true,
				handler: function() {
					var record = Cities.Grid.getSelectionModel().getSelection()[0];
					Ext.Msg.show({
						title: 'Удалить город',
						msg: 'Вы действительно хотите удалить?',
						buttons: Ext.Msg.YESNO,
						fn: function(btn){
							if (btn=='yes') {
								Ext.Ajax.request({
									url: '/lib/api/cms/delete.php',
									params: {act: 'city', id: record.data.id},
									success: function() {
										Cities.Store.reload();
									}
								});
							}
						}
					});
				}
			}
		]
	});

	var limitPageSize = 20;

	Cities.Store = Ext.create('Ext.data.Store', {
		storeId: 'CitiesStore',
		model: 'CityModel',
		autoDestroy: true,
		autoSync: false, // ???
		autoLoad: true,
		pageSize: limitPageSize,
		remoteSort: true, // параметры сортировки будут посылаться на сервер
		sorters: [
			{
				property: 'id',
				direction: 'DESC'
			},{
				property: 'city',
				direction: 'ASC'
			}
		],
		listeners: {
			'load': function(store, records, success) {
				var msg = store.getProxy().getReader().rawData.message;
				if (success) {
					if (!isEmpty(msg)) {
						Ext.Msg.show({
							modal  : true,
							title  : 'Операция выполнена успешно',
							msg    : msg,
							buttons: Ext.Msg.OK
						});
					}

				} else {
					Ext.Msg.show({
						modal  : true,
						title  : 'Ошибка!',
						msg    : msg,
						icon   : Ext.Msg.ERROR,
						buttons: Ext.Msg.OK
					});
				}
			}
		},
		proxy: {
			type: 'ajax',
			url: '/lib/api/cms/cities.php',
			reader: {
				id: 'id',
				type: 'json',
				root: 'data',
				totalProperty: 'total',
				messageProperty: 'message',
				successProperty: 'success'
			}
			, simpleSortMode: true // если включить, тогда будет посылаться простой вариант параметров sorters, в противном случае аяксом уходят json-параметры sorters
		}
	});

	// configure whether filter query is encoded or not (initially)
	var encode = false;

	// configure whether filtering is performed locally or remotely (initially)
	var local = false;

	var filters = {
		ftype: 'filters',
		// encode and local configuration options defined previously for easier reuse
		encode: encode, // json encode the filter query
		local: local,   // defaults to false (remote filtering)

		// Filters are most naturally placed in the column definition, but can also be
		// added here.
		filters: [{
			type: 'boolean',
			dataIndex: 'city'
		}]
	};

	Cities.Grid = Ext.create('Ext.grid.Panel', {
		tbar: Cities.Toolbar,
		store: Cities.Store,
		layout: 'fit',
		features: [filters],
		autoExpandColumn: 'city',
		width: 900,
		renderTo: 'citiesStore', // идентификатор объекта (тэга) куда будем выводить
		emptyText: 'No Matching Records',
		columns: [
			{
				text: 'ID',
				width: 50,
				align: 'center',
				sortable: true,
				dataIndex: 'id'
			},{
				text: 'Город',
				width: 200,
				sortable: true,
				dataIndex: 'city',
				flex: 1,
				filter: {
					type: 'string'
				}
			}
		],
		listeners: {
			itemdblclick: function(grid, record) {
				var form = City.Form.create(),
					window = City.Window.create();

				// предзаполнение хранилищ значениями из записи
				form.loadRecord(record);

				window.setTitle('ID: #' + record.get("id"));
				window.add(form);
				window.show();
			},
			'select': function() {
				var buttonRemove = Ext.getCmp('removeButton');
				buttonRemove.setDisabled(false);
			}
		},
		bbar: Ext.create("Ext.PagingToolbar", {
			store: Cities.Store,
			displayInfo: true,
			pageSize: limitPageSize,
			displayMsg: "Показано {0} - {1} из {2}",
			emptyMsg: 'Нет записей'
		})
	});
}); // eo function onReady
