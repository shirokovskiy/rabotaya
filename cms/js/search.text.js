/**
 * Created by Dmitry Sirokovskiy.
 * Email: info@phpwebstudio.com
 * Date: 4/6/14
 * Time: 1:35 AM
 * Description:
 */

Ext.Loader.setConfig({enabled: true});
Ext.Loader.setPath('Ext.ux', '/cms/js/extjs/resources/ux');
Ext.require([
	'Ext.grid.*',
	'Ext.data.*',
	'Ext.util.*',
	'Ext.toolbar.Paging',
	'Ext.ux.PreviewPlugin',
	'Ext.ModelManager',
	'Ext.tip.QuickTipManager'
]);
Ext.ns('SearchText');
// application main entry point
Ext.onReady(function() {
	Ext.QuickTips.init();

	Ext.define('SearchText.Window', {
		extend: 'Ext.window.Window',
		width: 680,
		height: 320,
		layout: 'fit',
		autoDestroy: true,
		closeAction: 'destroy'
	});

	Ext.define('SearchTextModel', {
		extend: 'Ext.data.Model',
		idProperty: 'sst_id',
		fields: [
			{name:'sst_id', type: 'int'},
			'sst_word',
			'sst_text',
			{name:'sst_date', type:'date', dateFormat: 'Y-m-d H:i:s'}
		]
	});

	Ext.define('SearchText.Form', {
		extend: 'Ext.form.Panel',
		id: 'SearchTextForm',
		url: '/lib/api/cms/searchtext-edit.php',
		layout: 'anchor', // ???
		autoScroll: true,
		defaultType: 'textfield',
		defaults: {
			anchor: '98%',
			labelWidth: 100,
			labelStyle: 'padding-left: 10px',
			padding: '5px'
		},
		items: [
			{
				xtype: 'hiddenfield',
				name: 'sst_id'
			},{
				fieldLabel: 'Слово',
				name: 'sst_word',
				allowBlank: false
			},{
				xtype: 'htmleditor',// 'textareafield',
				fieldLabel: 'SEO текст',
				name: 'sst_text',
				grow      : true,
				allowBlank: false,
				height: 200
			}
		],
		buttons: [
			{
				text: 'Сохранить',
				handler: function(){
					var form = this.up('form').getForm();
					if(form.isValid()) {
						form.submit({
							success: function(){
								SearchText.Store.reload();
								this.up('window').close();
							},
							failure: function(form, action){
								Ext.Msg.alert('Failed', action.result.message);
							},
							scope: this
						});
					}
				}
			},{
				text: 'Отмена',
				handler: function(){
					this.up('window').close();
				}
			}
		]
	});

	SearchText.Toolbar = Ext.create('Ext.toolbar.Toolbar',{
		items: [
			{
				text: 'Новое слово',
				scope: this,
				handler: function() {
					var window = SearchText.Window.create();
					var form = SearchText.Form.create();

					window.setTitle('Новое слово');
					window.add(form);
					window.show();
				}
			},'-',{
				text: 'Удалить',
				id: 'removeButton',
				action: 'all',
				disabled: true,
				handler: function() {
					var record = SearchText.Grid.getSelectionModel().getSelection()[0];
					Ext.Msg.show({
						title: 'Удалить слово',
						msg: 'Вы действительно хотите удалить запись?',
						buttons: Ext.Msg.YESNO,
						fn: function(btn){
							if (btn=='yes') {
								Ext.Ajax.request({
									url: '/lib/api/cms/delete.php',
									params: {act: 'st', id: record.data.sst_id},
									success: function() {
										SearchText.Store.reload();
									}
								});
							}
						}
					});
				}
			}
		]
	});

	var limitPageSize = 15;

	SearchText.Store = Ext.create('Ext.data.Store', {
		storeId: 'SearchTextStore',
		model: 'SearchTextModel',
		autoSync: false, // ???
		pageSize: limitPageSize,
		remoteSort: true, // параметры сортировки будут посылаться на сервер
		sorters: [
			{
				property: 'sst_id',
				direction: 'DESC'
			}
		],
		listeners: {
			'load': function(store, records, success) {
				var msg = store.getProxy().getReader().rawData.message;
				if (success) {
					if (!isEmpty(msg)) {
						Ext.Msg.show({
							modal  : true,
							title  : 'Операция выполнена успешно',
							msg    : msg,
							buttons: Ext.Msg.OK
						});
					}

				} else {
					Ext.Msg.show({
						modal  : true,
						title  : 'Ошибка!',
						msg    : msg,
						icon   : Ext.Msg.ERROR,
						buttons: Ext.Msg.OK
					});
				}
			}
		},
		proxy: {
			type: 'ajax',
			url: '/lib/api/cms/searchtext.php',
			reader: {
				id: 'sst_id',
				type: 'json',
				root: 'data',
				totalProperty: 'total',
				messageProperty: 'message',
				successProperty: 'success'
			}
			, simpleSortMode: true // если включить, тогда будет посылаться простой вариант параметров sorters, в противном случае аяксом уходят json-параметры sorters
		}
		, autoLoad: true
	});

	SearchText.Grid = Ext.create('Ext.grid.Panel', {
		tbar: SearchText.Toolbar,
		store: SearchText.Store,
		layout: 'fit',
		autoExpandColumn: 'sst_word',
		width: 600,
		renderTo: 'searchTextStore',
		columns: [
			{
				text: 'ID',
				width: 50,
				align: 'center',
				sortable: true,
				dataIndex: 'sst_id'
			},{
				text: 'Слово',
				width: 200,
				sortable: true,
				dataIndex: 'sst_word',
				flex: 1
			},{
				text: 'Дата созд.',
				width: 120,
				sortable: true,
				dataIndex: 'sst_date',
				renderer: function(value){
					return Ext.util.Format.date(value, 'd.m.Y H:i:s');
				}
			}
		],
		listeners: {
			itemdblclick: function(grid, record) {
				var formPanel = SearchText.Form.create(),
					window = SearchText.Window.create();

				// предзаполнение хранилищ организатора/лидера значениями из записи
				formPanel.loadRecord(record);

				window.setTitle('ID: #' + record.get("sst_id"));
				window.add(formPanel);
				window.show();
			},
			'select': function() {
				var buttonRemove = Ext.getCmp('removeButton');
				buttonRemove.setDisabled(false);
			}/*,
			 'viewready': function(){
			 setHeightExtJSGirdOnResizeWindow(SearchText.Grid, $('#center-content-work-area'), -35);
			 }*/
		},
		bbar: Ext.create("Ext.PagingToolbar", {
			store: SearchText.Store,
			displayInfo: true,
			pageSize: limitPageSize,
			displayMsg: "Показано {0} - {1} из {2}",
			emptyMsg: 'Нет записей'
		})
	});
}); // eo function onReady
