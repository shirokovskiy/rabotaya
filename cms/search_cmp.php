<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 8/16/13
 * Time         : 12:50 AM
 * Description  : 
 */
include_once "../cfg/web.cfg.php";

$arr = array();

if(!empty($_REQUEST['term'])) {
    include_once "models/Company.php";
    $objCompany = new Company($_db_config);

    $arr = $objCompany->getCompaniesByName($_REQUEST['term']);
}

//$arr[0]['id'] = 'Деловой Петербург';
//$arr[0]['company'] = 'Деловой Петербург';
//$arr[0]['type'] = 'direct';
//$arr[0]['web'] = 'www.dp.ru';
//$arr[0]['email'] = 'info@dp.ru';
//$arr[0]['phone'] = '+7 812 325 0101';
//$arr[0]['desc'] = 'Bla bla bla';
//$arr[0]['address'] = 'Avenue 17';
//$arr[0]['label'] = $arr[0]['company'];
//$arr[0]['value'] = $arr[0]['id'];

echo json_encode($arr);
