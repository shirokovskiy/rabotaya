<?php
$arrTplVars['module.name'] = "stat.error.404";
$arrTplVars['module.title'] = "СТАТИСТИКА НЕ НАЙДЕННЫХ СТРАНИЦ";
$arrTplVars['module.name.child'] = "stat.error.404";

if ( isset($_GET['action']) ) {
  switch ($_GET['action']) {
    case 'delete':
      if ( intval($_GET['ID'])>0 ) {

      }
      break;
    default: break;
  }
}



// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars["error".$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars["error".$errSuf] = $objUtils->echoMessage($arrTplVars["error".$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************


$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

$strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM ".$_db_tables['stError404'];
$arrTplVars['intQuantitySelectRecords'] = $arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery , 'intQuantAllRecords');

// ***** BEGIN: Построение пейджинга для вывода списка
$objPaginator = new tplPaginator($arrTplVars['intQuantitySelectRecords']);
// Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу, кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
$objPaginator->intQuantRecordPage = 50;
// Создаем блок пэйджинга
$objPaginator->paCreate();
// ***** END: Построение пейджинга для вывода списка

$arrListStat = $objDb->fetchall( "SELECT id as strId, address as strAddress, referer as strReferer, DATE_FORMAT(date,'%d.%m.%Y %H:%i') as strDate FROM ".$_db_tables['stError404']." ORDER BY `id` DESC".$objPaginator->strSqlLimit );
$arrTplVars['intQuantityShowRecOnPage'] = count($arrListStat); // кол-во строк показанных на странице
if (is_array($arrListStat) && count($arrListStat)>0) { // Если есть записи
	$objTpl->tpl_loop($arrTplVars['module.name'], "stat.list", $arrListStat, array($arrTplVars['cBgColor'], $arrTplVars['cThirdColor']));
}

$arrTplVars['blockPaginator'] = $objPaginator->paShow();

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
