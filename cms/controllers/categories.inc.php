<?php
$arrTplVars['module.name'] = "categories";
$arrTplVars['module.title'] = "Отраслевые категории";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

/**
 * Взять все параметры GET-строки
 */
if (is_array($_GET) && !empty($_GET)) {
    $Params = null;
    foreach ($_GET as $k => $value) {
        if ($k == 'errMess') continue;
        $Params[] = $k.'='.$value;
    }

    if (is_array($Params) && !empty($Params)) {
        $strUrlParams = implode('&', $Params);
    }
}

/**
 * Добавляем потомка
 */
if (intval($_GET['idItem'])) {
    $idItem = intval($_GET['idItem']);
    $strSqlQuery = "SELECT * FROM `job_categories` WHERE jc_id = ".$idItem;
    $arrSqlQuery = $objDb->fetch( $strSqlQuery );

    if (!$arrSqlQuery['jc_parent']) {
        $arrTplVars['currentParentGroup'] = $arrSqlQuery['jc_title'];
        $arrIf['addChild'] = true;
    }
}

/**
 * Редактирование категорий
 */
if ($_POST ['frmEdit'] == 'true') {
    foreach ($_POST as $key => $value ) {
        // если обрабатываем имя
        if (substr ($key, 0, 4) == 'name') {
            $nameInt = intval(substr ($key, 4));
            $tmpName = addslashes((trim($value)));
            $strSqlQuery = "UPDATE `job_categories` SET jc_title = '".$tmpName."' WHERE jc_id = ".$nameInt." LIMIT 1";
            if (!$objDb->query ($strSqlQuery)) {
                $GLOBALS['manStatusError']=1;
                $GLOBALS['manCodeError'][]['code'] = 'msgEmptyTitle';
                break;
            }
        }

        // если обрабатываем перемещение подкатегории в другую родительскую категорию
        if (substr ($key, 0, 11) == 'moveToCatID') {
            $cID = intval(substr ($key, 11)); // категория, которую перемещаем
            $catID = intval($value); // категория, куда перемещаем

            if ($catID > 0) {
                $strSqlQuery = "UPDATE `job_categories` SET jc_parent = $catID WHERE jc_id = $cID LIMIT 1";

                if (!$objDb->query ($strSqlQuery)) {
                    $GLOBALS['manStatusError']=1;
                    $GLOBALS['manCodeError'][]['code'] = 'msgEmptyTitle';
                    break;
                }
            }
        }

        // если обрабатываем перемещение подкатегории в другую родительскую категорию
        if (substr ($key, 0, 15) == 'relocateToCatID') {
            $cID = intval(substr ($key, 15)); // категория, которую перемещаем
            $catID = intval($value); // категория, куда перемещаем

            if ($catID > 0) {
                $strSqlQuery = "UPDATE `job_category_link` SET jcl_sjc_id = $catID WHERE jcl_sjc_id = $cID";

                if (!$objDb->query ($strSqlQuery)) {
                    $GLOBALS['manStatusError']=1;
                    $GLOBALS['manCodeError'][]['code'] = 'msgEmptyTitle';
                    break;
                }
            }
        }
    }

    if (!$GLOBALS['manStatusError']) {
        header('location:'.$arrTplVars['module.name'].'?errMess=msgOk&'.$strUrlParams);
        exit();
    }
}

/**
 * Добавление категории
 */
if ($_POST ['frmAdd'] == 'true') {
    $frmName = addslashes(trim($_POST['frmName']));
    if ($frmName) {
        $strSqlQuery = "INSERT INTO `job_categories` SET jc_title = '".$frmName."'";
        if (!$objDb->query ($strSqlQuery)) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgEmptyTitle';
        }
    }

    if (!$GLOBALS['manStatusError']) {
        header('location:'.$arrTplVars['module.name'].'?errMess=msgOk&'.$strUrlParams);
        exit();
    }
}

/**
 * Добавление под-категории
 */
if ($_POST ['frmSubAdd'] == 'true') {
    $frmName = addslashes(trim($_POST['frmName']));
    if ($frmName) {
        $strSqlQuery = "INSERT INTO `job_categories` SET"
            ." jc_title = '$frmName'"
            .", jc_parent = '$idItem'";
        if (!$objDb->query($strSqlQuery)) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgEmptyTitle';
        }
    }

    if (!$GLOBALS['manStatusError']) {
        header('location:'.$arrTplVars['module.name'].'?errMess=msgOk&idItem='.$idItem);
        exit();
    }
}

/**
 * Удаление категории
 */
if (intval($_GET['delItem'])) {
    $delItem = intval($_GET['delItem']);

    // Ещё раз проверить, чтобы категория точно была пустой.
    $strSqlQuery = "SELECT COUNT(*) iC FROM `job_categories` WHERE jc_parent = ".$delItem;
    $thisTempCount = $objDb->fetch( $strSqlQuery , 'iC');
    $thisTempParent = $objDb->fetch( "SELECT jc_parent FROM `job_categories` WHERE jc_id = ".$delItem , 'jc_parent');

    if (empty($thisTempCount)) {
        $strSqlQuery = "DELETE FROM `job_categories` WHERE jc_id = ".$delItem.' LIMIT 1';
        if (!$objDb->query($strSqlQuery)) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDel';
        }

    } else {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgErrorDel';
    }

    if (!$GLOBALS['manStatusError']) {
        header('location:'.$arrTplVars['module.name'].'?errMess=msgDelOk'.($thisTempParent ? "&idItem=$thisTempParent" : ''));
        exit();
    }
}

// ***** Обработка ошибок для вывода *******************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// *****************************************************************************

// **** Загружаем и обрабатываем шаблон ****************************************
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

// ***** Формируем список категорий ********************************************
$strSqlQuery = "SELECT * FROM `job_categories` WHERE jc_parent IS NULL";
$arrRecords = $objDb->fetchall( $strSqlQuery );
$arrMoveCats = array();
if (is_array($arrRecords) && !empty($arrRecords)) {
    $z = 0;
    foreach ($arrRecords as $key => $value) {
        $arrRecords[$key]['intIdItem'] = $pid = intval($value['jc_id']);
        $arrRecords[$key]['strNameItem'] = htmlspecialchars($value['jc_title']);

        // Для пустых групп выводим кнопку "удалить"
        $strSqlQuery = "SELECT COUNT(*) iC FROM `job_categories` WHERE jc_parent = ".$pid;
        $thisTempCount = $objDb->fetch( $strSqlQuery , 'iC');

        if (empty($thisTempCount)) $arrIf['delItem'.$pid] = true;
        $arrParents[$pid] = true;
        /**
         * todo: SELECT * FROM `job_category_link` WHERE `jcl_sjc_id` NOT IN (SELECT jc_id FROM `job_categories`);
         * ALTER TABLE `job_category_link` ADD CONSTRAINT  `sjc_ID` FOREIGN KEY (`jcl_sjc_id`) REFERENCES `rabotaya_web`.`job_categories` (`jc_id`) ON DELETE CASCADE ON UPDATE CASCADE;
         */

        /**
         * Строим полный список для переноса категорий
         */
        $arrMoveCats[$z]['cID']   = $pid;                             # родительский ID
        $arrMoveCats[$z]['cName'] = $arrRecords[$key]['strNameItem']; # родительская категория

        $z++;
    }

} else {
    $arrIf['block.categories.empty'] = true;
}
$objTpl->tpl_loop($arrTplVars['module.name'], "list", $arrRecords);
$objTpl->tpl_loop($arrTplVars['module.name'], "list.all.parent.categories", $arrMoveCats);
$objTpl->tpl_loop($arrTplVars['module.name'], "list.all.parent2.categories", $arrMoveCats);

if (is_array($arrMoveCats) && !empty($arrMoveCats)) {
    foreach ($arrMoveCats as $j => $cat) {
        $arrListSubItems = $objDb->fetchall( "SELECT * FROM `job_categories` WHERE jc_parent = ".$cat['cID']." ORDER BY jc_title" );
        if (is_array($arrListSubItems) && !empty($arrListSubItems)) {
            $arrRelocateCats = array();
            foreach ($arrListSubItems as &$arr) {
                #
                $arr['ccID'] = intval($arr['jc_id']);;
                $arr['ccName'] = htmlspecialchars($arr['jc_title']);
            }
        }
        $objTpl->tpl_loop($arrTplVars['module.name'], "list.all.tree.categories.".$cat['cID'], $arrListSubItems);
    }
}






/**
 * Если выбрана категория
 */
if ($_GET['idItem'] > 0) {
    $sid = intval($_GET['idItem']);
    // ***** Выбираем вложенные категории
    $arrListSubItems = $objDb->fetchall( "SELECT * FROM `job_categories` WHERE jc_parent = ".$sid." ORDER BY jc_title" );
    if (is_array($arrListSubItems) && !empty($arrListSubItems)) {
        $arrIf['block.'.$sid] = true;
        $Category = new JobCategory();

        foreach ($arrListSubItems as $key => $value) {
            $arrListSubItems[$key]['intIdSubItem'] = $value['jc_id'];
            $arrListSubItems[$key]['strNameSubItem'] = htmlspecialchars($value['jc_title']);
            $arrListSubItems[$key]['intResNum'] = $Category->getCategoryLinksNumber(intval($value['jc_id']), 'resume');
            $arrListSubItems[$key]['intVacNum'] = $Category->getCategoryLinksNumber(intval($value['jc_id']), 'vacancy');
        }

    }

    $objTpl->tpl_loop($arrTplVars['module.name'], "list.subitem.".$sid, $arrListSubItems);
}


$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);

