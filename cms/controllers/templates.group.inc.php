<?php
$arrTplVars['module.name'] = "templates.group";
$arrTplVars['module.parent'] = "site.templates";
$arrTplVars['module.title'] = "ГРУППЫ ШАБЛОНОВ";

#$currentProject = $_SESSION['arrAccessProjectId'][1];
$currentProject = $_SESSION['intIdDefaultProject'];


// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");


/* Удаление группы
    Влад Андерсен, 16:09:14 */
if (intval($_GET ['delItem'])) {
    $delItem = intval($_GET ['delItem']);

    // Ещё раз проверить, чтобы группа точно была пустой.
    $strSqlQuery = "SELECT COUNT(*) FROM site_templates WHERE st_group = ".$delItem;
    $thisTempCount = $objDb->fetch( $strSqlQuery , 'COUNT(*)');
    $thisTempParent = $objDb->fetch( "SELECT stg_id_parent FROM site_templates_group WHERE stg_id = ".$delItem , 'stg_id_parent');

    if ($thisTempCount == 0) {
        $strSqlQuery = "DELETE FROM site_templates_group WHERE stg_id = ".$delItem.' LIMIT 1';
        $objDb->query ($strSqlQuery);
        header ("location: ".$arrTplVars['module.name']."".($thisTempParent ? "?idItem=$thisTempParent" : ''));
    }
}


/* Смотрим, добавлять ли чайлд-группу
    Влад Андерсен, 13:39:33 */
if (intval($_GET ['idItem'])) {
    $idItem = intval($_GET ['idItem']);
    $strSqlQuery = "SELECT * FROM site_templates_group WHERE stg_id = '".$idItem."'";
    $arrSqlQuery = $objDb->fetch( $strSqlQuery );

    if (!$arrSqlQuery['stg_id_parent']) {
        $arrTplVars['currentParentGroup'] = $arrSqlQuery['stg_name'];
        $arrIf['addChild'] = true;
    }
}



/* Апдейтим имена и статусы групп шаблонов
    Влад Андерсен, 12:51:25 */
if ($_POST ['frmGroupEdit'] == true) {
    foreach ($_POST as $key => $value ) { // 12:52:49
        if (substr ($key, 0, 4) == 'name') {
            $nameInt = intval(substr ($key, 4));
            $tmpName = addslashes((trim($value)));
            $strSqlQuery = "UPDATE site_templates_group SET stg_name = '".$tmpName."', stg_status = '".($_POST['status'.$nameInt] ? 'Y' : 'N')."' WHERE stg_id = '".$nameInt."' LIMIT 1";
            $objDb->query ($strSqlQuery);
            //echo $strSqlQuery.'<br>';
        }
    }
}


/* Форма добавления корневой группы шаблонов
    Влад Андерсен, 13:22:24 */

if ($_POST ['frmGroupAdd'] == 'true') {
    $frmName = addslashes(trim($_POST['frmName']));
    if ($frmName) {
        $strSqlQuery = "INSERT INTO site_templates_group (stg_id_project, stg_id_parent, stg_name, stg_status) VALUES (".
            "'".$currentProject."', ".
            "NULL, ".
            "'".$frmName."', ".
            "'Y'".
            ")";
        $objDb->query($strSqlQuery);
    }
}

/* Форма добавления подгруппы страниц
    Влад Андерсен, 13:50:37 */

if ($_POST ['frmSubGroupAdd'] == 'true') {
    $frmName = addslashes(trim($_POST['frmName']));
    if ($frmName) {
        $strSqlQuery = "INSERT INTO site_templates_group (stg_id_project, stg_id_parent, stg_name, stg_status) VALUES (".
            "'".$currentProject."', ".
            $idItem.", ".
            "'".$frmName."', ".
            "'Y'".
            ")";
        $objDb->query($strSqlQuery);
        //echo $strSqlQuery;
    }
}


// ***** Формируем список групп *********************************************
$strSqlQuery = "SELECT * FROM site_templates_group WHERE stg_id_project='".$_SESSION['intIdDefaultProject']."' AND stg_id_parent IS NULL";
$arrLstItem = $objDb->fetchall( $strSqlQuery );

if(is_array($arrLstItem)) {
    foreach($arrLstItem as $key=>$value) {
        $arrItemForTpl[$key] =
            array('intIdItem'=>$arrLstItem[$key]['stg_id'],
                'strNameItem'=>htmlspecialchars($arrLstItem[$key]['stg_name']),
                'strStatusItem'=>($arrLstItem[$key]['stg_status'] == 'Y' ? 'checked' : '')
            );

        /* Для пустых групп выводим кнопку "удалить"
                     Влад Андерсен, 16:51:18 */

        $strSqlQuery = "SELECT COUNT(*) FROM site_templates WHERE st_group = ".$value['stg_id'];
        $thisTempCount = $objDb->fetch( $strSqlQuery , 'COUNT(*)');

        if ($thisTempCount == 0) $arrIf['delItem'.$value['stg_id']] = true;


        $arrParent[$arrLstItem[$key]['stg_id']] = true;
    }
} else { // Если каталог пуст (нет гркпп страниц)
    $arrIf['block.catalogue.empty'] = true;
}

$objTpl->tpl_loop($arrTplVars['module.name'], "lst.item", $arrItemForTpl);

$arrTplVars['intIdItem'] = intval($_GET['idItem']);

if($arrTplVars['intIdItem']>0) { // Если ID выбранного каталога больше 0
    if($arrParent[$arrTplVars['intIdItem']]!=true)
        $inIdParentSubItem = $objDb->fetch( "SELECT stg_id_parent FROM site_templates_group WHERE stg_id_project='".$_SESSION['intIdDefaultProject']."' AND stg_id='".$arrTplVars['intIdItem']."'" , 'stg_id_parent');
    else
        $inIdParentSubItem = $arrTplVars['intIdItem'];

// ***** Выбираем вложенные папки в родительской
    $arrLstSubItem = $objDb->fetchall( "SELECT * FROM site_templates_group WHERE stg_id_project='".$_SESSION['intIdDefaultProject']."' AND stg_id_parent='".$inIdParentSubItem."'" );
    if (is_array($arrLstSubItem)) {
        $arrIf['block.'.$inIdParentSubItem] = true;
        foreach ($arrLstSubItem as $key=>$value) {
            $arrSubItemForTpl[$key] =
                array('intIdSubItem'=>$arrLstSubItem[$key]['stg_id'],
                    'strNameSubItem'=>htmlspecialchars($arrLstSubItem[$key]['stg_name']),
                    'strStatusSubItem'=>($arrLstSubItem[$key]['stg_status'] == 'Y' ? 'checked' : '')
                );

            /* Для пустых подгрупп выводим кнопку "удалить" */
            $strSqlQuery = "SELECT COUNT(*) FROM site_templates WHERE st_group = ".$value['stg_id'];
            $thisTempCount = $objDb->fetch( $strSqlQuery , 'COUNT(*)');

            if ($thisTempCount == 0) $arrIf['delItem'.$value['stg_id']] = true;
        }
    }

    $objTpl->tpl_loop($arrTplVars['module.name'], "lst.subitem.".$inIdParentSubItem, $arrSubItemForTpl);
}
// **************************************************************************************

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
