<?php
$arrTplVars['module.name'] = "users.edit";
$arrTplVars['module.title'] = "АДМИНИСТРАТОРЫ";
$arrTplVars['module.name.parent'] = "users";

$arrTplVars['stPage'] = $stPage = ( intval($_GET['stPage']) > 0 ? $_GET['stPage'] : 1 );

require_once "models/CmsUser.php";
if (!is_object($objCmsUser)) {
    $objCmsUser = new CmsUser();
}

if ( isset($_POST['frmUserEditModulesAccess']) && intval($_POST['intIdUser']) && intval($_POST['intIdPrj']) ) {
    $intIdUser = intval($_POST['intIdUser']);
    $intIdPrj = intval($_POST['intIdPrj']);
    $arrModules = $_POST['arrModules'];

    if ( !$objCmsUser->unsetAllModulesAccess($intIdUser, $intIdPrj) ) {
        trace_log('Не смог удалить доступ к модулям для пользователя '.$intIdUser.' в проекте '.$intIdPrj);
    }

    if (is_array($arrModules) && !empty($arrModules)) {
        foreach ($arrModules as $module => $val) {
            if (intval($module)) {
                $objCmsUser->setModuleAccess($intIdUser, $module, $intIdPrj);
            }
        }
    }

    $_SESSION['manCodeError'][]['code'] = 'msgUserPrjAccessModulesOk';
    $_SESSION['errSuf'] = '_project';
    header('location: '.$arrTplVars['module.name'].'?stPage='.$stPage.'&idManager='.intval($_GET['idManager']));
    exit;
}

/************************************
 ** Shirokovskiy D.2005 Jimmy™. Mon Oct 10 11:57:31 MSD 2005
 **
 ** При сохранении параметров пользователя, используем 1 форму с несколькими кнопками Submit-а
 ** чтобы разделить запись данных по порциям, а не использовать "огромный массив данных".
 **
 ** За каждую порцию данных отвечает своя кнопка сабмита, и соответственно свой кусок обработчика
 ** несмотря на то, что в POST всё равно приходят ВСЕ данные и только одна из нажатых Submit.
 */

if ( isset($_POST['frmUserAddEdit']) && $_POST['frmUserAddEdit'] == 'true' ) {
    if ( isset($_POST['saveUserInfo']) ) {
        # Запись данных о пользователе
        $arrSqlData['strLNameUser'] = addslashes(trim($_POST['strLNameUser']));
        $arrTplVars['strLNameUser'] = htmlspecialchars(stripslashes(trim($_POST['strLNameUser'])));

        $arrSqlData['strFNameUser'] = addslashes(trim($_POST['strFNameUser']));
        $arrTplVars['strFNameUser'] = htmlspecialchars(stripslashes(trim($_POST['strFNameUser'])));

        $arrSqlData['strPNameUser'] = addslashes(trim($_POST['strPNameUser']));
        $arrTplVars['strPNameUser'] = htmlspecialchars(stripslashes(trim($_POST['strPNameUser'])));

        $arrSqlData['strLoginUser'] = addslashes(trim($_POST['strLoginUser']));
        $arrTplVars['strLoginUser'] = htmlspecialchars(stripslashes(trim($_POST['strLoginUser'])));

        $arrSqlData['strPassUser'] = addslashes(trim($_POST['strPassUser']));
        $arrTplVars['strPassUser'] = htmlspecialchars(stripslashes(trim($_POST['strPassUser'])));

        if ( empty($_POST['intIdUser']) && empty($arrSqlData['strPassUser'])) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = '102usr';
        }

        $arrSqlData['strEmailUser'] = addslashes(trim($_POST['strEmailUser']));
        $arrTplVars['strEmailUser'] = htmlspecialchars(stripslashes(trim($_POST['strEmailUser'])));

        $arrSqlData['bolSexUser'] = $_POST['bolSexUser'];
        $arrTplVars['bolSexUser_'.$_POST['bolSexUser']] = ' checked';

        $arrSqlData['chUserStatus'] = ($_POST['chUserStatus']=='on'?'Y':'N');
        $arrTplVars['chUserStatus_ch'] = ($_POST['chUserStatus']=='on'?' checked':'');

        $arrSqlData['intGroupID'] = $arrTplVars['intGroupID'] = intval(trim($_POST['intGroupID']));

        if (empty($arrSqlData['strLNameUser']) || empty($arrSqlData['strFNameUser']) || empty($arrSqlData['strLoginUser']) || empty($arrSqlData['bolSexUser']) ) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = '102usr';
        }

        if ( $GLOBALS['manStatusError']!=1 ) {

            $strSqlFields = ""
                ." cu_group      = '".$arrSqlData['intGroupID']."'"
                .", cu_fname     = '".$arrSqlData['strFNameUser']."'"
                .", cu_lname     = '".$arrSqlData['strLNameUser']."'"
                .", cu_pname     = '".$arrSqlData['strPNameUser']."'"
                .", cu_login     = '".$arrSqlData['strLoginUser']."'"
                . ( !empty($arrSqlData['strPassUser']) ? ", cu_password  = MD5('".$arrSqlData['strPassUser']."')" : "" )
                .", cu_status    = '".$arrSqlData['chUserStatus']."'"
                .", cu_email     = '".$arrSqlData['strEmailUser']."'"
                .", cu_gender    = '".$arrSqlData['bolSexUser']."'"
            ;
            if ( intval($_POST['intIdUser']) > 0 ) {
                $strSqlQuery = "UPDATE cms_users SET $strSqlFields WHERE cu_id = '".$_POST['intIdUser']."'";
            } else {
                $strSqlQuery = "INSERT INTO cms_users SET $strSqlFields";
            }
            if ( !$objDb->query($strSqlQuery) ) {
                $GLOBALS['manStatusError']=1;
                $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
            } else {
                if ( intval($_POST['intIdUser']) > 0 ) {
                    $editedUserID = $_POST['intIdUser'];
                } else {
                    $editedUserID = $objDb->insert_id();
                    if ($editedUserID>0) {
                        $strSqlFields = "SELECT `cug_name` FROM `cms_users_group` WHERE `cug_id` = ".$arrTplVars['intGroupID'];
                        $gr_name = $objDb->fetch( $strSqlFields );
                        if (!is_object($objMail)) { $objMail = new clsMail(); }
                        $debugLogName = "mail.".date('Y.m.d_H').".log";
                        $sendFrom = "info@rabota-ya.ru";
                        $sendTo = $arrSqlData['strEmailUser'];
                        $msgSubject = "Регистрация на сайте";
                        $msgInText = "Добрый день, ".$arrSqlData['strFNameUser']." ".$arrSqlData['strLNameUser'].". Вы зарегестрировались на сайте rabota-ya.ru как ".$gr_name['cug_name'].".\r\n";
                        $msgInText.= "Ваш логин: ".$arrSqlData['strLoginUser']."\r\n";
                        $msgInText.= "Ваш пароль: ".$arrSqlData['strPassUser']."\r\n";
                        $msgInText.= "Для работы с сайтом Вы можете перейти по ссылке http://www.rabota-ya.ru/cms/.\r\n";

                        $msgInHtml = "<p>Добрый день, ".$arrSqlData['strFNameUser']." ".$arrSqlData['strLNameUser'].". </p><p>Вы зарегистрировались на сайте rabota-ya.ru как ".$gr_name['cug_name']." .</p>";
                        $msgInHtml.= "<p>Ваш логин: ".$arrSqlData['strLoginUser']."</p>";
                        $msgInHtml.= "<p>Ваш пароль: ".$arrSqlData['strPassUser']."</p>";
                        $msgInHtml.= "<p>Для работы с сайтом Вы можете перейти по ссылке http://www.rabota-ya.du/cms/.</p>";
                        if (!is_null($sendTo) && $objUtils->checkEmail($sendTo)) {
                            $objMail->new_mail( $sendFrom, $sendTo, $msgSubject, $msgInText, $msgInHtml );
                            if ( $objMail->send() !== true ) {
                                trace_log('Письмо НЕ ОТПРАВЛЕНО по непонятным причинам на '.$sendTo, $debugLogName);
                            }
                            else {
                                trace_log('Письмо отправлено на '.$sendTo, $debugLogName);
                            }
                        }
                        else {
                            trace_log('Email получателя не указан или не верен: '.$sendTo.' |', $debugLogName);
                        }
                    }
                }

                header('location: '.$arrTplVars['module.name']."?idManager=$editedUserID&stPage=".$arrTplVars['stPage']."&errMess=100usr");
                exit();
            }
        }
    }

    if ( isset($_POST['saveUserProjects']) ) {
        // сначала всё выключим
        $strSqlQuery = "UPDATE cms_projects_access SET fpa_status = 'N' WHERE fpa_id_user='".$_POST['intIdUser']."'";
        if ( !$objDb->query($strSqlQuery) ) {
            echo("Error: $strSqlQuery");echo "<br/>\n";echo(__FILE__);echo "<br/>\n";echo(__LINE__);
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
        }

        // и если надо добавим, там где надо
        if ( isset($_POST['arrAccessProject']) && is_array($_POST['arrAccessProject'])) {
            foreach ( $_POST['arrAccessProject'] as $key => $value ) {
                $strSqlQuery = "UPDATE cms_projects_access SET fpa_status = 'Y'"
                    ." WHERE fpa_id_project='".$key."' AND fpa_id_user='".$_POST['intIdUser']."'";
                if ( !$objDb->query($strSqlQuery) ) {
                    echo("Error: $strSqlQuery");echo "<br/>\n";echo(__FILE__);echo "<br/>\n";echo(__LINE__);
                    $GLOBALS['manStatusError']=1;
                    $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
                } else {
                    if ( $objDb->affected() == 0 ) {
                        $strSqlQuery = "INSERT INTO cms_projects_access SET"
                            ." fpa_id_project='".$key."'"
                            .", fpa_id_user='".$_POST['intIdUser']."'"
                            .", fpa_status = 'Y'"
                        ;
                        if ( !$objDb->query($strSqlQuery) ) {
                            echo("Error: $strSqlQuery");echo "<br/>\n";echo(__FILE__);echo "<br/>\n";echo(__LINE__);
                            $GLOBALS['manStatusError']=1;
                            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
                        }
                    }
                }
            }
        }

        $errSuf = '_project';
        $GLOBALS['manCodeError'][1]['code'] = '101usr';
    }

    if ( isset($_POST['saveUserPermissions']) ) {
        $userID = intval($_POST['intIdUser']);
        $arrModulesOn = $_POST['arrModule'];

        if ( !empty( $userID ) ) {
            $strSqlQuery = "UPDATE cms_users_access_modules SET cuam_access='N' WHERE cuam_id_owner='$userID'";
            if ( !$objDb->query($strSqlQuery) ) {
                print_r( $strSqlQuery );
                die(__FILE__."\n".__LINE__."\n");
                $GLOBALS['manStatusError']=1;
                $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
            }

            if ( !empty( $arrModulesOn ) ) {
                foreach ( $arrModulesOn as $key => $value ) {
                    $isAccess = '';
                    $strSqlQuery = "SELECT cuam_access FROM cms_users_access_modules WHERE cuam_id_owner='$userID' AND cuam_id_module='$key'";
                    $isAccess = $objDb->fetch( $strSqlQuery , 0);

                    if ( empty($isAccess) ) {
                        $strSqlQuery = "INSERT INTO cms_users_access_modules SET"
                            ." cuam_type='user'"
                            .", cuam_access='Y'"
                            .", cuam_id_owner='$userID'"
                            .", cuam_id_module='$key'";
                        if ( !$objDb->query($strSqlQuery) ) {
                            print_r( $strSqlQuery );
                            die(__FILE__."\n".__LINE__."\n");
                            $GLOBALS['manStatusError']=1;
                            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
                        }
                    } else {
                        if ( $isAccess == 'N' ) {
                            $strSqlQuery = "UPDATE cms_users_access_modules SET cuam_access='Y' WHERE cuam_id_owner='$userID' AND cuam_id_module='$key'";
                            if ( !$objDb->query($strSqlQuery) ) {
                                print_r( $strSqlQuery );
                                die(__FILE__."\n".__LINE__."\n");
                                $GLOBALS['manStatusError']=1;
                                $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
                            }
                        }
                    }
                }
            }
        }
    }
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

$arrTplVars['intIdEditUser'] = intval($_GET['idManager']);
$strSortField = "cm_id";

/**
 * Загрузка данных
 */
if( $arrTplVars['intIdEditUser']>0 ) {
    $arrIf['access.prj'] = true;
    $arrTplVars['urlReload'] = CMS_URL.$arrTplVars['module.name']."?idManager={$arrTplVars['intIdEditUser']}&stPage={$arrTplVars['stPage']}";

    // Информация о пользователе
    $strSqlQuery = "SELECT * FROM cms_users WHERE cu_id='".$arrTplVars['intIdEditUser']."'";
    $arrInfoEditUser = $objDb->fetch( $strSqlQuery );

    if(!is_array($arrInfoEditUser)) {
        header("location: ".$arrTplVars['module.name.parent']);
        exit;
    }

    $arrTplVars['strEditUserName'] = '['.$arrInfoEditUser['cu_lname'].' '.$arrInfoEditUser['cu_fname'].' '.$arrInfoEditUser['cu_pname'].']';

    $arrTplVars['intIdEditUser'] = $arrInfoEditUser['cu_id'];
    $arrTplVars['strLNameUser'] = htmlspecialchars($arrInfoEditUser['cu_lname']);
    $arrTplVars['strFNameUser'] = htmlspecialchars($arrInfoEditUser['cu_fname']);
    $arrTplVars['strPNameUser'] = htmlspecialchars($arrInfoEditUser['cu_pname']);
    $arrTplVars['strLoginUser'] = htmlspecialchars($arrInfoEditUser['cu_login']);
    //$arrTplVars['strPassUser'] = htmlspecialchars($arrInfoEditUser['cu_password']);
    $arrTplVars['strEmailUser'] = $arrInfoEditUser['cu_email'];
    #$arrTplVars['intUserAccess'] = $arrInfoEditUser['cu_group'];
    $arrTplVars['bolSexUser_'.$arrInfoEditUser['cu_gender']] = ' checked';
    $arrTplVars['chUserStatus'] = ( $arrInfoEditUser['cu_status']=='Y' ? ' checked' : '');

    /**
     * Доступ к модулям
     */
    if ( isset($_GET['sort']) && !empty($_GET['sort']) ) {
        switch ($_GET['sort']) {
            case 'alias':
                $strSortField = "cm_link";
                break;

            case 'name':
                $strSortField = "cm_name";
                break;

            case 'id':
                $strSortField = "cm_id";
                break;

            default:
                $strSortField = "cm_id";
                break;
        }

        $arrTplVars[$_GET['sort'].'_sel'] = " selected";
    }
}



/**
 * Перебираем проекты
 */
$strSqlQuery = "SELECT * FROM cms_projects WHERE fp_status='Y'";
$arrInfoProjects = $objDb->fetchall( $strSqlQuery );

if(is_array($arrInfoProjects)) {
    foreach ($arrInfoProjects as $pkey=>$project) {
        /**
         * BEGIN BLOCK #1: Выбираем доступ пользователя к проекту
         */
        $strSqlQuery = "SELECT * FROM cms_projects_access WHERE fpa_id_project = ".$project['fp_id']." AND fpa_id_user=".$arrTplVars['intIdEditUser']." AND fpa_status='Y'";
        $arrAccessProjectUser = $objDb->fetch( $strSqlQuery );
        if(is_array($arrAccessProjectUser)) {
            $arrFormatAccessProject[$arrAccessProjectUser['fpa_id_project']] = true; // есть доступ

            /**
             * Определим к какому модулю по проектно есть доступ у пользователя
             */
            $strSqlQuery = "SELECT * FROM cms_modules tCM"
                ." LEFT JOIN cms_users_access_modules tCUAM ON (cuam_id_module=cm_id AND cuam_id_owner=".$arrTplVars['intIdEditUser']." AND cuam_type = 'user')"
                ." WHERE cuam_id_project=".$project['fp_id']." AND cuam_access = 'Y'"
                ." ORDER BY $strSortField";
            $arrPrjAcsModules = $objDb->fetchall( $strSqlQuery );
            $arrJsonProjectAccessModules = array();
            if ( !empty( $arrPrjAcsModules ) ) {
                foreach ( $arrPrjAcsModules as $mkey => $module ) {
                    if (!empty($module['cuam_id_module'])) {
                        $arrJsonProjectAccessModules[] = intval($module['cm_id']);
                    }
                }
            }

            if (is_array($arrJsonProjectAccessModules)) {
                $arrInfoProjects[$pkey]['arrProjectModuleAccess'] = json_encode($arrJsonProjectAccessModules);
            }
        }
        /**
         * END BLOCK #1
         */

        $arrInfoProjects[$pkey]['arrAccessProjectChecked'] = ($arrFormatAccessProject[$project['fp_id']]==true) ? ' checked' : '';
    }
}
$objTpl->tpl_loop($arrTplVars['module.name'], "lst.projects", $arrInfoProjects);
$objTpl->tpl_loop($arrTplVars['module.name'], "lst.projects.module.access", $arrInfoProjects);

/**
 * Выведем список модулей
 */
$arrJsonModules = array();
$strSqlQuery = "SELECT * FROM cms_modules ORDER BY ".$strSortField;
$arrModules = $objDb->fetchall( $strSqlQuery );
if (is_array($arrModules) && !empty($arrModules)) {
    foreach ($arrModules as $k => $module) {
        $arrModules[$k]['moduleID'] = $arrJsonModules[] = intval($module['cm_id']);
        $arrModules[$k]['moduleName'] = $module['cm_name'];
        $arrModules[$k]['moduleCssClass'] = ( empty($module['cm_id_parent']) ? ( $module['cm_type'] == "system" ? "tab-r" : "tab-o" ) : "tab-gb" );
        $arrModules[$k]['moduleAlias'] = $module['cm_link'];
    }

    $arrTplVars['arrJsonModules'] = json_encode($arrJsonModules);

    $countOfColumns = 3;
    $rowsInCol = ceil(count($arrModules)/$countOfColumns);

    $arrModules  = array_chunk($arrModules, $rowsInCol, true);

    foreach ( $arrModules as $key => $value ) {
        $arrModules[$key]['keyArr'] = $key;
    }
}
$objTpl->tpl_loop($arrTplVars['module.name'], "list.modules", $arrModules);

if ( is_array( $arrModules ) && !empty( $arrModules ) ) {
    $arrIf['is.modules'] = true;
    foreach ( $arrModules as $key => $value ) {
        unset($arrModules[$key]['keyArr']); // т.к. keyArr не вписывается в структуру массива для LOOP, keyArr был просто впомогательной переменной
        $objTpl->tpl_loop($arrTplVars['module.name'], "list.sub.modules.".$key, $value);
    }
}


/************************************
 ** Shirokovskiy D.2005 Jimmy™. Mon Oct 10 13:36:17 MSD 2005
 **
 ** Выборка групп пользователей
 */
$strSqlQuery = "SELECT * FROM cms_users_group WHERE cug_status='Y'";

$arrUserGroups = $objDb->fetchall( $strSqlQuery );
foreach ( $arrUserGroups as $key => $value ) {
    $arrUserGroups[$key]['intGroupID'] = $value['cug_id'];
    $arrUserGroups[$key]['intGroupID_sel'] = ( $arrInfoEditUser['cu_group']==$value['cug_id'] ? ' selected' : '');
    $arrUserGroups[$key]['strGroupName'] = $value['cug_name'];
}
$objTpl->tpl_loop($arrTplVars['module.name'], "list.user.groups", $arrUserGroups);


/************************************
 ** Shirokovskiy D.2005 Jimmy™. Tue Oct 11 15:19:27 MSD 2005
 **
 ** Выборка модулей для определения доступа по проектам
 */
$strSqlQuery = "SELECT * FROM cms_modules Mls"
    ." LEFT JOIN cms_projects_access aPrj ON aPrj.fpa_id_project = Mls.xxxxxxx"
    ." WHERE ";
/* ДОДЕЛАТЬ */


//*********************************************************
$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);

