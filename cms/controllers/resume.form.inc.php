<?php
include_once "models/JobWorkHistory.php";
include_once "models/JobCategory.php";

$arrTplVars['module.name'] = "resume.form";
$arrTplVars['module.parent'] = "resumes";
$arrTplVars['module.title'] = "Новое резюме";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);
$SiteUser = new SiteUser();

/**
 * Удаление истории работ
 */
if (isset($_GET['jrw_id']) && $_GET['jrw_id'] > 0 && $_GET['act']=='del' && isset($_GET['jr_id']) && $_GET['jr_id'] > 0) {
    $objWorkHistory = new JobWorkHistory($_db_config);
    if ($objWorkHistory->removeHistoryRecord($_GET['jrw_id'], $_GET['jr_id'])) {
        header("Location: {$arrTplVars['module.name']}?stPage={$arrTplVars['stPage']}".'&id='.$_GET['jr_id'].'&errMess=msgDelOk&tab=2');
    } else {
        header("Location: {$arrTplVars['module.name']}?stPage={$arrTplVars['stPage']}".'&id='.$_GET['jr_id'].'&errMess=msgErrorDel');
    }
    exit();
}

/**
 * Запись данных
 */
if ( isset($_POST['frmEdit']) && $_POST['frmEdit'] == 'true' ) {
    $intRecordId = $arrSqlData['intRecordId'] = intval(trim($_POST['intRecordId']));
    $arrTplVars['intRecordId'] = ( $arrSqlData['intRecordId'] > 0 ? $arrSqlData['intRecordId'] : '');

    $arrSqlData['strTitle'] = mysql_real_escape_string( ucfirst(trim($_POST['strTitle'])) );
    $arrTplVars['strTitle'] = htmlspecialchars(stripslashes(trim($_POST['strTitle'])));

    if (empty($arrSqlData['strTitle']) ) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgEmptyTitle';
    }

    $arrSqlData['strFio'] = mysql_real_escape_string( ucfirst(trim($_POST['strFio'])) );
    $arrTplVars['strFio'] = htmlspecialchars(stripslashes(trim($_POST['strFio'])));

    $rbSex = $_POST['rbSex'];
    $arrTplVars['jr_sex_'.$rbSex] = " checked";

    $arrSqlData['strBirthday'] = mysql_real_escape_string(trim($_POST['strBirthday']));
    $arrTplVars['strBirthday'] = htmlspecialchars(stripslashes(trim($_POST['strBirthday'])));

    $arrSqlData['strAge'] = intval(trim($_POST['strAge']));
    $arrTplVars['strAge'] = htmlspecialchars(stripslashes(trim($_POST['strAge'])));

    if ($arrSqlData['strAge'] > 0 && !empty($arrSqlData['strBirthday'])) {
        // количество лет между датами
//        $a = new DateTime($arrSqlData['strBirthday']);
//        $b = new DateTime(date('Y-m-d'));
//        $arrSqlData['strAge'] = $a->diff($b)->format('%y');
        $arrSqlData['strAge'] = $arrTplVars['strAge'] = null;
    }

    $rbMarriage = ($_POST['rbMarriage'] == 'Y' ? 'Y' : 'N');
    $arrTplVars['jr_marriage_'.$rbMarriage] = ' checked';

    $rbChildren = $_POST['rbChildren'];
    $arrTplVars['jr_children_'.$rbChildren] = ' checked';

    $arrSqlData['strSalary'] = intval(trim($_POST['strSalary']));
    $arrTplVars['strSalary'] = htmlspecialchars(stripslashes(trim($_POST['strSalary'])));

    $rbWorkbusy = $arrRecord['jr_work_busy'] = intval($_POST['rbWorkbusy']);

    $arrSqlData['strCity'] = mysql_real_escape_string(trim($_POST['strCity']));
    $arrTplVars['strCity'] = htmlspecialchars(stripslashes(trim($_POST['strCity'])));

    $cityId = $SiteUser->getCityId($arrSqlData['strCity']);

    if (empty($cityId)) {
        $cityId = $SiteUser->saveCity(trim($_POST['strCity']));
    }

    $rbTrips = $_POST['rbTrips'];
    $arrTplVars['jr_trips_'.$rbTrips] = " checked";

    $arrSqlData['strAbout'] = mysql_real_escape_string(trim($_POST['strAbout']));
    $arrTplVars['strAbout'] = htmlspecialchars(stripslashes(trim($_POST['strAbout'])));

    $arrSqlData['strPhone'] = mysql_real_escape_string(trim($_POST['strPhone']));
    $arrTplVars['strPhone'] = htmlspecialchars(stripslashes(trim($_POST['strPhone'])));

    $arrSqlData['strEmail'] = mysql_real_escape_string(trim($_POST['strEmail']));
    $arrTplVars['strEmail'] = htmlspecialchars(stripslashes(trim($_POST['strEmail'])));

    if(!empty($arrSqlData['strEmail']) && !$objUtils->checkEmail($arrSqlData['strEmail'])) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgEmptyEmail';
    }

    $arrSqlData['strSkype'] = mysql_real_escape_string(trim($_POST['strSkype']));
    $arrTplVars['strSkype'] = htmlspecialchars(stripslashes(trim($_POST['strSkype'])));

    $arrSqlData['strIcq'] = mysql_real_escape_string(trim($_POST['strIcq']));
    $arrTplVars['strIcq'] = htmlspecialchars(stripslashes(trim($_POST['strIcq'])));

    $arrSqlData['strLinkedin'] = mysql_real_escape_string(trim($_POST['strLinkedin']));
    $arrTplVars['strLinkedin'] = htmlspecialchars(stripslashes(trim($_POST['strLinkedin'])));

    $rbDriver = $_POST['rbDriver'];
    $arrTplVars['jr_driver_'.$rbDriver] = " checked";

    if (!empty($_POST['strDatePublish'])) {
        $arrSqlData['strDatePublish'] = strtotime(trim($_POST['strDatePublish']));
        $arrTplVars['strDatePublish'] = htmlspecialchars(stripslashes(trim($_POST['strDatePublish'])));
    } else {
        $arrSqlData['strDatePublish'] = time();
        $arrTplVars['strDatePublish'] = date('d.m.Y', $arrSqlData['strDatePublish']);
    }

    $rbEdu = intval($_POST['rbEdu']);

    $arrSqlData['strEduPlus'] = mysql_real_escape_string(trim($_POST['strEduPlus']));
    $arrTplVars['strEduPlus'] = htmlspecialchars(stripslashes(trim($_POST['strEduPlus'])));

    $arrSqlData['strMetaDesc'] = mysql_real_escape_string(trim($_POST['strMetaDesc']));
    $arrTplVars['strMetaDesc'] = htmlspecialchars(stripslashes(trim($_POST['strMetaDesc'])));

    $arrSqlData['strMetaKeywords'] = mysql_real_escape_string(trim($_POST['strMetaKeywords']));
    $arrTplVars['strMetaKeywords'] = htmlspecialchars(stripslashes(trim($_POST['strMetaKeywords'])));

    $arrSqlData['cbxStatus'] = ($_POST['cbxStatus'] == 'on' ? 'Y' : 'N');
    $arrTplVars['cbxStatus'] = ($_POST['cbxStatus'] == 'on' ? ' checked' : '');

    $arrSqlData['cbxReturn'] = ($_POST['cbxReturn'] == 'on' ? 'Y' : 'N');
    $arrTplVars['cbxReturn'] = ($_POST['cbxReturn'] == 'on' ? ' checked' : '');

    if ($GLOBALS['manStatusError']!=1) {
        $strSqlFields = ""
            ." jr_title = '{$arrSqlData['strTitle']}'"
            .", jr_salary = '{$arrSqlData['strSalary']}'"
            .", jr_fio = '{$arrSqlData['strFio']}'"
            . ($arrSqlData['strAge']>0?", jr_age = {$arrSqlData['strAge']}":'')
            . (!empty($arrSqlData['strBirthday']) ? ", jr_birthday = '".date('Y-m-d',strtotime($arrSqlData['strBirthday']))."'" : '' )
            .", jr_sex = '".$rbSex."'"
            .", jr_children = '".$rbChildren."'"
            .", jr_marriage = '".$rbMarriage."'"
            .", jr_city = ".(empty($cityId) && !empty($arrSqlData['strCity']) ? "'".$arrSqlData['strCity']."'" : 'NULL' )
            . (intval($cityId)>0 ? ", jr_city_id = ".$cityId : '' )
            .", jr_driver = '$rbDriver'"
            .", jr_work_busy = '$rbWorkbusy'"
            .", jr_trips = '$rbTrips'"
            .", jr_phone = '{$arrSqlData['strPhone']}'"
            .", jr_email = '{$arrSqlData['strEmail']}'"
            .", jr_skype = '{$arrSqlData['strSkype']}'"
            .", jr_icq = '{$arrSqlData['strIcq']}'"
            .", jr_linkedin = '{$arrSqlData['strLinkedin']}'"
            .", jr_edu_plus = '{$arrSqlData['strEduPlus']}'"
            .", jr_about = '{$arrSqlData['strAbout']}'"
            .", jr_status = '{$arrSqlData['cbxStatus']}'"
            .", jr_meta_description = '{$arrSqlData['strMetaDesc']}'"
            .", jr_meta_keywords = '{$arrSqlData['strMetaKeywords']}'"
        ;
        if($intRecordId > 0) {
            $strSqlQuery = "UPDATE job_resumes SET ".$strSqlFields
            ." WHERE jr_id = ".$intRecordId;
        } else {
            $strSqlQuery = "INSERT INTO job_resumes SET".$strSqlFields
                .", jr_date_publish = ".$arrSqlData['strDatePublish'];
        }

        if ( !$objDb->query( $strSqlQuery ) ) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
        } else {
            if ( empty($intRecordId) ) {
                $arrTplVars['intRecordId'] = $intRecordId = $objDb->insert_id();
            }

            if ($intRecordId > 0) {
                // Записать опыт работы в историю работ
                $arrSqlData['strCompany'] = mysql_real_escape_string(trim($_POST['strCompany']));
                $arrTplVars['strCompany'] = htmlspecialchars(stripslashes(trim($_POST['strCompany'])));

                $arrSqlData['strPosition'] = mysql_real_escape_string(trim($_POST['strPosition']));
                $arrTplVars['strPosition'] = htmlspecialchars(stripslashes(trim($_POST['strPosition'])));

                $arrSqlData['strDateFrom'] = mysql_real_escape_string(trim($_POST['strDateFrom']));
                $arrSqlData['strDateFrom'] = $objUtils->workDate(13, $arrSqlData['strDateFrom']);
                $arrTplVars['strDateFrom'] = htmlspecialchars(stripslashes(trim($_POST['strDateFrom'])));

                $arrSqlData['strDateTo'] = mysql_real_escape_string(trim($_POST['strDateTo']));
                $arrSqlData['strDateTo'] = $objUtils->workDate(13, $arrSqlData['strDateTo']);
                $arrTplVars['strDateTo'] = htmlspecialchars(stripslashes(trim($_POST['strDateTo'])));

                $arrSqlData['strRegion'] = mysql_real_escape_string(trim($_POST['strRegion']));
                $arrTplVars['strRegion'] = htmlspecialchars(stripslashes(trim($_POST['strRegion'])));

                $arrSqlData['strWeb'] = mysql_real_escape_string(trim($_POST['strWeb']));
                $arrTplVars['strWeb'] = htmlspecialchars(stripslashes(trim($_POST['strWeb'])));

                $arrSqlData['strResponsibility'] = mysql_real_escape_string(trim($_POST['strResponsibility']));
                $arrTplVars['strResponsibility'] = htmlspecialchars(stripslashes(trim($_POST['strResponsibility'])));

                $arrSqlData['intCategoryId'] = intval(trim($_POST['intCategoryId']));
                $arrTplVars['intCategoryId'] = ($arrSqlData['intCategoryId'] > 0 ? $arrSqlData['intCategoryId'] : '');

                if (!empty($arrSqlData['strCompany']) && !empty($arrSqlData['strDateFrom']) && !empty($arrSqlData['strPosition'])) {
                    $strSqlFields = " jrw_jr_id = ".$intRecordId
                        .", jrw_jc_id = ".$arrSqlData['intCategoryId']
                        .", jrw_position = '{$arrSqlData['strPosition']}'"
                        .", jrw_company = '{$arrSqlData['strCompany']}'"
                        .", jrw_region = '{$arrSqlData['strRegion']}'"
                        .", jrw_web = '{$arrSqlData['strWeb']}'"
                        . ($objUtils->dateValid($arrSqlData['strDateFrom']) ? ", jrw_date_start = '{$arrSqlData['strDateFrom']}'" : '')
                        . ($objUtils->dateValid($arrSqlData['strDateTo']) ? ", jrw_date_fin = '{$arrSqlData['strDateTo']}'" : '')
                        .", jrw_responsibility = '{$arrSqlData['strResponsibility']}'"
                    ;
                    $strSqlQuery = "INSERT INTO job_resumes_works SET ".$strSqlFields;
                    if ( !$objDb->query( $strSqlQuery ) ) {
                        $GLOBALS['manStatusError']=1;
                        $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
                    }
                }

                /**
                 * Записать образование
                 */
                $intEduId = $arrSqlData['intEduId'] = intval(trim($_POST['intEduId']));
                $arrTplVars['intEduId'] = ($arrSqlData['intEduId'] > 0 ? $arrSqlData['intEduId'] : '');

                $arrSqlData['strInstitution'] = mysql_real_escape_string(trim($_POST['strInstitution']));
                $arrTplVars['strInstitution'] = htmlspecialchars(stripslashes(trim($_POST['strInstitution'])));

                $arrSqlData['strFaculty'] = mysql_real_escape_string(trim($_POST['strFaculty']));
                $arrTplVars['strFaculty'] = htmlspecialchars(stripslashes(trim($_POST['strFaculty'])));

                $arrSqlData['strSpecialization'] = mysql_real_escape_string(trim($_POST['strSpecialization']));
                $arrTplVars['strSpecialization'] = htmlspecialchars(stripslashes(trim($_POST['strSpecialization'])));

                $arrSqlData['strEduDateStart'] = mysql_real_escape_string(trim($_POST['strEduDateStart']));
                $arrTplVars['strEduDateStart'] = htmlspecialchars(stripslashes(trim($_POST['strEduDateStart'])));

                $arrSqlData['strEduDateFin'] = mysql_real_escape_string(trim($_POST['strEduDateFin'])); // обычная дата, не timestamp
                $arrTplVars['strEduDateFin'] = htmlspecialchars(stripslashes(trim($_POST['strEduDateFin'])));

                if (!empty($rbEdu) && !empty($arrSqlData['strInstitution']) && !empty($arrTplVars['strEduDateFin'])) {
                    $strSqlFields = " jre_jr_id = ".$intRecordId
                        .", jre_jet_id = ".$rbEdu
                        .", jre_institution = '{$arrSqlData['strInstitution']}'"
                        .", jre_faculty = '{$arrSqlData['strFaculty']}'"
                        .", jre_spec = '{$arrSqlData['strSpecialization']}'"
                        . ($objUtils->dateValid(date('Y-m-d',strtotime($arrSqlData['strEduDateStart']))) ? ", jre_date_start = '".$objUtils->workDate(13, $arrSqlData['strEduDateStart'])."'" : '')
                        . ($objUtils->dateValid(date('Y-m-d',strtotime($arrSqlData['strEduDateFin']))) ? ", jre_date_fin = '".$objUtils->workDate(13, $arrSqlData['strEduDateFin'])."'" : '')
                    ;
                    if($intEduId > 0) {
                        $strSqlQuery = "UPDATE job_resumes_education SET ".$strSqlFields." WHERE jre_id = ".$intEduId;
                    } else {
                        $strSqlQuery = "INSERT INTO job_resumes_education SET ".$strSqlFields;
                    }
                    if ( !$objDb->query( $strSqlQuery ) ) {
                        $GLOBALS['manStatusError']=1;
                        $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
                    }
                }
            }
        }
    }

    if ( $GLOBALS['manStatusError']!=1 ) {
        if ( $arrSqlData['cbxReturn'] == 'Y' ) {
            header('location:'.$arrTplVars['module.name'].'?errMess=msgOk&stPage='.$arrTplVars['stPage'].'&id='.$intRecordId);
        } else {
            header('location:'.$arrTplVars['module.parent'].'?errMess=msgOk&stPage='.$arrTplVars['stPage']);
        }
        exit;
    }
}

// ***** Обработка ошибок для вывода *******************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// *****************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if($_GET['id'] > 0) {
    $arrTplVars['module.title'] = "Редактировать резюме";
    $intRecordId = $arrTplVars['intRecordId'] = intval($_GET['id']);

    $arr_string_fields = array('strTitle'=>'jr_title', 'strFio'=>'jr_fio', 'strSalaryOld'=>'jr_salary', 'strLinkedin'=>'jr_linkedin', 'strSkype'=>'jr_skype', 'strPhone'=>'jr_phone', 'strCity'=>'jr_city', 'strAbout'=>'jr_about', 'strEmail'=>'jr_email', 'strEduPlus'=>'jr_edu_plus', 'strMetaDesc'=>'jr_meta_description', 'strMetaKeywords'=>'jr_meta_keywords');
    $arr_checked_fields = array('jr_sex', 'jr_marriage', 'jr_children', 'jr_trips', 'jr_driver');
    $arr_int_fields = array('strSalary'=>'jr_salary', 'strIcq'=>'jr_icq', 'strAge'=>'jr_age');

    $strSqlQuery = "SELECT * FROM `job_resumes` WHERE `jr_id` = ".$intRecordId;
    $arrRecord = $objDb->fetch($strSqlQuery);

    if (is_array($arrRecord) && !empty($arrRecord)) {
        foreach($arr_string_fields as $key => $field) {
            $arrRecord[$key] = htmlspecialchars($arrRecord[$field]);
        }

        foreach($arr_checked_fields as $field) {
            $arrRecord[$field.'_'.$arrRecord[$field]] = ' checked';
        }

        foreach($arr_int_fields as $key => $field) {
            $arrRecord[$key] = (intval($arrRecord[$field]) ? $arrRecord[$field] : '');
        }

        if (!empty($arrRecord['jr_birthday']) && $objUtils->dateValid($arrRecord['jr_birthday'])) {
            $arrRecord['strBirthday'] = date('d.m.Y', strtotime($arrRecord['jr_birthday']));
        } else {
            $arrRecord['strBirthday'] = '';
        }

        $arrRecord['strSalaryOld'] = ($arrRecord['strSalaryOld']!=$arrRecord['strSalary']?$arrRecord['strSalaryOld']:'');
        $arrRecord['strDatePublish'] = date('d.m.Y', $arrRecord['jr_date_publish']);
        $arrRecord['cbxStatus'] = $arrRecord['jr_status']=='Y'?' checked':'';
        $arrCity = $SiteUser->getCityById($arrRecord['jr_city_id']);
        if (is_array($arrCity) && !empty($arrCity)) {
            $arrRecord['strCity'] = $arrCity['city'];
        }
        if (!empty($arrRecord['strMetaDesc'])) {
            if (!preg_match('/[работа от а до я]/i', $arrRecord['strMetaDesc'])) {
                $arrRecord['strMetaDesc'] = "Газета Работа от А до Я, резюме - ".(preg_match('/'.$arrRecord['strTitle'].'/i', $arrRecord['strMetaDesc'])?'':$arrRecord['strTitle']).'. '.$arrRecord['strMetaDesc'];
            }
        } else {
            $arrRecord['strMetaDesc'] = "Газета Работа от А до Я, резюме - ".$arrRecord['strTitle'];
        }

        if (!empty($arrRecord['strMetaKeywords'])) {
            foreach ($arrKeyWords as $k => $val) {
                if (!preg_match('/['.$val.']/i', $arrRecord['strMetaKeywords'])) {
                    $arrRecord['strMetaKeywords'] .= ",".$val;
                }
            }
        } else {
            $arrRecord['strMetaKeywords'] = $arrRecord['strTitle'].','.implode(',', $arrKeyWords);
        }

        $objCategory = new JobCategory($_db_config);

        /**
         * Выборка о работе
         */
        $strSqlQuery = "SELECT * FROM `job_resumes_works` WHERE `jrw_jr_id` = ".$intRecordId;
        $arrWorksHistory = $objDb->fetchall($strSqlQuery);
        if (is_array($arrWorksHistory) && !empty($arrWorksHistory)) {
            foreach ($arrWorksHistory as $k => $arr) {
                $arrWorksHistory[$k]['strHistoryCompany'] = (!empty($arr['jrw_company']) ? htmlspecialchars($arr['jrw_company']) : '&mdash;' );
                $arrWorksHistory[$k]['strHistoryCategory'] = $objCategory->getCategoryFieldById($arr['jrw_jc_id'], 'jc_title');
                $arrWorksHistory[$k]['strHistoryPosition'] = (!empty($arr['jrw_position']) ? htmlspecialchars($arr['jrw_position']) : '&mdash;' );
                $arrWorksHistory[$k]['strHistoryRegion'] = (!empty($arr['jrw_region']) ? htmlspecialchars($arr['jrw_region']) : '&mdash;' );
                $arrWorksHistory[$k]['strHistoryWeb'] = '&mdash;';
                if (!empty($arr['jrw_web'])) {
                    $arr['jrw_web'] = str_replace('http://', '', $arr['jrw_web']);
                    $arr['jrw_web'] = str_replace('https://', '', $arr['jrw_web']);
                    $arrWorksHistory[$k]['strHistoryWeb'] = '<a href="http://'.$arr['jrw_web'].'" target=_blank>'. $arr['jrw_web'] .'</a>';
                }
                $arrWorksHistory[$k]['strHistoryResponsibilities'] = (!empty($arr['jrw_responsibility']) ? htmlspecialchars($arr['jrw_responsibility']) : '&mdash;' );
                $arrWorksHistory[$k]['strHistoryDateFrom'] = $objUtils->workDate(5, $arr['jrw_date_start']);
                $arrWorksHistory[$k]['strHistoryDateTo'] = ($objUtils->dateValid($arr['jrw_date_fin']) ? $objUtils->workDate(5, $arr['jrw_date_fin']) : '&mdash;' );
            }
        }


        /**
         * Выборка по образованию
         */
        $arr_string_fields = array('strInstitution'=>'jre_institution', 'strFaculty'=>'jre_faculty', 'strSpecialization'=>'jre_spec');
        $strSqlQuery = "SELECT * FROM `job_resumes_education` WHERE `jre_jr_id` = ".$intRecordId;
        $arrEdu = $objDb->fetch($strSqlQuery);
        if (is_array($arrEdu) && !empty($arrEdu)) {
            foreach($arr_string_fields as $key => $field) {
                $arrRecord[$key] = htmlspecialchars($arrEdu[$field]);
            }

            if (!empty($arrEdu['jre_date_start']) && $objUtils->dateValid($arrEdu['jre_date_start'])) {
                $arrRecord['strEduDateStart'] = date('d.m.Y', strtotime($arrEdu['jre_date_start']));
            } else {
                $arrRecord['strEduDateStart'] = '';
            }

            if (!empty($arrEdu['jre_date_fin']) && $objUtils->dateValid($arrEdu['jre_date_fin'])) {
                $arrRecord['strEduDateFin'] = date('d.m.Y', strtotime($arrEdu['jre_date_fin']));
            } else {
                $arrRecord['strEduDateFin'] = '';
            }
        }
        $arrRecord['intEduId'] = (isset($arrEdu['jre_id']) ? intval($arrEdu['jre_id']) : '');
    }
}
$arrRecord['strCity'] = (!empty($arrRecord['strCity'])?$arrRecord['strCity']:'Санкт-Петербург');
if (!isset($arrRecord['jr_trips_Y']) && !isset($arrRecord['jr_trips_S'])) {
    $arrRecord['jr_trips_N'] = ' checked';
}
if (!isset($arrRecord['jr_driver_Y'])) {
    $arrRecord['jr_driver_N'] = ' checked';
}
if (!isset($arrRecord['strDatePublish'])) {
    $arrRecord['strDatePublish'] = date('d.m.Y');
}
$objTpl->tpl_array($arrTplVars['module.name'], $arrRecord);
$objTpl->tpl_loop($arrTplVars['module.name'], "resume.history", $arrWorksHistory);

// Тип занятости
$strSqlQuery = "SELECT * FROM `job_workbusy` ORDER BY `jw_id`";
$arrWorkbusyTypes = $objDb->fetchall($strSqlQuery);
if (is_array($arrWorkbusyTypes) && !empty($arrWorkbusyTypes)) {
    foreach ($arrWorkbusyTypes as $k => $val) {
        $arrWorkbusyTypes[$k]['checked'] = (isset($arrRecord['jr_work_busy']) && $val['jw_id']==$arrRecord['jr_work_busy'] ? ' checked' : '');
    }
}
$objTpl->tpl_loop($arrTplVars['module.name'], "workbusy.types", $arrWorkbusyTypes);

// Образование
$strSqlQuery = "SELECT * FROM `job_education_types` ORDER BY jet_id";
$arrEduTypes = $objDb->fetchall($strSqlQuery);
if (is_array($arrEduTypes) && !empty($arrEduTypes)) {
    foreach ($arrEduTypes as $k => $val) {
        $arrEduTypes[$k]['checked'] = (isset($arrEdu['jre_jet_id']) && $val['jet_id']==$arrEdu['jre_jet_id'] ? ' checked' : '');
    }
}
$objTpl->tpl_loop($arrTplVars['module.name'], "education.types", $arrEduTypes);

// Категории - Сфера деятельности
$strSqlQuery = "SELECT * FROM `job_categories` WHERE `jc_parent` IS NULL ORDER BY `jc_order`";
$arrCats = $objDb->fetchall($strSqlQuery);
if (is_array($arrCats) && !empty($arrCats)) {
    foreach ($arrCats as $k => $arr) {
        $arrCats[$k]['strOptGroupName'] = htmlspecialchars($arr['jc_title']);
        $arrCats[$k]['intGroupID'] = $arr['jc_id'];
    }
}
$objTpl->tpl_loop($arrTplVars['module.name'], "option.group.category", $arrCats);
if (is_array($arrCats) && !empty($arrCats)) {
    foreach ($arrCats as $k => $arr) {
        $strSqlQuery = "SELECT * FROM `job_categories` WHERE `jc_parent` = ".$arr['jc_id']." ORDER BY `jc_order`";
        $arrSubCats = $objDb->fetchall($strSqlQuery);

        if (is_array($arrSubCats) && !empty($arrSubCats)) {
            foreach ($arrSubCats as $kk => $varr) {
                $arrSubCats[$kk]['intOptionValue'] = intval($varr['jc_id']);
                $arrSubCats[$kk]['strOptionTitle'] = htmlspecialchars($varr['jc_title']);
            }
        }

        $objTpl->tpl_loop($arrTplVars['module.name'], "group.options.".$arr['jc_id'], $arrSubCats);
    }
}

// Города
$strSqlQuery = "SELECT `city` FROM `site_cities` ORDER BY `pos`, `id`";
$arrCities = $objDb->fetchcol($strSqlQuery);
foreach($arrCities as $key => $value) {
    $arrCities[$key] = htmlentities($value, ENT_QUOTES);
}
$arrTplVars['strCities'] = "'".implode("','", $arrCities)."'";// Санкт-Петербург','Москва','Лабытнанги','Салехард'";

$arrIf['no.image'] = !$arrIf['exist.image'];

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);

