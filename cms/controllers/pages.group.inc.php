<?php
$arrTplVars['module.name'] = "pages.group";
$arrTplVars['module.title'] = "ГРУППЫ СТРАНИЦ";
$arrTplVars['module.parent'] = "pages";

$currentProject = $_SESSION['intIdDefaultProject'];

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");


/* Удаление группы
    Влад Андерсен, 16:09:14 */
if (intval($_GET ['delItem'])) {
  $delItem = intval($_GET ['delItem']);

  // Ещё раз проверить, чтобы группа точно была пустой.

  $strSqlQuery = "SELECT COUNT(*) FROM site_pages WHERE sp_id_group = ".$delItem;
  $thisTempCount = $objDb->fetch( $strSqlQuery , 'COUNT(*)');
  $thisTempParent = $objDb->fetch( "SELECT spg_id_parent FROM site_pages_group WHERE spg_id = ".$delItem , 'spg_id_parent');


  if ($thisTempCount == 0) {
    $strSqlQuery = "DELETE FROM site_pages_group WHERE spg_id = ".$delItem.' LIMIT 1';
    $objDb->query ($strSqlQuery);
    header ("location: ".$arrTplVars['module.name']."".($thisTempParent ? "?idItem=$thisTempParent" : ''));
  }

}



/* Смотрим, добавлять ли чайлд-группу
    Влад Андерсен, 13:39:33 */

if (intval($_GET ['idItem'])) {
  $idItem = intval($_GET ['idItem']);
  $strSqlQuery = "SELECT * FROM site_pages_group WHERE spg_id = '".$idItem."'";
  $arrSqlQuery = $objDb->fetch( $strSqlQuery );

//  print_r ($arrSqlQuery);

  if (!$arrSqlQuery['spg_id_parent']) {
    $arrTplVars['currentParentGroup'] = $arrSqlQuery['spg_name'];
    $arrIf['addChild'] = true;
  }

}



/* Апдейтим имена и статусы групп страниц
    Влад Андерсен, 12:51:25 */

if ($_POST ['frmGroupEdit'] == true) {
  foreach ($_POST as $key => $value ) { // 12:52:49
      if (substr ($key, 0, 4) == 'name') {
        $nameInt = intval(substr ($key, 4));
        $tmpName = addslashes((trim($value)));
          $strSqlQuery = "UPDATE site_pages_group SET spg_name = '".$tmpName."', spg_status = '".($_POST['status'.$nameInt] ? 'Y' : 'N')."' WHERE spg_id = '".$nameInt."' LIMIT 1";
          $objDb->query ($strSqlQuery);
          //echo $strSqlQuery.'<br>';
      }
  }
}


/* Форма добавления корневой группы страниц
    Влад Андерсен, 13:22:24 */

if ($_POST['frmGroupAdd'] == 'true') {
  $frmName = addslashes(trim($_POST['frmName']));
  if ($frmName) {
    $strSqlQuery = "INSERT INTO site_pages_group SET"
      ." spg_id_project    = '".$currentProject."'"
      .", spg_name         = '".$frmName."'"
      .", spg_status       = 'Y'";
    $objDb->query($strSqlQuery);
  }
}

/* Форма добавления подгруппы страниц
    Влад Андерсен, 13:50:37 */

if ($_POST ['frmSubGroupAdd'] == 'true') {
  $frmName = addslashes(trim($_POST['frmName']));
  if ($frmName) {
    $strSqlQuery = "INSERT INTO site_pages_group (spg_id_project, spg_id_parent, spg_name, spg_status) VALUES (".
      "'".$currentProject."', ".
      $idItem.", ".
      "'".$frmName."', ".
      "'Y'".
    ")";
    $objDb->query($strSqlQuery);
    //echo $strSqlQuery;
  }
}


// ***** Кол-во страниц
$strSqlQuery = "SELECT COUNT(*) AS strQuantPages FROM site_pages WHERE sp_id_project='".$_SESSION['intIdDefaultProject']."'";
$arrTplVars['strQuantPages'] = $objDb->fetch( $strSqlQuery , 'strQuantPages');

$strSqlQuery = "SELECT COUNT(*) AS strActiveQuantPages FROM site_pages WHERE sp_id_project='".$_SESSION['intIdDefaultProject']."' AND sp_status='Y'";
$arrTplVars['strActiveQuantPages'] = $objDb->fetch( $strSqlQuery , 'strActiveQuantPages');

$arrTplVars['strActiveNoQuantPages'] = $arrTplVars['strQuantPages']-$arrTplVars['strActiveQuantPages'];

// ***** Формируем список групп *********************************************
$strSqlQuery = "SELECT * FROM site_pages_group WHERE spg_id_project='".$_SESSION['intIdDefaultProject']."' AND spg_id_parent IS NULL";
$arrLstItem = $objDb->fetchall( $strSqlQuery );

if(is_array($arrLstItem)) {
  foreach($arrLstItem as $key=>$value) {
    $arrItemForTpl[$key] =
          array('intIdItem'=>$arrLstItem[$key]['spg_id'],
                'strNameItem'=>htmlspecialchars($arrLstItem[$key]['spg_name']),
                'strStatusItem'=>($arrLstItem[$key]['spg_status'] == 'Y' ? 'checked' : '')
                );
    $arrParent[$arrLstItem[$key]['spg_id']] = true;

    	  /* Для пустых групп выводим кнопку "удалить"
	               Влад Андерсен, 16:51:18 */

    $strSqlQuery = "SELECT COUNT(*) AS iCount FROM site_pages WHERE sp_id_group = ".$value['spg_id'];
    $thisTempCount = $objDb->fetch( $strSqlQuery , 'iCount');
    if ($thisTempCount == 0) $arrIf['delItem'.$value['spg_id']] = true;

  }
} else { // Если каталог пуст (нет гркпп страниц)
  $arrIf['block.catalogue.empty'] = true;
}

$objTpl->tpl_loop($arrTplVars['module.name'], "lst.item", $arrItemForTpl);

$arrTplVars['intIdItem'] = intval($_GET['idItem']);

if($arrTplVars['intIdItem']>0) { // Если ID выбранного каталога больше 0
  if($arrParent[$arrTplVars['intIdItem']]!=true)
    $inIdParentSubItem = $objDb->fetch( "SELECT spg_id_parent FROM site_pages_group WHERE spg_id_project='".$_SESSION['intIdDefaultProject']."' AND spg_id='".$arrTplVars['intIdItem']."' AND spg_status='Y'" , 'spg_id_parent');
  else
    $inIdParentSubItem = $arrTplVars['intIdItem'];

// ***** Выбираем вложенные папки в родительской
  $arrLstSubItem = $objDb->fetchall( "SELECT * FROM site_pages_group WHERE spg_id_project='".$_SESSION['intIdDefaultProject']."' AND spg_id_parent='".$inIdParentSubItem."'" );

  if (is_array($arrLstSubItem)) {
    $arrIf['block.'.$inIdParentSubItem] = true;
  	foreach ($arrLstSubItem as $key=>$value) {
	    $arrSubItemForTpl[$key] =
	           array('intIdSubItem'=>$arrLstSubItem[$key]['spg_id'],
	           'strNameSubItem'=>htmlspecialchars($arrLstSubItem[$key]['spg_name']),
	           'strStatusSubItem'=>($arrLstSubItem[$key]['spg_status'] == 'Y' ? 'checked' : '')
	           );

	           	  /* Для пустых подгрупп выводим кнопку "удалить"
	               Влад Андерсен, 16:51:18 */

    $strSqlQuery = "SELECT COUNT(*) FROM site_pages WHERE sp_id_group = ".$value['spg_id'];
    $thisTempCount = $objDb->fetch( $strSqlQuery , 'COUNT(*)');
    if ($thisTempCount == 0) $arrIf['delItem'.$value['spg_id']] = true;


  	}
	}



	$objTpl->tpl_loop($arrTplVars['module.name'], "lst.subitem.".$inIdParentSubItem, $arrSubItemForTpl);

}

// **********************************************************************

//$objTpl->strip_loops($arrTplVars['module.name']);
$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
