<?php
$arrTplVars['module.name'] = "module.access";
$arrTplVars['module.title'] = "Доступ к модулю";

if ( isset($_POST['frmEditForm']) && $_POST['frmEditForm'] == 'true' ) {
    $intRecordId = $arrSqlData['intRecordId'] = intval(trim($_POST['intRecordId']));
    $arrTplVars['intRecordId'] = ( $arrSqlData['intRecordId'] > 0 ? $arrSqlData['intRecordId'] : '');

    $arrSqlData['intModuleAccess'] = intval(trim($_POST['intModuleAccess']));
    $arrTplVars['intModuleAccess'] = ( $arrSqlData['intModuleAccess'] > 0 ? $arrSqlData['intModuleAccess'] : '');

    $arrSqlData['intGroupID'] = intval(trim($_POST['intGroupID']));
    $arrTplVars['intGroupID'] = ( $arrSqlData['intGroupID'] > 0 ? $arrSqlData['intGroupID'] : '');

    if ( !empty($arrSqlData['intModuleAccess']) && !empty($intRecordId) ) {

        if ($arrSqlData['intModuleAccess'] == 1) {
            // установка доступа всем активным пользователям
            $strSqlQuery = "SELECT cu_id FROM cms_users WHERE cu_status = 'Y'";

        } elseif ($arrSqlData['intModuleAccess'] == 2) {
            // установка доступа группе
            $strSqlQuery = "SELECT cu_id FROM cms_users WHERE cu_status = 'Y' AND cu_group = '{$arrSqlData['intGroupID']}'";
        } else {
            die("<hr>\n".'In file:'.__FILE__.' at line:'.__LINE__."<hr>\n");
        }

        $arrExistActiveUsers = $objDb->fetchall( $strSqlQuery );

        if (!empty($arrExistActiveUsers)) {
            foreach ($arrExistActiveUsers as $kUser => $vUser) {
                // проверим, нет ли у данного пользователя УЖЕ доступа к данному модулю?
                $strSqlQuery = "SELECT * FROM cms_users_access_modules"
                    ." WHERE cuam_access='Y' AND cuam_id_owner='{$vUser['cu_id']}' AND cuam_id_module = '$intRecordId'";
                $arrModuleAccessInfo = $objDb->fetch( $strSqlQuery );

                if ( !empty($arrModuleAccessInfo) ) {
                    if ($arrModuleAccessInfo['cuam_access'] == 'N') {
                        // возобновим доступ
                        $strSqlQuery = "UPDATE cms_users_access_modules SET"
                            ." cuam_access = 'Y'"
                            ." WHERE cuam_id_owner = '{$vUser['cu_id']}' AND cuam_id_module = '$intRecordId'"
                        ;
                        if ( !$objDb->query( $strSqlQuery ) ) {
                            # sql error
                            $GLOBALS['manStatusError']=1;
                            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
                            die("<hr>\n".'In file:'.__FILE__.' at line:'.__LINE__."<hr>\n");
                        }
                    }
                } else {
                    // добавим доступ
                    $strSqlQuery = "INSERT INTO cms_users_access_modules SET"
                        ." cuam_type = 'user'"
                        .", cuam_id_owner = '{$vUser['cu_id']}'"
                        .", cuam_id_module = '$intRecordId'"
                        .", cuam_access = 'Y'"
                    ;
                    if ( !$objDb->query( $strSqlQuery ) ) {
                        # sql error
                        $GLOBALS['manStatusError']=1;
                        $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
                        die("<hr>\n".'In file:'.__FILE__.' at line:'.__LINE__."<hr>\n");
                    }
                }
            }
        }
    } else {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = '101mdl';
    }

    if ($GLOBALS['manStatusError']!=1) {
        $GLOBALS['manCodeError'][]['code'] = '100mdl';
    }
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if (isset($_GET['moduleID']) && intval($_GET['moduleID']) > 0) {
    $intRecordId = $arrTplVars['intRecordId'] = intval($_GET['moduleID']);

    $strSqlQuery = "SELECT cm_link FROM cms_modules"
        ." WHERE cm_id=$intRecordId";
    $arrTplVars['strModuleName'] = $objDb->fetch( $strSqlQuery , "cm_link");

    $strSqlQuery = "SELECT cuam_id_owner uid, cu_group gid, cug_name ug, CONCAT( cu_lname, ' ', cu_fname) fio FROM `cms_users_access_modules`"
        ." LEFT JOIN `cms_users` ON (cu_id = cuam_id_owner)"
        ." LEFT JOIN `cms_users_group` ON (cu_group = cug_id)"
        ." WHERE cuam_id_module=$intRecordId AND cug_status = 'Y'";
    $arrCmsUsersHaveAccess = $objDb->fetchall( $strSqlQuery );
    if (is_array($arrCmsUsersHaveAccess) && !empty($arrCmsUsersHaveAccess)) {
        foreach ($arrCmsUsersHaveAccess as $key => $value) {
            #

        }
    }
}
$objTpl->tpl_loop($arrTplVars['module.name'], "list.user.access", $arrCmsUsersHaveAccess);

/************************************
 ** Shirokovskiy D.2006 Jimmy™.
 **
 ** Выборка групп пользователей
 */
$strSqlQuery = "SELECT * FROM cms_users_group WHERE cug_status='Y'";
$arrUserGroups = $objDb->fetchall( $strSqlQuery );

foreach ( $arrUserGroups as $key => $value ) {
    $arrUserGroups[$key]['intGroupID'] = $value['cug_id'];
    #$arrUserGroups[$key]['intGroupID_sel'] = ( $xxxxxxxxxxxx['cu_group']==$value['cug_id'] ? ' selected' : '');
    $arrUserGroups[$key]['strGroupName'] = $value['cug_name'];
}
$objTpl->tpl_loop($arrTplVars['module.name'], "list.user.groups", $arrUserGroups);

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
