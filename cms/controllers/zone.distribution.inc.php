<?php
$arrTplVars['module.name'] = "zone.distribution";
$arrTplVars['module.title'] = "Зона распространения";

$strImageName = 'distribution-full';
$ext = null;
$intWidth = 2000; // Необходимая ширина картинок


/**
 * Запись файла
 */
if ( isset($_POST['frmEditForm']) && $_POST['frmEditForm'] == 'true' ) {
    if ( $_FILES['flPhoto']['error'] == 0 && $_FILES['flPhoto']['size'] <= 2000000 ) {
        /////////////////////////////////////////////////////////////////////////////////////
        // extended
        $arrExts['pjpeg'] = 'jpg';
        $arrExts['jpeg'] = 'jpg';

        $mimeType = explode("/", $_FILES['flPhoto']['type']);
        $mimeType = $mimeType[1];

        if ( array_key_exists($mimeType , $arrExts) ) {
            $mimeType = $arrExts[$mimeType];
        }

        if ( $mimeType != 'jpg' && $mimeType != 'png' ) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
        } else {
            $fileName = "$strImageName.$mimeType";
            $fileNameOrig = "temp.$strImageName.$mimeType";

            $dirImages = PRJ_IMAGES.'zone';
            if ( !file_exists($dirImages) ) {
                mkdir($dirImages, 0775, true);
            }
        }

        if ($GLOBALS['manStatusError']!=1) {
            if (file_exists($dirImages."/".$fileName) && is_uploaded_file($_FILES['flPhoto']['tmp_name'])) {
                unlink($dirImages."/".$fileName);
            }

            if (!file_exists($dirImages."/".$fileName)) {
                if ( move_uploaded_file( $_FILES['flPhoto']['tmp_name'], $dirImages."/".$fileNameOrig ) ) {
                    chmod($dirImages."/".$fileNameOrig, 0775);
                }

                if ( !is_object($objImage) ) {
                    include_once("cls.images.php");
                    $objImage = new clsImages();
                }

                $arrPreloadSize = getimagesize($dirImages."/".$fileNameOrig);

                if ($arrPreloadSize[0]<=$intWidth) { // картинка меньше по ширине? ничего с ней не делаем, просто переименуем оригинал
                    rename($dirImages."/".$fileNameOrig,  $dirImages."/".$fileName);
                } else {
                    if ( $intHeight > 0 ) {
                        $objImage->resizeImage($dirImages."/".$fileNameOrig, $dirImages."/".$fileName, $intWidth, $intHeight );
                    } else {
                        $objImage->resizeImage($dirImages."/".$fileNameOrig, $dirImages."/".$fileName, $intWidth );
                    }
                    unlink($dirImages."/".$fileNameOrig);
                }
            } else {
                $GLOBALS['manStatusError']=1;
                $GLOBALS['manCodeError'][]['code'] = 'msgFileAlreadyExist';
            }
        }
        /////////////////////////////////////////////////////////////////////////////////////
    } elseif ($_FILES['flPhoto']['size'] > 2000000) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgTooBigFile';
    }

    if ( $GLOBALS['manStatusError']!=1 ) {
        header('location:'.$arrTplVars['module.name']."?errMess=msgOk");
        exit;
    }
}



// ***** Обработка ошибок для вывода *******************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// *****************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if (file_exists(PRJ_IMAGES.'zone/'.$strImageName.'.jpg')) {
    $ext = 'jpg';
} elseif (file_exists(PRJ_IMAGES.'zone/'.$strImageName.'.png')) {
    $ext = 'png';
}

if (!empty($ext)) {
    // File exist
    $arrIf['is.image'] = true;
    $arrTplVars['strFileName'] = $strImageName.'.'.$ext.'?q='.rand(1,10000);
}

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);

