<?php
$arrTplVars['module.name'] = "trainings";
$arrTplVars['module.child'] = "training.form";
$arrTplVars['module.title'] = "Тренинги";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

/**
 * Удаление записи
 */
if ( intval($_GET['id']) > 0 && $_GET['act'] == 'del' ) {
    $intRecordId = intval($_GET['id']);
    $strSqlQuery = "DELETE FROM site_trainings WHERE str_id = ".$intRecordId;
    if ( !$objDb->query($strSqlQuery) ) {
        #mysql error
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
    } else {
        header("Location: {$arrTplVars['module.name']}?stPage={$arrTplVars['stPage']}");
        exit();
    }
}

// ***** Обработка ошибок для вывода *******************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// *****************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

/**
 * Подготовка сессионного поиска
 */
if ( isset($_POST['frmSearch']) ) {
    $_SESSION['frmSearch'] = $_POST;
}

if ( !isset($_POST['frmSearch']) && intval($_GET['stPage']) <= 0 ) {
    unset($_SESSION['frmSearch']);
}

if ( isset($_SESSION['frmSearch']) ) {
    if ( intval( $_SESSION['frmSearch']['intID2Find'] )>0 ) {
        $intFindID = $arrTplVars['intID2Find'] = $_SESSION['frmSearch']['intID2Find'];
        $arrSqlWhere[] = " str_id = ".$intFindID;
    }
    if ( !empty( $_SESSION['frmSearch']['strWord2Find'] ) ) {
        $strFindName = $arrTplVars['strWord2Find'] = mysql_real_escape_string( $_SESSION['frmSearch']['strWord2Find'] );
        $arrSqlWhere[] = " str_title LIKE '%$strFindName%' ";
    }
}

if ( is_array($arrSqlWhere) && !empty($arrSqlWhere) ) {
    $strSqlWhere = implode(' AND ', $arrSqlWhere);
}

// Всего записей в базе
$strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM site_trainings";
$arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');

$strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM site_trainings".( !empty( $strSqlWhere ) ? " WHERE ".$strSqlWhere : '' );
$arrTplVars['intQuantitySelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');

// ***** BEGIN: Построение пейджинга для вывода списка
$objPagination = new tplPaginator($arrTplVars['intQuantitySelectRecords'], SITE_TPL_TPL_DIR);
/**
 * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
 * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
 */
$objPagination->strPaginatorTpl = "site.global.1.tpl";
$objPagination->intQuantRecordPage = 15;
$objPagination->strColorLinkStyle = "link-b";
$objPagination->strColorActiveStyle = "tab-bl-b";
$objPagination->strNameImageBack = "arrow_one_left.gif";
$objPagination->strNameImageForward = "arrow_one_right.gif";
// Создаем блок пэйджинга
$objPagination->paCreate();
// ***** END: Построение пейджинга для вывода списка
// Запрос для выборки нужных записей
$strSqlQuery = "SELECT * FROM site_trainings".( !empty( $strSqlWhere ) ? " WHERE ".$strSqlWhere : '' )." ORDER BY str_id DESC ".$objPagination->strSqlLimit;
$arrTrainings = $objDb->fetchall( $strSqlQuery );
// кол-во публикаций показанных на странице
$arrTplVars['intQuantityShowRecOnPage'] = ( !empty($arrTrainings) ? count($arrTrainings) : 0 );
// Присвоение значения пэйджинга для списка
$arrTplVars['blockPaginator'] = $objPagination->paShow();

if ( is_array($arrTrainings) ) {
    foreach ( $arrTrainings as $key => $value ) {
        $arrTrainings[$key]['cbxStatus'] = $value['str_status'] == 'Y' ? ' checked' : '';
    }
}
$objTpl->tpl_loop($arrTplVars['module.name'], "trainings", $arrTrainings);

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);

