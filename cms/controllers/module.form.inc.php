<?php
$arrTplVars['module.name'] = "module.form";
$arrTplVars['module.access'] = "module.access";
$arrTplVars['module.title'] = "МОДУЛИ CMS";

$arrTplVars['stPage'] = ( intval($_GET['stPage']) ? intval($_GET['stPage']) : 1 );

if ( isset($_POST['frmModuleForm']) && $_POST['frmModuleForm'] == 'true') {

    /************************************
     ** Shirokovskiy D.2005 Jimmy™. Mon Oct 17 16:47:25 MSD 2005
     **
     ** Получаем данные
     */
    $arrSqlData['intModuleID'] = $intModuleID = intval(trim($_POST['intModuleID']));
    $arrTplVars['intModuleID'] = ( $arrSqlData['intModuleID'] > 0 ? $arrSqlData['intModuleID'] : '');

    $arrSqlData['strModuleName'] = addslashes(trim($_POST['strModuleName']));
    $arrTplVars['strModuleName'] = htmlspecialchars(stripslashes(trim($_POST['strModuleName'])));

    $arrSqlData['strModuleAlias'] = addslashes(trim($_POST['strModuleAlias']));
    $arrTplVars['strModuleAlias'] = htmlspecialchars(stripslashes(trim($_POST['strModuleAlias'])));

    $arrSqlData['intParentModuleID'] = intval(trim($_POST['intParentModuleID']));
    $arrTplVars['intParentModuleID'] = ( $arrSqlData['intParentModuleID'] > 0 ? $arrSqlData['intParentModuleID'] : '');

    $arrSqlData['intParentModuleID_other'] = intval(trim($_POST['intParentModuleID_other']));
    $arrTplVars['intParentModuleID_other'] = ( $arrSqlData['intParentModuleID_other'] > 0 ? $arrSqlData['intParentModuleID_other'] : '');

    $arrSqlData['strModuleVersion'] = addslashes(trim($_POST['strModuleVersion']));
    $arrTplVars['strModuleVersion'] = htmlspecialchars(stripslashes(trim($_POST['strModuleVersion'])));

    $arrSqlData['strErrorAlias'] = addslashes(trim($_POST['strErrorAlias']));
    $arrTplVars['strErrorAlias'] = htmlspecialchars(stripslashes(trim($_POST['strErrorAlias'])));

    $arrSqlData['intModuleOrder'] = intval(trim($_POST['intModuleOrder']));
    $arrTplVars['intModuleOrder'] = ( $arrSqlData['intModuleOrder'] > 0 ? $arrSqlData['intModuleOrder'] : '');

    $arrSqlData['enumModulePosition'] = $_POST['enumModulePosition'];
    $arrTplVars['enumModulePosition_'.$_POST['enumModulePosition']] = ' checked';

    $arrSqlData['cbxSepModule'] = ($_POST['cbxSepModule']=='on'?'Y':'N');
    $arrTplVars['cbxSepModule'] = ($_POST['cbxSepModule']=='on'?' checked':'');

    $arrSqlData['cbxHideModule'] = ($_POST['cbxHideModule']=='on'?'Y':'N');
    $arrTplVars['cbxHideModule'] = ($_POST['cbxHideModule']=='on'?' checked':'');

    $arrSqlData['cbxStatusModule'] = ($_POST['cbxStatusModule']=='on'?'Y':'N');
    $arrTplVars['cbxStatusModule'] = ($_POST['cbxStatusModule']=='on'?' checked':'');

    $arrSqlData['cbxPopup'] = ($_POST['cbxPopup']=='on'?'Y':'N');
    $arrTplVars['cbxPopup'] = ($_POST['cbxPopup']=='on'?' checked':'');

    $arrProjectAccess = $_POST['chProject'];

    /************************************
     ** Shirokovskiy D.2005 Jimmy™. Mon Oct 17 16:47:39 MSD 2005
     **
     ** Проверяем на ошибки
     */
    if ( empty($arrSqlData['strModuleName']) ) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = '101mod';
        $arrTplVars['styleModuleName'] = "border-color:#CC0000";
    }

    if ( empty($arrSqlData['strModuleAlias']) ) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = '102mod';
        $arrTplVars['styleModuleAlias'] = "border-color:#CC0000";
    }

    if ( $GLOBALS['manStatusError']!=1 && empty($intModuleID) ) {
        $strSqlQuery = "SELECT cm_name FROM cms_modules WHERE cm_link LIKE '".$arrSqlData['strModuleAlias']."'";
        $existModule = $objDb->fetch( $strSqlQuery , 0);

        if ( !empty( $existModule ) ) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = '103mod';
            $arrTplVars['styleModuleAlias'] = "border-color:#CC0000";
        }
    }


    /************************************
     ** Shirokovskiy D.2005 Jimmy™. Mon Oct 17 17:17:15 MSD 2005
     **
     ** Запишем данные если нет ошибок
     */
    if ( $GLOBALS['manStatusError']!=1 ) {

        if ( !empty($arrSqlData['intParentModuleID']) ) {
            $parentID = intval($arrSqlData['intParentModuleID']);
        } else if ( !empty($arrSqlData['intParentModuleID_other']) ) {
            $parentID = intval($arrSqlData['intParentModuleID_other']);
        }

        if ( empty($arrSqlData['strModuleVersion']) ) {
            $arrSqlData['strModuleVersion'] = "v. 1.0";
        }

        if ( !empty( $arrSqlData['intModuleOrder'] ) ) {
            $orderNum = $arrSqlData['intModuleOrder'];
        } else {
            $strSqlQuery = "SELECT cm_order FROM cms_modules WHERE "
                . ( !empty($parentID) ? " cm_id_parent = '$parentID'" : " cm_id_parent IS NULL" )
                ." ORDER BY cm_order DESC LIMIT 1";
            $orderNum = $objDb->fetch( $strSqlQuery , 0);
            $orderNum += 1;
        }

        /** Jimmy™, Mon Jan 09 00:30:33 MSK 2006
         * Проверим упорядоченность
         **/
        $strSqlQuery = "SELECT cm_id, cm_order FROM cms_modules WHERE "
            . ( !empty($parentID) ? " cm_id_parent = '$parentID'" : " cm_id_parent IS NULL" )
            ." AND cm_order = '$orderNum' LIMIT 1";
        $arrExistOrder = $objDb->fetch( $strSqlQuery );
        if ( !empty($arrExistOrder) && is_array($arrExistOrder) ) {
            $strSqlQuery = "UPDATE cms_modules SET"
                ." cm_order=cm_order+1 WHERE "
                . ( !empty($parentID) ? " cm_id_parent = '$parentID'" : " cm_id_parent IS NULL" )
                ." AND cm_order >= '$orderNum'";
            if ( !$objDb->query( $strSqlQuery ) ) {
                print_r( $strSqlQuery );
                echo("<br />");
                print_r( mysql_error() );
                echo("<br />");
                die("<hr>\n".'In file:'.__FILE__.' at line:'.__LINE__."<hr>\n");
            }
        }

        $strSqlFields = ""
            ." cm_type             = 'user'"
            . ( !empty($parentID) ? ", cm_id_parent = '$parentID'" : "" )
            .", cm_insert_item     = '".$arrSqlData['enumModulePosition']."'"
            .", cm_separator       = '".$arrSqlData['cbxSepModule']."'"
            .", cm_hide            = '".$arrSqlData['cbxHideModule']."'"
            .", cm_status          = '".$arrSqlData['cbxStatusModule']."'"
            .", cm_version         = '".$arrSqlData['strModuleVersion']."'"
            .", cm_name            = '".$arrSqlData['strModuleName']."'"
            .", cm_link            = '".$arrSqlData['strModuleAlias']."'"
            .", cm_order           = '$orderNum'"
            .", cm_error_file_name = '".$arrSqlData['strErrorAlias']."'"
            .", cm_popup           = '".$arrSqlData['cbxPopup']."'"
        ;

        if ( !empty($intModuleID) ) {
            $strSqlQuery = "UPDATE cms_modules SET $strSqlFields WHERE cm_id='$intModuleID'";
        } else {
            $strSqlQuery = "INSERT INTO cms_modules SET $strSqlFields, cm_date_version = NOW()";
        }

        if ( !$objDb->query($strSqlQuery) ) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
        } else {
            $intModuleID = empty($intModuleID) ? $objDb->insert_id() : $intModuleID;

            /************************************
             ** Shirokovskiy D.2005 Jimmy™. Mon Oct 17 17:17:43 MSD 2005
             **
             ** Если запись модуля в базу произошла успешно, создадим шаблон и обработчик этого модуля
             */
            if ($arrSqlData['cbxPopup']=='Y') {
                $strFileTemplate = ""
                    ."<tpl popup.header>\n\n"
                    ."<table cellpadding=0 cellspacing=0 width=\"100%\">\n"
                    ."  <tr>\n"
                    ."    <td align=\"right\"><a href=\"javascript:void('');\" onclick=\"window.close();\" class=\"link-red\"><b>закрыть окно [x]</b></a></td>\n"
                    ."  </tr>\n"
                    ."</table>\n\n"
                    ."<table cellpadding=0 cellspacing=0 width=\"90%\">\n"
                    ."<tr>\n"
                    ."  <td><span class=\"txt-page-ob\"><b>{module.title}</b></span><br>\n"
                    ."    <span class=\"txt-page-ob-desc\">Раздел для работы с {module.title}.</span>{dot1px}</td>\n"
                    ."</tr>\n"
                    ."</table>\n"
                    ."{error}\n\n\n\n\n"
                    ."<tpl popup.footer>\n";
            } else {
                $strFileTemplate = "<tpl header>\n"
                    ."<tpl header.login.true>\n\n"
                    ."<table class=\"box-page-title\">\n"
                    ."<tr>\n"
                    ."  <td><span class=\"txt-page-ob\"><b>{module.title}</b></span><br>\n"
                    ."    <span class=\"txt-page-ob-desc\">Раздел для работы с {module.title}.</span>{dot1px}</td>\n"
                    ."</tr>\n"
                    ."</table>\n"
                    ."{error}\n\n\n\n\n"
                    ."<tpl footer>\n";
            }


            $strFilePHP = '<?php'."\n"
                .'$arrTplVars[\'module.name\'] = "'.$arrSqlData['strModuleAlias'].'";'."\n"
                .'$arrTplVars[\'module.title\'] = "'.$arrSqlData['strModuleName'].'";'."\n\n"
                .'$arrTplVars[\'stPage\'] = (!empty($_GET[\'stPage\']) ? intval($_GET[\'stPage\']) : 1);'."\n\n"
                .'// ***** Обработка ошибок для вывода *******************************************'."\n"
                .'$arrTplVars[\'error\'.$errSuf] = $objUtils->errorParse($GLOBALS[\'manCodeError\'], $GLOBALS[\'manStatusError\']);'."\n"
                .'$arrTplVars[\'error\'.$errSuf] = $objUtils->echoMessage($arrTplVars[\'error\'.$errSuf], $GLOBALS[\'manStatusError\']);'."\n"
                .'// *****************************************************************************'."\n\n"
                .'// **** Загружаем и обрабатываем шаблон'."\n"
                .'$objTpl->tpl_load($arrTplVars[\'module.name\'], $arrTplVars[\'module.name\'].".html");'."\n\n\n\n"
                .'$objTpl->tpl_if($arrTplVars[\'module.name\'], $arrIf);'."\n"
                .'$objTpl->tpl_array($arrTplVars[\'module.name\'], $arrTplVars);'."\n"
                ."\n";

            $strFileErr = '<?php'."\n"
                .'// **** Обработка ошибок раздела "'.$arrSqlData['strModuleName'].'"'."\n"
                .'$errMsg = array ('."\n"
                .'  \'msgErrorDB\'  =>  "<b>Работа с базой данных невозможна. Обратитесь к разработчикам.</b>",'."\n"
                .'  \'msgOk\'       =>  "Данные успешно сохранены",'."\n\n"
                .'  \'msgAccessOk\' =>  "Доступ успешно установлен",'."\n"
                .');'."\n"
                ."";

            if ( !file_exists(ADMTPL.$_POST['strModuleAlias'].'.html') ) {
                $objFile->writeFile($strFileTemplate, $_POST['strModuleAlias'].'.html', ADMTPL );
            }
            if ( !file_exists(ADMPHP.$_POST['strModuleAlias'].'.inc.php') ) {
                $objFile->writeFile($strFilePHP, $_POST['strModuleAlias'].'.inc.php', ADMPHP );
            }
            @chmod(ADMTPL.$_POST['strModuleAlias'].'.html', 0664);
            @chmod(ADMPHP.$_POST['strModuleAlias'].'.inc.php', 0664);

            if (!empty($arrSqlData['strErrorAlias'])) {
                if ( !file_exists(ADMERR.$_POST['strErrorAlias'].'.err.php') ) {
                    $objFile->writeFile($strFileErr, $_POST['strErrorAlias'].'.err.php', ADMERR );
                }
                @chmod(ADMERR.$_POST['strErrorAlias'].'.err.php', 0664);
            }

            if ( !empty($arrProjectAccess) ) {
                $strSqlQuery = "UPDATE cms_projects_modules SET"
                    ." fpm_status = 'N' WHERE fpm_module='$intModuleID'";
                if ( !$objDb->query($strSqlQuery) ) {
                    $GLOBALS['manStatusError']=1;
                    $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
                } else {
                    foreach ( $arrProjectAccess as $key => $value ) {
                        $strSqlQuery = "UPDATE cms_projects_modules SET"
                            ." fpm_status = 'Y' WHERE fpm_module='$intModuleID' AND fpm_project='$key'";
                        if ( !$objDb->query($strSqlQuery) ) {
                            $GLOBALS['manStatusError']=1;
                            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
                        } else {
                            if ( $objDb->affected() <= 0 ) {
                                $strSqlQuery = "INSERT INTO cms_projects_modules SET"
                                    ." fpm_status = 'Y'"
                                    .", fpm_project = '$key'"
                                    .", fpm_module = '$intModuleID'"
                                ;
                                if ( !$objDb->query($strSqlQuery) ) {
                                    $GLOBALS['manStatusError']=1;
                                    $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    if ( $GLOBALS['manStatusError']!=1 ) {
        header('location: '.$arrTplVars['module.name']."?errMess=msgModuleSave&cl=x&stPage=".$arrTplVars['stPage'].'&moduleID='.$intModuleID);
        exit();
    }
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

// Для формы редактирования
// вытащим все "условно" родительские модули, т.е. те, у которых id_parent = NULL
$strSqlQuery = "SELECT cm_id, cm_name FROM cms_modules WHERE cm_id_parent IS NULL ORDER BY cm_id";
$arrResult = $objDb->fetchall( $strSqlQuery );

// узнаем про существующие проекты
$strSqlQuery = "SELECT fp_id, fp_name FROM cms_projects WHERE fp_status = 'Y' ORDER BY fp_id";
$arrProjects = $objDb->fetchall( $strSqlQuery );

if ( isset($_GET['moduleID']) && intval($_GET['moduleID'])>0 ) {
    $strSqlQuery = "SELECT * FROM cms_modules WHERE cm_id='".$_GET['moduleID']."'";
    $arrModuleInfo = $objDb->fetch( $strSqlQuery );

    if ( !empty($arrModuleInfo) ) {
        $arrTplVars['intModuleID'] = $arrModuleInfo['cm_id'];

        $arrTplVars['strModuleName'] = htmlspecialchars($arrModuleInfo['cm_name']);
        $arrTplVars['strModuleAlias'] = htmlspecialchars($arrModuleInfo['cm_link']);

        if ( !empty($arrModuleInfo['cm_id_parent']) ) {
            foreach ( $arrResult as $key => $value ) {
                if ( $arrModuleInfo['cm_id_parent'] == $value['cm_id'] ) {
                    $arrResult[$key]['cm_selected'] = " selected";
                    $isSelectedParent = true;
                }
            }
            if ( !$isSelectedParent ) {
                $arrTplVars['intParentModuleID_other'] = intval($arrModuleInfo['cm_id_parent']);
            }
        }

        $arrTplVars['enumModulePosition_'.$arrModuleInfo['cm_insert_item']] = " selected";
        $arrTplVars['intModuleOrder'] = $arrModuleInfo['cm_order'];
        $arrTplVars['strModuleVersion'] = $arrModuleInfo['cm_version'];
        $arrTplVars['strErrorAlias'] = $arrModuleInfo['cm_error_file_name'];

        $arrTplVars['cbxSepModule'] = $arrModuleInfo['cm_separator'] == 'Y' ? " checked" : '';
        $arrTplVars['cbxHideModule'] = $arrModuleInfo['cm_hide'] == 'Y' ? " checked" : '';
        $arrTplVars['cbxStatusModule'] = $arrModuleInfo['cm_status'] == 'Y' ? " checked" : '';
        $arrTplVars['cbxPopup'] = $arrModuleInfo['cm_popup'] == 'Y' ? " checked" : '';

        $strSqlQuery = "SELECT * FROM cms_projects_modules WHERE fpm_module='".$arrModuleInfo['cm_id']."' AND fpm_status='Y'";
        $arrModuleProjects = $objDb->fetchall( $strSqlQuery );

        if ( !empty( $arrModuleProjects ) ) {
            foreach ( $arrProjects as $key => $value ) {
                foreach ( $arrModuleProjects as $keyMP => $valueMP ) {
                    if ( $value['fp_id'] == $valueMP['fpm_project'] ) {
                        $arrProjects[$key]['chProject'] = " checked";
                    }
                }
            }
        }
    }
}

if ( is_array($arrProjects) && !empty($arrProjects) && count($arrProjects) == 1 ) {
    $arrProjects[0]['chProject'] = " checked";
}

// Jimmy™ edition. Sun Oct 30 14:55:32 MSK 2005 //
// обработаем массив парентовых модулей только после обработки GETа.
$objTpl->tpl_loop($arrTplVars['module.name'], "list.parent.modules", $arrResult);
// то же самое и с проектами
$objTpl->tpl_loop($arrTplVars['module.name'], "list.projects", $arrProjects);

if ($_GET['cl']=='x') {
    $arrIf['edit.true'] = true;
}

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
