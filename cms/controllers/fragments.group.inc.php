<?php
$arrTplVars['module.name'] = "fragments.group";
$arrTplVars['module.title'] = "ГРУППЫ ФРАГМЕНТОВ";
$arrTplVars['module.parent'] = "fragments";

//$currentProject = $_SESSION['arrAccessProjectId'][1];
$currentProject = $_SESSION['intIdDefaultProject'];

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");


/* Удаление группы
    Влад Андерсен, 16:09:14 */
if (intval($_GET ['delItem'])) {
  $delItem = intval($_GET ['delItem']);

  // Ещё раз проверить, чтобы группа точно была пустой.

  $strSqlQuery = "SELECT COUNT(*) FROM site_fragments WHERE sf_id_parent = ".$delItem;
  $thisTempCount = $objDb->fetch( $strSqlQuery, 'COUNT(*)');
  $thisTempParent = $objDb->fetch( "SELECT sfg_parent FROM site_fragments_group WHERE sfg_id = ".$delItem, 'sfg_parent');


  if ($thisTempCount == 0) {
    $strSqlQuery = "DELETE FROM site_fragments_group WHERE sfg_id = ".$delItem.' LIMIT 1';
    $objDb->query($strSqlQuery);
    header ("location: ".$arrTplVars['module.name']."".($thisTempParent ? "?idItem=$thisTempParent" : ''));
    exit();
  }
}



/* Смотрим, добавлять ли чайлд-группу */

if (intval($_GET ['idItem'])) {
  $idItem = intval($_GET ['idItem']);
  $strSqlQuery = "SELECT * FROM site_fragments_group WHERE sfg_id = '".$idItem."'";
  $arrSqlQuery = $objDb->fetch( $strSqlQuery );

  if (!$arrSqlQuery['sfg_parent']) {
    $arrTplVars['currentParentGroup'] = $arrSqlQuery['sfg_name'];
    $arrIf['addChild'] = true;
  }
}



/* Апдейтим имена и статусы групп страниц */

if ($_POST ['frmGroupEdit'] == true) {
  foreach ($_POST as $key => $value ) {
      if (substr ($key, 0, 4) == 'name') {
        $nameInt = intval(substr ($key, 4));
        $tmpName = addslashes((trim($value)));
          $strSqlQuery = "UPDATE site_fragments_group SET sfg_name = '".$tmpName."', sfg_status = '".($_POST['status'.$nameInt] ? 'Y' : 'N')."' WHERE sfg_id = '".$nameInt."' LIMIT 1";
          $objDb->query($strSqlQuery);
      }
  }
}


/* Форма добавления корневой группы страниц */

if ($_POST ['frmGroupAdd'] == 'true') {
  $frmName = addslashes(trim($_POST['frmName']));
  if ($frmName) {
    $strSqlQuery = "INSERT INTO site_fragments_group (sfg_id_project, sfg_parent, sfg_name, sfg_status) VALUES (".
      "'".$currentProject."', ".
      "NULL, ".
      "'".$frmName."', ".
      "'Y'".
    ")";
    $objDb->query($strSqlQuery);
  }
}

/* Форма добавления подгруппы страниц */

if ($_POST ['frmSubGroupAdd'] == 'true') {
  $frmName = addslashes(trim($_POST['frmName']));
  if ($frmName) {
    $strSqlQuery = "INSERT INTO site_fragments_group (sfg_id_project, sfg_parent, sfg_name, sfg_status) VALUES (".
      "'".$currentProject."', ".
      $idItem.", ".
      "'".$frmName."', ".
      "'Y'".
    ")";
    $objDb->query($strSqlQuery);
  }
}


// ***** Кол-во страниц
$strSqlQuery = "SELECT COUNT(*) AS strQuantPages FROM site_fragments WHERE sf_id_project='".$_SESSION['intIdDefaultProject']."'";
$arrTplVars['strQuantPages'] = $objDb->fetch($strSqlQuery, 'strQuantPages');

$strSqlQuery = "SELECT COUNT(*) AS strActiveQuantPages FROM site_fragments WHERE sf_id_project='".$_SESSION['intIdDefaultProject']."' AND sf_status='Y'";
$arrTplVars['strActiveQuantPages'] = $objDb->fetch($strSqlQuery, 'strActiveQuantPages');

$arrTplVars['strActiveNoQuantPages'] = $arrTplVars['strQuantPages']-$arrTplVars['strActiveQuantPages'];

// ***** Формируем список групп *********************************************
$strSqlQuery = "SELECT * FROM site_fragments_group WHERE sfg_id_project='".$_SESSION['intIdDefaultProject']."' AND sfg_parent IS NULL";
$arrLstItem = $objDb->fetchall($strSqlQuery);

if(is_array($arrLstItem)) {
  foreach($arrLstItem as $key=>$value) {
    $arrItemForTpl[$key] =
          array('intIdItem'=>$arrLstItem[$key]['sfg_id'],
                'strNameItem'=>htmlspecialchars($arrLstItem[$key]['sfg_name']),
                'strStatusItem'=>($arrLstItem[$key]['sfg_status'] == 'Y' ? 'checked' : '')
                );
    $arrParent[$arrLstItem[$key]['sfg_id']] = true;

    	  /* Для пустых групп выводим кнопку "удалить"
	               Влад Андерсен, 16:51:18 */

    $strSqlQuery = "SELECT COUNT(*) AS cCount FROM site_fragments WHERE sf_id_parent = ".$value['sfg_id'];
    $thisTempCount = $objDb->fetch( $strSqlQuery, 'cCount');
    if ($thisTempCount == 0) $arrIf['delItem'.$value['sfg_id']] = true;

  }
} else { // Если каталог пуст (нет групп страниц)
  $arrIf['block.catalogue.empty'] = true;
}

$objTpl->tpl_loop($arrTplVars['module.name'], "lst.item", $arrItemForTpl);

$arrTplVars['intIdItem'] = intval($_GET['idItem']);

if($arrTplVars['intIdItem']>0) { // Если ID выбранного каталога больше 0
  if($arrParent[$arrTplVars['intIdItem']]!=true)
    $inIdParentSubItem = $objDb->fetch( "SELECT sfg_parent FROM site_fragments_group WHERE sfg_id_project='".$_SESSION['intIdDefaultProject']."' AND sfg_id='".$arrTplVars['intIdItem']."' AND sfg_status='Y'", 'sfg_parent');
  else
    $inIdParentSubItem = $arrTplVars['intIdItem'];

// ***** Выбираем вложенные папки в родительской
  $arrLstSubItem = $objDb->fetchall("SELECT * FROM site_fragments_group WHERE sfg_id_project='".$_SESSION['intIdDefaultProject']."' AND sfg_parent='".$inIdParentSubItem."'");

  if (is_array($arrLstSubItem)) {
    $arrIf['block.'.$inIdParentSubItem] = true;
  	foreach ($arrLstSubItem as $key=>$value) {
	    $arrSubItemForTpl[$key] =
	           array('intIdSubItem'=>$arrLstSubItem[$key]['sfg_id'],
	           'strNameSubItem'=>htmlspecialchars($arrLstSubItem[$key]['sfg_name']),
	           'strStatusSubItem'=>($arrLstSubItem[$key]['sfg_status'] == 'Y' ? 'checked' : '')
	           );

	           	  /* Для пустых подгрупп выводим кнопку "удалить"
	               Влад Андерсен, 16:51:18 */

    $strSqlQuery = "SELECT COUNT(*) AS cCount FROM site_fragments WHERE sf_id_parent = ".$value['sfg_id'];
    $thisTempCount = $objDb->fetch( $strSqlQuery, 'cCount');
    if ($thisTempCount == 0) $arrIf['delItem'.$value['sfg_id']] = true;


  	}
	}



	$objTpl->tpl_loop($arrTplVars['module.name'], "lst.subitem.".$inIdParentSubItem, $arrSubItemForTpl);

}

// **************************************************************************************

//$objTpl->strip_loops($arrTplVars['module.name']);
$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
