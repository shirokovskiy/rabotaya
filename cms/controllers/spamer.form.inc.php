<?php
$arrTplVars['module.name'] = "spamer.form";
$arrTplVars['module.parent'] = "spamer";
$arrTplVars['module.title'] = "Создать шаблон";

// Include SPAW editor
include_once(SITE_LIB_DIR.'spaw2/spaw.inc.php');
$objSpawEditor = new SpawEditor("strBodyHtml");
$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

/**
 * Запись данных
 */
if ( isset($_POST['frmEdit']) && $_POST['frmEdit'] == 'true' ) {
    $intRecordId = $arrSqlData['intRecordId'] = intval(trim($_POST['intRecordId']));
    $arrTplVars['intRecordId'] = ( $arrSqlData['intRecordId'] > 0 ? $arrSqlData['intRecordId'] : '');

    $arrSqlData['strTitle'] = addslashes(trim($_POST['strTitle']));
    $arrTplVars['strTitle'] = htmlspecialchars(stripslashes(trim($_POST['strTitle'])));

    if (empty($arrSqlData['strTitle']) ) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgEmptyTitle';
    }

    $arrSqlData['strBodyText'] = addslashes(trim($_POST['strBodyText']));
    $arrTplVars['strBodyText'] = htmlspecialchars(stripslashes(trim($_POST['strBodyText'])));

    if (empty($arrSqlData['strBodyText']) ) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgEmptyContent';
    }

    $arrSqlData['strBodyHtml'] = addslashes(trim($_POST['strBodyHtml']));
    $objSpawEditor->getActivePage()->value = trim($_POST['strBodyHtml']);

    $arrSqlData['cbxReturn'] = ($_POST['cbxReturn']=='on'?'Y':'N');
    $arrTplVars['cbxReturn'] = ($_POST['cbxReturn']=='on'?' checked':'');

    if ($GLOBALS['manStatusError']!=1) {
        $strSqlFields = ""
            ." ste_title = '{$arrSqlData['strTitle']}'"
            .", ste_body = '{$arrSqlData['strBodyText']}'"
            .", ste_html = '{$arrSqlData['strBodyHtml']}'"
        ;

        if ( empty($intRecordId) ) {
            $strSqlQuery = "INSERT INTO site_templates_email SET ".$strSqlFields
                .", ste_date_create = UNIX_TIMESTAMP()"
                .", ste_date_update = UNIX_TIMESTAMP()";
        } else {
            $strSqlQuery = "UPDATE site_templates_email SET".$strSqlFields
                .", ste_date_update = UNIX_TIMESTAMP()"
                ." WHERE ste_id = ".$intRecordId;
        }

        if ( !$objDb->query( $strSqlQuery ) ) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
        } else {
            if ( empty($intRecordId) ) {
                $intRecordId = $objDb->insert_id();
            }
        }

        if ( $GLOBALS['manStatusError']!=1 ) {
            if ( $arrSqlData['cbxReturn'] == 'Y' ) {
                header('location:'.$arrTplVars['module.name'].'?errMess=msgOk&stPage='.$arrTplVars['stPage'].'&id='.$intRecordId);
            } else {
                header('location:'.$arrTplVars['module.parent'].'?errMess=msgOk&stPage='.$arrTplVars['stPage']);
            }
            exit;
        }
    }
}

// ***** Обработка ошибок для вывода *******************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// *****************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if ( isset($_GET['id']) && intval($_GET['id']) > 0 && $GLOBALS['manStatusError']!=1 ) {
    $arrTplVars['module.title'] = "Редактировать шаблон письма";
    $intRecordId = $arrTplVars['intRecordId'] = intval($_GET['id']);

    $strSqlQuery = "SELECT * FROM site_templates_email WHERE ste_id = ".$intRecordId;
    $arrInfo = $objDb->fetch( $strSqlQuery );

    if ( is_array( $arrInfo )) {
        $arrTplVars['strTitle'] = htmlspecialchars(stripslashes($arrInfo['ste_title']));
        $arrTplVars['strBodyText'] = htmlspecialchars(stripslashes($arrInfo['ste_body']));

        $objSpawEditor->getActivePage()->value = $arrInfo['ste_html'];

        $arrTplVars['strDateCreate'] = date('d.m.Y в H:i', $arrInfo['ste_date_create']);
        $arrTplVars['strDateUpdate'] = date('d.m.Y в H:i', $arrInfo['ste_date_update']);

        $arrTplVars['cbxStatus'] = $arrInfo['ste_status'] == 'Y' ? ' checked' : '';

//        $arrIf['exist.image'] = $arrInfo['ste_image'] == 'Y';
    }
}

//$objSpawEditor->setConfigValueElement('PG_SPAWFM_SETTINGS', 'allow_modify', true);
//$objSpawEditor->setConfigValueElement('PG_SPAWFM_SETTINGS', 'allow_upload', true);
$objSpawEditor->setDimensions('600px', null);
$arrTplVars['strBodyHtml'] = $objSpawEditor->getHtml();


$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);

