<?php
$arrTplVars['module.name'] = "users";
$arrTplVars['module.title'] = "МЕНЕДЖЕРЫ ПАНЕЛИ УПРАВЛЕНИЯ";
$arrTplVars['module.name.child'] = "users.edit";

$arrTplVars['stPage'] = intval($_GET['stPage'])>0 ? intval($_GET['stPage']) : 1 ;

if (isset($_GET['action'])) {
    switch ($_GET['action']) {
        case 'delete':
            if ( isset($_GET['id']) && intval($_GET['id'])>0 ) {
                $intId = $_GET['id'];
                // удалим пользователя
                $strSqlQuery = "DELETE FROM cms_users WHERE cu_id='$intId'";
                if ( !$objDb->query( $strSqlQuery ) ) {
                    $GLOBALS['manStatusError']=1;
                    $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
                } else {
                    // удалим права доступа к проектам
                    $strSqlQuery = "DELETE FROM cms_projects_access WHERE fpa_id_user = '$intId'";
                    if ( !$objDb->query( $strSqlQuery ) ) {
                        $GLOBALS['manStatusError']=1;
                        $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
                    } else {
                        // удалим права доступа на модули
                        $strSqlQuery = "DELETE FROM cms_users_access_modules WHERE cuam_id_owner='$intId'";
                        if ( !$objDb->query( $strSqlQuery ) ) {
                            $GLOBALS['manStatusError']=1;
                            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
                        } else {
                            header("location: ".$arrTplVars['module.name']."?stPage=".$arrTplVars['stPage']."&errMess=103usr");
                            exit();
                        }
                    }
                }
            } else {
                header("location: ".$arrTplVars['module.name']."?stPage=".$arrTplVars['stPage']."");
                exit();
            }
            break;

        default:
            break;
    }
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

$strSqlQuery = "SELECT COUNT(*) AS strQuantAllRecords FROM cms_users";
$arrTplVars['intQuantitySelectRecords'] = $arrTplVars['strQuantAllRecords'] = $objDb->fetch( $strSqlQuery , 'strQuantAllRecords');

/**
 * Построение пейджинга для вывода списка записей полученных из БД
 */
$objPaginator = new tplPaginator($arrTplVars['intQuantitySelectRecords'], SITE_TPL_TPL_DIR);
/**
 * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
 * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
 */
$objPaginator->strPaginatorTpl = "site.global.1.tpl";
$objPaginator->intQuantRecordPage = 18;
// Создаем блок пэйджинга
$objPaginator->paCreate();

/**
 * Выбираем записи из БД с учетом постраничного вывода и заполняем массив для вывода в шаблон
 */
$strSqlQuery = "SELECT * FROM cms_users, cms_users_group WHERE cug_id=cu_group ORDER BY cu_lname".$objPaginator->strSqlLimit;
$arrSelectedRecords = $objDb->fetchall( $strSqlQuery );
$arrTplVars['intQuantityShowRecOnPage'] = ( !empty($arrSelectedRecords) ? count($arrSelectedRecords) : 0 ); // кол-во строк показанных на странице

if (is_array($arrSelectedRecords)) {
    foreach($arrSelectedRecords as $key=>$value) {
        $arrSelRecForTpl[$key]['intIdManager'] = $arrSelectedRecords[$key]['cu_id'];
        $arrSelRecForTpl[$key]['strFIOManager'] = $arrSelectedRecords[$key]['cu_lname'].' '.$arrSelectedRecords[$key]['cu_fname'].' '.$arrSelectedRecords[$key]['cu_pname'];
        $arrSelRecForTpl[$key]['strLoginManager'] = $arrSelectedRecords[$key]['cu_login'];
        $arrSelRecForTpl[$key]['strTypeAccess'] = $arrSelectedRecords[$key]['cug_name'];
        $arrSelRecForTpl[$key]['strLastEnter'] = ($arrSelectedRecords[$key]['cu_last_enter']===NULL) ? 'не определено' : $objUtils->workDate(4, $arrSelectedRecords[$key]['cu_last_enter']);

        # Jimmy™, Fri Sep 01 14:08:19 MSD 2006
        $arrSelRecForTpl[$key]['bgRow'] = ($value['cu_status']=='N'? "class='inactive-user'" : "" );
        $arrSelRecForTpl[$key]['bgRow'] = ($value['cu_id']==$_SESSION['userInfoStorage']['cu_id'] ? "class='active-user'" : $arrSelRecForTpl[$key]['bgRow'] );
    }
}
$arrTplVars['blockPaginatorTop'] = ($arrTplVars['intQuantitySelectRecords'] > 20?$objPaginator->paShow():"") ;
$arrTplVars['blockPaginator'] = $objPaginator->paShow();

$objTpl->tpl_loop($arrTplVars['module.name'], "users", $arrSelRecForTpl, array($arrTplVars['cBgColor'], $arrTplVars['cThirdColor']));

$arrTplVars['clrActiveUser'] = 'active-user';
$arrTplVars['clrInActiveUser'] = 'inactive-user';

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);

