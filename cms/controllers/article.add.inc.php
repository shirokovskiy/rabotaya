<?php
$arrTplVars['module.name'] = "article.add";
$arrTplVars['module.parent'] = "articles";
$arrTplVars['module.title'] = "Добавить статью";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

/**
 * Удаление фото
 */
if ( intval($_GET['id']) > 0 && $_GET['act'] == 'del' ) {
    $intRecordId = intval($_GET['id']);
    $strSqlQuery = "UPDATE site_articles SET sa_image = 'N' WHERE sa_id = ".$intRecordId." AND sa_type='article'";
    if ( !$objDb->query($strSqlQuery) ) {
        #mysql error
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
    } else {
        if ( file_exists(PRJ_IMAGES."articles/articles.photo.$intRecordId.jpg")) {
            unlink(PRJ_IMAGES."articles/articles.photo.$intRecordId.jpg");
            unlink(PRJ_IMAGES."articles/articles.photo.$intRecordId.b.jpg");
        }

        header("Location: {$arrTplVars['module.name']}?id=$intRecordId&stPage={$arrTplVars['stPage']}");
        exit();
    }
}


/**
 * Запись данных
 */
if ( isset($_POST['frmEdit']) && $_POST['frmEdit'] == 'true' ) {
    $intRecordId = $arrSqlData['intRecordId'] = intval(trim($_POST['intRecordId']));
    $arrTplVars['intRecordId'] = ( $arrSqlData['intRecordId'] > 0 ? $arrSqlData['intRecordId'] : '');

    $arrSqlData['strTitle'] = addslashes(trim($_POST['strTitle']));
    $arrTplVars['strTitle'] = htmlspecialchars(stripslashes(trim($_POST['strTitle'])));

    if (empty($arrSqlData['strTitle']) ) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgEmptyTitle';
    }

    $arrSqlData['strBody'] = addslashes(trim($_POST['strBody']));
    $arrTplVars['strBody'] = htmlspecialchars(stripslashes(trim($_POST['strBody'])));

    if (empty($arrSqlData['strBody']) ) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgEmptyContent';
    }

    $arrSqlData['strDate'] = mysql_real_escape_string(trim($_POST['strDate']));
    $arrTplVars['strDate'] = htmlspecialchars(stripslashes(trim($_POST['strDate'])));

    if (empty($arrSqlData['strDate'])) {
        $arrSqlData['strDate'] = $arrTplVars['strDate'] = date('d.m.Y');
    }

    $datePublish = date('Y-m-d', strtotime($arrSqlData['strDate']));

    if (!$objUtils->dateValid($datePublish)) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgFailDate';
    } else {
        $datePublish = strtotime($arrSqlData['strDate']);
    }

    $arrSqlData['cbxStatus'] = ($_POST['cbxStatus']=='on'?'Y':'N');
    $arrTplVars['cbxStatus'] = ($_POST['cbxStatus']=='on'?' checked':'');

    $arrSqlData['cbxReturn'] = ($_POST['cbxReturn']=='on'?'Y':'N');
    $arrTplVars['cbxReturn'] = ($_POST['cbxReturn']=='on'?' checked':'');

    if ($GLOBALS['manStatusError']!=1) {
        $strSqlFields = ""
            ." sa_title = '{$arrSqlData['strTitle']}'"
            .", sa_content = '{$arrSqlData['strBody']}'"
            .", sa_date_publish = ".$datePublish
            .", sa_status = '{$arrSqlData['cbxStatus']}'"
        ;

        if ( empty($intRecordId) ) {
            $strSqlQuery = "INSERT INTO site_articles SET ".$strSqlFields
                .", sa_date_add = NOW()";
        } else {
            $strSqlQuery = "UPDATE site_articles SET".$strSqlFields
                .", sa_date_change = NOW()"
                ." WHERE sa_id = ".$intRecordId." AND sa_type='article'";
        }

        if ( !$objDb->query( $strSqlQuery ) ) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
        } else {
            if ( empty($intRecordId) ) {
                $intRecordId = $objDb->insert_id();
            }
        }

        /**
         * Загрузка фото
         */
        if ( $_FILES['flPhoto']['error'] == 0 && $_FILES['flPhoto']['size'] > 0 && $_FILES['flPhoto']['size'] < 2000000 ) {
            $strImageName = "articles.photo.$intRecordId";

            /////////////////////////////////////////////////////////////////////////////////////
            // extended
            $arrExts['pjpeg'] = 'jpg';
            $arrExts['jpeg'] = 'jpg';

            $mimeType = explode("/", $_FILES['flPhoto']['type']);
            $mimeType = $mimeType[1];

            if ( array_key_exists($mimeType , $arrExts) ) {
                $mimeType = $arrExts[$mimeType];
            }

            if ( $mimeType != 'jpg' ) {
                $GLOBALS['manStatusError']=1;
                $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
            } else {
                $fileName = "$strImageName.$mimeType";
                $fileNameBig = "$strImageName.b.$mimeType";
                $fileNameOrig = "temp.$strImageName.$mimeType";

                $dirStorage = DROOT."storage";
                if ( !file_exists($dirStorage) ) {
                    if (!mkdir($dirStorage, 0775)) {
                        $GLOBALS['manStatusError']=1;
                        $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
                    }
                }

                $dirImages = $dirStorage."/files";
                if ( !file_exists($dirImages) ) {
                    if (!mkdir($dirImages, 0775)) {
                        $GLOBALS['manStatusError']=1;
                        $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
                    }
                }

                $dirImages = $dirImages."/images";
                if ( !file_exists($dirImages) ) {
                    if (!mkdir($dirImages, 0775)) {
                        $GLOBALS['manStatusError']=1;
                        $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
                    }
                }

                $dirImages = $dirImages."/articles";
                if ( !file_exists($dirImages) ) {
                    if (!mkdir($dirImages, 0775)) {
                        $GLOBALS['manStatusError']=1;
                        $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
                    }
                }

                if ($GLOBALS['manStatusError']!=1) {
                    move_uploaded_file($_FILES['flPhoto']['tmp_name'], $dirImages."/".$fileNameOrig );

                    if ( !is_object($objImage) ) {
                        include_once(SITE_LIB_DIR."cls.images.php");
                        $objImage = new clsImages();
                    }

                    $objImage->resizeImage($dirImages."/".$fileNameOrig, $dirImages."/".$fileName, 200 );
                    $objImage->resizeImage($dirImages."/".$fileNameOrig, $dirImages."/".$fileNameBig, 800 );

                    unlink($dirImages."/".$fileNameOrig);
                }
            }
            /////////////////////////////////////////////////////////////////////////////////////

            if ($GLOBALS['manStatusError']!=1) {
                $strSqlQuery = "UPDATE site_articles SET"
                    ." sa_image = 'Y'"
                    .", sa_date_change = NOW()"
                    ." WHERE sa_id = ".$intRecordId;
                if ( !$objDb->query( $strSqlQuery ) ) {
                    $GLOBALS['manStatusError']=1;
                    $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
                }
            }
        }

        if ( $GLOBALS['manStatusError']!=1 ) {
            if ( $arrSqlData['cbxReturn'] == 'Y' ) {
                header('location:'.$arrTplVars['module.name'].'?errMess=msgOk&stPage='.$arrTplVars['stPage'].'&id='.$intRecordId);
            } else {
                header('location:'.$arrTplVars['module.parent'].'?errMess=msgOk&stPage='.$arrTplVars['stPage']);
            }
            exit;
        }
    }
}

// ***** Обработка ошибок для вывода *******************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// *****************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");


if ( isset($_GET['id']) && intval($_GET['id']) > 0 ) {
    $arrTplVars['module.title'] = "Редактировать статью";
    $intRecordId = $arrTplVars['intRecordId'] = intval($_GET['id']);

    $strSqlQuery = "SELECT * FROM site_articles WHERE sa_id = ".$intRecordId." AND sa_type='article'";
    $arrInfo = $objDb->fetch( $strSqlQuery );

    if ( is_array( $arrInfo )) {
        $arrTplVars['strTitle'] = htmlspecialchars(stripslashes($arrInfo['sa_title']));
        $arrTplVars['strBody'] = htmlspecialchars(stripslashes($arrInfo['sa_content']));
        $arrTplVars['strDate'] = date('d.m.Y', $arrInfo['sa_date_publish']);
        $arrTplVars['cbxStatus'] = $arrInfo['sa_status'] == 'Y' ? ' checked' : '';
        $arrIf['exist.image'] = $arrInfo['sa_image'] == 'Y';
    }
}
$arrIf['no.image'] = !$arrIf['exist.image'];

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);

