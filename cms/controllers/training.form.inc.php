<?php
$arrTplVars['module.name'] = "training.form";
$arrTplVars['module.parent'] = "trainings";
$arrTplVars['module.title'] = "Добавить тренинг";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

/**
 * Удаление фото
 */
if ( intval($_GET['id']) > 0 && $_GET['act'] == 'del' ) {
    $intRecordId = intval($_GET['id']);
    $strSqlQuery = "UPDATE site_trainings SET str_image = 'N' WHERE str_id = ".$intRecordId." AND str_type='article'";
    if ( !$objDb->query($strSqlQuery) ) {
        #mysql error
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
    } else {
        if ( file_exists(PRJ_IMAGES."trainings/trainings.photo.$intRecordId.jpg")) {
            unlink(PRJ_IMAGES."trainings/trainings.photo.$intRecordId.jpg");
            unlink(PRJ_IMAGES."trainings/trainings.photo.$intRecordId.b.jpg");
        }

        header("Location: {$arrTplVars['module.name']}?id=$intRecordId&stPage={$arrTplVars['stPage']}");
        exit();
    }
}

/**
 * Запись данных
 */
if ( isset($_POST['frmEdit']) && $_POST['frmEdit'] == 'true' ) {
    $intRecordId = $arrSqlData['intRecordId'] = intval(trim($_POST['intRecordId']));
    $arrTplVars['intRecordId'] = ( $arrSqlData['intRecordId'] > 0 ? $arrSqlData['intRecordId'] : '');

    $intRubricID = $arrSqlData['intRubricId'] = intval(trim($_POST['intRubricId']));
    $arrTplVars['intRubricId'] = ( $arrSqlData['intRubricId'] > 0 ? $arrSqlData['intRubricId'] : '');

    $arrSqlData['strTitle'] = addslashes(trim($_POST['strTitle']));
    $arrTplVars['strTitle'] = htmlspecialchars(stripslashes(trim($_POST['strTitle'])));

    if (empty($arrSqlData['strTitle']) ) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgEmptyTitle';
    }

    $arrSqlData['strBody'] = addslashes(trim($_POST['strBody']));
    $arrTplVars['strBody'] = htmlspecialchars(stripslashes(trim($_POST['strBody'])));

    if (empty($arrSqlData['strBody']) ) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgEmptyContent';
    }

    $arrSqlData['strAddress'] = mysql_real_escape_string(trim($_POST['strAddress']));
    $arrTplVars['strAddress'] = htmlspecialchars(stripslashes(trim($_POST['strAddress'])));

    $arrSqlData['strPhone'] = mysql_real_escape_string(trim($_POST['strPhone']));
    $arrTplVars['strPhone'] = htmlspecialchars(stripslashes(trim($_POST['strPhone'])));

    $arrSqlData['strEmail'] = mysql_real_escape_string(trim($_POST['strEmail']));
    $arrTplVars['strEmail'] = htmlspecialchars(stripslashes(trim($_POST['strEmail'])));

    $arrSqlData['strWeb'] = mysql_real_escape_string(trim($_POST['strWeb']));
    $arrTplVars['strWeb'] = htmlspecialchars(stripslashes(trim($_POST['strWeb'])));

    $arrSqlData['strDate'] = mysql_real_escape_string(trim($_POST['strDate']));
    $arrTplVars['strDate'] = htmlspecialchars(stripslashes(trim($_POST['strDate'])));

    if (empty($arrSqlData['strDate'])) {
        $arrSqlData['strDate'] = $arrTplVars['strDate'] = date('d.m.Y');
    }

    $datePublish = date('Y-m-d', strtotime($arrSqlData['strDate']));

    if (!$objUtils->dateValid($datePublish)) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgFailDate';
    } else {
        $datePublish = strtotime($arrSqlData['strDate']);
    }

    $arrSqlData['cbxStatus'] = ($_POST['cbxStatus']=='on'?'Y':'N');
    $arrTplVars['cbxStatus'] = ($_POST['cbxStatus']=='on'?' checked':'');

    $arrSqlData['cbxReturn'] = ($_POST['cbxReturn']=='on'?'Y':'N');
    $arrTplVars['cbxReturn'] = ($_POST['cbxReturn']=='on'?' checked':'');

    if ($GLOBALS['manStatusError']!=1) {
        $strSqlFields = ""
            ." str_title = '{$arrSqlData['strTitle']}'"
            .", str_desc = '{$arrSqlData['strBody']}'"
            .", str_address = '{$arrSqlData['strAddress']}'"
            .", str_phone = '{$arrSqlData['strPhone']}'"
            .", str_email = '{$arrSqlData['strEmail']}'"
            .", str_web = '{$arrSqlData['strWeb']}'"
            .", str_status = '{$arrSqlData['cbxStatus']}'"
        ;

        if ( empty($intRecordId) ) {
            $strSqlQuery = "INSERT INTO site_trainings SET ".$strSqlFields
                .", str_date_create = NOW(), `str_alias` = CONCAT('http://www.rabota-ya.ru/trainings/', UNIX_TIMESTAMP())";
        } else {
            $strSqlQuery = "UPDATE site_trainings SET".$strSqlFields
                ." WHERE str_id = ".$intRecordId;
        }

        if ( !$objDb->query( $strSqlQuery ) ) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
        } else {
            if ( empty($intRecordId) ) {
                $intRecordId = $objDb->insert_id();
            }

            if ($intRubricID>0) {
                $strSqlQuery = "SELECT * FROM `site_training_rubric_lnk` WHERE `strl_st_id` = ".$intRecordId." AND `strl_str_id` = ".$intRubricID;
                $arr = $objDb->fetch($strSqlQuery);
                if (is_array($arr) && !empty($arr)) {
                    # do something if necessary
                } else {
                    $strSqlQuery = "INSERT INTO `site_training_rubric_lnk` SET"
                        . " strl_str_id = $intRubricID"
                        . ", strl_st_id = $intRecordId";
                    if (!$objDb->query($strSqlQuery)) {
                        # sql error
                        $GLOBALS['manStatusError']=1;
                        $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
                    }
                }
            }
        }

        /**
         * Загрузка фото
         */
        if ( $_FILES['flPhoto']['error'] == 0 && $_FILES['flPhoto']['size'] > 0 && $_FILES['flPhoto']['size'] < 2000000 ) {
            $strImageName = "trainings.photo.$intRecordId";

            /////////////////////////////////////////////////////////////////////////////////////
            // extended
            $arrExts['pjpeg'] = 'jpg';
            $arrExts['jpeg'] = 'jpg';

            $mimeType = explode("/", $_FILES['flPhoto']['type']);
            $mimeType = $mimeType[1];

            if ( array_key_exists($mimeType , $arrExts) ) {
                $mimeType = $arrExts[$mimeType];
            }

            if ( $mimeType != 'jpg' ) {
                $GLOBALS['manStatusError']=1;
                $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
            } else {
                $fileName = "$strImageName.$mimeType";
                $fileNameBig = "$strImageName.b.$mimeType";
                $fileNameOrig = "temp.$strImageName.$mimeType";

                $dirStorage = DROOT."storage";
                if ( !file_exists($dirStorage) ) {
                    if (!mkdir($dirStorage, 0775)) {
                        $GLOBALS['manStatusError']=1;
                        $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
                    }
                }

                $dirImages = $dirStorage."/files";
                if ( !file_exists($dirImages) ) {
                    if (!mkdir($dirImages, 0775)) {
                        $GLOBALS['manStatusError']=1;
                        $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
                    }
                }

                $dirImages = $dirImages."/images";
                if ( !file_exists($dirImages) ) {
                    if (!mkdir($dirImages, 0775)) {
                        $GLOBALS['manStatusError']=1;
                        $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
                    }
                }

                $dirImages = $dirImages."/trainings";
                if ( !file_exists($dirImages) ) {
                    if (!mkdir($dirImages, 0775)) {
                        $GLOBALS['manStatusError']=1;
                        $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
                    }
                }

                if ($GLOBALS['manStatusError']!=1) {
                    move_uploaded_file($_FILES['flPhoto']['tmp_name'], $dirImages."/".$fileNameOrig );

                    if ( !is_object($objImage) ) {
                        include_once(SITE_LIB_DIR."cls.images.php");
                        $objImage = new clsImages();
                    }

                    $objImage->resizeImage($dirImages."/".$fileNameOrig, $dirImages."/".$fileName, 200 );
                    $objImage->resizeImage($dirImages."/".$fileNameOrig, $dirImages."/".$fileNameBig, 800 );

                    unlink($dirImages."/".$fileNameOrig);
                }
            }
            /////////////////////////////////////////////////////////////////////////////////////

            if ($GLOBALS['manStatusError']!=1) {
                $strSqlQuery = "UPDATE site_trainings SET"
                    ." str_image = 'Y'"
                    .", str_date_change = NOW()"
                    ." WHERE str_id = ".$intRecordId;
                if ( !$objDb->query( $strSqlQuery ) ) {
                    $GLOBALS['manStatusError']=1;
                    $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
                }
            }
        }

        if ( $GLOBALS['manStatusError']!=1 ) {
            if ( $arrSqlData['cbxReturn'] == 'Y' ) {
                header('location:'.$arrTplVars['module.name'].'?errMess=msgOk&stPage='.$arrTplVars['stPage'].'&id='.$intRecordId);
            } else {
                header('location:'.$arrTplVars['module.parent'].'?errMess=msgOk&stPage='.$arrTplVars['stPage']);
            }
            exit;
        }
    }
}

// ***** Обработка ошибок для вывода *******************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// *****************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

$intRubricID = 0;

if ( isset($_GET['id']) && intval($_GET['id']) > 0 ) {
    $arrTplVars['module.title'] = "Редактировать тренинг";
    $intRecordId = $arrTplVars['intRecordId'] = intval($_GET['id']);

    $strSqlQuery = "SELECT * FROM site_trainings WHERE str_id = ".$intRecordId;
    $arrInfo = $objDb->fetch( $strSqlQuery );

    if ( is_array( $arrInfo )) {
        $arrTplVars['strTitle'] = htmlspecialchars(stripslashes($arrInfo['str_title']));
        $arrTplVars['strBody'] = htmlspecialchars(stripslashes($arrInfo['str_desc']));
        $arrTplVars['strAddress'] = htmlspecialchars(stripslashes($arrInfo['str_address']));
        $arrTplVars['strWeb'] = htmlspecialchars(stripslashes($arrInfo['str_web']));
        $arrTplVars['strPhone'] = htmlspecialchars(stripslashes($arrInfo['str_phone']));
        $arrTplVars['strEmail'] = htmlspecialchars(stripslashes($arrInfo['str_email']));
        $arrTplVars['strDate'] = date('d.m.Y', $arrInfo['str_date_create']);
        $arrTplVars['cbxStatus'] = $arrInfo['str_status'] == 'Y' ? ' checked' : '';

        /**
         * Выбрать инфо о рубрике
         */
        $strSqlQuery = "SELECT * FROM `site_training_rubric_lnk` WHERE `strl_st_id` = " . $intRecordId;
        $arrRubricLnk = $objDb->fetch($strSqlQuery);
        if (is_array($arrRubricLnk) && !empty($arrRubricLnk)) {
            $intRubricID = $arrRubricLnk['strl_str_id'];
        }
    }
} else {
    if (isset($_REQUEST['intRubricId'])) {
        $intRubricID = $_REQUEST['intRubricId'];
    }
}

/**
 * Рубрики
 */
$strSqlQuery = "SELECT * FROM `site_training_rubrics` WHERE `str_parent_id` IS NULL ORDER BY `str_id`";
$arrRubrics = $objDb->fetchall($strSqlQuery);
if (is_array($arrRubrics) && !empty($arrRubrics)) {
    foreach ($arrRubrics as $k => $rubric) {
        $arrRubrics[$k]['strRubricName'] = htmlspecialchars($rubric['str_rubricname']);
        $arrRubrics[$k]['intRubricID'] = $rubric['str_id'];
        $arrRubrics[$k]['selected'] = $rubric['str_id'] == $intRubricID ? 'selected' : '';
    }
}
$objTpl->tpl_loop($arrTplVars['module.name'], "main.rubrics", $arrRubrics);

/**
 * Дочерние рубрики
 */
if (is_array($arrRubrics) && !empty($arrRubrics)) {
    foreach ($arrRubrics as $k => $rubric) {
        $strSqlQuery = "SELECT * FROM `site_training_rubrics` WHERE `str_parent_id` = " . $rubric['str_id']." ORDER BY `str_id`";
        $arrSubRubrics = $objDb->fetchall($strSqlQuery);

        if (is_array($arrSubRubrics) && !empty($arrSubRubrics)) {
            foreach ($arrSubRubrics as $kk => $subrubric) {
                $arrSubRubrics[$kk]['intSubRubricID'] = (int) $subrubric['str_id'];
                $arrSubRubrics[$kk]['subselected'] = $subrubric['str_id'] == $intRubricID ? 'selected' : '';
                $arrSubRubrics[$kk]['strSubRubricName'] = htmlspecialchars($subrubric['str_rubricname']);
            }
        }

        $objTpl->tpl_loop($arrTplVars['module.name'], "sub.rubrics.".$rubric['str_id'], $arrSubRubrics);
    }
}

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);

