<?php
$arrTplVars['module.name'] = "templates";
$arrTplVars['module.title'] = "ШАБЛОНЫ ПРОЕКТОВ";
$arrTplVars['module.child'] = "templates.edit";

// ***** Сохранение редактируемого шаблона **********************************************
if ($_POST['frmTemplateEdit']=='true' && intval($_POST['idTemplate'])!=0) {
  $dataForSQL['tplName'] = addslashes(trim($_POST['tplName']));
  $dataForSQL['tplBody'] = addslashes(trim($_POST['tplBody']));
  $dataForSQL['tplPublish'] = ($_POST['tplPublish']=='on') ? "NOW()" : "NULL";

  if(!$objDb->query("UPDATE site_templates SET t_name='".$dataForSQL['tplName']."', t_body='".$dataForSQL['tplBody']."', t_publish=".$dataForSQL['tplPublish']." WHERE t_id='".intval($_POST['idTemplate'])."'")) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
  } else {
    $GLOBALS['manCodeError'][]['code'] = '100tpl';

    if($_POST['tplPublish']=='on') {
      $objFile->writeFile($objTpl->parseFormVars(trim($_POST['tplBody']), "out"), "site.global.".intval($_POST['idTemplate']).".tpl", SITE_TPL_TPL_DIR );
      $_errMess = '101tpl';
    } else {
      $_errMess = '100tpl';
    }

    if ($_POST['tplReturn']=='on') {
      header("location: ".$arrTplVars['module.name']."?action=edit&tpl=".intval($_POST['idTemplate'])."&errMess=".$_errMess."&pageReturn=true");
    } else {
      header("location: ".$arrTplVars['module.name']."?errMess=".$_errMess);
    }
    exit();
  }
}
// **************************************************************************************

// ***** Обработка ошибок для вывода ****************************************************
${"error".$errSuf} = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
${"error".$errSuf} = $objUtils->echoMessage($error, $GLOBALS['manStatusError']);
// **************************************************************************************

$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

$strSqlQuery = "SELECT * FROM site_templates WHERE st_project=".$_SESSION['intIdDefaultProject'];
$arrLstTemplates = $objDb->fetchall( $strSqlQuery ); // Выбираем шаблоны

if(is_array($arrLstTemplates)) { // Если есть шаблоны в базе
    foreach($arrLstTemplates as $key=>$arrTemplate) {
        $arrLstTemplates[$key]['status_checked'] = ($arrTemplate['st_status']=='Y') ? ' checked' : '';

        $strFileContent = $objFile->readFile($_SESSION['arrInfoAccessProject'][$_SESSION['intIdDefaultProject']]['strPathProject'].REL_TPL_PATH."tpl/site.global.{$arrTemplate['st_id']}.tpl");
    }

    $arrIf['lst.templates'] = true;
} else {
    $arrIf['lst.empty'] = true; // Если нет шаблонов в базе
}

$objTpl->tpl_loop($arrTplVars['module.name'], "lst.templates", $arrLstTemplates, array($_SESSION['userConfig']['cBgColor'], $_SESSION['userConfig']['cThirdColor']));

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
