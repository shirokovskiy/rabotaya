<?php
$arrTplVars['module.name'] = "resumes";
$arrTplVars['module.child'] = "resume.form";
$arrTplVars['module.title'] = "Резюме";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

$SiteUser = new SiteUser();

/**
 * Удаление
 */
if ( intval($_GET['id']) > 0 && $_GET['act'] == 'del' ) {
    $intRecordId = intval($_GET['id']);
    $strSqlQuery = "DELETE FROM job_resumes WHERE jr_id = ".$intRecordId;
    if ( !$objDb->query($strSqlQuery) ) {
        #mysql error
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
    } else {
        header("Location: {$arrTplVars['module.name']}?id=$intRecordId&stPage={$arrTplVars['stPage']}");
        exit();
    }
}

// ***** Обработка ошибок для вывода *******************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// *****************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

/**
 * Подготовка сессионного поиска
 */
if ( isset($_POST['frmSearch']) ) {
    $_SESSION['frmSearch'] = $_POST;
}

if ( !isset($_POST['frmSearch']) && intval($_GET['stPage']) <= 0 && !isset($_GET['export'])) {
    unset($_SESSION['frmSearch']);
}

if ( isset($_SESSION['frmSearch']) ) {
    if ( intval( $_SESSION['frmSearch']['intID2Find'] )>0 ) {
        $intFindID = $arrTplVars['intID2Find'] = $_SESSION['frmSearch']['intID2Find'];
        $arrSqlWhere[] = " jr_id = '$intFindID' ";
    }
    if ( !empty( $_SESSION['frmSearch']['strTitle2Find'] ) ) {
        $strFindName = $arrTplVars['strTitle2Find'] = mysql_real_escape_string( $_SESSION['frmSearch']['strTitle2Find'] );
        $arrSqlWhere[] = " jr_title LIKE '%$strFindName%' ";
    }
    if ( !empty( $_SESSION['frmSearch']['strCity2Find'] ) ) {
        $strFindCity = $arrTplVars['strCity2Find'] = mysql_real_escape_string( $_SESSION['frmSearch']['strCity2Find'] );
        $cityID = $SiteUser->getCityId($strFindCity);
        if ($cityID <= 0) {
            $arrSqlWhere[] = " jr_city LIKE '%$strFindCity%' ";
        } else {
            $arrSqlWhere[] = " (jr_city LIKE '%$strFindCity%' OR jr_city_id = $cityID)";
        }
    }
    if ( !empty( $_SESSION['frmSearch']['strEmail2Find'] ) ) {
        $strFindEmail = $arrTplVars['strEmail2Find'] = mysql_real_escape_string( $_SESSION['frmSearch']['strEmail2Find'] );
        $arrSqlWhere[] = " jr_email LIKE '%$strFindEmail%' ";
    }
    if ( !empty( $_SESSION['frmSearch']['strPhone2Find'] ) ) {
        $strFindPhone = $arrTplVars['strPhone2Find'] = mysql_real_escape_string( $_SESSION['frmSearch']['strPhone2Find'] );
        $arrSqlWhere[] = " jr_phone LIKE '%$strFindPhone%' ";
    }
}

if ( is_array($arrSqlWhere) && !empty($arrSqlWhere) ) {
    $strSqlWhere = implode(' AND ', $arrSqlWhere);
}

if (isset($_GET['export']) && $_GET['export'] == 'csv') {
    /**
     * Выборка для экспорта
     */
    $strSqlQuery = "SELECT * FROM job_resumes".( !empty( $strSqlWhere ) ? " WHERE ".$strSqlWhere : '' )." ORDER BY jr_id DESC LIMIT 10000";
    $arrRecords = $objDb->fetchall( $strSqlQuery );

    if (is_array($arrRecords) && !empty($arrRecords)) {
        $strFileNameInfo = "resume.search.results.csv";
        $SourceFile = PRJ_FILES.$strFileNameInfo;
        $fp = fopen($SourceFile, "w");

        foreach ($arrRecords as $value) {
            if (!empty($value['jr_fio']) && !empty($value['jr_phone'])) {
                $phone = preg_replace('/-|\)|\(|раб|моб|\s/', '', $value['jr_phone']);
                fwrite($fp, '"'.$value['jr_fio'].'", "'.$phone.'", "'.$value['jr_email'].'"'. "\n");
            }
        }

        fclose($fp);

        if (file_exists($SourceFile)) {
            $sizeResFile = filesize($SourceFile);

            Header( "Expires: Mon, 5 Jul 1977 09:30:00 GMT\r\n" );
            Header( "Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT\r\n" );
            Header( "Cache-Control: no-store, no-cache, must-revalidate\r\n" );
            Header( "Cache-Control: post-check=0, pre-check=0, false\r\n" );
            Header( "Pragma: no-cache\r\n" );
            Header( "HTTP/1.1 200 OK\r\n" );

            Header( "Content-Disposition: attachment; filename=".date('Y.m.d_H.i_')."$strFileNameInfo\r\n" );
            Header( "Accept-Ranges: bytes\r\n" );
            Header( "Content-Type: application/force-download" );
            Header( "Content-Length: $sizeResFile\r\n\r\n" );

            readfile( $SourceFile );
            die();
        } else {
            header ("HTTP/1.0 404 Not Found");
            $arrTplVars['strMessage'] = "Ошибка! Файл не существует.";
        }
    }
}




// Всего записей в базе
$strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM job_resumes";
$arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');
// Выбрано записей
$strSqlQuery = "SELECT COUNT(*) AS intQuantitySelectRecords FROM job_resumes".( !empty( $strSqlWhere ) ? " WHERE ".$strSqlWhere : '' );
$arrTplVars['intQuantitySelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantitySelectRecords');
// ***** BEGIN: Построение пейджинга для вывода списка
$objPagination = new tplPaginator($arrTplVars['intQuantitySelectRecords'], SITE_TPL_TPL_DIR);
/**
 * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
 * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
 */
$objPagination->strPaginatorTpl = "site.global.1.tpl";
$objPagination->intQuantLinkPage = 15;
$objPagination->intQuantRecordPage = 15;
$objPagination->strColorLinkStyle = "link-b";
$objPagination->strColorActiveStyle = "tab-bl-b";
$objPagination->strNameImageBack = "arrow_one_left.gif";
$objPagination->strNameImageForward = "arrow_one_right.gif";
// Создаем блок пэйджинга
$objPagination->paCreate();
// ***** END: Построение пейджинга для вывода списка
// Запрос для выборки нужных записей
$strSqlQuery = "SELECT * FROM job_resumes".( !empty( $strSqlWhere ) ? " WHERE ".$strSqlWhere : '' )." ORDER BY jr_id DESC ".$objPagination->strSqlLimit;
$arrRecords = $objDb->fetchall( $strSqlQuery );
// кол-во публикаций показанных на странице
$arrTplVars['intQuantityShowRecOnPage'] = ( !empty($arrRecords) ? count($arrRecords) : 0 );
// Присвоение значения пэйджинга для списка
$arrTplVars['blockPaginator'] = $objPagination->paShow();

if ( is_array($arrRecords) ) {
    foreach ( $arrRecords as $key => $value ) {
        $arrRecords[$key]['strDatePublish'] = (!empty($value['jr_date_publish']) ? date( "d.m.Y", $value['jr_date_publish']) : '&mdash;');
        $arrRecords[$key]['strFIO'] = htmlspecialchars($value['jr_fio']);
//        $arrRecords[$key]['strDateChange'] = !empty($value['jr_date_change']) ? date( "d.m.Y H:i", strtotime($value['jr_date_change'])) : '';
        $arrRecords[$key]['cbxStatus'] = $value['jr_status'] == 'Y' ? ' checked' : '';
        $arrRecords[$key]['cbxGrab'] = $value['jr_grab_src_id'] > 0 ? ' checked' : '';
        $arrRecords[$key]['strSourceGrab'] = $value['jr_grab_src_alias'] == 'jbfn' ? ' JobFine' : '';
//        $arrIf['attach.'.$value['jr_id']] = ( file_exists(PRJ_IMAGES."resumes/".$resumeFoto) && $value['jr_image'] == 'Y');
    }
    $arrIf['is.result'] = isset($_SESSION['frmSearch']);
}

$objTpl->tpl_loop($arrTplVars['module.name'], "records", $arrRecords);
$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);

