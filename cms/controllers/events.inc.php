<?php
$arrTplVars['module.name'] = "events";
$arrTplVars['module.child'] = "event.form";
$arrTplVars['module.title'] = "События";

include_once "models/Events.php";
$objEvents = new Events();

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

// ***** Обработка ошибок для вывода *******************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// *****************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

// Всего записей в базе
$strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM `site_events`";
$arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');
// Выбрано записей
$strSqlQuery = "SELECT COUNT(*) AS intQuantitySelectRecords FROM `site_events`";
$arrTplVars['intQuantitySelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantitySelectRecords');
// ***** BEGIN: Построение пейджинга для вывода списка
$objPagination = new tplPaginator($arrTplVars['intQuantitySelectRecords'], SITE_TPL_TPL_DIR);
/**
 * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
 * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
 */
$objPagination->strPaginatorTpl = "site.global.1.tpl";
$objPagination->intQuantRecordPage = 15;
$objPagination->strColorLinkStyle = "link-b";
$objPagination->strColorActiveStyle = "tab-bl-b";
$objPagination->strNameImageBack = "arrow_one_left.gif";
$objPagination->strNameImageForward = "arrow_one_right.gif";
// Создаем блок пэйджинга
$objPagination->paCreate();
// ***** END: Построение пейджинга для вывода списка
// Запрос для выборки нужных записей
$strSqlQuery = "SELECT * FROM `site_events` ORDER BY se_id DESC ".$objPagination->strSqlLimit;
$arrNews = $objDb->fetchall( $strSqlQuery );
// кол-во публикаций показанных на странице
$arrTplVars['intQuantityShowRecOnPage'] = ( !empty($arrNews) ? count($arrNews) : 0 );
// Присвоение значения пэйджинга для списка
$arrTplVars['blockPaginator'] = $objPagination->paShow();

if ( is_array($arrNews) ) {
    foreach ( $arrNews as $key => $value ) {
        $arrNews[$key]['strDatePubl'] = date( "d.m.Y", strtotime($value['se_date_publ']));
        $arrNews[$key]['strDateAdd'] = date( "d.m.Y H:i", strtotime($value['se_date_add']));
        $arrNews[$key]['strDateChange'] = !empty($value['se_date_change']) ? date( "d.m.Y H:i", strtotime($value['se_date_change'])) : '';
        $arrNews[$key]['cbxStatus'] = $value['se_status'] == 'Y' ? ' checked' : '';
        $arrIf['attach.'.$value['se_id']] = ( file_exists(DROOT."storage/files/images/events/events.photo.".$value['se_id'].".jpg") && $objEvents->getEventImage($value['se_id'])!==false);
    }
}

$objTpl->tpl_loop($arrTplVars['module.name'], "events", $arrNews);

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);

