<?php
$arrTplVars['module.name'] = "fragments.edit";
$arrTplVars['module.title'] = "Редактирование фрагмента";
$arrTplVars['module.parent'] = "fragments";

require_once(SITE_LIB_DIR.'cls.fragments.php'); // Подключаем сопутствующий класс для работы с фрагментами страниц и шаблонов

$arrTplVars['intIdItem'] = intval($_GET['idItem']); // ID каталога фрагмента
$arrTplVars['intIdFragment'] = intval($_GET['frg']); // ID редактируемого фрагмента
$arrTplVars['cbxReturn_checked'] = ($_GET['strReturn']=='true') ? ' checked' : '';

/**
 * Обработка удаления фрагмента страницы
 */
if ( $_GET['action']=='delete' ) {
    $arrDataForSql['intIdFragment'] = intval($_GET['frg']);
    if($arrDataForSql['intIdFragment']>0) {
        $strSqlQuery = "SELECT * FROM site_fragments WHERE sf_id='".$arrDataForSql['intIdFragment']."'";
        $arrInfoDeleteFrg = $objDb->fetch( $strSqlQuery );
        if(is_array($arrInfoDeleteFrg)) {

            $strSqlQuery = "DELETE FROM site_fragments WHERE sf_id='".$arrDataForSql['intIdFragment']."'";
            if(!$objDb->query($strSqlQuery)) {
                $GLOBALS['manStatusError']=1;
                $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
            } else {
                $GLOBALS['manCodeError'][1]['code'] = '109frg';
                unlink($_SESSION['arrInfoAccessProject'][$_SESSION['intIdDefaultProject']]['strPathProject'].REL_TPL_PATH."fragments/".$arrInfoDeleteFrg['sf_id_name'].".frg");
                unlink($_SESSION['arrInfoAccessProject'][$_SESSION['intIdDefaultProject']]['strPathProject'].REL_PHP_PATH.'fragments/'.$arrInfoDeleteFrg['sf_id_name'].".inc.php");
                header("location: ".$arrTplVars['module.parent'].'?idItem='.$_GET['idItem'].'&errMess='.$GLOBALS['manCodeError'][1]['code']);
                exit();
            }

        } else {
            $GLOBALS['manCodeError'][1]['code'] = '110frg';
            header("location: ".$arrTplVars['module.parent'].'?idItem='.$_GET['idItem'].'&errMess='.$GLOBALS['manCodeError'][1]['code']);
            exit();
        }
    }
}
// *************************************************************************************************


/** >>************************************************************************\
 ** Thu Nov 16 00:08:18 MSK 2006, Shirokovskiy Dmitry aka Jimmy™
 * Сохранение фрагмента **/
if ($_POST['frmFragmentAddEdit']=='true') {
    $arrDataForSql['intIdFragment'] = $arrTplVars['intIdFragment'] = intval($_POST['intIdFragment']);

    $arrDataForSql['strNameFrg'] = addslashes(trim($_POST['strNameFrg']));
    $arrTplVars['strNameFrg'] = htmlspecialchars(trim($_POST['strNameFrg']));

    $strOriginalFrgName = trim($_POST['strUNameFrg']);
    $arrDataForSql['strUNameFrg'] = mysql_real_escape_string($strOriginalFrgName);
    $arrTplVars['strUNameFrg'] = htmlspecialchars(trim($_POST['strUNameFrg']));

    if ($objUtils->checkBadChars($strOriginalFrgName) ) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgIncorrectFrgAlias';
    }

    $strBody = trim($_POST['strBody']); // Тело для сохранения в файл
    $strBody = str_replace("&#123", "{", $strBody);
    $strBody = str_replace("&#125", "}", $strBody);
    $strBody = str_replace("\r\n", "\n", $strBody);

    $arrDataForSql['strBody'] = mysql_real_escape_string($strBody);
    $arrTplVars['strBody'] = htmlspecialchars($strBody);

    $arrDataForSql['strStatusFrg'] = ($_POST['cbxStatusFrg']=='on') ? 'Y' : 'N';
    $arrTplVars['cbxStatusFrg_checked'] = ($_POST['cbxStatusFrg']=='on') ? ' checked' : '';

    $arrDataForSql['strDescFrg'] = addslashes(trim($_POST['strDescFrg']));
    $arrTplVars['strDescFrg'] = htmlspecialchars(trim($_POST['strDescFrg']));

    $arrDataForSql['intFrgGroup'] = $arrTplVars['intFrgGroup'] = intval($_POST['intFrgGroup']);

    $dataForSQL['strPublishFrg'] = ($_POST['cbxPublish']=="on") ? "NOW()" : "NULL";

    $arrTplVars['cbxReturn_checked'] = ($_POST['cbxReturn']=='on') ? ' checked' : '';
    $arrTplVars['cbxPublish_checked'] = ($_POST['cbxPublish']=='on') ? ' checked' : '';

    /**
     * Проверка обязательных полей для сохранения фрагмента страниц и шаблонов
     */
    if (empty($arrDataForSql['strNameFrg'])) { // Если не введено обязательное поле "Название фрагмента"
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = '100frg';
    }

    if (empty($arrDataForSql['strUNameFrg'])) {  // Если не введено обязательное поле "Уникальное имя фрагмента"
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = '106frg';
    } else {
        if(!preg_match("/[A-z0-9\._-]+/", $arrDataForSql['strUNameFrg'])) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = '112frg';
        }

        if ($_POST['strUNameFrgSaved']!=$arrDataForSql['strUNameFrg']) {
            /**
             * Проверяем нет ли фрагмента с таким именем
             */
            $idExistsFragment = $objDb->fetch("SELECT sf_id FROM site_fragments WHERE sf_id_name='".$arrDataForSql['strUNameFrg']."' AND sf_id_project='".$_SESSION['intIdDefaultProject']."'", 'sf_id');
            if($idExistsFragment>0) {
                $GLOBALS['manStatusError']=1;
                $GLOBALS['manCodeError'][]['code'] = '107frg';
            }
        }
    }

    if (empty($arrDataForSql['strBody'])) { // Если тело фрагмента пустое
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = '108frg';
    }

    if ($arrDataForSql['intFrgGroup']==0) { // Если группа фрагмента не выбрана
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = '111frg';
    }

    if ($GLOBALS['manStatusError']!=1 ) {
        $sqlFields = ""
            ." sf_id_parent    ='".$arrDataForSql['intFrgGroup']."'"
            .", sf_id_project  ='".$_SESSION['intIdDefaultProject']."'"
            .", sf_name        ='".$arrDataForSql['strNameFrg']."'"
            .", sf_id_name     ='".$arrDataForSql['strUNameFrg']."'"
            .", sf_body        ='".$arrDataForSql['strBody']."'"
            .", sf_desc        ='".$arrDataForSql['strDescFrg']."'"
            .", sf_status      ='".$arrDataForSql['strStatusFrg']."'"
            .", sf_publish     =".$dataForSQL['strPublishFrg']
        ;
        if ( empty($arrDataForSql['intIdFragment']) ) {
            $strSqlQuery = "INSERT INTO site_fragments SET $sqlFields";
        } else {
            $strSqlQuery = "UPDATE site_fragments SET $sqlFields WHERE sf_id=".$arrDataForSql['intIdFragment'];
        }

        if(!$objDb->query($strSqlQuery)) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
        } else {
            /**
             * Пишем обработчик фрагмента (UID-фрагмента.inc.php)
             */
            if (empty($arrDataForSql['intIdFragment'])) {
                $objFrg = new clsFragments($arrDataForSql['strUNameFrg'], $arrDataForSql['strNameFrg']);
                $objFrg->savePHPFile();
            } else {
                if($_POST['strUNameFrgSaved']<>$arrDataForSql['strUNameFrg']) {
                    $objFrg = new clsFragments($_POST['strUNameFrgSaved'], $arrDataForSql['strNameFrg']);
                    $objFrg->getContentPHPHandler();
                    $objFrg->replaceNameInPHPContent($_POST['strUNameFrgSaved'], $arrDataForSql['strUNameFrg']);
                    $objFrg->deletePHPFile();
                    $objFrg->setFrgName($arrDataForSql['strUNameFrg']);
                    $objFrg->savePHPFile();
                } else {
                    $objFrg = new clsFragments($arrDataForSql['strUNameFrg'], $arrDataForSql['strNameFrg']);
                    if(!file_exists($objFrg->frgPHPPath.$arrDataForSql['strUNameFrg'].'.inc.php')) {
                        $objFrg->savePHPFile();
                    }
                }
            }

            // ***** Пишем тело фрагмента (UID-фрагмента.frg) ****************************************************
            // * Если при сохранении фрагмента атрибут "применить изменения в
            // * фрагменте к сайту" установлен в значение "on", то сохраняем
            // * тело фрагмента в файл, иначе - не сохраняем и удаляем существующий
            // * файл-шаблон фрагмента.
            if ( $_POST['cbxPublish']=='on' && $_POST['cbxStatusFrg']=='on' ) {
                if($arrDataForSql['intIdFragment']==0) {
                    $objFrg->frgHTMLContentFile = $strBody;
                    $objFrg->saveHTMLFile();
                } else {
                    if($_POST['strUNameFrgSaved']<>$arrDataForSql['strUNameFrg']) {
                        $objFrg->frgHTMLContentFile = $strBody;
                        $objFrg->setFrgName($_POST['strUNameFrgSaved']);
                        $objFrg->deleteHTMLFile();
                        $objFrg->setFrgName($arrDataForSql['strUNameFrg']);
                        $objFrg->saveHTMLFile();
                    } else {
                        $objFrg->frgHTMLContentFile = $strBody;
                        $objFrg->saveHTMLFile();
                    }
                }
            } else if($_POST['cbxStatusFrg']!='on') { // Если фрагмент не доступен, то...
                if($_POST['strUNameFrgSaved']<>$arrDataForSql['strUNameFrg']) { // .. проверяем не изменяется ли его UID, если да, то удаляем старый файл-шаблон фрагмента (че мусорить то?)
                    $objFrg->setFrgName($_POST['strUNameFrgSaved']);
                    $objFrg->deleteHTMLFile();
                } else { // ... удаляем файл-шаблон фрагмента
                    $objFrg->deleteHTMLFile();
                }
            }
            //***************************************************************************************************

            if ($arrDataForSql['intIdFragment']==0) // Если добавляется новый фрагмент, то берем ID вставленной записи
                $arrDataForSql['intIdFragment'] = $objDb->insert_id();

            if ($_POST['cbxPublish']=='on')
                $GLOBALS['manCodeError'][1]['code'] = '102frg';
            else
                $GLOBALS['manCodeError'][1]['code'] = '101frg';

            $arrTplVars['intIdItem'] = $arrDataForSql['intFrgGroup'];

            if ($_POST['cbxReturn']=='on') {
                header("location: ".$arrTplVars['module.name']."?idItem=".$arrTplVars['intIdItem']."&frg=".$arrDataForSql['intIdFragment']."&errMess=".$GLOBALS['manCodeError'][1]['code']."&strReturn=true");
            } else {
                header("location: ".$arrTplVars['module.parent']."?idItem=".$arrTplVars['intIdItem']."&errMess=".$GLOBALS['manCodeError'][1]['code']);
            }
            exit();
        }
    }
}
// **************************************************************************************


// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars["error".$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars["error".$errSuf] = $objUtils->echoMessage($arrTplVars["error".$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

/**
 * Получаем информацию о выбранном фрагменте если существует ID и нет ошибки сохранения
 */
if ( $arrTplVars['intIdFragment']>0 && $GLOBALS['manStatusError']!=1 ) {
    $arrInfoFragment = $objDb->fetch("SELECT * FROM site_fragments WHERE sf_id=".intval($arrTplVars['intIdFragment']));
    if ( is_array($arrInfoFragment) ) {
        $arrIf['block.fragment.edit'] = true;
        $arrTplVars['strNameFrg'] = htmlspecialchars(stripslashes($arrInfoFragment['sf_name']));
        $arrTplVars['strUNameFrg'] = $arrInfoFragment['sf_id_name'];
        $arrTplVars['strUNameFrgSaved'] = $arrInfoFragment['sf_id_name'];

        $strBodyOfMySQL = htmlspecialchars($arrInfoFragment['sf_body']);
        $strBodyOfMySQL = str_replace("{", "&#123", $strBodyOfMySQL);
        $strBodyOfMySQL = str_replace("}", "&#125", $strBodyOfMySQL);
        $arrTplVars['strBody'] = $strBodyOfMySQL;

        $intIdGroupSelected = $arrInfoFragment['sf_id_parent'];
        $arrTplVars['cbxStatusFrg_checked'] = ($arrInfoFragment['sf_status']=='Y') ? ' checked' : '';
        $arrTplVars['strDescFrg'] = htmlspecialchars(stripslashes($arrInfoFragment['sf_desc']));

        // ***** Проверяем опубликован ли фрагмент после последнего изменения, если нет, вывдим информационное сообщение.
        if($arrInfoFragment['sf_publish']===NULL) {
            $arrTplVars['strNoPublishMessage'] = $errMsg['150frg'];
            $arrIf['message.noPublish.yes'] = true;
            $arrTplVars['strLastPublishDate'] = '-';
        } else {
            $arrTplVars['strLastPublishDate'] = $objUtils->workDate(4, $arrInfoFragment['sf_publish']);
        }

        // ***** Проверяем идентичность фрагмента хранимого в базе(оригинал) и на диске *********
        $path = $_SESSION['arrInfoAccessProject'][$_SESSION['intIdDefaultProject']]['strPathProject'].REL_TPL_PATH.'fragments/';

        if(file_exists($path.$arrTplVars['strUNameFrg'].".frg")) {
            $strFileContent = $objFile->readFile($path.$arrTplVars['strUNameFrg'].".frg");

            $strFileContent = htmlspecialchars($strFileContent);
            $strFileContent = str_replace("{", "&#123", $strFileContent);
            $strFileContent = str_replace("}", "&#125", $strFileContent);
            $arrTplVars['strBody'] = $arrTplVars['strFileContent'] = $strFileContent;
        }
        // **************************************************************************************
    }
} else {
    $arrIf['block.fragment.add'] = true;
}

if(empty($arrTplVars['strNameFrg'])) {
    $arrTplVars['strUNameFrg'] = (int)$objDb->fetch("SELECT max(sf_id) AS maxId FROM site_fragments", "maxId")+1;
    $arrTplVars['strUNameFrg'] = 'fragment.'.$arrTplVars['strUNameFrg'];
}

// **** Выбираем группы фрагментов ******************************************************
if (empty($intIdGroupSelected))
    $intIdGroupSelected = intval($_POST['intFrgGroup']);

$strSqlQuery = "SELECT * FROM site_fragments_group WHERE sfg_status='Y' AND sfg_id_project=".$_SESSION['intIdDefaultProject'];
$arrListGroup = $objDb->fetchall($strSqlQuery);
foreach($arrListGroup as $key=>$value) {
    if(empty($arrListGroup[$key]['sfg_parent'])) {
        $arrListGroupForm[$key]['name'] = $arrListGroup[$key]['sfg_name'];
        $arrListGroupForm[$key]['id'] = $arrListGroup[$key]['sfg_id'];
    } else {
        $arrListSubGroupForm[$arrListGroup[$key]['sfg_parent']][] = array('id'=>$arrListGroup[$key]['sfg_id'], 'name'=>stripslashes($arrListGroup[$key]['sfg_name']));
    }
}


foreach($arrListGroupForm as $key=>$value) {
    if($intIdGroupSelected == $arrListGroupForm[$key]['id']) // Если эта группа была выбрана, ставим ей "selected"
        $arrListGroupToTpl[] = array('intIdGroup'=>$arrListGroupForm[$key]['id'], 'strNameGroup'=>$arrListGroupForm[$key]['name'], 'intIdGroup_selected'=>' selected');
    else
        $arrListGroupToTpl[] = array('intIdGroup'=>$arrListGroupForm[$key]['id'], 'strNameGroup'=>$arrListGroupForm[$key]['name']);

    if(count($arrListSubGroupForm[$arrListGroupForm[$key]['id']])>0) {
        foreach($arrListSubGroupForm[$arrListGroupForm[$key]['id']] as $kkey=>$vvalue) {
            if($intIdGroupSelected == $arrListSubGroupForm[$arrListGroupForm[$key]['id']][$kkey]['id']) // Если эта группа была выбрана, ставим ей "selected"
                $arrListGroupToTpl[] = array('intIdGroup'=>$arrListSubGroupForm[$arrListGroupForm[$key]['id']][$kkey]['id'], 'strNameGroup'=>'--> '.$arrListSubGroupForm[$arrListGroupForm[$key]['id']][$kkey]['name'], 'intIdGroup_selected'=>' selected');
            else
                $arrListGroupToTpl[] = array('intIdGroup'=>$arrListSubGroupForm[$arrListGroupForm[$key]['id']][$kkey]['id'], 'strNameGroup'=>'--> '.$arrListSubGroupForm[$arrListGroupForm[$key]['id']][$kkey]['name']);
        }
    }
}

$objTpl->tpl_loop($arrTplVars['module.name'], "lst.fragment.group", $arrListGroupToTpl);
// **************************************************************************************
$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
