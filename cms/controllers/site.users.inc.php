<?php
$arrTplVars['module.name'] = "site.users";
$arrTplVars['module.name.child'] = "site.user.form";
$arrTplVars['module.title'] = "Пользователи сайта";

include_once "models/SiteUser.php";
$objSiteUser = new SiteUser($_db_config);

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

// ***** Обработка ошибок для вывода *******************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// *****************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

/**
 * Подготовка сессионного поиска
 */
if ( isset($_POST['frmSearch']) ) {
    $_SESSION['frmSearch'] = $_POST;
}

if ( !isset($_POST['frmSearch']) && intval($_GET['stPage']) <= 0 ) {
    unset($_SESSION['frmSearch']);
}

if ( isset($_SESSION['frmSearch']) ) {
    if ( intval( $_SESSION['frmSearch']['intID2Find'] )>0 ) {
        $intFindID = $arrTplVars['intID2Find'] = $_SESSION['frmSearch']['intID2Find'];
        $arrSqlWhere[] = "su_id = ".$intFindID;
    }
    if ( !empty( $_SESSION['frmSearch']['strFio2Find'] ) ) {
        $strFindName = $arrTplVars['strFio2Find'] = mysql_real_escape_string( $_SESSION['frmSearch']['strFio2Find'] );
        $arrSqlWhere[] = "(su_lname LIKE '%$strFindName%' OR su_fname LIKE '%$strFindName%' OR su_mname LIKE '%$strFindName%' )";
    }
    if ( !empty( $_SESSION['frmSearch']['strEmail2Find'] ) ) {
        $strFindEmail = $arrTplVars['strEmail2Find'] = mysql_real_escape_string( $_SESSION['frmSearch']['strEmail2Find'] );
        $arrSqlWhere[] = "su_login LIKE '%$strFindEmail%'";
    }
    if ( !empty( $_SESSION['frmSearch']['strPhone2Find'] ) ) {
        $strFindPhone = $arrTplVars['strPhone2Find'] = mysql_real_escape_string( $_SESSION['frmSearch']['strPhone2Find'] );
        $arrSqlWhere[] = "su_phone LIKE '%$strFindPhone%'";
    }
}

if ( is_array($arrSqlWhere) && !empty($arrSqlWhere) ) {
    $strSqlWhere = implode(' AND ', $arrSqlWhere);
}

$strSqlQuery = "SELECT COUNT(*) AS quantity FROM site_users";
$arrTplVars['strQuantityAllRecords'] = $objDb->fetch( $strSqlQuery , 'quantity');

$strSqlQuery = "SELECT COUNT(*) AS quantity FROM site_users".( !empty( $strSqlWhere ) ? " WHERE ".$strSqlWhere : '' );
$arrTplVars['intQuantitySelectRecords'] = $objDb->fetch( $strSqlQuery , 'quantity');

/**
 * Построение пейджинга для вывода списка записей полученных из БД
 */
$objPaginator = new tplPaginator($arrTplVars['intQuantitySelectRecords'], SITE_TPL_TPL_DIR);
/**
 * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
 * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
 */
$objPaginator->strPaginatorTpl = "site.global.1.tpl";
$objPaginator->intQuantRecordPage = 20;
// Создаем блок пэйджинга
$objPaginator->paCreate();

/**
 * Выбираем записи из БД с учетом постраничного вывода и заполняем массив для вывода в шаблон
 */
$strSqlQuery = "SELECT stU.*, si_filename FROM `site_users` stU LEFT JOIN `site_images` ON (si_rel_id = su_id AND si_type = 'users')".( !empty( $strSqlWhere ) ? " WHERE ".$strSqlWhere : '' )." ORDER BY `su_id` DESC ".$objPaginator->strSqlLimit;
$arrSelectedRecords = $objDb->fetchall( $strSqlQuery );
$arrTplVars['intQuantityShowRecOnPage'] = ( !empty($arrSelectedRecords) ? count($arrSelectedRecords) : 0 ); // кол-во строк показанных на странице

if (is_array($arrSelectedRecords)) {
    foreach($arrSelectedRecords as $key=>$value) {
        $arrSelRecForTpl[$key]['intId'] = $value['su_id'];
        $arrSelRecForTpl[$key]['strFIO'] = $value['su_lname'].' '.$value['su_fname'].' '.$value['su_pname'];
        $arrSelRecForTpl[$key]['strLogin'] = $value['su_login'];
        $arrSelRecForTpl[$key]['strLastEntry'] = ($value['su_last_visit']===NULL) ? 'не определено' : $objUtils->workDate(4, $value['su_last_visit']);
        $arrIf['attach.'.$value['su_id']] = ( file_exists(DROOT."storage/files/images/users/".$value['si_filename']) && $objSiteUser->getImage($value['su_id'])!==false);
    }
}
$arrTplVars['blockPaginatorTop'] = ($arrTplVars['intQuantitySelectRecords'] > 20?$objPaginator->paShow():"") ;
$arrTplVars['blockPaginator'] = $objPaginator->paShow();

$objTpl->tpl_loop($arrTplVars['module.name'], "users", $arrSelRecForTpl, array($arrTplVars['cBgColor'], $arrTplVars['cThirdColor']));

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);

