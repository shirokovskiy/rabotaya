<?php
$arrTplVars['module.name'] = "vacancy.form";
$arrTplVars['module.parent'] = "vacancies";
$arrTplVars['module.title'] = "Новая вакансия";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);
if (!isset($SiteUser)) {
    $SiteUser = new SiteUser();
}

/**
 * Запись данных
 */
if ( isset($_POST['frmEdit']) && $_POST['frmEdit'] == 'true' ) {
    $intRecordId = $arrSqlData['intRecordId'] = intval(trim($_POST['intRecordId']));
    $arrTplVars['intRecordId'] = ( $arrSqlData['intRecordId'] > 0 ? $arrSqlData['intRecordId'] : '');

    $intCompanyId = $arrSqlData['intCompanyId'] = intval(trim($_POST['intCompanyId']));
    $arrTplVars['intCompanyId'] = ( $arrSqlData['intCompanyId'] > 0 ? $arrSqlData['intCompanyId'] : '');

    $arrSqlData['strTitle'] = mb_ucfirst(mysql_real_escape_string(trim($_POST['strTitle'])));
    $arrTplVars['strTitle'] = mb_ucfirst(htmlspecialchars(stripslashes(trim($_POST['strTitle']))));

    if (empty($arrSqlData['strTitle']) ) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgEmptyTitle';
    }

    $arrSqlData['strSalaryFrom'] = intval(trim($_POST['strSalaryFrom']));
    $arrTplVars['strSalaryFrom'] = ($arrSqlData['strSalaryFrom'] > 0 ? $arrSqlData['strSalaryFrom'] : '');

    $arrSqlData['strSalaryTo'] = intval(trim($_POST['strSalaryTo']));
    $arrTplVars['strSalaryTo'] = ($arrSqlData['strSalaryTo'] > 0 ? $arrSqlData['strSalaryTo'] : '');

    $arrSqlData['strCity'] = mysql_real_escape_string(trim($_POST['strCity']));
    $arrTplVars['strCity'] = htmlspecialchars(stripslashes(trim($_POST['strCity'])));

    $cityId = $SiteUser->getCityId(trim($_POST['strCity']));
    if (empty($cityId)) {
        $cityId = $SiteUser->saveCity(trim($_POST['strCity']));
    }

    $rbSex = $_POST['rbSex'];
    $arrTplVars['jv_sex_'.$rbSex] = " checked";

    $rbEdu = intval($_POST['rbEdu']);

    $rbWorkbusy = $arrRecord['jv_jw_id'] = intval($_POST['rbWorkbusy']);

    $cbxDriver = $_POST['cbxDriver'];
    $arrTplVars['jv_driver_'.$cbxDriver] = " checked";

    $cbxTrips = $_POST['cbxTrips'];
    $arrTplVars['jv_trips_'.$cbxTrips] = " checked";

    $arrSqlData['strDescription'] = mysql_real_escape_string(trim($_POST['strDescription']));
    $arrTplVars['strDescription'] = htmlspecialchars(stripslashes(trim($_POST['strDescription'])));

    $arrSqlData['strContactPerson'] = mysql_real_escape_string(trim($_POST['strContactPerson']));
    $arrTplVars['strContactPerson'] = htmlspecialchars(stripslashes(trim($_POST['strContactPerson'])));

    $arrSqlData['strPhone'] = mysql_real_escape_string(trim($_POST['strPhone']));
    $arrTplVars['strPhone'] = htmlspecialchars(stripslashes(trim($_POST['strPhone'])));

    $arrSqlData['strEmail'] = mysql_real_escape_string(trim($_POST['strEmail']));
    $arrTplVars['strEmail'] = htmlspecialchars(stripslashes(trim($_POST['strEmail'])));

    if (!empty($arrSqlData['strEmail']) && !$objUtils->checkEmail($arrSqlData['strEmail'])) {
        $GLOBALS['manStatusError']=1;
//        $GLOBALS['manCodeError'][]['code'] = 'msgEmptyPhone';
        $GLOBALS['manCodeError'][]['code'] = 'msgEmptyEmail';
    }

    $arrSqlData['strMetaDesc'] = mysql_real_escape_string(trim($_POST['strMetaDesc']));
    $arrTplVars['strMetaDesc'] = htmlspecialchars(stripslashes(trim($_POST['strMetaDesc'])));

    $arrSqlData['strMetaKeywords'] = mysql_real_escape_string(trim($_POST['strMetaKeywords']));
    $arrTplVars['strMetaKeywords'] = htmlspecialchars(stripslashes(trim($_POST['strMetaKeywords'])));

    $arrSqlData['cbxStatus'] = ($_POST['cbxStatus'] == 'on' ? 'Y' : 'N');
    $arrTplVars['cbxStatus'] = ($_POST['cbxStatus'] == 'on' ? ' checked' : '');

    $arrSqlData['cbxReturn'] = ($_POST['cbxReturn'] == 'on' ? 'Y' : 'N');
    $arrTplVars['cbxReturn'] = ($_POST['cbxReturn'] == 'on' ? ' checked' : '');

    if ($GLOBALS['manStatusError']!=1) {
        $strSqlFields = " jv_title = '{$arrSqlData['strTitle']}'"
            .", jv_jw_id = ".$rbWorkbusy
            .", jv_jet_id = ".$rbEdu
            . ($intCompanyId>0?", jv_jc_id = ".$intCompanyId:'')
            . ($arrSqlData['strSalaryFrom']>0?", jv_salary_from = ".$arrSqlData['strSalaryFrom']:'')
            . ($arrSqlData['strSalaryTo']>0?", jv_salary_to = ".$arrSqlData['strSalaryTo']:'')
            .", jv_sex = '".$rbSex."'"
            .", jv_driver = '$cbxDriver'"
            .", jv_trips = '$cbxTrips'"
            . (empty($cityId) && !empty($arrSqlData['strCity']) ? ", jv_city = '".$arrSqlData['strCity']."'" : '' )
            . (!empty($cityId) ? ", jv_city_id = ".$cityId : '' )
            .", jv_description = '{$arrSqlData['strDescription']}'"
            .", jv_contact_fio = '{$arrSqlData['strContactPerson']}'"
            .", jv_phone = '{$arrSqlData['strPhone']}'"
            .", jv_email = '{$arrSqlData['strEmail']}'"
            .", jv_status = '{$arrSqlData['cbxStatus']}'"
            .", jv_meta_description = '{$arrSqlData['strMetaDesc']}'"
            .", jv_meta_keywords = '{$arrSqlData['strMetaKeywords']}'"
        ;
        if($intRecordId > 0) {
            $strSqlQuery = "UPDATE job_vacancies SET ".$strSqlFields." WHERE jv_id = ".$intRecordId;
        } else {
            $strSqlQuery = "INSERT INTO job_vacancies SET ".$strSqlFields.", jv_date_publish = UNIX_TIMESTAMP()";
        }

        if ( !$objDb->query( $strSqlQuery ) ) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
        } else {
            if ( empty($intRecordId) ) {
                $arrTplVars['intRecordId'] = $intRecordId = $objDb->insert_id();
            }

            if ($intRecordId > 0) {
                // Записать опыт работы в историю работ
                $arrSqlData['strCompany'] = mysql_real_escape_string(trim($_POST['strCompany']));
                $arrTplVars['strCompany'] = htmlspecialchars(stripslashes(trim($_POST['strCompany'])));

                $rbAgency = $_POST['rbAgency'];
                $arrTplVars['jc_type_'.$rbAgency] = " checked";

                $arrSqlData['strCompanyCity'] = mysql_real_escape_string(trim($_POST['strCompanyCity']));
                $arrTplVars['strCompanyCity'] = htmlspecialchars(stripslashes(trim($_POST['strCompanyCity'])));

                $companyCityId = $SiteUser->getCityId(trim($_POST['strCompanyCity']));
                if (empty($companyCityId)) {
                    $companyCityId = $SiteUser->saveCity(trim($_POST['strCompanyCity']));
                }

                $arrSqlData['strAddress'] = mysql_real_escape_string(trim($_POST['strAddress']));
                $arrTplVars['strAddress'] = htmlspecialchars(stripslashes(trim($_POST['strAddress'])));

                $arrSqlData['strWeb'] = mysql_real_escape_string(trim($_POST['strWeb']));
                $arrTplVars['strWeb'] = htmlspecialchars(stripslashes(trim($_POST['strWeb'])));

                $arrSqlData['strCompanyEmail'] = mysql_real_escape_string(trim($_POST['strCompanyEmail']));
                $arrTplVars['strCompanyEmail'] = htmlspecialchars(stripslashes(trim($_POST['strCompanyEmail'])));

                $arrSqlData['strCompanyPhone'] = mysql_real_escape_string(trim($_POST['strCompanyPhone']));
                $arrTplVars['strCompanyPhone'] = htmlspecialchars(stripslashes(trim($_POST['strCompanyPhone'])));

                $arrSqlData['strCompanyDescription'] = mysql_real_escape_string(trim($_POST['strCompanyDescription']));
                $arrTplVars['strCompanyDescription'] = htmlspecialchars(stripslashes(trim($_POST['strCompanyDescription'])));

                if (!empty($arrSqlData['strCompany'])) {
                    $strSqlFields = " jc_title = '".$arrSqlData['strCompany']."'"
                        .", jc_type = '$rbAgency'"
                        .(empty($companyCityId) && !empty($arrSqlData['strCompanyCity']) ? ", jc_city = '{$arrSqlData['strCompanyCity']}'" : '' )
                        .(!empty($companyCityId) ? ", jc_city_id = ".$companyCityId : '' )
                        .", jc_address = '{$arrSqlData['strAddress']}'"
                        .", jc_web = '{$arrSqlData['strWeb']}'"
                        .", jc_email = '{$arrSqlData['strCompanyEmail']}'"
                        .", jc_phone = '{$arrSqlData['strCompanyPhone']}'"
                        .", jc_description = '{$arrSqlData['strCompanyDescription']}'"
                    ;
                    if($intCompanyId > 0) {
                        $strSqlQuery = "UPDATE job_companies SET $strSqlFields WHERE jc_id = ".$intCompanyId;
                    } else {
                        $strSqlQuery = "INSERT INTO job_companies SET $strSqlFields, jc_date_create = UNIX_TIMESTAMP()";
                    }
                    if ( !$objDb->query( $strSqlQuery ) ) {
                        $GLOBALS['manStatusError']=1;
                        $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
                    }
                }
            }
        }
    }

    if ( $GLOBALS['manStatusError']!=1 ) {
        if ( $arrSqlData['cbxReturn'] == 'Y' ) {
            header('location:'.$arrTplVars['module.name'].'?errMess=msgOk&stPage='.$arrTplVars['stPage'].'&id='.$intRecordId);
        } else {
            header('location:'.$arrTplVars['module.parent'].'?errMess=msgOk&stPage='.$arrTplVars['stPage']);
        }
        exit;
    }
}

// ***** Обработка ошибок для вывода *******************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// *****************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if($_GET['id'] > 0) {
    $intRecordId = $arrTplVars['intRecordId'] = intval($_GET['id']);
    $arrTplVars['module.title'] = "Редактировать вакансию, ID:$intRecordId";

    $arr_string_fields = array('strTitle'=>'jv_title', 'strSalary'=>'jv_salary', 'strDescription'=>'jv_description', 'strContactPerson'=>'jv_contact_fio', 'strPhone'=>'jv_phone', 'strCity'=>'jv_city', 'strEmail'=>'jv_email', 'strMetaDesc'=>'jv_meta_description', 'strMetaKeywords'=>'jv_meta_keywords');
    $arr_checked_fields = array('jv_driver', 'jv_sex', 'jv_trips');
    $arr_int_fields = array('strSalaryFrom'=>'jv_salary_from', 'strSalaryTo'=>'jv_salary_to', 'intCompanyId'=>'jv_jc_id');

    $strSqlQuery = "SELECT * FROM `job_vacancies` WHERE `jv_id` = ".$intRecordId;
    $arrRecord = $objDb->fetch($strSqlQuery);

    if (is_array($arrRecord) && !empty($arrRecord)) {
        foreach($arr_string_fields as $key => $field) {
            $arrRecord[$key] = htmlspecialchars($arrRecord[$field]);
        }

        foreach($arr_checked_fields as $field) {
            $arrRecord[$field.'_'.$arrRecord[$field]] = ' checked';
        }

        foreach($arr_int_fields as $key => $field) {
            $arrRecord[$key] = (intval($arrRecord[$field]) ? $arrRecord[$field] : '');
        }

        $arrRecord['strTitle'] = mb_ucfirst($arrRecord['strTitle']);
        $arrRecord['strDatePublish'] = date('d.m.Y', $arrRecord['jv_date_publish']);
        $arrRecord['cbxStatus'] = $arrRecord['jv_status']=='Y'?' checked':'';
        if ($arrRecord['jv_city_id'] > 0) {
            $arrCity = $SiteUser->getCityById($arrRecord['jv_city_id']);
            if (is_array($arrCity) && !empty($arrCity)) {
                $arrRecord['strCity'] = $arrCity['city'];
            }
        }

        if (!empty($arrRecord['strMetaDesc'])) {
            if (!preg_match('/[работа от а до я]/i', $arrRecord['strMetaDesc'])) {
                $arrRecord['strMetaDesc'] = "Газета Работа от А до Я, вакансия - ".(preg_match('/'.$arrRecord['strTitle'].'/i', $arrRecord['strMetaDesc'])?'':$arrRecord['strTitle']).'. '.$arrRecord['strMetaDesc'];
            }
        } else {
            $arrRecord['strMetaDesc'] = "Газета Работа от А до Я, вакансия - ".$arrRecord['strTitle'];
        }

        if (!empty($arrRecord['strMetaKeywords'])) {
            foreach ($arrKeyWords as $k => $val) {
                if (!preg_match('/['.$val.']/i', $arrRecord['strMetaKeywords'])) {
                    $arrRecord['strMetaKeywords'] .= ",".$val;
                }
            }
        } else {
            $arrRecord['strMetaKeywords'] = $arrRecord['strTitle'].','.implode(',', $arrKeyWords);
        }

        $arrIf['record.exists'] = true;

        /**
         * Информация о компании
         */
        if(!empty($arrRecord['jv_jc_id']) && $arrRecord['jv_jc_id'] > 0) {
            $arr_string_fields = array('strCompany'=>'jc_title', 'strCompanyCity'=>'jc_city', 'strAddress'=>'jc_address', 'strCompanyDescription'=>'jc_description', 'strCompanyPhone'=>'jc_phone', 'strCompanyEmail'=>'jc_email', 'strWeb'=>'jc_web');
            $arr_checked_fields = array('jc_type');

            $strSqlQuery = "SELECT * FROM `job_companies` WHERE `jc_id` = ".$arrRecord['jv_jc_id'];
            $arrCompany = $objDb->fetch($strSqlQuery);
            if (is_array($arrCompany) && !empty($arrCompany)) {
                foreach($arr_string_fields as $key => $field) {
                    $arrRecord[$key] = htmlspecialchars($arrCompany[$field]);
                }

                foreach($arr_checked_fields as $field) {
                    $arrRecord[$field.'_'.$arrCompany[$field]] = ' checked';
                }

                if ($arrCompany['jc_city_id'] > 0) {
                    $arrCity = $SiteUser->getCityById($arrCompany['jc_city_id']);
                    if (is_array($arrCity) && !empty($arrCity)) {
                        $arrRecord['strCompanyCity'] = $arrCity['city'];
                    }
                }
            }
        }
    }
}
$arrRecord['strCity'] = (!empty($arrRecord['strCity'])?$arrRecord['strCity']:'Санкт-Петербург');
$arrRecord['strCompanyCity'] = (!empty($arrRecord['strCompanyCity'])?$arrRecord['strCompanyCity']:'Санкт-Петербург');
$objTpl->tpl_array($arrTplVars['module.name'], $arrRecord);

// Тип занятости
$strSqlQuery = "SELECT * FROM `job_workbusy` ORDER BY jw_id";
$arrWorkbusyTypes = $objDb->fetchall($strSqlQuery);
if (is_array($arrWorkbusyTypes) && !empty($arrWorkbusyTypes)) {
    foreach ($arrWorkbusyTypes as $k => $val) {
        $arrWorkbusyTypes[$k]['checked'] = (isset($arrRecord['jv_jw_id']) && $val['jw_id']==$arrRecord['jv_jw_id'] ? ' checked' : '');
    }
}
$objTpl->tpl_loop($arrTplVars['module.name'], "workbusy.types", $arrWorkbusyTypes);

// Образование
$strSqlQuery = "SELECT * FROM `job_education_types` ORDER BY jet_id";
$arrEduTypes = $objDb->fetchall($strSqlQuery);
if (is_array($arrEduTypes) && !empty($arrEduTypes)) {
    foreach ($arrEduTypes as $k => $val) {
        $arrEduTypes[$k]['checked'] = (isset($arrRecord['jv_jet_id']) && $val['jet_id']==$arrRecord['jv_jet_id'] ? ' checked' : '');
    }
}
$objTpl->tpl_loop($arrTplVars['module.name'], "education.types", $arrEduTypes);

// Города
$strSqlQuery = "SELECT `city` FROM `site_cities` ORDER BY `pos`, `id`";
$arrCities = $objDb->fetchcol($strSqlQuery);
foreach($arrCities as $key => $value) {
    $arrCities[$key] = htmlentities($value, ENT_QUOTES);
}
$arrTplVars['strCities'] = "'".implode("','", $arrCities)."'";// Санкт-Петербург','Москва','Лабытнанги','Салехард'";
$arrTplVars['strCity'] = (!empty($arrTplVars['strCity'])?$arrTplVars['strCity']:'Санкт-Петербург');

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);

