<?php
$arrTplVars['module.name'] = "project";
$arrTplVars['module.title'] = "Проекты";

/*******************************************************************
* Created by Jimmy™. Shirokovskiy D.A. Fri Jan 06 03:32:01 MSK 2006
*
* Вносим изменения
*
*******************************************************************/
if ($_POST['frmAddEdit']=='true') {
	$arrSqlData['intProjectID']   = intval($_POST['intProjectID']);
	$arrSqlData['strProjectPath'] = addslashes(trim($_POST['strProjectPath']));
	$arrSqlData['strProjectUrl']  = addslashes(trim($_POST['strProjectUrl']));
	$arrSqlData['strProjectName'] = addslashes(trim($_POST['strProjectName']));

	$arrTplVars['intProjectID'] = htmlspecialchars(trim($_POST['intProjectID']));
	$arrTplVars['strProjectPath'] = htmlspecialchars(trim($_POST['strProjectPath']));
	$arrTplVars['strProjectUrl'] = htmlspecialchars(trim($_POST['strProjectUrl']));
	$arrTplVars['strProjectName'] = htmlspecialchars(trim($_POST['strProjectName']));


    if (empty($arrSqlData['strProjectPath']) || empty($arrSqlData['strProjectUrl']) || empty($arrSqlData['strProjectName'])) {
        $GLOBALS['manCodeError'][1]['code'] = '114';
    } else {
        if (empty($arrSqlData['intProjectID'])) {
            $strSqlQuery = "INSERT INTO cms_projects SET"
                ." fp_name   = '".$arrSqlData['strProjectName']."'"
                .", fp_path = '".$arrSqlData['strProjectPath']."'"
                .", fp_url   = '".$arrSqlData['strProjectUrl']."'"
                .", fp_status = 'Y'"
                ."";
        } else {
            $strSqlQuery = "UPDATE cms_projects SET"
                ." fp_name   = '".$arrSqlData['strProjectName']."'"
                .", fp_path = '".$arrSqlData['strProjectPath']."'"
                .", fp_url   = '".$arrSqlData['strProjectUrl']."'"
                ." WHERE fp_id = '".$arrSqlData['intProjectID']."'";
        }

        if ( !$objDb->query( $strSqlQuery ) ) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
        } else {
            header('location: '.$arrTplVars['module.name']."?errMess=101prj");
            exit();
        }
    }
}

// ******* Редактируем
if (isset($_GET['action'])) {
	switch ($_GET['action']) {
		case "edit":
			$arrIf['show.edit.form'] = true;
			$intRecId = intval($_GET['idRec']);
			$strSqlQuery = "SELECT * FROM cms_projects WHERE fp_id = '$intRecId'";
			$arrProjectInfo = $objDb->fetch( $strSqlQuery );
			$arrTplVars['intProjectID'] = $arrProjectInfo['fp_id'];
			$arrTplVars['strProjectPath'] = htmlspecialchars(stripslashes($arrProjectInfo['fp_path']));
			$arrTplVars['strProjectUrl'] = htmlspecialchars(stripslashes($arrProjectInfo['fp_url']));
			$arrTplVars['strProjectName'] = htmlspecialchars(stripslashes($arrProjectInfo['fp_name']));
      unset($arrProjectInfo);
			break;

    case "delete":

			break;
    case "add":
      $arrIf['show.edit.form'] = true;
			break;

		default:
			break;
	}
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if (!$arrIf['show.edit.form']) {
	// ******* Выборка проектов
  $strSqlQuery = "SELECT * FROM cms_projects ORDER BY fp_id";
  $arrListOfProjects = $objDb->fetchall( $strSqlQuery );

  $objTpl->tpl_loop($arrTplVars['module.name'], "lst.projects", $arrListOfProjects);
}

$arrIf['show.list'] = !$arrIf['show.edit.form'];
$arrTplVars['DROOT'] = DROOT;

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
