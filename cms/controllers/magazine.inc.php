<?php
$arrTplVars['module.name'] = "magazine";
$arrTplVars['module.title'] = "Газета";

// http://issuu.com/rabotayaru/docs/999

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

if (isset($_POST['week_num'])) {
    $arrSqlData['week_num'] = intval(trim($_POST['week_num']));
    $arrTplVars['week_num'] = ($arrSqlData['week_num'] > 0 ? $arrSqlData['week_num'] : '');

    $arrSqlData['year_num'] = intval(trim($_POST['year_num']));
    $arrTplVars['year_num'] = ($arrSqlData['year_num'] > 0 ? $arrSqlData['year_num'] : '');

    $arrSqlData['num_tu'] = $arrTplVars['num_tu'] = intval($_REQUEST['num_tu']);
    $arrSqlData['num_we'] = $arrTplVars['num_we'] = intval($_REQUEST['num_we']);
    $arrSqlData['num_th'] = $arrTplVars['num_th'] = intval($_REQUEST['num_th']);

    if( empty($arrSqlData['num_tu']) && empty($arrSqlData['num_we']) && empty($arrSqlData['num_th']) ) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgErrorNoUrl';
    } else {
        /**
         * Загрузка фото
         */
        if ( !is_object($objImage) ) {
            include_once(SITE_LIB_DIR."cls.images.php");
            $objImage = new clsImages();
        }

        // extended extensions
        $arrExts['pjpeg'] = 'jpg';
        $arrExts['jpeg'] = 'jpg';

        $dirImages = PRJ_IMAGES."magazines";
        if(!file_exists($dirImages) || !is_dir($dirImages)) {
            if (!mkdir($dirImages, 0775, true)) {
                $GLOBALS['manStatusError']=1;
                $GLOBALS['manCodeError'][]['code'] = 'msgErrorCantDir';
            }
        }

        $arrDow = array('tu', 'we', 'th');
        foreach ($arrDow as $dow) {
            // Обложка журнала
            if ( $_FILES['file'.ucfirst($dow)]['error'] == 0 && $_FILES['file'.ucfirst($dow)]['size'] > 0 && $_FILES['file'.ucfirst($dow)]['size'] < 2000000 ) {
                $strImageName = "magazine.thumb.$dow.".$arrSqlData['week_num'].'.'.$arrSqlData['year_num'];

                if(empty($arrSqlData['num_'.$dow]) /*|| $objUtils->isUrl($arrSqlData['num_'.$dow])*/) {
                    $GLOBALS['manStatusError']=1;
                    $GLOBALS['manCodeError'][]['code'] = 'msgErrorBadNum';
                } else {
                    $mimeType = explode("/", $_FILES['file'.ucfirst($dow)]['type']);
                    $mimeType = $mimeType[1];

                    if ( array_key_exists($mimeType , $arrExts) ) {
                        $mimeType = $arrExts[$mimeType];
                    }

                    if ( $mimeType != 'jpg' ) {
                        $GLOBALS['manStatusError']=1;
                        $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
                    } else {
                        $fileName = "$strImageName.$mimeType";
                        $fileNameOrig = "temp.$strImageName.$mimeType";

                        if ($GLOBALS['manStatusError']!=1) {
                            if (file_exists($dirImages."/".$fileName)) {
                                rename($dirImages."/".$fileName, $dirImages."/bkp.".time().'.'.$fileName);
                            }

                            if (move_uploaded_file($_FILES['file'.ucfirst($dow)]['tmp_name'], $dirImages."/".$fileNameOrig )) {
                                $objImage->resizeImage($dirImages."/".$fileNameOrig, $dirImages."/".$fileName, 168, 237 );
                                chmod($dirImages."/".$fileName, 0664);
                                unlink($dirImages."/".$fileNameOrig);
                            }
                        }
                    }
                }
            }

            # Если фото загружено
            if ($GLOBALS['manStatusError']!=1) {
                $strSqlQuery = "REPLACE INTO `job_magazines` SET"
                    ." `jm_year` = ".$arrSqlData['year_num']
                    .", `jm_week` = ".$arrSqlData['week_num']
                    .", `jm_dow` = '$dow'"
                    .", `jm_url` = '{$arrSqlData['num_'.$dow]}'"
                ;
                if (!$objDb->query($strSqlQuery)) {
                    $GLOBALS['manStatusError']=1;
                    $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
                }
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    if ( $GLOBALS['manStatusError']!=1 ) {
        if ( $arrSqlData['cbxReturn'] == 'Y' ) {
            header('location:'.$arrTplVars['module.name'].'?errMess=msgOk&stPage='.$arrTplVars['stPage'].'&id='.$intRecordId);
        } else {
            header('location:'.$arrTplVars['module.parent'].'?errMess=msgOk&stPage='.$arrTplVars['stPage']);
        }
        exit;
    }
}

// ***** Обработка ошибок для вывода *******************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// *****************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

// img = 168 x 237
$week_num = $arrTplVars['week_num'] = !empty($_GET['week_num']) ? intval(trim($_GET['week_num'])) : date('W');
$year_num = $arrTplVars['year_num'] = !empty($_GET['year_num']) ? intval(trim($_GET['year_num'])) : date('Y');

if (!empty($year_num) && !empty($week_num)) {
    $strSqlQuery = "SELECT * FROM `job_magazines` WHERE `jm_week` = ".$week_num." AND `jm_year` = ".$year_num;
    $arrMags = $objDb->fetchall($strSqlQuery);

    if (is_array($arrMags) && !empty($arrMags)) {
        foreach ($arrMags as $k => $mag) {
            $srcImage = 'magazines/magazine.thumb.'.$mag['jm_dow'].'.'.$mag['jm_week'].'.'.$mag['jm_year'].'.jpg';
            if (file_exists(PRJ_IMAGES.$srcImage)) {
                $arrTplVars['script_'.$mag['jm_dow']] = '$("#'.$mag['jm_dow'].'").attr("src", "'.$arrTplVars['cfgAllImg'].$srcImage.'").show(); $("#file'.ucfirst($mag['jm_dow']).'").hide();';
            }
            $arrTplVars['num_'.$mag['jm_dow']] = htmlspecialchars($mag['jm_url']);
        }
    }
}

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);

