<?php
$arrTplVars['module.name'] = "dictionary";
$arrTplVars['module.child'] = "dictionary.form";
$arrTplVars['module.title'] = "Словарь профессий";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

/**
 * Удаление записи
 */
if ( intval($_GET['id']) > 0 && $_GET['act'] == 'del' ) {
    $intRecordId = intval($_GET['id']);
    $strSqlQuery = "DELETE FROM site_articles WHERE sa_id = ".$intRecordId." AND sa_type = 'dict'";
    if ( !$objDb->query($strSqlQuery) ) {
        #mysql error
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
    } else {
        if ( file_exists(PRJ_IMAGES."articles/articles.photo.$intRecordId.jpg")) {
            unlink(PRJ_IMAGES."articles/articles.photo.$intRecordId.jpg");
            unlink(PRJ_IMAGES."articles/articles.photo.$intRecordId.b.jpg");
        }

        header("Location: {$arrTplVars['module.name']}?stPage={$arrTplVars['stPage']}");
        exit();
    }
}

// ***** Обработка ошибок для вывода *******************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// *****************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

// Всего записей в базе
$strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM site_articles WHERE sa_type = 'dict'";
$arrTplVars['intQuantitySelectRecords'] = $arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');
// Выбрано записей
//$strSqlQuery = "SELECT COUNT(*) AS intQuantitySelectRecords FROM site_articles";
//$arrTplVars['intQuantitySelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantitySelectRecords');
// ***** BEGIN: Построение пейджинга для вывода списка
$objPagination = new tplPaginator($arrTplVars['intQuantitySelectRecords'], SITE_TPL_TPL_DIR);
/**
 * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
 * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
 */
$objPagination->strPaginatorTpl = "site.global.1.tpl";
$objPagination->intQuantRecordPage = 15;
$objPagination->strColorLinkStyle = "link-b";
$objPagination->strColorActiveStyle = "tab-bl-b";
$objPagination->strNameImageBack = "arrow_one_left.gif";
$objPagination->strNameImageForward = "arrow_one_right.gif";
// Создаем блок пэйджинга
$objPagination->paCreate();
// ***** END: Построение пейджинга для вывода списка
// Запрос для выборки нужных записей
$strSqlQuery = "SELECT * FROM site_articles WHERE sa_type='dict' ORDER BY sa_id DESC ".$objPagination->strSqlLimit;
$arrArticles = $objDb->fetchall( $strSqlQuery );
// кол-во публикаций показанных на странице
$arrTplVars['intQuantityShowRecOnPage'] = ( !empty($arrArticles) ? count($arrArticles) : 0 );
// Присвоение значения пэйджинга для списка
$arrTplVars['blockPaginator'] = $objPagination->paShow();

if ( is_array($arrArticles) ) {
    foreach ( $arrArticles as $key => $value ) {
        $arrArticles[$key]['strDatePublish'] = date( "d.m.Y", $value['sa_date_publish']);
        $arrArticles[$key]['cbxStatus'] = $value['sa_status'] == 'Y' ? ' checked' : '';
        $arrIf['attach.'.$value['sa_id']] = ( file_exists(DROOT."storage/images/articles/articles.photo.".$value['sa_id'].".jpg") && $value['sa_image'] == 'Y');
    }
}

$objTpl->tpl_loop($arrTplVars['module.name'], "articles", $arrArticles);

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);

