<?php
$arrTplVars['page.name'] = "site.images";

$nameImg = "asd.gif";

$arrAllowFormatImg = array(
  'image/pjpeg'  =>  true,
  'image/gif'  =>  true,
  'application/x-shockwave-flash'  =>  true,
);

// ***** Обработка удаления изображения ***********************************************
if ($_GET['action']=='delete' && intval($_GET['img'])>0) {
    $arrInfoImage = $objDb->fetch( "SELECT si_name, si_format FROM site_images WHERE si_id='".intval($_GET['img'])."'" );
    if(!is_array($arrInfoImage)) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = '106img';
    } else {
        if(file_exists(UPLOADIMG.$arrInfoImage['si_name'].".".$arrInfoImage['si_format'])) {
            if (!unlink(UPLOADIMG.$arrInfoImage['si_name'].".".$arrInfoImage['si_format'])) {
                $GLOBALS['manStatusError']=1;
                $GLOBALS['manCodeError'][]['code'] = '222';
            }
        }

        if($GLOBALS['manStatusError']!=1) {
            $strSQLQuery = "DELETE FROM site_images WHERE si_id='".intval($_GET['img'])."'";
            if(!$objDb->query($strSQLQuery)) {
                $GLOBALS['manStatusError']=1;
                $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
            } else {
                $GLOBALS['manCodeError'][]['code'] = '107img';
            }
        }
    }
}
// **************************************************************************************

// ***** Обработка сохранения изображений ***********************************************
if ($_POST['frmAddImage']=='true') {
// --- проверяем формат загружаемого изображения
  $arrTplVars['intUploadGroup'] = intval($_POST['intUploadGroup']);

  if (empty($_FILES['strUploadFile']['tmp_name'])) { // Если не указано загружаемое изображение
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = '100img';
  } else {
    if($arrAllowFormatImg[$_FILES['strUploadFile']['type']]!=true) { // Если неподдерживаемый тип изображения
      $GLOBALS['manStatusError']=1;
      $GLOBALS['manCodeError'][]['code'] = '101img';
    } else {

      if(!eregi("^[-a-z0-9_\.]+$", $_FILES['strUploadFile']['name'])) { // Если не допустимые знаки
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = '103img';
      } else {
        if(intval($objDb->fetch( "SELECT si_id FROM site_images WHERE si_name='".substr($_FILES['strUploadFile']['name'], 0, strrpos($_FILES['strUploadFile']['name'], "."))."' AND si_format='".substr($_FILES['strUploadFile']['name'], -3, 3)."'"), "si_id" )>0) { // Проверяем нет ли уже изображения с таким именем
          $GLOBALS['manStatusError']=1;
          $GLOBALS['manCodeError'][]['code'] = '104img';
        } else {
          if(is_uploaded_file($_FILES['strUploadFile']['tmp_name'])) {
            $arrImagesSize = getimagesize($_FILES['strUploadFile']['tmp_name']);
            if(!move_uploaded_file($_FILES['strUploadFile']['tmp_name'], UPLOADIMG.$_FILES['strUploadFile']['name'])) {
              $GLOBALS['manStatusError']=1;
              $GLOBALS['manCodeError'][]['code'] = '222';
            }
          } else {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = '222';
          }
        }
      }
    }
  }

  if ($GLOBALS['manStatusError']!=1) { // Закидываем в базу
    $strSQLQuery = "INSERT INTO site_images (si_group, si_name, si_format, si_width, si_height) VALUES ('".$arrTplVars['intUploadGroup']."', '".substr($_FILES['strUploadFile']['name'], 0, strrpos($_FILES['strUploadFile']['name'], "."))."', '".substr($_FILES['strUploadFile']['name'], strrpos($_FILES['strUploadFile']['name'], ".")+1)."', '".$arrImagesSize[0]."', '".$arrImagesSize[1]."')";
    if(!$objDb->query($strSQLQuery)) {
      $GLOBALS['manStatusError']=1;
      $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
    } else {
      $GLOBALS['manCodeError'][]['code'] = '105img';
    }
  }

  $errSuf = '_uploadImg';
}
// **************************************************************************************

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars["error".$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars["error".$errSuf] = $objUtils->echoMessage($arrTplVars["error".$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

$objTpl->tpl_load($arrTplVars['page.name'], $arrTplVars['page.name'].".html");
$objTpl->tpl_array($arrTplVars['page.name'], $ConfigManager);

$arrTplVars['intImagesCount'] = $objDb->fetch( "SELECT COUNT(*) AS intImagesCount FROM site_images" , 'intImagesCount');

// ***** Формируем список групп изображений *********************************************
$arrLstItemImages = $objDb->fetchall( "SELECT * FROM ".$_db_tables['fcmImagesGroup']." WHERE sig_parent IS NULL" );

foreach($arrLstItemImages as $key=>$value) {
  $arrItemImagesTpl[$key] = array('intIdItem'=>$arrLstItemImages[$key]['sig_id'], 'strNameItem'=>$arrLstItemImages[$key]['sig_name']);
  $arrParent[$arrLstItemImages[$key]['sig_id']] = true;
}
$objTpl->tpl_loop($arrTplVars['page.name'], "lst.item.images", $arrItemImagesTpl);

$arrTplVars['intIdItem'] = intval($_GET['idItem']);

$arrTplVars['intUploadGroup'] = ($arrTplVars['intIdItem']>0) ? $arrTplVars['intIdItem'] : $arrTplVars['intUploadGroup'];

if($arrTplVars['intIdItem']>0) {
  if($arrParent[$arrTplVars['intIdItem']]!=true)
    $inIdParentSubItem = $objDb->fetch( "SELECT sig_parent FROM ".$_db_tables['fcmImagesGroup']." WHERE sig_id='".$arrTplVars['intIdItem']."'" , 'sig_parent');
  else
    $inIdParentSubItem = $arrTplVars['intIdItem'];

  $arrIf['block.images.'.$inIdParentSubItem] = true;
  $arrIf['block.show.images'] = true;



  $arrLstSubitemImages = $objDb->fetchall( "SELECT * FROM ".$_db_tables['fcmImagesGroup']." WHERE sig_parent='".$inIdParentSubItem."'" );

  foreach ($arrLstSubitemImages as $key=>$value) {
    $arrSubitemImagesTpl[$key] = array('intIdSubItem'=>$arrLstSubitemImages[$key]['sig_id'], 'strNameSubItem'=>$arrLstSubitemImages[$key]['sig_name']);
  }
  $objTpl->tpl_loop($arrTplVars['page.name'], "lst.subitem.images.".$inIdParentSubItem, $arrSubitemImagesTpl);
}
// **************************************************************************************

// ***** Группы изображений *****************************************************************
$arrGroupImages = $objDb->fetchall( "SELECT * FROM ".$_db_tables['fcmImagesGroup'] );
if(count($arrGroupImages)>0) {
  foreach($arrGroupImages as $key=>$value) {
    if(empty($arrGroupImages[$key]['sig_parent']))
      $arrItemGroup[] = $arrGroupImages[$key];
    else
      $arrSubItemGroup[$arrGroupImages[$key]['sig_parent']][] = $arrGroupImages[$key];
  }

  foreach($arrItemGroup as $key=>$value) {
    $arrGroupForTpl[] = array('intIdUploadGroup'=>$arrItemGroup[$key]['sig_id'], 'intIdUploadGroup_selected'=>($arrItemGroup[$key]['sig_id']==$arrTplVars['intUploadGroup']) ? ' selected' : '', 'strNameUploadGroup'=>$arrItemGroup[$key]['sig_name']);

    if(count($arrSubItemGroup[$arrItemGroup[$key]['sig_id']])>0) {
      foreach($arrSubItemGroup[$arrItemGroup[$key]['sig_id']] as $kkey=>$vvalue) {
        $arrGroupForTpl[] = array('intIdUploadGroup'=>$arrSubItemGroup[$arrItemGroup[$key]['sig_id']][$kkey]['sig_id'], 'intIdUploadGroup_selected'=>($arrSubItemGroup[$arrItemGroup[$key]['sig_id']][$kkey]['sig_id']==$arrTplVars['intUploadGroup']) ? ' selected' : '', 'strNameUploadGroup'=>'&nbsp;&nbsp;&nbsp;'.$arrSubItemGroup[$arrItemGroup[$key]['sig_id']][$kkey]['sig_name']);
      }
    }
  }

}

$objTpl->tpl_loop($arrTplVars['page.name'], "lst.group.images", $arrGroupForTpl);
// **************************************************************************************



// ***** Формируем список изображений ***************************************************
$arrLstImages = $objDb->fetchall( "SELECT * FROM site_images WHERE si_group='".$arrTplVars['intIdItem']."'" );

$arrTplVars['intImagesCountGroup'] = count($arrLstImages);

if (count($arrLstImages)==0) { // Если нет доступных изображений
  $arrIf['lst.images.empty'] = true;
} else {
  $arrIf['lst.images'] = true;
  $i=0;
  foreach ($arrLstImages as $key=>$value) {
    $arrLstImages[$key]['strIdImage'] = $arrLstImages[$key]['si_id'];
    if($arrLstImages[$key]['si_format']!='swf') {
      $arrLstImages[$key]['strNameImage'] = $arrLstImages[$key]['si_name'].'.'.$arrLstImages[$key]['si_format'];
      $arrLstImages[$key]['strWidthComres'] = ($arrLstImages[$key]['si_width']>170) ? '170' : $arrLstImages[$key]['si_width'];
      $arrLstImages[$key]['strObject'] = '<img src="'.$arrTplVars['siteimg'].$arrLstImages[$key]['strNameImage'].'" width="'.$arrLstImages[$key]['strWidthComres'].'">';
      $arrLstImages[$key]['strSizeImage'] = $arrLstImages[$key]['si_width'].'x'.$arrLstImages[$key]['si_height'];
    } else {
      $arrLstImages[$key]['strNameImage'] = $arrLstImages[$key]['si_name'].'.'.$arrLstImages[$key]['si_format'];
      $arrLstImages[$key]['strObject'] = '<img src="'.$arrTplVars['adminimg'].'icon_swf.jpg" width="70" height="73">';
      $arrLstImages[$key]['strSizeImage'] = '?';
    }

    $arrLstImages[$key]['strDateAdd'] = $objUtils->workDate(3, $arrLstImages[$key]['si_date_add']);
    $i++;
    if($i==3) {
      $arrLstImages[$key]['strTableTr'] = '</tr><tr>';
      $i=0;
    }

  }
}

$objTpl->tpl_loop($arrTplVars['page.name'], "lst.images", $arrLstImages);

print_r($arrHeightTableRows);

$objTpl->tpl_array($arrTplVars['page.name'], $arrHeightTableRows);



$objTpl->tpl_if($arrTplVars['page.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['page.name'], $arrTplVars);
