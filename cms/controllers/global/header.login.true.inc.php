<?php
$arrTplVars['frg.name'] = "header.login.true"; // Имя фрагмента
$arrTplVars['strCurrentDate'] = $objUtils->workDate(1); // Текущая дата

$objTpl->Template(ADMTPLGL);
$objTpl->tpl_load($arrTplVars['frg.name'], $arrTplVars['frg.name'].".html"); // Загружаем фрагмент
$objTpl->Template(ADMTPL);

// ***** проверяем наличие модуля и права доступа пользователя к вызываемому модулю
$strSqlQuery = "SELECT * FROM cms_modules WHERE cm_link='".$arrTplVars['reqModule']."' AND cm_status='Y'";
$arrInfoModule = $objDb->fetch( $strSqlQuery );

if ((file_exists(ADMPHP.$arrTplVars['reqModule'].".inc.php") && intval($arrInfoModule['cm_id'])>0) || $arrTplVars['reqModule']=='logout') {
    if ($arrInfoAccessModules[$arrTplVars['reqModule']]==true || $arrTplVars['reqModule']=='user.main' || $arrTplVars['reqModule']=='logout') {
        // Глобальный файл сообщений модулей
        include_once( ADMERR.'globals.err.php' );

        // Специфичный файл сообщений для конкретного модуля
        if ( file_exists(ADMERR.$arrInfoModule['cm_error_file_name'].'.err.php') ) {
            include_once( ADMERR.$arrInfoModule['cm_error_file_name'].'.err.php' );
        }
        $arrIf['block.user'] = true;
        include_once( ADMPHP.$arrTplVars['reqModule'].'.inc.php' );
    } else {
        $arrIf['module.access.error'] = true;
        $arrTplVars['reqModule'] = 'user.main';
        include_once( ADMPHP.$arrTplVars['reqModule'].".inc.php" );
    }
} else {
    $arrIf['module.exists.error'] = true;
    $arrTplVars['reqModule'] = 'user.main';
    include_once( ADMPHP.$arrTplVars['reqModule'].".inc.php" );
}

// ***** Выбор проекта для работы
if( $_POST['frmSelectProject']=='true' ) {
    if( $_POST['enumProjects']>0 ) {
        $_SESSION['intIdDefaultProject'] = intval($_POST['enumProjects']);
        header("location: ".CMS_URL);
        exit();
    }
}

$arrIf['title.module'] = (isset($arrTplVars['module.title'])) ? true : false;

$objTpl->tpl_if($arrTplVars['frg.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['frg.name'], $arrTplVars);
