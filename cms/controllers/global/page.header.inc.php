<?php
include_once "models/CmsUser.php";
if(!is_object($objCmsUser)) $objCmsUser = new CmsUser();
$objTpl->tpl_load("header", "global/page.header.html");

$arrIf['block.login.true'] = (isset($_SESSION['userInfoStorage']['cu_id'])) ? true : false;
$arrIf['block.login.false'] = !$arrIf['block.login.true'];

$arrIf['users.access'] = $objCmsUser->hasUserModuleAccess($_SESSION['userInfoStorage']['cu_id'], 18, $_SESSION['intIdDefaultProject']) != false;
$arrIf['pages.access'] = $objCmsUser->hasUserModuleAccess($_SESSION['userInfoStorage']['cu_id'], 9, $_SESSION['intIdDefaultProject']) != false;
$arrIf['fragments.access'] = $objCmsUser->hasUserModuleAccess($_SESSION['userInfoStorage']['cu_id'], 14, $_SESSION['intIdDefaultProject']) != false;
$arrIf['settings.access'] = $objCmsUser->hasUserModuleAccess($_SESSION['userInfoStorage']['cu_id'], 2, $_SESSION['intIdDefaultProject']) != false;
$arrIf['templates.access'] = $objCmsUser->hasUserModuleAccess($_SESSION['userInfoStorage']['cu_id'], 6, $_SESSION['intIdDefaultProject']) != false;

$objTpl->tpl_if("header", $arrIf);
$objTpl->tpl_array("header", $arrTplVars);
