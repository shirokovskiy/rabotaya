<?php
$arrTplVars['frg.name'] = "user.menu"; // Имя фрагмента
$objTpl->Template(ADMTPLGL);
$objTpl->tpl_load($arrTplVars['frg.name'], $arrTplVars['frg.name'].".html"); // Загружаем фрагмент

$arrSubMenuItemModules = false;
$arrChapterMenuItemModules = false;

/**
 * Отображение списка проектов к которым имеем доступ
 */
if (isset($_SESSION['arrInfoAccessProject'])) {
    if(count($_SESSION['arrInfoAccessProject'])>1) {
        $arrIf['block.lst.projects'] = true; // блок "Проекты" активен

        foreach($_SESSION['arrInfoAccessProject'] as $key=>$value) {
            if ( $_SESSION['intIdDefaultProject'] == $_SESSION['arrInfoAccessProject'][$key]['intIdProject'] ) {
                $_SESSION['arrInfoAccessProject'][$key]['intIdProjectSelected'] = ' selected';
            } else {
                $_SESSION['arrInfoAccessProject'][$key]['intIdProjectSelected'] = '';
            }
        }

        $objTpl->tpl_loop($arrTplVars['frg.name'], "lst.projects", $_SESSION['arrInfoAccessProject']);
    } else { // Если доступ только к одному проекту, список select не нужен
        $arrIf['block.project'] = true;
        $arrTplVars['strCurrentProject'] = $_SESSION['arrInfoAccessProject'][1]['strNameProject'];
    }
}


/**
 * Выбрать модули к которым имеем доступ
 */
$strSqlQuery = "SELECT * FROM cms_users_access_modules WHERE cuam_id_owner=".$_SESSION['userInfoStorage']['cu_id']." AND cuam_id_project = ".$_SESSION['intIdDefaultProject']." AND cuam_access != 'N'";
$arrAccessUser = $objDb->fetchall( $strSqlQuery );
if (is_array($arrAccessUser)) {
	foreach($arrAccessUser as $key=>$value) {
		$arrIdModule[] = $arrAccessUser[$key]['cuam_id_module'];
	}

    if (is_array($arrIdModule) && !empty($arrIdModule)) {
        /**
        * Получаем список модулей
        */
        $strSqlQuery = "SELECT * FROM cms_modules WHERE cm_id IN (".implode(",", $arrIdModule).") AND cm_status='Y' ORDER BY cm_order";
        $arrLoadModules = $objDb->fetchall( $strSqlQuery );
        if (is_array($arrLoadModules)) {
            foreach($arrLoadModules as $key=>$arrModule) {
                $arrParentModules[$arrModule['cm_id']] = $arrModule['cm_link']; // Глобальный массив модулей
                $arrInfoAccessModules[$arrModule['cm_link']] = true; // Глобальный массив доступа к модулям

                if ( $arrModule['cm_hide']=='N' ) {
                    if ( intval($arrModule['cm_id_parent'])== 0 ) {
                        // Установим список модулей, которые являются "главными" пунктами меню в блоке "Управление сайтом"
                        // при этом, если они не являются ТОПовыми.
                        if ($arrModule['cm_top_show']=='N') {
                            $arrMainMenuItemModules[$key]['strModuleName'] = $arrModule['cm_name'];
                            $arrMainMenuItemModules[$key]['intIdModule'] = $arrModule['cm_id'];
                            $arrMainMenuItemModules[$key]['strLink'] = $arrModule['cm_link'];
                        }
                    } else {
                        // Установим пункты меню (модули) доступные в блоке "Управление разделом"
                        if ( $arrModule['cm_insert_item']=='Y' ) {
                            $arrChapterMenuItemModules[$arrModule['cm_id_parent']][$key]['strModuleName'] = $arrModule['cm_name'];
                            $arrChapterMenuItemModules[$arrModule['cm_id_parent']][$key]['intIdModule'] = $arrModule['cm_id'];
                            $arrChapterMenuItemModules[$arrModule['cm_id_parent']][$key]['strLink'] = $arrModule['cm_link'];

                            /*** Tue Oct 11 13:12:39 MSD 2005, Shirokovskiy D/Jimmy
                            ***
                            ** Добавка разделителя
                            */
                            if ( $arrModule['cm_separator'] == 'Y' ) {
                                $arrChapterMenuItemModules[$arrModule['cm_id_parent']][$key]['dotSeparator'] = '<tr><td colspan=2 height=10 width=100% background="{cfgAdminImg}dotHor3px_'.$arrTplVars['cId'].'.gif" style="background-repeat: repeat-x; background-position: center;"></td></tr>';
                            }

                        } else if ( $arrModule['cm_insert_item']=='N' && intval($arrModule['cm_id_parent']) > 0 ) {
                            // Установим пункты меню как подменю в блоке "Управление сайтом"
                            $arrSubMenuItemModules[$arrModule['cm_id_parent']][$key]['strSubModuleName'] = $arrModule['cm_name'];
                            $arrSubMenuItemModules[$arrModule['cm_id_parent']][$key]['intSubIdModule'] = $arrModule['cm_id'];
                            $arrSubMenuItemModules[$arrModule['cm_id_parent']][$key]['strSubLink'] = $arrModule['cm_link'];
                        }
                    }
                }


                /**
                 * Если модуль вложенный, то определяем ID его родителя, дабы открыть меню нужного родителя
                 */
                if ( $arrTplVars['reqModule'] == $arrModule['cm_link'] ) {

                    if ( intval($arrModule['cm_id_parent'])== 0 ) {
                        $intIdLoadInsertModule = $arrModule['cm_id'];
                    } else if ( intval($arrModule['cm_id_parent'])>0 && $arrModule['cm_insert_item'] == 'N' ) { // Added by Jimmy™ 24.10.2005
                        $intIdLoadInsertModule = $arrModule['cm_id'];
                    } else {
                        $intIdLoadInsertModule = $arrModule['cm_id_parent'];
                    }
                }
            }
        }
    }
}

// Заполняем меню админки в соответствии с доступами пользователя
$objTpl->tpl_loop($arrTplVars['frg.name'], "menu.item", $arrMainMenuItemModules);

if (is_array($arrSubMenuItemModules)) {
  foreach ($arrSubMenuItemModules as $key=>$value) {
    $objTpl->tpl_loop($arrTplVars['frg.name'], "menu.sub.".$key, $arrSubMenuItemModules[$key]);
  }
}

if (is_array($arrChapterMenuItemModules)) {
  if(is_array($arrChapterMenuItemModules[$intIdLoadInsertModule])) {
    $arrIf['block.subitem.menu'] = true;
    $objTpl->tpl_loop($arrTplVars['frg.name'], "menu.insert.item", $arrChapterMenuItemModules[$intIdLoadInsertModule]);
  }
}


$objTpl->tpl_if($arrTplVars['frg.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['frg.name'], $arrTplVars);
$objTpl->tpl_strip_loops($arrTplVars['frg.name']);
