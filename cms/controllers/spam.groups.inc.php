<?php
$arrTplVars['module.name'] = "spam.groups";
$arrTplVars['module.title'] = "Группы подписчиков";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

include_once "models/Spamer.php";
$objSpamer = new Spamer();

/**
 * Удаление
 */
if ( intval($_GET['id']) > 0 && $_GET['act'] == 'untache' && intval($_GET['mid']) > 0 ) {
    $intRecordId = intval($_GET['id']);
    $intMailId = intval($_GET['mid']);
    if ($objSpamer->removeSubscriberFromGroup($intMailId, $intRecordId) == false) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
    } else {
        $_SESSION['manCodeError'][]['code'] = 'msgDelOk';
        header("Location: {$arrTplVars['module.name']}?id=$intRecordId&stPage={$arrTplVars['stPage']}");
        exit();
    }
}

/**
 * Запись нового подписчика
 */
if (isset($_GET['id']) && isset($_POST['strNewSubsEmail'])) {
    $intRecordId = $arrSqlData['intRecordId'] = intval(trim($_GET['id']));

    $arrSqlData['strNewSubsEmail'] = mysql_real_escape_string(trim($_POST['strNewSubsEmail']));
    $arrTplVars['strNewSubsEmail'] = htmlspecialchars(stripslashes(trim($_POST['strNewSubsEmail'])));

    if(!$objUtils->isValidEmail($arrSqlData['strNewSubsEmail'])) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgEmptyEmail';
    }

    $arrSqlData['strNewSubsName'] = mysql_real_escape_string(trim($_POST['strNewSubsName']));
    $arrTplVars['strNewSubsName'] = htmlspecialchars(stripslashes(trim($_POST['strNewSubsName'])));

    if ($GLOBALS['manStatusError']!=1) {
        $strSqlQuery = "SELECT * FROM `site_users_subscribers` WHERE `sus_email` = '".$arrSqlData['strNewSubsEmail']."' ORDER BY `sus_id` LIMIT 1";
        $arrExistUser = $objDb->fetch($strSqlQuery);

        if (is_array($arrExistUser) && !empty($arrExistUser)) {
            $intUserId = $arrExistUser['sus_id'];
        } else {
            $strSqlFields = ""
                ." sus_email = '{$arrSqlData['strNewSubsEmail']}'"
                .", sus_name = '{$arrSqlData['strNewSubsName']}'"
            ;
            $strSqlQuery = "INSERT INTO site_users_subscribers SET ".$strSqlFields.", sus_date_create = UNIX_TIMESTAMP()";
            if ( !$objDb->query( $strSqlQuery, false ) ) {
                $GLOBALS['manStatusError']=2;
                $GLOBALS['manCodeError'][]['code'] = 'msgRecordExist';
            } else {
                # Подписать на данную конкретную рассылку нового пользователя
                $intUserId = $objDb->insert_id();
            }
        }

        if ($intUserId > 0) {
            $strSqlQuery = "INSERT INTO `site_users_subscriber_groups_lnk` SET"
                ." susgl_sus_id = ".$intUserId
                .", susgl_susg_id = ".$intRecordId
                .", susgl_date_subscribe = UNIX_TIMESTAMP()";
            if (!$objDb->query($strSqlQuery, false)) {
                # sql error
                $GLOBALS['manStatusError']=2;
                $GLOBALS['manCodeError'][]['code'] = 'msgRecordExist';
            }
        } else {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgNoUserData';
        }


        if (!$GLOBALS['manStatusError']) {
            header('location:'.$arrTplVars['module.name'].'?errMess=msgOk&stPage='.$arrTplVars['stPage'].'&id='.$intRecordId);
            exit;
        }
    }
}

/**
 * Запись данных
 */
if ( isset($_POST['frmEdit']) && $_POST['frmEdit'] == 'true' ) {
    $intRecordId = $arrSqlData['intRecordId'] = intval(trim($_POST['intRecordId']));
    $arrTplVars['intRecordId'] = ( $arrSqlData['intRecordId'] > 0 ? $arrSqlData['intRecordId'] : '');

    $arrSqlData['strTitle'] = addslashes(trim($_POST['strTitle']));
    $arrTplVars['strTitle'] = htmlspecialchars(stripslashes(trim($_POST['strTitle'])));

    if (empty($arrSqlData['strTitle']) ) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgEmptyTitle';
    }

    $arrSqlData['strDesc'] = addslashes(trim($_POST['strDesc']));
    $arrTplVars['strDesc'] = htmlspecialchars(stripslashes(trim($_POST['strDesc'])));

    $arrSqlData['cbxReturn'] = ($_POST['cbxReturn']=='on'?'Y':'N');
    $arrTplVars['cbxReturn'] = ($_POST['cbxReturn']=='on'?' checked':'');

    if ($GLOBALS['manStatusError']!=1) {
        $strSqlFields = ""
            ." susg_title = '{$arrSqlData['strTitle']}'"
            .", susg_desc = '{$arrSqlData['strDesc']}'"
        ;

        if ( empty($intRecordId) ) {
            $strSqlQuery = "INSERT INTO site_users_subscriber_groups SET ".$strSqlFields
                .", susg_date_create = UNIX_TIMESTAMP()";
        } else {
            $strSqlQuery = "UPDATE site_users_subscriber_groups SET".$strSqlFields
                ." WHERE susg_id = ".$intRecordId;
        }

        if ( !$objDb->query( $strSqlQuery ) ) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
        } else {
            if ( empty($intRecordId) ) {
                $intRecordId = $objDb->insert_id();
            }
        }

        if ( $GLOBALS['manStatusError']!=1 ) {
            if ( $arrSqlData['cbxReturn'] == 'Y' ) {
                header('location:'.$arrTplVars['module.name'].'?errMess=msgOk&stPage='.$arrTplVars['stPage'].'&id='.$intRecordId);
            } else {
                header('location:'.$arrTplVars['module.name'].'?errMess=msgOk&stPage='.$arrTplVars['stPage']);
            }
            exit;
        }
    }
}

// ***** Обработка ошибок для вывода *******************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// *****************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if ($_GET['id'] > 0) {
    $arrTplVars['module.title'] = "Редактировать группу подписчиков";
    $intRecordId = $arrTplVars['intRecordId'] = intval($_GET['id']);

    $strSqlQuery = "SELECT * FROM site_users_subscriber_groups WHERE susg_id = ".$intRecordId;
    $arrInfo = $objDb->fetch( $strSqlQuery );

    if ( is_array( $arrInfo )) {
        $arrTplVars['strTitle'] = htmlspecialchars(stripslashes($arrInfo['susg_title']));
        $arrTplVars['strDesc'] = htmlspecialchars(stripslashes($arrInfo['susg_desc']));
        $arrTplVars['strDateCreate'] = date('d.m.Y в H:i', $arrInfo['susg_date_create']);

//        $arrTplVars['cbxStatus'] = $arrInfo['susg_status'] == 'Y' ? ' checked' : '';

        $strSqlFilter = '';
        if (isset($_REQUEST['frmSearch']) && $_REQUEST['frmSearch'] == 'true') {
            if (intval($_REQUEST['intID2Find'])) {
                $arrTplVars['intID2Find'] = $fID = intval($_REQUEST['intID2Find']);
                $strSqlFilter .= " AND sus_id = ".$fID;
            }
            if (!empty($_REQUEST['strEmail2Find'])) {
                $arrTplVars['strEmail2Find'] = htmlspecialchars($_REQUEST['strEmail2Find']);
                $strSqlFilter .= " AND sus_email LIKE '%".mysql_real_escape_string($_REQUEST['strEmail2Find'])."%'";
            }
        }


        /**
         * Достать список подписчиков
         */
        // Всего записей в базе
        $strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM site_users_subscribers LEFT JOIN site_users_subscriber_groups_lnk ON (susgl_sus_id = sus_id) WHERE susgl_susg_id = ".$intRecordId.$strSqlFilter;
        $arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');
        // Выбрано записей
        $strSqlQuery = "SELECT COUNT(*) AS intQuantitySelectRecords FROM site_users_subscribers LEFT JOIN site_users_subscriber_groups_lnk ON (susgl_sus_id = sus_id) WHERE susgl_susg_id = ".$intRecordId.$strSqlFilter;
        $arrTplVars['intQuantitySelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantitySelectRecords');
        // ***** BEGIN: Построение пейджинга для вывода списка
        $objPagination = new tplPaginator($arrTplVars['intQuantitySelectRecords'], SITE_TPL_TPL_DIR, 'p');
        /**
         * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
         * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
         */
        $objPagination->strPaginatorTpl = "site.global.1.tpl";
        $objPagination->intQuantLinkPage = 10;
        $objPagination->intQuantRecordPage = 25;
        $objPagination->strColorLinkStyle = "link-b";
        $objPagination->strColorActiveStyle = "tab-bl-b";
        $objPagination->strNameImageBack = "arrow_one_left.gif";
        $objPagination->strNameImageForward = "arrow_one_right.gif";
        // Создаем блок пэйджинга
        $objPagination->paCreate();
        // ***** END: Построение пейджинга для вывода списка
        // Запрос для выборки нужных записей
        $strSqlQuery = "SELECT tSUS.*, tSUSGL.susgl_date_subscribe FROM site_users_subscribers `tSUS`"
        ." LEFT JOIN site_users_subscriber_groups_lnk tSUSGL ON (susgl_sus_id = sus_id)"
        ." WHERE susgl_susg_id = ".$intRecordId.$strSqlFilter
        ." ORDER BY susgl_date_subscribe DESC, sus_id DESC ".$objPagination->strSqlLimit;
        $arrRecords = $objDb->fetchall( $strSqlQuery );
        // кол-во публикаций показанных на странице
        $arrTplVars['intQuantityShowRecOnPage'] = ( !empty($arrRecords) ? count($arrRecords) : 0 );
        // Присвоение значения пэйджинга для списка
        $arrTplVars['blockPaginator'] = $objPagination->paShow();

        if ( is_array($arrRecords) ) {
            foreach ( $arrRecords as $key => $value ) {
                $arrRecords[$key]['strDateSubs'] = date( "d.m.Y в H:i", $value['susgl_date_subscribe']);
            }
        }

        $objTpl->tpl_loop($arrTplVars['module.name'], "subscribers", $arrRecords);
    }

    $arrIf['form'] = true;
} else {
    $arrIf['list'] = true;

    // Всего записей в базе
    $strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM site_users_subscriber_groups";
    $arrTplVars['intQuantitySelectRecords'] = $arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');
    // Выбрано записей
    //$strSqlQuery = "SELECT COUNT(*) AS intQuantitySelectRecords FROM site_users_subscriber_groups";
    //$arrTplVars['intQuantitySelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantitySelectRecords');
    // ***** BEGIN: Построение пейджинга для вывода списка
    $objPagination = new tplPaginator($arrTplVars['intQuantitySelectRecords'], SITE_TPL_TPL_DIR);
    /**
     * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
     * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
     */
    $objPagination->strPaginatorTpl = "site.global.1.tpl";
    $objPagination->intQuantRecordPage = 15;
    $objPagination->strColorLinkStyle = "link-b";
    $objPagination->strColorActiveStyle = "tab-bl-b";
    $objPagination->strNameImageBack = "arrow_one_left.gif";
    $objPagination->strNameImageForward = "arrow_one_right.gif";
    // Создаем блок пэйджинга
    $objPagination->paCreate();
    // ***** END: Построение пейджинга для вывода списка
    // Запрос для выборки нужных записей
    $strSqlQuery = "SELECT * FROM site_users_subscriber_groups ORDER BY susg_id DESC ".$objPagination->strSqlLimit;
    $arrRecords = $objDb->fetchall( $strSqlQuery );
    // кол-во публикаций показанных на странице
    $arrTplVars['intQuantityShowRecOnPage'] = ( !empty($arrRecords) ? count($arrRecords) : 0 );
    // Присвоение значения пэйджинга для списка
    $arrTplVars['blockPaginator'] = $objPagination->paShow();

    if ( is_array($arrRecords) ) {
        foreach ( $arrRecords as $key => $value ) {
            $arrRecords[$key]['strDateCreate'] = date( "d.m.Y в H:i", $value['susg_date_create']);
//            $arrRecords[$key]['strStatus'] = '';

            $strSqlQuery = "SELECT COUNT(*) as iC FROM `site_users_subscriber_groups_lnk` WHERE `susgl_susg_id` = ".$value['susg_id'];
            $intCount = (int) $objDb->fetch($strSqlQuery, 'iC');

            $arrRecords[$key]['intCountSubs'] = $intCount;
        }
    }

    $objTpl->tpl_loop($arrTplVars['module.name'], "subscriber.groups", $arrRecords);
}


$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);

