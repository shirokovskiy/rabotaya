<?php
$arrTplVars['module.name'] = "news.form";
$arrTplVars['module.parent'] = "news";
$arrTplVars['module.title'] = "Добавить новость";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

include_once "models/News.php";
$objNews = new News();

$fileLimitSize = 1024 * 1024 * 2; // эквивалент 2 Мб

/**
 * Удаление фото
 */
if ( intval($_GET['id']) > 0 && $_GET['act'] == 'del' ) {
    $intRecordId = intval($_GET['id']);
    $objectId = $objNews->removeImageRecord($intRecordId);

    header("Location: {$arrTplVars['module.name']}?id=$objectId&stPage={$arrTplVars['stPage']}");
    exit();
}

/**
 * Запись данных
 */
if ( isset($_POST['frmEditNews']) && $_POST['frmEditNews'] == 'true' ) {
    $intRecordId = $arrSqlData['intRecordId'] = intval(trim($_POST['intRecordId']));
    $arrTplVars['intRecordId'] = ( $arrSqlData['intRecordId'] > 0 ? $arrSqlData['intRecordId'] : '');

    $arrSqlData['strTitle'] = addslashes(trim($_POST['strTitle']));
    $arrTplVars['strTitle'] = htmlspecialchars(stripslashes(trim($_POST['strTitle'])));

    if (empty($arrSqlData['strTitle']) ) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgEmptyTitle';
    }

    $arrSqlData['strNewsBody'] = addslashes(trim($_POST['strNewsBody']));
    $arrTplVars['strNewsBody'] = htmlspecialchars(stripslashes(trim($_POST['strNewsBody'])));

    if (empty($arrSqlData['strNewsBody']) ) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgEmptyNewsbody';
    }

    $dateNewsPublish = !empty($_POST['strDatePublish']) ? date('Y-m-d', strtotime(trim($_POST['strDatePublish']))) : date('Y-m-d');

    if (!$objUtils->dateValid($dateNewsPublish)) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgFailDate';
    }

    $arrSqlData['cbxStatus'] = ($_POST['cbxStatus']=='on'?'Y':'N');
    $arrTplVars['cbxStatus'] = ($_POST['cbxStatus']=='on'?' checked':'');

    $arrSqlData['cbxReturn'] = ($_POST['cbxReturn']=='on'?'Y':'N');
    $arrTplVars['cbxReturn'] = ($_POST['cbxReturn']=='on'?' checked':'');

    if ($GLOBALS['manStatusError']!=1) {
        $strSqlFields = ""
            ." sn_title = '{$arrSqlData['strTitle']}'"
            .", sn_body = '{$arrSqlData['strNewsBody']}'"
            .", sn_date_publ = '$dateNewsPublish'"
            .", sn_status = '{$arrSqlData['cbxStatus']}'"
        ;

        if ( empty($intRecordId) ) {
            $strSqlQuery = "INSERT INTO site_news SET".$strSqlFields
                .", sn_date_add = NOW(), sn_project = ".$_SESSION['intIdDefaultProject'];
        } else {
            $strSqlQuery = "UPDATE site_news SET".$strSqlFields
                .", sn_date_change = NOW()"
                ." WHERE sn_id = $intRecordId AND sn_project = ".$_SESSION['intIdDefaultProject'];
        }

        if ( !$objDb->query( $strSqlQuery ) ) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
        } else {
            if ( empty($intRecordId) ) {
                $intRecordId = $objDb->insert_id();
            }
        }

        /**
         * Загрузка фото
         */
        if ( $_FILES['flPhoto']['error'] == 0 && $_FILES['flPhoto']['size'] > 0 && $_FILES['flPhoto']['size'] < $fileLimitSize ) {
//            $strImageName = "news.photo.$intRecordId";
            $strImageName = $objUtils->translitKyrToLat($_FILES['flPhoto']['name']);
            $strImageName = $objUtils->translitBadChars($strImageName);
            $strImageName = str_replace('.jpg','',$strImageName);

            /////////////////////////////////////////////////////////////////////////////////////
            // extended
            $arrExts['pjpeg'] = 'jpg';
            $arrExts['jpeg'] = 'jpg';

            $mimeType = explode("/", $_FILES['flPhoto']['type']);
            $mimeType = $mimeType[1];

            if ( array_key_exists($mimeType , $arrExts) ) {
                $mimeType = $arrExts[$mimeType];
            }

            if ( $mimeType != 'jpg' ) {
                $GLOBALS['manStatusError']=1;
                $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
            } else {
                $fileName = "$strImageName.$mimeType";
                $fileNameBig = "$strImageName.b.$mimeType";
                $fileNameOrig = "temp.$strImageName.$mimeType";

                $dirImages = PRJ_IMAGES.'news/'.$intRecordId;
                if ( !file_exists($dirImages) ) {
                    mkdir($dirImages, 0775, true);
                }

                if ($GLOBALS['manStatusError']!=1) {
                    if (move_uploaded_file($_FILES['flPhoto']['tmp_name'], $dirImages."/".$fileNameOrig )) {
                        chmod($dirImages."/".$fileNameOrig, 0664);
                    }

                    if ( !is_object($objImage) ) {
                        include_once(SITE_LIB_DIR."cls.images.php");
                        $objImage = new clsImages();
                    }

                    $objImage->resizeImage($dirImages."/".$fileNameOrig, $dirImages."/".$fileName, 200 );
                    $objImage->resizeImage($dirImages."/".$fileNameOrig, $dirImages."/".$fileNameBig, 800 );
                    chmod($dirImages."/".$fileName, 0664);
                    chmod($dirImages."/".$fileNameBig, 0664);
                    unlink($dirImages."/".$fileNameOrig);
                }
            }
            /////////////////////////////////////////////////////////////////////////////////////

            /**
             * Если ошибок нет, значит файл был загружен на сервер, и теперь необходимо добавить запись в БД
             */
            if ($GLOBALS['manStatusError']!=1) {
                // добавить запись о картинке
                $objNews->setImageRecord($intRecordId, $fileNameBig, $arrSqlData['strTitle']);
            }
        } elseif ($_FILES['flPhoto']['size'] > $fileLimitSize) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgTooBigFile';
        }

        if ( $GLOBALS['manStatusError']!=1 ) {
            if ( $arrSqlData['cbxReturn'] == 'Y' ) {
                header('location:'.$arrTplVars['module.name'].'?errMess=msgOk&stPage='.$arrTplVars['stPage'].'&id='.$intRecordId);
            } else {
                header('location:'.$arrTplVars['module.parent'].'?errMess=msgOk&stPage='.$arrTplVars['stPage']);
            }
            exit;
        }
    }
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if ( isset($_GET['id']) && intval($_GET['id']) > 0 ) {
    $arrTplVars['module.title'] = "Редактировать новость";
    $intRecordId = $arrTplVars['intRecordId'] = intval($_GET['id']);

    $strSqlQuery = "SELECT * FROM site_news WHERE sn_id = ".$intRecordId." AND sn_project = ".$_SESSION['intIdDefaultProject'];
    $arrInfo = $objDb->fetch( $strSqlQuery );

    if ( is_array( $arrInfo )) {
        $arrTplVars['strTitle'] = htmlspecialchars(stripslashes($arrInfo['sn_title']));
        $arrTplVars['strNewsBody'] = htmlspecialchars(stripslashes($arrInfo['sn_body']));
        $arrTplVars['strDatePublish'] = date('d.m.Y', strtotime($arrInfo['sn_date_publ']));
        $arrTplVars['cbxStatus'] = $arrInfo['sn_status'] == 'Y' ? ' checked' : '';

        $arrNewsImages = $objNews->getNewsImage($intRecordId);
        if (is_array($arrNewsImages) && !empty($arrNewsImages)) {
            #
            $arrIf['exist.image'] = true;

            foreach ($arrNewsImages as $k => $image) {
                $arrNewsImages[$k]['small_filename'] = str_replace('.b.', '.', $image['si_filename']);
            }
        }
        $objTpl->tpl_loop($arrTplVars['module.name'], "list.images", $arrNewsImages);
    }
}

$arrIf['no.image'] = !$arrIf['exist.image'];

/**
 * Установка дат
 */
// Дни
for ($i=1;$i<=31;$i++) {
    $arrDays[$i]['intDay'] = $i;
    $arrDays[$i]['strDay'] = str_pad($i, 2, "0", STR_PAD_LEFT);
    $arrDays[$i]['selDay'] = ( ($i == $arrTplVars['intDay']) || (!isset($arrTplVars['intDay']) && $i == intval(date('d'))) ? " selected" : "");
}
$objTpl->tpl_loop($arrTplVars['module.name'], "days", $arrDays);


// Месяцы
for ($i=1;$i<=12;$i++) {
    $arrMonths[$i]['intMonth'] = $i;
    $arrMonths[$i]['strMonth'] = $objUtils->arrAtMonth[$i];
    $arrMonths[$i]['selMonth'] = ( ($i == $arrTplVars['intMonth']) || (!isset($arrTplVars['intMonth']) && $i == intval(date('m'))) ? " selected" : "");
}
$objTpl->tpl_loop($arrTplVars['module.name'], "months", $arrMonths);

// Годы
for ($i=2005;$i<=(intval(date("Y"))+3);$i++) {
    $arrYears[$i]['intYear'] = $i;
    $arrYears[$i]['strYear'] = str_pad($i, 2, "0", STR_PAD_LEFT);
    $arrYears[$i]['selYear'] = ( ($i == $arrTplVars['intYear']) || (!isset($arrTplVars['intYear']) && $i == intval(date('Y'))) ? " selected" : "");

}
$objTpl->tpl_loop($arrTplVars['module.name'], "years", $arrYears);

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
