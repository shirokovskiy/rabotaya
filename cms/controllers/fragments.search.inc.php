<?php
$arrTplVars['module.name'] = "fragments.search";
$arrTplVars['module.title'] = "Поиск  фрагмента";
$arrTplVars['module.parent'] = "fragments";
$arrTplVars['module.child'] = "fragments.edit";

if ( $_POST["frmSearchFrg"] == "true" ) {

  $arrSqlData["strFrgSearch"] = addslashes(trim($_POST["strFrgSearch"]));
  $arrTplVars["strFrgSearch"] = htmlspecialchars(trim($_POST["strFrgSearch"]));

  $arrSqlData['strFrgInFrg'] = addslashes(trim($_POST['strFrgInFrg']));
  $arrTplVars['strFrgInFrg'] = htmlspecialchars(stripslashes(trim($_POST['strFrgInFrg'])));

  $arrSqlData['cbxOtherProjects'] = ($_POST['cbxOtherProjects']=='on'?'Y':'N');
  $arrTplVars['cbxOtherProjects'] = ($_POST['cbxOtherProjects']=='on'?' checked':'');

  if ( !empty($arrSqlData["strFrgSearch"]) && empty($arrSqlData['strFrgInFrg']) ) {

    $strSqlQuery = "SELECT sf_id, sf_id_parent, sf_id_project, sf_name, sf_id_name FROM ".$_db_tables["stFragments"]
      ." WHERE sf_id_name LIKE '%". $arrSqlData["strFrgSearch"] ."%'"
      . ( $arrSqlData['cbxOtherProjects'] != 'Y' ? " AND sf_id_project = '".$_SESSION['intIdDefaultProject']."'" : "" )
      ;
    $arrFrgData = $objDb->fetchall( $strSqlQuery );

  } elseif ( !empty($arrSqlData['strFrgInFrg']) && empty($arrSqlData["strFrgSearch"])) {

    $strSqlQuery = "SELECT sf_id, sf_id_parent, sf_id_project, sf_name, sf_id_name FROM ".$_db_tables["stFragments"]
      ." WHERE sf_body LIKE '%[frg ". $arrSqlData["strFrgInFrg"] ."]%'"
      . ( $arrSqlData['cbxOtherProjects'] != 'Y' ? " AND sf_id_project = '".$_SESSION['intIdDefaultProject']."'" : "" )
      ;
    $arrFrgData = $objDb->fetchall( $strSqlQuery );
  } elseif ( !empty($arrSqlData['strFrgInFrg']) && !empty($arrSqlData["strFrgSearch"])) {

    $strSqlQuery = "SELECT sf_id, sf_id_parent, sf_id_project, sf_name, sf_id_name FROM ".$_db_tables["stFragments"]
      ." WHERE sf_body LIKE '%[frg ". $arrSqlData["strFrgInFrg"] ."]%'"
      ." AND sf_id_name LIKE '%". $arrSqlData["strFrgSearch"] ."%'"
      . ( $arrSqlData['cbxOtherProjects'] != 'Y' ? " AND sf_id_project = '".$_SESSION['intIdDefaultProject']."'" : "" )
      ;
    $arrFrgData = $objDb->fetchall( $strSqlQuery );
  }

  if ( !empty( $arrFrgData ) ) {
    foreach ( $arrFrgData as $key => $frgData ) {

      if ( !is_null( $frgData["sf_id_parent"] ) ) {

        $strSqlQuery = "SELECT * FROM site_fragments_group"
          ." WHERE sfg_id = ".$frgData["sf_id_parent"]
          . ( $arrSqlData['cbxOtherProjects'] != 'Y' ? " AND sfg_id_project = '".$_SESSION['intIdDefaultProject']."'" : "" )
          ;
        $arrChapterFrg = $objDb->fetch( $strSqlQuery );

        if ( !empty( $arrChapterFrg ) ) {
          if ( $arrChapterFrg['sfg_id_project'] != $_SESSION['intIdDefaultProject'] ) {
            $strSqlQuery = "SELECT fp_name strOtherProjectName FROM cms_projects"
                ." WHERE fp_id='{$arrChapterFrg['sfg_id_project']}'";
            $arrFrgData[$key]["pProjectName"] = $objDb->fetch( $strSqlQuery , 'strOtherProjectName');
          } else {
            $arrIf['this.project.'.$frgData['sf_id']] = true;
            $arrFrgData[$key]["pProjectName"] = "текущий проект";
          }

          $arrFrgData[$key]["pChapterName"] = $arrChapterFrg["sfg_name"];
          $arrFrgData[$key]["pChapterID"]   = $arrChapterFrg["sfg_id"];
          $arrFrgData[$key]["hrefChapter"]  = ( $arrIf['this.project.'.$frgData['sf_id']] ? $arrTplVars['module.parent'].'?idItem='.$arrChapterFrg["sfg_id"] : "" );
          $arrFrgData[$key]["hrefProject"]  = "";
        }
      }
    }
  }
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if ( !empty( $arrFrgData ) ) {
  $objTpl->tpl_loop($arrTplVars['module.name'], "lst.searched.frgs", $arrFrgData );
  $arrIf["alias.searched"] = true;
} else {
  if ( $_POST["frmSearchFrg"] == "true" ) {
    $arrIf["alias.not.found"] = true;
  }
}

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
