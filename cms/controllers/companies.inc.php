<?php
$arrTplVars['module.name'] = "companies";
$arrTplVars['module.child'] = "company.form";
$arrTplVars['module.title'] = "Компании";

include_once "models/Company.php";
$objCompany = new Company($_db_config);

include_once "models/Vacancy.php";
$objVacancy = new Vacancy($_db_config);

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

/**
 * Удаление
 */
if ( intval($_GET['id']) > 0 && $_GET['act'] == 'del' ) {
    $intRecordId = intval($_GET['id']);
    // Проверить привязку к вакансиям
    $strSqlQuery = "SELECT * FROM `job_vacancies` WHERE `jv_jc_id` = ".$intRecordId;
    $arrVacs = $objDb->fetchall($strSqlQuery);
    if ($arrVacs===null) {
        $strSqlQuery = "DELETE FROM `job_companies` WHERE jc_id = ".$intRecordId;
        if ( !$objDb->query($strSqlQuery) ) {
            #mysql error
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
        } else {
            // delete logo
            $strSqlQuery = "SELECT si_id, si_filename FROM `site_images` WHERE `si_type` = 'company' AND `si_rel_id` = ".$intRecordId;
            $arrFilename = $objDb->fetch($strSqlQuery);
            if (is_array($arrFilename) && !empty($arrFilename)) {
                $intImgId = $arrFilename["si_id"];
                $strFilename = $arrFilename["si_filename"];
            }

            if(!empty($strFilename) && file_exists(PRJ_IMAGES.'companies/'.$strFilename)) {
                $strSqlQuery = "DELETE FROM site_images WHERE si_id=".$intImgId." AND `si_type` = 'company' LIMIT 1";
                if ( !$objDb->query($strSqlQuery) ) {
                    #mysql error
                    $GLOBALS['manStatusError']=1;
                    $GLOBALS['manCodeError'][]['code'] = 'msgErrorDel';
                } else {
                    if(!unlink(PRJ_IMAGES.'companies/'.$strFilename)) {
                        $GLOBALS['manStatusError']=1;
                        $GLOBALS['manCodeError'][]['code'] = 'msgErrorDelFile';
                    }
                }
            }

//            foreach (glob(PRJ_IMAGES.'companies/logo.'.$intRecordId.'.*') as $filename) {
//                if( file_exists(PRJ_IMAGES.'companies/'.$filename) ) {
//                    unlink(PRJ_IMAGES.'companies/'.$filename);
//                    break;
//                }
//            }
        }
    } else {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgCompNotEmpty';
    }


    if (!$GLOBALS['manStatusError']) {
        // todo: также удалить лого и проверить привязку к вакансиям
        $_SESSION['manCodeError'][]['code'] = 'msgDelOk';
        header("Location: {$arrTplVars['module.name']}?stPage={$arrTplVars['stPage']}");
        exit();
    }
}

// ***** Обработка ошибок для вывода *******************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// *****************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

/**
 * Подготовка сессионного поиска
 */
if ( isset($_POST['frmSearch']) ) {
    $_SESSION['frmSearch'] = $_POST;
}

if ( !isset($_POST['frmSearch']) && intval($_GET['stPage']) <= 0 ) {
    unset($_SESSION['frmSearch']);
}

if ( isset($_SESSION['frmSearch']) ) {
    if ( intval( $_SESSION['frmSearch']['intID2Find'] )>0 ) {
        $intFindID = $arrTplVars['intID2Find'] = $_SESSION['frmSearch']['intID2Find'];
        $arrSqlWhere[] = " jc_id = '$intFindID' ";
    }
    if ( !empty( $_SESSION['frmSearch']['strTitle2Find'] ) ) {
        $strFindName = $arrTplVars['strTitle2Find'] = mysql_real_escape_string( $_SESSION['frmSearch']['strTitle2Find'] );
        $arrSqlWhere[] = " jc_title LIKE '%$strFindName%' ";
    }
    if ( !empty( $_SESSION['frmSearch']['strCity2Find'] ) ) {
        $strFindCity = $arrTplVars['strCity2Find'] = mysql_real_escape_string( $_SESSION['frmSearch']['strCity2Find'] );
        $arrSqlWhere[] = " jc_city LIKE '%$strFindCity%' ";
    }
    if ( !empty( $_SESSION['frmSearch']['cbxVed'] ) ) {
        $sqlData['cbxVed'] =  $_SESSION['frmSearch']['cbxVed'] ? 'Y' : 'N';
        $arrTplVars['cbxVed'] =  $_SESSION['frmSearch']['cbxVed'] ? ' checked' : '';
        $arrSqlWhere[] = " jc_top = '{$sqlData['cbxVed']}' ";
    }
}

if ( is_array($arrSqlWhere) && !empty($arrSqlWhere) ) {
    $strSqlWhere = implode(' AND ', $arrSqlWhere);
}



if (isset($_GET['export']) && $_GET['export'] == 'csv') {
    /**
     * Выборка для экспорта
     */
    $strSqlQuery = "SELECT jc_title, jc_email, jc_phone FROM job_companies".( !empty( $strSqlWhere ) ? " WHERE ".$strSqlWhere : '' )." ORDER BY jc_id DESC LIMIT 10000";
    $arrRecords = $objDb->fetchall( $strSqlQuery );

    if (is_array($arrRecords) && !empty($arrRecords)) {

        if ( $_GET['type'] == 'email' ) {
            $strFileNameInfo = "companies.emails.results.csv";
        } else {
            $strFileNameInfo = "companies.phone.results.csv";
        }

        $SourceFile = PRJ_FILES.$strFileNameInfo;
        $fp = fopen($SourceFile, "w");

        foreach ($arrRecords as $value) {
            if ( $_GET['type'] == 'email' ) {
                if (!empty($value['jc_email'])) {
                    fwrite($fp, '"'.str_replace('"','',$value['jc_title']).'", "'.$value['jc_email'].'"'. "\n");
                }

            } else {
                if (!empty($value['jc_phone'])) {
                    $phone = preg_replace('/-|\)|\(|раб|моб|\s/', '', $value['jc_phone']);
                    fwrite($fp, '"'.str_replace('"','',$value['jc_title']).'", "'.$phone.'"'. "\n");
                }
            }
        }

        fclose($fp);

        if (file_exists($SourceFile)) {
            $sizeResFile = filesize($SourceFile);

            Header( "Expires: Mon, 5 Jul 1977 09:30:00 GMT\r\n" );
            Header( "Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT\r\n" );
            Header( "Cache-Control: no-store, no-cache, must-revalidate\r\n" );
            Header( "Cache-Control: post-check=0, pre-check=0, false\r\n" );
            Header( "Pragma: no-cache\r\n" );
            Header( "HTTP/1.1 200 OK\r\n" );

            Header( "Content-Disposition: attachment; filename=".date('Y.m.d_H.i_')."$strFileNameInfo\r\n" );
            Header( "Accept-Ranges: bytes\r\n" );
            Header( "Content-Type: application/force-download" );
            Header( "Content-Length: $sizeResFile\r\n\r\n" );

            readfile( $SourceFile );
            die();
        } else {
            header ("HTTP/1.0 404 Not Found");
            $arrTplVars['strMessage'] = "Ошибка! Файл не существует.";
        }
    }
}



// Всего записей в базе
$strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM `job_companies`";
$arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');
// Выбрано записей
$strSqlQuery = "SELECT COUNT(*) AS intQuantitySelectRecords FROM `job_companies`".( !empty( $strSqlWhere ) ? " WHERE ".$strSqlWhere : '' );
$arrTplVars['intQuantitySelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantitySelectRecords');
// ***** BEGIN: Построение пейджинга для вывода списка
$objPagination = new tplPaginator($arrTplVars['intQuantitySelectRecords'], SITE_TPL_TPL_DIR);
/**
 * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
 * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
 */
$objPagination->strPaginatorTpl = "site.global.1.tpl";
$objPagination->intQuantRecordPage = 15;
$objPagination->strColorLinkStyle = "link-b";
$objPagination->strColorActiveStyle = "tab-bl-b";
$objPagination->strNameImageBack = "arrow_one_left.gif";
$objPagination->strNameImageForward = "arrow_one_right.gif";
// Создаем блок пэйджинга
$objPagination->paCreate();
// ***** END: Построение пейджинга для вывода списка
// Запрос для выборки нужных записей
$strSqlQuery = "SELECT * FROM `job_companies`".( !empty( $strSqlWhere ) ? " WHERE ".$strSqlWhere : '' )." ORDER BY jc_title, jc_id DESC ".$objPagination->strSqlLimit;
$arrRecords = $objDb->fetchall( $strSqlQuery );
// кол-во публикаций показанных на странице
$arrTplVars['intQuantityShowRecOnPage'] = ( !empty($arrRecords) ? count($arrRecords) : 0 );
// Присвоение значения пэйджинга для списка
$arrTplVars['blockPaginator'] = $objPagination->paShow();

if ( is_array($arrRecords) ) {
    foreach ( $arrRecords as $key => $value ) {
        $arrRecords[$key]['cbxStatus'] = $value['jc_status'] == 'Y' ? ' checked' : '';
        $arrIf['attach.'.$value['jc_id']] = ( file_exists(DROOT."storage/files/images/companies/logo.".$value['jc_id'].".jpg") && $objCompany->getLogoImage($value['jc_id'])!==false);

        $arrRecords[$key]['intVacs'] = $objVacancy->getCountByCompany($value['jc_id']);
    }
}

$objTpl->tpl_loop($arrTplVars['module.name'], "records", $arrRecords);

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);

