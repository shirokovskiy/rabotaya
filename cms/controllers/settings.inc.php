<?php
$arrTplVars['module.name'] = "settings";
$arrTplVars['module.title'] = "Настройки";


// ***** Непосредственно смена пароля *****
if (!empty($_POST['submitChangePwd'])) {
  $pass1 = trim($_POST['PwdChangeOne']);
  $pass2 = trim($_POST['PwdChangeToo']);

  if (empty($pass1) || empty($pass2)) {
    $GLOBALS['manStatusError'] = 1;
    $GLOBALS['manCodeError'][]['code'] = '114';
  }

  if ( $pass1 != $pass2 ) {
    $GLOBALS['manStatusError'] = 1;
    $GLOBALS['manCodeError'][]['code'] = '115';
  }

  if (eregi("[а-яА-Я ]+", $pass1)) {
    $GLOBALS['manStatusError'] = 1;
    $GLOBALS['manCodeError'][]['code'] = '116';
  }

  if ( $GLOBALS['manStatusError'] != 1 ) {
    $strSqlQuery = "UPDATE cms_users SET"
      ." cu_password = MD5('".$pass1."')"
      ." WHERE cu_id = '".$_SESSION['userInfoStorage']['cu_id']."'";

    if ( !$objDb->query( $strSqlQuery )) {
      $GLOBALS['manStatusError']=1;
      $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
    } else {
      $GLOBALS['manCodeError'][]['code'] = '117';
    }
  }
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");



$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
