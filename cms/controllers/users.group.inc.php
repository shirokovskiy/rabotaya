<?php
$arrTplVars['module.name'] = "users.group";
$arrTplVars['module.title'] = "Группы пользователей";

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

$strSqlQuery = "SELECT * FROM cms_users_group";
$arrUsersGroup = $objDb->fetchall( $strSqlQuery );

foreach ( $arrUsersGroup as $key => $value ) {
    $arrUsersGroup[$key]['chStatus'] = ( $value['cug_status'] == 'Y' ? ' checked' : '' );
}
$objTpl->tpl_loop($arrTplVars['module.name'], "list.users.group", $arrUsersGroup);

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
