<?php
$arrTplVars['module.name'] = "module.search";
$arrTplVars['module.access'] = "module.access";
$arrTplVars['module.child'] = "module.form";
$arrTplVars['module.title'] = "Поиск модуля WMBM";

if ( $_POST['frmModuleSearch'] == 'true' ) {

  $arrSqlData['strModuleName'] = addslashes(trim($_POST['strModuleName']));
  $arrTplVars['strModuleName'] = htmlspecialchars(stripslashes(trim($_POST['strModuleName'])));

  $arrSqlData['strModuleAlias'] = addslashes(trim($_POST['strModuleAlias']));
  $arrTplVars['strModuleAlias'] = htmlspecialchars(stripslashes(trim($_POST['strModuleAlias'])));

  if ( empty($arrSqlData['strModuleName']) && empty($arrSqlData['strModuleAlias']) ) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
  } elseif (empty($arrSqlData['strModuleName']) && !empty($arrSqlData['strModuleAlias'])) {
    $strSqlQuery = "SELECT * FROM cms_modules WHERE cm_link LIKE '%".$arrSqlData['strModuleAlias']."%'";
  } elseif (!empty($arrSqlData['strModuleName']) && empty($arrSqlData['strModuleAlias'])) {
    $strSqlQuery = "SELECT * FROM cms_modules WHERE cm_name LIKE '%".$arrSqlData['strModuleName']."%'";
  } else {
    $strSqlQuery = "SELECT * FROM cms_modules WHERE cm_name LIKE '%".$arrSqlData['strModuleName']."%' OR cm_link LIKE '%".$arrSqlData['strModuleAlias']."%'";
  }

  if ( $GLOBALS['manStatusError']!=1 ) {
    $arrModulesList = $objDb->fetchall( $strSqlQuery );
  }
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if ( !empty( $arrModulesList ) ) {
  $objTpl->tpl_loop($arrTplVars['module.name'], "list.modules", $arrModulesList);
  $arrIf['is.searched'] = true;
} else {
  $arrIf['no.result'] = true;
}

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
