<?php
$arrTplVars['module.name'] = "user.search";
$arrTplVars['module.child'] = "users.edit";
$arrTplVars['module.title'] = "Поиск пользователя";

if ( $_POST['frmUserSearch'] == 'true' ) {

  $arrSqlData['strUserNameSearch'] = addslashes(trim($_POST['strUserNameSearch']));
  $arrTplVars['strUserNameSearch'] = htmlspecialchars(stripslashes(trim($_POST['strUserNameSearch'])));

  $arrSqlData['strUserLoginSearch'] = addslashes(trim($_POST['strUserLoginSearch']));
  $arrTplVars['strUserLoginSearch'] = htmlspecialchars(stripslashes(trim($_POST['strUserLoginSearch'])));

  if ( empty($arrSqlData['strUserNameSearch']) && empty($arrSqlData['strUserLoginSearch']) ) {
    $GLOBALS['manStatusError']=1;
    $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
  } elseif (empty($arrSqlData['strUserNameSearch']) && !empty($arrSqlData['strUserLoginSearch'])) {
    $strSqlQuery = "SELECT * FROM cms_users WHERE cu_login LIKE '%".$arrSqlData['strUserLoginSearch']."%'";
  } elseif (!empty($arrSqlData['strUserNameSearch']) && empty($arrSqlData['strUserLoginSearch'])) {
    $strSqlQuery = "SELECT * FROM cms_users WHERE cu_fname LIKE '%".$arrSqlData['strUserNameSearch']."%' OR cu_lname LIKE '%".$arrSqlData['strUserNameSearch']."%' OR cu_pname LIKE '%".$arrSqlData['strUserNameSearch']."%'";
  } else {
    $strSqlQuery = "SELECT * FROM cms_users WHERE cu_login LIKE '%".$arrSqlData['strUserLoginSearch']."%' OR cu_fname LIKE '%".$arrSqlData['strUserNameSearch']."%' OR cu_lname LIKE '%".$arrSqlData['strUserNameSearch']."%' OR cu_pname LIKE '%".$arrSqlData['strUserNameSearch']."%'";
  }

  if ( $GLOBALS['manStatusError']!=1 ) {
    $arrUsersList = $objDb->fetchall( $strSqlQuery );
  }
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if ( !empty( $arrUsersList ) ) {
  foreach ( $arrUsersList as $key => $value ) {
    $arrUsersList[$key]['strFIO'] = $value['cu_lname']." ".$value['cu_fname']." ".$value['cu_pname'];
    $arrUsersList[$key]['strLastEnter'] = $objUtils->workDate(4, $value['cu_last_enter']);
  }
  $objTpl->tpl_loop($arrTplVars['module.name'], "list.users", $arrUsersList);
  $arrIf['is.searched'] = true;
}

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
