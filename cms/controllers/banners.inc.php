<?php
$arrTplVars['module.name'] = "banners";
$arrTplVars['module.title'] = "Реклама";

$arrTplVars['stPage'] = $stPage = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

$arrRasp['top'] = "в верхнем блоке (800 x 160)";
$arrRasp['left'] = "в левом блоке";
$arrRasp['right'] = "в правом блоке (240 x 375)";
$i = 1;
foreach ($arrRasp as $k => $value) {
    $arrRaspList[$i]['rasp'] = $value;
    $arrRaspList[$i]['x'] = $i;
    $arrRaspList[$i]['ch'] = $k;
    $i++;
}
$fileLimitSize = 1024 * 1024 * 2; // эквивалент 2 Мб

/**
 * Удалить изображение
 */
if ( intval($_GET['id_del']) > 0 ) {
	$intRecordId = intval($_GET['id_del']);
	$strSqlQuery = "SELECT si_filename FROM site_images WHERE si_id=".$intRecordId." AND si_project = ".$_SESSION['intIdDefaultProject'];
	$strFilename = $objDb->fetch($strSqlQuery, "si_filename");

	$strSqlQuery = "DELETE FROM site_images WHERE si_id=".$intRecordId." AND si_project = ".$_SESSION['intIdDefaultProject']." LIMIT 1";
	if ( !$objDb->query($strSqlQuery) ) {
		#mysql error
		$GLOBALS['manStatusError']=1;
		$GLOBALS['manCodeError'][]['code'] = 'msgErrorDel';
	} else {
		if (file_exists(PRJ_IMAGES.'banners/'.$strFilename)) {
			if(!unlink(PRJ_IMAGES.'banners/'.$strFilename)) {
				$GLOBALS['manStatusError']=1;
				$GLOBALS['manCodeError'][]['code'] = 'msgErrorDelFile';
			}
		}
	}

	if (!$GLOBALS['manStatusError']) {
		header('Location: '.$arrTplVars['module.name'].'?stPage='.$stPage);
		exit;
	}
}

/**
 * Запись файла
 */
if ( isset($_POST['frmEditForm']) && $_POST['frmEditForm'] == 'true' ) {
	$intRecordId = $arrSqlData['intRecordId'] = intval(trim($_POST['intRecordId']));
	$arrTplVars['intRecordId'] = ( $arrSqlData['intRecordId'] > 0 ? $arrSqlData['intRecordId'] : '');

	$arrSqlData['strFileDesc'] = addslashes(trim($_POST['strFileDesc']));
	$arrTplVars['strFileDesc'] = htmlspecialchars(stripslashes(trim($_POST['strFileDesc'])));

	if ( empty($arrSqlData['strFileDesc']) ) {
		$GLOBALS['manStatusError']=1;
		$GLOBALS['manCodeError'][]['code'] = 'msgEmptyFileDesc';
	}

	if ( empty($_POST['strBannerContent']) && ($_FILES['flPhoto']['error'] != 0 || $_FILES['flPhoto']['size'] == 0) && empty($intRecordId) ) {
		$GLOBALS['manStatusError']=1;
		$GLOBALS['manCodeError'][]['code'] = 'msgNoFile';
	}

	$arrSqlData['strFileUrlTo'] = addslashes(trim($_POST['strFileUrlTo']));
	$arrTplVars['strFileUrlTo'] = htmlspecialchars(stripslashes(trim($_POST['strFileUrlTo'])));

	if (!empty($arrSqlData['strFileUrlTo']) ) {
		preg_replace("/^http\:\/\//i", "", $arrSqlData['strFileUrlTo']);
	}

    if (isset($_POST['isFileExists']) && $_POST['isFileExists']=='Y') { // если картинка уже подгружена
        $strImageName = $_POST['strFileName'];
    }
    if (is_uploaded_file($_FILES['flPhoto']['tmp_name'])) {
        $strImageName = strtolower( substr($objUtils->translitKyrToLat($objUtils->translitBadChars($_POST['strFileDesc'], array(' '))), 0, 16) );
    }

	$arrSqlData['rbWhere'] = trim($_POST['rbWhere']);
	$arrTplVars['ch_'.$arrSqlData['rbWhere']] = ' checked';

	$arrSqlData['strBannerContent'] = mysql_real_escape_string(trim($_POST['strBannerContent']));
	$arrTplVars['strBannerContent'] = htmlspecialchars(trim($_POST['strBannerContent']));

	$arrSqlData['cbxStatus'] = ($_POST['cbxStatus']=='on'?'Y':'N');
	$arrTplVars['cbxStatus'] = ($_POST['cbxStatus']=='on'?' checked':'');

	$arrTplVars['cbxReturn'] = ($_POST['cbxReturn']=='on'?' checked':'');

	if ( $GLOBALS['manStatusError'] != 1 ) {
		$strSqlFields = ""
			." si_desc = '{$arrSqlData['strFileDesc']}'"
			. ( !empty($arrSqlData['strFileUrlTo']) ? ", si_url = '{$arrSqlData['strFileUrlTo']}'" : "" )
			.", si_type = 'banner'"
			.", si_banner_type = '{$arrSqlData['rbWhere']}'"
			.", si_banner_content = '{$arrSqlData['strBannerContent']}'"
			.", si_status = '{$arrSqlData['cbxStatus']}'"
			.", si_filename = '$strImageName'"
		;

		if ( empty($intRecordId) ) {
			$strSqlQuery = "INSERT INTO site_images SET ".$strSqlFields.", si_date_add = NOW(), si_project = ".$_SESSION['intIdDefaultProject'];
		} else {
			$strSqlQuery = "UPDATE site_images SET ".$strSqlFields.", si_date_change = NOW() WHERE si_id = ".$intRecordId." AND si_project = ".$_SESSION['intIdDefaultProject'];
		}

		if ( !$objDb->query( $strSqlQuery ) ) {
			$GLOBALS['manStatusError']=1;
			$GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
		} else {
			$intRecordId = empty($intRecordId) ? $objDb->insert_id() : $intRecordId;
		}

		if ($GLOBALS['manStatusError']!=1) {
			if ( $_FILES['flPhoto']['error'] == 0 && $_FILES['flPhoto']['size'] < $fileLimitSize ) {
				/////////////////////////////////////////////////////////////////////////////////////
				// extended
				$arrExts['pjpeg'] = 'jpg';
				$arrExts['jpeg'] = 'jpg';

				$mimeType = explode("/", $_FILES['flPhoto']['type']);
				$mimeType = $mimeType[1];

				if ( array_key_exists($mimeType , $arrExts) ) {
					$mimeType = $arrExts[$mimeType];
				}

				if ( $mimeType != 'jpg' && $mimeType != 'gif' && $mimeType != 'png' ) {
					$GLOBALS['manStatusError']=1;
					$GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
				} else {
					$fileName = "$strImageName.$mimeType";
					$fileNameOrig = "temp.$strImageName.$mimeType";

					$dirImages = PRJ_IMAGES.'banners';
					if ( !file_exists($dirImages) ) {
						mkdir($dirImages, 0775, true);
					}
				}

				if ($GLOBALS['manStatusError']!=1) {
					// предварительно запишем новое имя файла, потому что такой файл может быть уже записан на сервер, а поле БД ещё не изменено
					$strSqlQuery = "UPDATE site_images SET si_filename='$fileName', si_date_change = NOW() WHERE si_id=".$intRecordId;
					if ( !$objDb->query($strSqlQuery) ) {
						#mysql error
						$GLOBALS['manStatusError']=1;
						$GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
					}

                    if (file_exists($dirImages."/".$fileName) && is_uploaded_file($_FILES['flPhoto']['tmp_name'])) {
                        unlink($dirImages."/".$fileName);
                    }

					if (!file_exists($dirImages."/".$fileName)) {
						if ( move_uploaded_file( $_FILES['flPhoto']['tmp_name'], $dirImages."/".$fileNameOrig ) ) {
                            chmod($dirImages."/".$fileNameOrig, 0664);
                        }

						if ( !is_object($objImage) ) {
							include_once("cls.images.php");
							$objImage = new clsImages();
						}

						switch ($arrSqlData['rbWhere']) {
							case 'left':
								$intNeededWidth = 133; // Необходимая ширина картинок баннеров
								break;
							case 'top':
							case 'top_r':
								$intNeededWidth = 800; // Необходимая ширина картинок баннеров
								$intNeededHeight = 160; // Необходимая высота картинок баннеров
								break;
							case 'right':
								$intNeededWidth = 240; // Необходимая ширина картинок баннеров
								$intNeededHeight = 375; // Необходимая высота картинок баннеров
								break;
							default:
								break;
						}

						$arrPreloadSize = getimagesize($dirImages."/".$fileNameOrig);
						if ($arrPreloadSize[0]<=$intNeededWidth) {
							rename($dirImages."/".$fileNameOrig,  $dirImages."/".$fileName);
						} else {
							if ( $intNeededHeight > 0 ) {
								$objImage->resizeImage($dirImages."/".$fileNameOrig, $dirImages."/".$fileName, $intNeededWidth, $intNeededHeight );
							} else {
								$objImage->resizeImage($dirImages."/".$fileNameOrig, $dirImages."/".$fileName, $intNeededWidth );
							}
							unlink($dirImages."/".$fileNameOrig);
						}
					} else {
						$GLOBALS['manStatusError']=1;
						$GLOBALS['manCodeError'][]['code'] = 'msgFileAlreadyExist';
					}
				}
				/////////////////////////////////////////////////////////////////////////////////////
			} elseif ($_FILES['flPhoto']['size'] > $fileLimitSize) {
				$GLOBALS['manStatusError']=1;
				$GLOBALS['manCodeError'][]['code'] = 'msgTooBigFile';
			}
		}

		if ( $GLOBALS['manStatusError']!=1 ) {
			if ( $_POST['cbxReturn']=='on' ) {
				header('location:'.$arrTplVars['module.name']."?errMess=msgOk&id=$intRecordId&stPage=$stPage");
			} else {
				header('location:'.$arrTplVars['module.name']."?errMess=msgOk&stPage=$stPage");
			}
			exit;
		}
	}
}

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

$arrTplVars['isFileExists'] = 'N';

if ( $_GET['action'] == 'add' || (isset($_GET['id']) && intval($_GET['id']) > 0) ) {
	$arrIf['editrecord'] = true;
	$intRecordId = $arrTplVars['intRecordId'] = intval($_GET['id']);

	$strSqlQuery = "SELECT * FROM site_images WHERE si_id = ".$intRecordId." AND si_project = ".$_SESSION['intIdDefaultProject'];
	$arrInfo = $objDb->fetch( $strSqlQuery );

	if ( is_array( $arrInfo )) {
		$arrTplVars['strFileDesc'] = htmlspecialchars(stripslashes($arrInfo['si_desc']));
		$arrTplVars['strFileUrlTo'] = htmlspecialchars(stripslashes($arrInfo['si_url']));
		$arrTplVars['strFileName'] = htmlspecialchars(stripslashes($arrInfo['si_filename']));
		$arrTplVars['strBannerContent'] = htmlspecialchars(stripslashes($arrInfo['si_banner_content']));
		$arrTplVars['ch_'.$arrInfo['si_banner_type']] = ' checked';

		if (file_exists(PRJ_IMAGES.'banners/'.$arrInfo['si_filename'])) {
			$arrIf['is.image'] = true;
            $arrTplVars['isFileExists'] = 'Y';
		}

		$arrTplVars['cbxStatus'] = $arrInfo['si_status'] == 'Y' ? ' checked' : '';
	}
} else {
	// Всего записей в базе
	$strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM site_images WHERE si_type = 'banner' AND si_project = ".$_SESSION['intIdDefaultProject'];
	$arrTplVars['intQuantAllRecords'] = $arrTplVars['intQuantitySelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');
	// ***** BEGIN: Построение пейджинга для вывода списка
	$objPagination = new tplPaginator($arrTplVars['intQuantitySelectRecords'], SITE_TPL_TPL_DIR);
	/**
	 * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
	 * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
	 */
	$objPagination->strPaginatorTpl = "site.global.1.tpl";
	$objPagination->intQuantRecordPage = 12;
	$objPagination->strColorLinkStyle = "link-b";
	$objPagination->strColorActiveStyle = "tab-bl-b";
	$objPagination->strNameImageBack = "arrow_one_left.gif";
	$objPagination->strNameImageForward = "arrow_one_right.gif";
	// Создаем блок пэйджинга
	$objPagination->paCreate();
	// ***** END: Построение пейджинга для вывода списка
	// Запрос для выборки нужных записей
	$strSqlQuery = "SELECT * FROM site_images WHERE si_type = 'banner' AND si_project = ".$_SESSION['intIdDefaultProject']." ORDER BY si_id DESC ".$objPagination->strSqlLimit;
	$arrSelectedRecords = $objDb->fetchall( $strSqlQuery );
	// кол-во публикаций показанных на странице
	$arrTplVars['intQuantityShowRecOnPage'] = ( !empty($arrSelectedRecords) ? count($arrSelectedRecords) : 0 );
	// Присвоение значения пэйджинга для списка
	$arrTplVars['blockPaginator'] = $objPagination->paShow();

	$arrIf['records'] = $arrTplVars['intQuantityShowRecOnPage']>0;

	if ( is_array($arrSelectedRecords) ) {
		foreach ( $arrSelectedRecords as $key => $value ) {
			$arrSelectedRecords[$key]['cbxStatus'] = $value['si_status'] == 'Y' ? ' checked' : '';
			$arrSelectedRecords[$key]['strRasp'] = $arrRasp[$value['si_banner_type']];
		}
	}

	$objTpl->tpl_loop($arrTplVars['module.name'], "list", $arrSelectedRecords, array($_SESSION['userConfig']['cBgColor'], $_SESSION['userConfig']['cThirdColor']));
}

$arrIf['showlist'] = !$arrIf['editrecord'];
$arrIf['norecords'] = !$arrIf['records'];

$objTpl->tpl_loop($arrTplVars['module.name'], "list.rasp", $arrRaspList);
$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);

