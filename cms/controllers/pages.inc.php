<?php
$arrTplVars['module.name'] = "pages";
$arrTplVars['module.title'] = "СТРАНИЦЫ ПРОЕКТА";
$arrTplVars['module.child'] = "pages.edit";

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");


// ***** Кол-во страниц
$strSqlQuery = "SELECT COUNT(*) AS strQuantPages FROM site_pages WHERE sp_id_project='".$_SESSION['intIdDefaultProject']."'";
$arrTplVars['strQuantPages'] = $objDb->fetch( $strSqlQuery , 'strQuantPages');

$strSqlQuery = "SELECT COUNT(*) AS strActiveQuantPages FROM site_pages WHERE sp_id_project='".$_SESSION['intIdDefaultProject']."' AND sp_status='Y'";
$arrTplVars['strActiveQuantPages'] = $objDb->fetch( $strSqlQuery , 'strActiveQuantPages');

$arrTplVars['strActiveNoQuantPages'] = $arrTplVars['strQuantPages']-$arrTplVars['strActiveQuantPages'];

// Формируем список групп
$strSqlQuery = "SELECT * FROM site_pages_group WHERE spg_id_project='".$_SESSION['intIdDefaultProject']."' AND spg_id_parent IS NULL AND spg_status='Y'";
$arrLstItem = $objDb->fetchall( $strSqlQuery );

$arrTplVars['intIdItem'] = intval($_GET['idItem']);

if(is_array($arrLstItem)) {
    foreach($arrLstItem as $key=>$value) {
        $strTmpStyleHref = ($arrTplVars['intIdItem']==$arrLstItem[$key]['spg_id']) ? ' active' : '';
        $arrItemForTpl[$key] = array('intIdItem'=>$arrLstItem[$key]['spg_id'], 'strNameItem'=>$arrLstItem[$key]['spg_name'], 'strHrefStyle'=>$strTmpStyleHref);
        $arrParent[$arrLstItem[$key]['spg_id']] = true;
    }
} else { // Если каталог пуст (нет групп страниц)
    $arrIf['block.catalogue.empty'] = true;
}

$objTpl->tpl_loop($arrTplVars['module.name'], "lst.item", $arrItemForTpl);

if($arrTplVars['intIdItem']>0) { // Если ID выбранного каталога больше 0
    if($arrParent[$arrTplVars['intIdItem']]!=true)
        $inIdParentSubItem = $objDb->fetch( "SELECT spg_id_parent FROM site_pages_group WHERE spg_id_project='".$_SESSION['intIdDefaultProject']."' AND spg_id='".$arrTplVars['intIdItem']."' AND spg_status='Y'" , 'spg_id_parent');
    else
        $inIdParentSubItem = $arrTplVars['intIdItem'];

// ***** Выбираем вложенные папки в родительской
    $arrLstSubItem = $objDb->fetchall( "SELECT * FROM site_pages_group WHERE spg_id_project='".$_SESSION['intIdDefaultProject']."' AND spg_id_parent='".$inIdParentSubItem."' AND spg_status='Y'" );

    if (is_array($arrLstSubItem)) {
        $arrIf['block.'.$inIdParentSubItem] = true;
        foreach ($arrLstSubItem as $key=>$value) {
            $strTmpStyleHref = ($arrTplVars['intIdItem']==$arrLstSubItem[$key]['spg_id']) ? ' active' : '';
            $arrSubItemForTpl[$key] = array('intIdSubItem'=>$arrLstSubItem[$key]['spg_id'], 'strNameSubItem'=>$arrLstSubItem[$key]['spg_name'], 'strSubHrefStyle'=>$strTmpStyleHref);
        }
    }

    $objTpl->tpl_loop($arrTplVars['module.name'], "lst.subitem.".$inIdParentSubItem, $arrSubItemForTpl);

// ***** Выбираем список страниц в выбранном каталоге ***********************************
    $strSqlQuery = "SELECT * FROM site_pages WHERE sp_id_group='".$arrTplVars['intIdItem']."' AND sp_id_project='".$_SESSION['intIdDefaultProject']."' ORDER BY sp_alias ASC";
    $arrLstSubitemPages = $objDb->fetchall( $strSqlQuery );

    $arrIf['block.pages'] = true; // Открываем блок "Страницы в группе"

    if(is_array($arrLstSubitemPages)) {
        $arrIf['block.lst.pages'] = true;

        foreach($arrLstSubitemPages as $key=>$value) {
            $arrLstPagesForTpl[$key]['intIdPage'] = $arrLstSubitemPages[$key]['sp_id'];
            $arrLstPagesForTpl[$key]['strStatusPublish'] = ($arrLstSubitemPages[$key]['sp_publish']===NULL) ? ' style="color: #CC0000;"' : '';
            $arrLstPagesForTpl[$key]['strNamePage'] = $arrLstSubitemPages[$key]['sp_name'];
            $arrLstPagesForTpl[$key]['strAliasPage'] = $arrLstSubitemPages[$key]['sp_alias'];

            $strFileContent = $objFile->readFile($_SESSION['arrInfoAccessProject'][$_SESSION['intIdDefaultProject']]['strPathProject'].REL_TPL_PATH."pages/{$arrLstSubitemPages[$key]['sp_alias']}.".$arrLstPagesForTpl[$key]['intIdPage'].".tpl");
        }

        $objTpl->tpl_loop($arrTplVars['module.name'], "lst.pages", $arrLstPagesForTpl, array($arrTplVars['cBgColor'], $arrTplVars['cThirdColor']));
    } else { // Если не выбрано ни одной страницы в выбранном каталоге
        $arrIf['block.lst.pages.no'] = true;
    }
// **********************************************************************
}

// **********************************************************************

//$objTpl->strip_loops($arrTplVars['module.name']);
$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
