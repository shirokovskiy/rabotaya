<?php
$arrTplVars['module.name'] = "event.form";
$arrTplVars['module.parent'] = "events";
$arrTplVars['module.title'] = "Добавить событие";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

include_once "models/Events.php";
$objEvents = new Events();

/**
 * Удаление фото
 */
if ( intval($_GET['id']) > 0 && $_GET['act'] == 'del' ) {
    $intRecordId = intval($_GET['id']);
    $objEvents->removeImageRecord($intRecordId);

    if ( file_exists(PRJ_IMAGES."news/news.photo.$intRecordId.jpg")) {
        unlink(PRJ_IMAGES."news/news.photo.$intRecordId.jpg");
        unlink(PRJ_IMAGES."news/news.photo.$intRecordId.b.jpg");
    }

    header("Location: {$arrTplVars['module.name']}?id=$intRecordId&stPage={$arrTplVars['stPage']}");
    exit();
}

/**
 * Запись данных
 */
if ( isset($_POST['frmEditEvent']) && $_POST['frmEditEvent'] == 'true' ) {
    $intRecordId = $arrSqlData['intRecordId'] = intval(trim($_POST['intRecordId']));
    $arrTplVars['intRecordId'] = ( $arrSqlData['intRecordId'] > 0 ? $arrSqlData['intRecordId'] : '');

    $arrSqlData['strTitle'] = addslashes(trim($_POST['strTitle']));
    $arrTplVars['strTitle'] = htmlspecialchars(stripslashes(trim($_POST['strTitle'])));

    if (empty($arrSqlData['strTitle']) ) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgEmptyTitle';
    }

    $arrSqlData['strEventBody'] = addslashes(trim($_POST['strEventBody']));
    $arrTplVars['strEventBody'] = htmlspecialchars(stripslashes(trim($_POST['strEventBody'])));

    if (empty($arrSqlData['strEventBody']) ) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgEmptyNewsbody';
    }

    $dateNewsPublish = date('Y-m-d', strtotime(trim($_POST['strDatePublish'])));

    if (!$objUtils->dateValid($dateNewsPublish)) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgFailDate';
    }

    $arrSqlData['cbxStatus'] = ($_POST['cbxStatus']=='on'?'Y':'N');
    $arrTplVars['cbxStatus'] = ($_POST['cbxStatus']=='on'?' checked':'');

    $arrSqlData['cbxReturn'] = ($_POST['cbxReturn']=='on'?'Y':'N');
    $arrTplVars['cbxReturn'] = ($_POST['cbxReturn']=='on'?' checked':'');

    if ($GLOBALS['manStatusError']!=1) {
        $strSqlFields = ""
            ." se_title = '{$arrSqlData['strTitle']}'"
            .", se_body = '{$arrSqlData['strEventBody']}'"
            .", se_date_publ = '$dateNewsPublish'"
            .", se_status = '{$arrSqlData['cbxStatus']}'"
        ;

        if ( empty($intRecordId) ) {
            $strSqlQuery = "INSERT INTO site_events SET".$strSqlFields
                .", se_date_add = NOW()";
        } else {
            $strSqlQuery = "UPDATE site_events SET".$strSqlFields
                .", se_date_change = NOW()"
                ." WHERE se_id = '$intRecordId'";
        }

        if ( !$objDb->query( $strSqlQuery ) ) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
        } else {
            if ( empty($intRecordId) ) {
                $intRecordId = $objDb->insert_id();
            }
        }

        /**
         * Загрузка фото
         */
        if ( $_FILES['flPhoto']['error'] == 0 && $_FILES['flPhoto']['size'] > 0 && $_FILES['flPhoto']['size'] < 2000000 ) {
            $strImageName = "events.photo.$intRecordId";

            /////////////////////////////////////////////////////////////////////////////////////
            // extended
            $arrExts['pjpeg'] = 'jpg';
            $arrExts['jpeg'] = 'jpg';

            $mimeType = explode("/", $_FILES['flPhoto']['type']);
            $mimeType = $mimeType[1];

            if ( array_key_exists($mimeType , $arrExts) ) {
                $mimeType = $arrExts[$mimeType];
            }

            if ( $mimeType != 'jpg' ) {
                $GLOBALS['manStatusError']=1;
                $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
            } else {
                $fileName = "$strImageName.$mimeType";
                $fileNameBig = "$strImageName.b.$mimeType";
                $fileNameOrig = "temp.$strImageName.$mimeType";

                $dirStorage = DROOT."storage";
                if ( !file_exists($dirStorage) ) {
                    if (!mkdir($dirStorage, 0775)) {
                        $GLOBALS['manStatusError']=1;
                        $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
                    }
                }

                $dirImages = $dirStorage."/files";
                if ( !file_exists($dirImages) ) {
                    if (!mkdir($dirImages, 0775)) {
                        $GLOBALS['manStatusError']=1;
                        $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
                    }
                }

                $dirImages = $dirImages."/images";
                if ( !file_exists($dirImages) ) {
                    if (!mkdir($dirImages, 0775)) {
                        $GLOBALS['manStatusError']=1;
                        $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
                    }
                }

                $dirImages = $dirImages."/events";
                if ( !file_exists($dirImages) ) {
                    if (!mkdir($dirImages, 0775)) {
                        $GLOBALS['manStatusError']=1;
                        $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
                    }
                }

                if ($GLOBALS['manStatusError']!=1) {
                    move_uploaded_file($_FILES['flPhoto']['tmp_name'], $dirImages."/".$fileNameOrig );

                    if ( !is_object($objImage) ) {
                        include_once(SITE_LIB_DIR."cls.images.php");
                        $objImage = new clsImages();
                    }

                    $objImage->resizeImage($dirImages."/".$fileNameOrig, $dirImages."/".$fileName, 200 );
                    $objImage->resizeImage($dirImages."/".$fileNameOrig, $dirImages."/".$fileNameBig, 800 );
                    chmod($dirImages."/".$fileName, 0664);
                    chmod($dirImages."/".$fileNameBig, 0664);
                    unlink($dirImages."/".$fileNameOrig);
                }
            }
            /////////////////////////////////////////////////////////////////////////////////////

            /**
             * Если ошибок нет, значит файл был загружен на сервер, и теперь необходимо добавить запись в БД
             */
            if ($GLOBALS['manStatusError']!=1) {
                $arrNewsImage = $objEvents->getEventImage($intRecordId);
                if (is_array($arrNewsImage) && !empty($arrNewsImage)) {
                    // todo: подумать что тут можно сделать
                } else {
                    // добавить запись о картинке
                    $objEvents->setImageRecord($intRecordId, $fileNameBig, $arrSqlData['strTitle']);
                }
            }
        }

        if ( $GLOBALS['manStatusError']!=1 ) {
            if ( $arrSqlData['cbxReturn'] == 'Y' ) {
                header('location:'.$arrTplVars['module.name'].'?errMess=msgOk&stPage='.$arrTplVars['stPage'].'&id='.$intRecordId);
            } else {
                header('location:'.$arrTplVars['module.parent'].'?errMess=msgOk&stPage='.$arrTplVars['stPage']);
            }
            exit;
        }
    }
}

// ***** Обработка ошибок для вывода *******************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// *****************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if ( isset($_GET['id']) && intval($_GET['id']) > 0 ) {
    $arrTplVars['module.title'] = "Редактировать событие";
    $intRecordId = $arrTplVars['intRecordId'] = intval($_GET['id']);

    $strSqlQuery = "SELECT * FROM site_events WHERE se_id = ".$intRecordId;
    $arrInfo = $objDb->fetch( $strSqlQuery );

    if ( is_array( $arrInfo )) {
        $arrTplVars['strTitle'] = htmlspecialchars(stripslashes($arrInfo['se_title']));
        $arrTplVars['strEventBody'] = htmlspecialchars(stripslashes($arrInfo['se_body']));
        $arrTplVars['strDatePublish'] = date('d.m.Y', strtotime($arrInfo['se_date_publ']));
        $arrTplVars['cbxStatus'] = $arrInfo['se_status'] == 'Y' ? ' checked' : '';

        $arrEventImage = $objEvents->getEventImage($intRecordId);
        $arrIf['exist.image'] = isset($arrEventImage['si_id']);
    }
}

$arrIf['no.image'] = !$arrIf['exist.image'];

/**
 * Установка дат
 */
// Дни
for ($i=1;$i<=31;$i++) {
    $arrDays[$i]['intDay'] = $i;
    $arrDays[$i]['strDay'] = str_pad($i, 2, "0", STR_PAD_LEFT);
    $arrDays[$i]['selDay'] = ( ($i == $arrTplVars['intDay']) || (!isset($arrTplVars['intDay']) && $i == intval(date('d'))) ? " selected" : "");
}
$objTpl->tpl_loop($arrTplVars['module.name'], "days", $arrDays);


// Месяцы
for ($i=1;$i<=12;$i++) {
    $arrMonths[$i]['intMonth'] = $i;
    $arrMonths[$i]['strMonth'] = $objUtils->arrAtMonth[$i];
    $arrMonths[$i]['selMonth'] = ( ($i == $arrTplVars['intMonth']) || (!isset($arrTplVars['intMonth']) && $i == intval(date('m'))) ? " selected" : "");
}
$objTpl->tpl_loop($arrTplVars['module.name'], "months", $arrMonths);

// Годы
for ($i=2005;$i<=(intval(date("Y"))+3);$i++) {
    $arrYears[$i]['intYear'] = $i;
    $arrYears[$i]['strYear'] = str_pad($i, 2, "0", STR_PAD_LEFT);
    $arrYears[$i]['selYear'] = ( ($i == $arrTplVars['intYear']) || (!isset($arrTplVars['intYear']) && $i == intval(date('Y'))) ? " selected" : "");

}
$objTpl->tpl_loop($arrTplVars['module.name'], "years", $arrYears);

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);

