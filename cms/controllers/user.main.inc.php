<?php
$arrTplVars['module.name'] = "user.main";
$arrTplVars['module.title'] = "Добро Пожаловать в CMS";

// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

$arrTplVars['u_gender'] = ($_SESSION['userInfoStorage']['cu_gender']=='m') ? 'ый' : 'ая';

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

$arrTplVars['projectName'] = htmlspecialchars($_SESSION['arrInfoAccessProject'][$_SESSION['intIdDefaultProject']]['strNameProject']);


$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
