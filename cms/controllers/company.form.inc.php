<?php
$arrTplVars['module.name'] = "company.form";
$arrTplVars['module.name.child'] = "vacancy.form";
$arrTplVars['module.parent'] = "companies";
$arrTplVars['module.title'] = "Добавить компанию";

include_once "models/Company.php";
$objCompany = new Company($_db_config);
$SiteUser = new SiteUser();

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);
$intRecordId = intval(trim($_GET['id']));

/**
 * Запись данных
 */
if ( isset($_POST['frmEdit']) && $_POST['frmEdit'] == 'true' ) {
    $intRecordId = $arrSqlData['intRecordId'] = intval(trim($_POST['intRecordId']));
    $arrTplVars['intRecordId'] = ( $arrSqlData['intRecordId'] > 0 ? $arrSqlData['intRecordId'] : '');

    $arrSqlData['strTitle'] = mysql_real_escape_string(trim($_POST['strTitle']));
    $arrTplVars['strTitle'] = htmlspecialchars(stripslashes(trim($_POST['strTitle'])));

    if (empty($arrSqlData['strTitle']) ) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgEmptyTitle';
    }

    $arrSqlData['cbxStatus'] = ($_POST['cbxStatus'] == 'on' ? 'Y' : 'N');
    $arrTplVars['cbxStatus'] = ($_POST['cbxStatus'] == 'on' ? ' checked' : '');

    $arrSqlData['cbxTop'] = ($_POST['cbxTop'] == 'on' ? 'Y' : 'N');
    $arrTplVars['cbxTop'] = ($_POST['cbxTop'] == 'on' ? ' checked' : '');

    $isLogo = intval(trim($_POST['isLogo']));

    if($arrSqlData['cbxTop']=='Y' && $_FILES['flPhoto']['size']==0 && !$isLogo) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgEmptyNotTopComp';
    }

    $arrSqlData['cbxReturn'] = ($_POST['cbxReturn'] == 'on' ? 'Y' : 'N');
    $arrTplVars['cbxReturn'] = ($_POST['cbxReturn'] == 'on' ? ' checked' : '');

    $rbAgency = $_POST['rbAgency'];
    $arrTplVars['jc_type_'.$rbAgency] = " checked";

    $arrSqlData['strCompanyCity'] = mysql_real_escape_string(trim($_POST['strCompanyCity']));
    $arrTplVars['strCompanyCity'] = htmlspecialchars(stripslashes(trim($_POST['strCompanyCity'])));

    $cityID = $SiteUser->getCityId(trim($_POST['strCompanyCity']));
    if (empty($cityID)) {
        $cityID = $SiteUser->saveCity(trim($_POST['strCompanyCity']));
    }

    $arrSqlData['strAddress'] = mysql_real_escape_string(trim($_POST['strAddress']));
    $arrTplVars['strAddress'] = htmlspecialchars(stripslashes(trim($_POST['strAddress'])));

    $arrSqlData['strWeb'] = mysql_real_escape_string(trim($_POST['strWeb']));
    $arrTplVars['strWeb'] = htmlspecialchars(stripslashes(trim($_POST['strWeb'])));

    $arrSqlData['strCompanyEmail'] = mysql_real_escape_string(trim($_POST['strCompanyEmail']));
    $arrTplVars['strCompanyEmail'] = htmlspecialchars(stripslashes(trim($_POST['strCompanyEmail'])));

    if (!empty($arrSqlData['strCompanyEmail']) && !$objUtils->checkEmail($arrSqlData['strCompanyEmail'])) {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgEmptyEmail';
    }

    $arrSqlData['strCompanyPhone'] = mysql_real_escape_string(trim($_POST['strCompanyPhone']));
    $arrTplVars['strCompanyPhone'] = htmlspecialchars(stripslashes(trim($_POST['strCompanyPhone'])));

    $arrSqlData['strCompanyDescription'] = mysql_real_escape_string(trim($_POST['strCompanyDescription']));
    $arrTplVars['strCompanyDescription'] = htmlspecialchars(stripslashes(trim($_POST['strCompanyDescription'])));

    if ( $GLOBALS['manStatusError']!=1 ) {
        $strSqlFields = " jc_title = '".$arrSqlData['strTitle']."'"
            .", jc_type = '$rbAgency'"
            . ( !empty($arrSqlData['strCompanyCity']) && empty($cityID) ? ", jc_city = '{$arrSqlData['strCompanyCity']}'" : "" )
            . ( !empty($arrSqlData['strAddress']) ? ", jc_address = '{$arrSqlData['strAddress']}'" : "" )
            . ( !empty($arrSqlData['strWeb']) ? ", jc_web = '{$arrSqlData['strWeb']}'" : "" )
            . ( !empty($arrSqlData['strCompanyEmail']) ? ", jc_email = '{$arrSqlData['strCompanyEmail']}'" : "" )
            . ( !empty($arrSqlData['strCompanyPhone']) ? ", jc_phone = '{$arrSqlData['strCompanyPhone']}'" : "" )
            . ( !empty($arrSqlData['strCompanyDescription']) ? ", jc_description = '{$arrSqlData['strCompanyDescription']}'" : "" )
            .", jc_status = '{$arrSqlData['cbxStatus']}'"
            .", jc_top = '{$arrSqlData['cbxTop']}'"
            .", jc_city_id = ".$cityID
        ;
        if($intRecordId > 0) {
            $strSqlQuery = "UPDATE job_companies SET $strSqlFields WHERE jc_id = ".$intRecordId;
        } else {
            $strSqlQuery = "INSERT INTO job_companies SET $strSqlFields, jc_date_create = UNIX_TIMESTAMP()";
        }

        if ( !$objDb->query( $strSqlQuery ) ) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
        } else {
            /**
             * Загрузка фото
             */
            if ( $_FILES['flPhoto']['error'] == 0 && $_FILES['flPhoto']['size'] > 0 && $_FILES['flPhoto']['size'] < 2000000 ) {
                $strImageName = "logo.$intRecordId";

                // extended
                $arrExts['pjpeg'] = 'jpg';
                $arrExts['jpeg'] = 'jpg';

                $mimeType = explode("/", $_FILES['flPhoto']['type']);
                $mimeType = $mimeType[1];

                if ( array_key_exists($mimeType , $arrExts) ) {
                    $mimeType = $arrExts[$mimeType];
                }

                if ( !in_array($mimeType, array('jpg','png','gif')) ) {
                    $GLOBALS['manStatusError']=1;
                    $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
                } else {
                    $fileName = "$strImageName.$mimeType";
                    $fileNameOrig = "temp.$strImageName.$mimeType";

                    $dirImages = PRJ_IMAGES.'companies';
                    if ( !file_exists($dirImages) ) {
                        if (!mkdir($dirImages, 0775, true)) {
                            $GLOBALS['manStatusError']=1;
                            $GLOBALS['manCodeError'][]['code'] = 'msgErrorCantDir';
                        }
                    }

                    if ($GLOBALS['manStatusError']!=1) {
                        if( move_uploaded_file($_FILES['flPhoto']['tmp_name'], $dirImages."/".$fileNameOrig ) ) {
                            if ( !is_object($objImage) ) {
                                include_once(SITE_LIB_DIR."cls.images.php");
                                $objImage = new clsImages();
                            }

                            $objImage->resizeImage($dirImages."/".$fileNameOrig, $dirImages."/".$fileName, 120, 50, 0xFFFFFF, 100 );
                            chmod($dirImages."/".$fileName, 0664);
                            unlink($dirImages."/".$fileNameOrig);
                        }
                    }
                }
                /////////////////////////////////////////////////////////////////////////////////////

                /**
                 * Если ошибок нет, значит файл был загружен на сервер, и теперь необходимо добавить запись в БД
                 */
                if ($GLOBALS['manStatusError']!=1) {
                    $arrImage = $objCompany->getLogoImage($intRecordId);
                    if (is_array($arrImage) && !empty($arrImage)) {
                        // todo: подумать что тут можно сделать
                    } else {
                        // добавить запись о картинке
                        $objCompany->setImageRecord($intRecordId, $fileName, $arrSqlData['strTitle']);
                    }
                }
            }
        }
    }

    if ( $GLOBALS['manStatusError']!=1 ) {
        if ( $arrSqlData['cbxReturn'] == 'Y' ) {
            header('location:'.$arrTplVars['module.name'].'?errMess=msgOk&stPage='.$arrTplVars['stPage'].'&id='.$intRecordId);
        } else {
            header('location:'.$arrTplVars['module.parent'].'?errMess=msgOk&stPage='.$arrTplVars['stPage']);
        }
        exit;
    }
}


/**
 * Удалить изображение
 */
if ( intval($_GET['id_del']) > 0 && $_GET['act']=='del' && $intRecordId > 0 ) {
    $intImgId = intval($_GET['id_del']);
    $strSqlQuery = "SELECT si_filename FROM site_images WHERE si_id=".$intImgId." AND `si_type` = 'company' AND `si_rel_id` = ".$intRecordId;
    $strFilename = $objDb->fetch($strSqlQuery, "si_filename");

    if(!empty($strFilename) && file_exists(PRJ_IMAGES.'companies/'.$strFilename)) {
        $strSqlQuery = "DELETE FROM site_images WHERE si_id=".$intImgId." AND `si_type` = 'company' LIMIT 1";
        if ( !$objDb->query($strSqlQuery) ) {
            #mysql error
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDel';
        } else {
            if(!unlink(PRJ_IMAGES.'companies/'.$strFilename)) {
                $GLOBALS['manStatusError']=1;
                $GLOBALS['manCodeError'][]['code'] = 'msgErrorDelFile';
            }
        }

        if (!$GLOBALS['manStatusError']) {
            header('Location: '.$arrTplVars['module.name'].'?stPage='.$stPage.'&id='.$intRecordId);
            exit;
        }
    } else {
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgErrorDelFile';
    }
}

// ***** Обработка ошибок для вывода *******************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// *****************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");
$arrList = array();

if ( $intRecordId > 0 ) {
    $arrTplVars['module.title'] = "Редактировать компанию";
    $arrTplVars['intRecordId'] = $intRecordId;

    $arr_string_fields = array('strTitle'=>'jc_title', 'strCompanyCity'=>'jc_city', 'strAddress'=>'jc_address', 'strCompanyDescription'=>'jc_description', 'strCompanyPhone'=>'jc_phone', 'strCompanyEmail'=>'jc_email', 'strWeb'=>'jc_web');
    $arr_checked_fields = array('jc_type');

    $strSqlQuery = "SELECT * FROM `job_companies` WHERE `jc_id` = ".$intRecordId;
    $arrRecord = $objDb->fetch($strSqlQuery);
    if (is_array($arrRecord) && !empty($arrRecord)) {
        foreach($arr_string_fields as $key => $field) {
            $arrRecord[$key] = htmlspecialchars($arrRecord[$field]);
        }

        foreach($arr_checked_fields as $field) {
            $arrRecord[$field.'_'.$arrRecord[$field]] = ' checked';
        }

        $arrTplVars['strDatePublish'] = date('d.m.Y', strtotime($arrRecord['jc_date_create']));
        $arrTplVars['cbxStatus'] = $arrRecord['jc_status'] == 'Y' ? ' checked' : '';
        $arrTplVars['cbxTop'] = $arrRecord['jc_top'] == 'Y' ? ' checked' : '';
        if ($arrRecord['jc_city_id']>0) {
            $arrRecord['strCompanyCity'] = $SiteUser->getCityById($arrRecord['jc_city_id']);
            $arrRecord['strCompanyCity'] = $arrRecord['strCompanyCity']['city'];
        }

        $arrImage = $objCompany->getLogoImage($intRecordId);
        $arrIf['exist.image'] = isset($arrImage['si_id']);
        if($arrIf['exist.image']) {
            $arrTplVars['strFilename'] = $arrImage['si_filename'];
            $arrTplVars['intImgId'] = $arrImage['si_id'];
        }

        /**
         * List of vacancies of company
         */
        $strSqlQuery = "SELECT * FROM `job_vacancies` WHERE `jv_jc_id` = ".$intRecordId;
        $arrList = $objDb->fetchall($strSqlQuery);
        if (is_array($arrList) && !empty($arrList)) {
            $arrIf['dependencies'] = true;
            foreach ($arrList as $key => $value) {
                $arrList[$key]['strDatePublish'] = (!empty($value['jv_date_publish']) ? date( "d.m.Y", $value['jv_date_publish']) : '&mdash;');
            }
        }

        $arrIf['record.exists'] = true;
    }
}
$objTpl->tpl_loop($arrTplVars['module.name'], "lst.dependencies", $arrList);
$arrIf['no.image'] = !$arrIf['exist.image'];

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrRecord);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);

