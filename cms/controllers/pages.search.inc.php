<?php
$arrTplVars['module.name'] = "pages.search";
$arrTplVars['module.title'] = "ПОИСК СТРАНИЦ";
$arrTplVars['module.parent'] = "pages";
$arrTplVars['module.child'] = "pages.edit";


if ( $_POST["frmSearchPage"] == "true" ) {

  $arrSqlData["strPageSearch"]  = addslashes(trim($_POST["strPageSearch"]));
  $arrTplVars["strPageSearch"]    = htmlspecialchars(trim($_POST["strPageSearch"]));

  $arrSqlData['cbxCorrect'] = ($_POST['cbxCorrect']=='on'?'Y':'N');
  $arrTplVars['cbxCorrect'] = ($_POST['cbxCorrect']=='on'?' checked':'');

  $searchDirect = ( $arrSqlData['cbxCorrect'] == 'N' ) ? '%' : '';

  $arrSqlData['intPageID'] = intval(trim($_POST['intPageID']));
  $arrTplVars['intPageID'] = ( $arrSqlData['intPageID'] > 0 ? $arrSqlData['intPageID'] : '');

  $arrSqlData['strFrgNameInPage'] = addslashes(trim($_POST['strFrgNameInPage']));
  $arrTplVars['strFrgNameInPage'] = htmlspecialchars(stripslashes(trim($_POST['strFrgNameInPage'])));

  if ( !empty($arrSqlData["strPageSearch"]) || !empty($arrSqlData['intPageID']) || !empty($arrSqlData['strFrgNameInPage'])) {
    if ( eregi(',', $arrSqlData['strPageSearch']) ) {
      $arrFewSearchAlias = $objUtils->doExplode(',', $arrSqlData['strPageSearch']);
    }

    if ( is_array( $arrFewSearchAlias ) ) {
      foreach ($arrFewSearchAlias as $kFSA => $vFSA) {
        $arrFewSearchAlias[$kFSA] = " sp_alias LIKE '$searchDirect".$vFSA."$searchDirect'";
      }
      $strAliasSqlQuery = implode(' OR ', $arrFewSearchAlias);
    } else {
      $strAliasSqlQuery = ( !empty($arrSqlData["strPageSearch"]) ? "sp_alias LIKE '$searchDirect". $arrSqlData["strPageSearch"] ."$searchDirect'" : "" );
    }

    $strSqlQuery = "SELECT * FROM site_pages"
      ." WHERE ("
      . $strAliasSqlQuery
      . ( !empty($arrSqlData['intPageID']) ? (!empty($arrSqlData["strPageSearch"]) ? " OR " : "" )." sp_id=".$arrSqlData['intPageID'] : "" )
      . ( !empty($arrSqlData['strFrgNameInPage']) ? ((!empty($arrSqlData["strFrgNameInPage"])||!empty($arrSqlData['intPageID'])) ? " OR " : "" )." sp_body LIKE '%frg ".$arrSqlData['strFrgNameInPage']."%'" : "" )
      .")"
      ." AND sp_id_project = '".$_SESSION['intIdDefaultProject']."'";
    $arrPageData = $objDb->fetchall( $strSqlQuery );
  }

  if ( !empty( $arrPageData ) ) {

    foreach ( $arrPageData as $key => $pageData ) {

      if ( !is_null( $pageData["sp_id_group"] ) ) {

        $strSqlQuery = "SELECT * FROM site_pages_group"
          ." WHERE spg_id = ".$pageData["sp_id_group"]
          ." AND spg_id_project = '".$_SESSION['intIdDefaultProject']."'";
        $arrChapterPage = $objDb->fetch(  $strSqlQuery );

        if ( !empty( $arrChapterPage ) ) {
          $arrPageData[$key]["pChapterName"] = $arrChapterPage["spg_name"];
          $arrPageData[$key]["pChapterID"] = $arrChapterPage["spg_id"];
        }
      }
    }
  }
}


// ***** Обработка ошибок для вывода ****************************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// **************************************************************************************

$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if ( !empty( $arrPageData ) ) {
  $objTpl->tpl_loop($arrTplVars['module.name'], "lst.searched.pages", $arrPageData );
  $arrIf["alias.searched"] = true;
} else {
  if ( $_POST["frmSearchPage"] == "true" ) {
    $arrIf["alias.not.found"] = true;
  }
}

// **************************************************************************************
$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);
