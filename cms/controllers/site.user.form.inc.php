<?php
include_once "models/SiteUser.php";

$arrTplVars['module.name'] = "site.user.form";
$arrTplVars['module.parent'] = "site.users";
$arrTplVars['module.title'] = "Добавить пользователя сайта";

$objSiteUser = new SiteUser();

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);
$intRecordId = intval(trim($_GET['id']));

/**
 * Удаление фото
 */
if ( $intRecordId > 0 && intval($_GET['id_del']) > 0 && $_GET['act'] == 'del' ) {
    $intImageId = intval($_GET['id_del']);
    $strSqlQuery = "UPDATE site_images SET si_status = 'N', si_date_change = NOW() WHERE si_id = $intImageId AND si_rel_id = $intRecordId AND si_type='users'";
    $strSqlQuery = "DELETE FROM site_images WHERE si_id = $intImageId AND si_rel_id = $intRecordId AND si_type='users' LIMIT 1";
    if ( !$objDb->query($strSqlQuery) ) {
        #mysql error
        $GLOBALS['manStatusError']=1;
        $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
    } else {
        if ( file_exists(PRJ_IMAGES."users/user.photo.$intRecordId.jpg")) {
            unlink(PRJ_IMAGES."users/user.photo.$intRecordId.jpg");
            unlink(PRJ_IMAGES."users/user.photo.$intRecordId.b.jpg");
        }

        header("Location: {$arrTplVars['module.name']}?id=$intRecordId&stPage={$arrTplVars['stPage']}");
        exit();
    }
}


/**
 * Запись данных из формы
 */
if (isset($_POST['frmEdit']) && $_POST['frmEdit'] == 'true') {
    $arrSqlData['intRecordId'] = $intRecordId = intval(trim($_POST['intRecordId']));
    $arrTplVars['intRecordId'] = ($arrSqlData['intRecordId'] > 0 ? $arrSqlData['intRecordId'] : '');

    $arrSqlData['strType'] = mysql_real_escape_string(trim($_POST['strType']));
    $arrTplVars['strType'] = htmlspecialchars(stripslashes(trim($_POST['strType'])));

    $arrSqlData['strLname'] = mysql_real_escape_string(mb_ucfirst(trim($_POST['strLname'])));
    $arrTplVars['strLname'] = htmlspecialchars(stripslashes(mb_ucfirst(trim($_POST['strLname']))));

    $arrSqlData['strFname'] = mysql_real_escape_string(mb_ucfirst(trim($_POST['strFname'])));
    $arrTplVars['strFname'] = htmlspecialchars(stripslashes(mb_ucfirst(trim($_POST['strFname']))));

    $arrSqlData['strMname'] = mysql_real_escape_string(mb_ucfirst(trim($_POST['strMname'])));
    $arrTplVars['strMname'] = htmlspecialchars(stripslashes(mb_ucfirst(trim($_POST['strMname']))));

    $arrSqlData['strEmail'] = mysql_real_escape_string(trim($_POST['strEmail']));
    $arrTplVars['strEmail'] = htmlspecialchars(stripslashes(trim($_POST['strEmail'])));

    $arrSqlData['strPhone'] = mysql_real_escape_string(trim($_POST['strPhone']));
    $arrTplVars['strPhone'] = htmlspecialchars(stripslashes(trim($_POST['strPhone'])));

    $arrSqlData['strCompanyCity'] = mysql_real_escape_string(mb_ucfirst(trim($_POST['strCompanyCity'])));
    $arrTplVars['strCompanyCity'] = htmlspecialchars(stripslashes(mb_ucfirst(trim($_POST['strCompanyCity']))));

    $cityId = $objSiteUser->getCityId(trim($_POST['strCompanyCity']));
    if (empty($cityId)) {
        $cityId = $objSiteUser->saveCity(trim($_POST['strCompanyCity']));
    }

    $arrSqlData['strAddress'] = mysql_real_escape_string(trim($_POST['strAddress']));
    $arrTplVars['strAddress'] = htmlspecialchars(stripslashes(trim($_POST['strAddress'])));

    $arrSqlData['strPosition'] = mysql_real_escape_string(mb_ucfirst(trim($_POST['strPosition'])));
    $arrTplVars['strPosition'] = htmlspecialchars(stripslashes(mb_ucfirst(trim($_POST['strPosition']))));

    $arrSqlData['strInn'] = mysql_real_escape_string(trim($_POST['strInn']));
    $arrTplVars['strInn'] = htmlspecialchars(stripslashes(trim($_POST['strInn'])));

    $arrSqlData['cbxStatus'] = ($_POST['cbxStatus']=='on'?'Y':'N');
    $arrTplVars['cbxStatus'] = ($_POST['cbxStatus']=='on'?' checked':'');

    $arrSqlData['cbxReturn'] = ($_POST['cbxReturn']=='on'?'Y':'N');
    $arrTplVars['cbxReturn'] = ($_POST['cbxReturn']=='on'?' checked':'');

    if ($GLOBALS['manStatusError']!=1) {
        $strSqlFields = ""
            ." su_lname = '{$arrSqlData['strLname']}'"
            .", su_fname = '{$arrSqlData['strFname']}'"
            .", su_mname = '{$arrSqlData['strMname']}'"
            .", su_login = '{$arrSqlData['strEmail']}'"
            .", su_phone = '{$arrSqlData['strPhone']}'"
            .", su_position = '{$arrSqlData['strPosition']}'"
            .", su_inn = '{$arrSqlData['strInn']}'"
            .(empty($cityId) && !empty($arrSqlData['strCompanyCity']) ? ", su_city = '{$arrSqlData['strCompanyCity']}'" : '' )
            .(!empty($cityId) ? ", su_city_id = ".$cityId : '' )
            .", su_address = '{$arrSqlData['strAddress']}'"
            .", su_status = '{$arrSqlData['cbxStatus']}'"
            .", su_type = '{$arrSqlData['strType']}'"
        ;

        if ( empty($intRecordId) ) {
            $strSqlQuery = "INSERT INTO site_users SET ".$strSqlFields
                .", su_date = NOW()";
        } else {
            $strSqlQuery = "UPDATE site_users SET".$strSqlFields
                .", su_date = NOW()"
                ." WHERE su_id = ".$intRecordId;
        }

        if ( !$objDb->query( $strSqlQuery ) ) {
            $GLOBALS['manStatusError']=1;
            $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
        } else {
            if ( empty($intRecordId) ) {
                $intRecordId = $objDb->insert_id();
            }
        }

        /**
         * Загрузка фото
         */
        if ( $_FILES['flPhoto']['error'] == 0 && $_FILES['flPhoto']['size'] > 0 && $_FILES['flPhoto']['size'] < 2000000 ) {
            $strImageName = "user.photo.$intRecordId";

            /////////////////////////////////////////////////////////////////////////////////////
            // extended
            $arrExts['pjpeg'] = 'jpg';
            $arrExts['jpeg'] = 'jpg';

            $mimeType = explode("/", $_FILES['flPhoto']['type']);
            $mimeType = $mimeType[1];

            if ( array_key_exists($mimeType , $arrExts) ) {
                $mimeType = $arrExts[$mimeType];
            }

            if ( $mimeType != 'jpg' ) {
                $GLOBALS['manStatusError']=1;
                $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
            } else {
                $fileName = "$strImageName.$mimeType";
                $fileNameBig = "$strImageName.b.$mimeType";
                $fileNameOrig = "temp.$strImageName.$mimeType";

                $dirStorage = DROOT."storage";
                if ( !file_exists($dirStorage) ) {
                    if (!mkdir($dirStorage, 0775)) {
                        $GLOBALS['manStatusError']=1;
                        $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
                    }
                }

                $dirImages = $dirStorage."/files";
                if ( !file_exists($dirImages) ) {
                    if (!mkdir($dirImages, 0775)) {
                        $GLOBALS['manStatusError']=1;
                        $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
                    }
                }

                $dirImages = $dirImages."/images";
                if ( !file_exists($dirImages) ) {
                    if (!mkdir($dirImages, 0775)) {
                        $GLOBALS['manStatusError']=1;
                        $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
                    }
                }

                $dirImages = $dirImages."/users";
                if ( !file_exists($dirImages) ) {
                    if (!mkdir($dirImages, 0775)) {
                        $GLOBALS['manStatusError']=1;
                        $GLOBALS['manCodeError'][]['code'] = 'msgErrorFileType';
                    }
                }

                if ($GLOBALS['manStatusError']!=1) {
                    move_uploaded_file($_FILES['flPhoto']['tmp_name'], $dirImages."/".$fileNameOrig );

                    if ( !is_object($objImage) ) {
                        include_once(SITE_LIB_DIR."cls.images.php");
                        $objImage = new clsImages();
                    }

                    $objImage->resizeImage($dirImages."/".$fileNameOrig, $dirImages."/".$fileName, 180, 220 );
                    $objImage->resizeImage($dirImages."/".$fileNameOrig, $dirImages."/".$fileNameBig, 800 );

                    unlink($dirImages."/".$fileNameOrig);
                }
            }
            /////////////////////////////////////////////////////////////////////////////////////

            if ($GLOBALS['manStatusError']!=1) {
                $arrImage = $objSiteUser->getImage($intRecordId);

                if (is_array($arrImage) && !empty($arrImage)) {
                    $strSqlQuery = "UPDATE `site_images` SET"
                        . " `si_filename` = '".mysql_real_escape_string($fileName)."'"
                        . " `si_date_change` = NOW()"
                        . " WHERE `si_id` = $intRecordId";
                    if (!$objDb->query($strSqlQuery)) {
                        $GLOBALS['manStatusError']=1;
                        $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
                    }
                } else {
                    $strSqlQuery = "INSERT INTO site_images SET"
                        ." si_rel_id = ".$intRecordId
                        .", si_type = 'users'"
                        .", si_title = '".$arrSqlData['strLname'].' '.$arrSqlData['strFname'].' '.$arrSqlData['strMname']."'"
                        .", si_filename = '".mysql_real_escape_string($fileName)."'"
                        .", si_date_add = NOW()"
                        .", si_status = 'Y'"
                    ;
                    if ( !$objDb->query( $strSqlQuery ) ) {
                        $GLOBALS['manStatusError']=1;
                        $GLOBALS['manCodeError'][]['code'] = 'msgErrorDB';
                    }
                }
            }
        }

        if ( $GLOBALS['manStatusError']!=1 ) {
            if ( $arrSqlData['cbxReturn'] == 'Y' ) {
                header('location:'.$arrTplVars['module.name'].'?errMess=msgOk&stPage='.$arrTplVars['stPage'].'&id='.$intRecordId);
            } else {
                header('location:'.$arrTplVars['module.parent'].'?errMess=msgOk&stPage='.$arrTplVars['stPage']);
            }
            exit;
        }
    }
}




// ***** Обработка ошибок для вывода *******************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// *****************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

if ( $intRecordId > 0 ) {
    $arrTplVars['module.title'] = "Редактировать пользователя сайта";
    $arrTplVars['intRecordId'] = $intRecordId;
    $objSiteUser->setId($intRecordId);
    $arrRecord = $objSiteUser->getData();

    $arrTplVars['strLname'] = htmlspecialchars($arrRecord['su_lname'], ENT_QUOTES);
    $arrTplVars['strFname'] = htmlspecialchars($arrRecord['su_fname'], ENT_QUOTES);
    $arrTplVars['strMname'] = htmlspecialchars($arrRecord['su_mname'], ENT_QUOTES);
    $arrTplVars['strEmail'] = htmlspecialchars($arrRecord['su_login'], ENT_QUOTES);
    $arrTplVars['strPhone'] = htmlspecialchars($arrRecord['su_phone'], ENT_QUOTES);
    $arrTplVars['strPosition'] = htmlspecialchars($arrRecord['su_position'], ENT_QUOTES);
    $arrTplVars['strCompanyCity'] = htmlspecialchars($arrRecord['su_city'], ENT_QUOTES);
    if (!empty($arrRecord['su_city_id'])) {
        $arrCity = $objSiteUser->getCityById( $arrRecord['su_city_id'] );
        if (is_array($arrCity) && !empty($arrCity)) {
            $arrTplVars['strCompanyCity'] = htmlspecialchars($arrCity['city'], ENT_QUOTES);
        }
    }
    $arrTplVars['strAddress'] = htmlspecialchars($arrRecord['su_address'], ENT_QUOTES);
    $arrTplVars['strInn'] = htmlspecialchars($arrRecord['su_inn'], ENT_QUOTES);
    $arrTplVars['strType_'.($arrRecord['su_type'])] = ' checked';
    $arrImage = $objSiteUser->getImage($intRecordId);
    $arrIf['exist.image'] = !empty($arrImage) && $arrImage['si_id'] > 0;
    if ($arrIf['exist.image']) {
        $arrTplVars['strFilename'] = $arrImage['si_filename'];
        $arrTplVars['intImgId'] = $arrImage['si_id'];
    }
    $arrTplVars['cbxStatus'] = $arrRecord['su_status'] == 'Y' ? ' checked' : '';
}

$arrIf['no.image'] = !$arrIf['exist.image'];

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);

