<?php
$arrTplVars['module.name'] = "spamer";
$arrTplVars['module.child'] = "spamer.form";
$arrTplVars['module.title'] = "Шаблоны писем для рассылки";

$arrTplVars['stPage'] = (!empty($_GET['stPage']) ? intval($_GET['stPage']) : 1);

include_once "models/Spamer.php";
if (!is_object($objSpamer)) {
    $objSpamer = new Spamer();
}

/**
 * Обработка действий
 */
if (isset($_GET['action'])) {
    switch ($_GET['action']) {
        case 'start':
            $template_id = intval($_POST['intTemplateId']);
            $group_id = intval($_POST['intGroupId']);
            $objSpamer->startProcess($template_id, $group_id);
            break;
        case 'stop':
            $cron_id = intval($_GET['cid']);
            $objSpamer->stopProcess($cron_id);
            break;
        case 'pause':
            $cron_id = intval($_GET['cid']);
            $objSpamer->pauseProcess($cron_id);
            break;
        case 'play':
            $cron_id = intval($_GET['cid']);
            $objSpamer->playProcess($cron_id);
            break;
        default:
            break;
    }

    header('location:'.$arrTplVars['module.name'].'?stPage='.$arrTplVars['stPage']);
    exit;
}

// ***** Обработка ошибок для вывода *******************************************
$arrTplVars['error'.$errSuf] = $objUtils->errorParse($GLOBALS['manCodeError'], $GLOBALS['manStatusError']);
$arrTplVars['error'.$errSuf] = $objUtils->echoMessage($arrTplVars['error'.$errSuf], $GLOBALS['manStatusError']);
// *****************************************************************************

// **** Загружаем и обрабатываем шаблон
$objTpl->tpl_load($arrTplVars['module.name'], $arrTplVars['module.name'].".html");

// Всего записей в базе
$strSqlQuery = "SELECT COUNT(*) AS intQuantAllRecords FROM site_templates_email";
$arrTplVars['intQuantitySelectRecords'] = $arrTplVars['intQuantAllRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantAllRecords');
// Выбрано записей
//$strSqlQuery = "SELECT COUNT(*) AS intQuantitySelectRecords FROM site_templates_email";
//$arrTplVars['intQuantitySelectRecords'] = $objDb->fetch( $strSqlQuery, 'intQuantitySelectRecords');
// ***** BEGIN: Построение пейджинга для вывода списка
$objPagination = new tplPaginator($arrTplVars['intQuantitySelectRecords'], SITE_TPL_TPL_DIR);
/**
 * Здесь, если надо настраиваем свойства Paginator'а (кол-во выводимых записей на страницу,
 * кол-во ссылок страниц и т.п., подробнее см. класс tplPaginator)
 */
$objPagination->strPaginatorTpl = "site.global.1.tpl";
$objPagination->intQuantRecordPage = 15;
$objPagination->strColorLinkStyle = "link-b";
$objPagination->strColorActiveStyle = "tab-bl-b";
$objPagination->strNameImageBack = "arrow_one_left.gif";
$objPagination->strNameImageForward = "arrow_one_right.gif";
// Создаем блок пэйджинга
$objPagination->paCreate();
// ***** END: Построение пейджинга для вывода списка
// Запрос для выборки нужных записей
$strSqlQuery = "SELECT * FROM site_templates_email ORDER BY ste_id DESC ".$objPagination->strSqlLimit;
$arrEmailTemplates = $objDb->fetchall( $strSqlQuery );
// кол-во публикаций показанных на странице
$arrTplVars['intQuantityShowRecOnPage'] = ( !empty($arrEmailTemplates) ? count($arrEmailTemplates) : 0 );
// Присвоение значения пэйджинга для списка
$arrTplVars['blockPaginator'] = $objPagination->paShow();

if ( is_array($arrEmailTemplates) ) {
    foreach ( $arrEmailTemplates as $key => $value ) {
        $arrEmailTemplates[$key]['strDateCreate'] = date( "d.m.Y в H:i", $value['ste_date_create']);
        $arrEmailTemplates[$key]['strDateUpdate'] = date( "d.m.Y в H:i", $value['ste_date_update']);

        $arrIf['attach.'.$value['ste_id']] = ( file_exists(DROOT."storage/images/email/attachment.photo.".$value['ste_id'].".jpg") );

        $arrEmailTemplates[$key]['strStatus'] = 'ожидает';
        $arrProcess = $objSpamer->getTemplateProcess($value['ste_id']);
        if (is_array($arrProcess) && !empty($arrProcess)) {
            // всего подписчиков
            $intCountTotalSubs = $objSpamer->getSubscribersCountByProcessId($arrProcess['sc_id']);
            // осталось разослать
            $intCountSubsStayInQueue = $objSpamer->getSubscribersInQueueProcessId($arrProcess['sc_id']);

            if (empty($arrProcess['sc_date_finish'])) {
                $arrIf['is.started.'.$value['ste_id']] = true;
                if (!empty($arrProcess['sc_progress_pause'])) {
                    $arrEmailTemplates[$key]['strStatus'] = '<span class="pause">на паузе: '.$arrProcess['susg_title'].'</span><br />'."\n";
                    $arrIf['is.paused.'.$value['ste_id']] = true;
                } else {
                    $arrEmailTemplates[$key]['strStatus'] = 'рассылается: '.$arrProcess['susg_title'].'<br />'."\n".'отправлено '.($intCountTotalSubs - $intCountSubsStayInQueue).' из '.$intCountTotalSubs;

                    if ($intCountSubsStayInQueue == 0 && $intCountTotalSubs > 0) {
                        $objSpamer->stopProcess($arrProcess['sc_id']);
                    }
                }
            } else {
                $arrEmailTemplates[$key]['strStatus'] = 'отправка завершилась '.date('d.m.Y в H:i', $arrProcess['sc_date_finish']);
            }
            $arrEmailTemplates[$key]['intCronId'] = $arrProcess['sc_id'];
        }
    }
}

$objTpl->tpl_loop($arrTplVars['module.name'], "email.templates", $arrEmailTemplates);

/**
 * Подготовить список групп подписчиков
 */
$strSqlQuery = "SELECT * FROM `site_users_subscriber_groups` ORDER BY `susg_title`";
$arrGroups = $objDb->fetchall($strSqlQuery);
if (is_array($arrGroups) && !empty($arrGroups)) {
    foreach ($arrGroups as $k => $group) {
        $arrGroups[$k]['intGroupId'] = $group['susg_id'];
        $arrGroups[$k]['strGroupName'] = htmlspecialchars($group['susg_title']);
    }
}
$objTpl->tpl_loop($arrTplVars['module.name'], "subscribers.groups.list", $arrGroups);

$objTpl->tpl_if($arrTplVars['module.name'], $arrIf);
$objTpl->tpl_array($arrTplVars['module.name'], $arrTplVars);


/*INSERT INTO `site_users_subscriber_groups_lnk` (`susgl_sus_id`, `susgl_susg_id`, `susgl_date_subscribe`)
SELECT `sus_id`, 1, `sus_date_create` FROM `site_users_subscribers`;*/
