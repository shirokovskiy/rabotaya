<?php
// ***** Непосредственно смена пароля *****
if (!empty($_POST['submitChangePwd'])) {
    $pass1 = trim($_POST['PwdChangeOne']);
    $pass2 = trim($_POST['PwdChangeToo']);

    if (empty($pass1) || empty($pass2)) {
        $statusError['error']=1;
        $users->error[]['code'] = '114';
    } else {
        if ($pass1!=$pass2) {
            $statusError['error']=1;
            $users->error[]['code'] = '115';
        } else {
            if (eregi("[а-яА-Я ]+", $pass1)) {
                $statusError['error']=1;
                $users->error[]['code'] = '116';
            } else {
                if(!$objDb->query("UPDATE fcm_users".$_cfg_base_prefix." SET password_user='".$pass1."' WHERE id_user='".$_SESSION['userInfoStorage']['id_user']."'")) {
                    $statusError['error']=1;
                    $users->error[]['code'] = 'msgErrorDB';
                } else {
                    $users->error[]['code'] = '117';
                }
            }
        }
    }

    $error = $objUtils->errorParse($users->error, $statusError['error']);
    $error = $objUtils->echoMessage($error, $statusError['error']);

}

$objTpl->tpl_load("change.password", "change.password.html", "line1px, error");
$objTpl->tpl_array("change.password", $ConfigManager);

$objTpl->tpl_array("change.password", $arrTplVars);
