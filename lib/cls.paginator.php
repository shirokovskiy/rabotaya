<?php
/*******************************************************************
* Created by Jimmy™. Shirokovskiy D.A. Sat Jan 07 18:23:06 MSK 2006
*
* Класс tplPagination реализующий работу разделения на страницы
* выводимых записей.
* Класс наследуется от класса Template (работа с шаблонами = cls.tpl.php)
*
*******************************************************************/

class tplPaginator extends Template {
    public $intQuantLinkPage = 5;    // Какое кол-во ссылок на страницы показывать на странице
    public $intQuantRecordPage = 15; // Какое кол-во записей показывать на странице
    public $intNumCurrentPage = 1;   // Текущая страница
    public $arrResult = array();     // Результат разбиения на страницы
    public $arrParseUrl = array();   // Массив, содержащий результат разбора URL
    public $strSqlLimit;             // Строка для запроса в ограничение выборки
    public $intRecordStart;          // С какого числа (записи) начинать следующую страницу
    public $strColorActivePage = '4294FF'; // css-класс текущей страницы в списке страниц
    public $strColorActiveStyle = 'page-active'; // Цвет текущей страницы в списке страниц
    public $strColorLinkStyle = 'link-b';  // css-класс ссылок по умолчанию
    public $strNameImageForward = 'arrow_one_right.gif'; // Имя изображения для ссылки "вперед"
    public $strNameImageBack = 'arrow_one_left.gif';     //  Имя изображения для ссылки "назад"
    public $strSqlOrder = '';            // Если требуется задать сортировку для SQL запроса
    public $intQuantityShowRecOnPage = '';  // Кол-во показанных записей на странице
    public $arrUrlAtrException = array();  // Список исключений атрибутов URL
    public $strVarName = 'arrTplVars'; // глобальная переменная шаблона
    public $strFrgVarName = 'paginator'; // Переменная хранения фрагмента - глобальная переменная
    public $strPaginatorTpl = 'paginator.tpl'; // Файл шаблона paginator'a

    private $intQuantPages; // Всего страниц
    private $intQuantAllRecord; // Сколько всего записей
    private $strPageMark; // Какая переменная отвечает за постраничный вывод

    // Конструктор класса
    function __construct($intQuantAllRecord, $strTplPath='',$p='stPage'){
        $this->intQuantAllRecord = $intQuantAllRecord;
        $this->strPageMark = $p;
        if (isset($_GET[$p]) && intval($_GET[$p])>0) $this->intNumCurrentPage = intval($_GET[$p]);
        $GLOBALS[$this->strVarName]['intNumСurrentPage'] = $this->intNumCurrentPage;
        $this->paParseUrl(); // Разбираем URL
        $this->Template($strTplPath);
    }

    public function paCreate() {
        $this->tpl_load($this->strFrgVarName, $this->strPaginatorTpl);
        // Считаем сколько всего страниц получится
        $this->intQuantPages = ceil($this->intQuantAllRecord/$this->intQuantRecordPage);
        // C какого номера страницы начинать
        $intStartNumLink = ($this->intQuantLinkPage*ceil($this->intNumCurrentPage/$this->intQuantLinkPage)-$this->intQuantLinkPage)+1;
        // Каким номером страницы заканчивать
        $intFinishNumLink = $intStartNumLink+$this->intQuantLinkPage-1;

        $this->arrParseUrl['pageUrlAtrib'][$this->strPageMark] = $intStartNumLink-1;
        $arrTmpRecord['strUrlAtr'] = $this->paUrlAtrCreate($this->arrParseUrl['pageUrlAtrib']);
        $GLOBALS[$this->strVarName]['strLinkBack'] = $this->arrParseUrl['pageSelf'].'?'.$arrTmpRecord['strUrlAtr'];

        for($i = $intStartNumLink; ($i<=$intFinishNumLink && $i<=$this->intQuantPages); $i++) {
            $arrTmpRecord['numPage'] = $this->arrParseUrl['pageUrlAtrib'][$this->strPageMark] = $i;
            $arrTmpRecord['strUrlAtr'] = $this->paUrlAtrCreate($this->arrParseUrl['pageUrlAtrib']);
            $arrTmpRecord['strLink'] = ($i!=$this->intNumCurrentPage) ? '<a href="'.$this->arrParseUrl['pageSelf'].'?'.$arrTmpRecord['strUrlAtr'].'" class="'.$this->strColorLinkStyle.'">'.$arrTmpRecord['numPage'].'</a>' : '<span style="color: #'.$this->strColorActivePage.'" class="'.$this->strColorActiveStyle.'">'.$arrTmpRecord['numPage'].'</span>';
            $arrPagesForTpl[]= $arrTmpRecord;
        }

        $this->arrParseUrl['pageUrlAtrib'][$this->strPageMark]++;
        $arrTmpRecord['strUrlAtr'] = $this->paUrlAtrCreate($this->arrParseUrl['pageUrlAtrib']);
        $GLOBALS[$this->strVarName]['strLinkForward'] = $this->arrParseUrl['pageSelf'].'?'.$arrTmpRecord['strUrlAtr'];

        $this->intRecordStart = ($this->intQuantRecordPage*$this->intNumCurrentPage-$this->intQuantRecordPage)+1;
        $this->strSqlLimit = " LIMIT ".($this->intRecordStart-1).", ".$this->intQuantRecordPage;

        $this->tpl_loop($this->strFrgVarName, 'pa.lst.pages', $arrPagesForTpl);

        $this->tpl_if($this->strFrgVarName
            , array(
                'pa.next.arrow.pages'  =>  ($this->intQuantPages>$intFinishNumLink) ? true : false, // стрелка вправо
                'pa.prev.arrow.pages'  =>  ($this->intNumCurrentPage>$this->intQuantLinkPage) ? true : false, // стрелка влево
            )
        );
    }

    public function paShow() {
        $GLOBALS[$this->strVarName]['strNameImageForward'] = $this->strNameImageForward;
        $GLOBALS[$this->strVarName]['strNameImageBack'] = $this->strNameImageBack;
        $this->tpl_array($this->strFrgVarName, $GLOBALS[$this->strVarName]);
        return $this->files[$this->strFrgVarName];
    }

    /**
     * Метод paParseUrl() производит разбор текущего URL. Возвращает коллекцию свойств URL типа:
     * array (
     *    'pageSelf' - имя модуля (страницы)
     *    ...
     *    список аргументов текущей страницы
     * )
     *
     * @return unknown
     */
    private function paParseUrl(){
        $this->arrParseUrl['pageSelf'] = substr(strrchr(preg_replace("/\?.*/", "", $_SERVER["REQUEST_URI"]), "/"), 1);
        $this->arrParseUrl['pageUrlAtrib'] = $_GET;
        return true;
    }

    private function paUrlAtrCreate($arr, $arg=''){
        $intFlagSep = 0;

        foreach ( $arr as $key => $value ) {
			if(!isset($this->arrUrlAtrException[$key]) || $this->arrUrlAtrException[$key]!=true) {
				$arg .= ($intFlagSep>0 ? '&' : '');
				$arg .= $key."=".$value;
			}

            $intFlagSep++;
        }
        return $arg;
    }
}
