<?php
/**
 * Class Utils
 *
 * Shirokovskiy D.2007 Jimmy. Wed Aug 29 16:19:23 MSD 2007
 */

class Utils {

	public $strCurrentDate
	, $lenCompared

	, $start_time
	, $stop_time
	, $elapsed_time;

    public $foundBads;

	public function __construct() {
		date_default_timezone_set('Europe/Moscow');
		$this->strCurrentDate = getdate(time());
	}

	/**
	 * Аналог strtoupper() для русских букв, если не поддерживается на сервере
	 *
	 * @param string $content текст для выводы в верхнем регистре
	 *
	 * @return string текст для вывода
	 */
	function toUpper($content) {
		$content = strtr($content, "абвгдеёжзийклмнорпстуфхцчшщъьыэюя", "АБВГДЕЁЖЗИЙКЛМНОРПСТУФХЦЧШЩЪЬЫЭЮЯ");
		return strtoupper($content);
	}

	/**
	 * Аналог strtolower()
	 *
	 * @param string $content текст для выводы в нижнем регистре
	 *
	 * @return string текст для вывода
	 */
	function toLower($content) {
		$content = strtr($content, "АБВГДЕЁЖЗИЙКЛМНОРПСТУФХЦЧШЩЪЬЫЭЮЯ", "абвгдеёжзийклмнорпстуфхцчшщъьыэюя");
		return strtolower($content);
	}

	/**
	 * Метод получения даты в различном формате
	 *
	 * @param unknown_type $format
	 * @param unknown_type $date
	 * @param unknown_type $spacer
	 * @return unknown
	 */
	function workDate($format=1, $date=null, $spacer=".") {
		$this->strDate = $date;
		if ( empty( $date ) ) {
			$date = date( "Y-m-d H:i:s" );
		}
		switch ($format) {
			// Input:  берется настоящее время
			// Output: 27 Марта&nbsp;2006&nbsp;г.
			case 1:
				$this->strDate = $this->strCurrentDate['mday'].' '.$this->arrAtMonth[$this->strCurrentDate['mon']].'&nbsp;'.$this->strCurrentDate['year'].'&nbsp;г.';
				break;

			// Input:  YYYY-MM-DD
			// Output: 27 Марта&nbsp;2006&nbsp;г.
			case 2:
				if (!empty($date)) {
					$this->arrayDate = explode("-", $date);  // $date in format YYYY-MM-DD
					$this->strDate = $this->arrayDate['2'].' '.$this->arrAtMonth[(int)$this->arrayDate['1']].'&nbsp;'.$this->arrayDate['0'].'&nbsp;г.';
				}
				break;

			// Input:  YYYY-MM-DD HH:II:SS
			// Output: 27.03.2006
			case 3:
				if (!empty($date)) {
					$this->arrayStr = explode(" ", $date);
					$this->arrayDate = explode("-", $this->arrayStr[0]);
					$this->strDate = $this->arrayDate['2'].$spacer.$this->arrayDate['1'].$spacer.$this->arrayDate['0'];
				}
				break;

			// Output like: 01 Января 2000 г. в 00:00
			case 4:
				if (!empty($date)) {
					$this->arrayStr = explode(" ", $date);
					$this->arrayDate = explode("-", $this->arrayStr[0]);
					$this->arrayTime = explode(":", $this->arrayStr[1]);
					$this->strDate = $this->arrayDate['2'].' '.$this->arrAtMonth[(int)$this->arrayDate['1']].' '.$this->arrayDate['0'].' г. в '.$this->arrayTime[0].':'.$this->arrayTime[1];
				}
				break;

			// Input:  YYYY-MM-DD HH:II:SS
			// Input:  YYYY-MM-DD
			// Output like: 01 Января 2000 г.
			case 5:
				if (!empty($date)) {
					$this->arrayStr = explode(" ", $date);
					$this->arrayDate = explode("-", $this->arrayStr[0]);
					$this->arrayTime = explode(":", $this->arrayStr[1]);
					if (intval($this->arrayDate['2']) == 0) {
						$this->strDate = $this->arrMonth[(int)$this->arrayDate['1']].' '.$this->arrayDate['0'].' .';
					} else {
						$this->strDate = $this->arrayDate['2'].' '.$this->arrAtMonth[(int)$this->arrayDate['1']].' '.$this->arrayDate['0'].' г.';
					}
				}
				break;

			case 6: // Output like: dd.mm.YYYY HH:ii
				if (!empty($date)) {
					$this->arrayStr = explode(" ", $date);
					$this->arrayDate = explode("-", $this->arrayStr[0]);
					$this->arrayTime = explode(":", $this->arrayStr[1]);
					$this->strDate = $this->arrayDate['2'].$spacer.$this->arrayDate['1'].$spacer.$this->arrayDate['0'].' '.$this->arrayTime['0'].':'.$this->arrayTime['1'];
				}
				break;

			case 7: // Output like: dd.mm.YY HH:ii
				if (!empty($date)) {
					$this->arrayStr = explode(" ", $date);
					$this->arrayDate = explode("-", $this->arrayStr[0]);
					$this->arrayTime = explode(":", $this->arrayStr[1]);
					$this->strDate = $this->arrayDate['2'].$spacer.$this->arrayDate['1'].$spacer.substr($this->arrayDate['0'], 2).' '.$this->arrayTime['0'].':'.$this->arrayTime['1'];
				}
				break;

			case 8: // Output like: dd Month YYYY
				if (!empty($date)) {
					$this->arrayDate = explode("-", $date);   // $date = YYYY-MM-DD
					if (preg_match('/:/', $this->arrayDate['2'])) { // if input like: YYYY-MM-DD HH:II:SS
						$this->arrayDate['2'] = substr( $this->arrayDate['2'], 0, 2 );
					}
					$this->strDate = $this->arrayDate['2'].' '.$this->arrAtMonth[(int)$this->arrayDate['1']].' '.$this->arrayDate['0'];
				}
				break;

			case 9: // Output like: dd Month YYYY года
				if (!empty($date)) {
					$this->arrayDate = explode("-", $date);   // $date = YYYY-MM-DD
					$this->strDate = $this->arrayDate['2'].' '.$this->arrAtMonth[(int)$this->arrayDate['1']].'&nbsp;'.$this->arrayDate['0'].'&nbsp;года';
				}
				break;

			case 10: // Output like: dd month YYYY, hh:mm
				if (!empty($date)) {
					$this->arrayStr = explode(" ", $date);    // $date = YYYY-MM-DD HH:mm:ss
					$this->arrayDate = explode("-", $this->arrayStr[0]);
					$this->arrayTime = explode(":", $this->arrayStr[1]);
					$this->strDate = $this->arrayDate['2'].' '.strtolower($this->arrAtMonth[(int)$this->arrayDate['1']]).'&nbsp;'.$this->arrayDate['0'].', '.$this->arrayTime['0'].':'.$this->arrayTime['1'];
				}
				break;

			case 11: // Output like: 01 Января 2000, 00:00
				if (!empty($date)) {
					$this->arrayStr = explode(" ", $date);    // $date = YYYY-MM-DD HH:mm:ss
					$this->arrayDate = explode("-", $this->arrayStr[0]);
					$this->arrayTime = explode(":", $this->arrayStr[1]);
					$this->strDate = $this->arrayDate['2'].' '.$this->arrAtMonth[(int)$this->arrayDate['1']].' '.$this->arrayDate['0'].', '.$this->arrayTime[0].':'.$this->arrayTime[1];
				}
				break;

			case 12: // Output like: 00:00
				$this->strDate = date('H:i', strtotime($date));
				break;

            // Input: DD.MM.YYYY
            // Output: YYYY-MM-DD (MySQL format of DATE)
            case 13:
                $this->strDate = date('Y-m-d', strtotime($date));
                break;
		}

		return $this->strDate;
	}

	function getDateByMY ( $iM, $iY ) {
		$date = false;
		if ( intval($iM) > 0 && intval($iY) > 0 && strlen($iY) == 4 ) {
			$M = str_pad( $iM, 2, '0', STR_PAD_LEFT );
			$date = $iY.'-'.$M.'-01';
		}
		return $date;
	}

	function parseBrText($text, $align='') {
		$crlf = chr(13).chr(10);
		$text = str_replace($crlf, '<br />', $text);
		return $text;
	}

	function substrText( $inMessage, $limitChar = 400, $isDots=false, $strOtherChars = '...' ) {
		$intMessageLength = strlen($inMessage);
		if ( $intMessageLength <= $limitChar ) {
			return $inMessage;
		}
		$tmpMessage = substr($inMessage, 0, $limitChar);
		$stopChar = strrpos($tmpMessage, " ");
		if (false !== $stopChar) {
			$outMessage = substr($tmpMessage, 0, $stopChar);
		} else {
			$outMessage = $tmpMessage;
		}
		if($isDots) $outMessage = $outMessage.' '.$strOtherChars;
		return $outMessage;
	}

	/**
	 * Функция для удаления опасных символов из строки
	 *
	 * @param unknown_type $str
	 * @return unknown
	 */
	function PregTrim($str) {
		return preg_replace("/[^\x20-\xFF]/","",@strval($str));
	}

	/**
	 * Проверка корректности email
	 *
	 * @param string $mail
	 * @return string
	 */
	function checkEmail($mail) {
		$mail = trim($this->PregTrim($mail));
		if (strlen($mail) == 0) return false;
		if (!preg_match('/[A-Za-z0-9][\w\.-]*@[A-Za-z0-9][\w\-\.]+\.[A-Za-z0-9]{2,6}/', $mail)) {
			return false;
		}
		return $mail;
	}

    public function isValidEmail($mail)
    {
        return $this->checkEmail($mail);
    }

    public function isValidUrl($url)
    {
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE) {
            return false;
        }

        return true;
    }

    public static function makeClickableLinks($s, $target_blank = true) {
        return preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@', '<a href="$1"'.($target_blank ? ' target="_blank"' : '').'>$1</a>', $s);
    }

	/**
	 * Транслитерация русских символов
	 *
	 * @param unknown_type $str
	 * @return unknown
	 */
	function translitKyrToLat($str) {
		if (!is_string($str)) {
			return null;
		}

		$arr=array(
			"а" => 'a',
			"б" => 'b',
			"в" => 'v',
			"г" => 'g',
			"д" => 'd',
			"е" => 'e',
			"ё" => 'yo',
			"ж" => 'zh',
			"з" => "z",
			"и" => 'i',
			"й" => 'y',
			"к" => 'k',
			"л" => 'l',
			"м" => 'm',
			"н" => 'n',
			"о" => 'o',
			"п" => 'p',
			"р" => 'r',
			"с" => 's',
			"т" => 't',
			"у" => 'u',
			"ф" => 'f',
			"х" => 'h',
			"ц" => 'c',
			"ч" => 'ch',
			"ш" => 'sh',
			"щ" => 'sh',
			"ъ" => '',
			"ь" => '',
			"ы" => 'i',
			"э" => 'e',
			"ю" => 'yu',
			"я" => 'ya',

			"А" => "A",
			"Б" => "B",
			"В" => "V",
			"Г" => "G",
			"Д" => "D",
			"Е" => "E",
			"Ё" => "Yo",
			"Ж" => "Zh",
			"З" => "Z",
			"И" => 'I',
			"Й" => "J",
			"К" => "K",
			"Л" => "L",
			"М" => "M",
			"Н" => "N",
			"О" => "O",
			"П" => "P",
			"Р" => "R",
			"С" => "S",
			"Т" => "T",
			"У" => "U",
			"Ф" => "F",
			"Х" => "H",
			"Ц" => "C",
			"Ч" => "Ch",
			"Ш" => "Sh",
			"Щ" => "Sh",
			"Ъ" => "",
			"Ь" => "",
			"Ы" => "I",
			"Э" => "E",
			"Ю" => "Yu",
			"Я" => "Ya",
		);

		return strtr($str,$arr);
	}

	/**
	 * Избавляемся от нежелательных символов в строке
	 *
	 * @param unknown_type $str
	 * @return unknown
	 */
	function translitBadChars($str, $arr_more_chars_to_replace = array()) {
		if (!is_string($str)) {
			return null;
		}

		$arr=array(
			"[" => "",
			"]" => "",
			"^" => "",
			"?" => "",
			"*" => "",
			"%" => "",
			":" => "",
			";" => "",
			"$" => "",
			"#" => "",
			"@" => "",
			"|" => "",
			"/" => "",
			"\\" => "",
			"!" => "",
			"(" => "",
			")" => "",
			"=" => "",
			"+" => "",
			"`" => "",
			"~" => "",
			"–" => "-",
			"'" => "",
			"\"" => "",
			"," => "",
			"«" => "",
			"»" => ""
		);

        if(!empty($arr_more_chars_to_replace)) {
            foreach ($arr_more_chars_to_replace as $v) {
                $arr_change[$v] = '_';
            }

            $str = strtr($str, $arr_change);
        }

		return strtr($str,$arr);
	}

	function start() {
		$this->start_time = $this->getmicrotime();
	}

	function stop() {
		$this->stop_time = $this->getmicrotime();
		if (function_exists('bcsub'))
			$this->elapsed_time = bcsub($this->stop_time,$this->start_time,6);
		else
			$this->elapsed_time = $this->stop_time-$this->start_time;

		$this->elapsed_time = '<p style="font-size: 9px;">Время выполнения скрипта на сервере: <b>'.number_format($this->elapsed_time, 6).'</b> секунд.</p>';
		return $this->elapsed_time;
	}

	function getmicrotime() {
		list($usec, $sec) = explode(" ",microtime());
		return ((float)$usec + (float)$sec);
	}

	function getSqlQuery( &$db ) {
		if(empty($db->quantQuery))
			$db->quantQuery=0;
		echo "\n".'<!-- При генерации страницы было '.$db->quantQuery.' запроса(ов) к MySQL -->'."\n";
	}

	/**
	 * Генерация пароля
	 *
	 * @param unknown_type $passLen
	 * @return unknown
	 */
	static public function getRandomPassw($passLength = 7) {
		$allowChar = "243abcdefGH2345JKLnpqrstUVWXYZ5678ghijkmABCDE6789FMuvwxyzNPQRST9";

		srand((double)microtime()*1000000);

        $password = '';

		for($i=0; $i<$passLength; $i++) {
			$password .= $allowChar[rand()%strlen($allowChar)];
		}

		return $password;
	}

	/**
	 * Метод проверки строкового формата даты.
	 * Например: на входе строка типа `1999-01-02` или `1999-01-19 23:23:49`
	 * тогда TRUE, иначе FALSE
	 *
	 * @param unknown_type $date
	 * @return unknown
	 */
	function dateValid ($date) {
		$arrayStr = explode(" ", $date);
		$arrayDate = explode("-", $arrayStr[0]);

		if ( !empty( $arrayStr[1] ) && preg_match('/:/', $arrayStr[1]) ) {
			$arrayTime = explode(":", $arrayStr[1]);
		}

		if ( count($arrayDate) != 3 ) {
			return false;
		} else {
			// year
			if ( strlen($arrayDate[0]) != 4 || intval($arrayDate[0]) == 0 ) return false;
			// month
			if ( strlen($arrayDate[1]) != 2 || intval($arrayDate[1]) == 0 ) return false;
			// day
			if ( strlen($arrayDate[2]) != 2 || intval($arrayDate[2]) == 0 ) return false;
		}

		if ( !empty( $arrayTime ) && is_array( $arrayTime ) ) {
			if ( count($arrayTime) != 3 ) return false;
			// hours
			if ( strlen($arrayTime[0]) != 2 ) return false;
			// minutes
			if ( strlen($arrayTime[1]) != 2 ) return false;
			// seconds
			if ( strlen($arrayTime[2]) != 2 ) return false;
		}

		return true;
	}

	/**
	 * Метод для Суммы прописью
	 *
	 * @param unknown_type $i
	 * @param unknown_type $words
	 * @param unknown_type $fem
	 * @param unknown_type $f
	 */
	function semantic($i,&$words,&$fem,$f) {
		$words="";
		$fl=0;

		if ( $i >= 100 ) {
			$jkl = intval($i / 100);
			$words.=$this->hang[$jkl];
			$i%=100;
		}

		if ( $i >= 20 ) {
			$jkl = intval($i / 10);
			$words.=$this->des[$jkl];
			$i%=10;
			$fl=1;
		}

		switch ($i) {
			case 1: $fem=1; break;
			case 2:
			case 3:
			case 4: $fem=2; break;
			default: $fem=3; break;
		}

		if ( $i ) {
			if( $i < 3 && $f > 0 ) {
				if ( $f >= 2 ) {
					$words.=$this->_1_19[$i];
				} else {
					$words.=$this->_1_2[$i];
				}
			} else {
				$words.=$this->_1_19[$i];
			}
		}
	}


	/**
	 * Метод преобразования ЦИФРЫ в СТРОКУ
	 * Пример: 1204 => Одна тысяча двести четыре
	 *
	 * @param unknown_type $L
	 * @param unknown_type $is_kop
	 * @return unknown
	 */
	function num2str(/*Summa (float)*/ $L, /*print kopeek*/ $is_kop = false, /*currency*/ $currency = "rub") {
		$s=" ";
		$s1=" ";
		$s2=" ";
		$kop=intval( ( $L*100 - intval( $L )*100 ));
		$L=intval($L);

		//
		switch ($currency) {
			case "usd":
				$currency_arr = $this->nameusd;
				$coins_arr = $this->cents;
				break;
			case "eur":
				$currency_arr = $this->nameeur;
				$coins_arr = $this->cents;
				break;
			case "kzt":
				$currency_arr = $this->namekzt;
				$coins_arr = $this->tengecents;
				break;
			default:
				$currency_arr = $this->namerub;
				$coins_arr = $this->kopeek;
		}

		if ( $L>=1000000000 ) {
			$many=0;
			$this->semantic(intval($L / 1000000000),$s1,$many,3);
			$s.=$s1.$this->namemrd[$many];
			$L%=1000000000;
		}

		if ( $L >= 1000000 ) {
			$many=0;
			$this->semantic(intval($L / 1000000),$s1,$many,2);
			$s.=$s1.$this->namemil[$many];
			$L%=1000000;

			if ( $L==0 ) {
				$s.=$currency_arr[3];
			}
		}

		if ( $L >= 1000 ) {
			$many=0;
			$this->semantic(intval($L / 1000),$s1,$many,1);
			$s.=$s1.$this->nametho[$many];
			$L%=1000;

			if($L==0){
				$s.=$currency_arr[3];
			}
		}

		if ($L != 0) {
			$many=0;
			$this->semantic($L,$s1,$many,0);
			$s.=$s1.$currency_arr[$many];
		}

		if ($is_kop) {
			if ($kop > 0) {
				$many=0;
				$this->semantic($kop,$s1,$many,1);
				$s.=$s1.$coins_arr[$many];
			} else {
				$s.=" 00 ".trim($coins_arr[3]);
			}
		}

		return $s;
	}


    /**
     * Метод обработки ошибок
     *
     * @param $array_vars
     * @param $error_code
     * @return null|string
     */
    public function errorParse($array_vars, $error_code) {
		global $errMsg;
		if (!empty($array_vars)) {
			if ($error_code==1)
				$class = 'class="tab-10-r"';
			else
				$class = 'class="tab-10-green"';

			if (is_array($array_vars)) {
				if (count($array_vars)>1) {
					foreach ($array_vars as $keys => $values) {
						if (!empty($array_vars[$keys]['code'])) {
							$this->message .= '<li '.$class.'>'.str_replace('{vars}', '"<b>'.$array_vars[$keys]['txt'].'</b>"', $errMsg[$array_vars[$keys]['code']]).'</li>';
						}
					}
				} else {
					foreach ($array_vars as $keys => $values) {
						if (!empty($array_vars[$keys]['code'])) {
							$this->message .= str_replace('{vars}', '"<b>'.trim($array_vars[$keys]['txt']).'</b>"', $errMsg[$array_vars[$keys]['code']]);
						}
					}
				}
			} else {
				$this->message .= '<span '.$class.'>'.$errMsg[$array_vars].'</span>';
			}

			return $this->message;
		}
		return null;
	}


    /**
     * Метод вывода (печати) ошибки
     *
     * @param $message
     * @param $err
     * @return null|string
     */
    public function echoMessage($message, $err) {
		$style = " style='border:dashed 1px #".(!empty($err) ? 'FF0000' : '4AAE10')."'";
        $error_template = '';
		if (!empty($message)) {
			$error_template .= '<table style="margin:7px 0 9px 0;" cellpadding="2" cellspacing="1" width="450" align="center">';
			if ($err == 1) {
				$error_template .= '<tr bgcolor="#FCF1F0"><td '.$style.'><p class="tab-10-r" align="center"><b>Ошибка : </b>'.$message.'</td>';
			} elseif ($err == 2) {
				$error_template .= '<tr bgcolor="#FBFFF8"><td '.$style.'><p class="tab-10-r" align="center"><b>Заметка : </b>'.$message.'</td>';
			} else {
				$error_template .= '<tr bgcolor="#FBFFF8"><td '.$style.'><p class="tab-10-green" align="center"><span style="color:#4AAE10;">'.$message.'</span></td>';
			}
			$error_template .= '</tr></table>';
			return $error_template;
		}

		return null;
	}


    /**
     * Сравнение двух строк идентичны ли они по размеру
     *
     * @param $strFirst
     * @param $strSecond
     * @return bool
     */
    function strCompared( $strFirst, $strSecond ) {
		$lenFirstStr = strlen(trim($strFirst));
		$lenSecondStr = strlen(trim($strSecond));

		if ($lenFirstStr == $lenSecondStr) {
			return true;
		} else {
			$this->lenCompared = abs($lenFirstStr-$lenSecondStr); // Разница в кол-ве символов
			return false;
		}
	}



	/**
	 * Checks if string contains bad symbols
	 *
	 * @param unknown_type $str
	 * @return unknown
	 */
	function checkBadChars( $str ) {
		$this->foundBads = array();

		if ( empty($str) )
			return true;

		if ( preg_match("/[\*\? #\^&%\/@\!\(\)\{\}\[\]\=\+`'".'"'."<>:;|~]+/", $str, $this->foundBads) )
			return true;

		return false;
	}

	function doExplode($sep = null, $str = null) {
		if (empty($str) || empty($sep)) {
			return false;
		}

		$arr = explode( $sep, $str );

		if ( is_array( $arr ) ) {
			foreach ($arr as $k => $v) {
				$arr[$k] = trim($v);
			}

			return $arr;
		}

		return $str;
	}

	/** ==========================================================================
	 * Переменные
	 */
	public $arrDays = array (
		0 => "Воскресенье", 1 => "Понедельник", 2 => "Вторник", 3 => "Среда",
		4 => "Четверг",     5 => "Пятница",     6 => "Суббота"
	);

	public $arrShortDays = array (
		0 => "Вск.",  1 => "Пн.",   2 => "Вт.",   3 => "Ср.",
		4 => "Чт.",   5 => "Пт.",   6 => "Сб."
	);

	public $arrAtMonth = array (
		1 => "Января",      2 => "Февраля",     3 => "Марта",
		4 => "Апреля",      5 => "Мая",         6 => "Июня",
		7 => "Июля",        8 => "Августа",     9 => "Сентября",
		10  => "Октября",   11  => "Ноября",    12  => "Декабря",
	);

	public $arrMonth = array (
		1 => "Январь",      2 => "Февраль",     3 => "Март",
		4 => "Апрель",      5 => "Май",         6 => "Июнь",
		7 => "Июль",        8 => "Август",      9 => "Сентябрь",
		10  => "Октябрь",   11  => "Ноябрь",    12  => "Декабрь",
	);

	public $arrRusChars = array (
		1   => "А",
		2   => "Б",
		3   => "В",
		4   => "Г",
		5   => "Д",
		6   => "Е",
		7   => "Ж",
		8   => "З",
		9   => "И",
		10  => "К",
		11  => "Л",
		12  => "М",
		13  => "Н",
		14  => "О",
		15  => "П",
		16  => "Р",
		17  => "С",
		18  => "Т",
		19  => "У",
		20  => "Ф",
		21  => "Х",
		22  => "Ц",
		23  => "Ч",
		24  => "Ш",
		25  => "Щ",
		26  => "Э",
		27  => "Ю",
		28  => "Я"
	);

    public $arrEngChars = array (
        1   => "A",
        2   => "B",
        3   => "C",
        4   => "D",
        5   => "E",
        6   => "F",
        7   => "G",
        8   => "H",
        9   => "I",
        10  => "J",
        11  => "K",
        12  => "L",
        13  => "M",
        14  => "N",
        15  => "O",
        16  => "P",
        17  => "Q",
        18  => "R",
        19  => "S",
        20  => "T",
        21  => "U",
        22  => "V",
        23  => "W",
        24  => "X",
        25  => "Y",
        26  => "Z"
    );


	public $arrayCyrChar = array (
		192 => "A",
		193 => "B",
		194 => "V",
		195 => "G",
		196 => "D",
		197 => "E",
		168 => "Jo",
		198 => "Zh",
		199 => "Z",
		200 => "I",
		201 => "Ij",
		202 => "K",
		203 => "L",
		204 => "M",
		205 => "N",
		206 => "O",
		207 => "P",
		208 => "R",
		209 => "S",
		210 => "T",
		211 => "U",
		212 => "F",
		213 => "H",
		214 => "Ts",
		215 => "Ch",
		216 => "Sh",
		217 => "Sсh",
		219 => "Y",
		220 => "",
		218 => "",
		221 => "E",
		222 => "Ju",
		223 => "Ja",
		224 => "a",
		225 => "b",
		226 => "v",
		227 => "g",
		228 => "d",
		229 => "e",
		184 => "jo",
		230 => "zh",
		231 => "z",
		232 => "i",
		233 => "ij",
		234 => "k",
		235 => "l",
		236 => "m",
		237 => "n",
		238 => "o",
		239 => "p",
		240 => "r",
		241 => "s",
		242 => "t",
		243 => "u",
		244 => "f",
		245 => "h",
		246 => "ts",
		247 => "ch",
		248 => "sh",
		249 => "sсh",
		251 => "y",
		252 => "",
		250 => "",
		253 => "e",
		254 => "ju",
		255 => "ja"
	);

	/*** Thu Sep 15 14:11:47 MSD 2005, Shirokovskiy D/Jimmy
	 ***
	 ** Массив времени для часов
	 */
	public $arrHours = array(
		0 => 'часов',
		1 => 'час',
		2 => 'часа',
		3 => 'часа',
		4 => 'часа',
		5 => 'часов',
		6 => 'часов',
		7 => 'часов',
		8 => 'часов',
		9 => 'часов',
		10 => 'часов',
		11 => 'часов',
		12 => 'часов',
		13 => 'часов',
		14 => 'часов',
		15 => 'часов',
		16 => 'часов',
		17 => 'часов',
		18 => 'часов',
		19 => 'часов',
		20 => 'часов',
		21 => 'час',
		22 => 'часа',
		23 => 'часа'
	);

	/** >>************************************************************************\
	 ** Thu Oct 19 14:05:44 MSD 2006, Shirokovskiy Dmitry aka Jimmy™
	 * Для Суммы Прописью **/
	public $_1_2=array( "1"=>"одна ","2"=>"две ");

	public $_1_19=array( "1"=>"один ",
		"2"=>"два ",
		"3"=>"три ",
		"4"=>"четыре ",
		"5"=>"пять ",
		"6"=>"шесть ",
		"7"=>"семь ",
		"8"=>"восемь ",
		"9"=>"девять ",
		"10"=>"десять ",
		"11"=>"одиннацать ",
		"12"=>"двенадцать ",
		"13"=>"тринадцать ",
		"14"=>"четырнадцать ",
		"15"=>"пятнадцать ",
		"16"=>"шестнадцать ",
		"17"=>"семнадцать ",
		"18"=>"восемнадцать ",
		"19"=>"девятнадцать "  );

	public $des=array( "2"=>"двадцать ",
		"3"=>"тридцать ",
		"4"=>"сорок ",
		"5"=>"пятьдесят ",
		"6"=>"шестьдесят ",
		"7"=>"семьдесят ",
		"8"=>"восемдесят ",
		"9"=>"девяносто " );

	public $hang=array(  "1"=>"сто ",
		"2"=>"двести ",
		"3"=>"триста ",
		"4"=>"четыреста ",
		"5"=>"пятьсот ",
		"6"=>"шестьсот ",
		"7"=>"семьсот ",
		"8"=>"восемьсот ",
		"9"=>"девятьсот " );

	public $namerub=array( "1"=>"рубль ",
		"2"=>"рубля ",
		"3"=>"рублей " );

	public $nametho=array( "1"=>"тысяча ",
		"2"=>"тысячи ",
		"3"=>"тысяч " );

	public $namemil=array( "1"=>"миллион ",
		"2"=>"миллиона ",
		"3"=>"миллионов " );

	public $namemrd=array( "1"=>"миллиард ",
		"2"=>"миллиарда ",
		"3"=>"миллиардов " );

	public $kopeek=array(  "1"=>"копейка ",
		"2"=>"копейки ",
		"3"=>"копеек " );
}
