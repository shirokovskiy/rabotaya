<?php
/*******************************************
**  Tue Oct 02 23:19:12 MSD 2007, Shirokovskiy Dmitry aka Jimmy
**  [jimmy.webstudio at gmail dot com]
**  XML Parser (Universal)
*******************************************/


class jXmlParser {
  public $arrData;
  public $arrXml;
  public $arrPreSetTags;

  private $xmlParserId;
  private $activeTags = array(); // for control
  private $debug;
  private $counterTag = 0;

  function jXmlParser( $arrXmlForParser, $arrNeededXMLTags = array(), $debug = false ) {
    $this->arrXml = $arrXmlForParser;

    if ( !is_array( $this->arrXml ) || empty( $this->arrXml ) ) {
      return false;
    }

    $this->arrPreSetTags = $arrNeededXMLTags;
    $this->debug = $debug;

    $this->xmlParserId = xml_parser_create();
    xml_parser_set_option( $this->xmlParserId, XML_OPTION_CASE_FOLDING, true);
    xml_set_element_handler( $this->xmlParserId, array(&$this, "startElement"), array(&$this, "endElement") );
    xml_set_character_data_handler( $this->xmlParserId, array(&$this, "characterData") );
  }

  /******************************************************************/
  /******************************************************************/
  /******** Основные методы XML парсера *****************************/
  /******************************************************************/
  /******************************************************************/

  function startElement ($parser, $elementName, $elementAttr) {
    if ( $this->debug ) {
      print_r( "startElement <b>$elementName</b>" ); echo("\n");

      if ( !empty($elementAttr) ) {
        print "elementAttr ";
        print_r( $elementAttr );
        print "\n";
      }
    }

    if ( is_array( $this->arrPreSetTags ) && !empty( $this->arrPreSetTags ) ) {
      if ( in_array($elementName, $this->arrPreSetTags) ) {
        array_push( $this->activeTags, $elementName );

        foreach ( $this->activeTags as $activeTag ) {
          $t .= "['$activeTag']";
        }

        $this->counterTag += 1;
        $this->arrData[$elementName.':'.$this->counterTag]['attr'] = $elementAttr;
      }
    } else {
      array_push( $this->activeTags, $elementName );

      foreach ( $this->activeTags as $k => $activeTag ) {
        $t .= "['$activeTag:$k']";
      }

      $this->counterTag += 1;
      $this->arrData[$elementName.':'.$this->counterTag]['attr'] = $elementAttr;
    }

    print_r( $t ."\n" );
  }


  /**
   * Обработчик конечных тэгов (закрывающих)
   *
   * @param unknown_type $parser
   * @param unknown_type $elementName
   */
  function endElement ($parser, $elementName) {
    if ( $this->debug ) {
      print_r( "endElement <b>/$elementName</b>" ); echo("\n");
    }

    array_pop( $this->activeTags );
    $this->counterTag -= 1;
  }


  /**
   * Обработчик данных
   *
   * @param unknown_type $parser
   * @param unknown_type $elementData
   */
  function characterData ($parser, $elementData) {
    $elementData = trim($elementData);

    if ( $this->debug ) {
      echo("characterData ");
      if ( !empty( $elementData ) ) {
        print_r( '\n<b>'.$elementData.'</b>' );
      } else {
        echo( "EMPTY" );
      }
      echo("\n");
    }

    if ( !is_null($this->activeTag) ) {
      $this->arrData[$this->activeTag]['data'] = $elementData;
    }
  }


  function xmlParse () {
  	for ( $i = 0; $i < count( $this->arrXml ); $i++ ) {
    	$line = $this->arrXml[$i];
    	if ( !xml_parse( $this->xmlParserId, $line) ) {
    	  $flgParseError = true;
    	}
    }
  }
}


$xmlFile = "c:/development/jzend.xml";
$arrXml = file( $xmlFile );

$arrSearchTags = array();

$objXmlParser = new jXmlParser( $arrXml, $arrSearchTags, true );
$objXmlParser->xmlParse();

echo "\n\n\n=====================\n\n\n";
print_r( $objXmlParser->arrData );
echo "\n";
die("= = =\n".__FILE__.": ".__LINE__."\n\n");
