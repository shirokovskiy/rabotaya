<?php
/**
 * MySQL DB Class
 *
 * Shirokovskiy Dmitry edition (shirokovskij@mail.ru)
 * 2006 (c)
 */
include_once "cls.utils.php";

class Db extends Utils {
	public  $conn,
            $quantQuery = 0;

	private $debugOn = false;

    /**
     * @param null $dbConfig
     */
    function __construct($dbConfig = null) {
        parent::__construct();
        if (empty($dbConfig)) {
            global $_db_config;
            if (!empty($_db_config) && is_array($_db_config)) {
                $dbConfig = &$_db_config;
            }
        }
		$this->connect($dbConfig);

		$mver = mysql_get_server_info();
		$mver = explode(".", $mver);

		if (!empty($mver) && is_array($mver) && ( $mver[0]>4 || ($mver[0]=4 && $mver[1]>=1) ) ) {
			if ( isset($dbConfig['charset']) ) {
				if ( $mver[0]>=5 && (($mver[1]>=0 && $mver[2]>=7) || $mver[1]>0) ) {
					mysql_set_charset( $dbConfig['charset'] , $this->conn);
				} else {
					if (!$this->query("SET NAMES '".$dbConfig['charset']."'")) {
						$this->mysqlerror();
					}
				}
			}
		}
	}

	function connect($dbConfig, $err = true) {
		if ( isset($this->conn) && false !== $this->conn ) {
			$this->disconnect();
		}

		$this->conn = mysql_connect($dbConfig['host'], $dbConfig['login'], $dbConfig['password'], true);
		if (!$this->conn) if ($err) die("Can't connect to DB: ".$dbConfig['dbname']." on ".$dbConfig['host']); else return false;
		if (!$this->select_db($dbConfig['dbname'])) if ($err) die("Can`t select DB:".$dbConfig['dbname']); else return false;

		return true;
	}

	function select_db($dbname) {
		return mysql_select_db($dbname, $this->conn);
	}

	function disconnect() {
		return mysql_close($this->conn);
	}

	function query($query, $show_error = true) {
		if ($this->debugOn) $time_start = $this->getmicrotime();
		if ( false == $rc = mysql_query($query, $this->conn)) {
			if ( $show_error ) {
				echo("<b>MySQL Class Raport</b> at function (".__FUNCTION__."):");echo "<br />\n";
				print_r( $query );echo("<br />\n");
				$this->mysqlerror( true );
                if ($this->debugOn) {
                    $this->trace_log($query, 2, null, true);
                }
			} else {
				$message = $query."\n".$this->mysqlerror();
                if ($this->debugOn) $this->trace_log($message, 1, null, true);
			}
		} else {
			if ($this->debugOn) {
				$time_all = $this->getmicrotime() - $time_start;
				$performance_time = number_format($time_all,5);
				$asterisk = "";
				if ($performance_time > 0.9) {
					$asterisk = ">>> ";
				}
				$query_array = explode(" ",strtolower($query));
				switch ($query_array[0]) {
					case 'select':
						$a_general = array("START TRANSACTION","ROLLBACK","COMMIT");
						$cnt=0;
						if (array_search(strtoupper($query),$a_general)===false && $rc !== FALSE)
						{
							$cnt = $this->rows($rc);
						}
						$this->trace_log("|$asterisk Время:".number_format($time_all,5)." \t| Кол.во зап.: ".$cnt." \t| ".date("dM H:i:s ")." | ".(basename($_SERVER['PHP_SELF']))." | ".$query.";", 2);
						break;
					case 'delete':
					case 'update':
					case 'insert':
						$this->trace_log("|$asterisk Время:".number_format($time_all,5)." \t| \t\t\t| ".date("dM H:i:s ")." | ".(basename($_SERVER['PHP_SELF']))." | ".$query.";", 2);
						break;
				}
			}
		}

		++$this->quantQuery;
		return $rc;
	}

	function rows ($result) {
		if (!$result) return null;
		return mysql_num_rows($result);
	}

    /**
     * @param $query
     * @param $i
     * @param bool $err
     * @return array|null|string
     */
    function fetch ($query, $i=-1, $err = true) {
		$result = $this->query($query, $err);

		if (!$result) { return null; }

		if ($i==-1) {
			return mysql_fetch_assoc($result);
		} else {
			if ($this->rows($result)>0) {
				return mysql_result($result, 0, $i);
			}
		}

		return null;
	}

    /**
     * @param $query
     * @param bool $err
     * @return array|null
     */
    function fetchall($query, $err = true) {
		$result = $this->query($query, $err);

		if ($result) {
			while ($a = mysql_fetch_assoc($result)) {
				$r[]=$a;
			}
		}

		if (!isset($r) || !is_array($r))
			return null;

		return $r;
	}

	function fetchcol ($query, $i=0) {
		$result = $this->query($query);

		if (!$result) return null;

		$r=array();

		if ($result)
			while ($a=mysql_fetch_array($result,MYSQL_NUM)) $r[]=$a[$i];

		return $r;
	}

	function affected () {
		return mysql_affected_rows($this->conn);
	}

	function insert_id() {
		return mysql_insert_id($this->conn);
	}

	function mysqlerror( $print_error = false ) {
		if ( $print_error ) {
			print_r( mysql_error($this->conn) ); echo("\n<br />");
			$out = null;
		} else {
			$out = print_r( mysql_error($this->conn), true );
		}

		return $out;
	}

	function get_db_name() {
		$db_name = $this->fetch( "SELECT DATABASE() AS dbName", 0 );
		return $db_name;
	}

	/**
	 * Запись лога SQL-запросов
	 *
	 * @param $message - сообщение
	 * @param int $status - статус(1-error,2-success)
	 * @param string $fname - имя файла(без пути и расширения)
	 * @param bool $backtrace
	 * @return void
	 */
	function trace_log($message, $status = 1, $fname="", $backtrace = false)
	{
		$isLogIn = 'db.';

		if ($fname!="")
			$logFile=$fname;
		else {
			switch ($status) {
				case (1):
					$logFile=$isLogIn."error_".date("Y-m-d_H");
					break;
				case (2):
					$logFile=$isLogIn."success_".date("Y-m-d_H");
					break;
			}
		}

		if(!is_dir(DROOT."/logs")) {
			mkdir(DROOT."/logs", 0775);
		}

		$curLog = DROOT."/logs/".$logFile.".log";
		$fd = fopen($curLog, "a+");
		fwrite($fd, $message."\n".($backtrace? print_r( debug_backtrace() , true)."\n\n" : ''));
		fclose($fd);
	}

	function getmicrotime()
	{
		list($usec, $sec) = explode(" ", microtime());
		return ((float)$usec + (float)$sec);
	}

    public function setDebugMode($mode = true)
    {
        $this->debugOn = $mode;
    }

    public function setDebugModeOn()
    {
        $this->setDebugMode(true);
    }

    public function setDebugModeOff()
    {
        $this->setDebugMode(false);
    }
}
