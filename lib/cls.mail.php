<?php
/* **********************************************************************
 *
 *  Возможности:
 *
 *  отсылка писем
 *    - в текстовом формате
 *    - в формате HTML
 *    - в текстовом формате с вложениями
 *    - в HTML формате с вложенными файлами
 *    - в HTML формате с включенными в HTML изображениями
 *    - в HTML формате с включенными в HTML изображениями и вложенными файлами
 *
 * ********************************************************************** */


class clsMail {
    public $debug_status = "yes" // Режим отладки "yes" - включен | "no" - выключен | "halt" - включен с остановкой выполнения по ошибке
    , $charset_k = "UTF-8"
    , $charset = "UTF-8"
    , $mail_subject = "Mail Class"
    , $mail_from = "Rabota-ya Subscribers <info@rabota-ya.ru>"
    , $mail_to
    , $mail_cc
    , $mail_bcc
    , $mail_text
    , $mail_html
    , $mail_type
    , $mail_header
    , $mail_body
    , $mail_reply_to
    , $mail_return_path
    , $attachments_index
    , $attachments = array()
    , $attachments_img = array()
    , $boundary_mix
    , $boundary_rel
    , $boundary_alt
    , $sended_index;

    /**
     * @var array Возможные ошибки при отсылке письма
     */
    public $error_msg = array(
        1  =>  'Письмо не было отослано'
        , 2  =>  'Не законченное тело письма'
        , 3  =>  'Не указан получатель'
        , 4  =>  'Не корректный e-mail адрес'
        , 5  =>  'Открытие файла'
    );

    /**
     * @var array Описание типов файлов
     */
    public $mime_types = array(
        'gif'  => 'image/gif'
        , 'jpg'  => 'image/jpeg'
        , 'jpeg' => 'image/jpeg'
        , 'jpe'  => 'image/jpeg'
        , 'bmp'  => 'image/bmp'
        , 'png'  => 'image/png'
        , 'tif'  => 'image/tiff'
        , 'tiff' => 'image/tiff'
        , 'swf'  => 'application/x-shockwave-flash'
        , 'doc'  => 'application/msword'
        , 'xls'  => 'application/vnd.ms-excel'
        , 'ppt'  => 'application/vnd.ms-powerpoint'
        , 'pdf'  => 'application/pdf'
        , 'ps'   => 'application/postscript'
        , 'eps'  => 'application/postscript'
        , 'rtf'  => 'application/rtf'
        , 'bz2'  => 'application/x-bzip2'
        , 'gz'   => 'application/x-gzip'
        , 'tgz'  => 'application/x-gzip'
        , 'tar'  => 'application/x-tar'
        , 'zip'  => 'application/zip'
        , 'html' => 'text/html'
        , 'htm'  => 'text/html'
        , 'txt'  => 'text/plain'
        , 'css'  => 'text/css'
        , 'js'   => 'text/javascript'
    );

    /**
     * Constructor
     *
     * @return clsMail
     */
    function __construct(){
        $this->boundary_mix = "jimmy_mix_".md5(uniqid(rand()));
        $this->boundary_rel = "jimmy_rel_".md5(uniqid(rand()));
        $this->boundary_alt = "jimmy_alt_".md5(uniqid(rand()));
        $this->attachments_index = 0;
        $this->sended_index = 0 ;
        if(!defined('BR')){
            define('BR', "\n", TRUE);
        }
    }


    /**
     * Метод, устанавливающий адрес отправителя письма
     * void set_from(string mail_from, [string name])
     *
     * @param $mail_from
     * @param string $name
     */
    public function set_from( /*string*/ $mail_from, /*string*/ $name = ""){
        if ($this->validate_mail($mail_from)){
            $this->mail_from = !empty($name) ? "=?".$this->charset."?B?".base64_encode($name)."?= <".$mail_from.">" : $mail_from;
        }
        else {
            $this->mail_from = "Rabota-Ya.ru <info@rabota-ya.ru>";
        }
    }

    /**
     * Метод, устанавливающий получателя письма
     * bool set_to(string mail_to, [string name])
     *
     * @param $mail_to
     * @param string $name
     * @return bool
     */
    public function set_to($mail_to, $name = ""){
        if ($this->validate_mail($mail_to)){
            $this->mail_to = !empty($name) ? "=?".$this->charset."?B?".base64_encode($name)."?= <".$mail_to.">" : $mail_to;
            return true;
        }
        return false;
    }

    /**
     * Метод, устанавливающий получателя копии письма
     * bool set_cc(string mail_cc, [string name])
     *
     * @param $mail_cc
     * @param string $name
     * @return bool
     */
    public function set_cc($mail_cc, $name = ""){
        if ($this->validate_mail($mail_cc)){
//			$this->mail_cc = !empty($name) ? "=?".$this->charset."?B?".base64_encode(convert_cyr_string($name, "k","w"))."?= <$mail_cc>" : $mail_cc;
            $this->mail_cc = !empty($name) ? "=?".$this->charset."?B?".base64_encode($name)."?= <$mail_cc>" : $mail_cc;
            return true;
        }
        return false;
    }

    /**
     * Метод, устанавливающий получателя скрытой копии письма
     * bool set_bcc(string mail_bcc, [string name])
     *
     * @param $mail_bcc
     * @param string $name
     * @return bool
     */
    public function set_bcc($mail_bcc, $name = ""){
        if ($this->validate_mail($mail_bcc)){
            $this->mail_bcc = !empty($name) ? "=?".$this->charset."?B?".base64_encode($name)."?= <$mail_bcc>" : $mail_bcc;
            return true;
        }
        return false;
    }

    /**
     * Метод для указания адреса получателя письма
     * bool add_to(string mail_to, [string name])
     *
     * @param $mail_to
     * @param string $name
     * @return bool
     */
    public function add_to($mail_to, $name = ""){
        if ($this->validate_mail($mail_to)){
            $mail_to = !empty($name) ? "=?".$this->charset_k."?B?".base64_encode($name)."?= <$mail_to>" : $mail_to;
            $this->mail_to = !empty($this->mail_to) ? $this->mail_to .", ".$mail_to : $mail_to;
            return true;
        }
        return false;
    }

    /**
     * Метод реализующий возможность добавления адреса для копии письма
     * bool add_cc(string mail_cc, [string name])
     *
     * @param $mail_cc
     * @param string $name
     * @return bool
     */
    public function add_cc($mail_cc, $name = ""){
        if ($this->validate_mail($mail_cc)){
//            $mail_cc = !empty($name) ? "=?".$this->charset."?B?".base64_encode(convert_cyr_string($name, "k","w"))."?= <$mail_cc>" : $mail_cc;
            $mail_cc = !empty($name) ? "=?".$this->charset."?B?".base64_encode($name)."?= <$mail_cc>" : $mail_cc;
            $this->mail_cc = !empty($this->mail_cc) ? $this->mail_cc .", ".$mail_cc : $mail_cc;
            return true;
        }
        return false;
    }

    /**
     * Метод реализующий возможность добавления адреса скрытой копии для письма
     * bool add_bcc(string mail_bcc, [string name])
     *
     * @param $mail_bcc
     * @param string $name
     * @return bool
     */
    public function add_bcc($mail_bcc, $name = ""){
        if ($this->validate_mail($mail_bcc)){
            $mail_bcc = !empty($name) ? "=?".$this->charset_k."?B?".base64_encode($name)."?= <$mail_bcc>" : $mail_bcc;
            $this->mail_bcc = !empty($this->mail_bcc) ? $this->mail_bcc .", ".$mail_bcc : $mail_bcc;
            return true;
        }
        return false;
    }

    /**
     * Метод формирующий адрес для ответа на письмо
     * bool set_reply_to(string mail_reply_to, [string name])
     *
     * @param $mail_reply_to
     * @param string $name
     * @return bool
     */
    public function set_reply_to($mail_reply_to, $name = ""){
        if ($this->validate_mail($mail_reply_to)){
            $this->mail_reply_to = !empty($name) ? "$name <$mail_reply_to>" : $mail_reply_to;
            return true;
        }
        return false;
    }

    /**
     * Метод формирующий return path (обратный адрес) письма
     * bool set_return_path(string mail_return_path)
     *
     * @param $mail_return_path
     * @return bool
     */
    public function set_return_path($mail_return_path){
        if ($this->validate_mail($mail_return_path)){
            $this->mail_return_path = $mail_return_path;
            return true;
        }
        return false;
    }

    /**
     * Метод формирующий тему письма
     * void set_subject(string subject)
     *
     * @param $subject
     */
    public function set_subject($subject){
        $this->mail_subject = !empty($subject) ? "=?".$this->charset."?B?".base64_encode($subject)."?=" : $this->mail_subject;
    }

    /**
     * Метод устанавливающий тип сообщения письма в Plain text формат
     * void set_text(string text)
     *
     * @param $text
     */
    public function set_text($text){
        if (!empty($text)){
            $this->mail_text = $text;
        }
    }

    /**
     * Метод устанавливающий тип сообщения письма в HTML формат
     * void set_html(string html)
     *
     * @param $html
     */
    public function set_html($html){
        if (!empty($html)){
            $this->mail_html = $html;
        }
    }

    /**
     * Метод формирующий общий заголовок письма
     * string get_eml()
     *
     * @return bool|string
     */
    public function get_eml() {
        if ($this->build_body()){
            return
                'To: ' . $this->mail_to . BR .
                'Subject: ' . $this->mail_subject . BR .
                $this->mail_header . BR . BR .
                $this->mail_body;
        }
        return false;
    }

    /**
     * Метод создает объект нового письма
     * void new_mail([mixed from], [mixed to], [string subject], [string text], [string html])
     *
     * @param string $from
     * @param string $to
     * @param string $subject
     * @param string $text
     * @param string $html
     */
    public function new_mail($from = "", $to = "", $subject = "", $text = "", $html = ""){
        $this->mail_subject =
        $this->mail_from =
        $this->mail_to =
        $this->mail_cc =
        $this->mail_bcc =
        $this->mail_text =
        $this->mail_html =
        $this->mail_header =
        $this->mail_body =
        $this->mail_reply_to =
        $this->mail_return_path = "";
        $this->attachments_index =
        $this->sended_index = 0;

        $this->attachments =
        $this->attachments_img = array();

        if (is_array($from)){
            $this->set_from($from[0],$from[1]);
            $this->set_return_path($from[0]);
        }
        else {
            $this->set_from($from);
            $this->set_return_path($from);
        }

        if (is_array($to)){
            $this->set_to($to[0],$to[1]); // Email, Name
        } else {
            $this->set_to($to);
        }

        $this->set_subject($subject);
        $this->set_html($html);
        $this->set_text($text);
    }

    /**
     * Метод реализует добавление в письмо вложений
     * void add_attachment(mixed file, string name, [string type])
     *
     * @param $file
     * @param $name
     * @param string $type
     */
    public function add_attachment($file, $name, $type = ""){
        if (($content = $this->open_file($file))){
            $this->attachments[$this->attachments_index] = array(
                'content' => chunk_split(base64_encode($content), 76, BR),
                'name' => $name,
                'type' => (empty($type) ? $this->get_mimetype($name): $type),
                'embedded' => false
            );
            $this->attachments_index++;
        }
    }

    /**
     * Метод отсылки письма
     * bool send()
     *
     * @return bool
     */
    function send($write_to_file = false){
        if ($this->sended_index == 0 && !$this->build_body()) {
            $this->debug(1);
            return false;
        }

        if ((isset($_SERVER['_IS_DEVELOPER_MODE']) && $_SERVER['_IS_DEVELOPER_MODE'] == 1) || $write_to_file) {
            trace_log($this->mail_body, 'email-to-'.$this->mail_to.'.txt');
            //return true;
        }

        return mail($this->mail_to, $this->mail_subject, $this->mail_body, $this->mail_header, '-finfo@rabota-ya.ru');
    }

    /**
     * Метод формирования тела отсылаемого письма
     * bool build_body()
     *
     * @return bool
     */
    function build_body(){
        switch ($this->parse_elements()) {
            case 1:
                $this->build_header("Content-Type: text/plain; charset=\"$this->charset\"".BR );
                $this->mail_body = $this->mail_text;
                break;
            case 3:
                $this->build_header("Content-Type: multipart/alternative;boundary=\"".$this->boundary_alt."\"".BR);
                $this->mail_body = "Multipart Message coming up".BR.BR;
                $this->mail_body .= "--".$this->boundary_alt.BR;
                $this->mail_body .= "Content-Type: text/plain; charset=\"$this->charset\"".BR;
                $this->mail_body .= "Content-Transfer-Encoding: 7bit".BR.BR;
                $this->mail_body .= $this->mail_text.BR.BR;
                $this->mail_body .= "--".$this->boundary_alt.BR;
                $this->mail_body .= "Content-Type: text/html; charset=\"$this->charset\"".BR;
                $this->mail_body .= "Content-Transfer-Encoding: 7bit".BR.BR;
                $this->mail_body .= $this->mail_html.BR.BR;
                $this->mail_body .= "--".$this->boundary_alt."--";
                break;
            case 5:
                $this->build_header("Content-Type: multipart/mixed; boundary=\"$this->boundary_mix\"".BR);
                $this->mail_body .= "--".$this->boundary_mix . BR;
                $this->mail_body .= "Content-Type: text/plain".BR;
                $this->mail_body .= "Content-Transfer-Encoding: 7bit".BR . BR;
                $this->mail_body .= $this->mail_text . BR . BR;
                foreach($this->attachments as $value){
                    $this->mail_body .= "--".$this->boundary_mix . BR;
                    $this->mail_body .= "Content-Type: ".$value['type'] ."; name=\"".$value['name'] ."\"".BR;
                    $this->mail_body .= "Content-Disposition: attachment; filename=\"".$value['name'] ."\"".BR;
                    $this->mail_body .= "Content-Transfer-Encoding: base64".BR . BR;
                    $this->mail_body .= $value['content'] . BR . BR;
                }
                $this->mail_body .= "--".$this->boundary_mix ."--".BR;
                break;
            case 7:
                $this->build_header("Content-Type: multipart/mixed; boundary=\"$this->boundary_mix\"");
                $this->mail_body .= "--".$this->boundary_mix . BR;
                $this->mail_body .= "Content-Type: multipart/alternative; boundary=\"$this->boundary_alt\"".BR . BR;
                $this->mail_body .= "--".$this->boundary_alt . BR;
                $this->mail_body .= "Content-Type: text/plain".BR;
                $this->mail_body .= "Content-Transfer-Encoding: 7bit".BR . BR;
                $this->mail_body .= $this->mail_text . BR . BR;
                $this->mail_body .= "--".$this->boundary_alt . BR;
                $this->mail_body .= "Content-Type: text/html; charset=\"$this->charset\"".BR;
                $this->mail_body .= "Content-Transfer-Encoding: 7bit".BR . BR;
                $this->mail_body .= $this->mail_html . BR . BR;
                $this->mail_body .= "--".$this->boundary_alt ."--".BR . BR;
                foreach($this->attachments as $value){
                    $this->mail_body .= "--".$this->boundary_mix . BR;
                    $this->mail_body .= "Content-Type: ".$value['type'] ."; name=\"".$value['name'] ."\"".BR;
                    $this->mail_body .= "Content-Disposition: attachment; filename=\"".$value['name'] ."\"".BR;
                    $this->mail_body .= "Content-Transfer-Encoding: base64".BR . BR;
                    $this->mail_body .= $value['content'] . BR . BR;
                }
                $this->mail_body .= "--".$this->boundary_mix ."--".BR;
                break;
            case 11:
                $this->build_header("Content-Type: multipart/related; type=\"multipart/alternative\"; boundary=\"$this->boundary_rel\"");
                $this->mail_body .= "--".$this->boundary_rel . BR;
                $this->mail_body .= "Content-Type: multipart/alternative; boundary=\"$this->boundary_alt\"".BR . BR;
                $this->mail_body .= "--".$this->boundary_alt . BR;
                $this->mail_body .= "Content-Type: text/plain".BR;
                $this->mail_body .= "Content-Transfer-Encoding: 7bit".BR . BR;
                $this->mail_body .= $this->mail_text . BR . BR;
                $this->mail_body .= "--".$this->boundary_alt . BR;
                $this->mail_body .= "Content-Type: text/html; charset=\"$this->charset\"".BR;
                $this->mail_body .= "Content-Transfer-Encoding: 7bit".BR . BR;
                $this->mail_body .= $this->mail_html . BR . BR;
                $this->mail_body .= "--".$this->boundary_alt ."--".BR . BR;
                foreach($this->attachments as $value){
                    if ($value['embedded']){
                        $this->mail_body .= "--".$this->boundary_rel . BR;
                        $this->mail_body .= "Content-ID: <".$value['embedded'] .">".BR;
                        $this->mail_body .= "Content-Type: ".$value['type'] ."; name=\"".$value['name'] ."\"".BR;
                        $this->mail_body .= "Content-Disposition: attachment; filename=\"".$value['name'] ."\"".BR;
                        $this->mail_body .= "Content-Transfer-Encoding: base64".BR . BR;
                        $this->mail_body .= $value['content'] . BR . BR;
                    }
                }
                $this->mail_body .= "--".$this->boundary_rel ."--".BR;
                break;
            case 15:
                $this->build_header("Content-Type: multipart/mixed; boundary=\"$this->boundary_mix\"");
                $this->mail_body .= "--".$this->boundary_mix . BR;
                $this->mail_body .= "Content-Type: multipart/related; type=\"multipart/alternative\"; boundary=\"$this->boundary_rel\"".BR . BR;
                $this->mail_body .= "--".$this->boundary_rel . BR;
                $this->mail_body .= "Content-Type: multipart/alternative; boundary=\"$this->boundary_alt\"".BR . BR;
                $this->mail_body .= "--".$this->boundary_alt . BR;
                $this->mail_body .= "Content-Type: text/plain".BR;
                $this->mail_body .= "Content-Transfer-Encoding: 7bit".BR . BR;
                $this->mail_body .= $this->mail_text . BR . BR;
                $this->mail_body .= "--".$this->boundary_alt . BR;
                $this->mail_body .= "Content-Type: text/html; charset=\"$this->charset\"".BR;
                $this->mail_body .= "Content-Transfer-Encoding: 7bit".BR . BR;
                $this->mail_body .= $this->mail_html . BR . BR;
                $this->mail_body .= "--".$this->boundary_alt ."--".BR . BR;
                foreach($this->attachments as $value){
                    if ($value['embedded']){
                        $this->mail_body .= "--".$this->boundary_rel . BR;
                        $this->mail_body .= "Content-ID: <".$value['embedded'] .">".BR;
                        $this->mail_body .= "Content-Type: ".$value['type'] ."; name=\"".$value['name'] ."\"".BR;
                        $this->mail_body .= "Content-Disposition: attachment; filename=\"".$value['name'] ."\"".BR;
                        $this->mail_body .= "Content-Transfer-Encoding: base64".BR . BR;
                        $this->mail_body .= $value['content'] . BR . BR;
                    }
                }
                $this->mail_body .= "--".$this->boundary_rel ."--".BR . BR;
                foreach($this->attachments as $value){
                    if (!$value['embedded']){
                        $this->mail_body .= "--".$this->boundary_mix . BR;
                        $this->mail_body .= "Content-Type: ".$value['type'] ."; name=\"".$value['name'] ."\"".BR;
                        $this->mail_body .= "Content-Disposition: attachment; filename=\"".$value['name'] ."\"".BR;
                        $this->mail_body .= "Content-Transfer-Encoding: base64".BR . BR;
                        $this->mail_body .= $value['content'] . BR . BR;
                    }
                }
                $this->mail_body .= "--".$this->boundary_mix ."--".BR;
                break;
            default:
                $this->debug(2);
                return false;
        }
        $this->sended_index++;
        return true;
    }

    /**
     * Метод поcтроения заголовка отсылаемого письма
     * void build_header()
     *
     * @param $content_type
     */
    function build_header($content_type){
        if (!empty($this->mail_from)){
            $this->mail_header .= "From: ".$this->mail_from . BR;
            $this->mail_header .= !empty($this->mail_reply_to) ? "Reply-To: ".$this->mail_reply_to . BR : "Reply-To: ".$this->mail_from . BR;
        }
        if (!empty($this->mail_cc)){
            $this->mail_header .= "Cc: ".$this->mail_cc . BR;
        }
        if (!empty($this->mail_bcc)){
            $this->mail_header .= "Bcc: ".$this->mail_bcc . BR;
        }
        if (!empty($this->mail_return_path)){
            $this->mail_header .= "Return-Path: ".$this->mail_return_path . BR;
        }
        $this->mail_header .= "MIME-Version: 1.0". BR;
        if (!empty($this->x_mailer)) {
            $this->mail_header .= "X-Mailer: ".$this->x_mailer. BR;
        } else {
            $this->mail_header .= "X-Mailer: EMail System [phpWebStudio <info@phpwebstudio.com>]". BR;
        }
        $this->mail_header .= $content_type;
    }

    /**
     * Метод производит проверку PHP версии
     * bool php_version_check(string vercheck)
     *
     * @param $vercheck
     * @return bool
     */
    function php_version_check($vercheck){
        $minver = str_replace(".","", $vercheck);
        $curver = str_replace(".","", phpversion());
        if($curver >= $minver){
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Метод производит проверку объекта письма для определения его Content-Type.
     * mixed parse_elements()
     *
     * @return bool
     */
    private function parse_elements(){
        if (empty($this->mail_to)){
            $this->debug(3);
            return false;
        }
        $this->mail_type = 0;
        $this->search_images();
        if (!empty($this->mail_text)){
            $this->mail_type = $this->mail_type + 1;
        }
        if (!empty($this->mail_html)){
            $this->mail_type = $this->mail_type + 2;
            if (empty($this->mail_text)){
                $this->mail_text = strip_tags(preg_replace("/<br\s?\/?>/", BR, $this->mail_html));
                $this->mail_type = $this->mail_type + 1;
            }
        }
        if ($this->attachments_index != 0){
            if (count($this->attachments_img) != 0){
                $this->mail_type = $this->mail_type + 8;
            }
            if ((count($this->attachments) - count($this->attachments_img)) >= 1){
                $this->mail_type = $this->mail_type + 4;
            }
        }
        return $this->mail_type;
    }


    /**
     * Метод реализует проверку HTML тела письма, на включенные в него изображения,
     * для последующего включения их в письмо
     * void search_images()
     */
    private function search_images(){
        if ($this->attachments_index != 0){
            foreach($this->attachments as $key => $value){
                if (preg_match('/(css|image)/i', $value['type']) && preg_match('/\s(background|href|src)\s*=\s*[\"|\'](' . $value['name'] .')[\"|\'].*>/is', $this->mail_html)) {
                    $img_id = md5($value['name']) .".jimmy.webstudio@gmail.com";
                    $this->mail_html = preg_replace('/\s(background|href|src)\s*=\s*[\"|\']('. $value['name'] .')[\"|\']/is', ' \\1="cid:'. $img_id .'"', $this->mail_html);
                    $this->attachments[$key]['embedded'] = $img_id;
                    $this->attachments_img[] = $value['name'];
                }
            }
        }
    }

    /**
     * Метод реализует проверку email адреса на его корректность
     * bool validate_mail(string mail)
     *
     * @param $mail
     * @return bool
     */
    protected function validate_mail($mail){
        if (preg_match("/^[a-zA-Z0-9][\w\.-]+@[A-Za-z0-9][\w\-\.]+\.[A-Za-z0-9]{2,6}/", $mail)){
            return true;
        }
        $this->debug(4, $mail);
        return false;
    }

    /**
     * Метод возвращает MIME тип файла по его расширению
     * string get_mimetype(string name)
     *
     * @param $name
     * @return string
     */
    private function get_mimetype($name) {
        $ext_array = explode(".", $name);
        if (($last = count($ext_array) - 1) != 0){
            $ext = $ext_array[$last];
            if (isset($this->mime_types[$ext]))
                return $this->mime_types[$ext];
        }
        return "application/octet-stream";
    }

    /**
     * Метод возвращает содержимое указанного файла
     * int open_file(string file)
     *
     * @param $file
     * @return bool|string
     */
    protected function open_file($file){
        if(($fp = @fopen($file, 'r'))){
            $content = fread($fp, filesize($file));
            fclose($fp);
            return $content;
        }
        $this->debug(5, $file);
        return false;
    }

    /**
     * Метод, реализующий вывод отладочной информации
     * void debug(string msg, [string element])
     *
     * @param $msg
     * @param string $element
     * @return bool
     */
    public function debug($msg, $element=""){
        if ($this->debug_status == "yes") {
            echo "<br>\n<b>Error:</b> ".$this->error_msg[$msg] ." $element<br>\n";
        }
        elseif ($this->debug_status == "halt") {
            die ("<br>\n<b>Error:</b> ".$this->error_msg[$msg] ." $element<br>\n");
        }
        return false;
    }
}
