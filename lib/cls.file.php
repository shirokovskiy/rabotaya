<?php
/**
 * Class Files
 *
 * Shirokovskiy D.2007 Jimmy™. Sun Dec 02 23:10:56 MSK 2007
 */

class Files {
    public $db;
    public $util;
    public $fp;
    public $dir, $file_name;

    function __construct( $dirPath = null ) {
        if ( !empty($dirPath) && is_dir($dirPath) ) {
            $this->dir = $dirPath;
        } else {
            if ( isset($_SERVER['DOCUMENT_ROOT']) ) {
                $this->dir = $_SERVER['DOCUMENT_ROOT'].(preg_match("|.*\/$|", $this->dir) ? "" : "/");
            } else {
                $this->dir = dirname(__FILE__);
            }
        }

        $this->dir .= (preg_match("|.*\/$|", $this->dir) ? "" : "/");
    }

    /**
     * Записать файл с содержимым
     *
     * @param $body
     * @param null $file
     * @param null $path
     * @param int $binary
     * @return bool
     */
    public function writeFile( $body, $file = null, $path = null, $binary = 0 ) {
        if ( empty($path) ) {
            $path = $this->dir;
        }
        if ( !file_exists( $path ) ) {
            if (!$resMK = mkdir( $path, 0775, true ) ) {
                trace_log('Не могу создать директорию '.$path);
                return false;
            }
        }

        if ( empty($file) ) {
            $file = 'file.'.date('YmdHis').'.txt';
        }

        $this->fp = fopen($path.$file,"w".( $binary == 1 ? "b" : "" ) );
        if ( !$this->fp ) {
            trace_log('Не могу открыть файл '.$path.$file);
            return false;
        }

        $body = preg_replace("/\r\n/", "\n", $body);

        if (!fwrite($this->fp, $body)) {
            trace_log('Не могу записать файл. Длина файла = '.strlen($body));
            return false;
        }
        fclose($this->fp);
        return true;
    }

    /**
     * Чтение из файла
     *
     * @param $file_fullPath
     * @param int $binary
     * @return null|string
     */
    public function readFile ( $file_fullPath, $binary = 0 ) {
        $fp = @fopen( $file_fullPath, "r".( $binary == 1 ? "b" : "" ) );

        if ( false == $fp ) {
            return null;
        }

        if ( filesize( $file_fullPath ) > 0 ) {
            $content = fread( $fp, filesize( $file_fullPath ));
        } else {
            $content = null;
        }
        fclose( $fp );

        return $content;
    }

    /**
     * Установка прав доступа к файлу
     *
     * @param $filePath
     * @param int $chmod
     * @return bool
     */
    public function setChmod($filePath, $chmod=0664 ) {
        if (empty($filePath) || !file_exists($filePath)) {
            return false;
        }
        return @chmod($filePath, $chmod);
    }

    /**
     * Проверка существования закаченного изображения
     *
     * @param $refUploadFile
     * @return bool
     */
    function checkFileUpload($refUploadFile) {
        if(!is_uploaded_file($refUploadFile['tmp_name']))
            return false;
        return true;
    }

    /**
     * Смена рабочей директории
     *
     * @param $path
     */
    function changeCurrentDir($path) {
        $this->dir = $path;
        $this->dir .= (preg_match("|.*\/$|", $this->dir) ? "" : "/");
    }

    /**
     * Сохранение файла
     *
     * @param $arrFILES
     * @param null $toFileName
     * @return bool
     */
    function saveUploadedFile($arrFILES, $toFileName = null) {
        if(!$this->checkFileUpload($arrFILES)) {
            return false;
        }

        if ( move_uploaded_file($arrFILES['tmp_name'], $this->dir.$toFileName) ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $filePath
     * @param null $filaAttachName
     * @param bool $delAfterGetting
     */
    function downloadFile ( $filePath, $filaAttachName = null, $delAfterGetting = false ) {
        Header( "Expires: Mon, 26 Jul 1997 05:00:00 GMT\r\n" );
        Header( "Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT\r\n" );
        Header( "Cache-Control: no-store, no-cache, must-revalidate\r\n" );
        Header( "Cache-Control: post-check=0, pre-check=0, false\r\n" );
        Header( "Pragma: no-cache\r\n" );
        Header( "HTTP/1.1 200 OK\r\n" );

        $filaAttachName = !empty($filaAttachName) ? $filaAttachName : 'file.txt';

        Header( "Content-Disposition: attachment; filename=$filaAttachName\r\n" );
        Header( "Accept-Ranges: bytes\r\n" );
        Header( "Content-Type: application/force-download" );
//  	Header( "Content-Length: $sizeResFile\r\n\r\n" );

        readfile( $filePath );

        if ( $delAfterGetting ) {
            unlink( $filePath );
        }
        die();
    }

    /**
     * Определение расширения файла (из имени) а не по MIME ©2005, Jimmy™
     *
     * @param $fileName
     * @return null
     */
    function getFileExtension( /*string*/ $fileName ) {
        $ext = null;

        if ( true == preg_match( "/\./", $fileName ) ) {
            $fileParts = explode( ".", $fileName );

            $ext = $fileParts[count( $fileParts ) - 1];
        }

        return ( $ext );
    }
}
