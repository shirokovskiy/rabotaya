<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 1/26/14
 * Time         : 12:43 AM
 * Description  :
 */
include_once "../../cfg/web.cfg.php";
include_once "../models/SiteUser.php";

$User = new SiteUser();

$arrResult['status'] = 0;
$arrResult['message'] = '';

trace_log($_REQUEST);

if (!empty($_REQUEST['strRecoveryLogin'])) {
    $strRecoveryLogin = mysql_real_escape_string( trim($_REQUEST['strRecoveryLogin']) );
    $UserId = $User->isEmailExist($strRecoveryLogin);
    if ($User->isValidEmail($strRecoveryLogin) && intval($UserId)) {
        $newPassword = $User->getRandomPassw(7);

        # Логин и Пароль есть, осталось только отправить
        if ( !is_object( $objMail ) ) {
            include_once "../cls.mail.php";
            $objMail = new clsMail();
        }

        if ( !empty( $newPassword ) ) {
            $strSqlQuery = "UPDATE `site_users` SET"
                . " su_passw = md5('$newPassword')"
                . " WHERE su_id=".$UserId
            ;
            if ( !$User->query( $strSqlQuery ) ) {
                # sql error
                $arrResult['message'] = 'Не получилось! Ошибка базы данных.';
            } else {
                $sendTo = $strRecoveryLogin;
                $sendFrom = PROJECT_EMAIL;
                $sendSubject = PROJECT_NAME." | Восстановление пароля";
                $sendMessage = "Уважаемый пользователь сайта ".PROJECT_NAME."!\n";
                $sendMessage .= "Вы, или кто-то другой, запросили восстановить пароль для авторизации на сайте:\n";
                $sendMessage .= "\n";
                $sendMessage .= "Новый пароль: $newPassword\n";
                $sendMessage .= "\n";
                $sendMessage .= "По всем вопросам обращайтесь по телефону или посредствам E-mail указаных на странице ".SITE_URL."/contacts/\n";
                $sendMessage .= "\n\n";
                $sendMessage .= "Не отвечайте на данное письмо! Текст сформирован автоматически почтовым роботом.\n";

                $sendMessage .= "\n\n===============================================================\n\n";

                $sendMessage .= "Dear subscriber of ".PROJECT_NAME."!\n";
                $sendMessage .= "You, or someone else, had requested the recovery of password:\n";
                $sendMessage .= "Login: {$strRecoveryLogin}\n";
                $sendMessage .= "NEW Password: $newPassword\n";
                $sendMessage .= "\n";
                $sendMessage .= "If any questions, don't hesitate to call or Email us by contacts on ".SITE_URL."/contacts/\n";
                $sendMessage .= "\n\n";
                $sendMessage .= "Please, don't answer this Email! This is automatically generated message.\n";

                $sendHTMLMessage = file_get_contents(SITE_TPL_DIR.'emails/password.recovery.html');

                $linkToLogin = SITE_URL.'profile';
                $linkToContacts = SITE_URL.'contacts';
                $sendHTMLMessage = preg_replace("/\{\%LINK_LOGIN\%\}/", $linkToLogin, $sendHTMLMessage);
                $sendHTMLMessage = preg_replace("/\{\%LINK_CONTACTS\%\}/", $linkToContacts, $sendHTMLMessage);

                $sendHTMLMessage = preg_replace("/\{\%login\%\}/", $strRecoveryLogin, $sendHTMLMessage);
                $sendHTMLMessage = preg_replace("/\{\%password\%\}/", $newPassword, $sendHTMLMessage);

                if (isset($_SERVER['_IS_DEVELOPER_MODE']) && $_SERVER['_IS_DEVELOPER_MODE'] == 1) {
                    $sendTo = "jimmy.webstudio@gmail.com"; // DEBUG
                }

                $objMail->new_mail( $sendFrom, $sendTo, $sendSubject, $sendMessage, $sendHTMLMessage );
                if ($sendTo != "jimmy.webstudio@gmail.com") {
                    $objMail->add_bcc( "jimmy.webstudio@gmail.com" );
                }

                $objMail->add_attachment( PRJ_SITEIMG.'content/email-bg.jpg', 'email-bg.jpg', 'image' );

                if ( !$objMail->send() ) {
                    $arrResult['message'] = 'Наш почтовый сервер сейчас отдыхает. Попробуйте через пару минут.';
                } else {
                    $arrResult['status'] = 1;
                    $arrResult['message'] = "<h3>На указанный Вами адрес выслан пароль.</h3>
                            <p>В ближайшее будущее проверьте папку Входящие Вашего почтового ящика.</p>
                            <p>Убедитесь что письмо не попало в Спам.</p>
                            <p><b>Удачной авторизации!</b></p>";
                }
            }
        } else {
            $arrResult['message'] = 'Что-то пошло не так.';
        }
    } else {
        $arrResult['message'] = 'Убедитесь что Вы не ошиблись.';
    }
}


echo json_encode($arrResult);
