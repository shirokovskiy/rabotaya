<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 9/16/13
 * Time         : 1:13 AM
 * Description  : Registration for users
 */
include_once "../../cfg/web.cfg.php";
include_once "../models/SiteUser.php";

$objSiteUser = new SiteUser();

$arrResult['status'] = 0;
$arrResult['message'] = '';

trace_log($_REQUEST, 'api.registration.'.date('Ymd').'.log');

if ($_REQUEST['cbxAgree'] == 'Y') {
    $arr = array();
    foreach ($_REQUEST as $key => $value) {
        if (preg_match("/^str/", $key)) {
            $arr[strtolower(substr($key, 3))] = $value;
        }
    }

    if (!$objSiteUser->setData($arr)->save()) {
        $arrResult['message'] = 'Не могу записать данные!';
        switch ($objSiteUser->getError()) {
            case 'SU001':
                $arrResult['message'] .= "<br>Такой Email уже зарегистрирован на сайте. Попытайтесь <a href='/recovery'>восстановить пароль</a>.";
                break;
            case 'SU002':
                $arrResult['message'] .= "<br>Не верно введён пароль. Убедитесь что пароль одинаково набран в обоих полях.";
                break;
            case 'SU003':
                $arrResult['message'] .= "<br>Ошибка ".$objSiteUser->getError();
                break;
            case 'SU004':
                $arrResult['message'] .= "<br>Не верно введён Email.<br>Используйте только латинские символы.<br>Пример: ivanoff.ivan@email.com";
                break;
            default: break;
        }
    } else {
        $arrResult['status'] = 1;
        $arrResult['message'] = "Поздравляем! Вы успешно зарегистрировались.<br>Теперь, используйте форму авторизации, нажав ссылку &laquo;Войти&raquo; вверху сайта.";
    }
} else {
    $arrResult['message'] .= "<br>Ошибка: Вы не отметили и таким образом не согласились с &laquo;Пользовательским соглашением&raquo;";
}

echo json_encode($arrResult);
