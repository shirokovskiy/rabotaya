<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 4/4/14
 * Time         : 2:43 AM
 * Description  :
 */
require_once '../../../cfg/web.cfg.php';

//trace_log($_REQUEST, 'articlesApi.log');

$sort = '';
if (isset($_REQUEST['sort']) && isset($_REQUEST['dir'])) {
    $sort = ' ORDER BY `'.$_REQUEST['sort'].'` '.$_REQUEST['dir'];
}

$limit = '';
if (isset($_REQUEST['start']) && isset($_REQUEST['limit'])) {
    $start = intval($_REQUEST['start']);
    $limit = intval($_REQUEST['limit']);
    $limit = ' LIMIT '.$start.', '.$limit;
}

$strSqlQuery = "SELECT * FROM `site_articles` WHERE `sa_type` = 'article'".$sort.$limit;
$arrResult['data'] = $objDb->fetchall($strSqlQuery);

$strSqlQuery = "SELECT COUNT(sa_id) iC FROM `site_articles` WHERE `sa_type` = 'article'";
$arrResult['total'] = $objDb->fetch($strSqlQuery, 'iC');

echo json_encode($arrResult);
