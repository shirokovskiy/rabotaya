<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 5/28/14
 * Time         : 3:34 PM
 * Description  :
 */
require_once '../../../cfg/web.cfg.php';

//trace_log($_REQUEST, 'citiesApi.log');

$sort = '';
if (isset($_REQUEST['sort']) && isset($_REQUEST['dir'])) {
    $sort = ' ORDER BY `'.$_REQUEST['sort'].'` '.$_REQUEST['dir'];
}

$limit = '';
if (isset($_REQUEST['start']) && isset($_REQUEST['limit'])) {
    $start = intval($_REQUEST['start']);
    $limit = intval($_REQUEST['limit']);
    $limit = ' LIMIT '.$start.', '.$limit;
}

$where = '';
$arr_where = array();
if (isset($_REQUEST['filter']) && is_array($_REQUEST['filter'])) {
    foreach($_REQUEST['filter'] as $filter) {
        if ($filter['data']['type'] == 'string') {
            if (strlen($filter['data']['value']) >= 2) {
                $arr_where[] = "`".$filter['field']."` LIKE '%".mysql_real_escape_string($filter['data']['value'])."%'";
            }
        } else {
            $arr_where[] = "`".$filter['field']."` = '".mysql_real_escape_string($filter['data']['value'])."'";
        }
    }
}

if (is_array($arr_where) && !empty($arr_where)) {
    $where = ' WHERE '.implode(" AND ", $arr_where);
}

$strSqlQuery = "SELECT * FROM `site_cities`".$where.$sort.$limit;
$arrResult['data'] = $objDb->fetchall($strSqlQuery);

$strSqlQuery = "SELECT COUNT(id) iC FROM `site_cities`".$where;
$arrResult['total'] = $objDb->fetch($strSqlQuery, 'iC');

//trace_log($strSqlQuery, 'citiesApi.log');

echo json_encode($arrResult);
