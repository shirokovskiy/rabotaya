<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 4/6/14
 * Time         : 2:29 AM
 * Description  :
 */
require_once '../../../cfg/web.cfg.php';

//trace_log($_REQUEST, 'searchTextApiDelete.log');
$arrResult['success'] = true;// by default

if (isset($_REQUEST['act'])) {
    switch($_REQUEST['act']) {
        case 'st':
            if (isset($_REQUEST['id']) && $_REQUEST['id']) {
                $strSqlQuery = "DELETE FROM `site_seo_texts` WHERE sst_id = ".intval($_REQUEST['id'])." LIMIT 1";
                if (!$objDb->query($strSqlQuery)) {
                    # sql query error
                    $arrResult['success'] = false;
                    $arrResult['message'] = 'Ошибка базы данных';
                }
            }
            break;

        case 'city':
            if (isset($_REQUEST['id']) && $_REQUEST['id']) {
                $strSqlQuery = "DELETE FROM `site_cities` WHERE `id` = ".intval($_REQUEST['id'])." LIMIT 1";
                if (!$objDb->query($strSqlQuery)) {
                    # sql query error
                    $arrResult['success'] = false;
                    $arrResult['message'] = 'Ошибка базы данных';
                }
            }
            break;
        default:
            break;
    }
} else {
    $arrResult['success'] = false;
    $arrResult['message'] = 'Неизвестная операция';
}

//$arrResult['success'] = false;
//$arrResult['message'] = 'Jimmy';

echo json_encode($arrResult);
