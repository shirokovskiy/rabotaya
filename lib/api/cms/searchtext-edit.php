<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 4/6/14
 * Time         : 1:48 AM
 * Description  :
 */
require_once '../../../cfg/web.cfg.php';

//trace_log($_REQUEST, 'searchTextApiEdit.log');
$arrResult['success'] = true;// by default

if (is_array($_REQUEST) && !empty($_REQUEST)) {
    $arrSqlData['sst_id'] = mysql_real_escape_string(trim($_REQUEST['sst_id']));
    $arrTplVars['sst_id'] = htmlspecialchars(stripslashes(trim($_POST['sst_id'])));

    $arrSqlData['sst_word'] = mysql_real_escape_string(trim($_POST['sst_word']));
    $arrTplVars['sst_word'] = htmlspecialchars(stripslashes(trim($_POST['sst_word'])));

    $arrSqlData['sst_text'] = mysql_real_escape_string(trim($_POST['sst_text']));
    $arrTplVars['sst_text'] = htmlspecialchars(stripslashes(trim($_POST['sst_text'])));

    if (!empty($arrSqlData['sst_word']) &&!empty($arrSqlData['sst_text'])) {
        if (!empty($arrSqlData['sst_id'])) {
            $strSqlQuery = "UPDATE `site_seo_texts` SET"
                . " `sst_word` = '{$arrSqlData['sst_word']}'"
                . ", `sst_text` = '{$arrSqlData['sst_text']}'"
                . " WHERE `sst_id` = ".$arrSqlData['sst_id'];
            if (!$objDb->query($strSqlQuery)) {
                # sql error
                $arrResult['success'] = false;
                $arrResult['message'] = 'Ошибка базы данных';
            }
        } else {
            $strSqlQuery = "INSERT INTO `site_seo_texts` SET"
                . " `sst_word` = '{$arrSqlData['sst_word']}'"
                . ", `sst_text` = '{$arrSqlData['sst_text']}'";
            if (!$objDb->query($strSqlQuery)) {
                # sql error
                $arrResult['success'] = false;
                $arrResult['message'] = 'Ошибка базы данных';
            }
        }
        $arrResult['message'] = 'Данные успешно сохранены';
    } else {
        $arrResult['success'] = false;
        $arrResult['message'] = 'Нет данных для записи';
    }
}

//$arrResult['success'] = false;
//$arrResult['message'] = 'Jimmy';

echo json_encode($arrResult);
