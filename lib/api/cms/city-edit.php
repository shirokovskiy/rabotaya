<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 5/28/14
 * Time         : 3:33 PM
 * Description  :
 */
require_once '../../../cfg/web.cfg.php';
trace_log($_REQUEST, 'cityApiEdit.log');
$arrResult['success'] = true;// by default

if (is_array($_REQUEST) && !empty($_REQUEST)) {
    $arrSqlData['id'] = mysql_real_escape_string(trim($_REQUEST['id']));
    $arrTplVars['id'] = htmlspecialchars(stripslashes(trim($_POST['id'])));

    $arrSqlData['city'] = mysql_real_escape_string(trim($_POST['city']));
    $arrTplVars['city'] = htmlspecialchars(stripslashes(trim($_POST['city'])));

    $arrSqlData['city_eng'] = mysql_real_escape_string(trim($_POST['city_eng']));
    $arrTplVars['city_eng'] = htmlspecialchars(stripslashes(trim($_POST['city_eng'])));

    $arrSqlData['pos'] = mysql_real_escape_string(trim($_POST['pos']));
    $arrTplVars['pos'] = htmlspecialchars(stripslashes(trim($_POST['pos'])));

    if (!empty($arrSqlData['city'])) {
        if (!empty($arrSqlData['id'])) {
            $strSqlQuery = "UPDATE `site_cities` SET"
                . " `city` = '{$arrSqlData['city']}'"
                . ", `city_eng` = '{$arrSqlData['city_eng']}'"
                . ", `pos` = '{$arrSqlData['pos']}'"
                . " WHERE `id` = ".$arrSqlData['id'];
            if (!$objDb->query($strSqlQuery)) {
                # sql error
                $arrResult['success'] = false;
                $arrResult['message'] = 'Ошибка базы данных';
            }
        } else {
            $strSqlQuery = "INSERT INTO `site_cities` SET"
                . " `city` = '{$arrSqlData['city']}'"
                . ", `city_eng` = '{$arrSqlData['city_eng']}'"
                . ", `pos` = '{$arrSqlData['pos']}'";
            if (!$objDb->query($strSqlQuery)) {
                # sql error
                $arrResult['success'] = false;
                $arrResult['message'] = 'Ошибка базы данных';
            }
        }
        $arrResult['message'] = 'Данные успешно сохранены';
    } else {
        $arrResult['success'] = false;
        $arrResult['message'] = 'Нет данных для записи';
    }
}

//$arrResult['success'] = false;
//$arrResult['message'] = 'Jimmy';

echo json_encode($arrResult);
