<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 4/6/14
 * Time         : 1:48 AM
 * Description  :
 */
require_once '../../../cfg/web.cfg.php';

//trace_log($_REQUEST, 'articleApiEdit.log');
$arrResult['success'] = true;// by default

if (is_array($_REQUEST) && !empty($_REQUEST)) {
    $arrSqlData['sa_id'] = mysql_real_escape_string(trim($_REQUEST['sa_id']));
    $arrTplVars['sa_id'] = htmlspecialchars(stripslashes(trim($_POST['sa_id'])));

    $arrSqlData['sa_title'] = mysql_real_escape_string(trim($_POST['sa_title']));
    $arrTplVars['sa_title'] = htmlspecialchars(stripslashes(trim($_POST['sa_title'])));

    $arrSqlData['sa_content'] = mysql_real_escape_string(trim($_POST['sa_content']));
    $arrTplVars['sa_content'] = htmlspecialchars(stripslashes(trim($_POST['sa_content'])));

    if (!empty($arrSqlData['sa_title']) &&!empty($arrSqlData['sa_content'])) {
        if (!empty($arrSqlData['sa_id'])) {
            $strSqlQuery = "UPDATE `site_articles` SET"
                . " `sa_title` = '{$arrSqlData['sa_title']}'"
                . ", `sa_content` = '{$arrSqlData['sa_content']}'"
                . " WHERE `sa_id` = ".$arrSqlData['sa_id'];
            if (!$objDb->query($strSqlQuery)) {
                # sql error
                $arrResult['success'] = false;
                $arrResult['message'] = 'Ошибка базы данных';
            }
        } else {
            $strSqlQuery = "INSERT INTO `site_articles` SET"
                . " `sa_title` = '{$arrSqlData['sa_title']}'"
                . ", `sa_content` = '{$arrSqlData['sa_content']}'";
            if (!$objDb->query($strSqlQuery)) {
                # sql error
                $arrResult['success'] = false;
                $arrResult['message'] = 'Ошибка базы данных';
            }
        }
        $arrResult['message'] = 'Данные успешно сохранены';
    } else {
        $arrResult['success'] = false;
        $arrResult['message'] = 'Нет данных для записи';
    }
}

//$arrResult['success'] = false;
//$arrResult['message'] = 'Jimmy';

echo json_encode($arrResult);
