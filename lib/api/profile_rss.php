<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 3/27/14
 * Time         : 10:37 PM
 * Description  :
 */
include_once "../../cfg/web.cfg.php";
include_once "../models/SiteUser.php";

$User = new SiteUser($_SESSION['uAuthInfo']['uID']);

$arrResult['status'] = 0;
$arrResult['message'] = '';

//trace_log($_REQUEST);
if (!empty($_REQUEST['action']) && $_REQUEST['action'] == 'setRss') {
    if (isset($_SESSION['signInStatus']) && $_SESSION['signInStatus']) {
        if (isset($_SESSION['search']) && is_array($_SESSION['search']) && !empty($_SESSION['search'])) {
            $name = $_REQUEST['strSearchName'];
            $data = serialize($_SESSION['search']);
            if ($User->saveProfileSearchParams($name, $data)) {
                $arrResult['status'] = 1;
                $_SESSION['searchParameterSaved'] = true;
            } else {
                $arrResult['message'] = 'Запись параметров не удалась.';
            }
        } else {
            $arrResult['message'] = 'Параметры поиска не заданы.';
        }
    } else {
        $arrResult['message'] = 'Пользователь не авторизован.';
    }
} else {
    $arrResult['message'] = 'Неизвестное действие.';
}

echo json_encode($arrResult);
