<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 10/8/13
 * Time         : 2:05 PM
 * Description  : Set session city by request
 */
include_once "../../cfg/web.cfg.php";

$arrResult['status'] = 0;
$arrResult['message'] = '';

$arrResult['city'] = 'Санкт-Петербург';
$arrResult['citySubDomain'] = 'www';

//trace_log($_REQUEST);

if (!empty($_REQUEST['city'])) {
    $arrResult['city'] = $_SESSION['myCity'] = $_REQUEST['city'];

    if (!isset($objSiteUser)) {
        $objSiteUser = new SiteUser();
    }
    $citySubDomain = $objSiteUser->getCitySubDomain($_REQUEST['city']);
    $arrResult['citySubDomain'] = $citySubDomain;

    $_SESSION['isCityConfirmed'] = true;
}
echo json_encode($arrResult);
