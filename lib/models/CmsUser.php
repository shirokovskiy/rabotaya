<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 10/27/13
 * Time         : 11:26 PM
 * Description  :
 */

class CmsUser extends Db {
    private $id;

    public function unsetAllModulesAccess($user_id, $prj_id)
    {
        if ($user_id <= 0 || $prj_id <= 0) return false;
        $strSqlQuery = "DELETE FROM `cms_users_access_modules` WHERE cuam_type = 'user' AND cuam_id_owner = ".$user_id." AND cuam_id_project = ".$prj_id;
        if (!$this->query($strSqlQuery)) {
            # sql query error
            return false;
        }
        return true;
    }

    public function setModuleAccess($user_id, $mod_id, $prj_id)
    {
        if ($user_id <= 0 || $mod_id <= 0 || $prj_id <= 0) return false;
        $strSqlQuery = "INSERT INTO `cms_users_access_modules` SET"
            . " cuam_type = 'user'"
            . ", cuam_access = 'Y'"
            . ", cuam_id_owner = ".$user_id
            . ", cuam_id_module = ".$mod_id
            . ", cuam_id_project = ".$prj_id
        ;
        if (!$this->query($strSqlQuery)) {
            # sql error
            return false;
        }
        return true;
    }

    public function hasUserModuleAccess($user_id, $mod_id, $prj_id)
    {
        if ($user_id <= 0 || $mod_id <= 0 || $prj_id <= 0) return false;
        $strSqlQuery = "SELECT * FROM `cms_users_access_modules` WHERE `cuam_id_owner` = ".$user_id." AND `cuam_id_module` = ".$mod_id." AND `cuam_id_project` = ".$prj_id." AND `cuam_type` = 'user'";
        $arr = $this->fetch($strSqlQuery);
        if (is_array($arr) && !empty($arr) && $arr['cuam_id']>0) {
            return true;
        }

        return false;
    }
}
