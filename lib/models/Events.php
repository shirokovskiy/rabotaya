<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 10/28/13
 * Time         : 22:25 PM
 * Description  :
 */

class Events extends Db {
    public function getEventImage($id)
    {
        $strSqlQuery = "SELECT * FROM `site_images` WHERE `si_rel_id` = $id AND `si_type` = 'events' ORDER BY si_id LIMIT 1";
        return $this->fetch($strSqlQuery);
    }

    public function setImageRecord($id, $filename, $title = null) {
        if ($id > 0 && !empty($filename)) {
            $strSqlQuery = "INSERT INTO `site_images` SET"
                ." si_filename = '".mysql_real_escape_string($filename)."'"
                .", si_rel_id = ".$id
                .", si_title = '".mysql_real_escape_string($title)."'"
                .", si_type = 'events'"
                .", si_date_add = NOW()"
                .", si_status = 'Y'";
            if (!$this->query($strSqlQuery)) {
                # sql error
                throw new Exception("Ошибка базы данных: ".__FILE__.':'.__LINE__);
            } else {
                return $this->insert_id();
            }
        }

        return false;
    }

    public function removeImageRecord($id) {
        if ($id > 0) {
            $strSqlQuery = "DELETE FROM `site_images` WHERE si_rel_id = ".$id." AND si_type = 'events'";
            if (!$this->query($strSqlQuery)) {
                # sql query error
                throw new Exception("Ошибка базы данных: ".__FILE__.':'.__LINE__);
            }
        }
    }
}
