<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 8/19/13
 * Time         : 2:28 PM
 * Description  :
 */

class News extends Db {
    public function getNewsImage($id)
    {
        $strSqlQuery = "SELECT * FROM `site_images` WHERE `si_rel_id` = $id AND `si_type` = 'news' ORDER BY si_id";
        return $this->fetchall($strSqlQuery);
    }

    public function setImageRecord($id, $filename, $title = null) {
        if ($id > 0 && !empty($filename)) {
            $strSqlQuery = "INSERT INTO `site_images` SET"
                ." si_filename = '".mysql_real_escape_string($filename)."'"
                .", si_rel_id = ".$id
                .", si_title = '".mysql_real_escape_string($title)."'"
                .", si_type = 'news'"
                .", si_date_add = NOW()"
                .", si_status = 'Y'";
            if (!$this->query($strSqlQuery)) {
                # sql error
                throw new Exception("Ошибка базы данных: ".__FILE__.':'.__LINE__);
            } else {
                return $this->insert_id();
            }
        }

        return false;
    }

    public function removeImageRecord($id) {
        if ($id > 0) {
            $strSqlQuery = "SELECT * FROM `site_images` WHERE `si_id` = ".$id." AND `si_type` = 'news'";
            $arrImage = $this->fetch($strSqlQuery);
            if (is_array($arrImage) && !empty($arrImage)) {
                $strSqlQuery = "DELETE FROM `site_images` WHERE `si_id` = ".$id." AND `si_type` = 'news'";
                if (!$this->query($strSqlQuery)) {
                    # sql query error
                    throw new Exception("Ошибка базы данных: ".__FILE__.':'.__LINE__);
                } else {
                    $image = PRJ_IMAGES."news/".$arrImage['si_rel_id']."/".$arrImage['si_filename'];
                    if ( file_exists($image)) {
                        unlink($image);
                    }
                    $smallImage = str_replace('.b.','.',$arrImage['si_filename']);
                    $image = PRJ_IMAGES."news/".$arrImage['si_rel_id']."/".$smallImage;
                    if ( file_exists($image)) {
                        unlink($image);
                    }
                }

                return $arrImage['si_rel_id'];
            }
        }

        return false;
    }
}
