<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 8/14/13
 * Time         : 1:27 AM
 * Description  :
 */

class Vacancy extends Db {
    protected $id;
    protected $table = 'job_vacancies';

    public function setId($id)
    {
        $this->id = intval($id);
        return $this;
    }

    public function setCompanyId($id)
    {
        if (intval($id) > 0 && $this->getId()) {
            $strSqlQuery = "UPDATE `job_vacancies` SET"
                ." `jv_jc_id` = ".$id
                ." WHERE `jv_id` = ". $this->getId();
            if (!$this->query($strSqlQuery)) {
                # sql error
            }
        }

        return $this;
    }

    public function setIsInvited($id)
    {
        $strSqlQuery = "UPDATE `".$this->table."` SET"
            . " `jv_invited` = `jv_invited` + 1"
            . " WHERE `jv_id` = ".intval($id);
        if (!$this->query($strSqlQuery)) {
            # sql error
            return false;
        }

        return true;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getData($id)
    {
        if ($id <= 0) return null;
        $strSqlQuery = "SELECT * FROM `".$this->table."` WHERE `jv_id` = ".intval($id);
        return $this->fetch($strSqlQuery);
    }

    /**
     * Get total number of available vacancies
     * @return array|null|string
     */
    public function getTotalCount()
    {
        $strSqlQuery = "SELECT COUNT(*) FROM `job_vacancies` WHERE `jv_status` = 'Y'";
        return $this->fetch($strSqlQuery, 0);
    }

    /**
     * Get amount of vacancies for given company
     * @param $id
     * @return array|bool|null|string
     */
    public function getCountByCompany($id)
    {
        if ($id > 0) {
            $strSqlQuery = "SELECT COUNT(*) AS iC FROM `job_vacancies` WHERE `jv_jc_id` = ".$id;
            return $this->fetch($strSqlQuery, 'iC');
        }

        return false;
    }

    public function getRecordDataByHash($hash)
    {
        if (empty($hash) || strlen($hash) != 32) {
            return false;
        }

        $strSqlQuery = "SELECT * FROM `". $this->table."` LEFT JOIN `site_users` ON (jv_email = su_login AND su_type = 'jur') WHERE `jv_invite_hash` LIKE '".$hash."' AND `jv_invited` IS NOT NULL LIMIT 1";
        $arr = $this->fetch($strSqlQuery);
        if (is_array($arr) && !empty($arr)) {
            # значит хеш от резюме и от физ.лица

            return $arr;
        }

        # by default
        return false;
    }

    /**
     * Increment count of views for given vacancy by id
     * @param $id
     * @return bool
     */
    public function incViews($id)
    {
        $strSqlQuery = "UPDATE `job_vacancies` SET"
            . " `jv_views` = `jv_views`+1"
            . " WHERE `jv_id` = ".intval($id);
        if (!$this->query($strSqlQuery)) {
            # sql error
            return false;
        }

        return true;
    }

    public function isExistsByEmail($email)
    {
        $strSqlQuery = "SELECT jv_id FROM `".$this->table."` WHERE `jv_email` LIKE '". mysql_real_escape_string($email)."' ORDER BY jv_id LIMIT 1";
        return $this->fetch($strSqlQuery, 'jv_id');
    }

    public function getList($where = '', $order = '', $limit = '')
    {
        if (is_array($where)) {
            $where = ' AND '.implode(' AND ', $where);
        }

        if (is_array($order)) {
            $order = implode(',',$order);
        }

        if (!empty($order)) {
            $order = ' ORDER BY '.$order;
        }

        if (!empty($limit)) {
            $limit = ' LIMIT '.intval($limit);
        }

        $strSqlQuery = "SELECT * FROM `job_vacancies` WHERE `jv_status` = 'Y'".$where.$order.$limit;

//        echo $strSqlQuery;
//        echo '<br />';
//        die(__FILE__ . ':' . __LINE__);

        return $this->fetchall($strSqlQuery);
    }

    public function getVacancy($id)
    {
        $strSqlQuery = "SELECT * FROM `job_vacancies` WHERE `jv_id` = ".$id;
        return $this->fetch($strSqlQuery);
    }

    public function getSalary($id){
        $value = $this->getVacancy($id);
        $salary = null;
        if (is_array($value) && !empty($value)) {
            $salary = (!empty($value['jv_salary_from']) ? $value['jv_salary_from'] : '').(!empty($value['jv_salary_to']) ? (!empty($value['jv_salary_from'])?'-':'').$value['jv_salary_to'] : '');
            if (!empty($salary)) {

            } elseif (!empty($value['jv_salary'])) {
//                $salary = preg_replace("/[\w|\s]+/i", "", $value['jv_salary']);
                $salary = $value['jv_salary'];
            }
        }

        return $salary;
    }

    public function isValidLink2Profile($hash)
    {
        if (empty($hash) || strlen($hash) != 32) {
            return false;
        }

        $strSqlQuery = "SELECT * FROM `". $this->table."` WHERE `jv_invite_hash` LIKE '".$hash."' LIMIT 1";
        $arr = $this->fetch($strSqlQuery);
        if (is_array($arr) && !empty($arr)) {
            # значит хеш от вакансии и от юр.лица

            return true;
        }

        # by default
        return false;
    }

    /**
     * Check if vacancy exists by given user ID
     * @param int $vid
     * @param int $uid
     * @return bool
     */
    public function checkVacancyByUser($vid, $uid)
    {
        $strSqlQuery = "SELECT `jv_id` FROM `job_vacancies` WHERE `jv_id` = ".$vid." AND `jv_su_id` = ".$uid;
        $arrVacancy = $this->fetch($strSqlQuery);
        if (is_array($arrVacancy) && !empty($arrVacancy)) {
            return true;
        }

        return false;
    }


    /**
     * Remove vacancy from database
     * @param int $vacancyId
     * @return bool
     */
    public function delete($vacancyId)
    {
        $strSqlQuery = "DELETE FROM `job_vacancies` WHERE jv_id = ".intval($vacancyId);
        if (!$this->query($strSqlQuery)) {
            # sql query error
            return false;
        } else {
            // TODO: remove all depended files
        }

        return true;
    }

    /**
     * Set status of vacancy by given ID
     * @param int $id
     * @param string $status
     * @return bool
     */
    public function setStatus($id, $status)
    {
        $strSqlQuery = "UPDATE `job_vacancies` SET"
            . " `jv_status` = '$status'"
            . " WHERE `jv_id` = ".intval($id);
        if (!$this->query($strSqlQuery)) {
            # sql error
            return false;
        }

        return true;
    }

    /**
     * Set id of user for vacancy by given ID
     * @param int $id
     * @return bool
     */
    public function setUserId($id)
    {
        if (intval($id) > 0 && $this->getId()) {
            $strSqlQuery = "UPDATE `job_vacancies` SET"
                ." `jv_su_id` = ".$id
                ." WHERE `jv_id` = ". $this->getId();
            if (!$this->query($strSqlQuery)) {
                # sql error
                return false;
            }
        }

        return $this;
    }

    public function saveSeoUrl($url)
    {
        if ($this->getId() <= 0) return false;

        $strSqlQuery = "SELECT jv_id, jv_seo_url FROM `job_vacancies` WHERE `jv_id` = " . $this->getId();
        $arrVac = $this->fetch($strSqlQuery);

        if (is_array($arrVac) && !empty($arrVac)) {
            if (empty($arrVac['jv_seo_url'])) {
                $strSqlQuery = "UPDATE `job_vacancies` SET"
                    . " `jv_seo_url` = '$url'"
                    . " WHERE `jv_id` = ".$this->getId();
                if (!$this->query($strSqlQuery)) {
                    # sql error
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Отметим пользователя как Приглашён
     *
     * @param $id
     * @param $hash
     * @return bool
     */
    public function saveInviteLinkHash($id, $hash)
    {
        if (empty($hash) || $id <= 0) return false;
        $strSqlQuery = "UPDATE `" . $this->table . "` SET"
            . " `jv_invite_hash` = '$hash'"
            . " WHERE `jv_id` = ".$id;
        if (!$this->query($strSqlQuery)) {
            # sql error
            return false;
        }

        return true;
    }
}
