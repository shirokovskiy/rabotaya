<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 4/21/14
 * Time         : 4:34 PM
 * Description  : 
 */

class Address extends Db {
    public function saveCity($name, $country = null)
    {
        if (empty($name)) return false;
        $cityId = $this->getCityId($name);
        if ($cityId > 0) {
            return $cityId;
        } else {
            $countryId = (int) $this->saveCountry($country);
            $strSqlQuery = "INSERT INTO `site_cities` SET"
                ." city = '".mysql_real_escape_string($name)."'"
                .", city_eng = '".mysql_real_escape_string($name)."'"
                .($countryId>0 ? ", country_id = ".$countryId : '');
            if ($this->query($strSqlQuery)) {
                return $this->insert_id();
            }
        }

        return false;
    }

    public function getCityId($city)
    {
        $strSqlQuery = "SELECT `id` FROM `site_cities` WHERE `city` LIKE '".$city."' OR `city_eng` LIKE '".$city."' LIMIT 1";
        return (int)$this->fetch($strSqlQuery, 'id');
    }

    public function saveCountry($name)
    {
        $countryId = $this->getCountryId($name);
        if ($countryId <= 0) {
            $countryId = $this->saveCountry($name);
        }

        return $countryId;
    }

    public function getCountryId($country)
    {
        $strSqlQuery = "SELECT sc_id FROM `site_countries` WHERE `sc_title` LIKE '".$country."'";
        return $this->fetch($strSqlQuery, 'sc_id');
    }
} 