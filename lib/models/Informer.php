<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 1/3/14
 * Time         : 5:16 PM
 * Description  :
 */

class Informer extends Db {
    public function isInfoByDate($type, $date = 'CURDATE()')
    {
        $strSqlQuery = "SELECT * FROM `site_informers` WHERE `si_date` = " . (preg_match('/\(/', $date) ? $date : "'$date'" ) . " AND `si_type` = '$type'";
        return $this->fetch($strSqlQuery);
    }

    public function saveInfo($type, $value, $date = 'CURDATE()')
    {
        $strSqlQuery = "INSERT INTO `site_informers` SET"
            . " `si_date` = ".(preg_match('/\(/', $date) ? $date : "'$date'" )
            . ", `si_type` = '$type'"
            . ", `si_value` = '$value'";
        if (!$this->query($strSqlQuery,false)) {
            throw new Exception("Ошибка базы данных: ");
        }
    }

    public function updateInfo($type, $value, $date = 'CURDATE()')
    {
        $strSqlQuery = "REPLACE INTO `site_informers` SET"
            . " `si_date` = ".(preg_match('/\(/', $date) ? $date : "'$date'" )
            . ", `si_type` = '$type'"
            . ", `si_value` = '$value'";
        if (!$this->query($strSqlQuery,false)) {
            throw new Exception("Ошибка базы данных: ");
        }
    }

    /**
     * Get informer data
     * @param $type
     * @param string $date
     * @return array|null|string
     */
    public function getInformer($type, $date = 'CURDATE()')
    {
        $strSqlQuery = "SELECT `si_value`, `si_date` FROM `site_informers` WHERE `si_date` <= " . (preg_match('/\(/', $date) ? $date : "'$date'" ) . " AND `si_type` = '$type' ORDER BY `si_date` DESC LIMIT 1";
        return $this->fetch($strSqlQuery);
    }
}
