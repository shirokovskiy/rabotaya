<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 8/12/13
 * Time         : 11:54 PM
 * Description  :
 */

class JobEduType extends Db {
    public function getStatus($id)
    {
        $strSqlQuery = "SELECT jet_title FROM `job_education_types` WHERE `jet_id` = ".$id;
        return $this->fetch($strSqlQuery, 0);
    }


    public function getIdByResume($id)
    {
        if(intval($id) <= 0) return null;
        $strSqlQuery = "SELECT MAX(jre_jet_id) AS jet_id FROM `job_resumes_education` WHERE `jre_jr_id` = ".$id;
        return $this->fetch($strSqlQuery, 'jet_id');
    }
}
