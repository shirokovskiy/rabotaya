<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 8/11/13
 * Time         : 10:53 PM
 * Description  :
 */
include_once "cls.db.php";

class JobCategory extends Db {

    public function getCategory($id) {
        if (empty($id)) return false;
        $strSqlQuery = "SELECT * FROM `job_categories` WHERE `jc_id` = ".$id;
        return $this->fetch($strSqlQuery);
    }

    public function getCategoryFieldById($id, $field = 'jc_title') {
        $arr = $this->getCategory($id);
        return (isset($arr[$field]) ? $arr[$field] : false );
    }

    public function getCategories()
    {
        $strSqlQuery = "SELECT * FROM `job_categories` WHERE `jc_parent` IS NULL ORDER BY `jc_order`";
        return $this->fetchall($strSqlQuery);
    }

    public function getSubCategories($id)
    {
        if (empty($id)) return false;
        $strSqlQuery = "SELECT * FROM `job_categories` WHERE `jc_parent` = $id ORDER BY `jc_order`";
        return $this->fetchall($strSqlQuery);
    }

    public function getCategoryByVacancy($id)
    {
        if (empty($id)) return false;
        $strSqlQuery = "SELECT jc_title FROM `job_category_link` LEFT JOIN `job_categories` ON (jc_id = jcl_sjc_id) WHERE `jcl_rel_id` = $id AND jc_id > 0 LIMIT 1";
        return $this->fetch($strSqlQuery, 'jc_title');
    }

    public function getCategoryByName($name, $only_parents = false)
    {
        if (empty($name)) return false;
        $strSqlQuery = "SELECT jc_id FROM `job_categories` WHERE `jc_title` LIKE '".$name."'".($only_parents ? ' AND `jc_parent` IS NULL' : '');
        return $this->fetch($strSqlQuery, 'jc_id');
    }

    public function save($cat_name, $parent_id = null)
    {
        if (empty($cat_name)) return false;

        $catId = $this->getCategoryByName($cat_name);
        if ($catId > 0) {
            return (int) $catId;
        }

        $strSqlQuery = "INSERT INTO `job_categories` SET"
            . " `jc_title` = '$cat_name'"
            . ", `jc_parent` = ".($parent_id > 0 ? $parent_id : 'NULL')
            . ", `jc_order` = 0";
        if (!$this->query($strSqlQuery)) {
            # sql error
            return false;
        }

        return (int) $this->insert_id();
    }

    public function getCategoryLinksNumber($category_id, $type = '')
    {
        if (!empty($type)) $type = mysql_real_escape_string($type);
        $strSqlQuery = "SELECT COUNT(`jcl_sjc_id`) iC FROM `job_category_link` WHERE `jcl_sjc_id` = ".$category_id." AND `jcl_type` = '$type'";
        return $this->fetch($strSqlQuery, 'iC');
    }

    public function saveDependancy($catID, $recID, $type)
    {
        if ($catID <= 0 || $recID <= 0 || !in_array($type, array('vacancy', 'resume'))) {
            return false;
        }

        // Добавление сферы деятельности
        $strSqlQuery = "REPLACE INTO `job_category_link` SET"
            ." jcl_rel_id = ".$recID
            .", jcl_sjc_id = ".$catID
            .", jcl_type = '$type'"
        ;
        if (!$this->query($strSqlQuery)) {
            # sql error
            return false;
        }

        return true;
    }
}
