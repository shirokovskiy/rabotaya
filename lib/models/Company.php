<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 8/16/13
 * Time         : 1:14 AM
 * Description  :
 */

class Company extends Db {
    public function getCompaniesByName($name)
    {
        if (strlen($name) > 1) {
            $strSqlQuery = "SELECT *, `jc_title` as `label`, `jc_title` as `value` FROM `job_companies` WHERE `jc_title` LIKE '%".$name ."%' ORDER BY `jc_id`";
            return $this->fetchall($strSqlQuery);
        }
        return false;
    }

    public function getCompanyByName($name)
    {
        if (strlen($name) > 1) {
            $strSqlQuery = "SELECT *, `jc_title` as `label`, `jc_title` as `value` FROM `job_companies` WHERE `jc_title` LIKE '".$name ."' ORDER BY `jc_id` LIMIT 1";
            return $this->fetch($strSqlQuery);
        }
        return false;
    }

    public function getLogoImage($id)
    {
        $strSqlQuery = "SELECT * FROM `site_images` WHERE `si_type` = 'company' AND `si_rel_id` = ".$id;
        return $this->fetch($strSqlQuery);
    }

    public function setImageRecord($id, $filename, $title = null)
    {
        if ($id > 0 && !empty($filename)) {
            $strSqlQuery = "INSERT INTO `site_images` SET"
                ." si_filename = '".mysql_real_escape_string($filename)."'"
                .", si_rel_id = ".$id
                .", si_title = '".mysql_real_escape_string($title)."'"
                .", si_type = 'company'"
                .", si_date_add = NOW()"
                .", si_status = 'Y'";
            if (!$this->query($strSqlQuery)) {
                # sql error
                throw new Exception("Ошибка базы данных: ".__FILE__.':'.__LINE__);
            } else {
                return $this->insert_id();
            }
        }

        return false;
    }

    public function getCompany($id)
    {
        $strSqlQuery = "SELECT * FROM `job_companies` WHERE `jc_id` = ".$id;
        return $this->fetch($strSqlQuery);
    }

    public function getLogo($id)
    {
        $arr = $this->getLogoImage($id);
        return $arr['si_filename'];
    }
}
