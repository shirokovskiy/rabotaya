<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 9/13/13
 * Time         : 5:08 PM
 * Description  :
 */

include_once "cls.db.php";

class SiteUser extends Db {
    protected $id, $_errcode, $strLoginAuth, $strPasswordAuth;
    private $type, $fname, $lname, $mname, $email /* as login */, $phone, $position, $address, $city, $city_id, $inn, $password, $passwordrepeat, $curpassword;

    public function __construct($id = 0)
    {
        parent::__construct();
        if ($id > 0 && isset($_SESSION['uAuthInfo']) && $_SESSION['uAuthInfo']['uID'] > 0 && $_SESSION['uAuthInfo']['uID']==$id) {
            $this->setId($id);
        }
    }

    function __call( $method, $args ) {
        if (method_exists($this, $method)) {
            return $this->$method( $args );
        } else {
            $property = preg_replace("/^get/", "", $method);

            $property = preg_replace("/([A-Z])/", '_$1', $property);
            $property = strtolower($property);
            $property = preg_replace('/^_/', '', $property);

            if (property_exists($this, $property)) {
                return $this->{$property};
            }
        }

        return null;
    }

    public function setId($id)
    {
        $this->id = intval($id);
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getImage($id = null)
    {
        $id = $id?:$this->id;
        if (empty($id)) {
            return false;
        }

        $strSqlQuery = "SELECT * FROM `site_images` WHERE `si_rel_id` = $id AND `si_type` = 'users' ORDER BY si_id LIMIT 1";
        return $this->fetch($strSqlQuery);
    }

    public function getData()
    {
        if ($this->getId()) {
            $strSqlQuery = "SELECT *, su_login email FROM `site_users` WHERE `su_id` = ". $this->getId();
            return $this->fetch($strSqlQuery);
        }
        return false;
    }

    public function getError()
    {
        return $this->_errcode?:false;
    }

    public function getCityId($city)
    {
        $strSqlQuery = "SELECT `id` FROM `site_cities` WHERE `city` LIKE '".$city."' OR `city_eng` LIKE '".$city."' LIMIT 1";
        return (int)$this->fetch($strSqlQuery, 'id');
    }

    public function getCitySubDomain($city)
    {
        $strSqlQuery = "SELECT `SubDomain` FROM `site_cities` WHERE `city` LIKE '".$city."' OR `city_eng` LIKE '".$city."' LIMIT 1";
        return (string)$this->fetch($strSqlQuery, 'SubDomain');
    }

    public function getCityBySubDomain($citySubDomain)
    {
        $strSqlQuery = "SELECT `city` FROM `site_cities` WHERE `SubDomain` = '".$citySubDomain."' LIMIT 1";
        return (string)$this->fetch($strSqlQuery, 'city');
    }

    public function getCountryId($country)
    {
        $strSqlQuery = "SELECT sc_id FROM `site_countries` WHERE `sc_title` = '".$country."'";
        return $this->fetch($strSqlQuery, 'sc_id');
    }

    public function getCityById($id)
    {
        if (empty($id)) return null;
        $strSqlQuery = "SELECT * FROM `site_cities` WHERE `id` = ".$id;
        return $this->fetch($strSqlQuery);
    }

    /**
     * @param $id template ID
     */
    public function getCustomTemplate($id)
    {
        $strSqlQuery = "SELECT * FROM `site_users_custom_templates` WHERE `suct_su_id` = ".$this->getId()." AND `suct_sut_id` = ".$id;
        return $this->fetch($strSqlQuery);
    }

    public function getFullName()
    {
        return $this->getLname().' '. $this->getFname().' '. $this->getMname();
    }

    public function setData(array $arr)
    {
        foreach ($arr as $key => $value) {
            if (!empty($value) && property_exists($this, $key)) {
                $this->$key = $value;
            }
        }

        return $this;
    }

    public function save()
    {
        if ($this->id) {
            $is_password_can_fix = false;
            if (!empty($this->password) && !empty($this->passwordrepeat) && $this->password == $this->passwordrepeat) {
                /**
                 * тогда меняем пароль
                 * но не сразу =)
                 */
                $currHashPassw = $this->getData();
                if (!empty($this->curpassword) && $currHashPassw['su_passw'] == md5($this->curpassword)) {
                    $is_password_can_fix = true;
                }
            }

            if (!empty($this->email) && !$this->checkEmail($this->email)) {
                trace_log('Ошибка изменения логина - не верный формат: '.$this->email);
                $this->_errcode = 'SU004';
                return false;
            }

            if ($existId = $this->isEmailExist($this->email)) {
                if ($existId != $this->id) {
                    trace_log('Ошибка изменения логина - уже существует '.$this->email);
                    $this->_errcode = 'SU001';
                    return false;
                }
            }

            $strSqlQuery = "UPDATE `site_users` SET"
                .(!empty($this->email)?" su_login = '".mysql_real_escape_string($this->email)."'":'')
                .(!empty($this->fname)?", su_fname = '".mysql_real_escape_string(mb_ucfirst($this->fname))."'":'')
                .(!empty($this->lname)?", su_lname = '".mysql_real_escape_string(mb_ucfirst($this->lname))."'":'')
                .(!empty($this->mname)?", su_mname = '".mysql_real_escape_string(mb_ucfirst($this->mname))."'":'')
                .($is_password_can_fix?", su_passw = '".md5($this->password)."'":'')
                .(!empty($this->phone)?", su_phone = '".mysql_real_escape_string($this->phone)."'":'')
                .(!empty($this->address)?", su_address = '".mysql_real_escape_string($this->address)."'":'')
                .(!empty($this->position)?", su_position = '".mysql_real_escape_string($this->position)."'":'')
                .(!empty($this->city)?", su_city = '".mysql_real_escape_string($this->city)."'":'')
                .(!empty($this->inn)?", su_inn = '".mysql_real_escape_string($this->inn)."'":'')
                ." WHERE su_id = ". $this->id
            ;
            if (!$this->query($strSqlQuery, false)) {
                # sql error
                $this->_errcode = 'SU003';
                trace_log('Bad query: '.$strSqlQuery);
                return false;
            }

        } else {
            if (empty($this->password) || empty($this->passwordrepeat) || $this->password != $this->passwordrepeat) {
                trace_log('Ошибка регистрации пароля');
                $this->_errcode = 'SU002';
                return false;
            }

            if ($this->isEmailExist($this->email)) {
                trace_log('Ошибка регистрации логина - уже существует');
                $this->_errcode = 'SU001';
                return false;
            }

            if (!$this->checkEmail($this->email)) {
                trace_log('Ошибка регистрации логина - не верный формат');
                $this->_errcode = 'SU004';
                return false;
            }

            # INSERT with default status = Y
            $strSqlQuery = "INSERT INTO `site_users` SET"
                ." su_login = '".mysql_real_escape_string($this->email)."'"
                .", su_fname = ".(!empty($this->fname)?"'".mysql_real_escape_string(mb_ucfirst($this->fname))."'":'NULL')
                .", su_lname = ".(!empty($this->lname)?"'".mysql_real_escape_string(mb_ucfirst($this->lname))."'":'NULL')
                .", su_mname = ".(!empty($this->mname)?"'".mysql_real_escape_string(mb_ucfirst($this->mname))."'":'NULL')
                .", su_passw = '".md5($this->password)."'"
                .", su_phone = ".(!empty($this->phone)?"'".mysql_real_escape_string($this->phone)."'":'NULL')
                .", su_address = ".(!empty($this->address)?"'".mysql_real_escape_string($this->address)."'":'NULL')
                .", su_position = ".(!empty($this->position)?"'".mysql_real_escape_string($this->position)."'":'NULL')
                .", su_city = ".(!empty($this->city)?"'".mysql_real_escape_string($this->city)."'":'NULL')
                .", su_city_id = ".(!empty($this->city_id)? intval($this->city_id):'NULL')
                .", su_inn = ".(!empty($this->inn)?"'".mysql_real_escape_string($this->inn)."'":'NULL')
                . (!empty($this->type)?", su_type = '".mysql_real_escape_string($this->type)."'":'')
            ;
            if (!$this->query($strSqlQuery, false)) {
                # sql error
                $this->_errcode = 'SU003';
                // todo: отправить exception
                return false;
            } else {
                // todo: отправить email пользователю и возможно нам

                if (strlen($this->password) <= 6) {
                    // todo: дописать в письмо предупреждение что пароль слишком простой
                }
            }
        }

        return true;
    }

    public function passAuth($login, $password) {
        if (isset($_SESSION["uAuthInfo"])) unset($_SESSION["uAuthInfo"]);
        $this->strLoginAuth = strip_tags(addslashes(trim($login)));
        $this->strPasswordAuth = strip_tags(addslashes(trim($password)));

        $code = (!empty($_COOKIE["signInStatus"]) && strlen($_COOKIE["signInStatus"]) == 32) ? $_COOKIE["signInStatus"] : ((!empty($_SESSION["strLoginCode"])) ? $_SESSION["strLoginCode"] : md5(date("login %H:%i %d.%m.%Y")));

        $arrLoggedInUser = $this->loginUser();

        if (is_array($arrLoggedInUser) && !empty($arrLoggedInUser) && $this->id > 0 ) {
            $_SESSION['signInStatus'] = true;

            setcookie("signInStatus", (empty($_SESSION["strLoginCode"]) ? $_SESSION["strLoginCode"] : $code ), strtotime("+1 hour"));

            $_SESSION["strLoginCode"] = $code;
            $_SESSION['uAuthInfo'] = $arrLoggedInUser;

            $this->query("UPDATE `site_users` SET su_last_visit = NOW(), su_ip = INET_ATON('".$_SERVER['REMOTE_ADDR']."') WHERE su_id = ".$this->id);

            return true;
        }

        return false;
    }

    private function loginUser () {
        if ( empty($this->strLoginAuth) || empty($this->strPasswordAuth) ) {
            return false;
        } else {
            $strSqlQuery = "SELECT su_id AS uID, su_date AS uDateReg"
                .", su_fname AS uFName, su_lname AS uLName, su_mname AS uMName" //, su_company AS uCName
                .", su_login AS uLogin, su_phone AS uPhone"
                .", INET_NTOA(su_ip) AS lastIP"
                .", DATE_FORMAT(su_last_visit, '%H:%i %d.%m.%Y') AS lastVisit"
                ." FROM `site_users` WHERE su_login = '".mysql_real_escape_string($this->strLoginAuth)."' AND su_passw = MD5('".mysql_real_escape_string($this->strPasswordAuth)."') AND su_status = 'Y'";
            $arrUserExistInfo = $this->fetch( $strSqlQuery );

            if (empty($arrUserExistInfo)) return false;
            $this->id = $arrUserExistInfo['uID']; // ID клиента

            return $arrUserExistInfo;
        }

        return false;
    }

    public function setLogout() {
        unset($_SESSION['uAuthInfo']);
        unset($_SESSION['signInStatus']);
        unset($_SESSION["strLoginCode"]);

        setcookie("signInStatus", '', strtotime("-1 day"));
    }

    public function saveCountry($country)
    {
        $strSqlQuery = "INSERT INTO `site_countries` SET"
            ." sc_title = '$country'"
            .", sc_date_add = UNIX_TIMESTAMP()";
        if ($this->query($strSqlQuery)) {
            return $this->insert_id();
        }
    }

    public function saveCity($city, $countryId = 1)
    {
        if (empty($city)) return false;
        $strSqlQuery = "INSERT INTO `site_cities` SET"
            ." city = '".mysql_real_escape_string($city)."'"
            .", city_eng = '".mysql_real_escape_string($this->translitKyrToLat($city))."'"
            .", country_id = ".$countryId;
        if ($this->query($strSqlQuery)) {
            return $this->insert_id();
        }

        return false;
    }

    public function saveIPCityCountry($ip, $rcGeo)
    {
        if (!is_object($rcGeo)) return false;
        $city = $rcGeo->city;
        $country = $rcGeo->country_name;
        $this->saveGeo($ip, $city, $country);
        $cityId = $this->getCityId($city);
        if ($cityId > 0) {
            return $cityId;
        } else {
            $countryId = $this->getCountryId($country);
            if ($countryId <= 0) {
                $countryId = $this->saveCountry($country);
            }

            return $this->saveCity($city, $countryId);
        }

        return false;
    }

    public function saveGeo($ip, $city, $country)
    {
        $strSqlQuery = "SELECT sg_id FROM `site_geoip` WHERE `sg_ip` = '".$ip."' AND `sg_city` = '".mysql_real_escape_string($city)."' AND `sg_country` = '".mysql_real_escape_string($country)."'";
        $intGeoIP = $this->fetch($strSqlQuery);
        if (empty($intGeoIP)) {
            $strSqlQuery = "INSERT INTO `site_geoip` SET"
                ." sg_ip = '$ip'"
                .", sg_city = '".mysql_real_escape_string($city)."'"
                .", sg_country = '".mysql_real_escape_string($country)."'"
                .", sg_date_add = UNIX_TIMESTAMP()"
            ;
            if (!$this->query($strSqlQuery)) {
                return false;
            }

            return $this->insert_id();
        } else {
            return $intGeoIP;
        }
    }

    /**
     * @param $id base template ID
     */
    public function saveCustomTemplate($id, $body)
    {
        if ($arr = $this->getCustomTemplate($id)) {
            # update
            $strSqlQuery = "UPDATE `site_users_custom_templates` SET"
                . " `suct_body` = '".mysql_real_escape_string(trim($body))."'"
                . " WHERE `suct_id` = ".$arr['suct_id']." AND `suct_su_id` = ". $this->getId();
            if (!$this->query($strSqlQuery)) {
                # sql error
                return false;
            }
        } else {
            # insert
            $strSqlQuery = "INSERT INTO `site_users_custom_templates` SET"
                . " `suct_su_id` = ". $this->getId()
                . ", `suct_sut_id` = ".$id
                . ", `suct_body` = '".mysql_real_escape_string($body)."'"
            ;
            if (!$this->query($strSqlQuery)) {
                # sql error
                return false;
            } else {
                return $this->insert_id();
            }
        }

        return true;
    }

    public function isAuthed()
    {
        return $this->id > 0 && isset($_SESSION['uAuthInfo']['uID']) && $_SESSION['uAuthInfo']['uID'] == $this->id;
    }

    public function isEmailExist($email)
    {
        $strSqlQuery = "SELECT su_id FROM `site_users` WHERE `su_login` = '". mysql_real_escape_string($email)."'";
        return $this->fetch($strSqlQuery, 'su_id');
    }

    public function getLinkToLogin($id, $type)
    {
        # Общее правило составления линка такое: md5(md5( email + id ) + solt)

        if ($type == 'R') {
            $Record = new Resume();
            $prefix = 'jr_';
        } elseif ($type == 'V') {
            $Record = new Vacancy();
            $prefix = 'jv_';
        } else {
            return null;
        }

        $data = $Record->getData($id);

        if (is_array($data) && !empty($data)) {
            if (!empty($data[$prefix.'email']) && $this->checkEmail($data[$prefix.'email'])) {
                #
                $link = md5( md5( $data[$prefix.'email'] . $id ) . 'Rabota-Ya.ru' );

                if ($Record->saveInviteLinkHash($id, $link)) {
                    return SITE_URL. 'profile/autologin/'.$link;
                }
            }
        }

        return null;
    }

    public function getFavoriteVacancies()
    {
        $strSqlQuery = "SELECT tV.*, tC.jc_title, tL.date_created linkCreated FROM `job_vacancies_favorites_lnk` tL"
        ." LEFT JOIN `job_vacancies` tV ON (tV.`jv_id` = tL.`jv_id`)"
        ." LEFT JOIN `job_companies` tC ON (tV.`jv_jc_id` = `jc_id`)"
        ." WHERE tL.`su_id` = " . $this->getId();
        return $this->fetchall($strSqlQuery);
    }

    public function getBlacklistVacancies()
    {
        $strSqlQuery = "SELECT tV.*, tC.jc_title, `jvbl_date` linkCreated FROM `job_vacancies_blacklist_lnk` tL"
            ." LEFT JOIN `job_vacancies` tV ON (tV.`jv_id` = `jvbl_jv_id`)"
            ." LEFT JOIN `job_companies` tC ON (tV.`jv_jc_id` = `jc_id`)"
            ." WHERE `jvbl_su_id` = ".$this->getId();
        return $this->fetchall($strSqlQuery);
    }

    public function addFavoriteVacancy($id)
    {
        $strSqlQuery = "INSERT INTO `job_vacancies_favorites_lnk` SET `jv_id` = ".$id.", `su_id` = ". $this->getId();
        if (!$this->query($strSqlQuery)) {
            # sql query error
            return false;
        }

        return true;
    }

    public function removeFavoriteVacancy($id)
    {
        $strSqlQuery = "DELETE FROM `job_vacancies_favorites_lnk` WHERE `jv_id` = ".$id." AND `su_id` = ". $this->getId();
        if (!$this->query($strSqlQuery)) {
            # sql query error
            return false;
        }

        return true;
    }

    public function addBlacklistVacancy($id)
    {
        $strSqlQuery = "INSERT INTO `job_vacancies_blacklist_lnk` SET `jvbl_jv_id` = ".$id.", `jvbl_su_id` = ". $this->getId();
        if (!$this->query($strSqlQuery)) {
            # sql query error
            return false;
        }

        return true;
    }

    public function removeBlacklistVacancy($id)
    {
        $strSqlQuery = "DELETE FROM `job_vacancies_blacklist_lnk` WHERE `jvbl_jv_id` = ".$id." AND `jvbl_su_id` = ". $this->getId();
        if (!$this->query($strSqlQuery)) {
            # sql query error
            return false;
        }

        return true;
    }

    public function saveProfileSearchParams($title, $params)
    {
        if (empty($this->id)) return false;
//        $this->setDebugModeOn();
        $strSqlQuery = "INSERT INTO `site_users_searches` SET"
            . " `sus_su_id` = ". $this->getId()
            . ", `sus_title` = '".mysql_real_escape_string($title)."'"
            . ", `sus_params` = '".mysql_real_escape_string($params)."'"
            . ", `sus_created` = UNIX_TIMESTAMP()";
        if (!$this->query($strSqlQuery)) {
            # sql error
            trace_log($strSqlQuery);
            return false;
        } else
            return $this->insert_id();
    }

    public function getVacanciesSearches() {
        if (empty($this->id)) return false;

        $strSqlQuery = "SELECT * FROM `site_users_searches` WHERE `sus_su_id` = " . $this->getId();
        return $this->fetchall($strSqlQuery);
    }

    public function translateVacancySearches($params)
    {
        if (empty($params)) return false;

        $result = '';
        $params = unserialize($params);
        if (is_array($params) && !empty($params)) {
            if (isset($params['type'])) {
                $result .= 'Вы ищите ';
                switch ($params['type']) {
                    case 'vacancies':
                        $result .= 'вакансии ';
                        break;

                    default: break;
                }
            }

            if (isset($params['query']) && !empty($params['query'])) {
                $result .= 'по названию &laquo;'.htmlentities($params['query']).'&raquo;';
            }

            if (isset($params['search_city']) && !empty($params['search_city'])) {
                $result .= ', в городе &laquo;'.htmlentities($params['search_city']).'&raquo;';
            }

            if (isset($params['search_salary']) && !empty($params['search_salary'])) {
                $result .= ', з/п &laquo;'.htmlentities($params['search_salary']).'&raquo;';
            }

            if (isset($params['categoryId']) && !empty($params['categoryId'])) {
                $jobCat = new JobCategory();
                $result .= ', раздел &laquo;'.$jobCat->getCategoryFieldById($params['categoryId'], 'jc_title').'&raquo;';
            }

            if (isset($params['searchWhere'])) {
                $result .= ', искать';
                switch ($params['searchWhere']) {
                    case 1:$result .= ' везде';
                        break;

                    default:$result .= ' только по названиям';
                        break;
                }
            }

            if (isset($params['searchHow'])) {
                $result .= ', искать';
                switch ($params['searchHow']) {
                    case 1:$result .= ' хотя бы одно слово';
                        break;

                    default:$result .= ' все слова';
                    break;
                }
            }

            if (isset($params['subQuery']) && !empty($params['subQuery'])) {
                $result .= ', исключая слова &laquo;'.htmlentities($params['subQuery']).'&raquo;';
            }

            if (isset($params['cbxEdu']) && !empty($params['cbxEdu'])) {
                $jobEdu = new JobEduType();
                $result .= ', образование '.$jobEdu->getStatus($params['cbxEdu']);
            }

            if (isset($params['cbxWorkBusy']) && !empty($params['cbxWorkBusy'])) {
                $result .= ', cbxWorkBusy &laquo;'.htmlentities($params['cbxWorkBusy']).'&raquo;';
            }

            if (isset($params['sbxCompanyType']) && !empty($params['sbxCompanyType'])) {
                switch ($params['sbxCompanyType']) {
                    case 1:$result .= ', только по работодателям';
                        break;

                    case 2:$result .= ', только по КА';
                        break;

                    default:$result .= ', по работодателям и КА';
                        break;
                }
            }

            if (isset($params['sbxPeriod']) && !empty($params['sbxPeriod'])) {
                $result .= ', за период &laquo;'.htmlentities($params['sbxPeriod']).'&raquo;';
            }
        }
        return $result;
    }

    public function deleteVacancySearchFilter($id)
    {
        $strSqlQuery = "DELETE FROM `site_users_searches` WHERE sus_id = ".intval($id);
        if (!$this->query($strSqlQuery)) {
            return false;
        }

        return true;
    }

    public function getResumeSearches() {
        if (empty($this->id)) return false;

        $strSqlQuery = "SELECT * FROM `site_users_searches` WHERE `sus_su_id` = " . $this->getId();
        return $this->fetchall($strSqlQuery);
    }

    public function translateResumeSearches($params)
    {
        if (empty($params)) return false;

        $result = '';
        $params = unserialize($params);
        if (is_array($params) && !empty($params)) {
            if (isset($params['type'])) {
                $result .= 'Вы ищите ';
                switch ($params['type']) {
                    case 'resumes':
                        $result .= 'резюме ';
                        break;

                    default: break;
                }
            }

            if (isset($params['query']) && !empty($params['query'])) {
                $result .= 'по названию &laquo;'.htmlentities($params['query']).'&raquo;';
            }

            if (isset($params['search_city']) && !empty($params['search_city'])) {
                $result .= ', в городе &laquo;'.htmlentities($params['search_city']).'&raquo;';
            }

            if (isset($params['search_salary']) && !empty($params['search_salary'])) {
                $result .= ', з/п &laquo;'.htmlentities($params['search_salary']).'&raquo;';
            }

            if (isset($params['categoryId']) && !empty($params['categoryId'])) {
                $jobCat = new JobCategory();
                $result .= ', раздел &laquo;'.$jobCat->getCategoryFieldById($params['categoryId'], 'jc_title').'&raquo;';
            }

            if (isset($params['searchWhere'])) {
                $result .= ', искать';
                switch ($params['searchWhere']) {
                    case 1:$result .= ' везде';
                        break;

                    default:$result .= ' только по названиям';
                    break;
                }
            }

            if (isset($params['searchHow'])) {
                $result .= ', искать';
                switch ($params['searchHow']) {
                    case 1:$result .= ' хотя бы одно слово';
                        break;

                    default:$result .= ' все слова';
                    break;
                }
            }

            if (isset($params['subQuery']) && !empty($params['subQuery'])) {
                $result .= ', исключая слова &laquo;'.htmlentities($params['subQuery']).'&raquo;';
            }

            if (isset($params['cbxEdu']) && !empty($params['cbxEdu'])) {
                $jobEdu = new JobEduType();
                $result .= ', образование '.$jobEdu->getStatus($params['cbxEdu']);
            }

            if (isset($params['cbxWorkBusy']) && !empty($params['cbxWorkBusy'])) {
                $result .= ', cbxWorkBusy &laquo;'.htmlentities($params['cbxWorkBusy']).'&raquo;';
            }

            if (isset($params['sbxPeriod']) && !empty($params['sbxPeriod'])) {
                $result .= ', за период &laquo;'.htmlentities($params['sbxPeriod']).'&raquo;';
            }
        }
        return $result;
    }

    public function deleteResumeSearchFilter($id)
    {
        $strSqlQuery = "DELETE FROM `site_users_searches` WHERE sus_id = ".intval($id);
        if (!$this->query($strSqlQuery)) {
            return false;
        }

        return true;
    }

    public function getFavoriteResumes()
    {
        $strSqlQuery = "SELECT tR.*, tL.jrf_date FROM `job_resumes_favorites_lnk` tL"
            ." LEFT JOIN `job_resumes` tR ON (tR.`jr_id` = tL.`jrf_jr_id`)"
            ." WHERE tL.`jrf_su_id` = " . $this->getId();
        return $this->fetchall($strSqlQuery);
    }

    public function addFavoriteResume($id)
    {
        $strSqlQuery = "INSERT INTO `job_resumes_favorites_lnk` SET `jrf_jr_id` = ".$id.", `jrf_su_id` = ". $this->getId();
        if (!$this->query($strSqlQuery)) {
            # sql query error
            return false;
        }

        return true;
    }

    public function removeFavoriteResume($id)
    {
        $strSqlQuery = "DELETE FROM `job_resumes_favorites_lnk` WHERE `jrf_jr_id` = ".$id." AND `jrf_su_id` = ". $this->getId();
        if (!$this->query($strSqlQuery)) {
            # sql query error
            return false;
        }

        return true;

    }

    public function addBlacklistResume($id)
    {
        $strSqlQuery = "INSERT INTO `job_resumes_blacklist_lnk` SET `jrbl_jr_id` = ".$id.", `jrbl_su_id` = ". $this->getId();
        if (!$this->query($strSqlQuery)) {
            # sql query error
            return false;
        }

        return true;
    }

    public function removeBlacklistResume($id)
    {
        $strSqlQuery = "DELETE FROM `job_resumes_blacklist_lnk` WHERE `jrbl_jr_id` = ".$id." AND `jrbl_su_id` = ". $this->getId();
        if (!$this->query($strSqlQuery)) {
            # sql query error
            return false;
        }

        return true;

    }

    public function getBlacklistResumes()
    {
        $strSqlQuery = "SELECT tR.*, tL.jrbl_date FROM `job_resumes_blacklist_lnk` tL"
            ." LEFT JOIN `job_resumes` tR ON (tR.`jr_id` = tL.`jrbl_jr_id`)"
            ." WHERE tL.`jrbl_su_id` = " . $this->getId();
        return $this->fetchall($strSqlQuery);
    }

    public function saveProfileInformerParams($params, $compID = 0)
    {
        if (empty($this->id)) return false;
//        $this->setDebugModeOn();
        $strSqlQuery = "INSERT INTO `job_resumes_informer` SET"
            . " `jri_jc_id` = ".$compID
            . ", `jri_params` = '".mysql_real_escape_string($params)."'"
            . ", `jri_created` = UNIX_TIMESTAMP()";
        if (!$this->query($strSqlQuery)) {
            # sql error
            trace_log($strSqlQuery);
            return false;
        } else
            return $this->insert_id();
    }
}
