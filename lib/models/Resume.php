<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 8/14/13
 * Time         : 1:23 AM
 * Description  :
 */

class Resume extends Db {
    protected $table = 'job_resumes';
    protected $id;

    public function setId($id)
    {
        $this->id = intval($id);
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * Get total number of available resumes.
     * @return array|null|string
     */
    public function getTotalCount()
    {
        $strSqlQuery = "SELECT COUNT(*) FROM `job_resumes` WHERE `jr_status` = 'Y'";
        return $this->fetch($strSqlQuery, 0);
    }

    public function getCountByCategory($id)
    {
        $strSearchTerm = '';
        if (isset($_SESSION['myCity'])) {
            $objSiteUser = new SiteUser();
            $cityId = $objSiteUser->getCityId(trim($_SESSION['myCity']));
            if ($cityId > 0) {
                $strSearchTerm = " AND (jr_city LIKE '%".$_SESSION['myCity']."%' OR jr_city_id = $cityId)";
            } else {
                $strSearchTerm = " AND jr_city LIKE '".$_SESSION['myCity']."'";
            }
        }

        $strSqlQuery = "SELECT COUNT(*) AS intQuantitySelectRecords FROM `job_resumes`"
            ." LEFT JOIN `job_category_link` ON (jcl_rel_id = jr_id AND jcl_type = 'resume')"
            ." WHERE jr_status = 'Y' AND jcl_sjc_id = ".$id.$strSearchTerm;
        return $this->fetch($strSqlQuery, 0);
    }

    /**
     * Check if resume exists by given user ID
     * @param int $rid
     * @param int $uid
     * @return bool
     */
    public function checkResumeByUser($rid, $uid)
    {
        $strSqlQuery = "SELECT `jr_id` FROM `job_resumes` WHERE `jr_id` = ".$rid." AND `jr_su_id` = ".$uid;
        $arrResume = $this->fetch($strSqlQuery);
        if (is_array($arrResume) && !empty($arrResume)) {
            return true;
        }

        return false;
    }

    /**
     * Check if resume exists by given email
     * @param int $rid
     * @param string $email
     * @return bool
     */
    public function checkResumeByEmail($rid, $email)
    {
        $strSqlQuery = "SELECT `jr_id` FROM `job_resumes` WHERE `jr_id` = ".$rid." AND `jr_email` LIKE '".$email."'";
        $arrResume = $this->fetch($strSqlQuery);
        if (is_array($arrResume) && !empty($arrResume)) {
            return true;
        }

        return false;
    }

    /**
     * Remove resume from database
     * @param int $resumeId
     * @return bool
     */
    public function delete($resumeId)
    {
        $strSqlQuery = "DELETE FROM `job_resumes` WHERE jr_id = ".intval($resumeId);
        if (!$this->query($strSqlQuery)) {
            # sql query error
            return false;
        } else {
            // TODO: remove all depended files
        }

        return true;
    }

    /**
     * Set status of resume by given ID
     * @param int $id
     * @param string $status
     * @return bool
     */
    public function setStatus($id, $status)
    {
        $strSqlQuery = "UPDATE `job_resumes` SET"
            . " `jr_status` = '$status'"
            . " WHERE `jr_id` = ".intval($id);
        if (!$this->query($strSqlQuery)) {
            # sql error
            return false;
        }

        return true;
    }

    public function incViews($id)
    {
        $strSqlQuery = "UPDATE `job_resumes` SET"
            . " `jr_views` = `jr_views`+1"
            . " WHERE `jr_id` = ".intval($id);
        if (!$this->query($strSqlQuery)) {
            # sql error
            return false;
        }

        return true;
    }

    public function getList($exp = '', $limit = 100)
    {
        $strSqlWhere = '';
        $arrSqlWhere = array();
        if (is_array($exp)) {
            foreach ($exp as $field => $expression) {
                $arrSqlWhere[] = $field.' '.$expression;
            }
        }

        if (is_array($arrSqlWhere) && !empty($arrSqlWhere)) {
            $strSqlWhere = implode(' AND ', $arrSqlWhere);
        }

        $strSqlQuery = "SELECT * FROM `job_resumes` " . (!empty($strSqlWhere)? ' WHERE '.$strSqlWhere : '').($limit > 0? ' LIMIT '.intval($limit) : '');
        return $this->fetchall($strSqlQuery);
    }

    public function getData($id)
    {
        if ($id <= 0) return null;
        $strSqlQuery = "SELECT * FROM `".$this->table."` WHERE `jr_id` = ".intval($id);
        return $this->fetch($strSqlQuery);
    }

    public function isExistsByEmail($email)
    {
        $strSqlQuery = "SELECT jr_id FROM `".$this->table."` WHERE `jr_email` LIKE '". mysql_real_escape_string($email)."' ORDER BY jr_id LIMIT 1";
        return $this->fetch($strSqlQuery, 'jr_id');
    }


    /**
     * Возвращает линк для редактирования своих резюме не будучи зарегистрированным
     *
     * @param $id
     * @param $rand
     * @return null|string
     */
    public function getLinkToEdit($id, $rand)
    {
        if ($id <= 0 || empty($rand)) return null;
        # Общее правило составления линка такое: md5(md5( email + id ) + solt)
        $data = $this->getData($id);

        if (is_array($data) && !empty($data)) {
            if (!empty($data['jr_email']) && $this->checkEmail($data['jr_email'])) {
                #
                $link = md5( md5( $data['jr_email'] . $id ) . 'Rabota-Ya.ru'. $rand );

                if ($this->saveInviteLinkHash($id, $link)) {
                    return SITE_URL. 'profile/autologin/'.$link;
                }
            }
        }

        return null;
    }

    public function isValidLink2Profile($hash)
    {
        if (empty($hash) || strlen($hash) != 32) {
            return false;
        }

        $strSqlQuery = "SELECT `jr_id` FROM `". $this->table."` WHERE `jr_invite_hash` LIKE '".$hash."' LIMIT 1";
        $arr = $this->fetch($strSqlQuery);
        if (is_array($arr) && !empty($arr)) {
            # значит хеш от резюме и от физ.лица

            return $arr;
        }

        # by default
        return false;
    }

    public function getRecordDataByHash($hash)
    {
        if (empty($hash) || strlen($hash) != 32) {
            return false;
        }

        $strSqlQuery = "SELECT * FROM `". $this->table."` LEFT JOIN `site_users` ON (jr_email = su_login AND su_type = 'fiz') WHERE `jr_invite_hash` LIKE '".$hash."' AND `jr_invited` IS NOT NULL LIMIT 1";
        $arr = $this->fetch($strSqlQuery);
        if (is_array($arr) && !empty($arr)) {
            # значит хеш от резюме и от физ.лица

            return $arr;
        }

        # by default
        return false;
    }

    public function setIsInvited($id)
    {
        $strSqlQuery = "UPDATE `".$this->table."` SET"
            . " `jr_invited` = `jr_invited` + 1"
            . " WHERE `jr_id` = ".intval($id);
        if (!$this->query($strSqlQuery)) {
            # sql error
            return false;
        }

        return true;
    }

    /**
     * Отметим пользователя как Приглашён
     *
     * @param $id
     * @param $hash
     * @return bool
     */
    public function saveInviteLinkHash($id, $hash)
    {
        if (empty($hash) || $id <= 0) return false;
        $strSqlQuery = "UPDATE `" . $this->table . "` SET"
            . " `jr_invite_hash` = '$hash'"
            . " WHERE `jr_id` = ".$id;
        if (!$this->query($strSqlQuery)) {
            # sql error
            return false;
        }

        return true;
    }

    public function saveSeoUrl($url)
    {
        if ($this->getId() <= 0) return false;

        $strSqlQuery = "SELECT jr_id, jr_seo_url FROM `job_resumes` WHERE `jr_id` = " . $this->getId();
        $arrVac = $this->fetch($strSqlQuery);

        if (is_array($arrVac) && !empty($arrVac)) {
            if (empty($arrVac['jr_seo_url'])) {
                $strSqlQuery = "UPDATE `job_resumes` SET"
                    . " `jr_seo_url` = '$url'"
                    . " WHERE `jr_id` = ".$this->getId();
                if (!$this->query($strSqlQuery)) {
                    # sql error
                    return false;
                }
            }
        }

        return true;
    }
}
