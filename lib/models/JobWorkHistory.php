<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 8/12/13
 * Time         : 12:05 AM
 * Description  : 
 */

class JobWorkHistory extends Db {

    public function removeHistoryRecord($id, $resume_id) {
        $strSqlQuery = "DELETE FROM `job_resumes_works` WHERE jrw_id = ".$id." AND jrw_jr_id = ".$resume_id." LIMIT 1";
        if (!$this->query($strSqlQuery)) {
            # sql query error
            return false;
        }

        return true;
    }
}
