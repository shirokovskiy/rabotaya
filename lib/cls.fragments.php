<?php
/**
 * Class clsFragments
 *
 * Класс фрагментов страниц и шаблонов
 *
 * @version 1.0
 * @author  Dmitry Shirokovskiy
 * @date    2005.07.31
 */

class clsFragments extends Files {
	public $frgPHPPath; // путь к файлу-обработчику фрагмента
	public $frgHTMLPath // путь к файлу-шаблону фрагмента

	, $frgPHPFileName
	, $frgHTMLFileName

	, $frgPHPContentFile
	, $frgHTMLContentFile

	, $frgTextPHPFile
	, $frgName;

	function __construct($strFragmentIDName, $strFragmentName) {
        $_is_dir = true;
        if (isset($_SESSION['arrInfoAccessProject'][$_SESSION['intIdDefaultProject']]['strPathProject'])) {
            $baseDir = $_SESSION['arrInfoAccessProject'][$_SESSION['intIdDefaultProject']]['strPathProject'];
            if (is_dir($baseDir)) {
                $this->frgPHPPath = $baseDir.REL_PHP_PATH.'fragments/';
                $this->frgHTMLPath = $baseDir.REL_TPL_PATH.'fragments/';

                if(!is_dir($this->frgPHPPath)) {
                    if (!mkdir($this->frgPHPPath, 0775, true)) {
                        $_is_dir = false;
                        trace_log('Не могу создать директорию '.$this->frgPHPPath);
                    }
                }

                if(!is_dir($this->frgHTMLPath)) {
                    if (!mkdir($this->frgHTMLPath, 0775, true)) {
                        $_is_dir = false;
                        trace_log('Не могу создать директорию '.$this->frgHTMLPath);
                    }
                }
            }

        }
        if (!$_is_dir) {
            $this->frgPHPPath = SITE_PHP_FRG_DIR;
            $this->frgHTMLPath = SITE_TPL_FRG_DIR;
        }

		$this->frgName = $strFragmentIDName; // Уникальное имя фрагмента

		$this->frgPHPFileName = $this->frgName.'.inc.php'; // Файл-обработчик фрагмента
		$this->frgHTMLFileName = $this->frgName.'.frg'; // Файл-шаблон фрагмента

		$this->frgPHPContentFile = '<?php /* CMS - PhpWebStudio (Dmitry Shirokovskiy) Copyright 2006.10 - '.date('Y.m').' */'."\n".
       	'///+++ Обработчик фрагмента: '.stripslashes($strFragmentName).' ['.$this->frgName.']'."\n".
       	'$arrTplVars[\'name.fragment\'] = \''.$this->frgName.'\';'."\n".
       	'$objTpl->tpl_load($arrTplVars[\'name.fragment\'], "'.$this->frgHTMLFileName.'");'."\n\n\n".
       	'$objTpl->tpl_array($arrTplVars[\'name.fragment\'], $arrTplVars);'."\n";
	}

    /**
     * Смена UID-имени файла-обработчика фрагмента
     *
     * @param $strNewFrgName
     */
    public function setFrgName($strNewFrgName) {
		$this->frgName = $strNewFrgName;
		$this->frgPHPFileName = $this->frgName.'.inc.php'; // Файл-обработчик фрагмента
		$this->frgHTMLFileName = $this->frgName.'.frg'; // Файл-шаблон фрагмента
	}

    /**
     * Смена каталога для файлов-обработчиков фрагмента
     *  $strNewPath - путь к папке в которой находится структура папок
     *  SITE_PHP_FRG_DIR (по умолчанию: php/fragments/)
     *
     * @param $strNewPath
     */
    private function setPHPPath($strNewPath) {
		$this->frgPHPPath = $strNewPath.SITE_PHP_FRG_DIR;
	}

    /**
     * Смена каталога для файлов-шаблонов фрагмента
     *  $strNewPath - путь к папке в которой находится структура папок
     *  SITE_TPL_FRG_DIR (по умолчанию: templates/fragments/)
     *
     * @param unknown_type $strNewPath
     */
	private function setHTMLPath($strNewPath) {
		$this->frgPHPPath = $strNewPath.SITE_TPL_FRG_DIR;
	}

    /**
     * Чтение содержимого файла-обработчика текущего фрагмента
     */
	public function getContentPHPHandler() {
		$this->frgPHPContentFile = $this->readFile($this->frgPHPPath.$this->frgPHPFileName); // Читаем содержимое файла-обработчика фрагмента
	}

    /**
     * Чтение содержимого файла-шаблона текущего фрагмента
     *
     */
	private function getContentHTMLHandler() {
		$this->frgHTMLContentFile = $this->readFile($this->frgHTMLPath.$this->frgHTMLFileName); // Читаем содержимое файла-обработчика фрагмента
	}

    /**
     * Замена строк в содержимом файлов обработчиков фрагментов
     *
     * @param $strLastName
     * @param $strNewName
     */
    public function replaceNameInPHPContent($strLastName, $strNewName) {
		$this->frgPHPContentFile = str_replace($strLastName, $strNewName, $this->frgPHPContentFile);
	}

    /**
     * Сохранение файла-обработчика фрагмента
     *
     * @param bool $boolCHmod
     * @return bool
     */
    public function savePHPFile($boolCHmod=true) {
		if ( !$this->writeFile($this->frgPHPContentFile, $this->frgPHPFileName, $this->frgPHPPath) ) {
			return false;
		}

		if ( $boolCHmod ) {
			@chmod($this->frgPHPPath.$this->frgPHPFileName, 0664);
		}

		return true;
	}

    /**
     * Сохранение файла-шаблона фрагмента
     *
     * @param bool $boolCHmod
     * @return bool
     */
    public function saveHTMLFile($boolCHmod=true) {
		if(!$this->writeFile($this->frgHTMLContentFile, $this->frgHTMLFileName, $this->frgHTMLPath ))
			return false;

		if($boolCHmod)
			@chmod($this->frgHTMLPath.$this->frgHTMLFileName, 0664);

		return true;
	}

    /**
     * Удаление файла-обработчика фрагмента
     */
    public function deletePHPFile() {
		if(file_exists($this->frgPHPPath.$this->frgPHPFileName))
			@unlink($this->frgPHPPath.$this->frgPHPFileName);
	}

    /**
     * Удаление файла-шаблона фрагмента
     */
    public function deleteHTMLFile() {
		if(file_exists($this->frgHTMLPath.$this->frgHTMLFileName))
			@unlink($this->frgHTMLPath.$this->frgHTMLFileName);
	}
}
