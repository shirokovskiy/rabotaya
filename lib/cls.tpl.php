<?php
/**
 * Class Template
 *
 * Template library Class
 *
 * Shirokovskiy Dmitry edition (shirokovskij@mail.ru)
 * 2006 (c)
 */
class Template {
	public $dir   = "";
	public $files = array();
	public $arrFrgForLoad = array(); // Массив найденных в тексте шаблона фрагментов для загрузки

	function Template ($dir = '') {
		if (!empty($dir) && file_exists($dir))
			$this->dir = $dir;
		else {
			if (defined('SITE_TPL_DIR')) {
				$this->dir = SITE_TPL_DIR;
			}
		}

		$this->lst=500;
	}


    /**
     * Метод обработки контента шаблона, для вставки в формы
     *
     * @param $body
     * @param string $type
     * @return mixed
     */
    public function parseFormVars($body, $type="in") {
		if($type=="in") {
			$body = preg_replace("/\{([a-z_0-9\.]+)\}/i", "[\\1]", $body);
			$body = preg_replace("/\<(\/?[a-z0-9]+ [a-z_0-9\.]+)\>/i", "[\\1]", $body);
		} else {
			$body = preg_replace("/\[([a-z_0-9\.]+)\]/i", "{\\1}", $body);
			$body = preg_replace("/\[(\/?[a-z0-9]+ [a-z_0-9\.]+)\]/i", "<\\1>", $body);
		}
        $body = preg_replace("/\r/", "", $body);
		return $body;
	}

    /**
     * Метод обрабатывает текст шаблона, находит вхождения фрагментов.
     * Найденные фрагменты сохраняются в массиве $this->arrFrgForLoad
     *
     * @param $body
     */
    public function tpl_parse_frg($body) { // parseForLoadFrg
		preg_match_all("/<frg ([^>]+)>/", $body, $arrFrgMatches, PREG_PATTERN_ORDER);

		foreach ($arrFrgMatches[0] as $value) {
			$this->arrFrgForLoad[] = substr($value, 5, -1);
		}
	}

	public function tpl_load ($fid, $filename, $vars='') {
		if (empty($filename)) {
			echo '<br><p align="center" style="color:red"><b>Не указан файл модуля!</b>';
            exit();
		}

		if (!file_exists($this->dir.$filename)) {
			header('location:/error?file_not_found='.urlencode($filename));
			exit();
		}

		$this->files[$fid] = fread($fp = fopen($this->dir.$filename, 'r'), filesize($this->dir.$filename)+1);
		fclose($fp);

		if (!empty($vars)) {
			$this->tpl_vars($fid, $vars);
		}
	}

	function tpl_include ($fid, $filename){
		$include = fread($fp = fopen($this->dir.$filename, 'r'), filesize($this->dir.$filename));
		fclose($fp);

		$this->files[$fid] = str_replace("<include $filename>", $include, $this->files[$fid]);
	}

	function tpl_read ($filename){
		$a = fread($fp = fopen($filename, 'r'), filesize($filename));
		fclose($fp);
		return $a;
	}

	function tpl_vars ($fid, $vars = null) {
		if (is_null($vars)) return $vars;
		if (!is_array($vars)) {
			$vars = explode(',', $vars);
		}
		if (is_array($vars)) {
			foreach($vars as $var) {
				$tvr = trim($var);
				if(strpos($this->files[$fid], '{'.$tvr.'}')) {
					global $$tvr;
					if (isset($$tvr)) $this->files[$fid] = str_replace('{'.$tvr.'}', $$tvr, $this->files[$fid]);
				}
			}
		}
	}

	/**
	 * Обработать переменные в шаблоне
	 *
	 * @param $fid
	 * @param $ar
	 */
	function tpl_array ($fid, &$ar) {
		if (is_array($ar)) {
			foreach ($ar as $var => $value) {
				if ( strpos($this->files[$fid], '{'.$var.'}') ) {
					$this->files[$fid] = str_replace('{'.$var.'}', $value, $this->files[$fid]);
				}
			}
		}
	}

	function tpl_strip_loops($fid) {
		while (preg_match("/<loop ([0-9A-Za-z_\.]+)>/", $this->files[$fid], $arr)) {
			/*$fp = fopen("fname.$fid.txt", "a+");
						fwrite($fp, $this->files[$fid]);
						fclose($fp); die;*/

			$this->files[$fid]=preg_replace("/<loop ".$arr[1].">[\w\s\W]+<\/loop ".$arr[1].">/", "", $this->files[$fid]);
		}
	}

	function tpl_loop ($fid, $loop, $a, $bg = "", $bgvar = "bg"){
		$control = 1;
		$bg_control = '';

		$n = !empty($a) ? count($a) : 0;

		$tag1 = "<loop $loop>";
		$tag2 = "</loop $loop>";

		$pos1 = strpos($this->files[$fid], $tag1) + strlen($tag1);
		$pos2 = strpos($this->files[$fid], $tag2);

		$loopcode = substr($this->files[$fid], $pos1, $pos2-$pos1);

		if (!empty($loopcode)) {
			$newcode = '';
			$j=0;
			if ( is_array($a) ) {
				foreach ( $a as $row ) {
					$tempcode = $loopcode;
					if (is_array($row)) {
						foreach ($row as $var => $value) {
							if (!is_array($value)) $tempcode = str_replace('{'.$var.'}',$value, $tempcode);
							if (isset($bg) && is_array($bg) && $bg_control!=$control) {
								if ($j==1) {$j=0;} else {$j=1;}
								$tempcode = str_replace("{".$bgvar."}", "bgcolor=".$bg[$j], $tempcode);
								$bg_control=$control;
							}
						}
						$newcode .= $tempcode;
					}

					$control++;
				}
			}

			$this->files[$fid] = str_replace($tag1.$loopcode.$tag2, $newcode, $this->files[$fid]);
		}
	}

	function tpl_show ($fid) {
		foreach ($this->files as $k => $cnt) {
			$this->files[$k] = preg_replace("/[\n]{4,}/", "", $cnt);
		}
		echo $this->files[$fid];
	}

	function tpl_parse($fid, $stripempty=true) {
		do {
			$replaced = 0;
			for(reset($this->files); $key = key($this->files); next($this->files)) {
				if ($key!=$fid) {
					if (is_int(strpos($this->files[$fid],"<frg $key>"))) {
						$replaced++;
						$this->files[$fid] = str_replace("<frg ".$key.">", $this->files[$key], $this->files[$fid]);
						unset($this->files[$key]);
					} elseif(is_int(strpos($this->files[$fid],"<tpl $key>"))) {
						$replaced++;
						$this->files[$fid] = str_replace("<tpl ".$key.">", $this->files[$key], $this->files[$fid]);
						unset($this->files[$key]);
					}
				}
			}
		} while ($replaced>0);

		if ($stripempty) {
			$this->files[$fid]=preg_replace("/{[_a-z0-9\-\.]+}/i","",$this->files[$fid]);
		}

		$this->tpl_show($fid);
	}

	function tpl_if ($fid, $arIfs) {
		while (is_long($pos = strpos($this->files[$fid], '<ifelse '))) {
			$pos1 = strpos($this->files[$fid], '<ifelse ');
			$pos2 = strpos($this->files[$fid], '>', $pos1);

			$ifelsename = substr ($this->files[$fid], $pos1+8, $pos2-$pos1-8);
			$ifelsecode = substr ($this->files[$fid], $pos2+1, strpos($this->files[$fid],'</ifelse '.$ifelsename.'>',$pos2)-$pos2-1);

			$newelsecode = $ifelsecode;

			if ( $arIfs[$ifelsename] || !is_long(strpos($this->files[$fid], "<if $ifelsename")) ) {
				$newelsecode='';
			}

			if(stristr($this->files[$fid], '</ifelse '.$ifelsename.'>') === FALSE) {
				echo '<font color=red>Error in template: tag IFELSE <b>'.$ifelsename.'</b> not found!';
				break;
			}

			$this->files[$fid] = str_replace('<ifelse '.$ifelsename.'>'.$ifelsecode.'</ifelse '.$ifelsename.'>', $newelsecode, $this->files[$fid]);
		}

		while (is_long($pos = strpos($this->files[$fid], '<if '))) {
			$pos1 = strpos($this->files[$fid], '<if ');
			$pos2 = strpos($this->files[$fid], '>', $pos1);

			$ifname = substr ($this->files[$fid], $pos1+4, $pos2-$pos1-4);
			$ifcode = substr ($this->files[$fid], $pos2+1, strpos($this->files[$fid],'</if '.$ifname.'>',$pos2)-$pos2-1);

			$newcode = $ifcode;

			if (!isset($arIfs[$ifname]) || !$arIfs[$ifname]) {
				$newcode='';
			}

			if(stristr($this->files[$fid], '</if '.$ifname.'>') === FALSE) {
				echo '<font color=red>Error in template: tag IF <b>'.$ifname.'</b> not found!';
				break;
			}

			$this->files[$fid] = str_replace('<if '.$ifname.'>'.$ifcode.'</if '.$ifname.'>', $newcode, $this->files[$fid]);
		}
	}

	/**
	 * Метод обработки доступности елементов для Супер Пользователя
	 *
	 * @param unknown_type $fid
	 */
	function tpl_parse_root($fid) {
		while (is_int($intPos = strpos($this->files[$fid], '<ifa '))) {
			$intPosFirst = strpos($this->files[$fid], '<ifa '); // Вычисляем позицию начала строки
			$intPosSecond = strpos($this->files[$fid], '>', $intPosFirst); // Вычисляем позицию конца строки

			$ifname = substr($this->files[$fid], $intPosFirst+5, $intPosSecond-$intPosFirst-5);
			$ifcode = substr($this->files[$fid], $intPosSecond+1, strpos($this->files[$fid], '</ifa '.$ifname.'>', $intPosSecond)-$intPosSecond-1);

			$replaceCode = $ifcode;

			if ($_SESSION['user_sess']['cu_group']!='1') // Если не администратор, заменяем на пустышку
				$replaceCode='';

			$this->files[$fid] = str_replace('<ifa '.$ifname.'>'.$ifcode.'</ifa '.$ifname.'>', $replaceCode, $this->files[$fid]);
		}
	}

	var $arrLangWord = array(
		"<if "
	, "<loop "
	, "<tpl "
	, "<frg "

	, "\[if "
	, "\[loop "
	, "\[tpl "
	, "\[frg "
	, "\{"
	, "\}"
	);
}
