<?php
/** >***************************************************************
 * Created by Jimmy™. Shirokovskiy D.A. Thu Oct 19 14:23:53 MSD 2006
 *
 * Класс работы с графическими файлами
 *
 *******************************************************************/
class clsImages {
	public $arrMimeTypes = array(
		// .jpg
		'image/jpeg'                => 'jpeg',
		'image/jpg'                 => 'jpeg',
		'image/jp_'                 => 'jpeg',
		'application/jpg'           => 'jpeg',
		'application/x-jpg'         => 'jpeg',
		'image/pjpeg'               => 'jpeg',
		'image/pipeg'               => 'jpeg',
		'image/vnd.swiftview-jpeg'  => 'jpeg',
		// .gif
		'image/gif' => 'gif',
		'image/gi_' => 'gif',
		// .png
		'image/png' => 'png',
	);

	public function __construct() {
	}

	/**
	 * Метод преобразовывает изображение в нужный размер
	 * Если параметр intHeight равен 0 или пустой, преобразование будет
	 * происходить только по ширине изображения, высота будет выбрана
	 * пропорционально исходному размеру
	 *
	 * @param unknown_type $strSrc
	 * @param unknown_type $strDest
	 * @param unknown_type $intWidth
	 * @param unknown_type $intHeight
	 * @param unknown_type $strRgb
	 * @param unknown_type $intQuality
	 * @return unknown
	 */
	public function resizeImage($strSrc, $strDest, $intWidth = 200, $intHeight = 0, $strRgb=0xFFFFFF, $intQuality=100, $isSaveRatio = false) {
		if (!file_exists($strSrc)) return false;
		if ($intWidth <= 0 && $intHeight <= 0) return false;

		$arrSize = getimagesize($strSrc);

		if ($arrSize === false) {
			return false;
		} else {
			$intSourceWidth = $arrSize[0];
			$intSourceHeight = $arrSize[1];
		}

		$strFormat = strtolower(substr($arrSize['mime'], strpos($arrSize['mime'], '/')+1));
		$strCrtFunc = "imagecreatefrom".$strFormat;
		$imageFunc = "image".$strFormat;

		if ($strFormat=='png') $intQuality = 3;

        if(!is_dir(getcwd().DIRECTORY_SEPARATOR.'logs')) {
            mkdir(getcwd().DIRECTORY_SEPARATOR.'logs', 0775);
        }

		$fh = fopen(getcwd().DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR.'debug.log', "a+");
		fwrite($fh, date('H:i:s d.M ').'$intSourceWidth='.print_r($intSourceWidth, true)."\n");
		fwrite($fh, date('H:i:s d.M ').'$intSourceHeight='.print_r($intSourceHeight, true)."\n");
		fwrite($fh, date('H:i:s d.M ').'$strFormat='.print_r($strFormat, true)."\n");
		fwrite($fh, date('H:i:s d.M ').'$strCrtFunc='.print_r($strCrtFunc, true)."\n");
		fwrite($fh, date('H:i:s d.M ').'$imageFunc='.print_r($imageFunc, true)."\n");
		fclose($fh);

		if (!function_exists($strCrtFunc) || !function_exists($imageFunc)) {
			return false;
		}

		if ( empty($intHeight) && intval($intWidth) >= 2 ) {
			# высота (h) неизв., вычислим h из ширины (w)
			$ratio = $intWidth / $intSourceWidth;
			$intHeight = floor($intSourceHeight * $ratio);
		} elseif (empty($intWidth) && intval($intHeight) >= 2) {
			# w неизв., вычислим w из h
			$ratio = $intHeight / $intSourceHeight;
			$intWidth = floor($intSourceWidth * $ratio);
		}

		if ($isSaveRatio) {
			// получаем размеры новой картинки
			$xRatio = $intWidth / $intSourceWidth;
			$yRatio = $intHeight / $intSourceHeight;

			$ratio = min($xRatio, $yRatio);
			$use_x_ratio = ($xRatio == $ratio);

			$intWidth  = $use_x_ratio  ? $intWidth  : floor($intSourceWidth * $ratio);
			$intHeight = !$use_x_ratio ? $intHeight : floor($intSourceHeight * $ratio);

//		  	$new_left = (!$isSaveRatio) ? ($use_x_ratio  ? 0 : floor(($intWidth  - $intNewWidth)  / 2)) : 0;
//		  	$new_top  = (!$isSaveRatio) ? (!$use_x_ratio ? 0 : floor(($intHeight - $intNewHeight) / 2)) : 0;
		}

		# Execute
		try {
			$iSrc = $strCrtFunc($strSrc);
			$iDest = imagecreatetruecolor($intWidth, $intHeight);

			imagefill($iDest, 0, 0, $strRgb);

			if ( ($intSourceWidth/$intWidth) >= ($intSourceHeight/$intHeight) ) {

				$intNewWidth = round(($intHeight*$intSourceWidth)/$intSourceHeight);
				$intNewHeight = $intHeight;
				$intLeft = round(($intNewWidth-$intWidth)/2);

				imagecopyresampled($iDest, $iSrc, -$intLeft, 0, 0, 0, $intNewWidth+1, $intNewHeight+1, $intSourceWidth, $intSourceHeight);
			} else {
				$intNewHeight = round(($intWidth*$intSourceHeight)/$intSourceWidth);
				$intNewWidth = $intWidth;
				$intTop = round(($intNewHeight-$intHeight)/2);

				imagecopyresampled($iDest, $iSrc, 0, -$intTop, 0, 0, $intNewWidth+1, $intNewHeight+1, $intSourceWidth, $intSourceHeight);
			}

			$imageFunc($iDest, $strDest, $intQuality);
		} catch (Exception $ex) {
			$fp = fopen(DROOT."logs/classImages.trace.log", "a+");
			fwrite($fp, $ex->getTraceAsString()."\n");
			fwrite($fp, $ex->getMessage()."\n===\n");
			fclose($fp);
		}

		imagedestroy($iSrc);
		imagedestroy($iDest);

		return true;
	}


	/**
	 * Think about PNG only, maybe GIF
	 *
	 * @param $imgBase
	 * @param $imgMark
	 * @param int $posX
	 * @param int $posY
	 * @return bool
	 */
	public function setWaterMark($imgBase, $imgMark, $posX = 0, $posY = 0) {
		// Prepare base image
		$arrSizeBase = getimagesize($imgBase);

//		$fp = fopen(DROOT."logs/classImages.trace.log", "a+");
//		fwrite($fp, print_r($arrSizeBase,true)."\n");
//		fclose($fp);

		if ($arrSizeBase === false) {
			return false;
		}
		$strFormatBase = strtolower(substr($arrSizeBase['mime'], strpos($arrSizeBase['mime'], '/')+1));
		$imagecreateFunc = "imagecreatefrom".$strFormatBase;
		$imageFunc = "image".$strFormatBase;

		if (!function_exists($imagecreateFunc) || !function_exists($imageFunc)) {
			return false;
		}

		try {
//			$im = imagecreatefromstring(file_get_contents($imgBase)); // image handler
			$out = $imagecreateFunc($imgBase); // image handler
//			$out = imagecreatetruecolor($arrSizeBase[0],$arrSizeBase[1]); // dummy for output
//			imagecopymerge($out, $im, 0, 0, 0, 0, $arrSizeBase[0], $arrSizeBase[1], 90); // fill dummy by original

		} catch (Exception $ex) {
			$fp = fopen(DROOT."logs/classImages.trace.log", "a+");
			fwrite($fp, $ex->getTraceAsString()."\n");
			fwrite($fp, $ex->getMessage()."\n===\n");
			fclose($fp);
		}


		// Prepare marker
		$arrSizeMark = getimagesize($imgMark);
		if ($arrSizeMark === false) {
			return false;
		}
		$strFormat = strtolower(substr($arrSizeMark['mime'], strpos($arrSizeMark['mime'], '/')+1));
		$imagecreateFunc = "imagecreatefrom".$strFormat;

		if (!function_exists($imagecreateFunc)) {
			return false;
		}

		try {
			$stamp = $imagecreateFunc($imgMark);
//			$stamp = imagecreatefrompng($imgMark); // MUST BE .PNG

			if ($posX==$posY && $posX==0) {
				$posX = ($arrSizeBase[0]-$arrSizeMark[0])/2;
				$posY = ($arrSizeBase[1]-$arrSizeMark[1])/2;
			}

//			imagecopymerge($out, $stamp, $posX, $posY, 0, 0, imagesx($stamp), imagesy($stamp), 15); // set watermark
			imagecopy($out, $stamp, $posX, $posY, 0, 0, imagesx($stamp), imagesy($stamp)); // set watermark

			if ($strFormatBase == 'png') {
				$imageFunc($out, $imgBase, 0);

			} elseif ($strFormatBase == 'gif') {
				$imageFunc($out, $imgBase);

			} elseif ($strFormatBase == 'jpeg' || $strFormatBase == 'jpg') {
				$imageFunc($out, $imgBase, 85);
			}

//			imagedestroy($im);
			imagedestroy($out);
			imagedestroy($stamp);

		} catch (Exception $ex) {
			$fp = fopen(DROOT."logs/classImages.trace.log", "a+");
			fwrite($fp, $ex->getTraceAsString()."\n");
			fwrite($fp, $ex->getMessage()."\n===\n");
			fclose($fp);
		}
	}

    public function text2img($text, $fontsize = 11, $color = '#242424', $bg = '#FFFFFF')
    {
        // Create image
        $strLenth = strlen($text) * 11;

        $color = $this->hex2rgb($color);
        $bg = $this->hex2rgb($bg);

        $im = imagecreate($strLenth, 20);
        imagecolorallocatealpha($im,$bg[0],$bg[1],$bg[2],100);
        $color = imagecolorallocate($im,$color[0],$color[1],$color[2]);
        imagettftext($im,$fontsize, 0, 1, mt_rand(14,18), $color, "ext/fonts/Play-Regular.ttf", $text);

        ob_start();
        imagepng($im);
        $img = ob_get_contents();
        ob_end_clean();

        imagedestroy($im);

        return $img;
    }


    public function hex2rgb($hex)
    {
        $hex = str_replace("#", "", $hex);

        if(strlen($hex) == 3) {
            $r = hexdec(substr($hex,0,1).substr($hex,0,1));
            $g = hexdec(substr($hex,1,1).substr($hex,1,1));
            $b = hexdec(substr($hex,2,1).substr($hex,2,1));
        } else {
            $r = hexdec(substr($hex,0,2));
            $g = hexdec(substr($hex,2,2));
            $b = hexdec(substr($hex,4,2));
        }
        $rgb = array($r, $g, $b);
        //return implode(",", $rgb); // returns the rgb values separated by commas
        return $rgb; // returns an array with the rgb values
    }
}
