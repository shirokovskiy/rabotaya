<?php
/**
 * Authorization Class
 *
 * Shirokovskiy Dmitry edition (shirokovskij@mail.ru)
 * 2006 (c) WBM
 */
class wbmAuth {
    public $db;
    public $_db_tables;
    public $strLoginAuth;
    public $uPassw;

    function __construct() {
        $this->db = &$GLOBALS['objDb'];
        $this->_db_tables = &$GLOBALS['_db_tables'];
    }

    function x_authUser($id = 0) {
        if ($id > 0) {
            $this->uID = $id;
        } else {
            if ($_SESSION['uAuthInfo']['uID'] > 0) {
                $this->uID = $_SESSION['uAuthInfo']['uID'];
                setcookie("signInStatus", $_SESSION["strLoginCode"], strtotime("+1 HOUR"));
            }
        }
    }

    /**
     * Authorization of user
     *
     * @param unknown_type $redirect
     * @return unknown
     */
    function wbmAuthLogin($redirect = true) {
        unset($_SESSION["uAuthInfo"]);
        $this->strLoginAuth = strip_tags(addslashes(trim($_POST['strLoginAuth'])));
        $this->strPasswAuth = strip_tags(addslashes(trim($_POST['strPasswAuth'])));

        $code = (!empty($_COOKIE["signInStatus"]) && strlen($_COOKIE["signInStatus"]) == 32) ? $_COOKIE["signInStatus"] : ((!empty($_SESSION["strLoginCode"])) ? $_SESSION["strLoginCode"] : md5(date("login %H:%i %d.%m.%Y")));

        $infUserLoginStatus = $this->wbmLoadUser(0, $this->strLoginAuth, $this->strPasswAuth );

        if (is_array($infUserLoginStatus) && $this->uID > 0 ) {
            $_SESSION['signInStatus'] = true;

            setcookie("signInStatus", (empty($_SESSION["strLoginCode"]) ? $_SESSION["strLoginCode"] : $code ), strtotime("+1 hour"));

            $_SESSION["strLoginCode"] = $code;
            $_SESSION['uAuthInfo'] = $infUserLoginStatus;

            $this->db->query("UPDATE site_users SET su_last_visit = NOW(), su_ip = INET_ATON('".$_SERVER['REMOTE_ADDR']."') WHERE su_id='".$this->uID."'");

            return true;
        }

        return false;
    }

    /**
     * To get user authorization info
     *
     * @param unknown_type $id
     * @param unknown_type $login
     * @param unknown_type $passwd
     * @return unknown
     */
    function wbmLoadUser ( $id, $login='', $passwd='' ) {
        if ( intval($id) < 1 && empty($login) ) {
            return false;
        } else {
            if ( $id > 0 ) {
                $strStatement = " su_id = '$id'";
            } elseif ( !empty($login) && !empty($passwd) ) {
                $strStatement = " su_login = '$login' AND su_passw = MD5('$passwd') AND su_status = 'Y'";
            } else {
                return false;
            }

            $strSqlQuery = "SELECT su_id AS uID, su_date_add AS uDateReg"
                .", su_fname AS uFName, su_lname AS uLName, su_mname AS uMName, su_company AS uCName"
                .", su_login AS uLogin, su_email AS uEmail, su_phone AS uPhone"
                .", su_summ AS uSumm, su_date_untill AS uDateLimit, IF (su_summ > 0 AND NOW() < su_date_untill, 1, 0) AS uAccess"
                .", INET_NTOA(su_ip) AS lastIP"
                .", DATE_FORMAT(su_last_visit, '%H:%i %d.%m.%Y') AS lastVisit"
                ." FROM site_users WHERE $strStatement";
            $arrUserExistInfo = $this->db->fetch( $strSqlQuery );

            if ($arrUserExistInfo['uID'] < 1) return false;

            $this->uID = $arrUserExistInfo['uID'];                               // ID клиента

            return $arrUserExistInfo;
        }

        return false;
    }

    // ***  Проверка существования логина
    //      первый параметр - логин, второй - ид пользователя, если есть, для исключения из проверки
    function authLoginExistsCheck($login, $id = 0) {
        return $this->db->fetch( "SELECT COUNT(su_id) FROM site_users WHERE su_login = '".$login."'".(($id > 0) ? " and su_id <> ".$id : ""), 0) > 0;
    }


    function wbmLogout() {
        unset($_SESSION['uAuthInfo']);
        unset($_SESSION['signInStatus']);
        unset($_SESSION["strLoginCode"]);
    }

}
