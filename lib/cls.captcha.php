<?php
/** >***************************************************************************\
 ** Shirokovskiy D.2006 Jimmy™. Mon Nov 13 10:38:54 MSK 2006
 *
 * Класс реализующий метод CAPTCHA
 *
 * Completely Automated Public Turing test to Tell Computers and Humans Apart.
 * В переводе на русский: "Полностью автоматический публичный тест Тьюринга,
 * который может отличить человека от компьютера"
 **
\*******************************************************************************/

class clsCaptcha {
    protected $iWidth = 80;
    protected $iHeight = 20;
    protected $iColorRed = 255;
    protected $iColorGreen = 255;
    protected $iColorBlue = 255;
    protected $iLength = 4;
    private $codeName = 'strCptCode';

    public function __construct( $captchaCode = '', $set_sess = true ) {
        if ( $set_sess ) {
            $lcode = md5(uniqid(rand(), 1)); // 32 symbols

            if (!empty($captchaCode)) {
                $this->codeName = $captchaCode;
            }

            // убираем проблемные символы
            $lcode = str_replace("1", "", $lcode); // выглядит как l
            $lcode = str_replace("0", "", $lcode); // по форме напоминает О
            $lcode = str_replace("O", "", $lcode); // по форме напоминает О
            $lcode = str_replace("D", "", $lcode); // по форме напоминает О

            $scode = strtoupper(substr($lcode,0,4));
            $_SESSION[$this->codeName] = $scode; // вот те как ;) замудрил
        }
    }

    /**
     * OUTPUTs Image with secure code
     *
     * It always must DIE!
     */
    function imageCreateShow () {
        $img_head = @imagecreate ($this->iWidth, $this->iHeight) or die("Cannot initialize new GD image stream!");
        $bg = imagecolorallocate ($img_head, $this->iColorRed, $this->iColorGreen, $this->iColorBlue);
        $secureCode = $_SESSION[$this->codeName];

        if ( empty($secureCode) ) {
            die();
        }

        //создаём шум на фоне
        for ($i=0; $i<=128; $i++) {
            $cfgPixelColor = imagecolorallocate ($img_head, rand(0,255), rand(0,255), rand(0,255)); //задаём цвет
            imagesetpixel($img_head, rand(2,$this->iWidth), rand(2,$this->iHeight), $cfgPixelColor); //рисуем шумовой пиксель
        }

        //выводим символы кода
        for ($i = 0; $i < strlen($secureCode); $i++) {
            $cfgSymbolColor = imagecolorallocate ($img_head, rand(0,255), rand(0,128), rand(0,255)); //задаём цвет
            $x = rand(1, 10) + $i * $this->iHeight; // расположение символа
            $y = rand(1, intval($this->iHeight/4));
            imagechar ($img_head, 5, $x, $y, $secureCode[$i], $cfgSymbolColor);
        }

        //антикеширование
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        //создание рисунка в зависимости от доступного формата
        if (function_exists("imagepng")) {
            header("Content-type: image/png");
            imagepng($img_head);
        } elseif (function_exists("imagegif")) {
            header("Content-type: image/gif");
            imagegif($img_head);
        } elseif (function_exists("imagejpeg")) {
            header("Content-type: image/jpeg");
            imagejpeg($img_head);
        } else {
            die("No image support in this PHP server!");
        }
        imagedestroy ($img_head);
        die();
    }
}
