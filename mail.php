<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 8/6/13
 * Time         : 1:05 AM
 * Description  : run in CLI
 */
include_once "cfg/web.cfg.php";

if ( !is_object( $objMail ) ) {
    include_once( "cls.mail.php" );
    $objMail = new clsMail();
}

$sendTo = 'jimmy.webstudio@gmail.com';
$sendFrom = SITE_ADMIN_MAIL;
$sendSubject = "Rabota-Ya.ru | Новый шаблон";

$sendText = 'Ваш браузер не поддерживает HTML-письма. Очень печально =(';

$sendMessage = "<body style='width:100%'><div style='margin:0 auto; width: 60%; min-height: 300px; background-color: #8dc0f5; padding: 20px'>\n";
$sendMessage .= "<h1 style='color: darkblue'>Привет чувак или чувиха!</h1>"."\n";
$sendMessage .= "<img src='image.jpg' />\n";
$sendMessage .= "<p>Пробный текст письма!</p>\n";
$sendMessage .= "</body>";

$objMail->new_mail( $sendFrom, $sendTo, $sendSubject, $sendText, $sendMessage );
$objMail->add_attachment('image.jpg', 'image.jpg');
if ( !$objMail->send() ) {
    echo "Письмо не отправлено по техническим причинам!\nПопробуйте через пару минут.\nПриносим свои извинения!\n\n";
}

echo "Завершено!\n";
