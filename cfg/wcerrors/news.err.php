<?php
// **** Обработка ошибок раздела "НОВОСТИ"
$errMsg = array_merge($errMsg, array (
  	'msgEmptyTitle'    =>  "Заголовок Новости пустой"
  	,'msgEmptyNewsbody' =>  "Тело Новости пустое"
  	,'msgFailDate'      =>  "Формат даты неправильный"
  )
);
