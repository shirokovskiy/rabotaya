<?php
if (is_array($errMsg) && !empty($errMsg)) {
    $tmpErrMsg = $errMsg;
}
// **** Обработка ошибок раздела "Спамер"
$errMsg = array (
  'msgAccessOk'     =>  "Доступ успешно установлен"
  ,'msgRecordExist' =>  "Подписчик с таким Email уже существует"
  ,'msgNoUserData'  =>  "Нет данных о подписчике. Возможно что-то пошло не так."
);

if (is_array($tmpErrMsg) && !empty($tmpErrMsg)) {
    $errMsg = array_merge($tmpErrMsg, $errMsg);
}
