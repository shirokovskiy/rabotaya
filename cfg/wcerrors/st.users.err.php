<?php
// **** Обработка ошибок раздела "Пользователи сайта"
$errMsg = array_merge($errMsg, array (
    'msgAccessOk'     =>  "Доступ успешно установлен"
    , 'msgEmptyLName' =>  "Поле <b>Фамилия</b> пустое"
    , 'msgEmptyFName' =>  "Поле <b>Имя</b> пустое"
    , 'msgEmptyEmail' =>  "Поле <b>Email</b> пустое или Email написан неправильно"
    , 'msgEmptyLogin' =>  "Поле <b>Логин</b> пустое или содержит меньше 4 символов"
    , 'msgEmptyPassw' =>  "Поле <b>Пароль</b> пустое или содержит меньше 6 символов"
  )
);
