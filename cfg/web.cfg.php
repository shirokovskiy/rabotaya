<?php
// **** Установка локалей ******************************************************
setlocale(LC_ALL, "ru_RU.utf-8"); // en_US.utf-8 | "de_DE"
define('METACHARSET', "utf-8");
date_default_timezone_set('Europe/Moscow');

if(!isset($_SERVER['_IS_DEVELOPER_MODE'])) {
    $_SERVER['_IS_DEVELOPER_MODE'] = 0;

    if(php_sapi_name() == 'cli') {
#        $_SERVER['_IS_DEVELOPER_MODE'] = 1;
    }
}
// **** Уровень ошибок *********************************************************
if ($_SERVER['_IS_DEVELOPER_MODE']==1 || (isset($_SERVER['REMOTE_ADDR']) && in_array($_SERVER['REMOTE_ADDR'], array('188.134.80.143')))) {
//    error_reporting(E_ALL ^ E_NOTICE);
    error_reporting(E_ERROR | E_WARNING | E_PARSE);
//    error_reporting(E_ALL);
    ini_set("display_errors", "On");
} else {
    error_reporting(E_ERROR | E_WARNING);
}

// ******  Конфигурация сайта (изменяемо) **************************************
if (!defined("DROOT")) {
    $rootPath = (isset($_SERVER['DOCUMENT_ROOT']) && !empty($_SERVER['DOCUMENT_ROOT']) ? $_SERVER['DOCUMENT_ROOT'] : dirname(dirname(__FILE__)));
    define('DROOT', $rootPath.(preg_match("/.*\/$/", $rootPath) ? "" : "/") );
}
// ******

// Проект
define('SITE_ID_PROJECT', 1);
if (isset($_SERVER['HTTP_HOST'])) {
    define('SITE_URL', "http://".$_SERVER['HTTP_HOST']."/");
}

define('PROJECT_NAME', "Rabota-Ya.ru");
define('PROJECT_EMAIL', "info@rabota-ya.ru");
// ***** НИЖЕ НЕ РЕДАКТИРОВАТЬ *************************************************
define('PRJ_STORAGE', DROOT.'storage/');
define('PRJ_SITEIMG', PRJ_STORAGE.'siteimg/');
define('PRJ_FILES', PRJ_STORAGE.'files/');
define('PRJ_IMAGES', PRJ_FILES.'images/');
define('PRJ_LOGS', DROOT.'logs/');

define('SITE_CONF_DIR', DROOT."cfg/");
define('SITE_LIB_DIR', DROOT."lib/");

include_once(SITE_CONF_DIR."domain.cfg.php");

set_include_path(get_include_path()
    .PATH_SEPARATOR.SITE_LIB_DIR
    .PATH_SEPARATOR.SITE_LIB_DIR.'models/'
);

function pws_autoloader($class) {
    include_once $class. '.php';
}

spl_autoload_register('pws_autoloader');

define('REL_PHP_PATH', "site/controllers/");
define('REL_TPL_PATH', "site/templates/");

define('SITE_PHP_DIR', DROOT.REL_PHP_PATH);
define('SITE_TPL_DIR', DROOT.REL_TPL_PATH);

// PHP-обработчики
define('SITE_PHP_FRG_DIR',  SITE_PHP_DIR."fragments/");
define('SITE_PHP_PAGE_DIR', SITE_PHP_DIR."pages/");
// Шаблоны
define('SITE_TPL_TMP_DIR',  SITE_TPL_DIR."tmp/"); // папка-буфер (кеш)
define('SITE_TPL_TPL_DIR',  SITE_TPL_DIR."tpl/");
define('SITE_TPL_PAGE_DIR', SITE_TPL_DIR."pages/");
define('SITE_TPL_FRG_DIR',  SITE_TPL_DIR."fragments/");

define('SITE_ADMIN_MAIL', "info@phpwebstudio.com");
define('SITE_ROBOT_MAIL', "robot@phpwebstudio.com");

define('SITE_FILE_TMP', PRJ_FILES."__tmp/");
/* * * * */

// **** Конфигурация CMS (изменяемо) *******************************************
define('CMS_FOLDER', "cms"); // Папка CMS
define('CMS_URL', SITE_URL.CMS_FOLDER."/"); // Полный URL для CMS

define('ADMTPL', DROOT.CMS_FOLDER."/views/");
define('ADMTPLGL', ADMTPL."global/");
define('ADMTPLFRG', ADMTPL."fragments/");
define('ADMPHP', DROOT.CMS_FOLDER."/controllers/");
define('ADMPHPGL', ADMPHP."global/");
define('ADMERR', SITE_CONF_DIR."wcerrors/");

define('DEBUG_MAIL', "info@phpwebstudio.com");

define( 'GEOIP_DAT', SITE_LIB_DIR.'GeoData/GeoIP.dat' );
define( 'GEOIPCITY_DAT', SITE_LIB_DIR.'GeoData/GeoIPCity.dat' );

$arrTplVars['cfgAdminUrl']  = CMS_URL;
$arrTplVars['cfgAdminImg']  = CMS_URL."img/";
$arrTplVars['strAdminCss']  = CMS_URL."css/";
$arrTplVars['strAdminJs']  = CMS_URL."js/";
// **** END:Конфигурация CMS ***************************************************

$arrTplVars['cfgUrlSite'] = $arrTplVars['cfgSiteUrl'] = SITE_URL;
$arrTplVars['cfgUrlLink'] = "<a href='".SITE_URL."'>".SITE_URL."</a>";
$arrTplVars['cfgAdminMail'] = SITE_ADMIN_MAIL;
$arrTplVars['cfgSiteCurrentUri'] = SITE_URL.substr($_SERVER['REQUEST_URI'], 1).(preg_match('/\?/', $_SERVER['REQUEST_URI'])?'&':'?');
$arrTplVars['cfgCurrentUri'] = substr($_SERVER['REQUEST_URI'], 1);
$arrTplVars['cfgCurrentUri'] = !empty($arrTplVars['cfgCurrentUri']) ? (preg_match('/\?/', $_SERVER['REQUEST_URI'])?'&':'?') : '';

/**
 * Объявления переменных каталогов с изображениями
 */
$arrTplVars['cfgSiteImg'] = SITE_URL."storage/siteimg/";
$arrTplVars['cfgSiteFiles'] = SITE_URL."storage/files/";
$arrTplVars['cfgAllImg'] = $arrTplVars['cfgSiteFiles']."images/";

$arrTplVars['METACHARSET'] = METACHARSET;

// ***** Подключение библиотеки классов ****************************************
include_once(SITE_CONF_DIR."db.cfg.php");

include_once(SITE_LIB_DIR."cls.utils.php");
include_once(SITE_LIB_DIR."cls.db.php");
include_once(SITE_LIB_DIR."cls.tpl.php");
include_once(SITE_LIB_DIR."cls.error.php");
include_once(SITE_LIB_DIR."cls.mail.php");
include_once(SITE_LIB_DIR."cls.file.php");
include_once(SITE_LIB_DIR."cls.log.php");
include_once(SITE_LIB_DIR."cls.paginator.php");
include_once(SITE_LIB_DIR."cls.global.auth.php");
include_once(SITE_LIB_DIR."cls.captcha.php");
include_once(SITE_LIB_DIR."cls.images.php");
include_once(DROOT."funcs.php");
include_once(SITE_LIB_DIR."spaw2/class/config.class.php");

session_name("phpWebStudioCMS");
session_start();

$objDb    = new Db($_db_config);
$objUtils = new Utils();
// -----------------------------------
$objUtils->start();
// -----------------------------------
//$objDb->setDebugModeOn();
$strUsAgent = strtolower($_SERVER['HTTP_USER_AGENT']);

if (strstr($strUsAgent, 'opera')) $arrTplVars['strUserAgent'] = 'op';
elseif (strstr($strUsAgent, 'firefox')) $arrTplVars['strUserAgent'] = 'ff';
elseif (strstr($strUsAgent, 'netscape')) $arrTplVars['strUserAgent'] = 'nn';
elseif (strstr($strUsAgent, 'msie')) $arrTplVars['strUserAgent'] = 'ie';
else $arrTplVars['strUserAgent'] = 'ie';

$arrIf['production'] = !in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1'));

