<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 1/3/14
 * Time         : 5:04 PM
 * Description  : Get currency & weather
 * Run it in browser!!!
 *
 * Currency - http://www.cbr.ru/scripts/XML_daily.asp
 * Weather  - SPb http://informer.gismeteo.ru/xml/26063_1.xml
 *          - Msk http://informer.gismeteo.ru/xml/27612_1.xml
 */
$_SERVER['DOCUMENT_ROOT'] = dirname(__DIR__);

include_once $_SERVER['DOCUMENT_ROOT']."/cfg/web.cfg.php";
include_once "models/Informer.php";

/**
 * Я запускаюсь
 * и проверяю есть ли за сегодня инфо
 */
$objInformer = new Informer();

try {
    libxml_use_internal_errors(true);

    # Постараемся заполнить пробел по СПб
    $xmlstr = getUrlContents('http://informer.gismeteo.ru/xml/26063_1.xml');

    $xml = simplexml_load_string($xmlstr);
    if (!$xml) {
        trace_log('Error loading XML from '.$uri);
        foreach(libxml_get_errors() as $error) {
            trace_log( $error->message );
        }
    } else {
        /**
         * Parse custom XML
         *
         * $xml is object
         */
        if (isset($xml->REPORT->TOWN->FORECAST) && is_object($xml->REPORT->TOWN->FORECAST)) {
            foreach($xml->xpath('//FORECAST') as $forecast) {
                /**
                 * $forecast has @attributes
                 * day,month,year & etc.
                 */
                if ($forecast['day'].'.'.$forecast['month'].'.'.$forecast['year'] == date('d.m.Y')) {
                    if (isset($forecast->TEMPERATURE)) {
                        foreach($forecast->TEMPERATURE->attributes() as $name => $value) {
                            switch ($name) {
                                case 'min':
                                    $objInformer->updateInfo('w_min', $value);
                                    break;
                                case 'max':
                                    $objInformer->updateInfo('w_max', $value);
                                    break;
                                default:
                                    break;
                            }
                        }
                    } else {
                        trace_log('Error XML: unknown $forecast->TEMPERATURE: '.$forecast->saveXML());
                    }
                } else {
                    trace_log('Error XML: unknown forecast attributes of date (day,month,year): '.$forecast->saveXML().' +++ '.print_r($forecast, true));
                }
            }
        } else {
            trace_log('XML error: unknown $xml->REPORT->TOWN->FORECAST');
        }
    }


    $EUR = $objInformer->isInfoByDate('eur');

    if (empty($EUR)) {
        # Постараемся заполнить пробел по СПб
        $xmlstr = getUrlContents('http://www.cbr.ru/scripts/XML_daily.asp');

        $xml = simplexml_load_string($xmlstr);
        if (!$xml) {
            trace_log('Error loading XML from '.$uri);
            foreach(libxml_get_errors() as $error) {
                trace_log( $error->message );
            }
        } else {
            /**
             * Parse custom XML
             *
             * $xml is object
             */
            if (isset($xml['Date']) && !empty($xml['Date'])) {
                // date format in XML dd.mm.yyyy
                foreach($xml->xpath('//Valute') as $valute) {
                    if (isset($valute['ID']) && !empty($valute->Value))
                    {   if ($valute['ID'] == 'R01239' && $valute->CharCode == 'EUR')
                        {   if ($xml['Date'] == date('d.m.Y')) {
                                $objInformer->saveInfo('eur', str_replace(',','.', $valute->Value));
                            } else {
                                $EUR = $objInformer->isInfoByDate('eur', date('Y-m-d', strtotime($xml['Date'])));
                                if (empty($EUR))
                                {
                                    $objInformer->saveInfo('eur', str_replace(',','.', $valute->Value), date('Y-m-d', strtotime($xml['Date'])));
                                }
                            }
                        }
                    }
                }
            } else {
                trace_log('Error XML: currency Date unknown!');
            }
        }
    }


    $USD = $objInformer->isInfoByDate('usd');

    if (empty($USD)) {
        # Постараемся заполнить пробел по СПб
        $xmlstr = getUrlContents('http://www.cbr.ru/scripts/XML_daily.asp');

        $xml = simplexml_load_string($xmlstr);
        if (!$xml)
        {
            trace_log('Error loading XML from '.$uri);
            foreach(libxml_get_errors() as $error) {
                trace_log( $error->message );
            }
        } else {
            /**
             * Parse custom XML
             *
             * $xml is object
             */
            if (isset($xml['Date']) && !empty($xml['Date']))
            {   // date format in XML dd.mm.yyyy
                foreach($xml->xpath('//Valute') as $valute) {
                    if (isset($valute['ID']) && !empty($valute->Value))
                    {   if ($valute['ID'] == 'R01235' && $valute->CharCode == 'USD')
                        {   if ($xml['Date'] == date('d.m.Y')) {
                                $objInformer->saveInfo('usd', str_replace(',','.', $valute->Value));
                            } else {
                                $USD = $objInformer->isInfoByDate('usd', date('Y-m-d', strtotime($xml['Date'])));
                                if (empty($USD))
                                {
                                    $objInformer->saveInfo('usd', str_replace(',','.', $valute->Value), date('Y-m-d', strtotime($xml['Date'])));
                                }
                            }
                        }
                    }
                }
            } else {
                trace_log('Error XML: currency Date unknown!');
            }
        }
    }
} catch (Exception $ex) {
    trace_log($ex);
}






