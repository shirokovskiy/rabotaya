<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 10/14/13
 * Time         : 11:22 AM
 * Description  : Send newsletters
 */
$_SERVER['DOCUMENT_ROOT'] = dirname(__DIR__);

include_once $_SERVER['DOCUMENT_ROOT']."/cfg/web.cfg.php";
include_once "models/Spamer.php";
$objSpamer = new Spamer();
$intCountSubscribers = 32;
$isBccCopySent = false;
$msgInText = $msgInHtml = '';
$isDebugMode = false;

/**
 * Проверить запущенные рассылки
 * За один запуск спамера берём только одну рассылку и только указанное кол-во адресатов
 */
$arrCurrentJob = $objSpamer->getPriorityProcess();
$debugLogName = "subscribers.".date('Y.m.d_H').".log";

if (is_array($arrCurrentJob) && !empty($arrCurrentJob)) {
    if($isDebugMode) trace_log('Инфо о текущем задании: $arrCurrentJob '.print_r($arrCurrentJob, true), $debugLogName);
}


if (is_array($arrCurrentJob) && !empty($arrCurrentJob)) {
    // есть работа для рассылки, получим подписчиков
    $arrSubscribers = $objSpamer->getSubscribers($arrCurrentJob['sc_id'], $intCountSubscribers);

    if (is_array($arrSubscribers) && !empty($arrSubscribers)) {
        //trace_log('$arrSubscribers '.print_r($arrSubscribers, true), $debugLogName);
        if($isDebugMode) trace_log('Получены данные о подписчиках!', $debugLogName);
    }


    if (is_array($arrSubscribers) && !empty($arrSubscribers)) {
        if (!is_object($objMail)) {
            $objMail = new clsMail();
        }

        $sendFrom[] = "engeniya@rabota-ya.ru";
        $sendFrom[] = "Rabota-Ya.ru";

        $arrNewsletter = $objSpamer->getNewsletterProcessInfo($arrCurrentJob['sc_id']);// sc_group_id
        $strNewsletterSubject = $arrNewsletter['ste_title'];
        $msgInText = $strNewsletterText = $arrNewsletter['ste_body'];
        $msgInHtml = $strNewsletterHtml = $arrNewsletter['ste_html'];

        $msgSubject = "Работа от А до Я | ".$strNewsletterSubject;

        if (is_array($arrNewsletter) && !empty($arrNewsletter)) {
            if($isDebugMode) trace_log('Есть информация о письме $arrNewsletter '.print_r($arrNewsletter, true), $debugLogName);
        }

        foreach ($arrSubscribers as $k => $subscriber) {
            $arrUser = $objSpamer->getSubscriberEmailInfo($subscriber['ss_subscriber_id']);

            if (is_array($arrUser) && !empty($arrUser)) {
                if($isDebugMode) trace_log('Известен подписчик $arrUser '.print_r($arrUser, true), $debugLogName);
            }


            $sendTo = null;
            if (is_array($arrUser) && !empty($arrUser)) {
                # есть инфо о подписчике
                $sendTo = $arrUser['sus_email'];

                $msgInText  = str_replace('%SUBSCRIBER%', $arrUser['sus_name'], $strNewsletterText);
                $msgInHtml  = str_replace('%SUBSCRIBER%', $arrUser['sus_name'], $strNewsletterHtml);
            }

            $strAddOnsTxt = "\n\n\n"."Полезный совет: если Вам нравятся наши информационные письма, и если Вы не хотите потерять их в папке Спам,\nдобавьте наш информационный адрес info@rabota-ya.ru в Контактную книгу Вашего почтового ящика.\n\nС уважением,\nРабота-Я.ру";
            $strAddOnsHtml = '<p style="margin: 50px 0 10px">Полезный совет: <i>если Вам нравятся наши информационные письма, и если Вы не хотите потерять их в папке Спам,<br>добавьте наш информационный адрес info@rabota-ya.ru в Контактную книгу Вашего почтового ящика.</i><br><br>С уважением,<br>Работа-Я.ру</p>';

            $linkUnsubscribe = SITE_URL.'unsubscribe/email/'.urlencode($sendTo).'/'.md5( md5($sendTo) . $arrUser['sus_id'] );
            $strAddOnsTxt .= "\n\n\n"."Чтобы отписаться от рассылки кликните на ссылку: ".$linkUnsubscribe;
            $strAddOnsHtml .= '<p style="margin: 50px 0 10px">Чтобы отписаться от рассылки кликните на ссылку: <a target=_blank href="'.$linkUnsubscribe.'">'.$linkUnsubscribe.'</a></p>';

            if (!is_null($sendTo) && $objUtils->checkEmail($sendTo)) {
                $objMail->new_mail( $sendFrom, $sendTo, $msgSubject, $msgInText.$strAddOnsTxt, $msgInHtml.$strAddOnsHtml );
//                if ($sendTo != "jimmy.webstudio@gmail.com" && $isBccCopySent == false) {
//                    $objMail->add_bcc( "jimmy.webstudio@gmail.com" );
//                    $isBccCopySent = true;
//                }
                if ( $objMail->send() !== true ) {
                    // какая то ошибка
                    // пометить данный email как не отправленный
                    trace_log('Письмо не отправлено по непонятным причинам на '.$sendTo, $debugLogName);
                    $objSpamer->markSubscriberInQueue($arrUser['sus_id'], $arrCurrentJob['sc_id']);
                } else {
                    // письмо отправлено
                    // удалить его из очереди
                    if (!$objSpamer->removeSubscriberEmailFromQueue($arrUser['sus_id'], $arrCurrentJob['sc_id'])) {
                        trace_log('Очередь не очищается - это очень плохо!', $debugLogName);
                    } else {
                        if($isDebugMode) trace_log('Письмо ушло в очередь отправки!', $debugLogName);
                    }
                }
            } else {
                trace_log('Email получателя не указан или не верен: '.$sendTo.' |', $debugLogName);
            }
        }
    } else {
        /**
         * В случае когда процесс есть а подписчиков нет,
         * необходимо закрыть рассылку.
         */
        $objSpamer->stopProcess($arrCurrentJob['sc_id']);
        trace_log('Email рассылка остановлена!', $debugLogName);
    }
}
