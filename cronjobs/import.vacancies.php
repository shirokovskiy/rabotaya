<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 12/3/13
 * Time         : 8:56 PM
 * Description  : скрипт импорта вакансий из 1С на сайт.
 * Запускается каждый час.
 * Файл импорта появляется каждые 20 минут.
 *
 * CLI: php ./cronjobs/import.vacancies.php
 */
include_once dirname(__DIR__) .'/cfg/web.cfg.php';
include_once "models/Company.php";
include_once "models/Vacancy.php";

$file = 'import_products.csv';
$path = PRJ_STORAGE.'ftp1C/';
$filename = $path.$file;
$logfile = 'import.vacancies.'.date('Y.m.d').'.log';

$objVacancy = new Vacancy();
$objCompany = new Company();
$objSiteUser = new SiteUser();
$objCategory = new JobCategory();

$arrSqlFields[] = 'jv_title';
$arrSqlFields[] = 'jv_description';
$arrSqlFields[] = 'jv_phone';
$arrSqlFields[] = ''; // принадлежность газете
$arrSqlFields[] = ''; // адрес компании
$arrSqlFields[] = ''; // название компании
$arrSqlFields[] = 'jv_email';
$arrSqlFields[] = 'jv_contact_fio';
$arrSqlFields[] = 'jv_city';
$arrSqlFields[] = 'jv_salary';
$arrSqlFields[] = 'jv_date_begin';
$arrSqlFields[] = 'jv_erp_id';
$arrSqlFields[] = 'jv_date_end';
$arrSqlFields[] = 'rubricator'; // Рубрикатор

$arrSqlComp[5] = 'jc_title';
$arrSqlComp[4] = 'jc_address';

$isDebugMode = false;

if (file_exists($filename)) {
    $file = file($filename);
    unset($file[0]);
    foreach ($file as $k => $row) {
        $row_data = str_getcsv($row, ',', '"');

//        print_r($row_data);
//        die;

        $cols = count($row_data);
        if ($cols > 0 && !empty($row_data[0])) {
            $arrSqlQuery = $arrSqlCompQuery = array();
            $category_parent_id = $subcategory_id = 0;
            for ($c=0; $c < $cols; $c++) {
                if(isset($arrSqlFields[$c])) {
                    $data_string = trim($row_data[$c]);
                    if (!empty($data_string)) {
                        if(!empty($arrSqlFields[$c])) {
                            if (preg_match('/[\d]{2}\.[\d]{2}\.[\d]{4} 0:00:00/', $data_string)) {
                                $data_string = str_replace(' 0:00:00', '', $data_string);
                                $data_string = date('Y-m-d', strtotime($data_string));
                            }

                            if ($arrSqlFields[$c] == 'jv_city') {
                                // данные по городу
                                $cityId = $objSiteUser->getCityId($data_string);
                                if (empty($cityId)) {
                                    $cityId = $objSiteUser->saveCity($data_string);
                                }
                                if (!empty($cityId)) {
                                    $arrSqlQuery[$c] = "`jv_city_id` = ".$cityId;
                                }
                            } elseif ($arrSqlFields[$c] == 'rubricator') {
                                // строим рубрикатор
                                $category_parent_id = $objCategory->save(mysql_real_escape_string($data_string));
                                $subcategory_name = trim($row_data[0]);
                                if (!empty($subcategory_name) && $category_parent_id > 0) {
                                    $subcategory_id = $objCategory->save($subcategory_name, $category_parent_id);
                                }
                            } else {
                                // данные по вакансии
                                $arrSqlQuery[$c] = "`".$arrSqlFields[$c]."` = '".mysql_real_escape_string($data_string)."'";
                            }

                        } else {
                            // данные по компании
                            if (isset($arrSqlComp[$c])) {
                                $arrSqlCompQuery[$c] = "`".$arrSqlComp[$c]."` = '".mysql_real_escape_string($data_string)."'";
                            }
                        }
                    }
                }
//                echo "|". $data_string . "|\n";
            }

            if (is_array($arrSqlQuery) && !empty($arrSqlQuery)) {
                // Добавляем вакансию
                $strSqlQuery = "INSERT INTO `job_vacancies` SET ".implode(',',$arrSqlQuery)
                    .", jv_date_publish = UNIX_TIMESTAMP()";
                if (!$objDb->query($strSqlQuery, false)) {
                    # sql error
                    if ($isDebugMode) trace_log('Bad SQL query '.$strSqlQuery, $logfile);
                } else {
                    $intRecordId = $objDb->insert_id();
                    if ( $intRecordId > 0 && is_array($arrSqlCompQuery) && !empty($arrSqlCompQuery)) {
                        // Добавляем компанию
                        $strSqlQuery = "INSERT INTO job_companies SET ".implode(',',$arrSqlCompQuery)
                            .", jc_date_create = UNIX_TIMESTAMP()";
                        if ( !$objDb->query( $strSqlQuery, false ) ) {
                            if ($isDebugMode) trace_log('Ошибка базы данных! Возможно такая Компания уже существует.'."\n".$strSqlQuery, $logfile);

                            $strSqlQuery = "SELECT jc_id FROM `job_companies` WHERE " . $arrSqlCompQuery[5]; // ищем по названию
                            $intCompanyId = $objDb->fetch($strSqlQuery, 'jc_id');
                            if ($intCompanyId) {
                                if ($isDebugMode) trace_log('Компания найдена! ID = '.$intCompanyId."\n".$strSqlQuery, $logfile);
                            }
                        } else {
                            $intCompanyId = $objDb->insert_id();
                        }

                        if ($intCompanyId > 0) {
                            $objVacancy->setId($intRecordId)->setCompanyId($intCompanyId);
                        }
                    }

                    if ( $intRecordId > 0 ) {
                        # Запишем к какому проекту относится вакансия
                        $intProjectId = ($row_data[3] == 'Работа от А до Я' ? 1 : ($row_data[3] == 'Все вакансии' ? 2 : null));
                        if ($intProjectId > 0) {
                            $strSqlQuery = "INSERT INTO `job_vacancies_project_lnk` SET"
                                . " `jvpl_cp_id` = ".$intProjectId
                                . ", `jvpl_jv_id` = ".$intRecordId;
                            if (!$objDb->query($strSqlQuery)) {
                                # sql error
                                if ($isDebugMode) trace_log('Bad request '.$strSqlQuery, $logfile);
                            }
                        }

                        # Запишем к какой Рубрике вакансия
                        if ($subcategory_id > 0 || $category_parent_id > 0) {
                            $strSqlQuery = "INSERT INTO `job_category_link` SET"
                                . " `jcl_sjc_id` = ".($subcategory_id?:$category_parent_id)
                                . ", `jcl_rel_id` = ".$intRecordId
                                . ", `jcl_type` = 'vacancy'";
                            if (!$objDb->query($strSqlQuery)) {
                                # sql error
                            }
                        }
                    }
                }
            }
        }
    }

    file_put_contents($path.'imported.'.date('Ymd-His').'.csv', $file);
    unlink($filename);
} else {
    trace_log("Файл импорта не существует!", $logfile);
}
//echo "\nThe End!\n";
