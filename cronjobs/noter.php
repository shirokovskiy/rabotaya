<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 7/5/14
 * Time         : 4:09 PM
 * Description  : Get all grabed resume and send invitation to approve CV or disable.
 *
 * SQL query to check all "answered" people
 * SELECT COUNT(`jr_id`) iC FROM `job_resumes` WHERE  `jr_invited`=2
 */

$_SERVER['DOCUMENT_ROOT'] = dirname(__DIR__);
$debugLogName = "noter.".date('Y.m.d_H').".log";

include_once $_SERVER['DOCUMENT_ROOT']."/cfg/web.cfg.php";
include_once "models/Resume.php";

$Resume = new Resume();

$exp = array(
    'jr_email' => 'IS NOT NULL'
    , 'jr_email' => "NOT LIKE ''"
    , 'jr_grab_src_alias' => 'IS NOT NULL'
    , 'jr_invited' => '= 0' // ALTER TABLE  `job_resumes` ADD  `jr_invited` INT UNSIGNED NULL DEFAULT NULL COMMENT  'приглашение актуализировать';
    , 'jr_city_id' => '= 1'
);

$arrResumes = $Resume->getList($exp); // порциями по 30, потому что в кронекаждые 2..5 минут например

//echo "Count of resume: ", count($arrResumes);
//die("\n" . __FILE__ . ":" . __LINE__ . "\n\n");

if (!is_array($arrResumes)) {
    $exp = array(
        'jr_email' => 'IS NOT NULL'
        , 'jr_email' => "NOT LIKE ''"
        , 'jr_grab_src_alias' => 'IS NOT NULL'
        , 'jr_invited' => '= 0'
        , 'jr_city_id' => 'IS NULL'
        , 'jr_city' => "LIKE 'Санкт-Петербург%'"
    );

    $arrResumes = $Resume->getList($exp);
}

$sendFrom[] = "engeniya@rabota-ya.ru";
$sendFrom[] = "Rabota-Ya.ru";
$msgSubject = "Работа от А до Я | Актуализировать резюме";

if (is_array($arrResumes) && !empty($arrResumes)) {
    if (!is_object($objMail)) {
        $objMail = new clsMail();
    }

//    echo "Count of resume: ", count($arrResumes);
//    die("\n" . __FILE__ . ":" . __LINE__ . "\n\n");

    foreach ($arrResumes as $key => $resume) {
        $sendTo = $resume['jr_email'];

        $random_num = rand(10,99);

        $link = $Resume->getLinkToEdit($resume['jr_id'], $random_num); // ALTER TABLE  `job_resumes` ADD  `jr_invite_hash` VARCHAR( 32 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT  'для ссылки';

//        echo "\n";
//        print_r($link);
//        die("\n\n" . __FILE__ . ':' . __LINE__ . "\n");

        if (is_null($link)) continue;

        $strLetterTxt = "Добрый день уважаемый соискатель!\n\n"
                        ."Мы приглашаем Вас актуализировать Ваше резюме на сайте Работа от А до Я.\n"
                        ."Чтобы отредактировать Ваше резюме, перейдите по ссылке $link \n\n"
                        ."На открывшейся странице Вам необходимо будет ввести указанную в письме цифру. \n\n"
                        ."С уважением,\n"
                        ."Работа-Я.ру, www.rabota-ya.ru";

        $strLetterHtml = '<p style="margin:0 0 10px">Добрый день уважаемый соискатель!</p>'
                        .'<p style="margin:0 0 10px">Мы приглашаем Вас актуализировать Ваше резюме на сайте Работа от А до Я.<br>'
                        .'Чтобы отредактировать Ваше резюме, перейдите по <a href="'.$link.'" target="_blank">ссылке '.$link.'</a><br><br><br>'
                        .'С уважением,<br>'
                        .'Работа-Я.ру<br>www.rabota-ya.ru</p>';

        if ($objUtils->isValidEmail($sendTo)) {
//            $sendTo = 'jimmy.webstudio@gmail.com'; // DEBUG
//            $sendTo = 'info@rabota-ya.ru'; // DEBUG

            if ($sendTo != "jimmy.webstudio@gmail.com") {
                $objMail->add_bcc("jimmy.webstudio@gmail.com");
            }

            $objMail->new_mail( $sendFrom, $sendTo, $msgSubject, $strLetterTxt, $strLetterHtml );

            if ( $objMail->send() !== true ) {
                // какая-то ошибка
                trace_log('Письмо не отправлено по непонятным причинам на '.$sendTo, $debugLogName);
            } else {
                $Resume->setIsInvited($resume['jr_id']);
            }
        } else {
            trace_log('Email получателя не указан или не верен: '.$sendTo.' |', $debugLogName);
        }

//        die("<hr /><p style='color:red'><b>" . __FILE__ . "</b>:<i>" . __LINE__ . "</i></p>");
    }
}
