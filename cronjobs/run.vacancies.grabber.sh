#!/bin/bash

ps x | grep \[V\]acancies > /dev/null

if [ $? -eq 0 ]; then
  echo "Process is running. $(date)"
else
  echo "Process is not running."
  echo "Start it. $(date)"
  php -f /var/www/data/rabota-ya.ru/htroot/grabbers/getVacanciesFromJobsMarket.php >> /var/www/data/rabota-ya.ru/htroot/logs/getVacancies.`date +\%Y.\%m.\%d`.log 2>&1
fi

