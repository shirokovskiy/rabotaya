<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 8/7/14
 * Time         : 3:40 PM
 * Description  : 
 */
include_once $_SERVER['DOCUMENT_ROOT']."/cfg/web.cfg.php";

$strSqlQuery = "SELECT COUNT(jr_id) iC FROM `job_resumes` WHERE `jr_grab_src_alias` IS NOT NULL AND (jr_city_id = 1 OR jr_city LIKE 'Санкт-Петербург%')";
$intC1 = $objDb->fetch($strSqlQuery, 'iC');

$strSqlQuery = "SELECT COUNT(jr_id) iC FROM `job_resumes` WHERE `jr_grab_src_alias` IS NOT NULL AND `jr_invited` = 1 AND `jr_invite_hash` NOT LIKE ''";
$intC2 = $objDb->fetch($strSqlQuery, 'iC');

$strSqlQuery = "SELECT COUNT(jr_id) iC FROM `job_resumes` WHERE `jr_grab_src_alias` IS NOT NULL AND `jr_invited` = 2 AND `jr_invite_hash` NOT LIKE ''";
$intC3 = $objDb->fetch($strSqlQuery, 'iC');

echo "Количество резюме в БД для активации: <b>", $intC1, "</b>";
echo '<br>';
echo "Количество резюме приглашённых: <b>", $intC2, "</b>";
echo '<br>';
echo "Количество среагировавших пользователей: <b>", $intC3, "</b>";
echo '<br>';

if (!is_object($objMail)) {
    $objMail = new clsMail();
}
$sendFrom = 'info@rabota-ya.ru';
$sendTo = 'dshirokovskiy@gmail.com';
//if ($sendTo != "jimmy.webstudio@gmail.com") {
//    $objMail->add_bcc("jimmy.webstudio@gmail.com");
//}

$objMail->new_mail( $sendFrom, $sendTo, 'Кто-то зашёл на статистику', $_SERVER['REMOTE_ADDR'] );

if ( $objMail->send() !== true ) {
    // какая-то ошибка
    echo "<!-- Cant send email -->";
}