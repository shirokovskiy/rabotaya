<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 7/31/13
 * Time         : 2:30 AM
 * Description  : for CLI only
 *
 *  Convert city dependecies for resumes and vacancies
 */
include_once('cfg/web.cfg.php');
$SiteUser = new SiteUser();

echo "\nLet's check resumes...\n";
$strSqlQuery = "SELECT jr_id,jr_city,jr_city_id FROM `job_resumes` WHERE jr_city_id IS NULL AND jr_city != '' ORDER BY jr_city";
$arrRecords = $objDb->fetchall($strSqlQuery);

$stat['changed'] =
$stat['empty'] =
$stat['new'] =
$stat['exist'] = 0;

if (is_array($arrRecords) && !empty($arrRecords)) {
    echo "Resumes in progress...\n";
    foreach ($arrRecords as $k => $record) {
        # взять название города, выяснить ID, записать в БД ID, с педварительными проверками что данные валидны
        if (empty($record['jr_city'])) {
            $stat['empty']++;
            // no city name to detect
            continue;
        }

        if (intval($record['jr_city_id'])) {
            $stat['exist']++;
            // is city ID, no reason to detect
            continue;
        }

        $cityID = $SiteUser->getCityId($record['jr_city']);

        if ($cityID <= 0) {
            // There is no city in database. Let's record.
            $cityID = $SiteUser->saveCity($record['jr_city']);
            if ($cityID > 0) $stat['new']++;
        }

        if ($cityID > 0) {
            $strSqlQuery = "UPDATE `job_resumes` SET"
                . " `jr_city` = NULL"                   // TODO: SQL: в БД исправить таблицу Резюме чтобы в этом поле мог быть NULL
                . ", `jr_city_id` = ".$cityID
                . " WHERE `jr_id` = ".$record['jr_id'];
            if (!$objDb->query($strSqlQuery)) {
                # sql error
                die($strSqlQuery."\n\n");
            } else {
                $stat['changed']++;
            }
        }
    }
}

echo "Successfully done!\n";
echo "-=== Resume statistic ===-\n";
echo "Changed cities: ".$stat['changed']."\n";
echo "Exist cities: ".$stat['exist']."\n";
echo "Empty cities: ".$stat['empty']."\n";
echo "New cities: ".$stat['new']."\n";
echo "\n";

echo "Let's check vacancies...\n";
$strSqlQuery = "SELECT jv_id,jv_city,jv_city_id FROM `job_vacancies` WHERE jv_city_id IS NULL AND jv_city != '' ORDER BY jv_city";
$arrRecords = $objDb->fetchall($strSqlQuery);

$stat['changed'] =
$stat['new'] =
$stat['empty'] =
$stat['exist'] = 0;

if (is_array($arrRecords) && !empty($arrRecords)) {
    echo "Vacancies in progress...\n";
    foreach ($arrRecords as $k => $record) {
        if (empty($record['jv_city'])) {
            $stat['empty']++;
            // no city name to detect
            continue;
        }

        if (intval($record['jv_city_id'])) {
            $stat['exist']++;
            // is city ID, no reason to detect
            continue;
        }

        $cityID = $SiteUser->getCityId($record['jv_city']);

        if ($cityID <= 0) {
            // There is no city in database. Let's record.
            $cityID = $SiteUser->saveCity($record['jv_city']);
            if ($cityID > 0) $stat['new']++;
        }

        if ($cityID > 0) {
            $strSqlQuery = "UPDATE `job_vacancies` SET"
                . " `jv_city` = NULL"
                . ", `jv_city_id` = ".$cityID
                . " WHERE `jv_id` = ".$record['jv_id'];
            if (!$objDb->query($strSqlQuery)) {
                # sql error
                die($strSqlQuery."\n\n");
            } else {
                $stat['changed']++;
            }
        }
    }
}

echo "Successfully done!\n";
echo "-=== Vacancies statistic ===-\n";
echo "Changed cities: ".$stat['changed']."\n";
echo "Exist cities: ".$stat['exist']."\n";
echo "Empty cities: ".$stat['empty']."\n";
echo "New cities: ".$stat['new']."\n";
echo "\n";


echo "Let's check companies...\n";
$strSqlQuery = "SELECT jc_id,jc_city,jc_city_id FROM `job_companies` WHERE jc_city_id IS NULL AND jc_city != '' ORDER BY jc_city";
$arrRecords = $objDb->fetchall($strSqlQuery);

$stat['changed'] =
$stat['new'] =
$stat['empty'] =
$stat['exist'] = 0;

if (is_array($arrRecords) && !empty($arrRecords)) {
    echo "Companies in progress...\n";
    foreach ($arrRecords as $k => $record) {
        if (empty($record['jc_city'])) {
            $stat['empty']++;
            // no city name to detect
            continue;
        }

        if (intval($record['jc_city_id'])) {
            $stat['exist']++;
            // is city ID, no reason to detect
            continue;
        }

        $cityID = $SiteUser->getCityId($record['jc_city']);

        if ($cityID <= 0) {
            // There is no city in database. Let's record.
            $cityID = $SiteUser->saveCity($record['jc_city']);
            if ($cityID > 0) $stat['new']++;
        }

        if ($cityID > 0) {
            $strSqlQuery = "UPDATE `job_companies` SET"
                . " `jc_city` = NULL"
                . ", `jc_city_id` = ".$cityID
                . " WHERE `jc_id` = ".$record['jc_id'];
            if (!$objDb->query($strSqlQuery)) {
                # sql error
                die($strSqlQuery."\n\n");
            } else {
                $stat['changed']++;
            }
        }
    }
}

echo "Successfully done!\n";
echo "-=== Companies statistic ===-\n";
echo "Changed cities: ".$stat['changed']."\n";
echo "Exist cities: ".$stat['exist']."\n";
echo "Empty cities: ".$stat['empty']."\n";
echo "New cities: ".$stat['new']."\n";
echo "\n";
