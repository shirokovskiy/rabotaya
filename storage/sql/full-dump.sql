-- MySQL dump 10.13  Distrib 5.5.29, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: rabotaya_web
-- ------------------------------------------------------
-- Server version	5.5.29-0ubuntu0.12.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cms_config`
--

DROP TABLE IF EXISTS `cms_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_config` (
  `cc_id` int(11) NOT NULL AUTO_INCREMENT,
  `cc_user_id` int(10) unsigned DEFAULT NULL,
  `cc_name` varchar(255) NOT NULL DEFAULT '',
  `cc_ver` varchar(5) NOT NULL DEFAULT '',
  `cc_first_color` varchar(6) NOT NULL DEFAULT '',
  `cc_second_color` varchar(6) NOT NULL DEFAULT '',
  `cc_third_color` varchar(6) NOT NULL DEFAULT '',
  `cc_fourth_color` varchar(6) NOT NULL DEFAULT '',
  `cc_bg_color` varchar(6) NOT NULL DEFAULT '',
  `cc_default` enum('N','Y') NOT NULL DEFAULT 'N',
  UNIQUE KEY `id` (`cc_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_config`
--

LOCK TABLES `cms_config` WRITE;
/*!40000 ALTER TABLE `cms_config` DISABLE KEYS */;
INSERT INTO `cms_config` VALUES (1,NULL,'Web Manager Building Machine','1.2','0E467F','34618F','EDF1F5','E3FEDE','FFFFFF','Y');
/*!40000 ALTER TABLE `cms_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_modules`
--

DROP TABLE IF EXISTS `cms_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_modules` (
  `cm_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cm_id_parent` int(10) unsigned DEFAULT NULL,
  `cm_insert_item` enum('Y','N') NOT NULL DEFAULT 'Y',
  `cm_name` varchar(50) NOT NULL DEFAULT '',
  `cm_link` varchar(50) NOT NULL DEFAULT '',
  `cm_link_atrib` varchar(50) DEFAULT NULL,
  `cm_type` enum('system','user') NOT NULL DEFAULT 'system',
  `cm_top_show` enum('N','Y') NOT NULL DEFAULT 'N',
  `cm_date_version` date NOT NULL DEFAULT '0000-00-00',
  `cm_hide` enum('N','Y') NOT NULL DEFAULT 'N',
  `cm_separator` enum('N','Y') NOT NULL DEFAULT 'N',
  `cm_order` int(10) unsigned NOT NULL DEFAULT '0',
  `cm_version` varchar(10) NOT NULL DEFAULT '',
  `cm_error_file_name` varchar(50) DEFAULT NULL,
  `cm_popup` enum('N','Y') NOT NULL DEFAULT 'N',
  `cm_status` enum('Y','N') NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`cm_id`),
  KEY `fm_id_parent` (`cm_id_parent`),
  KEY `cm_order` (`cm_order`)
) ENGINE=MyISAM AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Модули CMS';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_modules`
--

LOCK TABLES `cms_modules` WRITE;
/*!40000 ALTER TABLE `cms_modules` DISABLE KEYS */;
INSERT INTO `cms_modules` VALUES (1,NULL,'N','Проекты','project',NULL,'user','N','2005-07-14','N','Y',6,'v. 1.0','','N','Y'),(2,NULL,'N','Настройки CMS','settings',NULL,'user','Y','2005-07-12','N','N',26,'v. 2.0','','N','Y'),(3,2,'Y','Добавить модуль','module.form',NULL,'user','N','2005-10-14','Y','N',3,'v. 1.0','modules','Y','Y'),(4,2,'N','Доступ к модулю','module.access',NULL,'user','N','2006-09-04','Y','N',8,'v. 1.0','modules','Y','Y'),(5,2,'Y','Поиск модуля','module.search',NULL,'user','N','2005-10-27','N','N',5,'v. 2.0','','N','Y'),(6,NULL,'N','Шаблоны','templates',NULL,'user','Y','2005-07-12','N','N',3,'v. 2.0','templates','N','Y'),(7,6,'Y','Создать шаблон','templates.edit',NULL,'user','N','2005-07-14','N','N',1,'v. 2.0','templates','N','Y'),(8,6,'Y','Группы шаблонов','templates.group',NULL,'user','N','2005-07-14','N','N',3,'v. 1.0','templates','N','Y'),(9,NULL,'N','Страницы','pages',NULL,'user','Y','2005-07-12','N','N',20,'v. 2.1','pages','N','Y'),(10,9,'Y','Создать страницу','pages.edit',NULL,'user','N','2005-07-27','N','N',1,'v. 2.0','pages','N','Y'),(11,9,'Y','Поиск страницы','pages.search',NULL,'user','N','2005-07-27','N','N',3,'v. 2.0','pages','N','Y'),(12,9,'Y','Группы страниц','pages.group',NULL,'user','N','2005-07-27','N','N',5,'v. 3.0','pages','N','Y'),(13,NULL,'N','Изображения','images',NULL,'user','Y','2005-07-12','N','N',22,'v. 2.0','','N','Y'),(14,NULL,'N','Фрагменты','fragments',NULL,'user','Y','2005-07-12','N','N',24,'v. 2.0','fragments','N','Y'),(15,14,'Y','Создать фрагмент','fragments.edit',NULL,'user','N','2005-07-31','N','N',1,'v. 2.1','fragments','N','Y'),(16,14,'Y','Группы фрагментов','fragments.group',NULL,'user','N','2005-09-14','N','N',5,'1.0','','N','Y'),(17,14,'Y','Поиск фрагмента','fragments.search',NULL,'user','N','2005-11-19','N','N',3,'v. 2.0','','N','Y'),(18,NULL,'N','Пользователи','users',NULL,'user','Y','2005-07-12','N','N',29,'v. 2.0','users','N','Y'),(19,NULL,'Y','Главная страница пользователя','user.main',NULL,'user','N','2006-10-25','Y','N',1,'1.0','users','N','Y'),(20,18,'Y','Добавить пользователя','users.edit',NULL,'user','N','2005-08-31','N','N',1,'v. 1.0','users','N','Y'),(21,18,'Y','Поиск пользователя','user.search',NULL,'user','N','2005-10-24','N','N',3,'v. 2.0','','N','Y'),(22,18,'Y','Группы пользователей','users.group',NULL,'user','N','2005-11-22','N','N',6,'v. 2.0','','N','Y'),(23,NULL,'N','Помощь','help',NULL,'user','Y','2005-07-12','N','N',31,'v. 1.0','','N','Y'),(24,NULL,'N','Выход','logout',NULL,'user','Y','2005-07-12','N','N',33,'v. 1.0','','N','Y'),(25,NULL,'N','Авторы','authors',NULL,'user','N','2006-09-01','Y','N',39,'v. 1.0','','N','Y'),(26,25,'Y','Добавить автора','author.edit',NULL,'user','N','2006-09-01','N','N',1,'v. 1.0','','N','Y'),(27,NULL,'N','Новости','news',NULL,'user','N','2006-11-23','N','N',41,'v. 1.0','','N','Y'),(28,27,'Y','Добавить новость','news.form',NULL,'user','N','2006-11-23','N','N',1,'v. 1.0','','N','Y'),(29,2,'Y','Список модулей','modules',NULL,'user','N','2006-11-25','N','N',1,'v. 1.0','modules','N','Y'),(30,NULL,'N','Реклама','banners',NULL,'user','N','2013-07-28','N','N',44,'v. 1.0','','N','Y'),(31,NULL,'N','Резюме','resumes',NULL,'user','N','2013-07-29','N','N',45,'v. 1.0','resumes','N','Y'),(32,NULL,'N','Вакансии','vacancies',NULL,'user','N','2013-07-29','N','N',46,'v. 1.0','vacancies','N','Y'),(33,NULL,'N','Статьи','articles',NULL,'user','N','2013-07-29','N','N',42,'v. 1.0','','N','Y'),(34,31,'Y','Добавить резюме','resume.form',NULL,'user','N','2013-07-31','N','N',1,'v. 1.0','resumes','N','Y'),(35,33,'Y','Добавить статью','article.add',NULL,'user','N','2013-08-05','N','N',1,'v. 1.0','','N','Y'),(36,32,'Y','Добавить вакансию','vacancy.form',NULL,'user','N','2013-08-05','N','N',1,'v. 1.0','','N','Y'),(37,NULL,'Y','Газета / журналы','magazine',NULL,'user','N','2013-08-20','N','N',47,'v. 1.0','magazine','N','Y'),(38,NULL,'N','Компании','companies',NULL,'user','N','2013-08-22','N','N',48,'v. 1.0','','N','Y'),(39,38,'Y','Добавить компанию','company.form',NULL,'user','N','2013-08-22','N','N',1,'v. 1.0','','N','Y'),(40,NULL,'N','Отраслевые категории','categories',NULL,'user','N','2013-09-03','N','N',49,'v. 1.0','','N','Y'),(41,NULL,'N','Пользователи сайта','site.users',NULL,'user','N','2013-09-13','N','N',50,'v. 1.0','site.users','N','Y'),(42,41,'Y','Добавить пользователя сайта','site.user.form',NULL,'user','N','2013-09-13','N','N',1,'v. 1.0','site.users','N','Y'),(43,NULL,'N','Зона распространения','zone.distribution',NULL,'user','N','2013-10-09','N','N',51,'v. 1.0','','N','Y'),(44,NULL,'N','Спамер - рассылка','spamer',NULL,'user','N','2013-10-10','N','N',52,'v. 1.0','spamer','N','Y'),(45,44,'Y','Создать шаблон','spamer.form',NULL,'user','N','2013-10-10','N','N',1,'v. 1.0','spamer','N','Y'),(46,44,'N','Группы подписчиков','spam.groups',NULL,'user','N','2013-10-11','N','N',2,'v. 1.0','spamer','N','Y'),(47,NULL,'N','События','events',NULL,'user','N','2013-10-28','N','N',53,'v. 1.0','','N','Y'),(48,NULL,'N','Словарь профессий','dictionary',NULL,'user','N','2013-10-28','N','N',54,'v. 1.0','','N','Y'),(49,47,'Y','Добавить событие','event.form',NULL,'user','N','2013-10-28','N','N',1,'v. 1.0','','N','Y'),(50,48,'Y','Добавить профессию','dictionary.form',NULL,'user','N','2013-10-28','N','N',1,'v. 1.0','','N','Y'),(51,NULL,'N','Тренинги','trainings',NULL,'user','N','2014-01-15','N','N',55,'v. 1.0','','N','Y'),(52,51,'Y','Добавить тренинг','training.form',NULL,'user','N','2014-01-15','N','N',1,'v. 1.0','','N','Y'),(53,NULL,'N','Поисковые тексты','search.text',NULL,'user','N','2014-04-03','N','N',56,'v. 1.0','','N','Y');
/*!40000 ALTER TABLE `cms_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_projects`
--

DROP TABLE IF EXISTS `cms_projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_projects` (
  `fp_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fp_name` varchar(255) NOT NULL DEFAULT '',
  `fp_path` varchar(255) NOT NULL DEFAULT '',
  `fp_url` varchar(50) NOT NULL DEFAULT '',
  `fp_status` enum('N','Y') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`fp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Таблица проектов';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_projects`
--

LOCK TABLES `cms_projects` WRITE;
/*!40000 ALTER TABLE `cms_projects` DISABLE KEYS */;
INSERT INTO `cms_projects` VALUES (1,'Rabota-Ya.ru','/var/www/rabotayaru/data/www/rabota-ya.ru/','http://www.rabota-ya.ru','Y'),(2,'VVK24','/var/www/rabotayaru/data/www/vvk24.ru/','http://www.vvk24.ru/','Y');
/*!40000 ALTER TABLE `cms_projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_projects_access`
--

DROP TABLE IF EXISTS `cms_projects_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_projects_access` (
  `fpa_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fpa_id_project` int(10) unsigned NOT NULL DEFAULT '0',
  `fpa_id_user` int(10) unsigned NOT NULL DEFAULT '0',
  `fpa_status` enum('N','Y') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`fpa_id`),
  UNIQUE KEY `fpa_id_project` (`fpa_id_project`,`fpa_id_user`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Доступ пользователей к проектам';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_projects_access`
--

LOCK TABLES `cms_projects_access` WRITE;
/*!40000 ALTER TABLE `cms_projects_access` DISABLE KEYS */;
INSERT INTO `cms_projects_access` VALUES (1,1,1,'Y'),(2,1,2,'Y'),(3,2,1,'Y'),(4,2,2,'Y');
/*!40000 ALTER TABLE `cms_projects_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_projects_modules`
--

DROP TABLE IF EXISTS `cms_projects_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_projects_modules` (
  `fpm_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fpm_project` int(10) unsigned DEFAULT NULL,
  `fpm_module` int(10) unsigned DEFAULT NULL,
  `fpm_status` enum('Y','N') NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`fpm_id`),
  KEY `fpm_project` (`fpm_project`)
) ENGINE=MyISAM AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 COMMENT='Таблица отношений проект<->модул?';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_projects_modules`
--

LOCK TABLES `cms_projects_modules` WRITE;
/*!40000 ALTER TABLE `cms_projects_modules` DISABLE KEYS */;
INSERT INTO `cms_projects_modules` VALUES (1,1,1,'Y'),(2,1,2,'Y'),(3,1,3,'Y'),(4,1,4,'Y'),(5,1,5,'Y'),(6,1,6,'Y'),(7,1,7,'Y'),(8,1,8,'Y'),(9,1,9,'Y'),(10,1,10,'Y'),(11,1,11,'Y'),(12,1,12,'Y'),(13,1,13,'Y'),(14,1,14,'Y'),(15,1,15,'Y'),(16,1,16,'Y'),(17,1,17,'Y'),(18,1,18,'Y'),(19,1,19,'Y'),(20,1,20,'Y'),(21,1,21,'Y'),(22,1,22,'Y'),(23,1,23,'Y'),(24,1,24,'Y'),(25,1,25,'Y'),(26,1,26,'Y'),(27,1,27,'Y'),(28,1,28,'Y'),(29,1,29,'Y'),(30,1,30,'Y'),(31,1,31,'Y'),(32,1,32,'Y'),(33,1,33,'Y'),(34,1,34,'Y'),(35,1,35,'Y'),(36,1,36,'Y'),(37,1,37,'Y'),(38,1,38,'Y'),(39,1,39,'Y'),(40,1,40,'Y'),(41,1,41,'Y'),(42,1,42,'Y'),(43,1,43,'Y'),(44,1,44,'Y'),(45,1,45,'Y'),(46,1,46,'Y'),(47,1,47,'Y'),(48,1,48,'Y'),(49,1,49,'Y'),(50,1,50,'Y'),(51,1,51,'Y'),(52,1,52,'Y'),(53,1,53,'Y');
/*!40000 ALTER TABLE `cms_projects_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_users`
--

DROP TABLE IF EXISTS `cms_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_users` (
  `cu_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cu_group` int(10) unsigned DEFAULT NULL,
  `cu_fname` varchar(50) NOT NULL DEFAULT '',
  `cu_lname` varchar(50) NOT NULL DEFAULT '',
  `cu_pname` varchar(50) NOT NULL DEFAULT '',
  `cu_login` varchar(50) NOT NULL DEFAULT '',
  `cu_password` varchar(32) NOT NULL DEFAULT '',
  `cu_status` enum('Y','N') NOT NULL DEFAULT 'Y',
  `cu_email` varchar(50) NOT NULL DEFAULT '',
  `cu_gender` enum('m','f') NOT NULL DEFAULT 'm',
  `cu_default_project` int(10) unsigned NOT NULL DEFAULT '1',
  `cu_last_enter` datetime DEFAULT NULL,
  UNIQUE KEY `id` (`cu_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_users`
--

LOCK TABLES `cms_users` WRITE;
/*!40000 ALTER TABLE `cms_users` DISABLE KEYS */;
INSERT INTO `cms_users` VALUES (1,1,'Дмитрий','Широковский','Анатольевич','admin','bcb5c99bb650301b69a776c0b88d9c59','Y','shirokovskij@mail.ru','m',1,'2014-04-05 23:17:30'),(2,2,'Евгения','Николаева','','evgeniya','827ccb0eea8a706c4c34a16891f84e7b','Y','evgenija.nikolaeva@rabota-ya.ru','f',1,'2014-04-07 09:44:33');
/*!40000 ALTER TABLE `cms_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_users_access_modules`
--

DROP TABLE IF EXISTS `cms_users_access_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_users_access_modules` (
  `cuam_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cuam_type` enum('group','user') NOT NULL DEFAULT 'group' COMMENT 'тип доступа',
  `cuam_id_owner` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'ID группы или пользователя',
  `cuam_id_module` int(10) unsigned NOT NULL DEFAULT '0',
  `cuam_id_project` int(10) unsigned DEFAULT NULL COMMENT 'проект',
  `cuam_access` enum('Y','N','R') NOT NULL DEFAULT 'Y',
  `cuam_user_set` enum('N','Y') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`cuam_id`),
  UNIQUE KEY `cuam_type` (`cuam_type`,`cuam_id_owner`,`cuam_id_module`,`cuam_id_project`),
  KEY `cuam_id_project` (`cuam_id_project`)
) ENGINE=MyISAM AUTO_INCREMENT=827 DEFAULT CHARSET=utf8 COMMENT='Таблица доступа к модулям';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_users_access_modules`
--

LOCK TABLES `cms_users_access_modules` WRITE;
/*!40000 ALTER TABLE `cms_users_access_modules` DISABLE KEYS */;
INSERT INTO `cms_users_access_modules` VALUES (781,'user',1,52,1,'Y','N'),(780,'user',1,51,1,'Y','N'),(779,'user',1,50,1,'Y','N'),(778,'user',1,49,1,'Y','N'),(777,'user',1,48,1,'Y','N'),(776,'user',1,47,1,'Y','N'),(775,'user',1,46,1,'Y','N'),(774,'user',1,45,1,'Y','N'),(773,'user',1,44,1,'Y','N'),(772,'user',1,43,1,'Y','N'),(771,'user',1,42,1,'Y','N'),(770,'user',1,41,1,'Y','N'),(769,'user',1,40,1,'Y','N'),(768,'user',1,39,1,'Y','N'),(767,'user',1,38,1,'Y','N'),(766,'user',1,37,1,'Y','N'),(765,'user',1,36,1,'Y','N'),(764,'user',1,35,1,'Y','N'),(763,'user',1,34,1,'Y','N'),(762,'user',1,33,1,'Y','N'),(761,'user',1,32,1,'Y','N'),(760,'user',1,31,1,'Y','N'),(759,'user',1,30,1,'Y','N'),(758,'user',1,29,1,'Y','N'),(757,'user',1,28,1,'Y','N'),(756,'user',1,27,1,'Y','N'),(755,'user',1,26,1,'Y','N'),(754,'user',1,25,1,'Y','N'),(753,'user',1,24,1,'Y','N'),(752,'user',1,23,1,'Y','N'),(825,'user',2,52,1,'Y','N'),(824,'user',2,51,1,'Y','N'),(823,'user',2,50,1,'Y','N'),(822,'user',2,49,1,'Y','N'),(821,'user',2,48,1,'Y','N'),(820,'user',2,47,1,'Y','N'),(819,'user',2,46,1,'Y','N'),(818,'user',2,45,1,'Y','N'),(817,'user',2,44,1,'Y','N'),(816,'user',2,43,1,'Y','N'),(815,'user',2,42,1,'Y','N'),(814,'user',2,41,1,'Y','N'),(813,'user',2,40,1,'Y','N'),(812,'user',2,39,1,'Y','N'),(811,'user',2,38,1,'Y','N'),(810,'user',2,37,1,'Y','N'),(809,'user',2,36,1,'Y','N'),(808,'user',2,35,1,'Y','N'),(807,'user',2,34,1,'Y','N'),(806,'user',2,33,1,'Y','N'),(805,'user',2,32,1,'Y','N'),(751,'user',1,22,1,'Y','N'),(804,'user',2,31,1,'Y','N'),(750,'user',1,21,1,'Y','N'),(803,'user',2,30,1,'Y','N'),(749,'user',1,20,1,'Y','N'),(802,'user',2,28,1,'Y','N'),(748,'user',1,19,1,'Y','N'),(801,'user',2,27,1,'Y','N'),(747,'user',1,18,1,'Y','N'),(800,'user',2,24,1,'Y','N'),(746,'user',1,17,1,'Y','N'),(799,'user',2,22,1,'Y','N'),(745,'user',1,16,1,'Y','N'),(798,'user',2,21,1,'Y','N'),(744,'user',1,15,1,'Y','N'),(797,'user',2,20,1,'Y','N'),(743,'user',1,14,1,'Y','N'),(796,'user',2,19,1,'Y','N'),(742,'user',1,13,1,'Y','N'),(795,'user',2,18,1,'Y','N'),(741,'user',1,12,1,'Y','N'),(794,'user',2,17,1,'Y','N'),(740,'user',1,11,1,'Y','N'),(793,'user',2,16,1,'Y','N'),(739,'user',1,10,1,'Y','N'),(792,'user',2,15,1,'Y','N'),(738,'user',1,9,1,'Y','N'),(791,'user',2,14,1,'Y','N'),(737,'user',1,8,1,'Y','N'),(790,'user',2,12,1,'Y','N'),(736,'user',1,7,1,'Y','N'),(789,'user',2,11,1,'Y','N'),(735,'user',1,6,1,'Y','N'),(826,'user',2,53,1,'Y','N'),(782,'user',1,53,1,'Y','N'),(729,'user',2,53,NULL,'Y','N'),(728,'user',1,53,NULL,'Y','N'),(788,'user',2,10,1,'Y','N'),(787,'user',2,9,1,'Y','N'),(734,'user',1,5,1,'Y','N'),(733,'user',1,4,1,'Y','N'),(539,'user',1,21,2,'Y','N'),(538,'user',1,20,2,'Y','N'),(537,'user',1,18,2,'Y','N'),(536,'user',1,17,2,'Y','N'),(535,'user',1,16,2,'Y','N'),(534,'user',1,15,2,'Y','N'),(533,'user',1,14,2,'Y','N'),(532,'user',1,12,2,'Y','N'),(531,'user',1,11,2,'Y','N'),(530,'user',1,10,2,'Y','N'),(529,'user',1,9,2,'Y','N'),(786,'user',2,8,1,'Y','N'),(785,'user',2,7,1,'Y','N'),(732,'user',1,3,1,'Y','N'),(731,'user',1,2,1,'Y','N'),(784,'user',2,6,1,'Y','N'),(783,'user',2,2,1,'Y','N'),(528,'user',1,8,2,'Y','N'),(527,'user',1,7,2,'Y','N'),(526,'user',1,6,2,'Y','N'),(525,'user',1,1,2,'Y','N'),(319,'user',2,1,2,'Y','N'),(320,'user',2,6,2,'Y','N'),(321,'user',2,7,2,'Y','N'),(322,'user',2,8,2,'Y','N'),(323,'user',2,9,2,'Y','N'),(324,'user',2,10,2,'Y','N'),(325,'user',2,11,2,'Y','N'),(326,'user',2,12,2,'Y','N'),(327,'user',2,14,2,'Y','N'),(328,'user',2,15,2,'Y','N'),(329,'user',2,16,2,'Y','N'),(730,'user',1,1,1,'Y','N'),(378,'user',2,48,NULL,'Y','N');
/*!40000 ALTER TABLE `cms_users_access_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_users_group`
--

DROP TABLE IF EXISTS `cms_users_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_users_group` (
  `cug_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cug_name` varchar(255) NOT NULL DEFAULT '',
  `cug_status` enum('N','Y') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`cug_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Таблица групп пользователей';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_users_group`
--

LOCK TABLES `cms_users_group` WRITE;
/*!40000 ALTER TABLE `cms_users_group` DISABLE KEYS */;
INSERT INTO `cms_users_group` VALUES (1,'Разработчики','Y'),(2,'Контент-менеджеры','Y');
/*!40000 ALTER TABLE `cms_users_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_categories`
--

DROP TABLE IF EXISTS `job_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_categories` (
  `jc_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jc_parent` int(10) unsigned DEFAULT NULL COMMENT 'группа',
  `jc_title` varchar(255) NOT NULL COMMENT 'Название категории',
  `jc_order` int(11) DEFAULT NULL COMMENT 'порядок сортировки',
  PRIMARY KEY (`jc_id`),
  KEY `jc_parent` (`jc_parent`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8 COMMENT='Категории работ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_categories`
--

LOCK TABLES `job_categories` WRITE;
/*!40000 ALTER TABLE `job_categories` DISABLE KEYS */;
INSERT INTO `job_categories` VALUES (1,51,'Руководители',0),(2,51,'Бухгалетеры,Финансисты,Экономисты',0),(3,51,'Инженерно-технические работники',0),(4,51,'Юристы',0),(5,51,'Офис-менеджеры,администраторы',0),(6,51,'Секретари,операторы ПК',0),(7,51,'Диспетчеры',0),(8,51,'Маркетологи,специалисты по рекламе PR',0),(9,51,'Переводчики,специалисты издательств,полиграфия',0),(10,52,'Менеджеры по продажам,покупкам',0),(11,52,'Агенты,торговые представители,мерчендайзеры',0),(12,54,'Водители,экспедиторы',0),(13,54,'Работник автосервиса',0),(15,103,'Работники охраны и правопорядка',0),(16,56,'Педагоги,домашний персонал',0),(17,58,'Рабочие',0),(18,58,'Специалисты',0),(20,59,'Специалисты по недвижимости',0),(21,59,'Специалисты в сфере страхования',0),(23,55,'Медработники,фармацевты,провизоры',0),(24,55,'Фитнес,салоны красоты,парикмахерские',0),(25,53,'Работники кафе,ресторанов',0),(30,52,'Продавцы, кассиры',0),(44,59,'Специалисты сервиса и бытовых услуг',0),(51,NULL,'Специалисты офиса',0),(52,NULL,'Специалисты в сфере тоговли',0),(53,NULL,'Работники общественного питания',0),(54,NULL,'Транспортные работники',0),(55,NULL,'Здоровье и красота',0),(56,NULL,'Работники сферы образования и воспитания',0),(58,NULL,'Производство',0),(59,NULL,'Работники сферы услуг',0),(60,NULL,'Неквалифицированный труд',0),(78,58,'Швейное производство',0),(79,55,'Ветеринария',0),(81,NULL,'Специалисты IT и коммуникаций',0),(82,NULL,'Строительство',0),(84,82,'Строительство',0),(91,58,'Производство промышленное',0),(92,58,'Производство пищевое',0),(94,81,'Инженеры интернет-сетей',0),(97,22,'Работа без опыта',0),(98,22,'Работа для студентов',0),(99,NULL,'Разработчики',0),(100,95,'Складские работники',0),(101,99,'Разработчики, программисты',0),(102,60,'Неквалифицированный труд',0),(103,NULL,'Работники охраны и правопорядка',0);
/*!40000 ALTER TABLE `job_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_category_link`
--

DROP TABLE IF EXISTS `job_category_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_category_link` (
  `jcl_sjc_id` int(10) unsigned NOT NULL COMMENT 'id категории',
  `jcl_rel_id` int(10) unsigned NOT NULL COMMENT 'id вакансии или резюме',
  `jcl_type` enum('resume','vacancy') DEFAULT NULL COMMENT 'тип связи',
  UNIQUE KEY `jcl_sjc_id` (`jcl_sjc_id`,`jcl_rel_id`,`jcl_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Привязка категорий с вакансиями и резюме';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_category_link`
--

LOCK TABLES `job_category_link` WRITE;
/*!40000 ALTER TABLE `job_category_link` DISABLE KEYS */;
INSERT INTO `job_category_link` VALUES (1,47,'resume'),(1,62,'resume'),(1,66,'resume'),(1,67,'resume'),(1,97,'resume'),(1,104,'vacancy'),(1,105,'vacancy'),(1,106,'vacancy'),(1,107,'vacancy'),(1,111,'vacancy'),(1,115,'resume'),(1,140,'resume'),(1,155,'resume'),(1,167,'resume'),(1,171,'resume'),(1,190,'vacancy'),(1,192,'resume'),(1,221,'resume'),(1,226,'resume'),(1,240,'resume'),(1,247,'resume'),(1,248,'resume'),(1,256,'resume'),(1,274,'vacancy'),(1,279,'resume'),(1,300,'vacancy'),(1,311,'resume'),(1,323,'resume'),(1,324,'vacancy'),(1,326,'vacancy'),(1,350,'vacancy'),(1,386,'resume'),(1,391,'vacancy'),(1,421,'vacancy'),(1,460,'vacancy'),(1,471,'resume'),(1,482,'resume'),(1,504,'vacancy'),(1,512,'vacancy'),(1,526,'vacancy'),(1,532,'vacancy'),(1,536,'vacancy'),(1,537,'vacancy'),(1,558,'vacancy'),(1,565,'vacancy'),(1,587,'resume'),(1,595,'vacancy'),(1,606,'vacancy'),(1,633,'vacancy'),(1,648,'resume'),(1,649,'vacancy');
/*!40000 ALTER TABLE `job_category_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_companies`
--

DROP TABLE IF EXISTS `job_companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_companies` (
  `jc_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `jc_title` varchar(255) NOT NULL COMMENT 'имя компании',
  `jc_type` enum('direct','recrut') NOT NULL DEFAULT 'direct' COMMENT 'тип нанимателя',
  `jc_top` enum('N','Y') NOT NULL DEFAULT 'N' COMMENT 'ведущая компания',
  `jc_city` varchar(255) DEFAULT NULL COMMENT 'город',
  `jc_address` varchar(255) DEFAULT NULL COMMENT 'адрес',
  `jc_web` varchar(255) DEFAULT NULL COMMENT 'сайт компании',
  `jc_email` varchar(255) DEFAULT NULL COMMENT 'контактный email',
  `jc_phone` varchar(255) NOT NULL COMMENT 'контактный телефон',
  `jc_description` text NOT NULL COMMENT 'описание компании',
  `jc_date_create` int(11) DEFAULT NULL COMMENT 'дата создания записи',
  `jc_status` enum('N','Y') NOT NULL DEFAULT 'Y' COMMENT 'статус показа на сайте',
  `jc_city_id` int(10) unsigned DEFAULT NULL COMMENT 'id города',
  PRIMARY KEY (`jc_id`),
  UNIQUE KEY `jc_title` (`jc_title`),
  UNIQUE KEY `jc_title_2` (`jc_title`,`jc_type`,`jc_city`),
  KEY `jc_city_id` (`jc_city_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35248 DEFAULT CHARSET=utf8 COMMENT='Компании работодатели';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_companies`
--

LOCK TABLES `job_companies` WRITE;
/*!40000 ALTER TABLE `job_companies` DISABLE KEYS */;
INSERT INTO `job_companies` VALUES (1,'Ирина','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391777,'Y',NULL),(2,'Евгения','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391862,'Y',NULL),(3,'НИИ Нейрохирургии им.проф.А.Л.Поленова','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391880,'Y',NULL),(4,'Белоусов Групп','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391880,'Y',NULL),(5,'ГК Барит','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391880,'Y',NULL),(6,'Фирма Севзапметалл','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391881,'Y',NULL),(7,'Клуб Fireball','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391881,'Y',NULL),(8,'ТАКСИ-071','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391882,'Y',NULL),(9,'Уборочная компания Клининг Люкс','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391882,'Y',NULL),(10,'Ночной клуб Fireball','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391882,'Y',NULL),(11,'Повар','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391882,'Y',NULL),(12,'Уборочная компания \"Клиниг люкс','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391883,'Y',NULL),(13,'Ресторан','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391884,'Y',NULL),(14,'Студенческая столовая при институте','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391885,'Y',NULL),(15,'Ип','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391886,'Y',NULL),(16,'ИП Куриловская','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391886,'Y',NULL),(17,'АМД Транспорт','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391887,'Y',NULL),(18,'Меровинг','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391889,'Y',NULL),(19,'Тиенс','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391890,'Y',NULL),(20,'УниверсалСервис','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391890,'Y',NULL),(21,'Альта-юр','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391890,'Y',NULL),(22,'\"Тиенс\"','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391891,'Y',NULL),(23,'ИП Маркова','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391891,'Y',NULL),(24,'Парикмахерская','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391891,'Y',NULL),(25,'ЦарьПышка','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391891,'Y',NULL),(26,'магазин','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391892,'Y',NULL),(27,'Дизельная станция','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391892,'Y',NULL),(28,'Компания \" Магистр\"','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391892,'Y',NULL),(29,'ТОРГОВЫЙ КОМПЛЕКС','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391892,'Y',NULL),(30,'Адмирал','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391893,'Y',NULL),(31,'Российская компания','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391893,'Y',NULL),(32,'ООО \"Ай Ди Груп\"','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391893,'Y',NULL),(33,'Фотосъемка','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391893,'Y',NULL),(34,'OАО \\','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391893,'Y',NULL),(35,'ОАО ','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391894,'Y',NULL),(36,'ОАО \"Новая ЭРА\"','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391894,'Y',NULL),(37,'Ресторан Ротонда','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391894,'Y',NULL),(38,'Кондитер Сервис','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391895,'Y',NULL),(39,'Охранное предприятие','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391895,'Y',NULL),(40,'Сеть европейских химчисток','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391895,'Y',NULL),(41,'Охранная Организация','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391896,'Y',NULL),(42,'Зэт-Сервис','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391896,'Y',NULL),(43,'Зэт','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391896,'Y',NULL),(44,'Зао','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391896,'Y',NULL),(45,'Салон красоты','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391896,'Y',NULL),(46,'ООО \"Севзапметалл\"','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391897,'Y',NULL),(47,'ИП Коваленко','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391897,'Y',NULL),(48,'ресторан Асахи','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391897,'Y',NULL),(49,'СПМ','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391898,'Y',NULL),(50,'Торговый Дом','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391898,'Y',NULL),(51,'Канат СТД','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391898,'Y',NULL),(52,'Ресторан La Hosyia','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391898,'Y',NULL),(53,'Коммпоненты','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391898,'Y',NULL),(54,'ООО','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391899,'Y',NULL),(55,'Концертный клуб \"Pent House\"','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391899,'Y',NULL),(56,'Завод Слоистых пластиков','direct','N','Санкт-Петербург','','','','','',1375391899,'Y',NULL),(57,'ИП Лисицина','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391900,'Y',NULL),(58,'Автомойка','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391900,'Y',NULL),(59,'ООО \"Уборочная компания Клининг Люкс\"','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391901,'Y',NULL),(60,'Фабрика окон \"Каприкон\"','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391901,'Y',NULL),(61,'Фабрика окон','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391902,'Y',NULL),(62,'Мебельное производство','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391902,'Y',NULL),(63,'ООО \"Альянс\"','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391902,'Y',NULL),(64,'Кронверк синема','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391903,'N',NULL),(65,'Швейное предприятие','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391904,'Y',NULL),(66,'Кафе','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391904,'Y',NULL),(67,'Ленпечать','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391904,'Y',NULL),(68,'Ресторанный комплекс \"Гранд-палас-К\"','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391905,'Y',NULL),(69,'Ресторанный комплекс','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391905,'Y',NULL),(70,'ООО \"Димакс\"','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391906,'Y',NULL),(71,'ООО \"ПКФ\"МСК2','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391906,'Y',NULL),(72,'НСГ','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391907,'Y',NULL),(73,'Производство','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391907,'Y',NULL),(74,'Cалон красоты','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391907,'Y',NULL),(75,'Нефтехимавтоматика-СПб','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391908,'Y',NULL),(76,'ФОРТУНА','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391908,'Y',NULL),(77,'Cтоматологическая клиника','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391908,'Y',NULL),(78,'Шиномантаж','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391908,'Y',NULL),(79,'Пивной клуб \"Шхера\"','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391909,'Y',NULL),(80,'крупное автопредприятие','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391909,'Y',NULL),(81,'Торговая компания','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391909,'Y',NULL),(82,'Автогидроподъёмник','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391910,'Y',NULL),(83,'Швейное пр-ие','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391910,'Y',NULL),(84,'корпорация ТИРА','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391910,'Y',NULL),(85,'Ассоциация РЕЗЕРВ','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391911,'Y',NULL),(86,'Вилью','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391911,'Y',NULL),(87,'организация','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391912,'Y',NULL),(88,'Металлообрабатывающая компания','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391912,'Y',NULL),(89,'ТАКСИ','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391913,'Y',NULL),(90,'крупная сеть универсамов КИР 24','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391914,'Y',NULL),(91,'оптовая компания','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391914,'Y',NULL),(92,'склад','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391915,'Y',NULL),(93,'строительная орг.','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391915,'Y',NULL),(94,'Такси 24','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391916,'Y',NULL),(95,'OOO','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391916,'Y',NULL),(96,'Крупная российская компания','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391916,'Y',NULL),(97,'ЗАО\"Росэкспопром\"','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391916,'Y',NULL),(98,'ООО \"Современные технологии строительства\"','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391917,'Y',NULL),(99,'Студия красоты','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391917,'Y',NULL),(100,'Столовая','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391917,'Y',NULL),(101,'Торговая Инжиниринговая компания','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391917,'Y',NULL),(102,'ООО\"Оникс\" швейное предприятие','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391918,'Y',NULL),(103,'ООО\"Диалог\"','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391918,'Y',NULL),(104,'КСУ','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391918,'Y',NULL),(105,'ОАО NSP','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391919,'Y',NULL),(106,'стоматология','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391920,'Y',NULL),(107,'ООО Жар-птица','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391921,'Y',NULL),(108,'ООО Прометей','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391922,'Y',NULL),(109,'ООО CанСитиИнжиниринг','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391922,'Y',NULL),(110,'ООО Россервис','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391922,'Y',NULL),(111,'welonda','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391924,'Y',NULL),(112,'ГК','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391925,'Y',NULL),(113,'ГК \"Инженерные экосистемы\"','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391925,'Y',NULL),(114,'В магазин Аквариумистики','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391925,'Y',NULL),(115,'РАБОТА В ОФИСЕ','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391926,'Y',NULL),(116,'Продовольственный магазин','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391926,'Y',NULL),(117,'Диакон','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391926,'Y',NULL),(118,'Диаконт','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391927,'Y',NULL),(119,'Прачечная-химчистка АПЕКС','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391927,'Y',NULL),(120,'Линдстрем','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391927,'Y',NULL),(121,'Рекламная компания','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391928,'Y',NULL),(122,'Автотранспортное предприяти','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391928,'Y',NULL),(123,'Автотранспортное предприятие','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391928,'Y',NULL),(124,'Транспортная компания','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391929,'Y',NULL),(125,'В кафе','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391929,'Y',NULL),(126,'ЭлектроМонтажАвтоматика','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391929,'Y',NULL),(127,'ЧП Фокин','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391930,'Y',NULL),(128,'ООО БЕТА','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391930,'Y',NULL),(129,'АЛ-КО','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391930,'Y',NULL),(130,'Завод Слоистых пластиков.','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391931,'Y',NULL),(131,'Требуется менеджер по работе с клиентами','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391932,'Y',NULL),(132,'ИП Черкунова','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391932,'Y',NULL),(133,'Ип Окулов','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391932,'Y',NULL),(134,'ооо ЛАЗУРИТ','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391932,'Y',NULL),(135,'ООО Интан','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391933,'Y',NULL),(136,'ИП Емельяненко','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391933,'Y',NULL),(137,'Салон-массажа.','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391933,'Y',NULL),(138,'ИП Мокшина','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391933,'Y',NULL),(139,'ЮрЛегион','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391933,'Y',NULL),(140,'Джой','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391934,'Y',NULL),(141,'ООО \" РТ\"','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391934,'Y',NULL),(142,'Ип КИМ Любовь','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391934,'Y',NULL),(143,'Страховая компания','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391935,'Y',NULL),(144,'ИП Мизин','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391936,'Y',NULL),(145,'Производственная компания','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391936,'Y',NULL),(146,'Маргарита Александровна Кирилова','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391937,'Y',NULL),(147,'Продуктовый магазин','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391937,'Y',NULL),(148,'Массажный салон','direct','N','Санкт-Петербург',NULL,NULL,NULL,'','',1375391938,'Y',NULL);
/*!40000 ALTER TABLE `job_companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_education_types`
--

DROP TABLE IF EXISTS `job_education_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_education_types` (
  `jet_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `jet_title` varchar(32) NOT NULL COMMENT 'вид образования',
  PRIMARY KEY (`jet_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Список видов образования';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_education_types`
--

LOCK TABLES `job_education_types` WRITE;
/*!40000 ALTER TABLE `job_education_types` DISABLE KEYS */;
INSERT INTO `job_education_types` VALUES (1,'Высшее'),(2,'Неоконченное высшее'),(3,'Среднее специальное'),(4,'Среднее'),(5,'Повышение квалификации, курсы');
/*!40000 ALTER TABLE `job_education_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_magazines`
--

DROP TABLE IF EXISTS `job_magazines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_magazines` (
  `jm_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jm_year` smallint(5) unsigned NOT NULL COMMENT 'Год выпуска',
  `jm_week` tinyint(3) unsigned NOT NULL COMMENT 'Неделя выхода газеты',
  `jm_dow` enum('tu','we','th') DEFAULT NULL COMMENT 'День номера газеты',
  `jm_url` varchar(255) DEFAULT NULL COMMENT 'URL на выпуск газеты',
  PRIMARY KEY (`jm_id`),
  UNIQUE KEY `jm_year` (`jm_year`,`jm_week`,`jm_dow`)
) ENGINE=MyISAM AUTO_INCREMENT=132 DEFAULT CHARSET=utf8 COMMENT='Номера газет';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_magazines`
--

LOCK TABLES `job_magazines` WRITE;
/*!40000 ALTER TABLE `job_magazines` DISABLE KEYS */;
INSERT INTO `job_magazines` VALUES (1,2013,31,'tu','368'),(2,2013,31,'we','369'),(3,2013,31,'th','370'),(4,2013,34,'tu','377'),(5,2013,34,'we','378'),(6,2013,34,'th','379'),(7,2013,35,'tu','380'),(8,2013,35,'we','381'),(9,2013,35,'th','382'),(10,2013,36,'tu','383'),(11,2013,36,'we','384'),(12,2013,36,'th','385'),(13,2013,37,'tu','386'),(14,2013,37,'we','387'),(15,2013,37,'th','388'),(16,2013,38,'tu','389'),(17,2013,38,'we','390'),(18,2013,38,'th','391'),(19,2013,39,'tu','392'),(20,2013,39,'we','393'),(21,2013,39,'th','394'),(22,2013,41,'tu','398'),(23,2013,41,'we','399'),(24,2013,41,'th','400'),(25,2013,43,'tu','404'),(26,2013,43,'we','405'),(27,2013,43,'th','406'),(30,2013,44,'tu','407'),(31,2013,44,'we','408'),(32,2013,44,'th','409'),(39,2013,47,'tu','413'),(40,2013,47,'we','414'),(41,2013,47,'th','415'),(42,2013,46,'tu','413'),(43,2013,46,'we','414'),(44,2013,46,'th','415'),(45,2013,48,'tu','416'),(46,2013,48,'we','417'),(47,2013,48,'th','418'),(51,2013,49,'tu','422'),(52,2013,49,'we','423'),(53,2013,49,'th','0'),(54,2013,50,'tu','425'),(55,2013,50,'we','426'),(56,2013,50,'th','427'),(57,2013,51,'tu','428'),(58,2013,51,'we','429'),(59,2013,51,'th','430'),(60,2014,2,'tu','431'),(61,2014,2,'we','432'),(62,2014,2,'th','433'),(63,2014,3,'tu','437'),(64,2014,3,'we','435'),(65,2014,3,'th','436'),(66,2014,4,'tu','0'),(67,2014,4,'we','438'),(68,2014,4,'th','439'),(69,2014,5,'tu','440'),(70,2014,5,'we','441'),(71,2014,5,'th','442'),(78,2014,6,'tu','443'),(79,2014,6,'we','444'),(80,2014,6,'th','445'),(81,2014,7,'tu','446'),(82,2014,7,'we','447'),(83,2014,7,'th','448'),(87,2014,8,'tu','449'),(88,2014,8,'we','450'),(89,2014,8,'th','451'),(96,2014,9,'tu','452'),(97,2014,9,'we','453'),(98,2014,9,'th','454'),(102,2014,10,'tu','455'),(103,2014,10,'we','456'),(104,2014,10,'th','457'),(114,2014,11,'tu','458'),(115,2014,11,'we','459'),(116,2014,11,'th','460'),(117,2014,12,'tu','0'),(118,2014,12,'we','462'),(119,2014,12,'th','463'),(123,2014,13,'tu','464'),(124,2014,13,'we','465'),(125,2014,13,'th','466'),(129,2014,14,'tu','467'),(130,2014,14,'we','468'),(131,2014,14,'th','469');
/*!40000 ALTER TABLE `job_magazines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_resumes`
--

DROP TABLE IF EXISTS `job_resumes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_resumes` (
  `jr_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jr_title` varchar(255) NOT NULL COMMENT 'должность соискателя',
  `jr_salary` varchar(64) DEFAULT NULL COMMENT 'ожидаемый доход',
  `jr_fio` varchar(255) NOT NULL COMMENT 'ФИО',
  `jr_age` int(11) DEFAULT NULL COMMENT 'возраст',
  `jr_birthday` date DEFAULT NULL COMMENT 'дата рождения',
  `jr_sex` enum('male','female') DEFAULT NULL COMMENT 'пол соискателя',
  `jr_children` enum('N','Y') NOT NULL DEFAULT 'N' COMMENT 'наличие детей',
  `jr_marriage` enum('N','Y') NOT NULL DEFAULT 'N' COMMENT 'семейное положение',
  `jr_city` varchar(255) NOT NULL COMMENT 'населённый пункт проживания',
  `jr_city_id` int(11) DEFAULT NULL COMMENT 'id города',
  `jr_driver` enum('N','Y') NOT NULL DEFAULT 'N' COMMENT 'водительское удостоверение',
  `jr_work_busy` int(10) unsigned DEFAULT NULL COMMENT 'занятость',
  `jr_work_graphic` enum('full','part') DEFAULT NULL COMMENT 'график работы',
  `jr_trips` enum('N','Y','S') NOT NULL DEFAULT 'N' COMMENT 'готовность к командировкам',
  `jr_relocation` enum('N','Y') NOT NULL DEFAULT 'N' COMMENT 'готов переехать',
  `jr_phone` varchar(255) NOT NULL COMMENT 'контактные телефоны',
  `jr_email` varchar(255) DEFAULT NULL,
  `jr_skype` varchar(255) DEFAULT NULL,
  `jr_icq` int(11) DEFAULT NULL,
  `jr_linkedin` varchar(255) DEFAULT NULL COMMENT 'url страницы соц.сети',
  `jr_edu_plus` text COMMENT 'дополнительное обучение',
  `jr_about` text COMMENT 'дополнительно о себе',
  `jr_meta_description` varchar(255) DEFAULT NULL,
  `jr_meta_keywords` varchar(255) DEFAULT NULL,
  `jr_status` enum('N','Y') NOT NULL DEFAULT 'Y' COMMENT 'публиковать на сайте',
  `jr_su_id` int(10) unsigned DEFAULT NULL COMMENT 'зарегистрированный пользователь',
  `jr_date_publish` int(11) DEFAULT NULL COMMENT 'дата публикации',
  `jr_grab_src_alias` varchar(7) DEFAULT NULL COMMENT 'Сокращенное название сайта-источника',
  `jr_grab_src_id` varchar(10) DEFAULT NULL COMMENT 'ID резюме на сайте-источнике',
  `jr_grab_has_email` enum('N','Y') DEFAULT 'N' COMMENT 'Есть ли картинка с email',
  `jr_grab_has_phone` enum('N','Y') DEFAULT 'N' COMMENT 'Есть ли картинка с телефоном',
  `jr_views` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'кол-во просмотров',
  PRIMARY KEY (`jr_id`),
  UNIQUE KEY `jr_grab_src_alias` (`jr_grab_src_alias`,`jr_grab_src_id`),
  KEY `sjr_status` (`jr_status`),
  KEY `jr_city_id` (`jr_city_id`),
  KEY `su_id` (`jr_su_id`),
  CONSTRAINT `job_resumes_ibfk_1` FOREIGN KEY (`jr_su_id`) REFERENCES `site_users` (`su_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8587 DEFAULT CHARSET=utf8 COMMENT='Резюме';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_resumes`
--

LOCK TABLES `job_resumes` WRITE;
/*!40000 ALTER TABLE `job_resumes` DISABLE KEYS */;
INSERT INTO `job_resumes` VALUES (1,'Продавца-кассира','13000','Баранова Оксана Анатольевна',33,NULL,NULL,'Y','Y','',NULL,'N',NULL,NULL,'N','N','','oksbar22011976@yandex.ru',NULL,NULL,NULL,NULL,'Образование: средне специальное\nУчебное заведение: 1994 ПТУ 40\nОпыт работы: работа кассиром,оптовым кассиром,продавцом-кассиром,знание компьютерной кассы и программы 1С более 6 лет\nО себе: коммуникабельна,легко обучаема,ответственна','Продавца-кассира','Продавца-кассира','Y',NULL,1376092800,NULL,NULL,'N','N',0),(2,'Воспитатель, учитель биологии, экологии',NULL,'Харламова Виктория Александровна',23,NULL,NULL,'N','N','',NULL,'N',NULL,NULL,'Y','N','','torik_love2@mail.ru',NULL,NULL,NULL,NULL,'Образование: бакалавр и магистр естественнонаучного образования\nУчебное заведение: 2007г. в государственном образовательном учреждении высшего профессионального образования \",Российский государственный педагогический университит им. А.И. Герцена\", (очная форма), 2009г. магистратура РГПУ\nОпыт работы: практика в гимназии №610 ( зоология, ботаника, общая биология)+ практика в РГПУ у студентов III и IV курсов биофака на кафедре Анатомии и физиологии человека и животных на тему \",Координация активности разных групп респираторных мышц крысы в условиях антиортостатичесого положения\",\nО себе: Есть желание работать по специальности','Воспитатель, учитель биологии, экологии','Воспитатель, учитель биологии, экологии','Y',NULL,1376092800,NULL,NULL,'N','N',1),(3,'Водитель','25000-30000','',NULL,NULL,NULL,'Y','Y','',NULL,'Y',NULL,NULL,'Y','N','','andrej-gornev@yandex.ru',NULL,NULL,NULL,NULL,'Образование: \nУчебное заведение: \nОпыт работы: \nО себе: Ищу работу.Водитель кат. ВС ,водительский стаж 10лет .Опыт грузовых,пассажирских перевозок,Знание транспортной документации.Техническое образование,исполнительность ответственность .Северныe районы С.П.Б. тел.526-27-87,+7-904-515-33-09,+7950-024-52-32.Андрей Валерьевич.','Водитель','Водитель','Y',NULL,1376092800,NULL,NULL,'N','N',1),(4,'Заместитель главного бухгалтера','30000','Толкачева Татьяна Петровна',47,NULL,NULL,'Y','N','',NULL,'N',NULL,NULL,'Y','N','','ok@tuboplast-otradnoe.ru',NULL,NULL,NULL,'Курсы: Налоги и налогообложение','Образование: высшее\nУчебное заведение: 1987 год Финансово-экономический институт\nОпыт работы: Главный бухгалтер ОКС : 2,5 года Зам главного бухгалтера Сбербанк: 5 лет Бухгалтер ОАО: 5,5 лет\nО себе: Активная жизненная позиция','Заместитель главного бухгалтера','Заместитель главного бухгалтера','Y',NULL,1376092800,NULL,NULL,'N','N',1),(5,'Помощник бухгалтера','15000','Николаева Кристина Николаевна',20,NULL,NULL,'N','N','',NULL,'N',NULL,NULL,'N','N','','nikolaeva.89@list.ru',NULL,NULL,NULL,NULL,'Образование: Начальное профессиональное\nУчебное заведение: 2008 год Профессиональное училище №70\nОпыт работы: Без опыта\nО себе: Легко обучаюсь,устойчива к стрессам','Помощник бухгалтера','Помощник бухгалтера','Y',NULL,1376092800,NULL,NULL,'N','N',1),(6,'Любая','7000','Васильев Денис Александрович',18,NULL,NULL,'N','N','',NULL,'N',NULL,NULL,'N','N','','den692@mail.ru',NULL,NULL,NULL,'нет','Образование: Средние\nУчебное заведение: 2008 ГОУ СОШ №692\nОпыт работы: нет\nО себе: ','Любая','Любая','Y',NULL,1376092800,NULL,NULL,'N','N',1),(7,'Любая','15000','Умрилова Надежда Витальевна',24,NULL,NULL,'N','N','',NULL,'Y',NULL,NULL,'Y','N','','umrilka@mail.ru',NULL,NULL,NULL,NULL,'Образование: Незаконченное высшее\nУчебное заведение: СПб Торгово - Экономический институт 2008 год\nОпыт работы: Бармен, официант, кассир (не компьютерные кассы, инкассирование денежных средств), уличный продавец продуктов питания и фастфуда), торговый представитель.\nО себе: Проживаю в Ленинградской области (15 минут от метро Проспект Просвещения), неплохое знание северных районов города, наличие собственного автомобиля. Стаж вождения 1,7 года (без аварий и лишения водительского удостоверения). Коммуникабельность, стрессоустойчивость, способность к быстрому обучению. Владение ПК на уровне пользователя, немного знаю английский язык, способности к матиматическим наукам (среднее образование в физико-математическом классе). ','Любая','Любая','Y',NULL,1376092800,NULL,NULL,'N','N',0),(8,'Кассир','13000','Баранова Оксана Анатольевна',33,NULL,NULL,'Y','Y','',NULL,'N',NULL,NULL,'N','N','','oksbar22011976@yandex.ru',NULL,NULL,NULL,NULL,'Образование: средне специальное\nУчебное заведение: 1994 ПТУ 40\nОпыт работы: работа кассиром,оптовым кассиром,продавцом-кассиром,знание компьютерной кассы и программы 1С более 6 лет\nО себе: коммуникабельна,легко обучаема,ответственна','Кассир','Кассир','Y',NULL,1376092800,NULL,NULL,'N','N',1),(9,'Главный бухгалтер, бухгалтер','5000','Гафурова Надия',34,NULL,NULL,'N','N','',NULL,'N',NULL,NULL,'Y','N','','nadija1@rambler.ru',NULL,NULL,NULL,NULL,'Образование: высшее\nУчебное заведение: Санкт-Петербургский Государственный Технологический Университет Растительных Полимеров (Санкт-Петербург) Дата окончания: Июнь 1996 года Факультет: Промышленной энергетики Специальность: Промышленная теплоэнергетика Степень: Диплом \nОпыт работы: ОПЫТ РАБОТЫ С сентября 2008 по июнь 2009 г. Главный бухгалтер (Полная занятость) в компании Производственное предприятие, г. Санкт-Петербург Описание деятельности компании: Производство Должностные обязанности: Ведение бухгалтерского и налогового учета, сдача всей отчетности, ТСНО и УСН, 1С 7.7 С апреля 2007 по август 2008 года Бухгалтер (Полная занятость) в компании Производственное предприятие, г. Санкт-Петербург Описание деятельности компании: Производство Должностные обязанности: Ведение бухучета, сдача всей отчетности, ТСНО и УСН С февраля 2005 по ноябрь 2006 года Cтатистик (Полная занятость) в компании Торговое предприятие, г. Санкт-Петербург Описание деятельности компании: Предоставление услуг для торговли Должностные обязанности: Ведение статистической отчетности, расчет заработной платы С февраля 2002 по февраль 2005 года Кассир (Полная занятость) в компании Торговое предприятие, г. Санкт-Петербург Описание деятельности компании: Предоставление услуг для торговли Должностные обязанности: Ведение кассовой книги и кассовых операций, подотчетные лица, расчет заработной платы С ноября 1997 по февраль 2002 года Контролер (Полная занятость) в компанииТорговое предприятие , г. Санкт-Петербург Описание деятельности компании: Предоставление услуг для торговли Должностные обязанности: Соблюдение порядка и правил торговли С октября 1996 по ноябрь 1997 года Инженер (Полная занятость) в компании Проектный институт, г. Санкт-Петербург Должностные обязанности: Выполнение проектных работ \nО себе: Английский: Базовый Немецкий: Базовый Уровень владения компьютером: Пользователь Компьютерные навыки: Word. Excel. Internet , 1С 7.7, Консультант ','Главный бухгалтер, бухгалтер','Главный бухгалтер, бухгалтер','Y',NULL,1376092800,NULL,NULL,'N','N',2),(10,'ВОДИТЕЛЬ','30000-','ПЕНДУС ИГОРЬ МИХАЙЛОВИЧЬ',28,NULL,NULL,'Y','Y','',NULL,'Y',NULL,NULL,'Y','N','','PENDUS-IGOR@MAIL.RU',NULL,NULL,NULL,NULL,'Образование: СРЕДНЕЕ\nУчебное заведение: \nОпыт работы: 10 ЛЕТ\nО себе: ЗНАНИЕ ТЕХНИКИ , АККУРАТНОЕ ВОЖДЕНИЕ ,ТРУДОГОЛИК . ','ВОДИТЕЛЬ','ВОДИТЕЛЬ','Y',NULL,1376092800,NULL,NULL,'N','N',1),(11,'Водитель','20000','Плотников вадим',45,NULL,NULL,'Y','Y','',NULL,'Y',NULL,NULL,'Y','N','','hohlevich2609@mail.ru',NULL,NULL,NULL,NULL,'Образование: среднее\nУчебное заведение: \nОпыт работы: 15лет\nО себе: категория В С пунктуальный,честный,образованный,физ.крепок.без аварий и вредных привычек.с рекомендацией .','Водитель','Водитель','Y',NULL,1376092800,NULL,NULL,'N','N',1),(12,'Помощник юриста',NULL,'Сухоженко Альбина Анатольевна',20,NULL,NULL,'N','Y','',NULL,'Y',NULL,NULL,'Y','N','','pten@mail.ru',NULL,NULL,NULL,NULL,'Образование: Неполное высшее\nУчебное заведение: 2011 Украинско-Российский институт (филиал)Московского государственного открытого университета\nОпыт работы: помощником юриста в юридической фирме\nО себе: Имею гражданство Украины, но в ВУЗе изучали законодательство РФ, опыт работы был в Санкт-Петербурге, имею очень большое желание работать.','Помощник юриста','Помощник юриста','Y',NULL,1376092800,NULL,NULL,'N','N',4),(13,'Любая',NULL,'Анненкова Ангелина Александровна',17,NULL,NULL,'N','Y','',NULL,'N',NULL,NULL,'N','N','','annenkova.gelya@mail.ru',NULL,NULL,NULL,NULL,'Образование: неполное среднее\nУчебное заведение: 2010 Спб ЭТК им.Д.И.Менделеева\nОпыт работы: Чайная ложка и промоутер\nО себе: Ищу любую работу','Любая','Любая','Y',NULL,1376092800,NULL,NULL,'N','N',1),(14,'Промоутер','150вчас','Арина',NULL,NULL,NULL,'N','N','',NULL,'N',NULL,NULL,'N','N','','pten12@mail.ru',NULL,NULL,NULL,NULL,'Образование: \nУчебное заведение: \nОпыт работы: есть\nО себе: 8 (952) 364-06-11','Промоутер','Промоутер','Y',NULL,1376092800,NULL,NULL,'N','N',1),(15,'Промоутер','130ч','Ольга',NULL,NULL,NULL,'N','N','',NULL,'N',NULL,NULL,'N','N','','pten12@mail.ru',NULL,NULL,NULL,'8-911-916-71-97','Образование: \nУчебное заведение: \nОпыт работы: есть\nО себе: ','Промоутер','Промоутер','Y',NULL,1376092800,NULL,NULL,'N','N',1),(16,'Менеджер по персоналу',NULL,'Эпп Ирина ',20,NULL,NULL,'N','Y','',NULL,'N',NULL,NULL,'N','N','','pten@mail.ru',NULL,NULL,NULL,NULL,'Образование: Неполное высшее\nУчебное заведение: 2013 Северо-Западный заочный технический университет\nОпыт работы: март 2009 — апрель 2009 Офис-менеджер по кадрам – ООО «МСи-ПРО» Обязанности, функции, достижения: 1)приём и распределение звонков, 2)организация снабжения офиса канцелярскими товарами, продуктами питания, техники офиса, 3)регистрация входящей/исходящей документации, 4)организация курьерской связи: внутренней и внешней, 5)приём гостей, 6)заказ билетов, гостиниц, 7)обеспечение организационно-технической деятельности офиса, 8)организация кадрового делопроизводства: ведение и учёт личных дел сотрудников в соответствии с действующим законодательством: включая прием‚ увольнение‚ отпуск‚ табель учёта рабочего времени‚ больничные листы, оформление документации на командировки, 9)учёт и ведение трудовых книжек, 10)организация подбора персонала для руководителей среднего и высшего звена, 11)участие в разработке должностных инструкций, 12) медицинское страхование сотрудников. ноябрь 2008 — февраль 2009 Финансовый консультант – НПФ «Стальфонд» Обязанности, функции, достижения: 1)подготовка и проведение презентаций 2)участие в переговорах 3)заключение договоров ОПС и НПО июнь 2007 — август 2007 Продавец-консультант – ООО «ОКЕЙ» Обязанности, функции, достижения: 1)прием товара по соответствующим документам 2)размещение товаров 3)обслуживание покупателей 4)проверка соответствия цен в прайс-листах и ценниках на товар \nО себе: коммуникабельна, ответственна','Менеджер по персоналу','Менеджер по персоналу','Y',NULL,1376092800,NULL,NULL,'N','N',2),(17,'Не указана',NULL,'Андреева Дана',23,NULL,NULL,'N','Y','',NULL,'Y',NULL,NULL,'Y','N','','dana_dr@mail.ru',NULL,NULL,NULL,'не указано','Образование: высшее\nУчебное заведение: Санкт-Петербургский Государственный Университет Технологии и Дизайна, г. Санкт-Петербург Дата окончания:	Май 2008 года Факультет:	Дизайна. Специальность:	Дизайн рекламы Форма обучения: Дневная/Очная\nОпыт работы: 3 года\nО себе: Ответственность, исполнительность, трудоспособность, целеустремленность, коммуникабельность, активность, организаторские способности, готовность к высокой динамике работы умение работать самостоятельно и в команде, желание профессионально развиваться.','Не указана','Не указана','Y',NULL,1376092800,NULL,NULL,'N','N',1),(18,'Мастер поукладке паркетной доски ламината','25000','Качмарик михаил зиновевичь',25,NULL,NULL,'N','Y','',NULL,'N',NULL,NULL,'Y','N','','loyalty@inbox.ru',NULL,NULL,NULL,NULL,'Образование: среднее\nУчебное заведение: \nОпыт работы: 1год\nО себе: гражданин украины ответственный пунктуальный свободный английский ','Мастер поукладке паркетной доски ламината','Мастер поукладке паркетной доски ламината','Y',NULL,1376092800,NULL,NULL,'N','N',1),(19,'Склейщица бумажных пакетов',NULL,'Жанна',32,NULL,NULL,'N','N','',NULL,'N',NULL,NULL,'N','N','','janna1776@mail.ru',NULL,NULL,NULL,NULL,'Образование: \nУчебное заведение: \nОпыт работы: \nО себе: ','Склейщица бумажных пакетов','Склейщица бумажных пакетов','Y',NULL,1376092800,NULL,NULL,'N','N',1),(20,'Оператор ПК','30000','Иванов Александр',18,NULL,NULL,'N','N','',NULL,'N',NULL,NULL,'N','N','','pten@mail.ru',NULL,NULL,NULL,NULL,'Образование: среднее\nУчебное заведение: школа 2009\nОпыт работы: \nО себе: ','Оператор ПК','Оператор ПК','Y',NULL,1376092800,NULL,NULL,'N','N',1),(21,'Бухгалтер-операционист,помощник бухгалтера','19000','Оксана владимировна',36,NULL,NULL,'Y','Y','',NULL,'N',NULL,NULL,'N','N','','x-fail73@mail.ru',NULL,NULL,NULL,'Бухучет','Образование: высшее техническое\nУчебное заведение: 1997 БГТУ им.Устинова\nОпыт работы: \nО себе: ','Бухгалтер-операционист,помощник бухгалтера','Бухгалтер-операционист,помощник бухгалтера','Y',NULL,1376092800,NULL,NULL,'N','N',1),(22,'Рыбообработчик','20000','Дзеркаль Светлана Владимировна',38,NULL,NULL,'Y','Y','',NULL,'N',NULL,NULL,'N','N','','trofim4ik@rambler.ru',NULL,NULL,NULL,NULL,'Образование: среднетехническое\nУчебное заведение: \nОпыт работы: 4 года\nО себе: ','Рыбообработчик','Рыбообработчик','Y',NULL,1376092800,NULL,NULL,'N','N',1),(23,'Охранник','18000-20000','Михаил',31,NULL,NULL,'Y','Y','',NULL,'N',NULL,NULL,'Y','N','','MAZUT-881@RAMBLER.RU',NULL,NULL,NULL,NULL,'Образование: среднее\nУчебное заведение: \nОпыт работы: Русский, петербуржец 31/175/82 без в/п лицензия продлена на 5 лет, последнее место работы чоп Ареал-карэ, 6 лет охрана офиса кампании ТЕЛЕФОРУМ т +79522020841\nО себе: знание пк продвинутый пользователь','Охранник','Охранник','Y',NULL,1376092800,NULL,NULL,'N','N',3),(24,'Отделочник',NULL,'Павлов михаил васильевич',48,NULL,NULL,'Y','Y','',NULL,'Y',NULL,NULL,'Y','N','','swet.pawlowa@yandex.ru',NULL,NULL,NULL,NULL,'Образование: \nУчебное заведение: \nОпыт работы: 15 лет\nО себе: ','Отделочник','Отделочник','Y',NULL,1376092800,NULL,NULL,'N','N',1),(25,'Любая в снабжении или административная','25000','Колесников Иван Григорьевич',52,NULL,NULL,'Y','Y','',NULL,'Y',NULL,NULL,'Y','N','','kigr57@mail.ru',NULL,NULL,NULL,NULL,'Образование:  Высшее\nУчебное заведение: Июль 1978г. Уссурийское высшее военное автомобильное командное училище\nОпыт работы:  1978-2000г.-служба в ВС РФ,уволился в звании -подполковник. 2000-2010г.-начальник материально-технического снабжения в \"ЗАО Электросервис-ГАЛАЦ\"\nО себе: Коммуникабелен,легко общаюсь с людьми, без вредных привычек.','Любая в снабжении или административная','Любая в снабжении или административная','Y',NULL,1376092800,NULL,NULL,'N','N',1),(26,' Повар','20000',' Шушарова Юлия Вячеславовна',24,NULL,NULL,'N','Y','',NULL,'N',NULL,NULL,'Y','N','',' shusharovajulia@mail.ru',NULL,NULL,NULL,' ','Образование:  Ср.- специальное\nУчебное заведение:  \nОпыт работы:  5 лет\nО себе:  Люблю свою работу на 100%!!!',' Повар',' Повар','Y',NULL,1376092800,NULL,NULL,'N','N',2),(27,'Отделочник',NULL,' павлов михаил васильевич',50,NULL,NULL,'Y','Y','',NULL,'Y',NULL,NULL,'Y','N','',' swet.pawlowa@yandex.ru',NULL,NULL,NULL,' ','Образование:  среднее\nУчебное заведение:  \nОпыт работы:  15 лет\nО себе:  все виды отделочных работ','Отделочник','Отделочник','Y',NULL,1376092800,NULL,NULL,'N','N',1),(28,' универсал-отделочник','30000',' павлов михаил васильевич',48,NULL,NULL,'Y','Y','',NULL,'Y',NULL,NULL,'Y','N','',' pawlow@aport.ru',NULL,NULL,NULL,' ','Образование:  среднее\nУчебное заведение:  \nОпыт работы:  \nО себе:  тел.89215631008',' универсал-отделочник',' универсал-отделочник','Y',NULL,1376092800,NULL,NULL,'N','N',1),(29,' уборщица','12000',' павлова светлана михайловна',49,NULL,NULL,'Y','Y','',NULL,'N',NULL,NULL,'N','N','',' swet.pawlowa@yandex.ru',NULL,NULL,NULL,' ','Образование:  среднее\nУчебное заведение:  \nОпыт работы:  \nО себе:  тел. 89500421264',' уборщица',' уборщица','Y',NULL,1376092800,NULL,NULL,'N','N',2),(30,' дамский парикмахер','30000',' татьяна',40,NULL,NULL,'Y','Y','',NULL,'Y',NULL,NULL,'Y','N','',' prosto7@list.ru',NULL,NULL,NULL,' ','Образование:  среднее специальное\nУчебное заведение:  \nОпыт работы:  \nО себе:  дамский парикмахер работаю на косметике лонда и шварцкопф',' дамский парикмахер',' дамский парикмахер','Y',NULL,1376092800,NULL,NULL,'N','N',1),(31,'Дамский парикмахер',NULL,' татьяна',40,NULL,NULL,'Y','Y','',NULL,'Y',NULL,NULL,'Y','N','',' prosto7@list.ru',NULL,NULL,NULL,' ','Образование:  \nУчебное заведение:  \nОпыт работы:  дамский парикмахер работаю на косметике лонда и шварцкопф\nО себе:  тел 921-898-86-20','Дамский парикмахер','Дамский парикмахер','Y',NULL,1376092800,NULL,NULL,'N','N',1),(32,' автослесарь','25000-40000',' Васильев Анлрей Игоревич',22,NULL,NULL,'N','Y','',NULL,'Y',NULL,NULL,'Y','N','','dron4ik.88.88.@mail.ru',NULL,NULL,NULL,' \"ПРОФИ\"','Образование:  среднее\nУчебное заведение:  \nОпыт работы:  2ГОДА\nО себе:  ',' автослесарь',' автослесарь','Y',NULL,1376092800,NULL,NULL,'N','N',1),(33,'Администратор','17000',' Новожилова Наталья Александровна',31,NULL,NULL,'N','Y','',NULL,'N',NULL,NULL,'Y','N','',' natusikn@list.ru',NULL,NULL,NULL,' Курсы секретарей-референтов (2001 г.), дизайн и верстка на ПК (2002 г.), агент по недвижимости (2003 г.), организация управления кадровой службой (2008 г.)','Образование:  Среднее-специальное\nУчебное заведение:  1997 г.\nОпыт работы: более 4-х лет.\nО себе:  ','Администратор','Администратор','Y',NULL,1376092800,NULL,NULL,'N','N',1),(34,' Диспетчер','16000',' Новожилова Наталья Александровна',31,NULL,NULL,'N','Y','',NULL,'N',NULL,NULL,'N','N','',' natusikn@list.ru',NULL,NULL,NULL,' Курсы секретарей-референтов (2001 г.), дизайн и верстка на ПК (2002 г.), агент по недвижимости (2003 г.), организация управления кадровой службой (2008 г.)','Образование:  Среднее-специальное\nУчебное заведение:  1997 г\nОпыт работы:  есть.\nО себе:  ',' Диспетчер',' Диспетчер','Y',NULL,1376092800,NULL,NULL,'N','N',5),(35,'Менеджер','15000',' Алексеева Надежда Юрьевна',21,NULL,NULL,'N','Y','',NULL,'N',NULL,NULL,'N','N','',' Nadigirl@mail.ru',NULL,NULL,NULL,'нет','Образование:  неполное высшее\nУчебное заведение: Санкт-Петербургский Технологический институт(Технический университет),дата окончания 2010 год\nОпыт работы: нет\nО себе:  ','Менеджер','Менеджер','Y',NULL,1376092800,NULL,NULL,'N','N',1),(36,' электромонтёр','20000',' Вильк Николай Юрьевич',36,NULL,NULL,'N','Y','',NULL,'N',NULL,NULL,'N','N','',' klaus74@list.ru',NULL,NULL,NULL,' ','Образование:  средне-специальое\nУчебное заведение:  1992 СПТУ-130\nОпыт работы:  \nО себе:  ',' электромонтёр',' электромонтёр','Y',NULL,1376092800,NULL,NULL,'N','N',1),(37,' Главный бухгалтер',NULL,'Зеленкова Галина Михайловна',58,NULL,NULL,'Y','Y','',NULL,'N',NULL,NULL,'N','N','','zelenkov700@yandex.ru',NULL,NULL,NULL,' ','Образование:  Высшее\nУчебное заведение:  \nОпыт работы:  20 лет главный бухгалтер\nО себе:  ',' Главный бухгалтер',' Главный бухгалтер','Y',NULL,1376092800,NULL,NULL,'N','N',6),(38,' И Т Р','20',' Ильин Юрий Иванович',62,NULL,NULL,'Y','Y','',NULL,'Y',NULL,NULL,'Y','N','',' starik.iljin@yandex.ru',NULL,NULL,NULL,' курсы повышения квалификацмм ( 4 ) ','Образование:  высшее\nУчебное заведение:  1975,ЛЭТИ\nОпыт работы:  разработчик систем АСУ,СУБД,электронщик\nО себе:  ',' И Т Р',' И Т Р','Y',NULL,1376092800,NULL,NULL,'N','N',1);
/*!40000 ALTER TABLE `job_resumes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_resumes_education`
--

DROP TABLE IF EXISTS `job_resumes_education`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_resumes_education` (
  `jre_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jre_jr_id` int(10) unsigned NOT NULL COMMENT 'id резюме',
  `jre_jet_id` int(11) unsigned NOT NULL COMMENT 'вид образования',
  `jre_date_start` date DEFAULT NULL COMMENT 'дата начала обучения',
  `jre_date_fin` date DEFAULT NULL COMMENT 'дата окончания',
  `jre_institution` varchar(255) DEFAULT NULL COMMENT 'учебное заведение',
  `jre_faculty` varchar(255) DEFAULT NULL COMMENT 'факультет',
  `jre_spec` varchar(255) DEFAULT NULL COMMENT 'специальность',
  PRIMARY KEY (`jre_id`),
  KEY `jre_jr_id` (`jre_jr_id`),
  KEY `jre_jet_id` (`jre_jet_id`),
  CONSTRAINT `job_resumes_education_ibfk_1` FOREIGN KEY (`jre_jr_id`) REFERENCES `job_resumes` (`jr_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `job_resumes_education_ibfk_2` FOREIGN KEY (`jre_jet_id`) REFERENCES `job_education_types` (`jet_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9346 DEFAULT CHARSET=utf8 COMMENT='Образование соискателей';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_resumes_education`
--

LOCK TABLES `job_resumes_education` WRITE;
/*!40000 ALTER TABLE `job_resumes_education` DISABLE KEYS */;
INSERT INTO `job_resumes_education` VALUES (1,949,3,'2013-09-10','2003-01-01','профессиональный лицей№35 г.зеленогорска красноярского края 2001-2003','',''),(2,954,1,'2010-09-01','2015-07-01','СПбГПУ','Механико-машиностроительный','Технология машиностроения'),(3,955,3,'2013-08-29','2003-05-25','ПТШ № 5','','ЭЛЕКТРО ГАЗОСВАРЩИК'),(4,956,3,'1982-08-31','1970-01-01','пту-112','','Радиомонтажник'),(6,958,1,'2013-09-04','2013-09-04','Санкт-Петербургский университет аэрокосмического приборостроения','Радиотехники, электроники и связи','Радиоэлектронные системы \\ Медико-биологические электронные компьютеризированные системы'),(7,962,1,'2013-09-01','2017-06-01','Санкт-петербургский морской технический университет','экономический','производственный менеджмент'),(8,963,3,'2013-09-13','2013-09-13','медицинский колледж','','сестринское дело'),(9,964,1,'2010-05-04','2010-09-01','морское кадровое агенсво','','машинист-тракторист'),(10,965,1,'2013-09-17','2013-09-17','НИУД (НОУ \"НеваЭксперт\")','коммерция','коммерция в логистике'),(11,966,3,'2004-09-01','2008-06-01','ИППЛ№6','ресторанный бизнес','Повар-кондитер');
/*!40000 ALTER TABLE `job_resumes_education` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_resumes_works`
--

DROP TABLE IF EXISTS `job_resumes_works`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_resumes_works` (
  `jrw_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jrw_jr_id` int(10) unsigned NOT NULL COMMENT 'id резюме',
  `jrw_company` varchar(255) NOT NULL COMMENT 'наименование компании',
  `jrw_region` varchar(255) DEFAULT NULL COMMENT 'регион / город',
  `jrw_web` varchar(255) DEFAULT NULL COMMENT 'сайт',
  `jrw_jc_id` int(11) DEFAULT NULL COMMENT 'сфера деятельности',
  `jrw_position` varchar(255) NOT NULL COMMENT 'должность',
  `jrw_date_start` date NOT NULL COMMENT 'приступил к работе',
  `jrw_date_fin` date DEFAULT NULL COMMENT 'уволился',
  `jrw_responsibility` text COMMENT 'описание',
  PRIMARY KEY (`jrw_id`),
  KEY `jrw_jr_id` (`jrw_jr_id`),
  CONSTRAINT `job_resumes_works_ibfk_1` FOREIGN KEY (`jrw_jr_id`) REFERENCES `job_resumes` (`jr_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18170 DEFAULT CHARSET=utf8 COMMENT='Хронологический опыт работы';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_resumes_works`
--

LOCK TABLES `job_resumes_works` WRITE;
/*!40000 ALTER TABLE `job_resumes_works` DISABLE KEYS */;
INSERT INTO `job_resumes_works` VALUES (1,949,'ФГУП Почта россии','','',18,'оператор','2012-05-01','2013-06-30',''),(2,949,'оао гатчинский хлебозавод','','',17,'упаковщик готовой продукции','2011-09-01','2012-04-30',''),(3,952,'Nestle','','',10,'Менеджер отдела продаж','2012-10-10','2013-04-15',''),(4,955,'казцинк','','',91,'электро газосварщик','2005-08-12','2013-07-20',''),(6,963,'стоматология','Санкт-Петербург','',0,'медсестра цсо','2012-12-01','2013-09-20','Дезинфекция,ПО,стерилизация медицинских инструментов.'),(7,964,'строй комфорт','санкт петербург','',84,'водитель погрузчика','2010-09-09','2013-09-01',''),(8,966,'ООО\"Невскиемолокопролдукты','Санкт-Петербург','',0,'кладовщик','2012-08-02','2013-09-23','Приемка товара на склад. сборка заказов. отгрузка товара по накладным. учет товара на складе,проведение инвентаризаций'),(9,967,'ООО «ДНС Плюс - Новокузнецк»','','',0,'Продавец - консультант','2012-03-21','2013-09-25','');
/*!40000 ALTER TABLE `job_resumes_works` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_vacancies`
--

DROP TABLE IF EXISTS `job_vacancies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_vacancies` (
  `jv_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `jv_jet_id` int(11) unsigned DEFAULT NULL COMMENT 'образование',
  `jv_jw_id` int(11) unsigned DEFAULT NULL COMMENT 'занятость',
  `jv_jc_id` int(11) unsigned DEFAULT NULL COMMENT 'id компании',
  `jv_title` varchar(255) NOT NULL COMMENT 'наименование вакансии',
  `jv_salary_from` int(10) unsigned DEFAULT NULL COMMENT 'з/п нижний предел',
  `jv_salary_to` int(10) unsigned DEFAULT NULL COMMENT 'з/п верхний предел',
  `jv_salary` varchar(255) DEFAULT NULL COMMENT 'информация о вознаграждении',
  `jv_sex` enum('male','female') DEFAULT NULL COMMENT 'пол кандидата',
  `jv_driver` enum('N','Y') NOT NULL DEFAULT 'N' COMMENT 'требуется вод.удост.',
  `jv_trips` enum('N','Y') NOT NULL DEFAULT 'N' COMMENT 'готовность к командировкам',
  `jv_date_publish` int(11) DEFAULT NULL COMMENT 'дата публикации',
  `jv_city` varchar(255) DEFAULT NULL COMMENT 'Город вакансии',
  `jv_city_id` int(10) unsigned DEFAULT NULL COMMENT 'id города',
  `jv_description` text COMMENT 'подробное описание вакансии',
  `jv_contact_fio` varchar(255) DEFAULT NULL COMMENT 'контактное лицо',
  `jv_phone` varchar(255) NOT NULL COMMENT 'контактный телефон',
  `jv_email` varchar(255) DEFAULT NULL COMMENT 'контактный email',
  `jv_status` enum('N','Y') NOT NULL DEFAULT 'Y' COMMENT 'отображать на сайте',
  `jv_meta_description` varchar(255) DEFAULT NULL,
  `jv_meta_keywords` varchar(255) DEFAULT NULL,
  `jv_src_url_alias` varchar(7) DEFAULT NULL COMMENT 'Сокращенное название сайта-источника',
  `jv_src_id` int(10) unsigned DEFAULT NULL COMMENT 'ID вакансии на сайте-источнике',
  `jv_erp_id` varchar(255) DEFAULT NULL COMMENT 'идентификатор из ERP',
  `jv_date_begin` date DEFAULT NULL COMMENT 'начало показа',
  `jv_date_end` date DEFAULT NULL COMMENT 'завершить показ',
  `jv_views` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'кол-во просмотров',
  PRIMARY KEY (`jv_id`),
  UNIQUE KEY `jv_src_url_alias` (`jv_src_url_alias`,`jv_src_id`),
  UNIQUE KEY `jv_erp_id` (`jv_erp_id`),
  KEY `sv_status` (`jv_status`),
  KEY `jv_jet_id` (`jv_jet_id`),
  KEY `jv_jw_id` (`jv_jw_id`),
  KEY `jv_jc_id` (`jv_jc_id`),
  KEY `jv_city_id` (`jv_city_id`),
  CONSTRAINT `job_vacancies_ibfk_1` FOREIGN KEY (`jv_jc_id`) REFERENCES `job_companies` (`jc_id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=1412613 DEFAULT CHARSET=utf8 COMMENT='Вакансии';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_vacancies`
--

LOCK TABLES `job_vacancies` WRITE;
/*!40000 ALTER TABLE `job_vacancies` DISABLE KEYS */;
INSERT INTO `job_vacancies` VALUES (1,NULL,NULL,NULL,'продавцы – консультанты',NULL,14000,'14000 руб',NULL,'N','N',1376092800,'Санкт-Петербург',NULL,'Производителю бытовой техники электроводонагревателей ТЕРМЕКС требуются: продавцы – консультанты, разные р-ны города. ЗП от 14 000 р. Резюме thermex@thermex.ru или (812) 346-57-77 доб.218\n\nКонтактная информация: thermex@thermex.ru 346-57-77\n\n',NULL,'dddddddd','pten@mail.ru','Y','продавцы – консультанты','продавцы – консультанты',NULL,NULL,NULL,NULL,NULL,14),(2,NULL,NULL,NULL,'Мерчендайзеров ',NULL,15000,'15000 руб',NULL,'N','N',1376092800,'Санкт-Петербург',NULL,'приглашаем на работу Мерчендайзеров активных и коммуникабельных, ОБЯЗАТЕЛЬНО с действующей мед.книжкой заработная плата 15 000 руб, тел. 322-99-75 Анастасия \n\nКонтактная информация:  322-99-75\n\n',NULL,'','pten@mail.ru','Y','Мерчендайзеров ','Мерчендайзеров ',NULL,NULL,NULL,NULL,NULL,14),(3,NULL,NULL,NULL,'Продавец в сеть бутиков ж/одежды',NULL,20000,'20000 руб',NULL,'N','N',1376092800,'Санкт-Петербург',NULL,'Продавец в сеть бутиков ж/одежды Ж 18-45, можно без о/р з/п от 20000т.р.+бонус г/р: 5/2 (10-16ч или 16-21ч) Т: 570-26-70,570-22-06, 310-14-07, 310-64-21. ЦЗ \n\nКонтактная информация:  570-26-70\n\n',NULL,'','pten@mail.ru','Y','Продавец в сеть бутиков ж/одежды','Продавец в сеть бутиков ж/одежды',NULL,NULL,NULL,NULL,NULL,15),(4,NULL,NULL,NULL,'Продавец в сеть бутиков ж/одежды',NULL,20000,'20000 руб',NULL,'N','N',1376092800,'Санкт-Петербург',NULL,'Продавец в сеть бутиков ж/одежды Ж 18-45, можно без о/р з/п от 20000т.р.+бонус г/р: 5/2 (10-16ч или 16-21ч) Т: 570-26-70,570-22-06, 310-14-07, 310-64-21. ЦЗ \n\nКонтактная информация:  570-26-70\n\n',NULL,'','pten@mail.ru','Y','Продавец в сеть бутиков ж/одежды','Продавец в сеть бутиков ж/одежды',NULL,NULL,NULL,NULL,NULL,10);
/*!40000 ALTER TABLE `job_vacancies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_vacancies_blacklist_lnk`
--

DROP TABLE IF EXISTS `job_vacancies_blacklist_lnk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_vacancies_blacklist_lnk` (
  `jvbl_su_id` int(10) unsigned NOT NULL COMMENT 'пользователь',
  `jvbl_jv_id` int(10) unsigned NOT NULL COMMENT 'вакансия',
  `jvbl_date` int(11) NOT NULL COMMENT 'дата занесения в ЧС',
  KEY `jvbl_su_id` (`jvbl_su_id`),
  KEY `jvbl_jv_id` (`jvbl_jv_id`),
  CONSTRAINT `job_vacancies_blacklist_lnk_ibfk_1` FOREIGN KEY (`jvbl_su_id`) REFERENCES `site_users` (`su_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `job_vacancies_blacklist_lnk_ibfk_2` FOREIGN KEY (`jvbl_jv_id`) REFERENCES `job_vacancies` (`jv_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Black list';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_vacancies_blacklist_lnk`
--

LOCK TABLES `job_vacancies_blacklist_lnk` WRITE;
/*!40000 ALTER TABLE `job_vacancies_blacklist_lnk` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_vacancies_blacklist_lnk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_vacancies_favorites_lnk`
--

DROP TABLE IF EXISTS `job_vacancies_favorites_lnk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_vacancies_favorites_lnk` (
  `su_id` int(10) unsigned NOT NULL COMMENT 'пользователь',
  `jv_id` int(10) unsigned NOT NULL COMMENT 'вакансия',
  `date_created` int(11) NOT NULL COMMENT 'когда добавлено в избранные',
  UNIQUE KEY `su_id` (`su_id`,`jv_id`),
  KEY `jv_id` (`jv_id`),
  CONSTRAINT `job_vacancies_favorites_lnk_ibfk_1` FOREIGN KEY (`su_id`) REFERENCES `site_users` (`su_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `job_vacancies_favorites_lnk_ibfk_2` FOREIGN KEY (`jv_id`) REFERENCES `job_vacancies` (`jv_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Favorite Vacancies';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_vacancies_favorites_lnk`
--

LOCK TABLES `job_vacancies_favorites_lnk` WRITE;
/*!40000 ALTER TABLE `job_vacancies_favorites_lnk` DISABLE KEYS */;
INSERT INTO `job_vacancies_favorites_lnk` VALUES (1,1197975,1394957664),(2,1,1394918855),(2,2,1394918891);
/*!40000 ALTER TABLE `job_vacancies_favorites_lnk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_vacancies_project_lnk`
--

DROP TABLE IF EXISTS `job_vacancies_project_lnk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_vacancies_project_lnk` (
  `jvpl_cp_id` int(11) unsigned NOT NULL COMMENT 'CMS проект',
  `jvpl_jv_id` int(11) unsigned NOT NULL COMMENT 'вакансия',
  UNIQUE KEY `jvpl_cp_id` (`jvpl_cp_id`,`jvpl_jv_id`),
  CONSTRAINT `job_vacancies_project_lnk_ibfk_1` FOREIGN KEY (`jvpl_cp_id`) REFERENCES `cms_projects` (`fp_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Link vacancies to projects';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_vacancies_project_lnk`
--

LOCK TABLES `job_vacancies_project_lnk` WRITE;
/*!40000 ALTER TABLE `job_vacancies_project_lnk` DISABLE KEYS */;
INSERT INTO `job_vacancies_project_lnk` VALUES (1,19979),(1,19980),(1,19981),(1,19982),(1,19985),(1,19987),(1,19988),(1,19989),(1,19990),(1,19991),(1,19992),(1,19993),(1,19994),(1,19995),(1,19996),(1,19997),(1,19998),(1,19999),(1,20000),(1,20002),(1,20003),(1,20004),(1,20005),(1,20006),(1,20007),(1,20008),(1,20009),(1,20010),(1,20011),(1,20012),(1,20015),(1,20016),(1,20017),(1,20018),(1,20019),(1,20020),(1,20021),(1,20022),(1,20023),(1,20024),(1,20025),(1,20026),(1,20027),(1,20028),(1,20029),(1,20030),(1,20031),(1,20034),(1,20035),(1,20037),(1,20045),(1,20046),(1,20047),(1,20048),(1,20052),(1,20053),(1,20054),(1,20055),(1,20057),(1,20058),(1,20059),(1,20060),(1,20065),(1,20069),(1,20070),(1,20072),(1,20073),(1,20074),(1,20075),(1,20082),(1,20083),(1,20084),(1,20085),(1,20086);
/*!40000 ALTER TABLE `job_vacancies_project_lnk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_workbusy`
--

DROP TABLE IF EXISTS `job_workbusy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_workbusy` (
  `jw_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `jw_title` varchar(32) NOT NULL COMMENT 'Тип занятости',
  PRIMARY KEY (`jw_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='Занятость';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_workbusy`
--

LOCK TABLES `job_workbusy` WRITE;
/*!40000 ALTER TABLE `job_workbusy` DISABLE KEYS */;
INSERT INTO `job_workbusy` VALUES (1,'полный рабочий день'),(2,'неполный рабочий день'),(3,'свободный график'),(4,'временная работа'),(5,'удалённая работа'),(6,'сменный график'),(7,'вахта');
/*!40000 ALTER TABLE `job_workbusy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_articles`
--

DROP TABLE IF EXISTS `site_articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_articles` (
  `sa_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sa_title` varchar(255) NOT NULL COMMENT 'заголовок статьи',
  `sa_content` text NOT NULL COMMENT 'текст статьи',
  `sa_date_publish` int(10) unsigned DEFAULT NULL COMMENT 'дата записи',
  `sa_date_add` datetime DEFAULT NULL COMMENT 'дата записи',
  `sa_date_change` datetime DEFAULT NULL COMMENT 'дата изменения',
  `sa_image` enum('N','Y') NOT NULL DEFAULT 'N' COMMENT 'есть ли фото',
  `sa_status` enum('N','Y') NOT NULL DEFAULT 'N' COMMENT 'статус статьи',
  `sa_type` enum('article','dict') NOT NULL DEFAULT 'article' COMMENT 'статья или словарь',
  PRIMARY KEY (`sa_id`),
  KEY `sa_type` (`sa_type`)
) ENGINE=InnoDB AUTO_INCREMENT=456 DEFAULT CHARSET=utf8 COMMENT='Статьи';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_articles`
--

LOCK TABLES `site_articles` WRITE;
/*!40000 ALTER TABLE `site_articles` DISABLE KEYS */;
INSERT INTO `site_articles` VALUES (1,'Как найти работу после 40 лет','<p>\r\n	<span style=\"color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\">Зрелый возраст &ndash; не повод считать, что всё хорошее, в том числе достойная работа, уже позади. И всё же соискатели, перешагнувшие рубеж 40-45 лет, сталкиваются с определёнными трудностями при поиске работы. Как же их преодолеть?&nbsp;</span><br />\r\n	<br style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\" />\r\n	<b style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\">На позитивной волне</b><br style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\" />\r\n	<span style=\"color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\">Перед тем как начать поиск работы, настройтесь на успех. Не допускайте мысли о бесперспективности Ваших начинаний. Помните, что причины, по которым поиск работы может затянуться, могут появиться в любом возрасте: выпускникам вузов мешает отсутствие опыта, молодым сотрудницам &ndash; наличие маленьких детей и т. п. Ваш козырь &ndash; это профессионализм и многолетний опыт работы. И если Вы уверены в себе, то Ваш возраст из недостатка легко превратится в плюс.</span><br style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\" />\r\n	<br style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\" />\r\n	<b style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\">Ищите свою нишу</b><br style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\" />\r\n	<span style=\"color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\">На рынке труда, безусловно, существуют сферы, где возрастной ценз остаётся жёстким, хотя ограничение по возрасту и противоречит ТК РФ. Это продажи, связи с общественностью, информационные технологии. Верхнюю возрастную планку работодатели устанавливают потому, что считают молодых сотрудников более способными к обучению, более выносливыми и активными. Отсюда вывод: работу стоит искать там, где в первую очередь нужны Ваши знания и опыт. Возраст &laquo;за 40&raquo; &ndash; не помеха для ищущих новую работу бухгалтеров, медицинских работников, юристов и инженеров. Но не забывайте держать руку на пульсе, то есть освежать свои профессиональные знания, овладевать новыми программами, необходимыми Вам в работе.</span><br style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\" />\r\n	<br style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\" />\r\n	<b style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\">На ступень выше</b><br style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\" />\r\n	<span style=\"color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\">Как правило, соискатели в возрасте 40-45 лет и старше, стремящиеся как можно скорее найти работу, существенно занижают свои ожидания, соглашаясь на маленькую зарплату и неприметную должность. Такая стратегия часто приводит к неудаче, поэтому советуем Вам попробовать совершенно иной ход, отправив резюме на руководящую должность. Работодатели понимают: энергию и креативность молодёжи необходимо направлять в нужное русло, а для этого нужен человек, обладающий жизненным и профессиональным опытом, внимательностью и организационными навыками.&nbsp;</span><br style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\" />\r\n	<br style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\" />\r\n	<b style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\">Резюме: расставьте акценты</b><br style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\" />\r\n	<span style=\"color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\">Чтобы работодатель понял, насколько Вы ценный сотрудник, следует уделить больше внимания составлению резюме. Вам необязательно описывать три-пять последних мест работы. Достаточно указать одно-два последних места (название организации, сферу деятельности, годы работы) и распределить весь Ваш богатый опыт по блокам (к примеру, &laquo;Управленческая деятельность&raquo;, &laquo;Преподавательская деятельность&raquo; и др.). Такое резюме называется функциональным и позволяет работодателю увидеть Ваши сильные стороны, не акцентируя внимание на долгих годах рабочего стажа, а значит, и на Вашем возрасте. Не забудьте указать курсы повышения квалификации, если они имели место в последние годы. Должно быть ясно: Вы идёте в ногу со временем и постоянно следите за изменениями в профессиональной среде, готовы учиться и узнавать новое.&nbsp;</span><br style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\" />\r\n	<br style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\" />\r\n	<b style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\">Обман не пройдёт</b><br style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\" />\r\n	<span style=\"color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\">Нередко соискатели зрелого возраста сомневаются, стоит ли им указывать в резюме свой точный возраст. В принципе можно этого не делать. Если вопрос о возрасте не будет задан Вам в ходе телефонного разговора, то на собеседовании Вы получите шанс убедить работодателя в своей незаменимости и Ваш возраст уже не будет его смущать. Если же, узнав дату Вашего рождения, Вам всё-таки откажут, - не огорчайтесь, вокруг немало работодателей, ценящих опыт и квалификацию. Чего не стоит делать, так это менять свой возраст в сторону уменьшения. Такая уловка не поможет, ведь при оформлении на работу у Вас всё равно попросят паспорт. Не портите отношения с работодателем, пытаясь его обмануть, это сведёт на нет все Ваши усилия.</span><br style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\" />\r\n	<br style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\" />\r\n	<b style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\">Собеседование: уверенность, стиль, такт&nbsp;</b><br style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\" />\r\n	<span style=\"color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\">Наконец, Вы получили приглашение на собеседование. Как одеться? Беспроигрышным вариантом будет деловой стиль &ndash; костюм классического кроя, неброские цвета, минимум аксессуаров. Но при этом Вы должны выглядеть моложаво и энергично, всем свои обликом демонстрируя, что на этом месте Вы собираетесь не досиживать до пенсии, а активно работать, принося пользу себе и компании. В разговоре с рекрутёром будьте тактичны, ни в коем случае не жалуйтесь на то, как Вам непросто трудоустроиться: &laquo;Возраст, сами понимаете&raquo;. Помните, что Вас воспринимают так, как Вы сами себя ощущаете. Отправляйтесь на собеседование с настроением победителя &ndash; и результат Вас приятно удивит!</span><br style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\" />\r\n	<br style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\" />\r\n	<span style=\"color: rgb(0, 0, 0); font-family: Arial, Tahoma, Verdana; font-size: 13px; line-height: 16px;\">Удачного Вам трудоустройства!&nbsp;</span></p>',1372622400,'2013-08-09 01:55:45','2013-08-09 17:04:53','N','Y','article'),(2,'Как вести себя на собеседовании: советы психолога','<p>\r\n	<span style=\"margin: 0px; padding: 0px; font-family: verdana, arial, helvetica; text-align: justify; color: rgb(0, 153, 255); font-size: medium;\">Собеседование &ndash; это встреча равных сторон!</span><br style=\"margin: 0px; padding: 0px;\" />\r\n	<span style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: verdana, arial, helvetica; text-align: justify; font-size: medium;\">Это самое главное, что вы должны помнить на собеседовании. Вы пришли не как проситель, а как профессионал &ndash; выяснить, насколько вам подходят предлагаемые работодателем условия и оплата труда. Иными словами, вам сделали деловое предложение, и в процессе собеседования вы решаете, стоит или не стоит его принять.<br style=\"margin: 0px; padding: 0px;\" />\r\n	<br style=\"margin: 0px; padding: 0px;\" />\r\n	<span style=\"margin: 0px; padding: 0px; color: rgb(51, 153, 255);\">Собеседование есть самопрезентация, а не исповедь!</span><br style=\"margin: 0px; padding: 0px;\" />\r\n	Конечно, необходимо придерживаться фактов, но любую информацию можно подать выгодно для себя. Например, &quot;Временно не работаю&quot; звучит лучше, чем &quot;Безработный&quot;. Или, если возраст солидный, то &quot;Мои дети уже выросли, и я могу больше сил отдавать работе&quot;. Если же пока опыта работы нет, то вспомните на собеседовании об учебе: &quot;Мне интересна эта профессия, и для того, чтобы ее овладеть, я сделал следующее&hellip;&quot;.<br style=\"margin: 0px; padding: 0px;\" />\r\n	<br style=\"margin: 0px; padding: 0px;\" />\r\n	<span style=\"margin: 0px; padding: 0px; color: rgb(51, 153, 255);\">Отвечайте на вопросы с удовольствием</span><br style=\"margin: 0px; padding: 0px;\" />\r\n	Здороваетесь ли вы, называете ли свое имя, отвечаете на вопросы &ndash; все это нужно делать с удовольствием! Вы обязаны настроиться на волну успеха и позитива. Чрезвычайно важно, с каким настроением вы пришли на собеседование, с какой интонацией и выражением лица вы говорите. Итак, третье правило гласит: доброжелательность, спокойствие, уверенность и правдивость.<br style=\"margin: 0px; padding: 0px;\" />\r\n	<br style=\"margin: 0px; padding: 0px;\" />\r\n	<span style=\"margin: 0px; padding: 0px; color: rgb(51, 153, 255);\">Позаботьтесь о первом впечатлении</span><br style=\"margin: 0px; padding: 0px;\" />\r\n	Все мы знаем об эффекте первого впечатления, которое складывается в первые секунды знакомства. Вы только открыли дверь, вошли в кабинет, а ваш собеседник уже имеет о вас определенное представление. Какое? А вот это полностью зависит от вас.<br style=\"margin: 0px; padding: 0px;\" />\r\n	<br style=\"margin: 0px; padding: 0px;\" />\r\n	Поэтому, при подготовке к собеседованию необходимо создать настроение победителя &ndash; &quot;я доволен собой, у меня есть все, что мне нужно, я открыт миру и позитивно воспринимаю любые его проявления&quot;. Как это сделать?&nbsp;<br style=\"margin: 0px; padding: 0px;\" />\r\n	прежде, чем войти в кабинет, вспомните что-то очень хорошее и держите мысленно этот образ во время беседы;&nbsp;<br style=\"margin: 0px; padding: 0px;\" />\r\n	одеться в стиле, принятом в фирме, для чего полезно провести предварительную разведку. Если это невозможно, оденьтесь на собеседование в деловом стиле. При этом вам нужно чувствовать себя естественно и комфортно &ndash; одежда должна быть удобной и не &quot;только что из магазина&quot;;<br style=\"margin: 0px; padding: 0px;\" />\r\n	документы о трудовом опыте, об образовании и их копии, паспорт, писчую бумагу, ручки: все, что вы хотите взять с собой на собеседование, положите в солидную папку;<br style=\"margin: 0px; padding: 0px;\" />\r\n	вы должны обязательно знать имя человека, с которым вам предстоит встретиться.&nbsp;<br style=\"margin: 0px; padding: 0px;\" />\r\n	<br style=\"margin: 0px; padding: 0px;\" />\r\n	<span style=\"margin: 0px; padding: 0px; color: rgb(51, 153, 255);\">Дополнительные сведения о собеседовании</span><br style=\"margin: 0px; padding: 0px;\" />\r\n	Теперь остановимся на некоторых вопросах собеседования более подробно.&nbsp;<br style=\"margin: 0px; padding: 0px;\" />\r\n	Прийти на собеседование необходимо чуть раньше назначенного времени &ndash; у вас будет возможность оценить обстановку, почитать внутрифирменную информацию<br style=\"margin: 0px; padding: 0px;\" />\r\n	С самого начала беседы умейте слышать вопрос и отвечать на него конкретно и полно.<br style=\"margin: 0px; padding: 0px;\" />\r\n	Рассказывая о себе, используйте активные глаголы: &quot;я умею&quot;, &quot;я владею&quot; и т.д. Употребление в речи сочетаний, типа &quot;как бы&quot;, &quot;всего лишь&quot;, &quot;немного&quot;, &quot;судя по всему&quot;, &quot;наверное&quot; и т.д. на собеседовании не допустимо, т.к., создает впечатление о говорящем как о неуверенном в себе человеке, непригодном для серьезной и ответственной работы. Понижают впечатление и такие самоуничижительные высказывания, как &quot;я не оратор&quot;, &quot;я еще малоопытный специалист&quot;, &quot;я &ndash; человек новый&quot;. Поскольку на собеседовании оценивается интеллектуальный и культурный уровень претендента, его словарный запас избегайте сленга, слов паразитов (Кто ясно говорит, тот ясно мыслит). Профессиональный жаргон уместен лишь как вкрапления, ненавязчиво показывающие вашу компетентность.<br style=\"margin: 0px; padding: 0px;\" />\r\n	Вопросы на собеседовании могут задаваться любые. И к этому надо быть готовым. Есть категория особо хитрых интервьюеров, которые знают, что опытные кандидаты, прошедшие не одно собеседование, всегда имеют домашние заготовки на наиболее типичные вопросы, поэтому они, например, не спросят прямо &quot;Почему Вы ушли с прежнего места работы?&quot;, а предпочтут обходной маневр: &quot;Что должно измениться на вашем прежнем рабочем месте, чтобы Вы согласились туда вернуться?&quot;. Такой вопрос позволяет выяснить истинные, а не декларированные причины ухода человека с предыдущей должности.<br style=\"margin: 0px; padding: 0px;\" />\r\n	Работодатель может задавать такие безобидные вопросы, как, например: &quot;Что вы делали вчера вечером?&quot;. Таким образом, скорее всего, хотят узнать о стиле жизни работника и о том, как он проводит свое свободное время. Здесь нужно рассказывать о своих увлечениях и хобби, при этом воодушевлено и со знанием дела.<br style=\"margin: 0px; padding: 0px;\" />\r\n	Вопрос о сильных сторонах обычно смущает. Рассказ о своих достоинствах на собеседовании вполне этично начать так: &quot;Коллеги по работе (или друзья) говорят, что я&hellip;&quot;. Далее сделать упор на профессиональные навыки, обучаемость и положительные характеристики, относящиеся к предполагаемой работе. Хорошо, если вы приведете конкретные примеры. Что касается &quot;минусов&quot;, то их надо подавать так, чтобы звучало как &quot;плюс&quot;: &quot;Нет опыта работы, но есть большая заинтересованность и желание работать&quot;. В данном контексте также хорошо выручают шутки, парочку которых лучше подобрать заранее, ведь импровизация, как известно, &ndash; всегда результат серьезной подготовки.<br style=\"margin: 0px; padding: 0px;\" />\r\n	Если у вас есть маленькие дети, обратите внимание на следующий вариант ответа на вопрос &quot;Не помешают ли маленькие дети работе?&quot; &ndash; &quot;Я думала над этим вопросом и решила его&quot;.<br style=\"margin: 0px; padding: 0px;\" />\r\n	В последней части собеседования обычно интересуются, есть ли у кандидата вопросы. По их содержанию оценивается качество вашей мотивации. Вот примеры некоторых тем:&nbsp;<br style=\"margin: 0px; padding: 0px;\" />\r\n	в чем заключается главная проблема этой работы,<br style=\"margin: 0px; padding: 0px;\" />\r\n	насколько важна эта работа для фирмы,<br style=\"margin: 0px; padding: 0px;\" />\r\n	что хорошего было в моем предшественнике,<br style=\"margin: 0px; padding: 0px;\" />\r\n	какой человек на этом месте был бы наиболее эффективен для вас,<br style=\"margin: 0px; padding: 0px;\" />\r\n	могу ли я встретиться со своим непосредственным начальником,<br style=\"margin: 0px; padding: 0px;\" />\r\n	как примерно будет выглядеть мой рабочий день.<br style=\"margin: 0px; padding: 0px;\" />\r\n	Остается один, пожалуй, наиболее важный, и зачастую решающий вопрос &ndash; о заработной плате. Что вы должны обязательно сделать: произвести маркетинг аналогичных позиций. Опыт проведения собеседований показывает, что в случае заинтересованности работодателя в соискателе, он сам начнет обсуждать вопрос оплаты труда. Если она мала, уместно спросить: &quot;Какой у вас пакет компенсаций?&quot;. Когда у кандидата выясняют, какие деньги он хотел бы получать, необходимо назвать адекватную сумму и держать паузу. Кто первым заговорил, тот и проиграл. Если же вопрос о зарплате вообще не поднимался, вы вправе задать его самостоятельно. Психологически лучше спрашивать таким образом: &quot;Какую зарплату принято платить в фирме на этой должности?&quot; или &quot;На какую зарплату я могу претендовать?&quot;.&nbsp;<br style=\"margin: 0px; padding: 0px;\" />\r\n	Не обрекайте себя на муки длительного ожидания. Сократить его до одной недели вполне в вашей власти. Прощаясь, спросите: &quot;Когда я могу узнать о результатах: в начале недели или в конце, когда мне позвонить?&quot;</span></p>',1373227200,'2013-08-09 01:56:16','2013-08-09 17:03:10','N','Y','article');
/*!40000 ALTER TABLE `site_articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_cities`
--

DROP TABLE IF EXISTS `site_cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_cities` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `city` varchar(200) NOT NULL COMMENT 'рускоязычное название',
  `city_eng` varchar(200) NOT NULL COMMENT 'англоязычное название',
  `country_id` int(11) NOT NULL DEFAULT '0' COMMENT '<fk> country.id',
  `pos` int(11) NOT NULL DEFAULT '0' COMMENT 'Поле сортировки',
  `main_selected` enum('N','Y') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq` (`city`),
  KEY `country_id` (`country_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1975 DEFAULT CHARSET=utf8 COMMENT='Список городов';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_cities`
--

LOCK TABLES `site_cities` WRITE;
/*!40000 ALTER TABLE `site_cities` DISABLE KEYS */;
INSERT INTO `site_cities` VALUES (1,'Санкт-Петербург','Saint Petersburg',1,2,'Y'),(4,'Алматы','Almaty',4,0,'N'),(3,'Москва','Moscow',1,1,'Y'),(5,'Владивосток','Vladivostok',1,0,'Y'),(6,'Донецк','Donezk',72,0,'Y'),(7,'Екатеринбург','Ekaterinburg',1,3,'Y'),(8,'Казань','Kazan',1,0,'Y'),(9,'Киев','Kiev',72,0,'Y'),(11,'Магнитогорск','Magnitogorsk',1,0,'N'),(12,'Минск','Minsk',73,0,'N'),(13,'Новосибирск','Novosibirsk',1,4,'Y'),(14,'Пермь','Perm',1,0,'Y'),(15,'Рига','Riga',18,0,'N'),(16,'Улан-Удэ','Ulan-Ude',1,0,'N'),(17,'Ханты-Мансийск','Khanti-Mansisk',1,0,'N'),(18,'Харьков','Kharkov',72,0,'Y'),(25,'Самара','Samara',1,5,'Y'),(20,'Запорожье','Zaporozhye',72,0,'Y'),(26,'Тольятти','Tolyatti',1,0,'N'),(27,'Томск','Tomsk',1,0,'N'),(28,'Уфа','Ufa',1,0,'Y'),(29,'Ухта','Ukhta',1,0,'N'),(30,'Хабаровск','Khabarovsk',1,0,'Y'),(31,'Якутск','Yakutsk',1,0,'N'),(32,'Алексеевка','Alekseyevka',1,0,'N'),(33,'Астана','Astana',4,0,'N'),(34,'Владимир','Vladimir',1,0,'N'),(35,'Иркутск','Irkutsk',1,0,'Y'),(36,'Красноярск','Krasnoyarsk',1,0,'Y'),(38,'Нижний Новгород','Nizhni Novgorod',1,0,'Y'),(39,'Омск','Omsk',1,0,'Y'),(40,'Армянск','Armansk',72,0,'N'),(41,'Баку','Baku',66,0,'N'),(42,'Нью-Йорк','New York',128,0,'N'),(43,'Лондон','London',71,0,'N'),(44,'Бишкек','Bishkek',132,0,'N'),(45,'Душанбе','Dushanbe',135,0,'N'),(46,'Ташкент','Tashkent',130,0,'N'),(47,'Кременчуг','Kremenchug',1,0,'N'),(48,'Харцызск','Kharzyzsk',72,0,'N'),(49,'Гомель','Gomel',73,0,'N'),(50,'Holmdel','Holmdel',128,0,'N'),(53,'Berkeley, CA','Berkeley, CA',128,0,'N'),(55,'Волгоград','Volgograd',1,0,'Y'),(56,'Вологда','Vologda',1,0,'N'),(57,'Краснодар','Krasnodar',1,0,'Y'),(58,'Киров','Kirov',1,0,'N'),(59,'Берлин','Berlin',87,0,'N'),(60,'Norwalk','Norwalk',0,0,'N'),(61,'Стокгольм','Stockholm',121,0,'N'),(62,'Салават','Salavat',1,0,'N'),(63,'Сыктывкар','Syktyvkar',1,0,'N'),(64,'Челябинск','Chelyabinsk',1,0,'Y'),(65,'Белгород','Belgorod',1,0,'N'),(66,'Караганда','Karaganda',4,0,'N'),(67,'Никосия','Nicosia',81,0,'N'),(68,'Принстон','Princeton',128,0,'N'),(70,'Салоники','Thessaloniki',47,0,'N'),(71,'Монте Карло','Monte Carlo',150,0,'N'),(72,'Warerloo','Waterloo',0,0,'N'),(73,'Вашингтон','Washington DC',128,0,'N'),(74,'Люксембург','Luxembourg',100,0,'N'),(75,'Вена','Vienna',65,0,'N'),(76,'St. Louis','St. Louis',0,0,'N'),(77,'Ульяновск','Ulyanovsk',1,0,'N'),(78,'Norfolk','Norfolk',0,0,'N'),(79,'Treviso','Treviso',0,0,'N'),(80,'София','Sofia',6,0,'N'),(81,'Hod Hkasharon','Hod Hkasharon',0,0,'N'),(82,'Абу Даби','Abu Dhabi',126,0,'N'),(83,'Greenwich','Greenwich',71,0,'N'),(84,'Франкфурт','Frankfurt',87,0,'N'),(85,'Brno-Jundrov','Brno-Jundrov',0,0,'N'),(86,'Чикаго','Chicago',128,0,'N'),(87,'Boca Raton Florida','Boca Raton Florida',128,0,'N'),(88,'Манила','Manila',32,0,'N'),(89,'Париж','Paris',85,0,'N'),(90,'Стамбул','Istanbul',29,0,'N'),(91,'Милан','Milano',95,0,'N'),(92,'Монако','Monaco',150,0,'N'),(93,'Прага','Prague',44,0,'N'),(94,'Berchem','Berchem',0,0,'N'),(95,'Набережные Челны','Naberezhnye Chelny',1,0,'N'),(96,'Львов','Lviv',72,0,'Y'),(97,'Одесса','Odessa',72,0,'Y'),(98,'Лугано','Lugano',122,0,'N'),(99,'Сантьяго','Santiago',34,0,'N'),(100,'Гонконг','Hong Kong',49,0,'N'),(101,'Иваново','',1,0,'N'),(102,'Калининград','',1,0,'Y'),(103,'Кемерово','',1,0,'N'),(104,'Курск','',1,0,'N'),(105,'Новочеркасск','',1,0,'N'),(106,'Озерск','',1,0,'N'),(107,'Республика Татарстан','',1,0,'N'),(108,'Ростов-на-Дону','Rostov-on-Don',1,0,'Y'),(109,'Саратов','Saratov',1,0,'Y'),(110,'Ставрополь','',1,0,'N'),(111,'Таганрог','Taganrog',1,0,'N'),(112,'Тверь','Tver',1,0,'N'),(113,'Тула','Tula',1,0,'N'),(114,'Тюмень','Tumen',1,0,'N'),(115,'Ярославль','',1,0,'N'),(116,'Горно-Алтайск','',1,0,'N'),(117,'Воронеж','',1,0,'N'),(118,'Батайск','',1,0,'N'),(119,'Аксай','',1,0,'N'),(120,'Азов','',1,0,'N'),(121,'Усолье-Сибирское','',1,0,'N'),(122,'Семикаракорск','',1,0,'N'),(123,'Волгодонск','',1,0,'N'),(124,'Кагальницкая, ст.','',1,0,'N'),(125,'Каменск-Шахтинский','',1,0,'N'),(126,'Константиновск','',1,0,'N'),(127,'Красный Сулин','',1,0,'N'),(128,'Миллерово','',1,0,'N'),(129,'Пролетарск','',1,0,'N'),(130,'Сальск','',1,0,'N'),(131,'Цимлянск','',1,0,'N'),(132,'Чалтырь','',1,0,'N'),(133,'Шахты','',1,0,'N'),(134,'Орловский','',1,0,'N'),(135,'Зерноград','',1,0,'N'),(136,'Сиракузы','Syracuse',95,0,'N'),(137,'Тамбов','Tambov',1,0,'N'),(138,'Оренбург','Orenburg',1,0,'N'),(139,'Пестрицы','',1,0,'N'),(140,'Ижевск','Izhevsk',1,0,'N'),(141,'Кострома','',1,0,'N'),(142,'Рязань','',1,0,'N'),(143,'Московская обл.','',1,0,'N'),(144,'Ленинградская обл.','',1,0,'N'),(145,'Архангельск','Arkhangelsk',1,0,'N'),(146,'Нижний Тагил','Nizny Tagil',1,0,'N'),(147,'Астрахань','',1,0,'N'),(148,'Барнаул','',1,0,'N'),(149,'Березники','',1,0,'N'),(150,'Владикавказ','',1,0,'N'),(151,'Выборг','',1,0,'N'),(152,'Майкоп','',1,0,'N'),(153,'Новокузнецк','',1,0,'N'),(154,'Орск','',1,0,'N'),(155,'Петропавловск-Камчатский','',1,0,'N'),(156,'Петрозаводск','',1,0,'N'),(157,'Сочи','',1,0,'N'),(158,'Чебоксары','',1,0,'N'),(159,'Южно-Сахалинск','',1,0,'N'),(160,'Благовещенск','',1,0,'N'),(161,'Брянск','',1,0,'N'),(162,'Йошкар-Ола','',1,0,'N'),(163,'Калуга','',1,0,'N'),(164,'Липецк','',1,0,'N'),(165,'Магадан','',1,0,'N'),(166,'Нальчик','',1,0,'N'),(167,'Находка','',1,0,'N'),(168,'Новороссийск','',1,0,'N'),(169,'Пенза','',1,0,'N'),(170,'Смоленск','',1,0,'N'),(171,'Чита','',1,0,'N'),(172,'Салехард','Salekhard',1,0,'N'),(173,'Орел','Orel',1,0,'N'),(174,'Великий Новгород','Veliky Novgorod',1,0,'N'),(175,'Анадырь','',1,0,'N'),(176,'Мурманск','',1,0,'N'),(177,'Обнинск','',1,0,'N'),(178,'Саяногорск','',1,0,'N'),(179,'Ростов','',1,0,'N'),(180,'Пятигорск','',1,0,'N'),(181,'Стерлитамак','',1,0,'N'),(182,'Сургут','',1,0,'N'),(183,'Курган','',1,0,'N'),(184,'Коломна','',1,0,'N'),(185,'Саранск','',1,0,'N'),(186,'Усть-Илимск','',1,0,'N'),(187,'Чайковский','',1,0,'N'),(188,'Нягань','',1,0,'N'),(189,'Альметьевск','',1,0,'N'),(190,'Винница','Vinnitsa',72,0,'N'),(191,'Днепропетровск','',72,0,'Y'),(192,'Сумы','',72,0,'N'),(193,'Ивано-Франковск','',72,0,'N'),(194,'Таллинн','Tallinn',83,0,'N'),(195,'Лимассол','Limassol',81,0,'N'),(196,'Кирово-Чепецк','Kirovo-Chepetsk',1,0,'N'),(197,'Бронницы','',1,0,'N'),(198,'Дмитров','',1,0,'N'),(199,'Домодедово','',1,0,'N'),(200,'Дубна','',1,0,'N'),(201,'Егорьевск','',1,0,'N'),(202,'Звенигород','',1,0,'N'),(203,'Истра','',1,0,'N'),(204,'Кашира','',1,0,'N'),(205,'Клин','',1,0,'N'),(206,'Куровское','',1,0,'N'),(207,'Луховицы','',1,0,'N'),(208,'Люберцы','',1,0,'N'),(209,'Михнево','',1,0,'N'),(210,'Можайск','',1,0,'N'),(211,'Наро-Фоминск','',1,0,'N'),(212,'Ногинск','',1,0,'N'),(213,'Одинцово','',1,0,'N'),(214,'Подольск','',1,0,'N'),(215,'Пущино','',1,0,'N'),(216,'Руза','',1,0,'N'),(217,'Сергиев Посад','',1,0,'N'),(218,'Серпухов','',1,0,'N'),(219,'Солнечногорск','',1,0,'N'),(220,'Ступино','',1,0,'N'),(221,'Талдом','',1,0,'N'),(222,'Чехов (Московская обл.)','',1,0,'N'),(223,'Шатура','',1,0,'N'),(224,'Щёлково','',1,0,'N'),(225,'Электросталь','',1,0,'N'),(226,'Ангарск','',1,0,'N'),(227,'Арзамас','',1,0,'N'),(228,'Братск','',1,0,'N'),(229,'Ворсма','',1,0,'N'),(230,'Заволжье','',1,0,'N'),(231,'Миасс','',1,0,'N'),(232,'Павлово','',1,0,'N'),(233,'Саянск','',1,0,'N'),(234,'Сергач','',1,0,'N'),(235,'Балаково','',1,0,'N'),(236,'Рыбинск','',1,0,'N'),(237,'Белебей','',1,0,'N'),(238,'Белорецк','',1,0,'N'),(239,'Бирск','',1,0,'N'),(240,'Ишимбай','',1,0,'N'),(241,'Кумертау','',1,0,'N'),(242,'Мелеуз','',1,0,'N'),(243,'Нефтекамск','',1,0,'N'),(244,'Октябрьский','',1,0,'N'),(245,'Сибай','',1,0,'N'),(246,'Туймазы','',1,0,'N'),(247,'Учалы','',1,0,'N'),(248,'Димитровград','',1,0,'N'),(249,'Нефтегорск','',1,0,'N'),(250,'Новокуйбышевск','',1,0,'N'),(251,'Похвистнево','',1,0,'N'),(252,'Абакан','',1,0,'N'),(253,'Гусиноозерск','',1,0,'N'),(254,'Дудинка','',1,0,'N'),(255,'Железногорск','',1,0,'N'),(256,'Жуковка','',1,0,'N'),(257,'Заполярный','',1,0,'N'),(258,'Зеленогорск (Красноярский край)','',1,0,'N'),(259,'Элиста','',1,0,'N'),(260,'Сызрань','',1,0,'N'),(261,'Северск','',1,0,'N'),(262,'Северобайкальск','',1,0,'N'),(263,'Кувандык','',1,0,'N'),(264,'Кызыл','',1,0,'N'),(265,'Минусинск','',1,0,'N'),(266,'Мончегорск','',1,0,'N'),(267,'Красногорск','',1,0,'N'),(268,'Кисловодск','',1,0,'N'),(269,'Южно-Курильск','',1,0,'N'),(270,'Северо-Курильск','',1,0,'N'),(271,'Тымовское','',1,0,'N'),(272,'Поронайск','',1,0,'N'),(273,'Корсаков','',1,0,'N'),(274,'Анива','',1,0,'N'),(275,'Невельск','',1,0,'N'),(276,'Холмск','',1,0,'N'),(277,'Томари','',1,0,'N'),(278,'Макаров','',1,0,'N'),(279,'Смирных','',1,0,'N'),(280,'Углегорск','',1,0,'N'),(281,'Ноглики','',1,0,'N'),(282,'Охта','',1,0,'N'),(283,'Вахрушев','',1,0,'N'),(284,'Чехов (Сахалин)','',1,0,'N'),(285,'Норильск','',1,0,'N'),(286,'Никель','',1,0,'N'),(287,'Зеленоград','Zelenograd',1,0,'N'),(288,'Славгород','',1,0,'N'),(289,'Рубцовск','',1,0,'N'),(290,'Бийск','',1,0,'N'),(291,'Шелехов','',1,0,'N'),(292,'Псков','',1,0,'N'),(293,'Балашов','',1,0,'N'),(294,'Бузулук','',1,0,'N'),(295,'Ахтубинск','',1,0,'N'),(296,'Камызяк','',1,0,'N'),(297,'Камышин','',1,0,'N'),(298,'Волжский','',1,0,'N'),(299,'Махачкала','',1,0,'N'),(300,'Ачинск','',1,0,'N'),(301,'Боготол','',1,0,'N'),(302,'Енисейск','',1,0,'N'),(303,'Канск','',1,0,'N'),(304,'Кодинск','',1,0,'N'),(305,'Лесосибирск','',1,0,'N'),(306,'Назарово','',1,0,'N'),(307,'Ужур','',1,0,'N'),(308,'Черногорск','',1,0,'N'),(309,'Шарыпово','',1,0,'N'),(310,'Игарка','',1,0,'N'),(311,'Нижневартовск','',1,0,'N'),(312,'Лабытнанги','',1,0,'N'),(313,'Когалым','',1,0,'N'),(314,'Надым','',1,0,'N'),(315,'Нефтеюганск','',1,0,'N'),(316,'Ноябрьск','',1,0,'N'),(317,'Тобольск','',1,0,'N'),(318,'Новый Уренгой','',1,0,'N'),(319,'Соликамск','',1,0,'N'),(320,'Усинск','',1,0,'N'),(321,'Прокопьевск','',1,0,'N'),(322,'Воркута','',1,0,'N'),(323,'Шадринск','',1,0,'N'),(324,'Далматово','',1,0,'N'),(325,'Макушино','',1,0,'N'),(326,'Златоуст','',1,0,'N'),(327,'Новошахтинск','',1,0,'N'),(328,'Анапа','',1,0,'N'),(329,'Армавир','',1,0,'N'),(330,'Азнакаево','',1,0,'N'),(331,'Актаныш, с.','',1,0,'N'),(332,'Арск','',1,0,'N'),(333,'Бавлы','',1,0,'N'),(334,'Бугульма','',1,0,'N'),(335,'Буинск','',1,0,'N'),(336,'Елабуга','',1,0,'N'),(337,'Заинск','',1,0,'N'),(338,'Зеленодольск','',1,0,'N'),(339,'Лениногорск','',1,0,'N'),(340,'Нижнекамск','',1,0,'N'),(341,'Нурлат','',1,0,'N'),(342,'Чистополь','',1,0,'N'),(343,'Зея','',1,0,'N'),(344,'Новобурейский (пос.)','',1,0,'N'),(345,'Райчихинск','',1,0,'N'),(346,'Свободный','',1,0,'N'),(347,'Высокая Гора, п.','',1,0,'N'),(348,'Лаишево, р.п.','',1,0,'N'),(349,'Пестрецы, с.','',1,0,'N'),(350,'Рыбная Слобода, с.','',1,0,'N'),(351,'Богатые Сабы, с.','',1,0,'N'),(352,'Тюлячи, с.','',1,0,'N'),(353,'Каменск-Уральский','',1,0,'N'),(354,'Туапсе','',1,0,'N'),(355,'Лебедянь','',1,0,'N'),(356,'Мегион','',1,0,'N'),(357,'Королев','',1,0,'N'),(358,'Раменское','',1,0,'N'),(359,'Стрежевой','',1,0,'N'),(360,'Череповец','Cherepovets',1,0,'N'),(361,'Тосно','',1,0,'N'),(362,'Протвино','',1,0,'N'),(363,'Краснокаменск','',1,0,'N'),(365,'Котлас','',1,0,'N'),(366,'Северодвинск','',1,0,'N'),(367,'Кириши','',1,0,'N'),(368,'Балашиха','',1,0,'N'),(369,'Железнодорожный','',1,0,'N'),(370,'Жуковский','',1,0,'N'),(371,'Дедовск','',1,0,'N'),(372,'Ново-Петровское','',1,0,'N'),(373,'Павловская Слобода','',1,0,'N'),(374,'Павловский Посад','',1,0,'N'),(375,'Пушкино','',1,0,'N'),(376,'Фрязино','',1,0,'N'),(377,'Хотьково','',1,0,'N'),(378,'Узловая','',1,0,'N'),(379,'Черноголовка','',1,0,'N'),(380,'Новомосковск','',1,0,'N'),(381,'Старый Оскол','',1,0,'N'),(382,'Химки','',1,0,'N'),(383,'Ивано-Франковская обл.','',72,0,'N'),(386,'Крым, Автономная Республика','',72,0,'N'),(387,'Волынская обл.','',72,0,'N'),(388,'Винницкая обл.','',72,0,'N'),(389,'Днепропетровская обл.','',72,0,'N'),(390,'Донецкая обл.','',72,0,'N'),(391,'Житомирская обл.','',72,0,'N'),(392,'Закарпатская обл.','',72,0,'N'),(393,'Запорожская обл.','',72,0,'N'),(394,'Киевская обл.','',72,0,'N'),(395,'Кировоградская обл.','',72,0,'N'),(396,'Луганская обл.','',72,0,'N'),(397,'Львовская обл.','',72,0,'N'),(398,'Севастополь','',72,0,'N'),(399,'Николаевская обл.','',72,0,'N'),(400,'Одесская обл.','',72,0,'N'),(401,'Полтавская обл.','',72,0,'N'),(402,'Ровенская обл.','',72,0,'N'),(403,'Сумская обл.','',72,0,'N'),(404,'Тернопольская обл.','',72,0,'N'),(405,'Харковская обл.','',72,0,'N'),(406,'Херсонская обл.','',72,0,'N'),(407,'Хмельницкая обл.','',72,0,'N'),(408,'Черкасская обл.','',72,0,'N'),(409,'Черновицкая обл.','',72,0,'N'),(410,'Черниговская обл.','',72,0,'N'),(411,'Бердск','',1,0,'N'),(412,'Верхняя Салда','',1,0,'N'),(413,'Ленинск-Кузнецкий','',1,0,'N'),(414,'Льгов','',1,0,'N'),(415,'Тим (пос.)','',1,0,'N'),(416,'Курчатов','',1,0,'N'),(417,'Новотроицк','',1,0,'N'),(418,'Новоорск','',1,0,'N'),(419,'Лысьва','',1,0,'N'),(420,'Комсомольск-на-Амуре','',1,0,'N'),(421,'Артем','',1,0,'N'),(422,'Дальнереченск','',1,0,'N'),(423,'Партизанск','',1,0,'N'),(424,'Славянка (пос.)','',1,0,'N'),(425,'Уссурийск','',1,0,'N'),(426,'Гаврилов-Ям','',1,0,'N'),(427,'Данилов','',1,0,'N'),(428,'Мышкин','',1,0,'N'),(429,'Переславль-Залесский','',1,0,'N'),(430,'Тутаев','',1,0,'N'),(431,'Углич','',1,0,'N'),(432,'Солигалич','',1,0,'N'),(433,'Шарья','',1,0,'N'),(434,'Клинцы','',1,0,'N'),(435,'Мытищи','',1,0,'N'),(436,'Джалиль','',1,0,'N'),(437,'Губкинский','',1,0,'N'),(438,'Абинск','',1,0,'N'),(439,'Апшеронск','',1,0,'N'),(440,'Армавир-1','',1,0,'N'),(441,'Брюховецкая, ст.','',1,0,'N'),(442,'Выселки, ст.','',1,0,'N'),(443,'Горячий Ключ','',1,0,'N'),(444,'Гулькевичи','',1,0,'N'),(445,'Ейск','',1,0,'N'),(446,'Кавказская, ст.','',1,0,'N'),(447,'Калининская, ст.','',1,0,'N'),(448,'Каневская, ст.','',1,0,'N'),(449,'Лабинск','',1,0,'N'),(450,'Павловская, ст.','',1,0,'N'),(451,'Приморско-Ахтарск','',1,0,'N'),(452,'Старощербиновская, ст.','',1,0,'N'),(453,'Успенское, с.','',1,0,'N'),(454,'Урай','',1,0,'N'),(455,'Лангепас','',1,0,'N'),(456,'Покачи','',1,0,'N'),(457,'Нарьян-Мар','',1,0,'N'),(458,'Сосновый Бор','',1,0,'N'),(459,'Печора','',1,0,'N'),(460,'Кингисепп','',1,0,'N'),(461,'Смирновка-2, пос.','',1,0,'N'),(462,'Ломоносов','',1,0,'N'),(463,'Петродворец','',1,0,'N'),(464,'Сестрорецк','',1,0,'N'),(465,'Кронштадт','',1,0,'N'),(466,'Колпино','',1,0,'N'),(467,'Пушкин','',1,0,'N'),(468,'Тихвин','',1,0,'N'),(469,'Ивангород','',1,0,'N'),(470,'Сланцы','',1,0,'N'),(471,'Гатчина','',1,0,'N'),(472,'Волосово','',1,0,'N'),(473,'Коммунар','',1,0,'N'),(474,'Подпорожье','',1,0,'N'),(475,'Луга','',1,0,'N'),(476,'Всеволожск','',1,0,'N'),(477,'Приозерск','',1,0,'N'),(478,'Кировск','',1,0,'N'),(479,'Волхов-2','',1,0,'N'),(480,'Советск','',1,0,'N'),(481,'Зеленоградск','',1,0,'N'),(482,'Гвардейск','',1,0,'N'),(483,'Североморск','',1,0,'N'),(484,'Апатиты','',1,0,'N'),(485,'Ковдор','',1,0,'N'),(486,'Кандалакша','',1,0,'N'),(487,'Снежногорск','',1,0,'N'),(488,'Костомукша','',1,0,'N'),(489,'Сортавала','',1,0,'N'),(490,'Сегежа','',1,0,'N'),(491,'Беломорск','',1,0,'N'),(492,'Чудово','',1,0,'N'),(493,'Боровичи','',1,0,'N'),(494,'Валдай','',1,0,'N'),(495,'Старая Русса','',1,0,'N'),(496,'Опочка','',1,0,'N'),(497,'Порхов','',1,0,'N'),(498,'Великие Луки','',1,0,'N'),(499,'Черняховск','',1,0,'N'),(500,'Гиагинская, ст.','',1,0,'N'),(501,'Кошехабль, а.','',1,0,'N'),(502,'Красногвардейское, с.','',1,0,'N'),(503,'Тульский, п.','',1,0,'N'),(504,'Тахтамукай, а.','',1,0,'N'),(505,'Понежукай, а.','',1,0,'N'),(506,'Адыгейск','',1,0,'N'),(507,'Хакуринохабль, а.','',1,0,'N'),(508,'Алейск','',1,0,'N'),(509,'Белокуриха','',1,0,'N'),(510,'Заринск','',1,0,'N'),(511,'Змеиногорск','',1,0,'N'),(512,'Камень-на-Оби','',1,0,'N'),(513,'Новоалтайск','',1,0,'N'),(514,'Алтайское, с.','',1,0,'N'),(515,'Баево, с.','',1,0,'N'),(516,'Благовещенка, пгт','',1,0,'N'),(517,'Бурла, с.','',1,0,'N'),(518,'Быстрый Исток, с.','',1,0,'N'),(519,'Волчиха, с.','',1,0,'N'),(520,'Новоегорьевское, с.','',1,0,'N'),(521,'Ельцовка, с.','',1,0,'N'),(522,'Завьялово, с. (Алтай)','',1,0,'N'),(523,'Залесово, с.','',1,0,'N'),(524,'Зональное, с.','',1,0,'N'),(525,'Калманка, с.','',1,0,'N'),(526,'Ключи, с.','',1,0,'N'),(527,'Косиха, с.','',1,0,'N'),(528,'Красногорское, с. (Алтай)','',1,0,'N'),(529,'Краснощеково, с.','',1,0,'N'),(530,'Крутиха, с.','',1,0,'N'),(531,'Кулунда, с.','',1,0,'N'),(532,'Курья, с.','',1,0,'N'),(533,'Кытманово, с.','',1,0,'N'),(534,'Горняк, с.','',1,0,'N'),(535,'Мамонтово, с.','',1,0,'N'),(536,'Михайловское, с.','',1,0,'N'),(537,'Новичиха, с.','',1,0,'N'),(538,'Павловск, р.п.','',1,0,'N'),(539,'Панкрушиха, с.','',1,0,'N'),(540,'Петропавловское, с.','',1,0,'N'),(541,'Биробиджан','',1,0,'N'),(542,'Ванино, пгт','',1,0,'N'),(543,'Поспелиха, с.','',1,0,'N'),(544,'Ребриха, с.','',1,0,'N'),(545,'Родино, с.','',1,0,'N'),(546,'Романово, с.','',1,0,'N'),(547,'Верх-Суетка, с.','',1,0,'N'),(548,'Смоленское, с.','',1,0,'N'),(549,'Советское, с.','',1,0,'N'),(550,'Солонешное, с.','',1,0,'N'),(551,'Солтон, с.','',1,0,'N'),(552,'Табуны, с.','',1,0,'N'),(553,'Тальменка, р.п.','',1,0,'N'),(554,'Тогул, с.','',1,0,'N'),(555,'Топчиха, с.','',1,0,'N'),(556,'Староалейское, с.','',1,0,'N'),(557,'Троицкое, с.','',1,0,'N'),(558,'Тюменцево, с.','',1,0,'N'),(559,'Угловское, с.','',1,0,'N'),(560,'Усть-Калманка, с.','',1,0,'N'),(561,'Усть-Пристань, с.','',1,0,'N'),(562,'Хабары, с.','',1,0,'N'),(563,'Целинное, с.','',1,0,'N'),(564,'Чарышское, с.','',1,0,'N'),(565,'Шелаболиха, с.','',1,0,'N'),(566,'Шипуново, с.','',1,0,'N'),(567,'Гальбштадт, с.','',1,0,'N'),(568,'Яровое','',1,0,'N'),(569,'Акуша, с.','',1,0,'N'),(570,'Ахты, с.','',1,0,'N'),(571,'Бабаюрт, с.','',1,0,'N'),(572,'Ботлих, с.','',1,0,'N'),(573,'Буйнакск','',1,0,'N'),(574,'Гуниб, с.','',1,0,'N'),(575,'Уркарах, с.','',1,0,'N'),(576,'Дербент','',1,0,'N'),(577,'Усухчай, с.','',1,0,'N'),(578,'Дылым, с.','',1,0,'N'),(579,'Маджалис, с.','',1,0,'N'),(580,'Карабудахкент, с.','',1,0,'N'),(581,'Н. Каякент, с.','',1,0,'N'),(582,'Кизляр','',1,0,'N'),(583,'Леваши, с.','',1,0,'N'),(584,'Магарамкент, с.','',1,0,'N'),(585,'Новолакское, с.','',1,0,'N'),(586,'Сергокала, с.','',1,0,'N'),(587,'Касумкент, с.','',1,0,'N'),(588,'Хучни, с.','',1,0,'N'),(589,'Тарумовка, с.','',1,0,'N'),(590,'Тлярата, с.','',1,0,'N'),(591,'Хасавюрт','',1,0,'N'),(592,'Хунзах, с.','',1,0,'N'),(593,'Кидеро, с.','',1,0,'N'),(594,'Хебда, с.','',1,0,'N'),(595,'Дагестанские Огни','',1,0,'N'),(596,'Избербаш','',1,0,'N'),(597,'Каспийск','',1,0,'N'),(598,'Кизилюрт','',1,0,'N'),(599,'Южносухокумск','',1,0,'N'),(600,'Карата, с.','',1,0,'N'),(601,'Бежта, с.','',1,0,'N'),(602,'Гергебель, с.','',1,0,'N'),(603,'Мехельта, с.','',1,0,'N'),(604,'Вачи, с.','',1,0,'N'),(605,'Коркмаскала, с.','',1,0,'N'),(606,'Курах, с.','',1,0,'N'),(607,'Кумух, с.','',1,0,'N'),(608,'Т-Мектеб, с.','',1,0,'N'),(609,'Рутул, с.','',1,0,'N'),(610,'Унцукуль, с.','',1,0,'N'),(611,'Хив, с.','',1,0,'N'),(612,'Агвали, с.','',1,0,'N'),(613,'Цуриб, с.','',1,0,'N'),(614,'Тпиг, с.','',1,0,'N'),(615,'Назрань','',1,0,'N'),(616,'Черкесск','',1,0,'N'),(617,'Пудож','',1,0,'N'),(618,'Олонец','',1,0,'N'),(619,'Пряжа, п.','',1,0,'N'),(620,'Питкяранта','',1,0,'N'),(621,'Лахденпохья','',1,0,'N'),(622,'Суоярви','',1,0,'N'),(623,'Кондопога','',1,0,'N'),(624,'Медвежьегорск','',1,0,'N'),(625,'Муезерский, п.','',1,0,'N'),(626,'Кемь','',1,0,'N'),(627,'Калевала, п.','',1,0,'N'),(628,'Лоухи, п.','',1,0,'N'),(629,'Волжск','',1,0,'N'),(630,'Козьмодемьянск','',1,0,'N'),(631,'Звенигово','',1,0,'N'),(632,'Килемары, п.','',1,0,'N'),(633,'Куженер, п.','',1,0,'N'),(634,'Мари-Турек, п.','',1,0,'N'),(635,'Медведево, п.','',1,0,'N'),(636,'Морки, п.','',1,0,'N'),(637,'Новый Торъял, п.','',1,0,'N'),(638,'Оршанка, п.','',1,0,'N'),(639,'Параньга, п.','',1,0,'N'),(640,'Сернур, п.','',1,0,'N'),(641,'Советский, п.','',1,0,'N'),(642,'Юрино, п.','',1,0,'N'),(643,'Белая Гора, п.','',1,0,'N'),(644,'Алдан','',1,0,'N'),(645,'Чокурдах, п.','',1,0,'N'),(646,'Амга, с.','',1,0,'N'),(647,'Саскылах, с.','',1,0,'N'),(648,'Тикси, п.','',1,0,'N'),(649,'Верхневилюйск, с.','',1,0,'N'),(650,'Зырянка, п.','',1,0,'N'),(651,'Батагай, п.','',1,0,'N'),(652,'Вилюйск','',1,0,'N'),(653,'Бердигестях, с.','',1,0,'N'),(654,'Жиганск, с.','',1,0,'N'),(655,'Сангар, п.','',1,0,'N'),(656,'Ленск','',1,0,'N'),(657,'Майя, с.','',1,0,'N'),(658,'Мирный','',1,0,'N'),(659,'Хонуу, с.','',1,0,'N'),(660,'Намцы, с.','',1,0,'N'),(661,'Нерюнгри','',1,0,'N'),(662,'Черский, п.','',1,0,'N'),(663,'Нюрба','',1,0,'N'),(664,'Усть-Нера, п.','',1,0,'N'),(665,'Олекминск','',1,0,'N'),(666,'Оленек, с.','',1,0,'N'),(667,'Среднеколымск','',1,0,'N'),(668,'Сунтар, с.','',1,0,'N'),(669,'Ытык-Кюель, с.','',1,0,'N'),(670,'Хандыга, п.','',1,0,'N'),(671,'Борогонцы, с.','',1,0,'N'),(672,'Усть-Мая, п.','',1,0,'N'),(673,'Депутатский, п.','',1,0,'N'),(674,'Покровск','',1,0,'N'),(675,'Чурапча, с.','',1,0,'N'),(676,'Бытагай-Алыта, с.','',1,0,'N'),(677,'Первоуральск','',1,0,'N'),(678,'Грозный','',1,0,'N'),(679,'Агинское, п.','',1,0,'N'),(680,'Палана, пгт','',1,0,'N'),(681,'Тура, п.','',1,0,'N'),(682,'Бокситогорск','',1,0,'N'),(683,'Волхов','',1,0,'N'),(684,'Лодейное Поле','',1,0,'N'),(685,'Пикалево','',1,0,'N'),(686,'Бодайбо','',1,0,'N'),(687,'Балаганск, п.','',1,0,'N'),(688,'Жигалово, п.','',1,0,'N'),(689,'Залари, п.','',1,0,'N'),(690,'Казачинское, с.','',1,0,'N'),(691,'Ербогачен, с.','',1,0,'N'),(692,'Качуг, п.','',1,0,'N'),(693,'Куйтун, п.','',1,0,'N'),(694,'Мама, п.','',1,0,'N'),(695,'Нижнеудинск','',1,0,'N'),(696,'Железногорск-Илимский','',1,0,'N'),(697,'Еланцы, с.','',1,0,'N'),(698,'Слюдянка','',1,0,'N'),(699,'Тайшет','',1,0,'N'),(700,'Тулун','',1,0,'N'),(701,'Усть-Кут','',1,0,'N'),(702,'Усть-Уда','',1,0,'N'),(703,'Черемхово','',1,0,'N'),(704,'Чуна, п.','',1,0,'N'),(705,'Киренск','',1,0,'N'),(706,'Усть-Ордынский, п.','',1,0,'N'),(707,'Кутулик, п.','',1,0,'N'),(708,'Баяндай, с.','',1,0,'N'),(709,'Бохан, п.','',1,0,'N'),(710,'Новонукутский, п.','',1,0,'N'),(711,'Балахна','',1,0,'N'),(712,'Богородск','',1,0,'N'),(713,'Бор','',1,0,'N'),(714,'Выкса','',1,0,'N'),(715,'Городец','',1,0,'N'),(716,'Кстово','',1,0,'N'),(717,'Дзержинск','',1,0,'N'),(718,'Кулебаки','',1,0,'N'),(719,'Саров','',1,0,'N'),(720,'Ардатов, п.г.т.','',1,0,'N'),(721,'Б.Болдино, с.','',1,0,'N'),(722,'Б.Мурашкино, р.п.','',1,0,'N'),(723,'Бутурлино, п.','',1,0,'N'),(724,'Вад, с.','',1,0,'N'),(725,'Варнавино, п.г.т.','',1,0,'N'),(726,'Вача, р.п.','',1,0,'N'),(727,'Ветлуга','',1,0,'N'),(728,'Вознесенское, р.п.','',1,0,'N'),(729,'Володарск','',1,0,'N'),(730,'Воротынец, р.п.','',1,0,'N'),(731,'Воскресенское, п.','',1,0,'N'),(732,'Гагино, с.','',1,0,'N'),(733,'Д-Константиново, р.п.','',1,0,'N'),(734,'Дивеево, с.','',1,0,'N'),(735,'Княгинино','',1,0,'N'),(736,'Ковернино, р.п.','',1,0,'N'),(737,'Красные Баки, п.','',1,0,'N'),(738,'Уразовка, с.','',1,0,'N'),(739,'Лукоянов','',1,0,'N'),(740,'Лысково','',1,0,'N'),(741,'Навашино','',1,0,'N'),(742,'Перевоз','',1,0,'N'),(743,'Первомайск','',1,0,'N'),(744,'Пильна, р.п.','',1,0,'N'),(745,'Починки, с.','',1,0,'N'),(746,'Семенов','',1,0,'N'),(747,'Сеченово, с.','',1,0,'N'),(748,'Сокольское, п.','',1,0,'N'),(749,'Сосновское, п.','',1,0,'N'),(750,'Спасское, п.','',1,0,'N'),(751,'Тонкино, р.п.','',1,0,'N'),(752,'Тоншаево, п.','',1,0,'N'),(753,'Урень','',1,0,'N'),(754,'Чкаловск','',1,0,'N'),(755,'Шаранга, р.п.','',1,0,'N'),(756,'Шатки, р.п.','',1,0,'N'),(757,'Шахунья','',1,0,'N'),(758,'Баган, с.','',1,0,'N'),(759,'Барабинск','',1,0,'N'),(760,'Болотное','',1,0,'N'),(761,'Венгерово, с.','',1,0,'N'),(762,'Довольное, с.','',1,0,'N'),(763,'Здвинск, с.','',1,0,'N'),(764,'Искитим','',1,0,'N'),(765,'Карасук','',1,0,'N'),(766,'Каргат','',1,0,'N'),(767,'Колывань, р.п.','',1,0,'N'),(768,'Коченево, р.п.','',1,0,'N'),(769,'Кочки, с.','',1,0,'N'),(770,'Краснозерское, р.п.','',1,0,'N'),(771,'Куйбышев','',1,0,'N'),(772,'Купино','',1,0,'N'),(773,'Кыштовка, с.','',1,0,'N'),(774,'Маслянино, р.п.','',1,0,'N'),(775,'Мошково, р.п.','',1,0,'N'),(776,'Обь','',1,0,'N'),(777,'Ордынское, р.п.','',1,0,'N'),(778,'Северное, с.','',1,0,'N'),(779,'Сузун, р.п.','',1,0,'N'),(780,'Татарск','',1,0,'N'),(781,'Тогучин','',1,0,'N'),(782,'Убинское, с.','',1,0,'N'),(783,'Усть-Тарка, с.','',1,0,'N'),(784,'Чаны, р.п.','',1,0,'N'),(785,'Черепаново','',1,0,'N'),(786,'Чистоозерное, р.п.','',1,0,'N'),(787,'Чулым','',1,0,'N'),(788,'Вильнюс','Vilnjus',20,0,'N'),(789,'Суворов','',1,0,'N'),(790,'Александровск','',1,0,'N'),(791,'Гремячинск','',1,0,'N'),(792,'Губаха','',1,0,'N'),(793,'Кизел','',1,0,'N'),(794,'Краснокамск','',1,0,'N'),(795,'Кунгур','',1,0,'N'),(796,'Чусовой','',1,0,'N'),(797,'Барда, с.','',1,0,'N'),(798,'Березовка, с.','',1,0,'N'),(799,'Б. Соснова, с.','',1,0,'N'),(800,'Верещагино','',1,0,'N'),(801,'Горнозаводск','',1,0,'N'),(802,'Добрянка','',1,0,'N'),(803,'Елово, с.','',1,0,'N'),(804,'Ильинский, п.','',1,0,'N'),(805,'Карагай, с.','',1,0,'N'),(806,'Усть-Кишерть, с.','',1,0,'N'),(807,'Красновишерск','',1,0,'N'),(808,'Куеда, п.','',1,0,'N'),(809,'Нытва','',1,0,'N'),(810,'Октябрьский, п.','',1,0,'N'),(811,'Орда, с.','',1,0,'N'),(812,'Оса','',1,0,'N'),(813,'Оханск','',1,0,'N'),(814,'Очер','',1,0,'N'),(815,'Сива, с.','',1,0,'N'),(816,'Суксун, п.','',1,0,'N'),(817,'Уинское, с.','',1,0,'N'),(818,'Усолье','',1,0,'N'),(819,'Частые, с.','',1,0,'N'),(820,'Чердынь','',1,0,'N'),(821,'Чернушка','',1,0,'N'),(822,'Звездный, п.','',1,0,'N'),(823,'Кудымкар','',1,0,'N'),(824,'Гайны, п.','',1,0,'N'),(825,'Коса, с.','',1,0,'N'),(826,'Кочево, с.','',1,0,'N'),(827,'Юрла, с.','',1,0,'N'),(828,'Юсьва, с.','',1,0,'N'),(829,'Кривой Рог','Kryvoi Rog',72,0,'Y'),(830,'Багаевская, ст.','',1,0,'N'),(831,'Боковская, ст.','',1,0,'N'),(832,'Казанская, ст.','',1,0,'N'),(833,'Весёлый, п.','',1,0,'N'),(834,'Романовская, ст.','',1,0,'N'),(835,'Дубовское, с.','',1,0,'N'),(836,'Егорлыкская, ст.','',1,0,'N'),(837,'Заветное, с.','',1,0,'N'),(838,'Зимовники, п.','',1,0,'N'),(865,'Донецк (Ростовская обл.)','',1,0,'N'),(840,'Глубокий, п.','',1,0,'N'),(841,'Кашары, с.','',1,0,'N'),(842,'Куйбышево, с.','',1,0,'N'),(843,'Большая Мартыновка, сл.','',1,0,'N'),(844,'Матвеево-Курган, п.','',1,0,'N'),(845,'Милютинская, ст.','',1,0,'N'),(846,'Морозовск','',1,0,'N'),(847,'Чалтырь, с.','',1,0,'N'),(848,'Покровское, с.','',1,0,'N'),(849,'Обливская, ст.','',1,0,'N'),(850,'Каменоломни, п.','',1,0,'N'),(851,'Орловский, п.','',1,0,'N'),(852,'Песчанокопское, с.','',1,0,'N'),(853,'Ремонтное, с.','',1,0,'N'),(854,'Родионово-Несветайская, сл.','',1,0,'N'),(855,'Тарасовский, р.п.','',1,0,'N'),(856,'Тацинская, ст.','',1,0,'N'),(857,'Усть-Донецкий, р.п.','',1,0,'N'),(858,'Целина, п.','',1,0,'N'),(859,'Чертково, п.','',1,0,'N'),(860,'Вешенская, ст.','',1,0,'N'),(861,'Советская, ст.','',1,0,'N'),(862,'Белая Калитва','',1,0,'N'),(863,'Гуково','',1,0,'N'),(864,'Зверево','',1,0,'N'),(866,'Кишинев','Kishnau',105,0,'N'),(867,'Неаполь','Napoli',95,0,'N'),(868,'Братислава','Bratislava',26,0,'N'),(869,'Комсомольск','Komsomolsk',72,0,'N'),(870,'Троицк','',1,0,'N'),(871,'Долгопрудный','',1,0,'N'),(872,'Реутов','',1,0,'N'),(873,'Видное','',1,0,'N'),(874,'Ивантеевка','',1,0,'N'),(875,'Красноармейск','',1,0,'N'),(876,'Воскресенск','',1,0,'N'),(877,'Орехово-Зуево','',1,0,'N'),(878,'Волоколамск','',1,0,'N'),(879,'Серебряные Пруды, п.','',1,0,'N'),(880,'Лобня','',1,0,'N'),(881,'Зарайск','',1,0,'N'),(882,'Озеры','',1,0,'N'),(883,'Бостон','Boston',128,0,'N'),(884,'Старое Дрожжаное, с.','',1,0,'N'),(885,'Тетюши','',1,0,'N'),(886,'Апастово, с.','',1,0,'N'),(887,'Камское Устье, р.п.','',1,0,'N'),(888,'Кайбицы, с.','',1,0,'N'),(889,'Верхний Услон, с.','',1,0,'N'),(891,'Большая Атня, с.','',1,0,'N'),(892,'Балтаси, с.','',1,0,'N'),(895,'Кукмор, с.','',1,0,'N'),(896,'Мамадыш','',1,0,'N'),(897,'Булгар','',1,0,'N'),(898,'Базарные Матаки, с.','',1,0,'N'),(899,'Алексеевское, р.п.','',1,0,'N'),(900,'Аксубаево, р.п.','',1,0,'N'),(901,'Черемшан, с.','',1,0,'N'),(902,'Новошешминск, с.','',1,0,'N'),(903,'Менделеевск','',1,0,'N'),(904,'Агрыз','',1,0,'N'),(905,'Мензелинск','',1,0,'N'),(906,'Муслюмово, с.','',1,0,'N'),(907,'Сармановоул, с.','',1,0,'N'),(908,'Уруссу, р.п.','',1,0,'N'),(909,'Воткинск','',1,0,'N'),(910,'Глазов','',1,0,'N'),(911,'Можга','',1,0,'N'),(912,'Сарапул','',1,0,'N'),(913,'Алнаши, с.','',1,0,'N'),(914,'Балезино, п.','',1,0,'N'),(915,'Вавож, с.','',1,0,'N'),(916,'Грахово, с.','',1,0,'N'),(917,'Дебесы, с.','',1,0,'N'),(918,'Игра, п.','',1,0,'N'),(919,'Камбарка','',1,0,'N'),(920,'Каракулино, с.','',1,0,'N'),(921,'Кез, пгт','',1,0,'N'),(922,'Кизнер, п.','',1,0,'N'),(923,'Киясово, с.','',1,0,'N'),(924,'Малая Пурга, с.','',1,0,'N'),(925,'Сигаево, с.','',1,0,'N'),(926,'Селты, с.','',1,0,'N'),(927,'Сюмси, с.','',1,0,'N'),(928,'Ува, п.','',1,0,'N'),(929,'Шаркан, с.','',1,0,'N'),(930,'Юкаменское, с.','',1,0,'N'),(931,'Якшур-Бодья, с.','',1,0,'N'),(932,'Яр, п.','',1,0,'N'),(933,'Завьялово, с. (Удмуртия)','',1,0,'N'),(934,'Красногорское, с. (Удмуртия)','',1,0,'N'),(935,'Актобе','Aktobe',4,0,'N'),(936,'Белореченск','',1,0,'N'),(937,'Геленджик','',1,0,'N'),(938,'Кропоткин','',1,0,'N'),(939,'Крымск','',1,0,'N'),(940,'Славянск-на-Кубани','',1,0,'N'),(941,'Тихорецк','',1,0,'N'),(942,'Белая Глина, с.','',1,0,'N'),(943,'Выселки, с.','',1,0,'N'),(944,'Динская, ст.','',1,0,'N'),(945,'Кореновск','',1,0,'N'),(946,'Полтавская, ст.','',1,0,'N'),(947,'Крыловская, ст.','',1,0,'N'),(948,'Курганинск','',1,0,'N'),(949,'Кущевская, ст.','',1,0,'N'),(950,'Ленинградская, ст.','',1,0,'N'),(951,'Мостовская, ст.','',1,0,'N'),(952,'Новокубанск','',1,0,'N'),(953,'Новопокровская, ст.','',1,0,'N'),(954,'Отрадная, ст.','',1,0,'N'),(955,'Северская, ст.','',1,0,'N'),(956,'Староминская, ст.','',1,0,'N'),(957,'Тбилисская, ст.','',1,0,'N'),(958,'Темрюк','',1,0,'N'),(959,'Тимашевск','',1,0,'N'),(960,'Кроянское, с.','',1,0,'N'),(961,'Успенская, ст.','',1,0,'N'),(962,'Усть-Лабинск','',1,0,'N'),(963,'Щербиновская, ст.','',1,0,'N'),(964,'Инта','',1,0,'N'),(965,'Актау','Aktau',4,0,'N'),(966,'Атырау','Atyrau',4,0,'N'),(967,'Костанай','Kostanay',4,0,'N'),(968,'Павлодар','Pavlodar',4,0,'N'),(969,'Бородино','',1,0,'N'),(970,'Дивногорск','',1,0,'N'),(971,'Заозерный','',1,0,'N'),(972,'Сосновоборск','',1,0,'N'),(973,'Тбилиси','Tbilissi',138,0,'N'),(974,'Советский','',1,0,'N'),(975,'Югорск','',1,0,'N'),(976,'Пыть-Ях','',1,0,'N'),(977,'Радужный','',1,0,'N'),(978,'Белоярский','',1,0,'N'),(979,'Черновцы','Chernovtsy',72,0,'N'),(980,'Андорра','Andorra',63,0,'N'),(981,'Конаково','',1,0,'N'),(982,'Женева','Geneva',122,0,'N'),(983,'Валетта','Valetta',103,0,'N'),(984,'Губкин','',1,0,'N'),(985,'Ровно','Rivne',72,0,'Y'),(986,'Эдинбург','Edinburgh',71,0,'N'),(987,'Сиэтл','Seattle',128,0,'N'),(988,'Хьюстон','Houston',128,0,'N'),(989,'Мюнхен','Monheim',87,0,'N'),(990,'Амстердам','Amsterdam',108,0,'N'),(991,'Горки-2, пос.','',1,0,'N'),(992,'Уват, с.','',1,0,'N'),(993,'Дублин','Dublin',93,0,'N'),(994,'Вятские поляны','',1,0,'N'),(995,'Невинномысск','',1,0,'N'),(996,'Любляна','Ljubljana',27,0,'N'),(997,'Бухарест','Bucharest',25,0,'N'),(999,'Базель','Basel',122,0,'N'),(1000,'Кузнецк','',1,0,'N'),(1001,'Ковров','',1,0,'N'),(1002,'Сокол','',1,0,'N'),(1003,'Свирск','',1,0,'N'),(1004,'Парфино (Новгородская обл.)','',1,0,'N'),(1005,'Соль-Илецк','',1,0,'N'),(1006,'Самарское','',1,0,'N'),(1007,'Касимов','',1,0,'N'),(1008,'Краснотурьинск','',1,0,'N'),(1009,'Верхняя Пышма','',1,0,'N'),(1010,'Качканар','',1,0,'N'),(1011,'Сухой Лог','',1,0,'N'),(1012,'Ревда','',1,0,'N'),(1013,'Новоуральск','',1,0,'N'),(1014,'Лесной (Свердловская обл)','',1,0,'N'),(1015,'Серов','',1,0,'N'),(1016,'Ивдель','',1,0,'N'),(1017,'Ессентуки','',1,0,'N'),(1018,'Железноводск','',1,0,'N'),(1019,'Билибино','',1,0,'N'),(1022,'Отрадный','',1,0,'N'),(1021,'Кинель-Черкассы, с.','',1,0,'N'),(1023,'Суходол, п.','',1,0,'N'),(1024,'Сергиевск, с.','',1,0,'N'),(1025,'Безенчук, пгт.','',1,0,'N'),(1026,'Хворостянка, с.','',1,0,'N'),(1027,'Кинель','',1,0,'N'),(1028,'Заречный','',1,0,'N'),(1029,'Жирновск','',1,0,'N'),(1030,'Котово','',1,0,'N'),(1031,'Фролово','',1,0,'N'),(1032,'Полазна, п.г.т.','',1,0,'N'),(1033,'Хельсинки','Helsinki',129,0,'N'),(1034,'Осло','Oslo',111,0,'N'),(1035,'Ереван','Erevan',134,0,'N'),(1036,'Малаховка, пос.','',1,0,'N'),(1037,'Снежинск','',1,0,'N'),(1038,'Краснообск','',1,0,'N'),(1039,'Варшава','Warsaw',24,0,'N'),(1040,'Джерси','Jersey City',128,0,'N'),(1041,'Осташков','',1,0,'N'),(1042,'Щадринск','',1,0,'N'),(1043,'Цуг','Zug',122,0,'N'),(1045,'Рабат','Rabat',53,0,'N'),(1047,'Модена','Modena',95,0,'N'),(1048,'Wichita','Wichita',128,0,'N'),(1049,'Ниш','Nis',137,0,'N'),(1050,'Филадельфия','Philadelphia',128,0,'N'),(1051,'Иерусалим','Ierusalim',94,0,'N'),(1052,'Сеул','Seoul',52,0,'N'),(1053,'Abuja','Abuja',54,0,'N'),(1054,'Балтийск','',1,0,'N'),(1055,'Светлогорск','',1,0,'N'),(1056,'Снежногорск-2','',1,0,'N'),(1057,'Полярные Зори','',1,0,'N'),(1058,'Струги Красные, п.г.т.','',1,0,'N'),(1059,'Остров','',1,0,'N'),(1060,'Буэнос-Айрес','Buenos Aires',5,0,'N'),(1061,'Флоренция','Florence ',95,0,'N'),(1062,'Ramat Gan','Ramat Gan',94,0,'N'),(1063,'Тегеран','Tehran',92,0,'N'),(1064,'Конотоп','',72,0,'N'),(1065,'Чернигов','',72,0,'Y'),(1066,'Южно-Казахстанская область','',4,0,'N'),(1067,'Афины','Athens',47,0,'N'),(1069,'Луцк','',72,0,'N'),(1070,'Токио','Tokio',96,0,'N'),(1071,'Панама','Panama',23,0,'N'),(1073,'Strawberry Hill','Strawberry Hill',71,0,'N'),(1074,'Дюссельдорф','Dusseldorf',87,0,'N'),(1075,'Шымкент','Shymkent',4,0,'N'),(1076,'Кольчугино','',1,0,'N'),(1077,'Цюрих','Zurich',122,0,'N'),(1078,'Мариинск','',1,0,'N'),(1079,'Новочебоксарск','',1,0,'N'),(1080,'Черкассы','',72,0,'N'),(1081,'Луганск','',72,0,'Y'),(1083,'Bethesda','Bethesda',128,0,'N'),(1085,'Копенгаген','Kopenhagen',82,0,'N'),(1086,'Кливленд','Klivlend',128,0,'N'),(1087,'Брюссель','Brussels',74,0,'N'),(1088,'Карачаевск','',1,0,'N'),(1089,'Краков','Krakov',24,0,'N'),(1090,'Лейпциг','Leipzig',87,0,'N'),(1091,'Будапешт','Budapesht',8,0,'N'),(1092,'Бангалор','Bangalore',41,0,'N'),(1093,'Павлоград','',72,0,'N'),(1094,'IJAIYE','',54,0,'N'),(1095,'Дубай','',126,0,'N'),(1096,'Беверли Хилз','Beverly Hills',2,0,'N'),(1097,'Kiez','Kiez',3,0,'N'),(1098,'Гуанджоу','Guangzhou',4,0,'N'),(1099,'Богот','Bogot',5,0,'N'),(1100,'Charlotte','Charlotte',2,0,'N'),(1101,'Kansas City','Kansas City',2,0,'N'),(1102,'Kisel','Kisel',6,0,'N'),(1103,'Reading','Reading',7,0,'N'),(1104,'Kingisepp','Kingisepp',1,0,'N'),(1105,'K','K',3,0,'N'),(1106,'Alliance','Alliance',2,0,'N'),(1107,'Simi Valley','Simi Valley',2,0,'N'),(1108,'Hudson','Hudson',2,0,'N'),(1109,'Caerphilly','Caerphilly',7,0,'N'),(1110,'Whippany','Whippany',2,0,'N'),(1111,'Nijmegen','Nijmegen',8,0,'N'),(1112,'Roubaix','Roubaix',9,0,'N'),(1113,'Tampa','Tampa',2,0,'N'),(1114,'Velikiy Novgorod','Velikiy Novgorod',1,0,'N'),(1115,'Bogot','Bogot',5,0,'N'),(1116,'Nizhnevartovsk','Nizhnevartovsk',1,0,'N'),(1117,'Korolev','Korolev',1,0,'N'),(1118,'Ottawa','Ottawa',10,0,'N'),(1119,'Kaliningrad','Kaliningrad',1,0,'N'),(1120,'Gatchina','Gatchina',1,0,'N'),(1121,'Saint Louis','Saint Louis',2,0,'N'),(1122,'Hermsdorf','Hermsdorf',3,0,'N'),(1123,'Milan','Milan',11,0,'N'),(1124,'Halethorpe','Halethorpe',2,0,'N'),(1125,'Kolomna','Kolomna',1,0,'N'),(1126,'Langen','Langen',3,0,'N'),(1127,'Odintsovo','Odintsovo',1,0,'N'),(1128,'Ludwigsfelde','Ludwigsfelde',3,0,'N'),(1129,'Leeuwarden','Leeuwarden',8,0,'N'),(1130,'Strunino','Strunino',1,0,'N'),(1131,'Sioux Falls','Sioux Falls',2,0,'N'),(1132,'Xian','Xian',4,0,'N'),(1133,'Burnaby','Burnaby',10,0,'N'),(1134,'Shanghai','Shanghai',4,0,'N'),(1135,'Nanjing','Nanjing',4,0,'N'),(1136,'Cary','Cary',2,0,'N'),(1137,'Beijing','Beijing',4,0,'N'),(1138,'Slavyansk','Slavyansk',6,0,'N'),(1139,'Voronezh','Voronezh',1,0,'N'),(1140,'New Middletown','New Middletown',2,0,'N'),(1141,'Guizhou','Guizhou',4,0,'N'),(1142,'Petrozavodsk','Petrozavodsk',1,0,'N'),(1143,'Sudbury','Sudbury',2,0,'N'),(1144,'Atlanta','Atlanta',2,0,'N'),(1145,'Krasnogorsk','Krasnogorsk',1,0,'N'),(1146,'North Hollywood','North Hollywood',2,0,'N'),(1147,'Silvolde','Silvolde',8,0,'N'),(1148,'Kristiansand','Kristiansand',12,0,'N'),(1149,'San Jose','San Jose',2,0,'N'),(1150,'Milpitas','Milpitas',2,0,'N'),(1151,'Mountain View','Mountain View',2,0,'N'),(1152,'Bergen','Bergen',12,0,'N'),(1153,'Chengdu','Chengdu',4,0,'N'),(1154,'Shenyang','Shenyang',4,0,'N'),(1155,'Simferopol','Simferopol',6,0,'N'),(1156,'Taipei','Taipei',13,0,'N'),(1157,'Wuhan','Wuhan',4,0,'N'),(1158,'Liverpool','Liverpool',2,0,'N'),(1159,'Zaraysk','Zaraysk',1,0,'N'),(1160,'Yantai','Yantai',4,0,'N'),(1161,'Eau Claire','Eau Claire',2,0,'N'),(1162,'Bellevue','Bellevue',2,0,'N'),(1163,'Springfield','Springfield',2,0,'N'),(1164,'Brunswick','Brunswick',3,0,'N'),(1165,'Voskresensk','Voskresensk',1,0,'N'),(1166,'Yoshkar-ola','Yoshkar-ola',1,0,'N'),(1167,'Rostov-na-donu','Rostov-na-donu',1,0,'N'),(1168,'Dalfsen','Dalfsen',8,0,'N'),(1169,'Amur','Amur',1,0,'N'),(1170,'Malaya','Malaya',6,0,'N'),(1171,'Bedford','Bedford',7,0,'N'),(1172,'Tel Aviv','Tel Aviv',14,0,'N'),(1173,'Schweiz','Schweiz',3,0,'N'),(1174,'Leeds','Leeds',7,0,'N'),(1175,'Seremban','Seremban',15,0,'N'),(1176,'Donetsk','Donetsk',6,0,'N'),(1177,'Bel','Bel',16,0,'N'),(1178,'San Diego','San Diego',2,0,'N'),(1179,'Columbus','Columbus',2,0,'N'),(1180,'Obukhovo','Obukhovo',1,0,'N'),(1181,'','',4,0,'N'),(1182,'Changsha','Changsha',4,0,'N'),(1183,'Tagil','Tagil',1,0,'N'),(1184,'Kstovo','Kstovo',1,0,'N'),(1185,'Novoye Devyatkino','Novoye Devyatkino',1,0,'N'),(1186,'Galati','Galati',17,0,'N'),(1187,'Tongling','Tongling',4,0,'N'),(1188,'Novorossiysk','Novorossiysk',1,0,'N'),(1189,'Losino-petrovskiy','Losino-petrovskiy',1,0,'N'),(1190,'Gunzenhausen','Gunzenhausen',3,0,'N'),(1191,'Baoji','Baoji',4,0,'N'),(1192,'Wemmel','Wemmel',18,0,'N'),(1193,'Kola','Kola',1,0,'N'),(1194,'Sosnova','Sosnova',6,0,'N'),(1195,'Decatur','Decatur',2,0,'N'),(1196,'Gagarina','Gagarina',1,0,'N'),(1197,'Jakarta','Jakarta',19,0,'N'),(1198,'Yekaterinburg','Yekaterinburg',1,0,'N'),(1199,'Apo','Apo',2,0,'N'),(1200,'Shenzhen','Shenzhen',4,0,'N'),(1201,'Noyabrsk','Noyabrsk',1,0,'N'),(1202,'Kirishi','Kirishi',1,0,'N'),(1203,'Stavropolskiy','Stavropolskiy',1,0,'N'),(1204,'Twin Falls','Twin Falls',2,0,'N'),(1205,'Alchevsk','Alchevsk',6,0,'N'),(1206,'Rocklin','Rocklin',2,0,'N'),(1207,'Vitebsk','Vitebsk',20,0,'N'),(1208,'Lipetsk','Lipetsk',1,0,'N'),(1209,'Ural','Ural',1,0,'N'),(1210,'Lobnya','Lobnya',1,0,'N'),(1211,'Cagliari','Cagliari',11,0,'N'),(1212,'Den Haag','Den Haag',8,0,'N'),(1213,'Birmingham','Birmingham',7,0,'N'),(1214,'Copenhagen','Copenhagen',21,0,'N'),(1215,'Kiselevsk','Kiselevsk',1,0,'N'),(1216,'Montreal','Montreal',10,0,'N'),(1217,'Ryazan','Ryazan',1,0,'N'),(1218,'Sertolovo','Sertolovo',1,0,'N'),(1219,'Richardson','Richardson',2,0,'N'),(1220,'Progress','Progress',1,0,'N'),(1221,'Modugno','Modugno',11,0,'N'),(1222,'Dallas','Dallas',2,0,'N'),(1223,'Chisinau','Chisinau',22,0,'N'),(1224,'Balgarovo','Balgarovo',23,0,'N'),(1225,'Kotelniki','Kotelniki',1,0,'N'),(1226,'Corona','Corona',2,0,'N'),(1227,'Haifa','Haifa',14,0,'N'),(1228,'Putian','Putian',4,0,'N'),(1229,'Suceava','Suceava',17,0,'N'),(1230,'Astrakhan','Astrakhan',1,0,'N'),(1231,'Accra','Accra',24,0,'N'),(1232,'Apatity','Apatity',1,0,'N'),(1233,'Mayaguez','Mayaguez',25,0,'N'),(1234,'Obolon','Obolon',6,0,'N'),(1235,'Lomonosova','Lomonosova',1,0,'N'),(1236,'Los Altos','Los Altos',2,0,'N'),(1237,'Laval','Laval',10,0,'N'),(1238,'Schlusselburg','Schlusselburg',1,0,'N'),(1239,'Crawley','Crawley',7,0,'N'),(1240,'Villavicencio','Villavicencio',5,0,'N'),(1241,'Lakeville','Lakeville',2,0,'N'),(1242,'Heston','Heston',7,0,'N'),(1243,'Adrian','Adrian',2,0,'N'),(1244,'Pervouralsk','Pervouralsk',1,0,'N'),(1245,'San Bruno','San Bruno',2,0,'N'),(1246,'Louisville','Louisville',2,0,'N'),(1247,'Poltava','Poltava',6,0,'N'),(1248,'Las Pi','Las Pi',26,0,'N'),(1249,'Glazov','Glazov',1,0,'N'),(1250,'Bryansk','Bryansk',1,0,'N'),(1251,'Canal Winchester','Canal Winchester',2,0,'N'),(1252,'Praha','Praha',27,0,'N'),(1253,'Lyse','Lyse',12,0,'N'),(1254,'Pskov','Pskov',1,0,'N'),(1255,'Neftekamsk','Neftekamsk',1,0,'N'),(1256,'Kessel','Kessel',18,0,'N'),(1257,'Irpen','Irpen',6,0,'N'),(1258,'Saransk','Saransk',1,0,'N'),(1259,'Hefei','Hefei',4,0,'N'),(1260,'Nauka','Nauka',1,0,'N'),(1261,'Czestochowa','Czestochowa',28,0,'N'),(1262,'Albany','Albany',2,0,'N'),(1263,'Plymouth','Plymouth',7,0,'N'),(1264,'Murmansk','Murmansk',1,0,'N'),(1265,'Nizhniy Novgorod','Nizhniy Novgorod',1,0,'N'),(1266,'Sayreville','Sayreville',2,0,'N'),(1267,'Krivoy Rog','Krivoy Rog',6,0,'N'),(1268,'Kovrov','Kovrov',1,0,'N'),(1269,'Vilnius','Vilnius',29,0,'N'),(1270,'Jakarta Pusat','Jakarta Pusat',19,0,'N'),(1271,'Bakersfield','Bakersfield',2,0,'N'),(1272,'Adkins','Adkins',2,0,'N'),(1273,'Steinsel','Steinsel',30,0,'N'),(1274,'San Antonio','San Antonio',2,0,'N'),(1275,'Dubai','Dubai',31,0,'N'),(1276,'Valera','Valera',32,0,'N'),(1277,'Rubio','Rubio',32,0,'N'),(1278,'Orlando','Orlando',2,0,'N'),(1279,'Lublin','Lublin',28,0,'N'),(1280,'Toronto','Toronto',10,0,'N'),(1281,'Scranton','Scranton',2,0,'N'),(1282,'Salisbury','Salisbury',7,0,'N'),(1283,'Smila','Smila',6,0,'N'),(1284,'Wettingen','Wettingen',33,0,'N'),(1285,'Tosno','Tosno',1,0,'N'),(1286,'Sevastopol','Sevastopol',6,0,'N'),(1287,'Yaroslavl','Yaroslavl',1,0,'N'),(1288,'Boulogne-billancourt','Boulogne-billancourt',9,0,'N'),(1289,'Ivanovo','Ivanovo',1,0,'N'),(1290,'Caracas','Caracas',32,0,'N'),(1291,'Marina Del Rey','Marina Del Rey',2,0,'N'),(1292,'Kuban','Kuban',1,0,'N'),(1293,'Dresden','Dresden',3,0,'N'),(1294,'Surgut','Surgut',1,0,'N'),(1295,'Wroclaw','Wroclaw',28,0,'N'),(1296,'Mariupol','Mariupol',6,0,'N'),(1297,'Coimbra','Coimbra',34,0,'N'),(1298,'Shakhty','Shakhty',1,0,'N'),(1299,'Angers','Angers',9,0,'N'),(1300,'Lutsk','Lutsk',6,0,'N'),(1301,'Mugla','Mugla',35,0,'N'),(1302,'Rome','Rome',11,0,'N'),(1303,'Novokuznetsk','Novokuznetsk',1,0,'N'),(1304,'Winter Park','Winter Park',2,0,'N'),(1305,'Nanchang','Nanchang',4,0,'N'),(1306,'Dolen','Dolen',23,0,'N'),(1307,'Iver','Iver',7,0,'N'),(1308,'Tbilisi','Tbilisi',36,0,'N'),(1309,'Paderborn','Paderborn',3,0,'N'),(1310,'Anchorage','Anchorage',2,0,'N'),(1311,'Burtonsville','Burtonsville',2,0,'N'),(1312,'Albuquerque','Albuquerque',2,0,'N'),(1313,'Scottsdale','Scottsdale',2,0,'N'),(1314,'Herndon','Herndon',2,0,'N'),(1315,'Arlington','Arlington',2,0,'N'),(1316,'Stavropol','Stavropol',1,0,'N'),(1317,'Florian','Florian',16,0,'N'),(1318,'Pushkin','Pushkin',1,0,'N'),(1319,'D','D',3,0,'N'),(1320,'Neptune','Neptune',2,0,'N'),(1321,'Naberezhnaya','Naberezhnaya',1,0,'N'),(1322,'Sterlitamak','Sterlitamak',1,0,'N'),(1323,'Madrid','Madrid',37,0,'N'),(1324,'Kaluga','Kaluga',1,0,'N'),(1325,'Kubinka','Kubinka',1,0,'N'),(1326,'Cupertino','Cupertino',2,0,'N'),(1327,'Zhuhai','Zhuhai',4,0,'N'),(1328,'Seversk','Seversk',1,0,'N'),(1329,'Cape Town','Cape Town',38,0,'N'),(1330,'Asheville','Asheville',2,0,'N'),(1331,'El Paso','El Paso',2,0,'N'),(1332,'Puerto Ordaz','Puerto Ordaz',32,0,'N'),(1333,'Maracaibo','Maracaibo',32,0,'N'),(1334,'Frankfurt Am Main','Frankfurt Am Main',3,0,'N'),(1335,'Woodland','Woodland',2,0,'N'),(1336,'Newberry','Newberry',2,0,'N'),(1337,'Dedovsk','Dedovsk',1,0,'N'),(1338,'Bilbao','Bilbao',37,0,'N'),(1339,'Ankara','Ankara',35,0,'N'),(1340,'York','York',7,0,'N'),(1341,'Qingdao','Qingdao',4,0,'N'),(1342,'Tianjin','Tianjin',4,0,'N'),(1343,'Tayga','Tayga',1,0,'N'),(1344,'Tyumen','Tyumen',1,0,'N'),(1345,'Shiraz','Shiraz',39,0,'N'),(1346,'Laguna Niguel','Laguna Niguel',2,0,'N'),(1347,'Angarsk','Angarsk',1,0,'N'),(1348,'Konakovo','Konakovo',1,0,'N'),(1349,'Fuzhou','Fuzhou',4,0,'N'),(1350,'Calgary','Calgary',10,0,'N'),(1351,'Skien','Skien',12,0,'N'),(1352,'Cherkassy','Cherkassy',6,0,'N'),(1353,'Komsomolsk-na-amure','Komsomolsk-na-amure',1,0,'N'),(1354,'Daegu','Daegu',40,0,'N'),(1355,'Algermissen','Algermissen',3,0,'N'),(1356,'Islamabad','Islamabad',41,0,'N'),(1357,'Zvenigorod','Zvenigorod',1,0,'N'),(1358,'Timisoara','Timisoara',17,0,'N'),(1359,'Chekhov','Chekhov',1,0,'N'),(1360,'Schaumburg','Schaumburg',2,0,'N'),(1361,'Yanji','Yanji',4,0,'N'),(1362,'Las Vegas','Las Vegas',2,0,'N'),(1363,'Chesterfield','Chesterfield',7,0,'N'),(1364,'Dostyk','Dostyk',42,0,'N'),(1365,'Orsk','Orsk',1,0,'N'),(1366,'Stepnogorsk','Stepnogorsk',42,0,'N'),(1367,'Hebei','Hebei',4,0,'N'),(1368,'Aachen','Aachen',3,0,'N'),(1369,'West Newbury','West Newbury',2,0,'N'),(1370,'Vyborg','Vyborg',1,0,'N'),(1371,'Stary Oskol','Stary Oskol',1,0,'N'),(1372,'Washington','Washington',2,0,'N'),(1373,'Izmir','Izmir',35,0,'N'),(1374,'San Francisco','San Francisco',2,0,'N'),(1375,'Dayton','Dayton',2,0,'N'),(1376,'Harbin','Harbin',4,0,'N'),(1377,'Munich','Munich',3,0,'N'),(1378,'Neuilly','Neuilly',9,0,'N'),(1379,'Kirovsk','Kirovsk',1,0,'N'),(1380,'Hopatcong','Hopatcong',2,0,'N'),(1381,'Peoria','Peoria',2,0,'N'),(1382,'Bangkok','Bangkok',43,0,'N'),(1383,'Posyet','Posyet',1,0,'N'),(1384,'Ternopol','Ternopol',6,0,'N'),(1385,'V','V',44,0,'N'),(1386,'Hamburg','Hamburg',3,0,'N'),(1387,'Bochum','Bochum',3,0,'N'),(1388,'Chelny','Chelny',1,0,'N'),(1389,'Gothenburg','Gothenburg',44,0,'N'),(1390,'Nikolaev','Nikolaev',6,0,'N'),(1391,'Shantou','Shantou',4,0,'N'),(1392,'Lenin','Lenin',1,0,'N'),(1393,'Kramatorsk','Kramatorsk',6,0,'N'),(1394,'Rimouski','Rimouski',10,0,'N'),(1395,'Marganets','Marganets',6,0,'N'),(1396,'Monchegorsk','Monchegorsk',1,0,'N'),(1397,'Budapest','Budapest',45,0,'N'),(1398,'Zaraisk','Zaraisk',1,0,'N'),(1399,'Rio De Janeiro','Rio De Janeiro',16,0,'N'),(1400,'Zilina','Zilina',46,0,'N'),(1401,'San Mateo','San Mateo',2,0,'N'),(1402,'Domodedovo','Domodedovo',1,0,'N'),(1403,'Chita','Chita',1,0,'N'),(1404,'Pasadena','Pasadena',2,0,'N'),(1405,'Podolsk','Podolsk',1,0,'N'),(1406,'Makarova','Makarova',1,0,'N'),(1407,'Port Isabel','Port Isabel',2,0,'N'),(1408,'Penza','Penza',1,0,'N'),(1409,'Huangdao','Huangdao',4,0,'N'),(1410,'La Crosse','La Crosse',2,0,'N'),(1411,'Kamensk-uralskiy','Kamensk-uralskiy',1,0,'N'),(1412,'Somerville','Somerville',2,0,'N'),(1413,'Shadrinsk','Shadrinsk',1,0,'N'),(1414,'Coburg','Coburg',47,0,'N'),(1415,'Cherkessk','Cherkessk',1,0,'N'),(1416,'Mamadysh','Mamadysh',1,0,'N'),(1417,'Henan','Henan',4,0,'N'),(1418,'Berezniki','Berezniki',1,0,'N'),(1419,'Kavkaz','Kavkaz',1,0,'N'),(1420,'Nefteyugansk','Nefteyugansk',1,0,'N'),(1421,'Ski','Ski',12,0,'N'),(1422,'Bregenz','Bregenz',48,0,'N'),(1423,'Verl','Verl',3,0,'N'),(1424,'Kherson','Kherson',6,0,'N'),(1425,'Oxford','Oxford',2,0,'N'),(1426,'Smolensk','Smolensk',1,0,'N'),(1427,'Indianapolis','Indianapolis',2,0,'N'),(1428,'Wayne','Wayne',2,0,'N'),(1429,'Ukraina','Ukraina',6,0,'N'),(1430,'Brooklyn','Brooklyn',2,0,'N'),(1431,'Namakkal','Namakkal',49,0,'N'),(1432,'Bagneux','Bagneux',9,0,'N'),(1433,'Togliatti','Togliatti',1,0,'N'),(1434,'Zhytomyr','Zhytomyr',6,0,'N'),(1435,'Stockbridge','Stockbridge',2,0,'N'),(1436,'Alheim','Alheim',3,0,'N'),(1437,'Curtea De Arges','Curtea De Arges',17,0,'N'),(1438,'Novosokolniki','Novosokolniki',1,0,'N'),(1439,'Vladikavkaz','Vladikavkaz',1,0,'N'),(1440,'Arzamas','Arzamas',1,0,'N'),(1441,'Kemerovo','Kemerovo',1,0,'N'),(1442,'Cherkasy','Cherkasy',6,0,'N'),(1443,'Guymon','Guymon',2,0,'N'),(1444,'Honolulu','Honolulu',2,0,'N'),(1445,'Simbirsk','Simbirsk',1,0,'N'),(1446,'Changchun','Changchun',4,0,'N'),(1447,'Pyatigorsk','Pyatigorsk',1,0,'N'),(1448,'Jinhua','Jinhua',4,0,'N'),(1449,'Ust-katav','Ust-katav',1,0,'N'),(1450,'Gorodets','Gorodets',1,0,'N'),(1451,'Staryy Oskol','Staryy Oskol',1,0,'N'),(1452,'Nagar','Nagar',49,0,'N'),(1453,'Cheboksary','Cheboksary',1,0,'N'),(1454,'Kashira','Kashira',1,0,'N'),(1455,'Slidell','Slidell',2,0,'N'),(1456,'Latina','Latina',11,0,'N'),(1457,'Campo Grande','Campo Grande',16,0,'N'),(1458,'Heyuan','Heyuan',4,0,'N'),(1459,'Chongqing','Chongqing',4,0,'N'),(1460,'Sydney','Sydney',47,0,'N'),(1461,'Okha','Okha',1,0,'N'),(1462,'Monterrey','Monterrey',50,0,'N'),(1463,'Braga','Braga',34,0,'N'),(1464,'Sabanilla','Sabanilla',51,0,'N'),(1465,'Selb','Selb',3,0,'N'),(1466,'Xiamen','Xiamen',4,0,'N'),(1467,'Artem','Artem',1,0,'N'),(1468,'Woodbridge','Woodbridge',2,0,'N'),(1469,'Mar','Mar',16,0,'N'),(1470,'Oldenburg','Oldenburg',3,0,'N'),(1471,'Dronten','Dronten',8,0,'N'),(1472,'Koryazhma','Koryazhma',1,0,'N'),(1473,'Courbevoie','Courbevoie',9,0,'N'),(1474,'Sherbrooke','Sherbrooke',10,0,'N'),(1475,'Tiraspol','Tiraspol',22,0,'N'),(1476,'Maring','Maring',16,0,'N'),(1477,'S','S',16,0,'N'),(1478,'Victoria','Victoria',10,0,'N'),(1479,'Mechelen','Mechelen',18,0,'N'),(1480,'Richmond Hill','Richmond Hill',10,0,'N'),(1481,'Bytom','Bytom',28,0,'N'),(1482,'Taichung','Taichung',13,0,'N'),(1483,'T','T',27,0,'N'),(1484,'Chernivtsi','Chernivtsi',6,0,'N'),(1485,'Lima','Lima',2,0,'N'),(1486,'Winterthur','Winterthur',33,0,'N'),(1487,'Bear','Bear',2,0,'N'),(1488,'Worthing','Worthing',7,0,'N'),(1489,'Johannesburg','Johannesburg',38,0,'N'),(1490,'Miami','Miami',2,0,'N'),(1491,'Ashkhabad','Ashkhabad',52,0,'N'),(1492,'Rostov','Rostov',1,0,'N'),(1493,'Baranovichi','Baranovichi',20,0,'N'),(1494,'Carlisle','Carlisle',2,0,'N'),(1495,'Irvine','Irvine',2,0,'N'),(1496,'Kokkedal','Kokkedal',21,0,'N'),(1497,'Cagua','Cagua',32,0,'N'),(1498,'Bombay','Bombay',49,0,'N'),(1499,'West Sacramento','West Sacramento',2,0,'N'),(1500,'Bethel','Bethel',2,0,'N'),(1501,'Ivanov','Ivanov',6,0,'N'),(1502,'Kuala Lumpur','Kuala Lumpur',15,0,'N'),(1503,'Bras','Bras',16,0,'N'),(1504,'Kuala Selangor','Kuala Selangor',15,0,'N'),(1505,'Bonsall','Bonsall',2,0,'N'),(1506,'Ikeja','Ikeja',53,0,'N'),(1507,'Gambrills','Gambrills',2,0,'N'),(1508,'Baton Rouge','Baton Rouge',2,0,'N'),(1509,'Beograd','Beograd',54,0,'N'),(1510,'Ortisei','Ortisei',11,0,'N'),(1511,'Fremont','Fremont',2,0,'N'),(1512,'Kalush','Kalush',6,0,'N'),(1513,'Edmond','Edmond',2,0,'N'),(1514,'Jyv','Jyv',55,0,'N'),(1515,'Lisboa','Lisboa',34,0,'N'),(1516,'Brea','Brea',2,0,'N'),(1517,'Burlington','Burlington',2,0,'N'),(1518,'Lakewood','Lakewood',2,0,'N'),(1519,'Ussuriysk','Ussuriysk',1,0,'N'),(1520,'Court','Court',33,0,'N'),(1521,'Morristown','Morristown',2,0,'N'),(1522,'Borovaya','Borovaya',1,0,'N'),(1523,'Freiburg','Freiburg',3,0,'N'),(1524,'Central District','Central District',56,0,'N'),(1525,'Knightdale','Knightdale',2,0,'N'),(1526,'Tartu','Tartu',57,0,'N'),(1527,'Jinan','Jinan',4,0,'N'),(1528,'Pompano Beach','Pompano Beach',2,0,'N'),(1529,'Kirkland','Kirkland',2,0,'N'),(1530,'Minusinsk','Minusinsk',1,0,'N'),(1531,'Sunnyvale','Sunnyvale',2,0,'N'),(1532,'Silkeborg','Silkeborg',21,0,'N'),(1533,'Medell','Medell',5,0,'N'),(1534,'Tilburg','Tilburg',8,0,'N'),(1535,'Nelson','Nelson',7,0,'N'),(1536,'Toyohara','Toyohara',1,0,'N'),(1537,'Herisau','Herisau',33,0,'N'),(1538,'Oakland','Oakland',2,0,'N'),(1539,'Fowler','Fowler',2,0,'N'),(1540,'Belgrade','Belgrade',54,0,'N'),(1541,'Volosovo','Volosovo',1,0,'N'),(1542,'Oroville','Oroville',2,0,'N'),(1543,'Davis','Davis',2,0,'N'),(1544,'Fort Lauderdale','Fort Lauderdale',2,0,'N'),(1545,'Datong','Datong',4,0,'N'),(1546,'Aberdeen','Aberdeen',2,0,'N'),(1547,'Brandon','Brandon',2,0,'N'),(1548,'Ningbo','Ningbo',4,0,'N'),(1549,'Auburn','Auburn',2,0,'N'),(1550,'Midland','Midland',2,0,'N'),(1551,'Sazan','Sazan',39,0,'N'),(1552,'Ransom Canyon','Ransom Canyon',2,0,'N'),(1553,'Johnson City','Johnson City',2,0,'N'),(1554,'Portland','Portland',2,0,'N'),(1555,'Mars','Mars',35,0,'N'),(1556,'Lucca','Lucca',11,0,'N'),(1557,'Laverne','Laverne',2,0,'N'),(1558,'Los Angeles','Los Angeles',2,0,'N'),(1559,'Zonhoven','Zonhoven',18,0,'N'),(1560,'Ishimbay','Ishimbay',1,0,'N'),(1561,'C','C',17,0,'N'),(1562,'Stanton','Stanton',2,0,'N'),(1563,'Riyadh','Riyadh',58,0,'N'),(1564,'Varna','Varna',23,0,'N'),(1565,'New Delhi','New Delhi',49,0,'N'),(1566,'Santa Monica','Santa Monica',2,0,'N'),(1567,'Southeastern','Southeastern',2,0,'N'),(1568,'Eindhoven','Eindhoven',8,0,'N'),(1569,'Tequisquiapan','Tequisquiapan',50,0,'N'),(1570,'Lugansk','Lugansk',6,0,'N'),(1571,'Radio','Radio',6,0,'N'),(1572,'Szczecin','Szczecin',28,0,'N'),(1573,'Rotterdam','Rotterdam',8,0,'N'),(1574,'Dmitrov','Dmitrov',1,0,'N'),(1575,'Hangzhou','Hangzhou',4,0,'N'),(1576,'Bad Homburg','Bad Homburg',3,0,'N'),(1577,'Ellenville','Ellenville',2,0,'N'),(1578,'Foshan','Foshan',4,0,'N'),(1579,'Ambato','Ambato',59,0,'N'),(1580,'Hanoi','Hanoi',60,0,'N'),(1581,'Kingsport','Kingsport',2,0,'N'),(1582,'Achinsk','Achinsk',1,0,'N'),(1583,'New Canton','New Canton',2,0,'N'),(1584,'Springville','Springville',2,0,'N'),(1585,'Conyers','Conyers',2,0,'N'),(1586,'Mount Sterling','Mount Sterling',2,0,'N'),(1587,'Kleinsaubernitz','Kleinsaubernitz',3,0,'N'),(1588,'Norilsk','Norilsk',1,0,'N'),(1589,'Nanuet','Nanuet',2,0,'N'),(1590,'Sever','Sever',1,0,'N'),(1591,'Ann Arbor','Ann Arbor',2,0,'N'),(1592,'Druzhkovka','Druzhkovka',6,0,'N'),(1593,'North Royalton','North Royalton',2,0,'N'),(1594,'Muenster','Muenster',3,0,'N'),(1595,'Rossosh','Rossosh',1,0,'N'),(1596,'Centerville','Centerville',2,0,'N'),(1597,'Camano Island','Camano Island',2,0,'N'),(1598,'Pomp','Pomp',16,0,'N'),(1599,'Asunci','Asunci',61,0,'N'),(1600,'Cadiz','Cadiz',37,0,'N'),(1601,'La Laguna','La Laguna',37,0,'N'),(1602,'Pervomayskiy','Pervomayskiy',1,0,'N'),(1603,'Khimki','Khimki',1,0,'N'),(1604,'Bloomfield Hills','Bloomfield Hills',2,0,'N'),(1605,'Cambridge','Cambridge',2,0,'N'),(1606,'Kolpino','Kolpino',1,0,'N'),(1607,'Semey','Semey',42,0,'N'),(1608,'Mesa','Mesa',2,0,'N'),(1609,'Germantown','Germantown',2,0,'N'),(1610,'Bekasi','Bekasi',19,0,'N'),(1611,'Miass','Miass',1,0,'N'),(1612,'Euless','Euless',2,0,'N'),(1613,'Hemel Hempstead','Hemel Hempstead',7,0,'N'),(1614,'Goodwell','Goodwell',2,0,'N'),(1615,'Fort Erie','Fort Erie',10,0,'N'),(1616,'Trondheim','Trondheim',12,0,'N'),(1617,'Chernyavskiy','Chernyavskiy',6,0,'N'),(1618,'Dnipropetrovsk','Dnipropetrovsk',6,0,'N'),(1619,'Dzerzhinsk','Dzerzhinsk',1,0,'N'),(1620,'Tomilino','Tomilino',1,0,'N'),(1621,'Leatherhead','Leatherhead',7,0,'N'),(1622,'Sevilla','Sevilla',37,0,'N'),(1623,'Barabinsk','Barabinsk',1,0,'N'),(1624,'Oberursel','Oberursel',3,0,'N'),(1625,'Chanh Hiep','Chanh Hiep',60,0,'N'),(1626,'Dolores Hidalgo','Dolores Hidalgo',50,0,'N'),(1627,'Salt Lake City','Salt Lake City',2,0,'N'),(1628,'Manaus','Manaus',16,0,'N'),(1629,'Troisdorf','Troisdorf',3,0,'N'),(1630,'Vanino','Vanino',1,0,'N'),(1631,'Penghu','Penghu',13,0,'N'),(1632,'Landskrona','Landskrona',44,0,'N'),(1633,'Pathum Thani','Pathum Thani',43,0,'N'),(1634,'Ilya','Ilya',35,0,'N'),(1635,'Orange','Orange',2,0,'N'),(1636,'Lincoln','Lincoln',2,0,'N'),(1637,'Sheridan','Sheridan',2,0,'N'),(1638,'Wolverhampton','Wolverhampton',7,0,'N'),(1639,'Uppsala','Uppsala',44,0,'N'),(1640,'H','H',35,0,'N'),(1641,'Quebec','Quebec',10,0,'N'),(1642,'Dnepropetrovsk','Dnepropetrovsk',6,0,'N'),(1643,'Great Kimble','Great Kimble',7,0,'N'),(1644,'Cubat','Cubat',16,0,'N'),(1645,'Ciudad Del Este','Ciudad Del Este',61,0,'N'),(1646,'Kaohsiung','Kaohsiung',13,0,'N'),(1647,'Kirovohrad','Kirovohrad',6,0,'N'),(1648,'Menlo Park','Menlo Park',2,0,'N'),(1649,'Natal','Natal',16,0,'N'),(1650,'Medicine Hat','Medicine Hat',10,0,'N'),(1651,'Bendery','Bendery',22,0,'N'),(1652,'Kowloon','Kowloon',56,0,'N'),(1653,'Reutlingen','Reutlingen',3,0,'N'),(1654,'Kurgan','Kurgan',1,0,'N'),(1655,'San Salvador','San Salvador',62,0,'N'),(1656,'Santa Teresa','Santa Teresa',32,0,'N'),(1657,'Yongzhou','Yongzhou',4,0,'N'),(1658,'Cahul','Cahul',22,0,'N'),(1659,'Dongguan','Dongguan',4,0,'N'),(1660,'Solnechnogorsk','Solnechnogorsk',1,0,'N'),(1661,'Pushkino','Pushkino',1,0,'N'),(1662,'Sault Sainte Marie','Sault Sainte Marie',10,0,'N'),(1663,'Saturn','Saturn',1,0,'N'),(1664,'Chaoyang','Chaoyang',4,0,'N'),(1665,'Nanchong','Nanchong',4,0,'N'),(1666,'Bocholt','Bocholt',3,0,'N'),(1667,'Zlatoust','Zlatoust',1,0,'N'),(1668,'Greven','Greven',3,0,'N'),(1669,'Glastonbury','Glastonbury',2,0,'N'),(1670,'Nashville','Nashville',2,0,'N'),(1671,'La Grange','La Grange',2,0,'N'),(1672,'Fairchance','Fairchance',2,0,'N'),(1673,'Kassel','Kassel',3,0,'N'),(1674,'Sochi','Sochi',1,0,'N'),(1675,'Lule','Lule',44,0,'N'),(1676,'Kwai Chung','Kwai Chung',56,0,'N'),(1677,'Lyon','Lyon',9,0,'N'),(1678,'Terney','Terney',1,0,'N'),(1679,'Qostanay','Qostanay',42,0,'N'),(1680,'North Bend','North Bend',2,0,'N'),(1681,'Sheffield','Sheffield',7,0,'N'),(1682,'Belo Horizonte','Belo Horizonte',16,0,'N'),(1683,'Windsor','Windsor',10,0,'N'),(1684,'Chattanooga','Chattanooga',2,0,'N'),(1685,'Romanov','Romanov',1,0,'N'),(1686,'Komsomolskaya','Komsomolskaya',1,0,'N'),(1687,'Scarborough','Scarborough',10,0,'N'),(1688,'Abdi','Abdi',1,0,'N'),(1689,'Santa Barbara','Santa Barbara',2,0,'N'),(1690,'Swierklaniec','Swierklaniec',28,0,'N'),(1691,'Nalchik','Nalchik',1,0,'N'),(1692,'Neuss','Neuss',3,0,'N'),(1693,'Izola','Izola',63,0,'N'),(1694,'Aalsmeer','Aalsmeer',8,0,'N'),(1695,'Makhachkala','Makhachkala',1,0,'N'),(1696,'Puebla','Puebla',50,0,'N'),(1697,'Bursa','Bursa',35,0,'N'),(1698,'Yuncheng','Yuncheng',4,0,'N'),(1699,'Montevideo','Montevideo',64,0,'N'),(1700,'Chon Buri','Chon Buri',43,0,'N'),(1701,'Chennai','Chennai',49,0,'N'),(1702,'Lockbourne','Lockbourne',2,0,'N'),(1703,'West Richland','West Richland',2,0,'N'),(1704,'Gdansk','Gdansk',28,0,'N'),(1705,'Forserum','Forserum',44,0,'N'),(1706,'Yuan','Yuan',4,0,'N'),(1707,'Almere','Almere',8,0,'N'),(1708,'Yogyakarta','Yogyakarta',19,0,'N'),(1709,'Barcelona','Barcelona',32,0,'N'),(1710,'Volgodonsk','Volgodonsk',1,0,'N'),(1711,'Nysa','Nysa',28,0,'N'),(1712,'Arezzo','Arezzo',11,0,'N'),(1713,'Kanash','Kanash',1,0,'N'),(1714,'Engels','Engels',1,0,'N'),(1715,'Petersburg','Petersburg',3,0,'N'),(1716,'Satka','Satka',1,0,'N'),(1717,'Daugavpils','Daugavpils',65,0,'N'),(1718,'Meckesheim','Meckesheim',3,0,'N'),(1719,'Quilpu','Quilpu',66,0,'N'),(1720,'Santa Clara','Santa Clara',2,0,'N'),(1721,'Bonn','Bonn',3,0,'N'),(1722,'Dexter','Dexter',2,0,'N'),(1723,'Mexico','Mexico',50,0,'N'),(1724,'Palma','Palma',37,0,'N'),(1725,'Polyanka','Polyanka',1,0,'N'),(1726,'Nevinnomyssk','Nevinnomyssk',1,0,'N'),(1727,'Fuengirola','Fuengirola',37,0,'N'),(1728,'Zabrze','Zabrze',28,0,'N'),(1729,'W','W',48,0,'N'),(1730,'San Carlos','San Carlos',32,0,'N'),(1731,'Nyagan','Nyagan',1,0,'N'),(1732,'Dubna','Dubna',1,0,'N'),(1733,'Baotou','Baotou',4,0,'N'),(1734,'Poway','Poway',2,0,'N'),(1735,'Renton','Renton',2,0,'N'),(1736,'Patras','Patras',67,0,'N'),(1737,'Karlskrona','Karlskrona',44,0,'N'),(1738,'Maasdijk','Maasdijk',8,0,'N'),(1739,'Hsinchu','Hsinchu',13,0,'N'),(1740,'Pleasanton','Pleasanton',2,0,'N'),(1741,'Taubat','Taubat',16,0,'N'),(1742,'Hermosillo','Hermosillo',50,0,'N'),(1743,'Woburn','Woburn',2,0,'N'),(1744,'Lecheria','Lecheria',32,0,'N'),(1745,'Leiden','Leiden',8,0,'N'),(1746,'Urengoy','Urengoy',1,0,'N'),(1747,'Marina','Marina',6,0,'N'),(1748,'Mytishchi','Mytishchi',1,0,'N'),(1749,'Miaoli','Miaoli',13,0,'N'),(1750,'Skopje','Skopje',68,0,'N'),(1751,'Vern','Vern',9,0,'N'),(1752,'Cartagena','Cartagena',37,0,'N'),(1753,'Vancouver','Vancouver',10,0,'N'),(1754,'Yokohama','Yokohama',69,0,'N'),(1755,'Pushchino','Pushchino',1,0,'N'),(1756,'Quer','Quer',50,0,'N'),(1757,'Mclean','Mclean',2,0,'N'),(1758,'Juarez','Juarez',50,0,'N'),(1759,'Claremore','Claremore',2,0,'N'),(1760,'Halle','Halle',3,0,'N'),(1761,'Brest','Brest',20,0,'N'),(1762,'M','M',32,0,'N'),(1763,'Radom','Radom',28,0,'N'),(1764,'Vvedenskaya','Vvedenskaya',1,0,'N'),(1765,'Rosario','Rosario',70,0,'N'),(1766,'Slave Lake','Slave Lake',10,0,'N'),(1767,'Hyderabad','Hyderabad',49,0,'N'),(1768,'Palo Alto','Palo Alto',2,0,'N'),(1769,'Ephrata','Ephrata',2,0,'N'),(1770,'Tainan','Tainan',13,0,'N'),(1771,'Valley','Valley',3,0,'N'),(1772,'Henley','Henley',7,0,'N'),(1773,'Serpukhov','Serpukhov',1,0,'N'),(1774,'Kuantan','Kuantan',15,0,'N'),(1775,'Trzin','Trzin',63,0,'N'),(1776,'Nonthaburi','Nonthaburi',43,0,'N'),(1777,'Cicero','Cicero',2,0,'N'),(1778,'Singapore','Singapore',71,0,'N'),(1779,'Gloucester','Gloucester',7,0,'N'),(1780,'Erfurt','Erfurt',3,0,'N'),(1781,'Milnor','Milnor',2,0,'N'),(1782,'Haidian','Haidian',4,0,'N'),(1783,'Gate City','Gate City',2,0,'N'),(1784,'Osaka','Osaka',69,0,'N'),(1785,'Nanning','Nanning',4,0,'N'),(1786,'Stavanger','Stavanger',12,0,'N'),(1787,'Neijiang','Neijiang',4,0,'N'),(1788,'Hagen','Hagen',3,0,'N'),(1789,'Colorado Springs','Colorado Springs',2,0,'N'),(1790,'Gainesville','Gainesville',2,0,'N'),(1791,'Wenatchee','Wenatchee',2,0,'N'),(1792,'Lvov','Lvov',6,0,'N'),(1793,'Porto Alegre','Porto Alegre',16,0,'N'),(1794,'Samut','Samut',43,0,'N'),(1795,'Krasnoarmeysk','Krasnoarmeysk',1,0,'N'),(1796,'Exeter','Exeter',7,0,'N'),(1797,'Dar Es Salaam','Dar Es Salaam',72,0,'N'),(1798,'Taoy','Taoy',13,0,'N'),(1799,'Cathedral City','Cathedral City',2,0,'N'),(1800,'Zamboanga','Zamboanga',26,0,'N'),(1801,'Marco','Marco',11,0,'N'),(1802,'Cluj','Cluj',17,0,'N'),(1803,'Malm','Malm',44,0,'N'),(1804,'Mississauga','Mississauga',10,0,'N'),(1805,'Cairo','Cairo',73,0,'N'),(1806,'Severodvinsk','Severodvinsk',1,0,'N'),(1807,'Birsk','Birsk',1,0,'N'),(1808,'Langfang','Langfang',4,0,'N'),(1809,'Lander','Lander',2,0,'N'),(1810,'Vulcan','Vulcan',17,0,'N'),(1811,'Ust-ilimsk','Ust-ilimsk',1,0,'N'),(1812,'Crystal','Crystal',2,0,'N'),(1813,'Piter','Piter',1,0,'N'),(1814,'Zaporizhzhya','Zaporizhzhya',6,0,'N'),(1815,'New Freedom','New Freedom',2,0,'N'),(1816,'La Paz','La Paz',74,0,'N'),(1817,'Barnaul','Barnaul',1,0,'N'),(1818,'Ashland','Ashland',2,0,'N'),(1819,'Duluth','Duluth',2,0,'N'),(1820,'Fountain Valley','Fountain Valley',2,0,'N'),(1821,'Haiku','Haiku',2,0,'N'),(1822,'Frankenmuth','Frankenmuth',2,0,'N'),(1823,'Salatiga','Salatiga',19,0,'N'),(1824,'Belleville','Belleville',2,0,'N'),(1825,'Henderson','Henderson',2,0,'N'),(1826,'Clearfield','Clearfield',2,0,'N'),(1827,'Vsevolozhsk','Vsevolozhsk',1,0,'N'),(1828,'Kerch','Kerch',6,0,'N'),(1829,'Kathmandu','Kathmandu',75,0,'N'),(1830,'Post Falls','Post Falls',2,0,'N'),(1831,'Chetumal','Chetumal',50,0,'N'),(1832,'Reston','Reston',2,0,'N'),(1833,'Delhi','Delhi',49,0,'N'),(1834,'Coquimbo','Coquimbo',66,0,'N'),(1835,'Troitsk','Troitsk',1,0,'N'),(1836,'Laconia','Laconia',2,0,'N'),(1837,'Rugby','Rugby',7,0,'N'),(1838,'Lake Havasu City','Lake Havasu City',2,0,'N'),(1839,'Leningrad','Leningrad',1,0,'N'),(1840,'Gagarinskiy','Gagarinskiy',1,0,'N'),(1841,'Villa Regina','Villa Regina',70,0,'N'),(1842,'P','P',45,0,'N'),(1843,'Lappeenranta','Lappeenranta',55,0,'N'),(1844,'Yuzhno-sakhalinsk','Yuzhno-sakhalinsk',1,0,'N'),(1845,'Shatura','Shatura',1,0,'N'),(1846,'Tura','Tura',1,0,'N'),(1847,'Mysen','Mysen',12,0,'N'),(1848,'Clearwater','Clearwater',2,0,'N'),(1849,'Vestby','Vestby',12,0,'N'),(1850,'Blagoveshchensk','Blagoveshchensk',1,0,'N'),(1851,'Leer','Leer',3,0,'N'),(1852,'Pattaya','Pattaya',43,0,'N'),(1853,'Afyonkarahisar','Afyonkarahisar',35,0,'N'),(1854,'Arya','Arya',1,0,'N'),(1855,'Wilmington','Wilmington',2,0,'N'),(1856,'Hammond','Hammond',2,0,'N'),(1857,'Pittsburgh','Pittsburgh',2,0,'N'),(1858,'Jhansi','Jhansi',49,0,'N'),(1859,'Yellowknife','Yellowknife',10,0,'N'),(1860,'Lugoj','Lugoj',17,0,'N'),(1861,'Soest','Soest',3,0,'N'),(1862,'Volsk','Volsk',1,0,'N'),(1863,'Dortmund','Dortmund',3,0,'N'),(1864,'Orekhovo-zuevo','Orekhovo-zuevo',1,0,'N'),(1865,'Sakura','Sakura',69,0,'N'),(1866,'Ashburn','Ashburn',2,0,'N'),(1867,'Port-of-spain','Port-of-spain',76,0,'N'),(1868,'Kiel','Kiel',3,0,'N'),(1869,'Ansonia','Ansonia',2,0,'N'),(1870,'Moulineaux','Moulineaux',9,0,'N'),(1871,'Leninsk-kuznetskiy','Leninsk-kuznetskiy',1,0,'N'),(1872,'Korenovsk','Korenovsk',1,0,'N'),(1873,'Vichuga','Vichuga',1,0,'N'),(1874,'Maardu','Maardu',57,0,'N'),(1875,'Shiojiri','Shiojiri',69,0,'N'),(1876,'Espinho','Espinho',34,0,'N'),(1877,'Rawalpindi','Rawalpindi',41,0,'N'),(1878,'Ling','Ling',4,0,'N'),(1879,'Yankton','Yankton',2,0,'N'),(1880,'Ancona','Ancona',11,0,'N'),(1881,'Marki','Marki',28,0,'N'),(1882,'Sampaloc','Sampaloc',26,0,'N'),(1883,'Beltsville','Beltsville',2,0,'N'),(1884,'Kamyshin','Kamyshin',1,0,'N'),(1885,'Villeurbanne','Villeurbanne',9,0,'N'),(1886,'Maykop','Maykop',1,0,'N'),(1887,'Elista','Elista',1,0,'N'),(1888,'Luki','Luki',1,0,'N'),(1889,'Pamplona','Pamplona',37,0,'N'),(1890,'Safonovo','Safonovo',1,0,'N'),(1891,'Zelenokumsk','Zelenokumsk',1,0,'N'),(1892,'Novopokrovka','Novopokrovka',1,0,'N'),(1893,'Chegdomyn','Chegdomyn',1,0,'N'),(1894,'Mega','Mega',1,0,'N'),(1895,'Austin','Austin',2,0,'N'),(1896,'Agalatovo','Agalatovo',1,0,'N'),(1897,'Poznan','Poznan',28,0,'N'),(1898,'Szentendre','Szentendre',45,0,'N'),(1899,'Lumberton','Lumberton',2,0,'N'),(1900,'Truskavets','Truskavets',6,0,'N'),(1901,'Kachkanar','Kachkanar',1,0,'N'),(1902,'Crici','Crici',16,0,'N'),(1903,'Klaipeda','Klaipeda',29,0,'N'),(1904,'Joshua Tree','Joshua Tree',2,0,'N'),(1905,'Mogilev','Mogilev',20,0,'N'),(1906,'Pinole','Pinole',2,0,'N'),(1907,'Boise','Boise',2,0,'N'),(1908,'Rome City','Rome City',2,0,'N'),(1909,'Jaguari','Jaguari',16,0,'N'),(1910,'Potomac','Potomac',2,0,'N'),(1911,'Glasgow','Glasgow',7,0,'N'),(1912,'Sakhalin','Sakhalin',1,0,'N'),(1913,'Tatarstan','Tatarstan',1,0,'N'),(1914,'Nizhnyaya Tura','Nizhnyaya Tura',1,0,'N'),(1915,'Navashino','Navashino',1,0,'N'),(1916,'Englewood','Englewood',2,0,'N'),(1917,'Sverdlovsk','Sverdlovsk',1,0,'N'),(1918,'Espoo','Espoo',55,0,'N'),(1919,'Novomoskovsk','Novomoskovsk',1,0,'N'),(1920,'Ulyanovka','Ulyanovka',1,0,'N'),(1921,'Uzhhorod','Uzhhorod',6,0,'N'),(1922,'Provo','Provo',2,0,'N'),(1923,'Vaassen','Vaassen',8,0,'N'),(1924,'Beloretsk','Beloretsk',1,0,'N'),(1925,'Lytkarino','Lytkarino',1,0,'N'),(1926,'Summerville','Summerville',2,0,'N'),(1927,'Coatzacoalcos','Coatzacoalcos',50,0,'N'),(1928,'Bad Langensalza','Bad Langensalza',3,0,'N'),(1929,'Nizhnekamsk','Nizhnekamsk',1,0,'N'),(1930,'Kerman','Kerman',39,0,'N'),(1931,'Visaginas','Visaginas',29,0,'N'),(1932,'Aljubarrota','Aljubarrota',34,0,'N'),(1933,'Managua','Managua',77,0,'N'),(1934,'Severomorsk','Severomorsk',1,0,'N'),(1935,'Council Bluffs','Council Bluffs',2,0,'N'),(1936,'Zalau','Zalau',17,0,'N'),(1937,'Ramenskoye','Ramenskoye',1,0,'N'),(1938,'Kursk','Kursk',1,0,'N'),(1939,'Balakhna','Balakhna',1,0,'N'),(1940,'Volendam','Volendam',8,0,'N'),(1941,'Viersen','Viersen',3,0,'N'),(1942,'Sayanogorsk','Sayanogorsk',1,0,'N'),(1943,'Fernando De La Mora','Fernando De La Mora',61,0,'N'),(1944,'Tilsonburg','Tilsonburg',10,0,'N'),(1945,'Paldiski','Paldiski',57,0,'N'),(1946,'Grand Rapids','Grand Rapids',2,0,'N'),(1947,'Vyksa','Vyksa',1,0,'N'),(1948,'Tjumen','Tjumen',1,0,'N'),(1949,'Solyanka','Solyanka',1,0,'N'),(1950,'Michurinsk','Michurinsk',1,0,'N'),(1951,'Haugesund','Haugesund',12,0,'N'),(1952,'Kurilsk','Kurilsk',1,0,'N'),(1953,'Neryungri','Neryungri',1,0,'N'),(1954,'Oulu','Oulu',55,0,'N'),(1955,'Makati','Makati',26,0,'N'),(1956,'Zvenigovo','Zvenigovo',1,0,'N'),(1957,'Arvada','Arvada',2,0,'N'),(1958,'Krasnoyarsk-45','Krasnoyarsk-45',1,0,'N'),(1959,'Brookfield','Brookfield',2,0,'N'),(1960,'Rizhao','Rizhao',4,0,'N'),(1961,'Bayfield','Bayfield',2,0,'N'),(1962,'Beihai','Beihai',4,0,'N'),(1963,'Zagreb','Zagreb',78,0,'N'),(1964,'Dobritch','Dobritch',23,0,'N'),(1965,'Al','Al',16,0,'N'),(1966,'Handan','Handan',4,0,'N'),(1967,'Lanzhou','Lanzhou',4,0,'N'),(1968,'Novonikolsk','Novonikolsk',1,0,'N'),(1969,'Obninsk','Obninsk',1,0,'N'),(1970,'Castelnuovo Rangone','Castelnuovo Rangone',11,0,'N'),(1971,'Samut Sakhon','Samut Sakhon',43,0,'N'),(1972,'Potsdam','Potsdam',3,0,'N'),(1973,'Shelton','Shelton',2,0,'N'),(1974,'Colima','Colima',50,0,'N');
/*!40000 ALTER TABLE `site_cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_countries`
--

DROP TABLE IF EXISTS `site_countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_countries` (
  `sc_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sc_title` varchar(32) NOT NULL COMMENT 'страна',
  `sc_date_add` int(11) DEFAULT NULL COMMENT 'дата записи',
  PRIMARY KEY (`sc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8 COMMENT='Страны';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_countries`
--

LOCK TABLES `site_countries` WRITE;
/*!40000 ALTER TABLE `site_countries` DISABLE KEYS */;
INSERT INTO `site_countries` VALUES (1,'Russian Federation',1381063206),(2,'United States',1381063391),(3,'Germany',1381065059),(4,'China',1381071709),(5,'Colombia',1381071764),(6,'Ukraine',1381079427),(7,'United Kingdom',1381080452),(8,'Netherlands',1381114026),(9,'France',1381121744),(10,'Canada',1381175045),(11,'Italy',1381200837),(12,'Norway',1381397907),(13,'Taiwan',1381477207),(14,'Israel',1381655518),(15,'Malaysia',1381698428),(16,'Brazil',1381742786),(17,'Romania',1381790672),(18,'Belgium',1381909420),(19,'Indonesia',1381957272),(20,'Belarus',1382095794),(21,'Denmark',1382259241),(22,'Moldova, Republic of',1382384101),(23,'Bulgaria',1382392262),(24,'Ghana',1382549797),(25,'Puerto Rico',1382590751),(26,'Philippines',1382887487),(27,'Czech Republic',1382973804),(28,'Poland',1383121816),(29,'Lithuania',1383360960),(30,'Luxembourg',1383415738),(31,'United Arab Emirates',1383439973),(32,'Venezuela',1383441013),(33,'Switzerland',1383610758),(34,'Portugal',1383893069),(35,'Turkey',1383942697),(36,'Georgia',1384186863),(37,'Spain',1384365754),(38,'South Africa',1384556214),(39,'Iran, Islamic Republic of',1384801078),(40,'Korea, Republic of',1385030004),(41,'Pakistan',1385112747),(42,'Kazakhstan',1385350019),(43,'Thailand',1385628366),(44,'Sweden',1385662818),(45,'Hungary',1385891928),(46,'Slovakia',1385970545),(47,'Australia',1386153270),(48,'Austria',1386269656),(49,'India',1386424140),(50,'Mexico',1386993954),(51,'Costa Rica',1387000108),(52,'Turkmenistan',1387270577),(53,'Nigeria',1387465909),(54,'Serbia',1387530605),(55,'Finland',1387700064),(56,'Hong Kong',1388009163),(57,'Estonia',1388078057),(58,'Saudi Arabia',1389257901),(59,'Ecuador',1389490193),(60,'Vietnam',1389543444),(61,'Paraguay',1390172173),(62,'El Salvador',1391063182),(63,'Slovenia',1391770345),(64,'Uruguay',1391900946),(65,'Latvia',1392146531),(66,'Chile',1392149202),(67,'Greece',1392382010),(68,'Macedonia',1392645935),(69,'Japan',1392720911),(70,'Argentina',1392828354),(71,'Singapore',1393020086),(72,'Tanzania, United Republic of',1393330419),(73,'Egypt',1393416017),(74,'Bolivia',1393523738),(75,'Nepal',1393855376),(76,'Trinidad and Tobago',1394628913),(77,'Nicaragua',1396100780),(78,'Croatia',1396571971);
/*!40000 ALTER TABLE `site_countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_events`
--

DROP TABLE IF EXISTS `site_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_events` (
  `se_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `se_title` varchar(255) NOT NULL DEFAULT '',
  `se_body` text NOT NULL,
  `se_url` varchar(255) DEFAULT NULL COMMENT 'URL of article',
  `se_date_publ` date DEFAULT NULL,
  `se_date_add` datetime DEFAULT NULL,
  `se_date_change` datetime DEFAULT NULL,
  `se_status` enum('N','Y') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`se_id`),
  KEY `sn_date_change` (`se_date_change`)
) ENGINE=MyISAM AUTO_INCREMENT=78 DEFAULT CHARSET=utf8 COMMENT='News';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_events`
--

LOCK TABLES `site_events` WRITE;
/*!40000 ALTER TABLE `site_events` DISABLE KEYS */;
INSERT INTO `site_events` VALUES (13,'9 декабря 2013 года -  ярмарка вакансий для студентов и выпускников колледжей и техникумов  «Профессиональный старт»','«Профессиональный старт»\r\n\r\nЯрмарка вакансий для студентов и выпускников колледжей и техникумов\r\n\r\nИщешь работу? Хочешь стать профессионалом?\r\n\r\n9 декабря 2013 в Санкт-Петербурге десятки компаний-работодателей ждут тебя на Ярмарке вакансий «Профессиональный старт» для студентов и выпускников колледжей и техникумов. Узнай все о том, как попасть в крупную компанию и найди работу по душе!\r\n\r\nСпециально для тебя были разработаны уникальные мастер-классы по построению карьеры и профориентационное тестирование. Полную программу мероприятий ты можешь найти здесь. Не забудь зарегистрироваться - количество мест ограничено!\r\n\r\nПрофессиональный старт – это возможность:\r\n\r\n- выбрать компанию, пообщаться с ее представителями и получить приглашение на работу или практику;\r\n\r\n- узнать, как быстро найти работу и развить свои сильные стороны на бесплатных семинарах и мастер-классах;\r\n\r\n- понять, в какой сфере у тебя больше возможностей для развития, пройдя бесплатное профориентационное тестирование.\r\n\r\nЯрмарка вакансий - это твой профессиональный старт! Найди работу по душе!\r\n\r\nУчастие для соискателей в Ярмарке бесплатное. Просто пройди регистрацию на сайте www.profystart.ru\r\n\r\nИнформация по участию работодателей здесь\r\n\r\nЯрмарка пройдет в Конгресс-центре гостиницы Holiday Inn «Московские ворота». Московский проспект, д. 97А, (рядом с метро «Московские ворота»).\r\n\r\nwww.vk.com/event60841068','9 dekabrya 2013 goda -  yarmarka vakansiy dlya studentov i vipusknikov kolledzhey i tehnikumov  professional_niy start','2013-11-07','2013-11-07 09:49:38','2013-12-03 11:52:38','Y'),(3,'27 августа - Ярмарка вакансий в Московском районе','27 августа - Ярмарка вакансий в Московском районе','A_vot_i_ethyo_test_novost','2013-08-19','2006-11-25 17:14:15','2013-08-19 15:55:57','Y'),(4,'12 СЕНТЯБРЯ 2013г. ЯРМАРКА ВАКАНСИЙ рабочих и учебных мест','12 СЕНТЯБРЯ 2013г. ЯРМАРКА ВАКАНСИЙ рабочих и учебных мест \r\nДС \"Юбилейный\" пр. Добролюбова, 18 м. Спортивная\r\n11.00 - 17.30\r\nвход свободный\r\nВакансии предприятий Санкт-Петербурга\r\nБанк вакансий Службы Занятости населения Санкт-Петербурга\r\nКонсультации психологов\r\nУслуги по профориентации','12 sentyabrya 2013g. yarmarka vakansij rabochih i uchebnih mest','2013-08-05','2013-08-05 23:31:33','2013-08-19 15:41:40','Y'),(5,'12 сентября - Ярмарка Вакансий учебных и рабочих мест','12 сентября 2013 г. в 11.00-17.30 в ДС \"Юбилейный\" (пр. Добролюбова 18, ст.м. \"Спортивная\") состоится Ярмарка Вакансий учебных и рабочих мест.\r\n\r\n- Собеседование с работодателями СПб;\r\n- Общегородской банк вакансий СПб;\r\n- Консультации специалистов.\r\n\r\nВХОД СВОБОДНЫЙ.','12 sentyabrya - yarmarka vakansiy uchebnih i rabochih mest','2013-09-12','2013-09-09 11:10:08','2013-09-10 10:07:46','N');
/*!40000 ALTER TABLE `site_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_fragments`
--

DROP TABLE IF EXISTS `site_fragments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_fragments` (
  `sf_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sf_id_parent` int(11) DEFAULT NULL,
  `sf_id_project` int(10) unsigned NOT NULL DEFAULT '0',
  `sf_name` varchar(255) NOT NULL DEFAULT '',
  `sf_id_name` varchar(100) DEFAULT NULL,
  `sf_body` text NOT NULL,
  `sf_desc` text NOT NULL,
  `sf_status` enum('N','Y') NOT NULL DEFAULT 'N',
  `sf_publish` datetime DEFAULT NULL,
  UNIQUE KEY `f_id` (`sf_id`),
  KEY `f_status` (`sf_status`),
  KEY `f_parent` (`sf_id_parent`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Фрагменты вставок сайта';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_fragments`
--

LOCK TABLES `site_fragments` WRITE;
/*!40000 ALTER TABLE `site_fragments` DISABLE KEYS */;
INSERT INTO `site_fragments` VALUES (1,2,1,'Заголовок страниц','page.header','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n    <title>{m_title}</title>\r\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset={METACHARSET}\" />\r\n    <meta name=\"keywords\" content=\"{m_keywords}\" />\r\n    <meta name=\"description\" content=\"{m_description}\" />\r\n    <meta name=\"distribution\" content=\"global\" />\r\n    <meta name=\"robots\" content=\"all\" />\r\n    <meta name=\"author\" content=\"Dimitry Shirokovskiy, phpwebstudio.com\" />\r\n    <meta http-equiv=\"pragma\" content=\"no-cache\" />\r\n    <meta name=\"classification\" content=\"Auto\" />\r\n    <meta name=\"Document-state\" content=\"Dynamic\" />\r\n    <link rel=\"icon\" href=\"{cfgSiteImg}favicon.ico\" type=\"image/x-icon\" />\r\n    <link rel=\"shortcut icon\" href=\"{cfgSiteImg}favicon.ico\" type=\"image/x-icon\" />\r\n    <script type=\"text/javascript\" src=\"{cfgSiteUrl}ext/js/jq.js\"></script>\r\n    <script type=\"text/javascript\" src=\"{cfgSiteUrl}ext/js/jq.mw.js\"></script>\r\n    <script type=\"text/javascript\" src=\"{cfgSiteUrl}ext/js/autocolumn.min.js\"></script>\r\n    <script type=\"text/javascript\" src=\"{cfgSiteUrl}ext/js/jq.ui.js\"></script>\r\n    <script type=\"text/javascript\" src=\"{cfgSiteUrl}ext/js/jq.ui.ru.js\"></script>\r\n    <script type=\"text/javascript\" src=\"{cfgSiteUrl}ext/js/jq.jcarousel.js\"></script>\r\n    <script type=\"text/javascript\" src=\"{cfgSiteUrl}ext/js/gui.js\"></script>\r\n    <script type=\"text/javascript\" src=\"{cfgSiteUrl}ext/js/jq.jqz.js\"></script>\r\n    <script type=\"text/javascript\" src=\"{cfgSiteUrl}ext/js/jq.z.js\"></script>\r\n\r\n    <link rel=\"stylesheet\" type=\"text/css\" href=\"{cfgSiteUrl}ext/css/general.css\" />\r\n    <link rel=\"stylesheet\" type=\"text/css\" href=\"{cfgSiteUrl}ext/css/ui/custom.css\" />\r\n    <link rel=\"stylesheet\" type=\"text/css\" href=\"{cfgSiteUrl}ext/css/jq.tools.css\" />\r\n    <link rel=\"stylesheet\" type=\"text/css\" href=\"{cfgSiteUrl}ext/css/jq.fb.css\" media=\"screen\" />\r\n    <link rel=\"stylesheet\" type=\"text/css\" href=\"{cfgSiteUrl}ext/css/jq.jqz.css\" />\r\n    <script type=\"text/javascript\" src=\"{cfgSiteUrl}ext/js/jq.fb.js\"></script>\r\n    <script type=\"text/javascript\" src=\"{cfgSiteUrl}ext/js/jq.t.js\"></script>\r\n\r\n    <!--[if lt IE 9]><script type=\"text/javascript\" src=\"{cfgSiteUrl}ext/js/excanvas.js\"></script><![endif]-->\r\n    <script type=\"text/javascript\" src=\"{cfgSiteUrl}ext/js/jq.tc.js\"></script>\r\n    <script type=\"text/javascript\" src=\"{cfgSiteUrl}ext/js/jq.mi.js\"></script>\r\n\r\n    <script type=\"text/javascript\" src=\"{cfgSiteUrl}ext/js/funcs.js\"></script>\r\n    <script type=\"text/javascript\">\r\n        $(function(){\r\n            //\r\n        });\r\n    </script>\r\n\r\n    <!-- Yandex.Metrika counter -->\r\n    <script type=\"text/javascript\">\r\n        (function (d, w, c) {\r\n            (w[c] = w[c] || []).push(function() {\r\n                try {\r\n                    w.yaCounter22257050 = new Ya.Metrika({id:22257050,\r\n                        clickmap:true,\r\n                        trackLinks:true,\r\n                        accurateTrackBounce:true});\r\n                } catch(e) { }\r\n            });\r\n\r\n            var n = d.getElementsByTagName(\"script\")[0],\r\n                    s = d.createElement(\"script\"),\r\n                    f = function () { n.parentNode.insertBefore(s, n); }\r\n            s.type = \"text/javascript\";\r\n            s.async = true;\r\n            s.src = (d.location.protocol == \"https:\" ? \"https:\" : \"http:\") + \"//mc.yandex.ru/metrika/watch.js\";\r\n\r\n            if (w.opera == \"[object Opera]\") {\r\n                d.addEventListener(\"DOMContentLoaded\", f, false);\r\n            } else { f(); }\r\n        })(document, window, \"yandex_metrika_callbacks\");\r\n    </script>\r\n    <noscript><div><img src=\"//mc.yandex.ru/watch/22257050\" style=\"position:absolute; left:-9999px;\" alt=\"\" /></div></noscript>\r\n    <!-- /Yandex.Metrika counter -->\r\n</head>\r\n\r\n<body bgcolor=\"#F5F3F7\" text=\"#2E2E2E\" link=\"#777777\" vlink=\"#696969\" alink=\"#4F4F4F\" leftmargin=\"0\" topmargin=\"0\" marginheight=\"0\" marginwidth=\"0\" background=\"{cfgSiteImg}bg-page.gif\">','test','Y','2014-01-03 20:12:30'),(2,2,1,'Завершение страницы','page.footer','<div class=\"under-construction\" id=\"uc\">\r\n    <img src=\"{cfgSiteImg}content/under-construction.gif\" alt=\"Under Construction\"/>\r\n    <h3>Раздел в стадии разработки!</h3> Приносим свои извинения за временные неудобства.\r\n</div>\r\n\r\n<div id=\"form_adv_website_request\">\r\n    <form id=\"adv_website_request\" method=\"POST\" action=\"{cfgSiteUrl}adv_ad.php\">\r\n        <input type=\"hidden\" name=\"adv\" value=\"web\"/>\r\n    <h4>Email</h4>\r\n        <input type=\"text\" id=\"strEmail\" name=\"strEmail\" class=\"plane\" maxlength=\"255\" placeholder=\"your@email.here\"/>\r\n    <h4>Телефон</h4>\r\n        <input type=\"text\" id=\"strPhone\" name=\"strPhone\" class=\"plane\" maxlength=\"255\" placeholder=\"контактный телефон\"/>\r\n    <h4>Контактное лицо</h4>\r\n        <input type=\"text\" id=\"strContact\" name=\"strContact\" class=\"plane\" maxlength=\"255\" placeholder=\"как к Вам обращаться (ФИО)\"/>\r\n    <h4>Текст обращения</h4>\r\n        <textarea id=\"strText\" name=\"strText\" style=\"overflow-y:auto;word-wrap:break-word\"\r\n                  class=\"plane\" placeholder=\"HTML-тэги не допускаются\">{strText}</textarea>\r\n        <div>\r\n        <!--CAPTCHA-->\r\n        <img src=\"?get_img=captcha\" border=\"0\" class=\"fl inl\">\r\n        <input type=\"text\" name=\"strCodeString\" class=\"plane\" maxlength=\"4\" style=\"width:56px;letter-spacing:5px\" onKeyUp=\"if(this.value.length >= this.maxLength) this.blur(); return false;\">\r\n            <span>введите буквы/цифры с картинки</span>\r\n        <!--//CAPTCHA-->\r\n        </div>\r\n    </form>\r\n    <div><button>Отправить</button></div>\r\n</div>\r\n\r\n<div id=\"overlay_login_box\" class=\"overlay\">\r\n    <div class=\"contentWrap\">\r\n        <form action=\"/profile\" method=\"POST\">\r\n            <input type=\"hidden\" name=\"auth\" value=\"true\"/>\r\n            <h1 class=\"title\">Личный кабинет</h1>\r\n            <h4>Email</h4>\r\n            <input type=\"text\" id=\"strAuthLogin\" name=\"strAuthLogin\" class=\"plane\" maxlength=\"255\" placeholder=\"ваш-адрес@email.ru\"/>\r\n            <h4>Пароль</h4>\r\n            <input type=\"password\" id=\"strAuthPassword\" name=\"strAuthPassword\" class=\"plane\" maxlength=\"255\" />\r\n            <button>Сим сим откройся</button>\r\n        </form>\r\n        <p><a href=\"javascript: void(0)\" id=\"closeLoginForm\">Восстановить пароль</a><a href=\"#\" class=\"recovery_form_trigger\" rel=\"#overlay_recovery_box\"></a></p>\r\n    </div>\r\n</div>\r\n\r\n<div id=\"overlay_recovery_box\" class=\"overlay\">\r\n    <div class=\"contentWrap\">\r\n        <form action=\"/lib/api/recovery.php\" method=\"post\">\r\n            <input type=\"hidden\" name=\"type\" value=\"recovery\"/>\r\n            <h1 class=\"title\">Выслать новый пароль</h1>\r\n            <h4>Зарегистрированный ранее Email</h4>\r\n            <input type=\"text\" id=\"strRecoveryLogin\" name=\"strRecoveryLogin\" class=\"plane\" maxlength=\"255\" placeholder=\"ваш-адрес@email.ru\"/>\r\n            <button>Получить пароль</button>\r\n        </form>\r\n    </div>\r\n</div>\r\n\r\n<div id=\"detected_region_box\">\r\n    <h4 class=\"ac\">Мы определили что Вы из <span></span>.</h4>\r\n    <p class=\"ac\">Это верно?</p>\r\n    <p class=\"ac\"><a href=\"javascript:void(0)\" city=\"\" id=\"city_confirm_yes\">Да</a> / <a href=\"javascript:void(0)\" city=\"Санкт-Петербург\" id=\"city_confirm_no\">Нет, показать Санкт-Петербург</a></p>\r\n    <p class=\"note\">P.S. Список резюме и вакансий на первой странице, а также при поиске отображается с учётом выбранного города!</p>\r\n</div>\r\n\r\n<script type=\"text/javascript\">\r\n    jQuery(document).ready(function($) {\r\n        var sizeW = 265; /* also see in jq.tools.css for #overlay rules */\r\n        var sizeH = 160;\r\n\r\n        // Overlay\r\n        function authOverlay() {\r\n            // вычисление размеров для всплывающего слоя\r\n            $(\'#overlay_login_box\').css(\'width\',sizeW);\r\n            // overlay: loading external pages - if the function argument is given to overlay, it is assumed to be the onBeforeLoad event listener\r\n            $(\".auth_form_trigger\").overlay({\r\n                top: ($(window).height() - sizeH)/2,\r\n                left: ($(window).width() - sizeW)/2 - 50,\r\n                mask: \'#939393\',\r\n                fixed: false,\r\n                closeOnClick: true\r\n            });\r\n\r\n            $(\'#overlay_recovery_box\').css(\'width\',sizeW);\r\n\r\n            $(\".recovery_form_trigger\").overlay({\r\n                top: ($(window).height() - sizeH)/2,\r\n                left: ($(window).width() - sizeW)/2 - 50,\r\n                mask: \'#939393\',\r\n                fixed: false,\r\n                closeOnClick: true,\r\n                onBeforeLoad: function () {\r\n                    //$(\"a[rel]\").overlay().close();\r\n                }\r\n            });\r\n        }\r\n\r\n        authOverlay();\r\n\r\n        $(\'#closeLoginForm\').click(function () {\r\n            $(\".auth_form_trigger\").overlay().close();\r\n            setTimeout(function () {\r\n                $(\".recovery_form_trigger\").overlay().load();\r\n            }, 500);\r\n        });\r\n\r\n        $(window).resize(function() {\r\n            //authOverlay();\r\n        });\r\n\r\n        $(\"#overlay_recovery_box form\").submit(function(event) {\r\n            /* stop form from submitting normally */\r\n            event.preventDefault();\r\n\r\n            /* get some values from elements on the page: */\r\n            var $form = $( this ),\r\n                url = $form.attr( \'action\'),\r\n                validation = true,\r\n                type = $form.find(\'input[name=type]\').val(),\r\n                strEmail = $( \'#strRecoveryLogin\' ).val();\r\n\r\n            if (isEmpty(strEmail)) {\r\n                validation = false;\r\n                alert(\'Ошибка: Вы не заполнили поле Email.\');\r\n                $( \'#strRecoveryLogin\').addClass(\'err\').change(function(){\r\n                    $(this).removeClass(\'err\');\r\n                });\r\n                return false;\r\n            }\r\n\r\n            if (!isEmail(strEmail)) {\r\n                validation = false;\r\n                alert(\'Ошибка: Вы не правильно заполнили поле Email.\');\r\n                $( \'#strRecoveryLogin\').addClass(\'err\').change(function(){\r\n                    $(this).removeClass(\'err\');\r\n                });\r\n                return false;\r\n            }\r\n\r\n            if (validation) {\r\n                /* Send the data using post */\r\n                var posting = $.post( url, $form.serialize(), null, \'json\' );\r\n\r\n                /* Put the results in a div */\r\n                posting.done(function( data ) {\r\n                    if (data.status == 1) {\r\n//                      console.log(\'OK\');\r\n                        $form.find(\"input[type=text]\").val(\'\');\r\n\r\n                        $(\".recovery_form_trigger\").overlay().close();\r\n                    } else {\r\n                        console.log(\'NO RESULT. ERRORS!\');\r\n                    }\r\n\r\n                    setTimeout(function () {\r\n                        $.fancybox(data.message);\r\n                    }, 500)\r\n                });\r\n            }\r\n        });\r\n\r\n        //$.fancybox(\"<p>На указанный Вами адрес выслан пароль.</p><p>В ближайшее будущее проверьте папку Входящие Вашего почтового ящика.</p><p>Убедитесь что письмо не попало в Спам.</p><p>Удачной авторизации!</p>\");\r\n    });\r\n</script>\r\n\r\n<img src=\"{cfgSiteImg}ajax-circle.gif\" width=\"200\" alt=\"ajax-loader\" id=\"ajax-loader\"/>\r\n</body>\r\n</html>','Завершающие тэги страниц','Y','2014-02-17 07:55:19'),(3,1,1,'Блок новостей','fragment.sidebar.news','<div id=\"news-brief\" class=\"sidebar_block\">\r\n    <a href=\"{cfgSiteUrl}news\"><h3>НОВОСТИ</h3></a>\r\n<loop sidebar.list.news>\r\n    <div class=\"novelty\">\r\n        <a href=\"{cfgSiteUrl}news/{sn_id}\">{strNewsTitle}</a>\r\n    </div>\r\n</loop sidebar.list.news>\r\n\r\n    <div class=\"block\">\r\n        <a href=\"{cfgSiteUrl}news\"><i>Все новости</i></a>\r\n    </div>\r\n</div>','Новости в колонке сбоку на сайте.','Y','2014-01-03 20:12:53'),(4,1,1,'Блок статей','fragment.sidebar.articles','<div id=\"sidebar-dictionary\" class=\"sidebar_block\">\r\n    <a href=\"{cfgSiteUrl}dictionary\"><h3>СЛОВАРЬ ПРОФЕССИЙ</h3></a>\r\n\r\n    <loop sidebar.list.dictionary>\r\n        <div class=\"row\">\r\n            <a href=\"{strSiteUrl}dictionary/{sa_id}\">{sa_title}</a>\r\n        </div>\r\n    </loop sidebar.list.dictionary>\r\n\r\n    <div class=\"block\">\r\n        <a href=\"{cfgSiteUrl}dictionary\"><i>Весь словарь</i></a>\r\n    </div>\r\n</div>\r\n\r\n\r\n<div id=\"sidebar-articles\" class=\"sidebar_block\">\r\n    <a href=\"{cfgSiteUrl}articles\"><h3>СТАТЬИ</h3></a>\r\n\r\n    <loop sidebar.list.articles>\r\n        <div class=\"row\">\r\n            <a href=\"{strSiteUrl}article/{sa_id}\">{sa_title}</a>\r\n        </div>\r\n    </loop sidebar.list.articles>\r\n\r\n    <div class=\"block\">\r\n        <a href=\"{cfgSiteUrl}articles\"><i>Все статьи</i></a>\r\n    </div>\r\n</div>','Несколько свежих статей в боковом блоке.','Y','2014-01-03 20:13:05'),(5,1,1,'Блок распространения Журнала','fragment.sidebar.magazin','<div class=\"section\">\r\n    <div id=\"week_magazines\" class=\"brd_tu\">\r\n        <div id=\"look_of_magazine\">\r\n            <img id=\"weekThumb\" src=\"{cfgAllImg}magazines/magazine.thumb.tu.{intWeekMagazine_tu}.{intYearMagazine_tu}.jpg\" />\r\n            <button id=\"butExpand\" class=\"butExpand\"><span>Раскрыть</span></button>\r\n        </div>\r\n    </div>\r\n    <div class=\"tabs\">\r\n        <a id=\"tu\" href=\"javascript:void(0)\" data-url=\"{strUrlMagazine_tu}\" data-week=\"{intWeekMagazine_tu}\" data-year=\"{intYearMagazine_tu}\" class=\"current\">ВТ</a>\r\n        <a id=\"we\" href=\"javascript:void(0)\" data-url=\"{strUrlMagazine_we}\" data-week=\"{intWeekMagazine_we}\" data-year=\"{intYearMagazine_we}\">СР</a>\r\n        <a id=\"th\" href=\"javascript:void(0)\" data-url=\"{strUrlMagazine_th}\" data-week=\"{intWeekMagazine_th}\" data-year=\"{intYearMagazine_th}\">ЧТ</a>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"section\">\r\n    <div class=\"block\">\r\n        <div class=\"title\">Зона распространения</div>\r\n        <a href=\"{strUrlPathDistribution}\" id=\"distribution\"><img src=\"{cfgSiteImg}content/distribution.png\" alt=\"Распространение\"/></a>\r\n    </div>\r\n</div>','Блок в левом столбце сайта','Y','2014-01-03 20:13:16'),(6,1,1,'Облако тэгов','fragment.cloud.tags','<!--Ведущие компании-->\r\n<div id=\"best-companies\" class=\"\">\r\n    <h1>Вакансии ведущих компаний</h1>\r\n    <div class=\"separator\"></div>\r\n\r\n    <div id=\"cloudTags\" class=\"block\">\r\n        <ul id=\"company_tags\" class=\"jcarousel jcarousel-skin-brands\">\r\n            <loop list.top.companies>\r\n                <li><a href=\"{cfgSiteUrl}company-vacancies/{jc_id}\"><img class=\"brand\" src=\"{cfgAllImg}companies/{si_filename}\" alt=\"{jc_title}\"/></a></li>\r\n            </loop list.top.companies>\r\n        </ul>\r\n    </div>\r\n</div>\r\n\r\n<div id=\"box_footer_links\">\r\n    <ul class=\"in_row\">\r\n        <li>\r\n            <p><a href=\"{cfgSiteUrl}about\">О портале</a></p>\r\n            <p><a href=\"{cfgSiteUrl}rules\">Правила работы на портале</a></p>\r\n            <p><a href=\"#form_adv_website_request\" class=\"uc\">Реклама на сайте</a></p>\r\n            <p><a href=\"{cfgSiteUrl}contacts\">Контактная информация</a></p>\r\n        </li>\r\n        <li>\r\n            <p><a href=\"{cfgSiteUrl}for_employers\" class=\"uc\">Работодателям</a></p>\r\n            <p><a href=\"#form_adv_website_request\" class=\"uc\">Реклама в газете</a></p>\r\n            <p><a href=\"{cfgSiteUrl}sovety_rabotodatelyam\" class=\"uc\">Советы для работодателей</a></p>\r\n            <p><a href=\"{cfgSiteUrl}trainings\">Тренинги</a></p>\r\n        </li>\r\n        <li>\r\n            <p><a href=\"{cfgSiteUrl}soiskatelyam\" class=\"uc\">Соискателям</a></p>\r\n            <p><a href=\"{cfgSiteUrl}usloviya_razmesheniya_resume\" class=\"uc\">Условия размещения резюме</a></p>\r\n            <p><a href=\"{cfgSiteUrl}sovety_dlya_soiskateley\" class=\"uc\">Советы для соискателей</a></p>\r\n            <p><a href=\"{cfgSiteUrl}articles\">Статьи</a></p>\r\n        </li>\r\n    </ul>\r\n</div>\r\n\r\n<!--Вакансии в облаке-->\r\n<div id=\"tag-vacancy-cloud\">\r\n    <canvas width=\"1200\" height=\"320\" id=\"vacanciesCanvas\">\r\n        <p>К сожалению Ваш браузер не поддерживает HTML5 и поэтому Облако Тэгов не может быть отображено здесь! =(</p>\r\n    </canvas>\r\n</div>\r\n<div id=\"vacancies_tags\">\r\n    <loop list.vacancies.in.cloud>\r\n        <a href=\"{strLink}\">{strTitle}</a>\r\n    </loop list.vacancies.in.cloud>\r\n</div>\r\n\r\n<script type=\"text/javascript\">\r\njQuery(document).ready(function ($) {\r\n    $(\"#company_tags\").jcarousel({\r\n        auto: 10,\r\n        scroll: 6,\r\n        wrap: \'last\',\r\n        initCallback: mycarousel_initCallback\r\n    });\r\n});\r\n</script>','Облако летающих тэгов','Y','2014-02-17 08:00:26'),(17,2,1,'Навигация','fragment.top.nav','<div class=\"top_navigation\">\n    <h1 class=\"logo fl\"><a href=\"{cfgSiteUrl}\" title=\"{strPageTitle}\">Работа от А до Я</a></h1>\n\n    <div class=\"top_actions\">\n        <div class=\"fl\">\n            <ul class=\"adv_pass in_row\">\n                <li><a href=\"#form_adv_website_request\" class=\"uc\">Реклама <span>в ГАЗЕТЕ</span></a></li>\n                <li><a href=\"#form_adv_website_request\" class=\"uc\">Реклама <span>на САЙТЕ</span></a></li>\n            </ul>\n        </div>\n        <div class=\"phone_box fr finger\">\n            <h2>мы вам перезвоним!</h2>\n            оставьте ваш номер телефона\n        </div>\n    </div>\n\n    <div class=\"top_links\">\n        <ul id=\"items\">\n            <li><a class=\"flow\" href=\"{cfgSiteUrl}about\">О газете</a></li>\n            <li><a class=\"flow uc\" href=\"{cfgSiteUrl}contacts\">Контакты</a></li>\n            <if authed><li><a class=\"flow\" href=\"{cfgSiteUrl}profile\">Личный кабинет</a></li></if authed>\n            <ifelse authed><li><a class=\"flow auth_form_trigger\" rel=\"#overlay_login_box\" href=\"#\">Войти</a></li></ifelse authed>\n\n            <if authed><li class=\"last\"><a class=\"flow\" href=\"{cfgSiteCurrentUri}logout=1\">Выйти</a></li></if authed>\n            <ifelse authed><li class=\"last\"><a class=\"flow\" href=\"{cfgSiteUrl}registration\">Зарегистрироваться</a></li></ifelse authed>\n        </ul>\n    </div>\n\n    <div class=\"clearer\"></div>\n    <div class=\"phone_number_block\">тел.: +7 (812) <span><span>305-305</span>-7</span></div>\n\n    <div class=\"region_set\">\n        <p class=\"tr\">Регион: <a href=\"javascript: void(0)\" class=\"select_region\">{strMyCity}</a></p>\n    </div>\n\n    <div class=\"search_box\">\n        <div class=\"bar\">\n            <div class=\"tabs\">\n                <ul>\n                    <li id=\"tab1\" class=\"color1\"><a class=\"flow change-field\" href=\"javascript:void(0)\">Поиск вакансий</a></li>\n                    <li id=\"tab2\" class=\"color2\"><a class=\"flow change-field\" href=\"javascript:void(0)\">Поиск сотрудников</a></li>\n                    <li id=\"tab3\" class=\"color3\"><a class=\"flow change-field\" href=\"javascript:void(0); setSearchForm(\'resumes\')\">Создать резюме</a></li>\n                    <li id=\"tab4\" class=\"color4\"><a class=\"flow change-field\" href=\"javascript:void(0); setSearchForm(\'vacancies\')\">Создать вакансию</a></li>\n                </ul>\n            </div>\n            <div id=\"search_field\" class=\"{active_color} flow\">\n                <div><form action=\"{cfgSiteUrl}search\" method=\"post\" id=\"searh_form\">\n                    <input type=\"hidden\" id=\"categoryId\" name=\"categoryId\" value=\"\"/>\n                    <input type=\"hidden\" id=\"type\" name=\"type\" value=\"{type}\"/>\n                    <div class=\"query\">\n                        <input type=\"text\" id=\"query\" name=\"query\" value=\"{query}\" class=\"plane fl\" placeholder=\"Введите название должности или город или ID вакансии\"/>\n                        <input type=\"submit\" value=\"Найти\" class=\"plane-b\"/>\n                    </div>\n\n                    <div class=\"precision\">\n                        <span class=\"fl\">Поиск по:</span>\n                        <ul>\n                            <li><a href=\"/po-otraslyam\" id=\"categories_selection_trigger\">отраслям</a></li>\n                            <li><a href=\"/po-kompaniyam\" id=\"companies_selection_trigger\">компаниям</a></li>\n                            <li><a href=\"/po-professiyam\" id=\"profession_selection_trigger\">профессиям</a></li>\n                            <li class=\"last\"><a href=\"#\" id=\"extended_search_trigger\">расширенный</a></li>\n                        </ul>\n                    </div>\n\n                    <div class=\"search_where clearer\">\n                        <ul class=\"fl\">\n                            <li><span>Искать в</span></li>\n                            <li><input type=\"text\" id=\"search_city\" name=\"search_city\" value=\"{search_city}\" placeholder=\"во всей России\" class=\"plane\"/></li>\n                            <li class=\"tr\"><span>з/п от</span></li>\n                            <li><input type=\"text\" id=\"search_salary\" name=\"search_salary\" value=\"{search_salary}\" placeholder=\"10000\" class=\"plane\" maxlength=\"7\"/></li>\n                            <li>руб.</li>\n                        </ul>\n\n                        <div class=\"total-count fr\">Всего вакансий: <span>{strTotalCountVacancies}</span></div>\n                        <div class=\"total-count fr\">Всего резюме: <span>{strTotalCountResumes}</span></div>\n                    </div>\n                </form>\n                </div>\n            </div>\n        </div>\n    </div>\n    <div class=\"clearer\"></div>\n\n    <div id=\"regions_box\">\n        <p>Выберите Ваш регион:</p>\n        <ul>\n            <li><a href=\"#\" style=\"font-weight: bold\">Санкт-Петербург</a></li>\n            <li><a href=\"#\" style=\"font-weight: bold\">Москва</a></li>\n        <loop list.cities>\n            <li><a href=\"#\">{strCity}</a></li>\n        </loop list.cities>\n        </ul>\n    </div>\n</div>\n<!-- END OF TOP NAVIGATION  -->\n\n<div id=\"block_nav_top\">\n    <div class=\"wrapper\">\n        <table border=\"0\">\n            <tr>\n                <td><a href=\"{cfgSiteUrl}\"><img class=\"logo\" src=\"/storage/siteimg/logo_s.png\" alt=\"Работа от А до Я\"/></a></td>\n                <td><div class=\"links_box\"><a href=\"#form_adv_website_request\" class=\"uc\"><b>Реклама в ГАЗЕТЕ или на САЙТЕ</b></a></div></td>\n                <td><div class=\"phone_number_block\">тел.: +7 (812) <span><span>305-305</span>-7</span></div></td>\n                <td><div class=\"phone_box finger\">\n                    <h2>мы вам перезвоним!</h2>\n                    оставьте ваш номер телефона\n                </div></td>\n                <td class=\"tr\">\n                    <p class=\"total-count\">Всего вакансий: <a href=\"/all-vacancies\"><span>{strTotalCountVacancies}</span></a></p>\n                    <p class=\"total-count\">Всего резюме: <a href=\"/all-resumes\"><span>{strTotalCountResumes}</span></a></p>\n                </td>\n            </tr>\n        </table>\n    </div>\n</div>\n\n\n<script type=\"text/javascript\">\nvar $window    = $(window);\n$(function(){\n    var $blockNav = $(\"#block_nav_top\"), is_shown = false, topView = 90;\n    $window.scroll(function(){\n        if ($window.scrollTop() > topView && !is_shown) {\n            $blockNav.stop().slideDown(1000);\n            is_shown = true;\n        }\n        if ($window.scrollTop() <= topView && is_shown) {\n            $blockNav.stop().slideUp();\n            is_shown = false;\n        }\n    });\n\n    var preSelectedExtension = \'{preSelectedExtension}\';\n    if (preSelectedExtension != \'\') {\n        $(\'.precision a\').css({\'color\':\'#4E4E4E\', \'font-weight\':\'normal\'});\n        $(\'#\'+preSelectedExtension).css({\'color\':\'black\', \'font-weight\':\'bold\'});\n\n        if (preSelectedExtension==\'companies_selection_trigger\') {\n            $(\'#type\').val( \'companies\' );\n            $(\'#query\').attr(\'placeholder\', \'Введите название компании\');\n        }\n    }\n\n    var unknownRegion = {isUnknownRegion}\n\n    if (unknownRegion) {\n        $(\'#detected_region_box span\').html(\'{strDetectedRegionName}\');\n        $(\'#detected_region_box #city_confirm_yes\').attr(\'city\', \'{strDetectedRegionName}\');\n        $(\'#detected_region_box\').css({top: ($(window).height() / 5), left: ($(window).width() / 2 - 100)}).show();\n    }\n\n    $(\'#detected_region_box a\').click(function () {\n        $(\'#detected_region_box\').hide();\n        var clickedID = $(this).attr(\'id\');\n        console.log(\'clickedID = \' + clickedID);\n        $.post(\"/lib/api/region.php\", { city: $(this).attr(\'city\') }, function(data){\n            if (data.status == 0)\n            {\n                $(\'.select_region\').text( data.city );\n                var myHost = getMyHost(\'/{cfgCurrentUri}\');\n                if (clickedID == \'city_confirm_no\')\n                    gotoUrl( myHost );\n            }\n        }, \"json\");\n    });\n    // www.phpwebstudio.com\n});\n</script>','Навигационная площадка сайта','Y','2014-04-03 11:22:13'),(7,1,1,'Фрагмент работа в городе','fragment.sidebar.job.in.cities','<div id=\"job-in-cities\" class=\"sidebar_block\">\r\n    <h3>РАБОТА в других городах</h3>\r\n<if is.list>\r\n    <ul>\r\n<loop list>\r\n        <li><a href=\"{cfgSiteUrl}search/city/{strCityUrl}\">работа в {strCity}</a></li>\r\n</loop list>\r\n    </ul>\r\n</if is.list>\r\n</div>','Фрагмент сайта: ссылки на работу в других городах.','Y','2014-01-03 20:13:59'),(8,1,1,'Футер контента','content.footer','<div id=\"content_footer\">\r\n    <div id=\"copyright\">\r\n        <em>Внимание!</em> При обращении в организацию, просьба ссылаться на сайт газеты «Работа от А до Я» - <a class=\"link_grey\" href=\"{cfgSiteUrl}\">www.rabota-ya.ru</a>\r\n        <br>При перепечатывании материалов, активная ссылка на сайт обязательна.\r\n        <br>© Все права защищены.\r\n        <br>Новая версия сайта находится в стадии бета-тестирования.\r\n        <br>Возможны проблемы с работой некоторых страниц. Если Вы обнаружили ошибку или неточность, просим написать об этом в\r\n        <a href=\"#\">службу поддержки</a><br>\r\n        <a href=\"{cfgSiteUrl}low\">Закон о СМИ</a>\r\n    </div>\r\n</div>','Фрагмент сайта, нижний повторяющийся блок контента. Ссылки, копирайт, и т.д.','Y','2014-01-03 20:14:12'),(9,2,1,'Рекламное место под навигацией','fragment.top.banner','<!-- TOP BANNER-->\r\n<if show.top.banner>\r\n<div id=\"top-banner\">\r\n    {contentTopBanner}\r\n</div>\r\n</if show.top.banner>\r\n<!-- // TOP BANNER-->\r\n\r\n<if show.top.banners>\r\n<ul id=\"top_banner\" class=\"jcarousel-skin-raya\">\r\n<loop top.banners>\r\n    <li><a href=\"{strUrl}\" target=\"_blank\"><img src=\"{cfgAllImg}banners/{si_filename}\" alt=\"{si_desc}\" border=\"0\"/></a></li>\r\n</loop top.banners>\r\n</ul>\r\n\r\n</if show.top.banners>\r\n\r\n<table class=\"weather_block informer\">\r\n    <tr><td><span class=\"title\">Погода: <span id=\"weather-max\">{strWeatherMin} C</span></span></td></tr>\r\n    <tr><td class=\"ac\">ночью: <span id=\"weather-min\">{strWeatherMax} C</span></td></tr>\r\n</table>\r\n\r\n<table class=\"currency_block informer\">\r\n    <caption class=\"title\">Курсы валют<br/><span id=\"currency-date\">на {strInformerDate}</span></caption>\r\n    <tr><td class=\"ac\"><a target=\"_blank\" href=\"http://www.cbr.ru/\">(USD) {strCurrencyUsd}</a></td></tr>\r\n    <tr><td class=\"ac\"><a target=\"_blank\" href=\"http://www.cbr.ru/\">(EUR) {strCurrencyEur}</a></td></tr>\r\n</table>','Рекламная площадка под верхним блоком Поиска','Y','2014-02-17 07:55:47'),(10,1,1,'События','fragment.sidebar.events','<div id=\"events-brief\" class=\"sidebar_block\">\r\n    <a href=\"{cfgSiteUrl}events\"><h3>СОБЫТИЯ</h3></a>\r\n    <loop sidebar.list.events>\r\n        <div class=\"novelty\">\r\n            <a href=\"{cfgSiteUrl}events/{se_id}\">{strTitle}</a>\r\n        </div>\r\n    </loop sidebar.list.events>\r\n\r\n    <div class=\"block\">\r\n        <a href=\"{cfgSiteUrl}events\"><i>Все события</i></a>\r\n    </div>\r\n</div>','События','Y','2014-01-03 20:14:25'),(11,3,2,'Новости','fragment.news','<div id=\"news_box\">\r\n    <p><a href=\"{cfgSiteUrl}news\">Новости</a></p>\r\n<loop list.news>\r\n    <p class=\"record\"><span>{strNewsDate}</span> <a href=\"#uc\" class=\"uc\">{strNewsTitle}</a></p>\r\n</loop list.news>\r\n</div>','Короткий список новостей','Y','2013-11-19 13:45:46'),(12,4,2,'HTML HEAD','html.head','<title>{m_title}</title>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset={METACHARSET}\" />\r\n<meta name=\"keywords\" content=\"{m_keywords}\" />\r\n<meta name=\"description\" content=\"{m_description}\" />\r\n<meta name=\"distribution\" content=\"global\" />\r\n<meta name=\"robots\" content=\"all\" />\r\n<meta name=\"author\" content=\"Dimitry Shirokovskiy, phpwebstudio.com\" />\r\n<meta http-equiv=\"pragma\" content=\"no-cache\" />\r\n<meta name=\"classification\" content=\"Auto\" />\r\n<meta name=\"Document-state\" content=\"Dynamic\" />\r\n<link rel=\"icon\" href=\"{cfgSiteImg}favicon.ico?{rand}\" type=\"image/x-icon\" />\r\n<link rel=\"shortcut icon\" href=\"{cfgSiteImg}favicon.ico?{rand}\" type=\"image/x-icon\" />\r\n<script type=\"text/javascript\" src=\"{cfgSiteUrl}ext/js/jq.js\"></script>\r\n<script type=\"text/javascript\" src=\"{cfgSiteUrl}ext/js/jq.mw.js\"></script>\r\n<script type=\"text/javascript\" src=\"{cfgSiteUrl}ext/js/autocolumn.min.js\"></script>\r\n<script type=\"text/javascript\" src=\"{cfgSiteUrl}ext/js/jq.ui.js\"></script>\r\n<script type=\"text/javascript\" src=\"{cfgSiteUrl}ext/js/jq.ui.ru.js\"></script>\r\n<script type=\"text/javascript\" src=\"{cfgSiteUrl}ext/js/jq.jcarousel.js\"></script>\r\n<script type=\"text/javascript\" src=\"{cfgSiteUrl}ext/js/gui.js\"></script>\r\n<script type=\"text/javascript\" src=\"{cfgSiteUrl}ext/js/jq.jqz.js\"></script>\r\n<script type=\"text/javascript\" src=\"{cfgSiteUrl}ext/js/jq.z.js\"></script>\r\n\r\n<link rel=\"stylesheet\" type=\"text/css\" href=\"{cfgSiteUrl}ext/css/general.css\" />\r\n<link rel=\"stylesheet\" type=\"text/css\" href=\"{cfgSiteUrl}ext/css/ui/custom.css\" />\r\n<link rel=\"stylesheet\" type=\"text/css\" href=\"{cfgSiteUrl}ext/css/jq.tools.css\" />\r\n<link rel=\"stylesheet\" type=\"text/css\" href=\"{cfgSiteUrl}ext/css/jq.fb.css\" media=\"screen\" />\r\n<link rel=\"stylesheet\" type=\"text/css\" href=\"{cfgSiteUrl}ext/css/jq.jqz.css\" />\r\n<script type=\"text/javascript\" src=\"{cfgSiteUrl}ext/js/jq.fb.js\"></script>\r\n<script type=\"text/javascript\" src=\"{cfgSiteUrl}ext/js/jq.t.js\"></script>\r\n\r\n<!--[if lt IE 9]><script type=\"text/javascript\" src=\"{cfgSiteUrl}ext/js/excanvas.js\"></script><![endif]-->\r\n<script type=\"text/javascript\" src=\"{cfgSiteUrl}ext/js/jq.tc.js\"></script>\r\n<script type=\"text/javascript\" src=\"{cfgSiteUrl}ext/js/jq.mi.js\"></script>\r\n\r\n<script type=\"text/javascript\" src=\"{cfgSiteUrl}ext/js/funcs.js\"></script>','','Y','2013-11-19 13:45:20'),(13,4,2,'Верхний блок страниц','page.header','<div id=\"header\">\n    <div id=\"top_region_link\">Отличная работа в городе: <a href=\"javascript: void(0)\" class=\"select_region\">{strMyCity}</a></div>\n    <div id=\"login_box\"><if authed><a href=\"{cfgSiteUrl}profile\">Личный кабинет</a></if authed><ifelse authed><a href=\"javascript: void(0)\" class=\"auth_form_trigger\" rel=\"#overlay_box\">Войти</a></ifelse authed> / <if authed><a href=\"{cfgSiteUrl}profile?logout=1\">Выйти</a></if authed><ifelse authed><a href=\"{cfgSiteUrl}registration\">Создать личный кабинет</a></ifelse authed></div>\n    <div id=\"logo_box\">\n        <a id=\"logo\" href=\"{cfgSiteUrl}\"><img src=\"/storage/siteimg/logo.png\" alt=\"VVK24.ru\"/></a>\n        <div id=\"phone_box\">\n            <frg fragment.phone>\n        </div>\n    </div>\n\n    <div id=\"regions_box\">\n        <p>Выберите Ваш город:</p>\n        <ul>\n            <li><a href=\"#\" style=\"font-weight: bold\">Санкт-Петербург</a></li>\n            <li><a href=\"#\" style=\"font-weight: bold\">Москва</a></li>\n            <loop list.cities>\n                <li><a href=\"#\">{strCity}</a></li>\n            </loop list.cities>\n        </ul>\n    </div>\n</div>\n\n<div id=\"overlay_box\" class=\"overlay\">\n    <div class=\"contentWrap\">\n        <form action=\"/profile\" method=\"POST\">\n            <input type=\"hidden\" name=\"auth\" value=\"true\"/>\n            <h1 class=\"title\">Личный кабинет</h1>\n            <h4>Email</h4>\n            <input type=\"text\" id=\"strAuthLogin\" name=\"strAuthLogin\" class=\"plane\" maxlength=\"255\" placeholder=\"ваш-адрес@email.ru\"/>\n            <h4>Пароль</h4>\n            <input type=\"password\" id=\"strAuthPassword\" name=\"strAuthPassword\" class=\"plane\" maxlength=\"255\" />\n            <button>Войти</button>\n        </form>\n    </div>\n</div>\n\n<script type=\"text/javascript\">\n    jQuery(document).ready(function ($) {\n        var sizeW = 245; /* also see in jq.tools.css for #overlay rules */\n        var sizeH = 160;\n\n        // Overlay\n        function authOverlay() {\n            // вычисление размеров для всплывающего слоя\n            $(\'#overlay_box\').css(\'width\',sizeW);\n            // overlay: loading external pages - if the function argument is given to overlay, it is assumed to be the onBeforeLoad event listener\n            $(\".auth_form_trigger\").overlay({\n                top: ($(window).height() - sizeH)/2,\n                left: ($(window).width() - sizeW)/2 - 50,\n                mask: \'#939393\',\n                fixed: false,\n                closeOnClick: true\n            });\n        }\n\n        authOverlay();\n\n        /**\n         * Вызываем меню выбора региона\n         */\n        $(\'.select_region\').click(function () {\n            $(\'#regions_box\').fadeIn();\n        });\n\n        /**\n         * Выбор региона по клику\n         */\n        $(\'#regions_box a\').click(function () {\n            $(\'#regions_box\').hide();\n            $.post(\"/lib/api/region.php\", { city: $(this).text() }, function(data){\n                if (data.status == 0)\n                {\n                    $(\'.select_region\').text( data.city );\n                    var myHost = getMyHost();\n                    gotoUrl( myHost );\n                }\n            }, \"json\");\n        });\n    });\n</script>\n\n<frg top.navigation>','','Y','2014-03-23 15:27:55'),(14,4,2,'Нижний блок страниц','page.footer','<div id=\"footer\">\r\n    <ul class=\"inline clearer\">\r\n        <li>\r\n            <ul>\r\n                <li><a href=\"{cfgSiteUrl}about\">О портале</a></li>\r\n                <li><a href=\"#uc\" class=\"uc\">Новости</a></li>\r\n                <li><a href=\"#uc\" class=\"uc\">Правила работы на портале</a></li>\r\n                <li><a href=\"#uc\" class=\"uc\">Реклама на портале</a></li>\r\n                <li><a href=\"#uc\" class=\"uc\">Контактная информация</a></li>\r\n                <li><a href=\"#uc\" class=\"uc\">Стандартный договор</a></li>\r\n            </ul>\r\n        </li>\r\n        <li>\r\n            <ul>\r\n                <li><a href=\"{cfgSiteUrl}employers\">Работодателям</a></li>\r\n                <li><a href=\"#uc\" class=\"uc\">Условия размещения вакансий</a></li>\r\n                <li><a href=\"#uc\" class=\"uc\">Советы для работодателей</a></li>\r\n                <li><a href=\"#uc\" class=\"uc\">Прайс-лист</a></li>\r\n                <li><a href=\"#uc\" class=\"uc\">Выписать счёт</a></li>\r\n            </ul>\r\n        </li>\r\n        <li>\r\n            <ul>\r\n                <li><a href=\"{cfgSiteUrl}applicants\">Соискателям</a></li>\r\n                <li><a href=\"#uc\" class=\"uc\">Условия размещения резюме</a></li>\r\n                <li><a href=\"#uc\" class=\"uc\">Советы для соискателей</a></li>\r\n            </ul>\r\n        </li>\r\n    </ul>\r\n\r\n    <p id=\"copyright_notes\">\r\n        Внимание! При обращении в организацию, просьба ссылаться на сайт газеты \"Все вакансии\". При перепечатывании материалов, активная ссылка на сайт обязательна. &copy; Все права защищены. <br>\r\n        Новая версия сайта находится в стадии бета-тестирования. Возможны проблемы в работе некоторых страниц. <br>\r\n        Если Вы обнаружили ошибку или неточность, просим написать об этом в службу поддержки. <br>\r\n        <a href=\"#uc\" class=\"uc\">Закон о СМИ</a>.\r\n    </p>\r\n</div>','','Y','2013-11-23 17:41:54'),(15,4,2,'Блок нижний неведимка','footer.scripts','<div class=\"under-construction\" id=\"uc\">\r\n    <img src=\"{cfgSiteImg}content/under-construction.gif\" alt=\"Under Construction\"/>\r\n    <h3>Раздел в стадии разработки!</h3> Приносим свои извинения за временные неудобства.\r\n</div>','','Y','2013-11-19 13:45:27'),(16,4,2,'Верхняя навигация','top.navigation','<div id=\"navigation\" class=\"clearer\">\r\n    <div class=\"fl\">\r\n        <ul class=\"inline\">\r\n            <li><a href=\"{cfgSiteUrl}\">Главная</a></li>\r\n            <li><a href=\"{cfgSiteUrl}about\">О портале</a></li>\r\n            <li><a href=\"{cfgSiteUrl}employers\">Работодателям</a></li>\r\n            <li><a href=\"{cfgSiteUrl}applicants\">Соискателям</a></li>\r\n        </ul>\r\n    </div>\r\n    <div class=\"fr\">\r\n        <ul class=\"inline\">\r\n            <li><a href=\"#uc\" class=\"uc\">Контакты</a></li>\r\n            <li><a href=\"#uc\" class=\"uc\">Стать партнёром</a></li>\r\n        </ul>\r\n    </div>\r\n</div>','','Y','2013-11-19 13:45:35'),(18,1,1,'Правое меню профайла','fragment.sidebar.profile.menu','Правое меню профайла','Только для авторизованных пользователей','Y','2014-03-16 21:09:42'),(19,3,2,'Телефон в шапке сайта','fragment.phone','<span class=\"small\">(812)</span>\n<span class=\"big\">702-50-55</span>','','Y','2014-03-23 15:26:52');
/*!40000 ALTER TABLE `site_fragments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_fragments_group`
--

DROP TABLE IF EXISTS `site_fragments_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_fragments_group` (
  `sfg_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sfg_id_project` int(10) unsigned NOT NULL DEFAULT '0',
  `sfg_parent` int(10) unsigned DEFAULT NULL,
  `sfg_name` varchar(255) NOT NULL DEFAULT '',
  `sfg_status` enum('Y','N') NOT NULL DEFAULT 'Y',
  UNIQUE KEY `sfg_id` (`sfg_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Группы фрагментов';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_fragments_group`
--

LOCK TABLES `site_fragments_group` WRITE;
/*!40000 ALTER TABLE `site_fragments_group` DISABLE KEYS */;
INSERT INTO `site_fragments_group` VALUES (1,1,NULL,'Тематические фрагменты','Y'),(2,1,NULL,'Конструктивные фрагменты','Y'),(3,2,NULL,'Тематические фрагменты','Y'),(4,2,NULL,'Конструктивные','Y');
/*!40000 ALTER TABLE `site_fragments_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_geoip`
--

DROP TABLE IF EXISTS `site_geoip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_geoip` (
  `sg_id` int(11) NOT NULL AUTO_INCREMENT,
  `sg_ip` varchar(32) NOT NULL COMMENT 'IP адрес посетителя',
  `sg_city` varchar(64) NOT NULL COMMENT 'город посетителя',
  `sg_country` varchar(32) NOT NULL COMMENT 'страна посетителя',
  `sg_date_add` int(11) DEFAULT NULL COMMENT 'дата записи',
  PRIMARY KEY (`sg_id`),
  UNIQUE KEY `sg_ip` (`sg_ip`,`sg_city`,`sg_country`)
) ENGINE=InnoDB AUTO_INCREMENT=12988 DEFAULT CHARSET=utf8 COMMENT='Гео база городов';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_geoip`
--

LOCK TABLES `site_geoip` WRITE;
/*!40000 ALTER TABLE `site_geoip` DISABLE KEYS */;
INSERT INTO `site_geoip` VALUES (1,'66.249.78.29','Beverly Hills','United States',1381064892),(2,'65.55.52.114','','United States',1381064924),(3,'66.249.78.209','Beverly Hills','United States',1381064983),(4,'144.76.166.251','Kiez','Germany',1381065059),(5,'95.108.129.207','Moscow','Russian Federation',1381065130),(6,'213.180.206.197','Moscow','Russian Federation',1381065141);
/*!40000 ALTER TABLE `site_geoip` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_images`
--

DROP TABLE IF EXISTS `site_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_images` (
  `si_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `si_rel_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Ид-р записи отношения',
  `si_type` enum('site','resume','company','banner','news','users','events') NOT NULL DEFAULT 'site' COMMENT 'тип отношения фотографии к rel_id',
  `si_banner_type` enum('top','left','right') DEFAULT NULL COMMENT 'Размещение баннера',
  `si_banner_content` text NOT NULL COMMENT 'Контент FLASH баннера',
  `si_filename` varchar(255) NOT NULL DEFAULT '',
  `si_url` varchar(255) DEFAULT NULL COMMENT 'Url of image to ...',
  `si_title` varchar(255) DEFAULT NULL COMMENT 'наименование изображения',
  `si_desc` varchar(255) DEFAULT NULL,
  `si_date_add` datetime DEFAULT NULL,
  `si_date_change` datetime DEFAULT NULL,
  `si_status` enum('N','Y') NOT NULL DEFAULT 'N',
  `si_project` int(10) unsigned DEFAULT NULL COMMENT 'проект',
  PRIMARY KEY (`si_id`),
  KEY `si_id_rel` (`si_rel_id`),
  KEY `si_project` (`si_project`)
) ENGINE=MyISAM AUTO_INCREMENT=147 DEFAULT CHARSET=utf8 COMMENT='Images of Web';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_images`
--

LOCK TABLES `site_images` WRITE;
/*!40000 ALTER TABLE `site_images` DISABLE KEYS */;
INSERT INTO `site_images` VALUES (1,0,'banner','top','','mcdonalds.jpg','www.rabotavmcdonalds.ru',NULL,'McDonalds','2013-07-28 23:49:45','2014-01-14 07:02:41','Y',1),(50,722,'company',NULL,'','logo.722.jpg',NULL,'ООО Мега',NULL,'2013-09-16 06:28:59',NULL,'Y',1),(5,4,'events',NULL,'','events.photo.4.b.jpg',NULL,'12 СЕНТЯБРЯ 2013г. ЯРМАРКА ВАКАНСИЙ рабочих и учебных мест',NULL,'2013-08-19 15:41:41',NULL,'Y',1);
/*!40000 ALTER TABLE `site_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_informers`
--

DROP TABLE IF EXISTS `site_informers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_informers` (
  `si_date` date NOT NULL,
  `si_type` enum('w_min','w_max','eur','usd') DEFAULT NULL COMMENT 'informer type',
  `si_value` float(8,2) DEFAULT NULL,
  UNIQUE KEY `si_date` (`si_date`,`si_type`),
  KEY `si_type` (`si_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Informers (currency, weather, etc)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_informers`
--

LOCK TABLES `site_informers` WRITE;
/*!40000 ALTER TABLE `site_informers` DISABLE KEYS */;
INSERT INTO `site_informers` VALUES ('2014-01-03','w_min',0.00),('2014-01-03','w_max',-2.00),('2014-01-01','eur',45.06),('2014-01-01','usd',32.66),('2014-01-04','w_min',0.00),('2014-01-04','w_max',2.00),('2014-01-05','w_max',1.00),('2014-01-05','w_min',-1.00),('2014-01-06','w_max',2.00),('2014-01-06','w_min',0.00),('2014-01-07','w_max',4.00),('2014-01-07','w_min',2.00),('2014-01-08','w_max',3.00);
/*!40000 ALTER TABLE `site_informers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_news`
--

DROP TABLE IF EXISTS `site_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_news` (
  `sn_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sn_title` varchar(255) NOT NULL DEFAULT '',
  `sn_body` text NOT NULL,
  `sn_url` varchar(255) DEFAULT NULL COMMENT 'URL of article',
  `sn_date_publ` date DEFAULT NULL,
  `sn_date_add` datetime DEFAULT NULL,
  `sn_date_change` datetime DEFAULT NULL,
  `sn_status` enum('N','Y') NOT NULL DEFAULT 'N',
  `sn_project` int(10) unsigned DEFAULT NULL COMMENT 'проект',
  PRIMARY KEY (`sn_id`),
  KEY `sn_date_change` (`sn_date_change`),
  KEY `sn_project` (`sn_project`)
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COMMENT='News';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_news`
--

LOCK TABLES `site_news` WRITE;
/*!40000 ALTER TABLE `site_news` DISABLE KEYS */;
INSERT INTO `site_news` VALUES (1,'Как встречать Новый 2014 год (год Лошади)','<p>\r\n	<span style=\"color: rgb(116, 99, 116); font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 18px; text-align: center; background-color: rgb(223, 234, 255);\">2014 год &mdash; год синей деревянной лошади &mdash; желательно встречать с друзьями. Хорошо если это будет просторное помещение, а не &quot;маленькое гнездышко&quot;, ведь в год лошади без простора действий никуда.</span><br style=\"color: rgb(116, 99, 116); font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 18px; text-align: center; background-color: rgb(223, 234, 255);\" />\r\n	<span style=\"color: rgb(116, 99, 116); font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 18px; text-align: center; background-color: rgb(223, 234, 255);\">Лошадь&nbsp;&ndash; животное, довольно уважаемое европейцами и не только ими. Достаточно вспомнить сколько значил хороший конь (лошадь) для воина или для пахаря, чтобы понять насколько он&nbsp;важен. Даже в наши дни, породистых лошадей&nbsp;выставляют на выставках, они участвуют в забегах, да их красотой просто любуются. Настолько это универсальное и желаемое животное, несмотря на изменившиеся исторические реалии.</span><br style=\"color: rgb(116, 99, 116); font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 18px; text-align: center; background-color: rgb(223, 234, 255);\" />\r\n	<span style=\"color: rgb(116, 99, 116); font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 18px; text-align: center; background-color: rgb(223, 234, 255);\">&nbsp;Если говорить о встрече Нового года лошади более подробно, то прежде всего, как мы говорили для&nbsp;празднования года потребуется просторное помещение, лучше справлять его в кругу друзей.&nbsp;В последние минуты уходящего года проститесь мысленно с уходящим&nbsp;и настройтесь на новый лад вашей жизни, где теперь вам придется рассчитывать лишь на себя. Знайте, что ваши старания не пройдут даром, все воздастся по трудам, именно это должно стать для вас мотивацией в Новом году лошади. Когда часы известят о начале Нового года, с первых же секунд думайте о хорошем, настраивайтесь на лучшее, мысленно взывайте к добру и справедливости.&nbsp;</span><br style=\"color: rgb(116, 99, 116); font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 18px; text-align: center; background-color: rgb(223, 234, 255);\" />\r\n	<span style=\"color: rgb(116, 99, 116); font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 18px; text-align: center; background-color: rgb(223, 234, 255);\">&nbsp;&nbsp;Празднование Нового года&nbsp;Лошади должно сопровождаться интенсивной,&nbsp;зажигательной музыкой,&nbsp; наиболее подойдут музыкальные произведения из рок-н-ролла и диско.</span><br style=\"color: rgb(116, 99, 116); font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 18px; text-align: center; background-color: rgb(223, 234, 255);\" />\r\n	<span style=\"color: rgb(116, 99, 116); font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 18px; text-align: center; background-color: rgb(223, 234, 255);\">&nbsp;Лошадь привыкла быть в основном на глазах, но она как и любое животное боится &quot;упряжек&quot;. Именно поэтому встречайте Новый год в дружной компании, но не навязывайте своего мнения другим. Тем не менее помните, что попытки встретить Новый 2014 год совсем уж неординарно и развязано могут привести к плачевным последствиям.</span></p>','kak_vstrechat_god_loshadi','2013-10-30','2006-11-25 16:02:11','2013-10-29 14:00:25','Y',1);
/*!40000 ALTER TABLE `site_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_pages`
--

DROP TABLE IF EXISTS `site_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_pages` (
  `sp_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sp_id_crumb` int(11) NOT NULL COMMENT 'for breadcrumbs',
  `sp_id_group` int(10) unsigned DEFAULT NULL COMMENT 'group of pages',
  `sp_name` varchar(255) NOT NULL DEFAULT '',
  `sp_alias` varchar(50) DEFAULT NULL,
  `sp_body` mediumtext NOT NULL,
  `sp_id_template` int(10) unsigned NOT NULL DEFAULT '0',
  `sp_status` enum('N','Y') NOT NULL DEFAULT 'N',
  `sp_publish` datetime DEFAULT NULL,
  `sp_id_project` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`sp_id`),
  KEY `sp_id_project` (`sp_id_project`),
  KEY `sp_id_crumb` (`sp_id_crumb`)
) ENGINE=MyISAM AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Страницы сайта';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_pages`
--

LOCK TABLES `site_pages` WRITE;
/*!40000 ALTER TABLE `site_pages` DISABLE KEYS */;
INSERT INTO `site_pages` VALUES (1,0,5,'Главная страница','index','<div class=\"wrapper_block\">\n    <div class=\"fl column first\">\n        <h1>НОВЫЕ ВАКАНСИИ</h1>\n\n        <loop list.top.vacancies>\n            <div class=\"row\">\n                <a href=\"{cfgSiteUrl}vacancy/{jv_id}\">{jv_title}</a>\n                <p class=\"salary\">{strSalary}</p>\n            </div>\n        </loop list.top.vacancies>\n    </div>\n    <div class=\"fl column last\">\n        <h1>НОВЫЕ РЕЗЮМЕ</h1>\n\n        <loop list.top.resumes>\n            <div class=\"row\">\n                <a href=\"{cfgSiteUrl}resume/{jr_id}\">{jr_title}</a>\n                <p class=\"salary\">{jr_salary} руб.</p>\n            </div>\n        </loop list.top.resumes>\n    </div>\n    <div class=\"clearer\"></div>\n</div>',2,'Y','2014-02-24 12:03:13',1),(2,1,2,'Страница \"Ошибка\"','error','<p>\r\n	<font color=\"red\" size=\"4\">Error 404<br />\r\n	Page not found </font></p>',3,'Y','2013-08-15 23:39:54',1);
/*!40000 ALTER TABLE `site_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_pages_group`
--

DROP TABLE IF EXISTS `site_pages_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_pages_group` (
  `spg_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `spg_id_project` int(10) unsigned NOT NULL DEFAULT '0',
  `spg_id_parent` int(10) unsigned DEFAULT NULL,
  `spg_name` varchar(255) NOT NULL DEFAULT '',
  `spg_status` enum('N','Y') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`spg_id`),
  KEY `spg_id_project` (`spg_id_project`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Группы страниц';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_pages_group`
--

LOCK TABLES `site_pages_group` WRITE;
/*!40000 ALTER TABLE `site_pages_group` DISABLE KEYS */;
INSERT INTO `site_pages_group` VALUES (1,1,NULL,'Основная группа статических страниц','Y'),(2,1,NULL,'Служебные страницы','Y'),(3,2,NULL,'Тематические страницы сайта','Y'),(4,2,NULL,'Технологические страницы','Y'),(5,1,NULL,'Динамические страницы','Y');
/*!40000 ALTER TABLE `site_pages_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_pages_history`
--

DROP TABLE IF EXISTS `site_pages_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_pages_history` (
  `sph_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sph_page_id` int(11) NOT NULL,
  `sph_user_id` int(10) unsigned NOT NULL COMMENT 'кто сохранил',
  `sph_body` text NOT NULL,
  `sph_time` int(10) unsigned NOT NULL,
  PRIMARY KEY (`sph_id`),
  KEY `sph_page_id` (`sph_page_id`),
  KEY `sph_user_id` (`sph_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='История контента страниц';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_pages_history`
--

LOCK TABLES `site_pages_history` WRITE;
/*!40000 ALTER TABLE `site_pages_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `site_pages_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_pages_meta`
--

DROP TABLE IF EXISTS `site_pages_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_pages_meta` (
  `spm_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `spm_id_rec` int(10) unsigned NOT NULL DEFAULT '0',
  `spm_subpage_uid` varchar(50) DEFAULT NULL,
  `spm_title` text NOT NULL,
  `spm_keywords` text NOT NULL,
  `spm_description` text NOT NULL,
  PRIMARY KEY (`spm_id`)
) ENGINE=MyISAM AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Мета-теги к страницам сайта';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_pages_meta`
--

LOCK TABLES `site_pages_meta` WRITE;
/*!40000 ALTER TABLE `site_pages_meta` DISABLE KEYS */;
INSERT INTO `site_pages_meta` VALUES (1,1,NULL,'Работа в Санкт-Петербурге, поиск работы, вакансии, вакансии СПб, поиск вакансий, работа в СПб','работа, вакансии, соискатели, резюме, сотрудник, должность, работа в спб, работа в петербурге, работа в питере, работа вакансии, работа в санкт петербурге, режим работы, работа 2013 РАБОТА В СПБ И ЛЕН ОБЛАСТИ, А ТАК ЖЕ РАБОТА В РЕГИОНАХ. У НАС ВЫ МОЖЕТЕ РАЗМЕСТИТЬ РЕЗЮМЕ БЕСПЛАТНО ИЛИ РАЗМЕСТИТЬ ВАКАНСИЮ БЕСПЛАТНО. РАБОТА В САНКТ-ПЕТЕРБУРГЕ И ЛЕНЕНГРАДСКОЙ ОБЛАСТИ ИЗ ОГРОМНОЙ БАЗЫ ДАННЫХ ВАКАНСИЙ. ЗДЕСЬ ВЫ НАЙДЕТЕ ТОЛЬКО САМЫЕ АКТУАЛЬНЫЕ ВАКАНСИИ И САМЫЕ АКТУАЛЬНЫЕ РЕЗЮМЕ,  ОТ ПРЯМЫХ РАБОТОДАТЕЛЕЙ. УДОБНЫЙ КАТАЛОГ ВАКАНСИЙ ПОМОЖЕТ БЫСТРО НАЙТИ РАБОТУ, НАЙТИ СОТРУДНИКА. ТУТ СОБРАНО КОЛОССАЛЬНОЕ КОЛИЧЕСТВО СВЕЖИХ ВАКАНСИЙ ДЛЯ СОИСКАТЕЛЕЙ ЛЮБОЙ ПРОФЕССИИ. НА САЙТЕ RABOTA-YA.RU  ВЫ С ЛЕГКОСТЬЮ НАЙДЕТЕ РАБОТУ В САНКТ ПЕТЕРБУРГЕ, ДА И ВАКАНСИИ ДРУГИХ ГОРОДОВ ТАКЖЕ ПРЕДСТАВЛЕНЫ НА НАШЕМ ПОРТАЛЕ. ПОЛЬЗУЙТЕСЬ НАШЕЙ БАЗОЙ ДАННЫХ ДЛЯ ПОИСКА РАБОТЫ, РАЗМЕЩАЙТЕ РЕЗЮМЕ, ИЩИТЕ СОТРУДНИКОВ.','РАБОТА В СПБ И ЛЕН ОБЛАСТИ, А ТАК ЖЕ РАБОТА В РЕГИОНАХ. У НАС ВЫ МОЖЕТЕ РАЗМЕСТИТЬ РЕЗЮМЕ БЕСПЛАТНО ИЛИ РАЗМЕСТИТЬ ВАКАНСИЮ БЕСПЛАТНО. РАБОТА В САНКТ-ПЕТЕРБУРГЕ И ЛЕНЕНГРАДСКОЙ ОБЛАСТИ ИЗ ОГРОМНОЙ БАЗЫ ДАННЫХ ВАКАНСИЙ. ЗДЕСЬ ВЫ НАЙДЕТЕ ТОЛЬКО САМЫЕ АКТУАЛЬНЫЕ ВАКАНСИИ И САМЫЕ АКТУАЛЬНЫЕ РЕЗЮМЕ,  ОТ ПРЯМЫХ РАБОТОДАТЕЛЕЙ. УДОБНЫЙ КАТАЛОГ ВАКАНСИЙ ПОМОЖЕТ БЫСТРО НАЙТИ РАБОТУ, НАЙТИ СОТРУДНИКА. ТУТ СОБРАНО КОЛОССАЛЬНОЕ КОЛИЧЕСТВО СВЕЖИХ ВАКАНСИЙ ДЛЯ СОИСКАТЕЛЕЙ ЛЮБОЙ ПРОФЕССИИ. НА САЙТЕ RABOTA-YA.RU  ВЫ С ЛЕГКОСТЬЮ НАЙДЕТЕ РАБОТУ В САНКТ ПЕТЕРБУРГЕ, ДА И ВАКАНСИИ ДРУГИХ ГОРОДОВ ТАКЖЕ ПРЕДСТАВЛЕНЫ НА НАШЕМ ПОРТАЛЕ. ПОЛЬЗУЙТЕСЬ НАШЕЙ БАЗОЙ ДАННЫХ ДЛЯ ПОИСКА РАБОТЫ, РАЗМЕЩАЙТЕ РЕЗЮМЕ, ИЩИТЕ СОТРУДНИКОВ. работа, вакансии, соискатели, резюме, сотрудник, должность, работа в спб, работа в петербурге, работа в питере, работа вакансии, работа в санкт петербурге, режим работы, работа 2013');
/*!40000 ALTER TABLE `site_pages_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_rubrics`
--

DROP TABLE IF EXISTS `site_rubrics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_rubrics` (
  `sr_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sr_name` varchar(64) DEFAULT NULL,
  `sr_alias` varchar(32) DEFAULT NULL,
  `sr_type` enum('site','enter') NOT NULL DEFAULT 'site',
  `sr_menu` enum('N','Y') NOT NULL DEFAULT 'N',
  `sr_status` enum('N','Y') NOT NULL DEFAULT 'N',
  `sr_date_add` datetime DEFAULT NULL,
  PRIMARY KEY (`sr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Рубрики сайта';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_rubrics`
--

LOCK TABLES `site_rubrics` WRITE;
/*!40000 ALTER TABLE `site_rubrics` DISABLE KEYS */;
/*!40000 ALTER TABLE `site_rubrics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_seo_texts`
--

DROP TABLE IF EXISTS `site_seo_texts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_seo_texts` (
  `sst_id` int(11) NOT NULL AUTO_INCREMENT,
  `sst_word` varchar(255) NOT NULL,
  `sst_text` text NOT NULL,
  `sst_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`sst_id`),
  UNIQUE KEY `sst_word` (`sst_word`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COMMENT='Search Texts for pages';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_seo_texts`
--

LOCK TABLES `site_seo_texts` WRITE;
/*!40000 ALTER TABLE `site_seo_texts` DISABLE KEYS */;
INSERT INTO `site_seo_texts` VALUES (1,'Автослесарь','Автослесарь – человек, осуществляющий проверку технического состояния транспортного средства, определяющий необходимость их ремонта и техобслуживания. \nРабота автослесаря заключается в осмотре транспортных средств и починке или замене поврежденных деталей. Поэтому автослесарь должен хорошо разбираться в чертежах и схемах, устройстве транспортных средств, порядке заказа деталей и, конечно, должен сам уметь водить автомобиль. Помимо этого, требуется знание различных видов топлива и смазочного материала.\nЧеловек, претендующий на место автослесаря должен обладать определенными психофизическими характеристиками, так как работа по этой специальности сопряжена со значительным физическими усилиями и необходимостью постоянной концентрации внимания. \nВ соответствии с нормативами, недельное время работы автослесаря не должно превышать сорока двух часов. \nДля поступления на должность автослесаря необходимо иметь диплом о среднем профессиональном образовании.','2014-04-07 09:43:37'),(2,'Агент по недвижимости','Агент по недвижимости – человек, осуществляющий покупку, продажу или аренду недвижимости от имени и по поручению клиента. \nАгент выполняет все необходимые операции, связанные с куплей-продажей или арендой недвижимого имущества. Он подробно изучает рынок недвижимости: анализирует спрос и предложение, регистрирует поступающую информацию о помещениях, предназначенных для купли-продажи или аренды, осуществляет поиск потенциальных покупателей, продавцов и арендаторов. Агент по недвижимости обрабатывает заказы клиентов, подбирает для них варианты. Он также организует встречи, предварительный осмотр помещений и консультирует по всем возникающим вопросам, связанных с недвижимым имуществом. Агент осуществляет большое количество «бумажной» работы: помогает в сборе всей необходимой для сделки документации, согласовывает и составляет договор купли-продажи или аренды, составляет отчетность по заключенным сделкам. Агент по недвижимости должен знать все нормативные акты, положения, инструкции, руководящие материалы и другие документы, регулирующие операции с недвижимостью. \nДля работы в должности агента по недвижимости необходимо среднее профессиональное образование и специальная подготовка по установленной программе, требования к опыту работы не предъявляются.','2014-04-07 09:47:14');
/*!40000 ALTER TABLE `site_seo_texts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_templates`
--

DROP TABLE IF EXISTS `site_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_templates` (
  `st_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `st_group` int(10) unsigned DEFAULT NULL,
  `st_project` int(10) unsigned NOT NULL DEFAULT '0',
  `st_name` varchar(50) NOT NULL DEFAULT '',
  `st_body` text NOT NULL,
  `st_status` enum('N','Y') NOT NULL DEFAULT 'N',
  `st_publish` datetime DEFAULT NULL,
  UNIQUE KEY `t_id` (`st_id`),
  KEY `st_project` (`st_project`),
  KEY `st_group` (`st_group`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Шаблоны страниц для сайтов';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_templates`
--

LOCK TABLES `site_templates` WRITE;
/*!40000 ALTER TABLE `site_templates` DISABLE KEYS */;
INSERT INTO `site_templates` VALUES (1,1,1,'Пагинатор страниц CMS','<!-- START: Информация о показанных записях, кол-ве записей, постраничный вывод -->\r\n<table cellpadding=\"1\" cellspacing=\"1\" width=\"90%\">\r\n    <tr>\r\n        <td><span>Всего записей выбрано: <b>[intQuantSelectRecords]</b> | Показано записей: <b>[intQuantShowRecOnPage]</b></span></td>\r\n        <td align=\"right\">\r\n            <table cellpadding=\"0\" cellspacing=\"0\">\r\n                <tr>\r\n                    <td><span>Страницы:</span></td>\r\n[if pa.prev.arrow.pages]\r\n                    <td><a href=\"[strLinkBack]\"><img src=\"[cfgAdminImg][strNameImageBack]\" border=\"0\" alt=\"Назад\" hspace=\"2\" vspace=\"3\"></a></td>\r\n[/if pa.prev.arrow.pages]\r\n[loop pa.lst.pages]\r\n                    <td width=\"12\" align=\"center\"> [strLink] </td>\r\n[/loop pa.lst.pages]\r\n[if pa.next.arrow.pages]\r\n                    <td align=\"center\"><a href=\"[strLinkForward]\"><img src=\"[cfgAdminImg][strNameImageForward]\" border=\"0\" alt=\"Далее\" hspace=\"2\" vspace=\"3\"></a></td>\r\n[/if pa.next.arrow.pages]\r\n                </tr>\r\n            </table>\r\n        </td>\r\n    </tr>\r\n</table>\r\n<!-- END: Информация о показанных записях, кол-ве записей, постраничный вывод -->','Y','2013-08-15 23:39:31');
/*!40000 ALTER TABLE `site_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_templates_email`
--

DROP TABLE IF EXISTS `site_templates_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_templates_email` (
  `ste_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ste_title` varchar(255) NOT NULL COMMENT 'Тема письма',
  `ste_body` text NOT NULL COMMENT 'Тект письма',
  `ste_html` text NOT NULL COMMENT 'HTML письма',
  `ste_date_create` int(10) unsigned NOT NULL COMMENT 'дата создания',
  `ste_date_update` int(10) unsigned NOT NULL COMMENT 'дата обновления',
  `ste_status` enum('off','on') NOT NULL DEFAULT 'off' COMMENT 'статус рассылки',
  PRIMARY KEY (`ste_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='Шаблон письма рассылки';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_templates_email`
--

LOCK TABLES `site_templates_email` WRITE;
/*!40000 ALTER TABLE `site_templates_email` DISABLE KEYS */;
INSERT INTO `site_templates_email` VALUES (1,'Тестовое письмо','Здравствуйте %SUBSCRIBER%\r\n\r\nМы рады сообщить что спамер теперь работает!!!\r\n\r\nРаботА-Я','<strong>Здравствуйте %SUBSCRIBER%</strong> <br /><br />Мы рады сообщить что спамер теперь работает!!!<h4><span style=\"color: rgb(255, 221, 0);\">Ра</span><span style=\"color: rgb(248, 156, 29);\">бо</span><span style=\"color: rgb(215, 26, 32);\">тА</span><span style=\"color: rgb(138, 2, 2);\">-Я</span></h4>',1381415752,1381753294,'off');
/*!40000 ALTER TABLE `site_templates_email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_templates_group`
--

DROP TABLE IF EXISTS `site_templates_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_templates_group` (
  `stg_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `stg_id_project` int(10) unsigned NOT NULL DEFAULT '0',
  `stg_id_parent` int(10) unsigned DEFAULT NULL,
  `stg_name` varchar(255) NOT NULL DEFAULT '',
  `stg_status` enum('Y','N') NOT NULL DEFAULT 'Y',
  UNIQUE KEY `stg_id` (`stg_id`),
  KEY `stg_parent_id` (`stg_id_parent`),
  KEY `stg_id_project` (`stg_id_project`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Группы шаблонов сайтов';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_templates_group`
--

LOCK TABLES `site_templates_group` WRITE;
/*!40000 ALTER TABLE `site_templates_group` DISABLE KEYS */;
INSERT INTO `site_templates_group` VALUES (1,1,NULL,'Базовая группа шаблонов','Y'),(2,2,NULL,'Шаблоны страниц','Y'),(3,2,NULL,'Шаблоны дополнений','Y');
/*!40000 ALTER TABLE `site_templates_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_training_rubric_lnk`
--

DROP TABLE IF EXISTS `site_training_rubric_lnk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_training_rubric_lnk` (
  `strl_str_id` int(10) unsigned NOT NULL COMMENT 'рубрика',
  `strl_st_id` int(10) unsigned NOT NULL COMMENT 'тренинг',
  UNIQUE KEY `strl_uniq` (`strl_str_id`,`strl_st_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Отношение тренингов к рубрикам';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_training_rubric_lnk`
--

LOCK TABLES `site_training_rubric_lnk` WRITE;
/*!40000 ALTER TABLE `site_training_rubric_lnk` DISABLE KEYS */;
INSERT INTO `site_training_rubric_lnk` VALUES (1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(1,7),(1,8),(1,9),(1,10),(1,11),(1,12),(1,13),(1,14),(1,15),(1,16),(1,17),(1,18),(2,19),(2,20),(2,21),(2,22),(2,23),(2,24),(2,25),(3,26),(3,27),(3,28),(3,29),(3,30),(3,31),(3,32),(3,33),(3,34),(3,35),(3,36),(3,37),(3,40),(3,41),(3,42),(3,43),(4,48),(4,49),(4,50),(4,51),(4,52),(4,53),(4,54),(4,55),(4,56),(4,57),(4,58),(4,59),(5,64),(6,76),(6,82),(6,83),(6,84),(7,88),(8,101),(8,103),(8,107),(8,109),(8,112),(8,114),(8,115),(9,121),(9,122),(9,124),(10,132),(10,135),(11,147),(11,150),(11,151),(11,153),(11,154),(12,159),(12,164),(13,177),(13,182),(14,183),(14,186),(14,190),(14,191),(14,192),(14,193),(14,196),(14,198),(14,199),(14,200),(15,201),(15,202),(15,203),(15,204),(15,205),(15,206),(15,207),(15,208),(15,209),(15,210),(15,211),(15,212),(15,213),(15,214),(15,215),(15,216),(15,217),(15,218),(16,227),(16,228),(16,229),(16,230),(16,231),(16,232),(16,233),(16,234),(16,235),(16,236),(17,242),(18,243),(18,244),(18,245),(18,246),(18,247),(18,248),(18,249),(18,251),(18,252),(18,253),(18,254),(18,255),(18,256),(18,257),(18,258),(18,259),(18,260),(19,270),(19,271),(19,272),(19,273),(19,274),(19,275),(20,280),(21,283),(21,284),(21,285),(21,286),(22,287),(22,288),(22,289),(22,290),(22,291),(22,292),(22,293),(22,294),(22,295),(22,296),(22,297),(22,298),(22,299),(22,301),(22,303),(22,304),(23,319),(23,320),(23,321),(23,322),(24,324),(25,325),(25,327),(25,328),(26,330),(27,333),(27,335),(28,337),(29,347),(29,348),(29,349),(29,350),(29,352),(29,353),(29,354),(30,363),(30,366),(30,370),(30,372),(31,375),(31,378),(31,380),(31,381),(31,382),(31,384),(31,385),(33,402),(33,403),(34,408),(37,418),(37,420),(37,421),(37,422),(37,423),(37,424),(37,425),(39,441),(39,442),(39,443),(39,444),(40,465),(40,467),(42,472),(44,485),(44,490),(44,492),(45,499),(45,500),(45,502),(45,503),(45,507),(45,508),(45,509),(45,510),(45,511),(45,513),(45,514),(45,515),(46,519),(46,520),(47,521),(47,522),(47,523),(47,524),(47,525),(47,526),(47,527),(47,528),(47,529),(47,530),(48,533),(48,535),(48,536),(48,537),(48,539),(49,543),(49,544),(49,545),(49,546),(49,547),(49,548),(50,561),(51,562),(52,563),(52,564),(52,565),(52,566),(52,567),(52,568),(52,569),(52,570),(52,571),(52,572),(52,573),(52,574),(52,575),(52,576),(52,577),(52,578),(52,579),(52,580),(53,582),(54,588),(54,589),(54,590),(54,591),(54,592),(54,593),(54,594),(54,595),(54,596),(54,597),(54,598),(54,599),(55,602),(55,604),(55,605),(55,606),(55,607),(55,608),(55,609),(55,610),(55,611),(55,612),(55,613),(55,614),(55,615),(55,616),(55,617),(56,620),(56,621),(56,622),(56,623),(56,624),(56,625),(56,626),(56,627),(56,628),(56,629),(56,630),(56,631),(56,632),(56,633),(56,634),(56,635),(57,641),(57,647),(57,648),(57,649),(57,650),(58,658),(59,662),(59,663),(59,665),(59,666),(59,667),(59,669),(59,670),(59,671),(59,672),(59,673),(59,674),(59,675),(60,682),(61,686),(61,687),(61,688),(61,689),(61,690),(61,691),(61,692),(61,693),(62,695),(62,700),(62,701),(64,703),(64,704),(65,705),(65,707),(65,708),(65,709),(65,710),(65,711),(65,712),(65,713),(65,714),(65,715),(65,716),(65,717),(65,718),(65,719),(65,720),(65,721),(66,724),(66,725),(66,726),(67,732),(67,733),(67,734),(67,735),(67,737),(67,738),(67,739),(67,740),(67,741),(67,742),(67,743),(67,744),(67,745),(68,755),(68,757),(68,758),(68,759),(68,760),(68,761),(69,764),(70,765),(70,766),(70,767),(70,768),(70,769),(70,770),(70,771),(70,772),(70,773),(70,774),(70,775),(70,776),(70,777),(70,778),(70,779),(70,780),(70,781),(70,782),(71,783),(71,784),(71,785),(71,786),(71,788),(71,790),(71,792),(71,793),(71,794),(71,795),(72,796),(72,798),(72,799),(72,800),(72,802),(72,803),(72,806),(72,807),(72,810),(72,811),(72,812),(73,814),(73,815),(73,816),(73,818),(73,820),(73,821),(73,822),(74,825);
/*!40000 ALTER TABLE `site_training_rubric_lnk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_training_rubrics`
--

DROP TABLE IF EXISTS `site_training_rubrics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_training_rubrics` (
  `str_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `str_parent_id` int(10) unsigned DEFAULT NULL,
  `str_rubricname` varchar(255) NOT NULL,
  PRIMARY KEY (`str_id`),
  UNIQUE KEY `str_parent_id_2` (`str_parent_id`,`str_rubricname`),
  KEY `str_parent_id` (`str_parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8 COMMENT='Рубрики тренингов';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_training_rubrics`
--

LOCK TABLES `site_training_rubrics` WRITE;
/*!40000 ALTER TABLE `site_training_rubrics` DISABLE KEYS */;
INSERT INTO `site_training_rubrics` VALUES (1,NULL,'Бизнес тренинги'),(2,NULL,'Детские курсы и занятия'),(3,NULL,'Компьютерные курсы'),(14,NULL,'Курсы 1С, курсы бухгалтеров'),(15,NULL,'Курсы авто и мото вождения'),(18,NULL,'Курсы актерского мастерства'),(21,NULL,'Курсы видеомонтажа'),(22,NULL,'Курсы иностранных языков'),(45,NULL,'Курсы менеджеров'),(52,NULL,'Курсы по уходу за телом'),(62,NULL,'Курсы по финансовым специальностям'),(64,NULL,'Курсы поваров'),(65,NULL,'Курсы самообороны, школы боевых искусств'),(69,NULL,'Курсы флористики'),(70,NULL,'Новости'),(71,NULL,'Образование за рубежом'),(72,NULL,'Разные курсы'),(73,NULL,'Фотошколы и курсы фотографии'),(74,NULL,'Шоу бизнес'),(4,3,'Курсы 3D MAX, 3d графики и моделирования'),(5,3,'Курсы Adobe Flash'),(6,3,'Курсы Adobe Photoshop'),(7,3,'Курсы AutoCad, ArchiCad'),(8,3,'Курсы MicroSoft Office'),(9,3,'Курсы SEO (поисковая оптимизация сайтов)'),(10,3,'Курсы веб-дизайна (web design)'),(11,3,'Курсы дизайна'),(12,3,'Курсы программирования'),(13,3,'Основы работы на ПК'),(16,15,'Автошколы и курсы вождения автомобиля'),(17,15,'Мотошколы и курсы вождения мотоцикла'),(19,18,'Курсы ораторского мастерства'),(20,18,'Курсы риторики'),(23,22,'Курсы английского языка'),(24,22,'Курсы арабского языка'),(25,22,'Курсы голландского языка'),(26,22,'Курсы греческого языка'),(27,22,'Курсы датского языка'),(28,22,'Курсы иврита'),(29,22,'Курсы испанского языка'),(30,22,'Курсы итальянского языка'),(31,22,'Курсы китайского языка'),(32,22,'Курсы корейского языка'),(33,22,'Курсы немецкого языка'),(34,22,'Курсы норвежского языка'),(35,22,'Курсы польского языка'),(36,22,'Курсы португальского языка'),(37,22,'Курсы русского языка'),(38,22,'Курсы турецкого языка'),(39,22,'Курсы финского языка'),(40,22,'Курсы французского языка'),(41,22,'Курсы хинди'),(42,22,'Курсы чешского языка'),(43,22,'Курсы шведского языка'),(44,22,'Курсы японского языка'),(46,45,'Курсы логистики'),(47,45,'Курсы маркетинга'),(48,45,'Курсы менеджеров по продажам'),(49,45,'Курсы менеджеров по туризму'),(50,45,'Курсы менеджмента'),(51,45,'Курсы офис-менеджеров'),(53,52,'Занятия йогой'),(54,52,'Курсы визажа'),(55,52,'Курсы для беременных'),(56,52,'Курсы косметологa'),(57,52,'Курсы макияжа'),(58,52,'курсы маникюра'),(59,52,'Курсы массажа'),(60,52,'курсы парикмахеров'),(61,52,'Фитнес клубы'),(63,62,'Курсы по недвижимости'),(66,65,'Секции тайского бокса'),(67,65,'Спортивные школы'),(68,65,'Школы боевых искусств');
/*!40000 ALTER TABLE `site_training_rubrics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_trainings`
--

DROP TABLE IF EXISTS `site_trainings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_trainings` (
  `str_id` int(11) NOT NULL AUTO_INCREMENT,
  `str_title` varchar(255) NOT NULL COMMENT 'назв.курса',
  `str_desc` text NOT NULL COMMENT 'описание',
  `str_address` varchar(255) DEFAULT NULL COMMENT 'адрес курсов',
  `str_phone` varchar(255) DEFAULT NULL COMMENT 'контакты',
  `str_email` varchar(255) DEFAULT NULL,
  `str_web` varchar(255) DEFAULT NULL COMMENT 'сайт',
  `str_date_create` int(11) NOT NULL COMMENT 'дата созд.',
  `str_alias` varchar(255) NOT NULL COMMENT 'URL стр.',
  `str_status` enum('N','Y') NOT NULL DEFAULT 'N' COMMENT 'статус',
  PRIMARY KEY (`str_id`),
  UNIQUE KEY `str_alias` (`str_alias`)
) ENGINE=InnoDB AUTO_INCREMENT=826 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_trainings`
--

LOCK TABLES `site_trainings` WRITE;
/*!40000 ALTER TABLE `site_trainings` DISABLE KEYS */;
INSERT INTO `site_trainings` VALUES (1,'Центр дополнительного образования «Альфа и омега»','Центр «Альфа и омега» работает на рынке дополнительного образования вот уже более 10 лет. За это время центр неоднократно участвовал в конкурсах среди таких же образовательных учреждений и является победителем конкурса «Сделано в Санкт-Петербурге 2008» в номинации «Бизнес-образование». Формы образовательного процесса, которые команда профессионалов «Альфа и омега» использует в учебном процессе, разнообразны: это семинары и тренинги, курсы и лекции, практические занятия и вебинары, рассчитанные на топ-менеджеров, чье свободное время крайне ограничено. При этом в обучающих программах могут участвовать как корпоративные клиенты, так и физические лица.\n\nНаправления, определенные специалистами центра «Альфа и омега», по которым проводится обучение, подразделяются на пять основные ветвей. Это семинары по маркетингу и рекламе; тренинги по повышению эффективности продаж товаров и услуг, бизнес-семинары и тренинги для руководителей, бизнес-семинары по финансовому менеджменту, а также по трудовому законодательству и кадровой работе.\n\nСотрудники центра предоставляют достоверную информацию, которую потом можно использовать в работе, они нацелены на создание коммуникативной связки слушатель-тренер, на свободное общение в ходе обучение и формирование уникального, индивидуального пласта знаний для каждого слушателя. Приветствуется общение с коллегами, заведение деловых связей, коррекция моделей бизнеса на основе опыта других слушателей – это помогает сделать обучение не сухим академическим процессом, а реально действующим инструментом повышения эффективности бизнеса.','Санкт-Петербург, пр. Энгельса д.34 (3 этаж)','+7(812) 988-15-95,+7(812) 294-04-29,+7(812) 248-97-15','info@alphaedu.ru',NULL,1391383445,'http://www.spbkurs.ru/tsentr-dopolnitelnogo-obrazovaniya-alfa-i-omega.htm','Y');
/*!40000 ALTER TABLE `site_trainings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_trainings_bkp`
--

DROP TABLE IF EXISTS `site_trainings_bkp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_trainings_bkp` (
  `str_id` int(11) NOT NULL AUTO_INCREMENT,
  `str_title` varchar(255) NOT NULL COMMENT 'назв.курса',
  `str_desc` text NOT NULL COMMENT 'описание',
  `str_address` varchar(255) DEFAULT NULL COMMENT 'адрес курсов',
  `str_phone` varchar(255) DEFAULT NULL COMMENT 'контакты',
  `str_email` varchar(255) DEFAULT NULL,
  `str_web` varchar(255) DEFAULT NULL COMMENT 'сайт',
  `str_date_create` int(11) NOT NULL COMMENT 'дата созд.',
  `str_alias` varchar(255) NOT NULL COMMENT 'URL стр.',
  `str_status` enum('N','Y') NOT NULL DEFAULT 'N' COMMENT 'статус',
  PRIMARY KEY (`str_id`),
  UNIQUE KEY `str_alias` (`str_alias`)
) ENGINE=InnoDB AUTO_INCREMENT=1590 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_trainings_bkp`
--

LOCK TABLES `site_trainings_bkp` WRITE;
/*!40000 ALTER TABLE `site_trainings_bkp` DISABLE KEYS */;
INSERT INTO `site_trainings_bkp` VALUES (1,'Центр дополнительного образования «Альфа и омега»','Центр «Альфа и омега» работает на рынке дополнительного образования вот уже более 10 лет. За это время центр неоднократно участвовал в конкурсах среди таких же образовательных учреждений и является победителем конкурса «Сделано в Санкт-Петербурге 2008» в номинации «Бизнес-образование». Формы образовательного процесса, которые команда профессионалов «Альфа и омега» использует в учебном процессе, разнообразны: это семинары и тренинги, курсы и лекции, практические занятия и вебинары, рассчитанные на топ-менеджеров, чье свободное время крайне ограничено. При этом в обучающих программах могут участвовать как корпоративные клиенты, так и физические лица.\n\nНаправления, определенные специалистами центра «Альфа и омега», по которым проводится обучение, подразделяются на пять основные ветвей. Это семинары по маркетингу и рекламе; тренинги по повышению эффективности продаж товаров и услуг, бизнес-семинары и тренинги для руководителей, бизнес-семинары по финансовому менеджменту, а также по трудовому законодательству и кадровой работе.\n\nСотрудники центра предоставляют достоверную информацию, которую потом можно использовать в работе, они нацелены на создание коммуникативной связки слушатель-тренер, на свободное общение в ходе обучение и формирование уникального, индивидуального пласта знаний для каждого слушателя. Приветствуется общение с коллегами, заведение деловых связей, коррекция моделей бизнеса на основе опыта других слушателей – это помогает сделать обучение не сухим академическим процессом, а реально действующим инструментом повышения эффективности бизнеса.','Санкт-Петербург, пр. Энгельса д.34 (3 этаж)','+7(812) 988-15-95,+7(812) 294-04-29,+7(812) 248-97-15','info@alphaedu.ru',NULL,1389815912,'http://www.spbkurs.ru/tsentr-dopolnitelnogo-obrazovaniya-alfa-i-omega.htm','Y');
/*!40000 ALTER TABLE `site_trainings_bkp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_users`
--

DROP TABLE IF EXISTS `site_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_users` (
  `su_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `su_lname` varchar(255) DEFAULT NULL,
  `su_fname` varchar(255) DEFAULT NULL,
  `su_mname` varchar(255) DEFAULT NULL,
  `su_login` varchar(32) NOT NULL COMMENT 'использовать Email',
  `su_phone` varchar(32) DEFAULT NULL COMMENT 'контактный телефон',
  `su_position` varchar(255) DEFAULT NULL COMMENT 'должность',
  `su_inn` int(10) DEFAULT NULL COMMENT 'ИНН',
  `su_city` varchar(255) DEFAULT NULL COMMENT 'Город пользователя',
  `su_address` text COMMENT 'Адрес пользователя',
  `su_passw` varchar(32) NOT NULL,
  `su_status` enum('N','Y') NOT NULL DEFAULT 'Y',
  `su_type` enum('fiz','jur') NOT NULL DEFAULT 'fiz' COMMENT 'тип аккаунта',
  `su_date` datetime DEFAULT NULL,
  `su_ip` int(11) DEFAULT NULL,
  `su_last_visit` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`su_id`),
  UNIQUE KEY `su_login` (`su_login`),
  KEY `su_ip` (`su_ip`)
) ENGINE=InnoDB AUTO_INCREMENT=192 DEFAULT CHARSET=utf8 COMMENT='Users of site';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_users`
--

LOCK TABLES `site_users` WRITE;
/*!40000 ALTER TABLE `site_users` DISABLE KEYS */;
INSERT INTO `site_users` VALUES (1,'Николаева','Евгения','','evgenija.nikolaeva@rabota-ya.ru','+7-953-370-67-76',NULL,NULL,NULL,NULL,'817659ff14bc396d13d49e15bfa864e6','Y','fiz',NULL,NULL,'2013-09-13 10:08:05'),(2,'Широковский','Дмитрий','Анатольевич','jimmy.webstudio@gmail.com','+7-921-641-9057',NULL,NULL,NULL,NULL,'c2fe677a63ffd5b7ffd8facbf327dad0','Y','fiz',NULL,2147483647,'2014-03-17 06:11:18');
/*!40000 ALTER TABLE `site_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_users_custom_templates`
--

DROP TABLE IF EXISTS `site_users_custom_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_users_custom_templates` (
  `suct_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `suct_su_id` int(10) unsigned NOT NULL COMMENT 'пользователь',
  `suct_sut_id` int(10) unsigned NOT NULL COMMENT 'шаблон',
  `suct_body` text NOT NULL,
  `suct_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`suct_id`),
  KEY `suct_su_id` (`suct_su_id`),
  KEY `suct_sut_id` (`suct_sut_id`),
  CONSTRAINT `site_users_custom_templates_ibfk_1` FOREIGN KEY (`suct_su_id`) REFERENCES `site_users` (`su_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `site_users_custom_templates_ibfk_2` FOREIGN KEY (`suct_sut_id`) REFERENCES `site_users_templates` (`sut_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='отредактированные шаблоны пользователей';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_users_custom_templates`
--

LOCK TABLES `site_users_custom_templates` WRITE;
/*!40000 ALTER TABLE `site_users_custom_templates` DISABLE KEYS */;
/*!40000 ALTER TABLE `site_users_custom_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_users_messages`
--

DROP TABLE IF EXISTS `site_users_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_users_messages` (
  `sum_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `sum_from_user_id` int(10) unsigned NOT NULL COMMENT 'отправитель',
  `sum_to_user_id` int(10) unsigned NOT NULL COMMENT 'получатель',
  `sum_message` text NOT NULL COMMENT 'сообщение',
  `sum_created` int(10) unsigned NOT NULL COMMENT 'дата отправки',
  `sum_readed` int(10) unsigned DEFAULT NULL COMMENT 'дата прочтения',
  `sum_from_user_status` tinyint(3) unsigned NOT NULL COMMENT 'статус вида у отправителя',
  `sum_to_user_status` tinyint(3) unsigned NOT NULL COMMENT 'статус вида у получателя',
  PRIMARY KEY (`sum_id`),
  KEY `sum_from_user_id` (`sum_from_user_id`),
  KEY `sum_to_user_id` (`sum_to_user_id`),
  KEY `sum_from_user_status` (`sum_from_user_status`),
  KEY `sum_to_user_status` (`sum_to_user_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Сообщения';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_users_messages`
--

LOCK TABLES `site_users_messages` WRITE;
/*!40000 ALTER TABLE `site_users_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `site_users_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_users_searches`
--

DROP TABLE IF EXISTS `site_users_searches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_users_searches` (
  `sus_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sus_su_id` int(11) unsigned NOT NULL COMMENT 'подписавшийся пользователь',
  `sus_title` varchar(64) NOT NULL COMMENT 'название подписки',
  `sus_params` text NOT NULL COMMENT 'параметры поиска',
  `sus_created` int(11) unsigned NOT NULL COMMENT 'дата создания записи',
  PRIMARY KEY (`sus_id`),
  KEY `sus_su_id` (`sus_su_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Подписка на параметры поиска';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_users_searches`
--

LOCK TABLES `site_users_searches` WRITE;
/*!40000 ALTER TABLE `site_users_searches` DISABLE KEYS */;
/*!40000 ALTER TABLE `site_users_searches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_users_subscriber_groups`
--

DROP TABLE IF EXISTS `site_users_subscriber_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_users_subscriber_groups` (
  `susg_id` int(11) NOT NULL AUTO_INCREMENT,
  `susg_title` varchar(255) NOT NULL COMMENT 'наименование группы',
  `susg_desc` text COMMENT 'описание группы',
  `susg_date_create` int(11) NOT NULL COMMENT 'дата создания',
  PRIMARY KEY (`susg_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Группы подписчиков';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_users_subscriber_groups`
--

LOCK TABLES `site_users_subscriber_groups` WRITE;
/*!40000 ALTER TABLE `site_users_subscriber_groups` DISABLE KEYS */;
INSERT INTO `site_users_subscriber_groups` VALUES (1,'Новостные подписчики','Основная группа подписчиков. Была импортирована из старой БД.',1381414424),(2,'Тестовая группа','Для тестовых рассылок',1381751798);
/*!40000 ALTER TABLE `site_users_subscriber_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_users_subscriber_groups_lnk`
--

DROP TABLE IF EXISTS `site_users_subscriber_groups_lnk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_users_subscriber_groups_lnk` (
  `susgl_sus_id` int(10) unsigned NOT NULL COMMENT 'адрес подписчика',
  `susgl_susg_id` int(10) unsigned NOT NULL COMMENT 'группа рассылки',
  `susgl_date_subscribe` int(10) unsigned DEFAULT NULL COMMENT 'дата подписки',
  UNIQUE KEY `susgl_sus_id` (`susgl_sus_id`,`susgl_susg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='подписчики в группах';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_users_subscriber_groups_lnk`
--

LOCK TABLES `site_users_subscriber_groups_lnk` WRITE;
/*!40000 ALTER TABLE `site_users_subscriber_groups_lnk` DISABLE KEYS */;
INSERT INTO `site_users_subscriber_groups_lnk` VALUES (1,1,1297428064),(2,1,1297428028),(3,1,1297427989);
/*!40000 ALTER TABLE `site_users_subscriber_groups_lnk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_users_subscribers`
--

DROP TABLE IF EXISTS `site_users_subscribers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_users_subscribers` (
  `sus_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sus_email` varchar(255) NOT NULL COMMENT 'эл.адр.подписчика',
  `sus_name` varchar(128) DEFAULT NULL COMMENT 'ФИО',
  `sus_date_create` int(11) NOT NULL COMMENT 'дата создания',
  PRIMARY KEY (`sus_id`),
  UNIQUE KEY `ss_email` (`sus_email`)
) ENGINE=InnoDB AUTO_INCREMENT=5291 DEFAULT CHARSET=utf8 COMMENT='Подписчики на рассылку';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_users_subscribers`
--

LOCK TABLES `site_users_subscribers` WRITE;
/*!40000 ALTER TABLE `site_users_subscribers` DISABLE KEYS */;
INSERT INTO `site_users_subscribers` VALUES (1,'info@remdisk.ru','Негина',1297428064),(2,'lesenok43@mail.ru','Олеся',1297428028),(3,'sidorova.tatyana@bk.ru','Татьяна',1297427989),(4,'med-info@bk.ru','Александр Станиславович',1297428113);
/*!40000 ALTER TABLE `site_users_subscribers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_users_templates`
--

DROP TABLE IF EXISTS `site_users_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_users_templates` (
  `sut_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sut_title` varchar(255) NOT NULL COMMENT 'Заголовок',
  `sut_body` text NOT NULL COMMENT 'Текст обращения',
  PRIMARY KEY (`sut_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Шаблоны обращений';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_users_templates`
--

LOCK TABLES `site_users_templates` WRITE;
/*!40000 ALTER TABLE `site_users_templates` DISABLE KEYS */;
INSERT INTO `site_users_templates` VALUES (1,'Отправить резюме на вакансию','Здравствуйте, @FIO@!\r\n\r\nПрошу Вас рассмотреть моё резюме на вакансию @VACANCY@.\r\nЖду Вашего ответа.\r\n\r\nСпасибо.\r\n\r\nС уважением, @MyFIO@.');
/*!40000 ALTER TABLE `site_users_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscribers_cronjobs`
--

DROP TABLE IF EXISTS `subscribers_cronjobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscribers_cronjobs` (
  `sc_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'приоритет рассылки',
  `sc_group_id` int(10) unsigned NOT NULL,
  `sc_template_id` int(10) unsigned NOT NULL,
  `sc_progress_pause` int(10) unsigned DEFAULT NULL COMMENT 'приостановить процесс',
  `sc_count_sent` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'кол-во отправл.писем',
  `sc_date_create` int(10) unsigned DEFAULT NULL COMMENT 'время старта рассылки',
  `sc_date_finish` int(10) unsigned DEFAULT NULL COMMENT 'время завершения рассылки',
  PRIMARY KEY (`sc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COMMENT='Очередь рассылки';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscribers_cronjobs`
--

LOCK TABLES `subscribers_cronjobs` WRITE;
/*!40000 ALTER TABLE `subscribers_cronjobs` DISABLE KEYS */;
INSERT INTO `subscribers_cronjobs` VALUES (1,2,1,NULL,0,1381752007,1381754485),(2,2,1,NULL,0,1381756441,1381756681),(3,1,2,NULL,2560,1381908167,1381958064),(4,1,3,NULL,4955,1381989427,1382007601),(5,1,4,NULL,4956,1382085838,1382103961),(6,2,5,NULL,3,1382515866,1382516041),(7,2,5,NULL,3,1382517006,1382517241),(8,2,5,NULL,3,1382518938,1382519161),(9,1,5,NULL,5050,1382604320,1382622841),(10,1,6,NULL,5050,1384946207,1384964761),(11,1,7,NULL,5050,1385385598,1385404081),(12,1,8,NULL,5050,1386159886,1386178441),(13,1,7,NULL,5050,1386223024,1386241561),(14,2,8,NULL,3,1386539633,1386539662),(15,2,8,NULL,3,1386540812,1386540917),(16,2,8,NULL,3,1386541526,1386541589),(17,2,8,NULL,3,1386542296,1386542430),(18,2,8,NULL,3,1386543244,1386543383),(19,2,8,NULL,3,1386545008,1386545076),(20,1,8,NULL,5050,1386545371,1386564361),(21,2,8,NULL,3,1386583409,1386583499),(22,2,8,NULL,3,1386584602,1386584662),(23,2,8,NULL,3,1386586222,1386586441),(24,2,8,NULL,3,1386586644,1386586801),(25,2,8,NULL,3,1386587521,1386587667),(26,2,8,NULL,3,1386587935,1386587946),(27,2,8,NULL,3,1386588071,1386588086),(28,2,8,NULL,3,1386588524,1386588646),(29,2,8,NULL,3,1386588811,1386588961),(30,1,8,NULL,4693,1386589377,1386607921),(31,1,8,NULL,4812,1387358930,1387378202),(32,1,9,NULL,4734,1390224015,1390243082),(33,1,10,NULL,4789,1391505585,1391524561),(34,1,11,NULL,4393,1391598201,1391617561),(35,1,12,NULL,5049,1392019037,1392039002),(36,1,11,NULL,5087,1392112113,1392131761),(37,1,11,NULL,4970,1392359558,1392379321),(38,1,13,NULL,5123,1393317606,1393340041),(39,1,13,NULL,5189,1393512818,1393532641),(40,1,14,NULL,5063,1394713798,1394733844);
/*!40000 ALTER TABLE `subscribers_cronjobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscribers_sending`
--

DROP TABLE IF EXISTS `subscribers_sending`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscribers_sending` (
  `ss_cronjob_id` int(10) unsigned NOT NULL COMMENT 'процесс рассылки',
  `ss_subscriber_id` int(10) unsigned NOT NULL COMMENT 'подписчик получивший письмо',
  `ss_subscriber_status` int(10) unsigned DEFAULT NULL COMMENT 'статус сбоя отправки',
  KEY `ss_cronjob_id` (`ss_cronjob_id`,`ss_subscriber_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='История рассылки';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscribers_sending`
--

LOCK TABLES `subscribers_sending` WRITE;
/*!40000 ALTER TABLE `subscribers_sending` DISABLE KEYS */;
INSERT INTO `subscribers_sending` VALUES (4,182,NULL),(4,191,NULL),(4,204,NULL),(4,343,NULL),(4,399,NULL),(4,526,NULL),(4,563,NULL),(4,781,NULL),(4,919,NULL),(4,961,NULL),(4,1493,NULL),(4,1678,NULL),(4,1707,NULL),(4,1797,NULL);
/*!40000 ALTER TABLE `subscribers_sending` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-04-07 13:59:11
