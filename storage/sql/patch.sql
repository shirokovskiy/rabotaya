-- Данный патч содержит запросы не выполненные на основном сайте
-- Как только патч применён к основной БД, запрос отсюда удаляется, и обновляется full-dump.sql


-- RENAME TABLE  `rabotaya_web`.`cms_access` TO  `rabotaya_web`.`cms_users_access_modules` ;
-- ALTER TABLE  `cms_users_access_modules` CHANGE  `fa_id_owner`  `fa_id_owner` INT( 10 ) UNSIGNED NOT NULL DEFAULT  '0' COMMENT 'ID группы или пользователя';
-- ALTER TABLE  `cms_users_access_modules` CHANGE  `fa_type`  `fa_type` ENUM(  'group',  'user' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'group' COMMENT  'тип доступа';
-- ALTER TABLE  `cms_users_access_modules` CHANGE  `fa_module`  `cuam_id_module` INT( 10 ) UNSIGNED NOT NULL DEFAULT  '0';
-- ALTER TABLE  `cms_users_access_modules` CHANGE  `fa_id_owner`  `cuam_id_owner` INT( 10 ) UNSIGNED NOT NULL DEFAULT  '0' COMMENT 'ID группы или пользователя';
-- ALTER TABLE  `cms_users_access_modules` CHANGE  `fa_type`  `cuam_type` ENUM(  'group',  'user' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  'group' COMMENT  'тип доступа';
-- ALTER TABLE  `cms_users_access_modules` CHANGE  `fa_id`  `cuam_id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT;
-- ALTER TABLE  `cms_users_access_modules` CHANGE  `fa_access`  `cuam_access` ENUM(  'Y',  'N',  'R' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  'Y';
-- ALTER TABLE  `cms_users_access_modules` CHANGE  `fa_user_set`  `cuam_user_set` ENUM(  'N',  'Y' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  'N';
-- ALTER TABLE  `cms_users_access_modules` ADD  `cuam_id_project` INT UNSIGNED NULL DEFAULT NULL COMMENT  'проект' AFTER  `cuam_id_module` , ADD INDEX (  `cuam_id_project` );
-- UPDATE  `cms_users_access_modules` SET  `cuam_id_project` =1;
-- ALTER TABLE  `cms_users_access_modules` ADD UNIQUE (`cuam_type` ,`cuam_id_owner` ,`cuam_id_module` ,`cuam_id_project`);
-- ALTER TABLE cms_projects_access DROP INDEX fpa_id_user;
-- ALTER TABLE  `cms_projects_access` ADD UNIQUE (`fpa_id_project` ,`fpa_id_user`);


CREATE TABLE `rabotaya_web`.`site_events` (
`sn_id` int( 10 ) unsigned NOT NULL AUTO_INCREMENT ,
`sn_title` varchar( 255 ) NOT NULL DEFAULT '',
`sn_body` text NOT NULL ,
`sn_url` varchar( 255 ) DEFAULT NULL COMMENT 'URL of article',
`sn_date_publ` date DEFAULT NULL ,
`sn_date_add` datetime DEFAULT NULL ,
`sn_date_change` datetime DEFAULT NULL ,
`sn_status` enum( 'N', 'Y' ) NOT NULL DEFAULT 'N',
PRIMARY KEY ( `sn_id` ) ,
KEY `sn_date_change` ( `sn_date_change` )
) ENGINE = MYISAM DEFAULT CHARSET = utf8 COMMENT = 'News';

SET SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO';

INSERT INTO `rabotaya_web`.`site_events`
SELECT *
FROM `rabotaya_web`.`site_news` ;

ALTER TABLE `site_events` CHANGE `sn_id` `se_id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT ,
CHANGE `sn_title` `se_title` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
CHANGE `sn_body` `se_body` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
CHANGE `sn_url` `se_url` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'URL of article',
CHANGE `sn_date_publ` `se_date_publ` DATE NULL DEFAULT NULL ,
CHANGE `sn_date_add` `se_date_add` DATETIME NULL DEFAULT NULL ,
CHANGE `sn_date_change` `se_date_change` DATETIME NULL DEFAULT NULL ,
CHANGE `sn_status` `se_status` ENUM( 'N', 'Y' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N';

UPDATE `job_magazines` SET `jm_url`=REPLACE(`jm_url`,"http://issuu.com/rabotayaru/docs/","");