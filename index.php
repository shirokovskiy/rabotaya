<?php
include_once("cfg/web.cfg.php");

/**
 * CAPTCHA
 */
if ( isset($_GET['get_img']) ) {
    include_once("cls.captcha.php");

    switch ($_GET['get_img']) {
        case 'captchaTrain':
            $objCaptcha = new clsCaptcha('captchaTrain');
            break;

        default: $objCaptcha = new clsCaptcha();
            break;
    }

    if ( is_object($objCaptcha) ) {
        $objCaptcha->imageCreateShow();
        // it stops here
    }
}
/***/

$objTpl = new Template(SITE_TPL_TPL_DIR);

// -----------------------------------
$objUtils->start();
// -----------------------------------
$objUser = new SiteUser();

/**
 * Обработка поддоменов
 */
if (!$_SESSION['isCityConfirmed']) {
    preg_match('%(^.+?)\..+%',$_SERVER["HTTP_HOST"],$_subdomen);
    $_subdomen = $_subdomen[1];
    $_city = $objUser->getCityBySubDomain($_subdomen);
    if(!empty($_city)) {
        $_cityId = $objUser->getCityId($_city);
        $_SESSION['myCity'] = $_city;
        $_SESSION['myCityId'] = $_cityId;
    } else {
        $_SESSION['myCity'] = "Санкт-Петербург";
        $_cityId = $objUser->getCityId($_SESSION['myCity']);
        $_SESSION['myCityId'] = $_cityId;
        #header('Location: http://www.'.MAIN_DOMAIN_NAME);
    }
}


/**
 * Авторизация пользователя
 */
if (isset($_POST['auth']) && $_POST['auth']=='true') {
    $objUser->passAuth($_POST['strAuthLogin'], $_POST['strAuthPassword']);
}

/**
 * Выход из авторизации
 */
if (isset($_GET['logout']) && $_GET['logout'] == 1 ) {
    $objUser->setLogout();
    $_SERVER['REQUEST_URI'] = str_replace("logout=1", "", $_SERVER['REQUEST_URI']);
    if ( preg_match( '/profile/i', $_SERVER["REQUEST_URI"]) ) {
        header("Location: /");
    } else {
        header("Location: ".$_SERVER['REQUEST_URI']);
    }
    exit;
}

/**
 * Проверка авторизации
 */
$arrIf['authed'] = $_SESSION['signInStatus'];
$arrIf['not.authed'] = !$arrIf['authed'];

if ($arrIf['authed']) {
    if (isset($_SESSION['uAuthInfo']) && $_SESSION['uAuthInfo']['uID'] > 0) {
        $objUser->setId($_SESSION['uAuthInfo']['uID']);
        $data = $objUser->getData();

        if (is_array($data) && !empty($data)) {
            foreach ($data as $key => $value) {
                $data[str_replace('su_', '', $key)] = $value;
            }

            $objUser->setData($data);
        }
    }
}

/**
 * ОБРАБОТКА запроса пользователя
 */
$strTmpReqUri = substr($_SERVER['REQUEST_URI'], 1);

if (strpos($strTmpReqUri, '?')>0)
    $strReqUri = substr($strTmpReqUri, 0, strpos($strTmpReqUri, '?'));
else
    $strReqUri = $strTmpReqUri;

// ** Запрашиваемая страница
if(!empty($strReqUri)) {
    $arrReqUri = explode('/', $strReqUri);
    if (SITE_DIR_FROM_ROOT!='' && (strpos($strReqUri, SITE_DIR_FROM_ROOT) !== false) ) {
        $intCountOffset = count(explode('/', SITE_DIR_FROM_ROOT));
        for ($i=0; $i<$intCountOffset; $i++) {
            array_shift($arrReqUri);
        }
    }
    $strReqUri = (is_array($arrReqUri)) ? $arrReqUri[0] : $arrReqUri;
}

if (empty($strReqUri))
    $strReqUri = 'index'; // главная страница
else
    $strReqUri = str_replace('\'', '', trim($strReqUri));

// Для дальнейшего распознавания страницы в стилях CSS
$arrTplVars['pageInCss'] = str_replace('.', '-',  $strReqUri);

// ** Передаваемые параметры
$arrReqQuery = substr(strstr($strTmpReqUri, '?'), 1);
// **************************************************************************************

$strSqlQuery = "SELECT * FROM site_pages, site_pages_meta WHERE sp_alias='".$strReqUri."' AND sp_id_project='".SITE_ID_PROJECT."' AND spm_id_rec=sp_id AND spm_subpage_uid IS NULL AND sp_status='Y'";
$arrInfoPage = $objDb->fetch($strSqlQuery);

/** >>************************************************************************\
 ** Thu Nov 16 00:27:08 MSK 2006, Shirokovskiy Dmitry aka Jimmy™
 * Если страница не найдена - пусть все видят! Пусть будет стыдно! Вот! Это 404 ошибка!!! **/
if ( !is_array($arrInfoPage) || empty($arrInfoPage) ) {
    $_SESSION['errorPage'] = $strReqUri;
    $strSqlQuery = "SELECT * FROM site_pages WHERE sp_alias='error' AND sp_id_project='".SITE_ID_PROJECT."'";
    $arrInfoPage = $objDb->fetch($strSqlQuery);
}

if (!empty($_SESSION['errorPage'])) {
    $arrTplVars['strPageNotFound'] = htmlspecialchars($_SESSION['errorPage']);
    unset($_SESSION['errorPage']);
}

// ***** Если страница не имеет обработчика тегов, ставим теги принадлежащие ей
if( is_null($arrInfoPage['sp_meta_method']) ) {
    $strSqlQuery = "SELECT fp_name FROM cms_projects WHERE fp_id=".SITE_ID_PROJECT;
    $strProjectName = $objDb->fetch($strSqlQuery, 0);
    $arrTplVars['m_title'] = htmlspecialchars($arrInfoPage['spm_title']).' | '.$strProjectName;
    $arrTplVars['m_description'] = htmlspecialchars($arrInfoPage['spm_description']);
    $arrTplVars['m_keywords'] = htmlspecialchars($arrInfoPage['spm_keywords']);
}

// Загрузка шаблона
$objTpl->tpl_load("global.tpl", "site.global.".$arrInfoPage['sp_id_template'].".tpl");
$objTpl->tpl_parse_frg($objTpl->files['global.tpl']); // определяем нужные фрагменты для загрузки

// Загрузка обработчика страницы
require(SITE_PHP_PAGE_DIR.$arrInfoPage['sp_alias'].".".$arrInfoPage['sp_id'].".php");
$objTpl->tpl_parse_frg($objTpl->files['page.content']); // определяем нужные фрагменты для загрузки

// Обработка фрагментов
$objTpl->Template(SITE_TPL_FRG_DIR);
do {
    $strCurrentFrg = current($objTpl->arrFrgForLoad);
    if (!empty($strCurrentFrg)) {
        $objTpl->tpl_load($strCurrentFrg, $strCurrentFrg.".frg");
        $objTpl->tpl_parse_frg($objTpl->files[$strCurrentFrg]);
    }
} while (next($objTpl->arrFrgForLoad));

foreach($objTpl->arrFrgForLoad as $key=>$value) {
    if (file_exists(SITE_PHP_FRG_DIR.$value.".inc.php"))
        include_once(SITE_PHP_FRG_DIR.$value.".inc.php"); // подгружаем обработчики найденных фрагментов
}

$objTpl->tpl_array('global.tpl', $arrTplVars);

header('Content-type: text/html; charset='.METACHARSET);

// Если выводим страницу 404, то и ответ сервера отдает 404
if ($arrInfoPage['sp_alias'] == 'error') {
    header('HTTP/1.0 404 Not Found');
    header('HTTP/1.1 404 Not Found');
    header('Status: 404 Not Found');
}

$objTpl->tpl_parse('global.tpl');
$objDb->disconnect();

echo "\n". '<!-- '.$objUtils->stop().' -->', "\n", '<!-- '.$objUtils->getSqlQuery($objDb).' -->';
