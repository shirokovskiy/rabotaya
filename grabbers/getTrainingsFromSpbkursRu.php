<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 1/10/14
 * Time         : 12:59 AM
 * Description  :
 */
require_once __DIR__ .'/grabber_function.php';

define('URL', "http://spbkurs.ru");
define('URL_ALIAS', "spbkurs");

$patterns['address'] = 'Адрес';
$patterns['web'] = 'Сайт';
$patterns['email'] = 'E-mail';
$patterns['phone'] = 'Телефон';

$objHtmlMainPage = new simple_html_dom(get_web_page(URL));
if (!$objHtmlMainPage) {
    echo "GRABBER ERROR";
    die();
}

$links = $objHtmlMainPage->find("li.cat-item a");
unset($objHtmlMainPage);

if (is_array($links) && !empty($links)) {
    foreach ($links as $link) {
        if (preg_match('/http/', $link->href)) {
            $objChapterPage = new simple_html_dom(get_web_page($link->href));
            echo $link->href. "\n";
            if (!$objChapterPage) {
                write_log(URL_ALIAS, 'Не могу получить страницу '.$link->href);
            } else {
                /**
                 * Получим ссылки на каждый тренинг
                 */
                $trainings = $objChapterPage->find("div.item h2 a");
                unset($objChapterPage);

                if (is_array($trainings) && !empty($trainings)) {
                    foreach ($trainings as $training) {
                        if (preg_match('/http/', $training->href)) {
                            /**
                             * Перейдём на страницу непосредственно тренинга
                             */
                            $objPage = new simple_html_dom(get_web_page($training->href));
                            if (!$objPage) {
                                write_log(URL_ALIAS, 'Не могу получить страницу '.$training->href);
                            } else {
                                $kurs_title = $objPage->find("div.course h1", 0);

                                if (is_object($kurs_title) && $kurs_title instanceof simple_html_dom_node) {
                                    $title = $kurs_title->text();

                                    if (!empty($title)) {
                                        /**
                                         * Если заголовок есть
                                         * прочитае описание курса
                                         */
                                        $kurs_descriptions = $objPage->find("div.options p");

                                        if (is_array($kurs_descriptions) && !empty($kurs_descriptions)) {
                                        	$mysql_kurs_desc = array();
                                            $i = 0;
                                            foreach ($kurs_descriptions as $kurs_text) {
                                                $kurs_text = trim($kurs_text->text());
                                                if (!empty($kurs_text) && !preg_match('/javascript/i', $kurs_text)) {
                                                    $mysql_kurs_desc[] = $kurs_text;
                                                }
                                                if ($i++ > 2) break;
                                            }
                                        } else {
                                            write_log(URL_ALIAS, 'Нет описания курса на стр.'.$training->href);
                                        }

                                        $description = '';
                                        if (is_array($mysql_kurs_desc) && !empty($mysql_kurs_desc)) {
                                            $description = implode("\n\n", $mysql_kurs_desc);
                                        }

                                        $kurs_html = $objPage->find("div.options", 0);
                                        $kurs_html = $kurs_html->innertext();
                                        $kurs_html = substr($kurs_html, strpos($kurs_html, "<div class=\"address\">"));
                                        $kurs_html = substr($kurs_html, 0, strpos($kurs_html, "</div>") + 6);

                                        $kurs_address_data = new simple_html_dom($kurs_html);
                                        $kurs_address_data = $kurs_address_data->find("li");

                                        $address = $phone = $email = '';
                                        $contact_data = array();
                                        if (is_array($kurs_address_data) && !empty($kurs_address_data)) {
                                            foreach($kurs_address_data as $data) {
                                                $string = $data->text();
                                                foreach($patterns as $key => $pattern) {
                                                    if (preg_match('/'.$pattern.'/i', $string)) {
                                                        $contact_data[$key] = trim(str_replace($pattern.":","", $string));
                                                        break;
                                                    }
                                                }
                                            }
                                        }

                                        if (!empty($contact_data['web'])) {
                                            if (!preg_match('/http/', $contact_data['web'])) {
                                                $contact_data['web'] = 'http://'.$contact_data['web'];
                                            }
                                        }

                                        $strSqlQuery = "INSERT INTO `site_trainings` SET"
                                            . " `str_title` = '".mysql_real_escape_string($title)."'"
                                            . ", `str_desc` = '".mysql_real_escape_string($description)."'"
                                            . ", `str_alias` = '".mysql_real_escape_string($training->href)."'"
                                            . (!empty($contact_data['address'])?", `str_address` = '".mysql_real_escape_string($contact_data['address'])."'":'')
                                            . (!empty($contact_data['phone'])?", `str_phone` = '".mysql_real_escape_string($contact_data['phone'])."'":'')
                                            . (!empty($contact_data['email'])?", `str_email` = '".mysql_real_escape_string($contact_data['email'])."'":'')
                                            . (!empty($contact_data['web'])?", `str_web` = '".mysql_real_escape_string($contact_data['web'])."'":'')
                                            . ", `str_date_create` = UNIX_TIMESTAMP()"
                                            . ", `str_status` = 'Y'"
                                        ;
                                        if (!$objDb->query($strSqlQuery, false)) {
                                            # sql error
                                            write_log(URL_ALIAS, 'Ошибка БД: '.$strSqlQuery);
                                        }

                                        unset($objPage);
                                    } else {
                                        write_log(URL_ALIAS, 'Заголовок пустой на стр.'.$training->href);
                                    }
                                } else {
                                    write_log(URL_ALIAS, 'Не разобрать заголовок курса на стр. '.$training->href);
                                }
                            }
                        } else {
                            write_log(URL_ALIAS, 'Ссылка битая '.$training->href);
                        }
                    }
                }
            }
        } else {
            write_log(URL_ALIAS, 'Ссылка битая '.$link->href);
        }
    }
}


