<?php
/*
 * Граббер вакансий для http://ljob.ru/
 * Запуск из командной строки:
 * php -f ./grabbers/getVacanciesFromLjobRu.php
 */
set_time_limit(0);
date_default_timezone_set('Europe/Moscow');
error_reporting(E_ALL & ~E_NOTICE | E_STRICT);
umask(0);
ini_set('memory_limit', '1024M');

require_once __DIR__ .'/grabber_function.php';

define('URL', "http://ljob.ru");
define('URL_ALIAS', "ljob");
define('COOKIEFILE', __DIR__."/cookie_".URL_ALIAS.".txt");

$file_path = __DIR__ ."/log-".URL_ALIAS .".log";
if (file_exists($file_path))
    unlink($file_path);

// http://ljob.ru/vacancy?page=2&fi_city=2&from=&to=&fi_node=all&fi_employment=all&fi_experience=all&fi_grafic=all&sorter=pub_date&order=d
// http://ljob.ru/vacancy?page=3&fi_city=2&from=&to=&fi_node=all&fi_employment=all&fi_experience=all&fi_grafic=all&sorter=pub_date&order=d

echo "Подключение к сайту ...\n";

$SiteUser = null;
$JobCategory = null;
$page = 1;

if (isset($argv[1]) && !empty($argv[1]) && intval($argv[1]) > 0) {
    $page = (int)$argv[1];
}

do {
    echo "Грабим список вакансий на странице номер {$page}...\n";
    $vacancyListHref = "/vacancy?page=$page&fi_city=2&from=&to=&fi_node=all&fi_employment=all&fi_experience=all&fi_grafic=all&sorter=pub_date&order=d";
    $objHtmlPage = new simple_html_dom(get_web_page(URL . $vacancyListHref));
    if (!$objHtmlPage) {
        echo "GRABBER ERROR: Page not found! ". $vacancyListHref ."\n";
        die();
    }

    $count_vacancies = 0;
    $problem_vacancies = 0;

    $is_vacancies = false;

    foreach ($objHtmlPage->find("div.vacancies li span.vac-title") as $list_element) {
        $vacancyTitle = $list_element->find("a",0)->plaintext;
        $vacancyHref = $list_element->find("a",0)->href;

        if (empty($vacancyTitle) || empty($vacancyHref)) {
            write_log(URL_ALIAS, "Вакансия не читается. Ошибка получения вакансии.");
            $problem_vacancies++;
            continue;
        }

        echo "Обработка вакансии '$vacancyTitle' ...\n";
        $objVacancyPage = new simple_html_dom(get_web_page(URL . $vacancyHref));
        if (!$objVacancyPage) {
            echo "GRABBER ERROR: Page not found! ".URL . $vacancyHref."\n";
            die();
        }

        if (!$is_vacancies)
            $is_vacancies = true;

        preg_match('/[^\?]+\?id=([\d]+)/', $vacancyHref, $matches);
        $ID = (int) $matches[1];

        if(!isExistVacancy(URL_ALIAS, $ID)) {
            if(processVacancy($vacancyHref, $ID) === false) {
                $problem_vacancies++;
            } else {
                $count_vacancies++;
            }
        } else {
            echo "Вакансия уже есть\n";
        }
    }

    $statictic = "";
    if($problem_vacancies > 0)
        $statictic .= "Пропущено  $problem_vacancies  вакансий. Смотри логфайл.\n";
    if($count_vacancies > 0)
        $statictic .= "В базу записано $count_vacancies новых вакансий.\n";
    else {
        $statictic = "Вакансий не обнаружено на стр. #$page\n".$vacancyListHref."\n";
    }
    echo $statictic ."\n";

    $page++;

    if (!$is_vacancies) $page = false;

//    if ($page >= 3) {
//        die("\n\n" . __FILE__ . " : " . __LINE__ . "\n\n");
//    }
} while($page !== false);

if (file_exists(COOKIEFILE))
    unlink (COOKIEFILE);
$objHtmlPage->clear();
unset($objHtmlPage);

function processVacancy($vacancyUrl, $vacancy_id) {
    global $SiteUser, $JobCategory;

    $get_url = URL . $vacancyUrl;
    $html_vacancy = send_get($get_url);
    if($html_vacancy == false) {
        write_log(URL_ALIAS, "($get_url) - Ошибка запроса страницы c вакансией");
        return false;
    }

    $objHtmlVacancy = new simple_html_dom($html_vacancy);
    if (!$objHtmlVacancy) {
        write_log(URL_ALIAS, "($get_url) - Ошибка обработки контента для вакансии");
        return false;
    }
    $vacancy['jv_status'] = "Y";
    $vacancy['jv_src_url_alias'] = URL_ALIAS;
    $vacancy['jv_src_id'] = $vacancy_id;

    $objTitleVac = $objHtmlVacancy->find(".content_pt h1", 0);
    if ($objTitleVac) {
        $vacancy['jv_title'] = trim($objTitleVac->plaintext);
    } else {
        write_log(URL_ALIAS, "Не обнаружен идентификатор заголовка для $get_url");
        return false;
    }

    // Chapter = .bor-t-b a span
    // Рубрика
    $rubricId = 0;
    $objRubric = $objHtmlVacancy->find(".content_pt .bor-t-b a span", 0);
    if ($objRubric) {
        $strRubric = trim($objRubric->plaintext);

        if (!empty($strRubric)) {
            #
            if (!is_object($JobCategory)) {
                $JobCategory = new JobCategory();
            }

            $rubricId = $JobCategory->save($strRubric);
        }
    }

    // Зарплата
    $objSalary = $objHtmlVacancy->find(".content_pt table tr td", 0);
    if ($objSalary) {
        $strSalary = $objSalary->find("p", 1)->plaintext;

        if (!empty($strSalary)) {
            $vacancy['jv_salary'] = preg_replace("/[\s]+/u", " ", trim($strSalary));
        }


        $salary_parsed = get_parsed_salary($vacancy['jv_salary']);
        if (isset($salary_parsed['from']))
            $vacancy['jv_salary_from'] = $salary_parsed['from'];
        if (isset($salary_parsed['to']))
            $vacancy['jv_salary_to'] = $salary_parsed['to'];
    }

    $objVacancyProps = $objHtmlVacancy->find("div.content-text", 0);

    $chapter = '';

    foreach($objVacancyProps->childNodes() as $elem) {
        if ($elem->nodeName() == 'h4') {
            $chapter = $elem->innertext;
            continue;
        }

        if ($elem->nodeName() == 'p' && !empty($chapter)) {
            # скорее всего это следующий блок текста с описанием к предыдущему заголовку

            $description = trim($elem->innertext);

            switch ($chapter) {
                case 'Обязанности:':
                case 'Требования:':
                case 'Условия:':
                case 'Адрес:':
                case 'Дополнительная информация:':
                    $vacancy['jv_description'] .= $chapter . "\n" . $description."\n\n";
                    break;

                case 'Занятость:':
                    $work_busy = prepare_work_busy_type($description);
                    $vacancy['jv_jw_id'] = isExistWorkBusyType($work_busy);
                    if ($vacancy['jv_jw_id'] == false)
                        unset($vacancy['jv_jw_id']);
                    break;

                case 'График работы:':
                    if ($description == 'Гибкий график') {
                        $vacancy['jv_jw_id'] = 3;
                    }
                    break;

                case 'Образование:':
                    $vacancy['jv_jet_id'] = isExistEducationType($description);
                    break;

                case 'Телефон:':
                    $vacancy['jv_phone'] = trim(str_replace("<br>", ";", $description));
                    $vacancy['jv_phone'] = preg_replace("/[\s]+/", " ", $vacancy['jv_phone']);
                    break;

                case 'Контактное лицо:':
//                    if (preg_match('/[ООО|ЗАО|ОАО]/', $description)) {
//                        // Вероятно название компании
//                        $company_title = $description;
//                        $company_id = isExistCompany($description);
//                        if (!$company_id) {
//                            $company_type = "direct";
//                            $company_id = parse_and_insert_new_company($company_title, $company_href, $company_type);
//                            if (!$company_id) {
//                                write_log(URL_ALIAS, "Ошибка парсинга работодателя $company_title");
//                            }
//                        } elseif ($company_id)
//                            $vacancy['jv_jc_id'] = $company_id;
//                    } else
                        $vacancy['jv_contact_fio'] = $description;
                    break;

                case 'Email:':
                    $vacancy['jv_email'] = trim(str_replace("<br>", ";", $description));
                    break;
            }
            $chapter = ''; // reset
        }
    }

    if (empty($vacancy['jv_email']) && empty($vacancy['jv_phone'])) {
        echo "Контакты не обнаружены на стр.$vacancyUrl\n";
        return false;
    }

    $objCity = $objHtmlVacancy->find(".content_pt table tr td", 1);

    if ($objCity) {
        $strCity = trim($objCity->find("p", 1)->plaintext);
        if (!empty($strCity)) {
            $vacancy['jv_city'] = preg_replace("/[\s]+/u", " ", $strCity);

            if (!is_object($SiteUser)) {
                $SiteUser = new SiteUser();
            }

            $cityID = $SiteUser->getCityId($strCity);

            if (is_null($cityID) && !empty($strCity)) {
                // There is no city in database. Let's record.
                $cityID = $SiteUser->saveCity($strCity);
            }

            $vacancy['jv_city_id'] = is_null($cityID) ? 1 : $cityID;
        }
    }

//    $objHtmlVacancy->clear();
    unset($objHtmlVacancy);

    $vacancy['jv_description'] = trim($vacancy['jv_description']);
    $vacancy['jv_date_publish'] = time();
    $vacancyId = insert_in_db("job_vacancies", $vacancy);

    if ($vacancyId > 0 && $rubricId > 0) {
        $JobCategory->saveDependancy($rubricId, $vacancyId, 'vacancy');
    }

    return $vacancyId;
}

function send_get($get_url, $headers = false)
{
    $uagent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $get_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_COOKIEJAR, COOKIEFILE);
    curl_setopt($ch, CURLOPT_COOKIEFILE, COOKIEFILE);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($ch, CURLOPT_USERAGENT, $uagent);
    curl_setopt($ch, CURLOPT_VERBOSE, 0);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}

function prepare_work_busy_type($type_frome_site)
{
    switch ($type_frome_site) {
        case "Временная/Проектная работа":
            $newtype = "временная работа";
            break;

        case "Полная занятость":
            $newtype = "полный рабочий день";
            break;

        case "Частичная занятость":
            $newtype = "неполный рабочий день";
            break;

        default:
            $newtype = $type_frome_site;
            break;
    }
    return $newtype;
}
