<?php
/*
 * Граббер резюме для http://domkadrov.ru/
 * Запуск из командной строки:
 * php -f ./grabbers/getResumesFromDomKadrovRu.php
 *
 * Tips & Tricks
 *      Записать объект в файл $objHtmlMainPage->save('ResRvG.'.date('YmdHis').'.htm');
 *      Если объект simple_html_dom_node то можно сделать ->__toString() чтобы посмотреть код
 *      У объекта ссылки:
 *          свойство $link->href        чтобы узнать адрес
 *          метод $link->text()         чтобы узнать текст ссылки
 *
 * Resume Simple   http://domkadrov.ru/empsearch.php?city=2
 * Resume Authed   http://domkadrov.ru/hrsearch.php?positioncode2=282680&city=2
 * Vacancies   http://domkadrov.ru/candsearch.php?city=2
 */
require_once __DIR__ .'/grabber_function.php';

define('URL', "http://www.domkadrov.ru");
define('URL_ALIAS', "dkdr");
define('IMG_PATH', dirname(__DIR__) ."/storage/files/images/resumes/contactimg");
define('COOKIEFILE', __DIR__."/cookie_".URL_ALIAS.".txt");

$Auth = true;
$Local = false;
$Address = new Address();

/**
 * Создадим директорию для картинок, если ещё нет
 */
if (!is_dir(IMG_PATH)) {
    if (!mkdir(IMG_PATH, 0775, true)) {
        die('CANT CREATE DIR FOR IMAGES');
    }
}

/**
 * Send Auth data
 */
$login = 'evgeniya2014@list.ru';
$login = 'jimmya@yandex.ru';
$passw = '12345';
$passw = '1qaz2wsx';

$PostData['lang'] = 2;
$PostData['part'] = '';
$PostData['button'] = '';
$PostData['id'] = $login;
$PostData['passwd'] = $passw;
$PostData['login'] = '';

$Url2Auth = URL.'/empenter.php';
echo "Авторизация ..........................................\n";
/**
 * 282680 - Evgeniya
 * 288007 - I'm
 */
$companyId = 288007;

/**
 * АВТОРИЗАЦИЯ
 *
 * Можно при запуске вручную и после создания файла куки, отключить
 * отправку POST-запроса
 */
if (!$Local && $Auth) {
    $Result = SendPost($Url2Auth, $PostData);
    write_file($Result, '_AfterAuth.htm');
    echo "Авторизация закончена, см.лог\n";
    sleep(2);

    $Url2Check = URL.'/employeredit.php?id='.$companyId;
    $Result = Read($Url2Check, $Url2Auth);
    write_file($Result, '_AfterCheck.htm');
    echo "Проверка аккаунта закончена, см.лог\n";
    sleep(2);
}

/**
 * Параметры запроса страницы первоначальной
 */
$city = 2; // Санкт-Петербург
$filter = "positioncode2=$companyId&city=".$city;
$next_page_url = URL ."/hrsearch.php?".$filter;
$num_page = 1;
$prev_page_url = $Url2Check;

/**
 * Начать постраничный цикл
 */
do {
    write_log(URL_ALIAS, "Загрузка страницы № $num_page со списком резюме ...");

    if (!$Local) {
        $Page = Read($next_page_url, $prev_page_url);
        write_file($Page, '_Page'.$num_page.'.htm', 'w');
    } else {
        $f = __DIR__.'/logs/_Page'.$num_page.'.htm';
        if (file_exists($f))
            $Page = file_get_contents($f);
        else {
            write_log(URL_ALIAS, 'File not found: '.$f);
            continue;
        }
        unset($f);
    }

    $objHtmlMainPage = new simple_html_dom($Page);
    if (!$objHtmlMainPage) {
        die("GRABBER ERROR");
    }
    $resumes_list = $objHtmlMainPage->find("table.test6t tr td a");
    $count = count($resumes_list);

    write_log(URL_ALIAS, "На странице, возможно, $count резюме. Идет обработка страницы...");

    /**
     * Вспомогательный блок статистики
     */
    $statistics['all'] = 0;
    $statistics['false'] = 0;
    $statistics['new'] = 0;
    $statistics['old'] = 0;

    /**
     * Цикл по найденным записям
     */
    foreach ($resumes_list as $resume_block) {
        if(isset($resumeID)) unset($resumeID);
        $href_to_resume = URL . '/'. $resume_block->href;

        write_log(URL_ALIAS, "Вычисляем ID резюме из ссылки ".$href_to_resume);

        $arrUrl = parse_url($href_to_resume);
        if (isset($arrUrl['query'])) {
            parse_str($arrUrl['query'], $arrUrl);
        }

        if (isset($arrUrl['id'])) {
            $resumeID = $arrUrl['id'];
//            if (in_array($resumeID, [1021518,768177])) continue;
            $statistics['all']++;

            write_log(URL_ALIAS, "Проверяем ID резюме [$resumeID] по базе ");

            if (!isExistResume(URL_ALIAS, $resumeID)) {
                write_log(URL_ALIAS, "Пытаемся прочитать резюме");
                if(processResume($href_to_resume, $resumeID))
                    $statistics['new']++;
                else
                    $statistics['false']++;
            } else {
                write_log(URL_ALIAS, "Резюме $href_to_resume УЖЕ есть в БД.");
                $statistics['old']++;
            }

        } else {
            write_log(URL_ALIAS, "$href_to_resume: Ошибка определения ID резюме");
            $statistics['false']++;
        }
    }

    $statictic = "Обработано {$statistics['all']} резюме.\n";
    if($statistics['false'] > 0)
        $statictic .= "Ошибок -  {$statistics['false']} резюме. Смотри логфайл.\n";
    if ($statistics['old'] > 0)
        $statictic .= "{$statistics['old']} резюме УЖЕ были в БД.\n";
    if($statistics['new'] > 0)
        $statictic .= "В базу записано {$statistics['new']} новых резюме.\n";
    else
        $statictic .= "Новых резюме не обнаружено\n";
    echo $statictic."\n";
    unset($statistics);

    /**
     * Проверить пагинатор
     */
    $next_link = $objHtmlMainPage->find("div.test2a div.test4", 1); // todo: а что значит -1 ? наверное "искать с конца"
    if ($next_link) {
        $a = $next_link->find("a", 0);
        $prev_page_url = $next_page_url;
        $num_page++;
        $next_page_url = URL ."/".$a->href;
        echo "Следующая страница $next_page_url \n";
    } else {
        $objHtmlMainPage->clear();
        unset($objHtmlMainPage);
        break;
    }
    $objHtmlMainPage->clear();
    unset($objHtmlMainPage);
} while (true);

echo "Конец работы граббера.", date("H:i:s d.m.Y") ,"\n";


/**
 * Обработать страницу резюме
 *
 * @param $href_to_resume
 * @param $resumeID
 * @return bool
 */
function processResume($href_to_resume, $resumeID, $href_ref = null) {
    global $objUtils, $Address, $Local, $city;

    echo('Resume '.$resumeID."\n");

    if (!$Local) {
        $Page = Read($href_to_resume, $href_ref);
        write_file($Page, '_Resume.'.$resumeID.'.htm', 'w');
    } else {
        $f = __DIR__.'/logs/'.date('Y.m.d').'/_Resume.'.$resumeID.'.htm';
        if (file_exists($f))
            $Page = file_get_contents($f);
        else {
            echo('File not found: '.$f."\n");
            return false;
        }
        unset($f);
    }

    $Page = new simple_html_dom($Page);

    $resume['jr_fio'] = trim($Page->find("head title", 0)->plaintext);
    $resume['jr_grab_src_alias'] = URL_ALIAS;
    $resume['jr_grab_src_id'] = $resumeID;
    if ($city == 2) {
        $resume['jr_city_id'] = 1;
        $resume['jr_city'] = "Санкт-Петербург";
    }

    $listParameters = $Page->find("div.test2 table.test6cv tr");
    foreach($listParameters as $recordTD) {
        $TD1 = $recordTD->find("td", 0)->plaintext;
        $TD2 = $recordTD->find("td", 1)->plaintext;
        $TD3 = $recordTD->find("td", 2)->plaintext;
        $TD4 = $recordTD->find("td", 3)->plaintext;

        if (preg_match('/Мобильный телефон/', $TD3)) {
            $resume['jr_phone'] = preg_replace("/[\s]+/",'', $TD4);
            $resume['jr_phone'] = preg_replace('/\-+/','-', $resume['jr_phone']);
        }

        if (preg_match('/Электронная почта/', $TD1)) {
            $resume['jr_email'] = $TD2;
            if (!$objUtils->isValidEmail($resume['jr_email'])) {
                unset($resume['jr_email']);
                $error = "Резюме $resumeID: [$href_to_resume] Ошибка парсинга Email\n";
                write_log(URL_ALIAS, $error);
            }
        }
    }

    $listParameters = $Page->find("table#frame table.test6cv tr");

    $c = 0; $startDetectionHR = false;
    foreach($listParameters as $recordTD) {
        $TD1 = trim($recordTD->find("td", 0)->plaintext);
        $TD2 = trim($recordTD->find("td", 1)->plaintext);
        $TD3 = trim($recordTD->find("td", 2)->plaintext);
        $TD4 = trim($recordTD->find("td", 3)->plaintext);
        $HR = $recordTD->find("td", 0)->find("hr", 0);

//        echo $TD1, ' | ',$TD2, ' | ',$TD3, ' | ',$TD4, " -=XXX=- \n";

        if ($startDetectionHR && is_object($HR)) {
//            echo "\n". 'HR'."\n";
            $startDetectionHR = false;
        }

        if ($c==0 && !empty($TD1) && !isset($resume['jr_fio'])) {
            // Возможно это ФИО
            $fio = $recordTD->find("td", 0)->find("b u", 0)->plaintext;
            if (!empty($fio)) {
                /**
                 * ФИО
                 */
                $resume['jr_fio'] = $fio;
            }
        }

        if (preg_match('/Дата рождения/', $TD1)) {
            $dob = $TD2;
            $dob = explode(' ', $dob);
            if (is_array($dob) && !empty($dob) && count($dob) == 3) {
                $resume['jr_birthday'] = $dob[2].'-'.$dob[1].'-'.$dob[0];
            } else {
                write_log(URL_ALIAS, "Резюме $resumeID: [$href_to_resume] Ошибка парсинга birthday\n");
            }
        }

        if (preg_match('/Пол/', $TD1)) {
            $gender = $TD2;
            $resume['jr_sex'] = $gender == 'муж' ? 'male' : ($gender == 'жен' ? 'female' : '');
            if (empty($resume['jr_sex'])) {
                write_log(URL_ALIAS, "Резюме $resumeID: [$href_to_resume] Ошибка парсинга gender\n");
                unset($resume['jr_sex']);
            }
        }

        if (preg_match('/Семейное положение/', $TD1) && !empty($TD2)) {
            $marriage = $TD2;
            if (!empty($marriage) && in_array($marriage, array('женат','замужем'))) {
                $resume['jr_marriage'] = 'Y';
            } else $resume['jr_marriage'] = 'N';
        }

//        if (preg_match('/Гражданство/', $TD1) && !empty($TD2)) {
            //echo 'Гражданство '.$TD2."\n";
//        }

        if (preg_match('/Образование/', $TD1) && !empty($TD2)) {
            $resume['jr_edu_plus'] = $TD2;
        }

        if (preg_match('/Языки/', $TD1) && !empty($TD2)) {
            $resume['jr_languages'] = $TD2;
        }

        if (preg_match('/Должность/', $TD1) && !empty($TD2)) {
            $resume['jr_title'] = $TD2;
        }

        if (preg_match('/Опыт работы/', $TD1)) {
            $startDetectionHR = true;
        }

        if (preg_match('/Резюме кандидата в свободном формате/', $TD1)) {
            $TD1 = preg_replace('/Резюме кандидата в свободном формате/', '', $TD1);
            $resume['jr_about'] = (isset($resume['jr_about']) ? $resume['jr_about']."\n\n".trim($TD1) : trim($TD1));
        }

        if (preg_match('/Нынешняя зарплата/', $TD1) && !empty($TD2)) {
            $resume['jr_salary'] = $TD2;
        }

        if ($startDetectionHR) {
            /**
             * Здесь собираем данные по Опыту работы
             */
            if (!empty($TD1)) {
                $resume['jr_about'] .= $TD1." ";
            }

            if (!empty($TD2)) {
                $resume['jr_about'] .= $TD2." ";
            }
        }

        $c++;// ;)
    }

    /**
     * Address
     */
    /*$address = $listParameters[$row + 2]->find("td", 0)->plaintext;
    if (preg_match('/[Адрес]/', $second_phone)) {
        $address = $listParameters[$row + 2]->find("td", 1)->plaintext;
        if (preg_match("/,/", $address)) {
            $address = explode(',',$address);
            if (is_array($address) && !empty($address)) {
                $city = $address[0];
                $country = $address[1];
                unset($address);

                $id = $Address->saveCity($city, $country);
                $resume['jr_city_id'] = $id;
                $resume['jr_city'] = $city;
                unset($id);
            }
        }
    }*/



    /**
     * Отрасль и должность
     */
//    $specific = $listParameters[$row + 3]->find("td", 1)->plaintext;
//    $specs = $listParameters[$row + 3]->find("td", 3)->plaintext;
//    $grazhd = $listParameters[$row + 11]->find("td", 1)->plaintext;
//    $foto = $listParameters[$row + 16]->find("td a img", 0)->src;


    /**
     * Дата публикации
     */
    $date_publish = $Page->find("table.statistics", 0);
    if(is_object($date_publish)) {
        $date_publish = $date_publish->lastChild()->find("td", 0)->plaintext;
    }
    if (!empty($date_publish) && is_string($date_publish)) {
        $resume['jr_date_publish'] = strtotime($date_publish);
    } else {
        write_log(URL_ALIAS, "Резюме $resumeID: [$href_to_resume] Ошибка парсинга Даты публикации\n");
        return false;
    }

    /**
     * Записываем резюме
     */
    $newResumeID = insert_in_db("job_resumes", $resume);

    if (!$newResumeID) {
        write_log(URL_ALIAS, "Резюме $resumeID: [$href_to_resume] Ошибка записи в базу данных\n");
        return false;
    }

    /*if (is_array($works)) {
        foreach ($works as $work) {
            $work['jrw_jr_id'] = $newResumeID;
            insert_in_db("job_resumes_works", $work);
        }
    }
    if (is_array($educations)) {
        foreach ($educations as $education) {
            $education['jre_jr_id'] = $newResumeID;
            insert_in_db("job_resumes_education", $education);
        }
    }*/

    $Page->clear();
    unset($Page);
    $pause = rand(2, 10);
    sleep($pause);
    echo "Следующая страница... \n";
    return true;
}

