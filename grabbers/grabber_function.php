<?php
$_SERVER['DOCUMENT_ROOT'] = dirname(__DIR__);
require_once __DIR__.'/simple_html_dom.php';
require_once dirname(__DIR__) .'/lib/cls.db.php';
require_once dirname(__DIR__) .'/cfg/db.cfg.php';
require_once dirname(__DIR__) .'/cfg/web.cfg.php';

$arrBrowsers = array(
    "Mozilla/4.0 (compatible; MSIE 6.0; America Online Browser 1.1; Windows NT 5.1; FunWebProducts)"
    , "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/31.0.1650.63 Chrome/31.0.1650.63 Safari/537.36"
    , "Mozilla/4.0 (compatible; MSIE 8.0; AOL 9.6; AOLBuild 4340.27; Windows NT 5.1; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)"
    , "Mozilla/4.0 (compatible; MSIE 7.0; AOL 9.5; AOLBuild 4337.81; Windows NT 6.0; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; .NET CLR 3.5.30729; .NET CLR 3.0.30618)"
    , "Mozilla/5.0 (X11; U; Linux; pt-PT) AppleWebKit/523.15 (KHTML, like Gecko, Safari/419.3) Arora/0.4"
    , "Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.8.1.9) Gecko/20071103 BonEcho/2.0.0.9"
    , "Mozilla/5.0 (BeOS; U; BeOS BePC; en-US; rv:1.8.1.17) Gecko/20080831 BonEcho/2.0.0.17"
    , "Mozilla/5.0 (Macintosh; U; PPC Mac OS X 10.4; en; rv:1.9.2.24) Gecko/20111114 Camino/2.1 (like Firefox/3.6.24)"
    , "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.9 Safari/536.5"
    , "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.215 Safari/535.1"
    , "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; Crazy Browser 1.0.5; (R1 1.3))"
    , "ELinks/0.11.1-1.4-debian (textmode; Linux 2.6.21-1-686 i686; 198x78-3)"
    , "Enigma Browser"
    , "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.6b) Gecko/20031212 Firebird/0.7+"
    , "Mozilla/5.0 (Windows; U; Windows NT 6.0; ko; rv:1.9.1.3) Gecko/20090824 Firefox/3.5.3 (.NET CLR 3.5.30729)"
    , "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_6; en-US) AppleWebKit/534.7 (KHTML, like Gecko) Flock/3.5.3.4628 Chrome/7.0.517.450 Safari/534.7"
    , "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.2) Gecko/20040906 Galeon/1.3.17"
    , "iCab/5.0 (Macintosh; U; PPC Mac OS X)"
    , "IBrowse/2.4demo (AmigaOS 3.9; 68K)"
    , "IBM WebExplorer /v0.94"
    , "HotJava/1.1.2 FCS"
    , "Mozilla/5.0 (Windows; U; MSIE 9.0; Windows NT 9.0; en-US)"
    , "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.21pre) Gecko K-Meleon/1.7.0"
);

$objDb = new Db($_db_config);
define('UserAgent', $arrBrowsers[array_rand($arrBrowsers)]);

function write_log($url_alias, $data) {
    $path = __DIR__ ."/logs/".date('Y.m.d');
    if (!file_exists($path) && !is_dir($path)) {
        if (!mkdir($path, 0775, true)) {
            die( "Can`t create DIR ". $path ."\n" );
        }
    }
    $file_path = $path ."/log-$url_alias-".date('YmdH').".log";
    $fp = fopen($file_path, "a");
    if (!$fp) {
        echo "Ошибка создания файла логов\n$file_path";
        die();
    }
    if (!fwrite($fp, date('Y-m-d H:i:s')." ".$data."\n")) {
        echo "Ошибка записи в файл логов";
        die();
    }
    return fclose($fp);
}

function write_file($data, $name='JIMMY.LOG', $mode = 'a+', $vardump = true)
{
    $path = __DIR__ ."/logs/".date('Y.m.d');
    if (!file_exists($path) && !is_dir($path)) {
        if (mkdir($path, 0777, true)) {
            die( "Can't create DIR ". $path ."\n" );
        }
    }
    $file_path = $path ."/".$name;
    $fp = fopen($file_path, $mode);
    if (!$fp) {
        echo "Ошибка создания файла\n$file_path";
        die();
    }

    $out = '';
    $object = $data;
    if (is_object($object)) {
        $classNameOfObj = get_class($object);
        $out .= $classNameOfObj . "\n";
        $ms = get_class_methods($object);
        sort($ms);
        $out .= print_r($ms, true);
        $out .= "\n\n";
    } else {
        $out .= 'Not object' . "\n";
        if (is_array($object)) {
            $out .= 'Array:' . "\n";
            foreach ($object as $k => $v) {
                if (is_object($v))
                    $object[$k] = get_class($v);
            }
            $out .= print_r($object, true) . "\n";
        } else {
            ob_start();
            if ($vardump) {
                var_dump($object);
            } else {
                print_r($object);
            }
            $vardump_out = ob_get_contents();
            ob_clean();
            $out .= $vardump_out . "\n";
        }
    }
    if (!fwrite($fp, $out."\n")) {
        echo "Ошибка записи в файл $file_path";
        die();
    }
    return fclose($fp);
}

function isExistVacancy($url_alias, $id_vacancy) {
    global $objDb;
    $data = $objDb->fetch("SELECT * FROM `job_vacancies` WHERE `jv_src_url_alias` ='$url_alias' AND `jv_src_id` ='$id_vacancy'");
    if (!is_array($data) || empty($data))
        return false;
    return true;
}

function isExistResume($url_alias, $id_resume) {
    global $objDb;
    $data = $objDb->fetch("SELECT * FROM `job_resumes` WHERE `jr_grab_src_alias` ='$url_alias' AND `jr_grab_src_id` = $id_resume");
    if (!is_array($data) || empty($data))
        return false;
    return true;
}

function isExistCompany($title) {
    global $objDb;
    $title = mysql_real_escape_string($title, $objDb->conn);
    $data = $objDb->fetch("SELECT * FROM `job_companies` WHERE `jc_title` ='$title'"); //AND `jc_city` ='$city'");
    if (!is_array($data) || empty($data))
        return false;
    return $data['jc_id'];
}

function isExistWorkBusyType($work_busy) {
    global $objDb;
    $buf = mb_strtolower($work_busy, 'UTF-8');
    $data = $objDb->fetch("SELECT * FROM `job_workbusy` WHERE `jw_title` ='$buf'");
    if (!is_array($data) || empty($data))
        return false;
    return $data['jw_id'];
}

function isExistEducationType($educ_type) {
    global $objDb;
    $educ_type = mb_ucfirst($educ_type);
    if($educ_type == "Неполное высшее")
        $educ_type = "Неоконченное высшее";
    if($educ_type == "Среднее-специальное")
        $educ_type = "Среднее специальное";

    $data = $objDb->fetch("SELECT * FROM `job_education_types` WHERE `jet_title` = '$educ_type'");
    if (!is_array($data) || empty($data))
        return false;
    return $data['jet_id'];
}

function insert_in_db($table, $data) {
    global $objDb;
    foreach ($data as $field => $value) {
        $all_fields[] = "`$field`";
        $value = mysql_real_escape_string($value);
        $all_values[] = "'{$value}'";
    }
    $str_fields = "(".implode(",", $all_fields) .")";
    $str_values = "(".implode(",", $all_values) .")";
    $objDb->setDebugModeOn();
    $sql = "INSERT INTO `$table` $str_fields VALUES $str_values";
    if ($objDb->query($sql)) {
        $objDb->setDebugModeOn();
        return $objDb->insert_id();
    }

    die("ERROR in SQL query: $sql");
}

/**
 * Возвращает контент по url посредством curl
 * @param type $url
 * @param array $post_fields - поля запроса  методом POST
 * @return string
 */
function get_web_page($url, $post_fields = false, $referer = false, $arr_headers = false) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);   // возвращает веб-страницу
    curl_setopt($ch, CURLOPT_HEADER, 0);           // не возвращает заголовки
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);   // переходит по редиректам
    curl_setopt($ch, CURLOPT_ENCODING, "");        // обрабатывает все кодировки
    //curl_setopt($ch, CURLOPT_USERAGENT, $uagent);  // useragent
    //curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120); // таймаут соединения
    //curl_setopt($ch, CURLOPT_TIMEOUT, 120);        // таймаут ответа
    curl_setopt($ch, CURLOPT_MAXREDIRS, 10);       // останавливаться после 10-ого редиректа

    if (is_array($post_fields) && !empty($post_fields)) {
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_fields));
    }
    if ($referer !== false)
        curl_setopt($ch, CURLOPT_REFERER, $referer);
    if (is_array($arr_headers) && !empty($arr_headers)) {
        curl_setopt($ch, CURLOPT_HTTPHEADER, $arr_headers);
    }
    $content = curl_exec($ch);
    $err = curl_errno($ch);

    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);
    curl_close($ch);

    $header['errno'] = $err;
    $header['errmsg'] = $errmsg;
    $header['content'] = $content;
    return $content;
    //return $header;
}

/**
 * Возвращает парсенную зарплату
 * @param type $salary_text
 * @return string|array
 */
function get_parsed_salary($salary_text) {
    $salary_text = trim($salary_text);
    $salary_text = preg_replace("/[\s]+/"," ", $salary_text);
    if (!preg_match("/от ([0-9]+[\s]?[0-9]+) [а-я]/", $salary_text, $out)) { //Проверяем зарплату ОТ
        if (!preg_match("/до ([0-9]+[\s]?[0-9]+)[\s]?[а-я]/", $salary_text, $out))
            return $salary_text; // выход без изменения входного инфо о зарплате
        $salary['to'] = preg_replace("/[\s]+/", "", $out[1]); //Удаляем пробел
        $salary['to'] = preg_replace("/[^0-9]+/", "", $salary['to']);
    } else {
        $salary['from'] = preg_replace("/[\s]+/", "", $out[1]); //Удаляем пробел
        $salary['from'] = preg_replace("/[^0-9]+/", "", $salary['from']);
        unset($out);
        if (preg_match("/до (.+) [а-я]+/", $salary_text, $out)) {//Проверяем зарплату ДО
            $salary['to'] = preg_replace("/[\s]+/", "", $out[1]); //Удаляем пробел
            $salary['to'] = preg_replace("/[^0-9]+/", "", $salary['to']);
        }
    }
    return $salary;
}

/*
 * возвращает метку
 */
function get_parsed_date($date) {
    $parsed_date = explode(" ", trim($date));
    $month[1] = 'январ';
    $month[] = 'феврал';
    $month[] = 'март';
    $month[] = 'апрел';
    $month[] = 'ма';
    $month[] = 'июн';
    $month[] = 'июл';
    $month[] = 'август';
    $month[] = 'сентябр';
    $month[] = 'октябр';
    $month[] = 'ноябр';
    $month[] = 'декабр';
    foreach ($month as $key => $value) {
        if (strpos($parsed_date[1], $value) !== false) {
            $parsed_date[1] = $key;
            return mktime(0, 0, 0, $parsed_date[1], $parsed_date[0], $parsed_date[2]);
        }
    }
    return false;
}

/*
 * Граббер вакансий для http://spb.vacansia.ru/
 *
 */
function get_vacancies_from_spbvacansiaru() {

    $url = "http://spb.vacansia.ru/index.php?act=searchvac";
    $referer = "http://spb.vacansia.ru/index.php?act=searchvac&mode=new_search";
    //region_fs=%24current_region_kod&city_fs=1&key_word=&stop_word=&zp=&search_vac=%ED%E0%E9%F2%E8
    $post_fields ['region_fs'] = "2";
    $post_fields ['city_fs'] = '1';
    //$post_fields ['search_vac'] = "Искать";//"%ED%E0%E9%F2%E8";
    //$post_fields ['key_word'] = "";
    //$post_fields ['stop_word'] = "";
    $post_fields ['send_fs'] = "Искать"; //iconv("UTF-8", "CP1251", "Искать");
    $post_fields ['what_search'] = "0";
    $post_fields ['rubr_fs'] = "0";

    $arr_headers[] = "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
    $arr_headers[] = "Accept-Encoding: gzip, deflate";
    $arr_headers[] = "Connection: keep-alive";
    $arr_headers[] = "Host: spb.vacansia.ru";
    $arr_headers[] = "Referer: http://spb.vacansia.ru/";
    $arr_headers[] = "Cookie: __utma=71231887.821169252.1377208224.1377284963.1377292830.6; __utmz=71231887.1377208224.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); vacansia=e93b3ad1e1c21a0606ccd69cd4de00a0; __utmc=71231887; __utmb=71231887.1.10.1377292830";

    $objSHD = new simple_html_dom(get_web_page($url, $post_fields, "http://spb.vacansia.ru/", $arr_headers));
    if (!$objSHD) {
        echo "Gtabbers ERROR";
        die();
    }
    foreach ($objSHD->find("a[href]") as $value) {
        echo $value->href ."\n\r";
    }
    die();
    echo $objSHD->innertext;
    die();
    $last_a = $objSHD->find(iconv("UTF-8", "CP1251", 'a[href ^= /index.php?act=searchvac&strp=]'), 0);
    if (isset($last_a->href))
        echo 'href exist!';
    die();
    for ($i = 1; $i < $count_pages; $i++) {
        // curl 'http://spb.vacansia.ru/index.php?act=searchvac' -H 'Cookie: vacansia=5540dec0a136f960175a3bc423cce573; __utma=71231887.140498764.1377202025.1377208160.1377211304.3; __utmb=71231887.2.10.1377211304; __utmc=71231887; __utmz=71231887.1377202025.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)' -H 'Origin: http://spb.vacansia.ru' -H 'Accept-Encoding: gzip,deflate,sdch' -H 'Host: spb.vacansia.ru' -H 'Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4' -H 'User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' -H 'Cache-Control: max-age=0' -H 'Referer: http://spb.vacansia.ru/index.php?act=searchvac&mode=new_search' -H 'Connection: keep-alive' --data 'region_fs=%24current_region_kod&city_fs=1&key_word=&stop_word=&zp=&search_vac=%ED%E0%E9%F2%E8' --compressed
    }
}

function get_web_page_frome_spbvacansiaru() {
    $uagent = "Opera/9.80 (Windows NT 6.1; WOW64) Presto/2.12.388 Version/12.14";
    //$uagent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0";
    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);   // возвращает веб-страницу
    curl_setopt($ch, CURLOPT_HEADER, 0);           // не возвращает заголовки
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);   // переходит по редиректам
    curl_setopt($ch, CURLOPT_ENCODING, "");        // обрабатывает все кодировки
    //curl_setopt($ch, CURLOPT_USERAGENT, $uagent);  // useragent
    //curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120); // таймаут соединения
    //curl_setopt($ch, CURLOPT_TIMEOUT, 120);        // таймаут ответа
    curl_setopt($ch, CURLOPT_MAXREDIRS, 10);       // останавливаться после 10-ого редиректа

    if (is_array($post_fields) && !empty($post_fields)) {
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_fields));
    }
    if ($referer !== false)
        curl_setopt($ch, CURLOPT_REFERER, $referer);
    if (is_array($arr_headers) && !empty($arr_headers)) {
        curl_setopt($ch, CURLOPT_HTTPHEADER, $arr_headers);
    }
    $content = curl_exec($ch);
    $err = curl_errno($ch);

    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);
    curl_close($ch);

    $header['errno'] = $err;
    $header['errmsg'] = $errmsg;
    $header['content'] = $content;
}

function getCP866($str, $in_char_set = false) {
    return iconv(($in_char_set === false ? "UTF-8" : $in_char_set), "CP866", $str);
}

/**
 * Авторизация через CURL (в основном), а также любой другой POST-запрос
 * @param $url
 * @param $PostData
 * @return mixed
 */
function SendPost($url,$PostData) {
    $Headers[] = "Content-Type: application/x-www-form-urlencoded; charset=windows-1251";

    $ch = curl_init();
//    if(strtolower((substr($url,0,5))=='https')) { // если соединяемся с https
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
//    }
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0); // пустые заголовки
    // откуда пришли на эту страницу
    curl_setopt($ch, CURLOPT_REFERER, $url);
    curl_setopt($ch, CURLOPT_VERBOSE, 0); // 1 = cURL будет выводить подробные сообщения о всех производимых действиях
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // следовать за редиректами
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($PostData));
    curl_setopt($ch, CURLOPT_USERAGENT, UserAgent);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //сохранять полученные COOKIE в файл
    curl_setopt($ch, CURLOPT_COOKIEJAR, COOKIEFILE);
    curl_setopt($ch, CURLOPT_COOKIEFILE, COOKIEFILE);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $Headers);

    if (!$result = curl_exec($ch)) {
        trigger_error(curl_error($ch));
    }
    curl_close($ch);
    return $result;
}

/**
 * Чтение страницы после авторизации
 * @param $url
 * @return mixed
 */
function Read($url, $referrer = null) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    // откуда пришли на эту страницу
    curl_setopt($ch, CURLOPT_REFERER, (!is_null($referrer)?$referrer:$url));
    //запрещаем делать запрос с помощью POST и соответственно разрешаем с помощью GET
    curl_setopt($ch, CURLOPT_POST, 0); // Get-query
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    //отсылаем серверу COOKIE полученные от него при авторизации
    curl_setopt($ch, CURLOPT_COOKIEJAR, COOKIEFILE);
    curl_setopt($ch, CURLOPT_COOKIEFILE, COOKIEFILE);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($ch, CURLOPT_USERAGENT, UserAgent);
    curl_setopt($ch, CURLOPT_VERBOSE, 0); // Не выводить доп.инфо
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Вернуть результат в виде строки

    if (!$result = curl_exec($ch)) {
        trigger_error(curl_error($ch));
    }
    curl_close($ch);
    return $result;
}

/**
 * String encoding in correct way
 * @param $sInput
 * @return string
 */
function cp1251_utf8( $sInput )
{
    $sOutput = "";

    for ( $i = 0; $i < strlen( $sInput ); $i++ )
    {
        $iAscii = ord( $sInput[$i] );

        if ( $iAscii >= 192 && $iAscii <= 255 )
            $sOutput .=  "&#".( 1040 + ( $iAscii - 192 ) ).";";
        else if ( $iAscii == 168 )
            $sOutput .= "&#".( 1025 ).";";
        else if ( $iAscii == 184 )
            $sOutput .= "&#".( 1105 ).";";
        else
            $sOutput .= $sInput[$i];
    }

    return $sOutput;
}

function removeTrash ($string) {
    $string = iconv("CP1251", "UTF-8", $string);
    $string = str_replace("charset=windows-1251", "charset=utf-8", $string);
    $string = str_replace("&nbsp;", " ", $string);
    $string = str_replace("&nbsp", " ", $string);
    $string = preg_replace("/<!--[^-]+[^-]+[^>]+>/im", "", $string);
    $string = preg_replace("/<script.+script>/im", "", $string);
    return trim($string);
}

/**
 * Проверить, читался ли этот ID-шник ранее.
 * Был ли он зафиксирован как успешное снятие вакансии?
 * Или не превышает ли он величину последнего прочитанного?
 *
 * @param $num
 * @return bool
 */
function isReadBefore($num)
{
    if (!is_dir(READ_FLAGS_DIR)) {
        return false;
    }

    if (file_exists(READ_FLAGS_DIR.'/'.$num)) {
        return true;
    }

    if (file_exists(READ_FLAGS_DIR.'/'.READ_FLAG_FILE)) {
        $data = file_get_contents(READ_FLAGS_DIR.'/'.READ_FLAG_FILE);
        $data = trim($data);
        if (!empty($data) && $num > intval($data)) {
            return true;
        }
    }

    return false;
}


/**
 * Записать файл с именем ID успешно снятого объекта
 * Сохранить ID последнего прочитанного объекта
 *
 * @param $num
 * @param bool $exist_record
 */
function setIReadIt($num, $exist_record = false)
{
    if (!is_dir(READ_FLAGS_DIR)) {
        mkdir(READ_FLAGS_DIR, 0777, true);
    }

    file_put_contents(READ_FLAGS_DIR.'/'.READ_FLAG_FILE, $num);

    if ($exist_record) {
        file_put_contents(READ_FLAGS_DIR.'/'.$num, '+');
    }
}

