<?php
/*
 * Граббер резюме для http://spb.jobfine.ru/
 * Запуск из командной строки:
 * php -f ./grabbers/getResumesFromJobFineRu.php
 */
require_once __DIR__ .'/grabber_function.php';

define('URL', "http://peterburg.jobfine.ru");
define('URL_ALIAS', "jbfn");
define('IMG_PATH', dirname(__DIR__) ."/storage/files/images/resumes/contactimg");

if (!is_dir(IMG_PATH)) {
    if (!mkdir(IMG_PATH, 0775, true)) {
        die('CANT CREATE DIR FOR IMAGES');
    }
}

$file_path = __DIR__ ."/log-".URL_ALIAS .".log";
if (file_exists($file_path))
    unlink($file_path);

//$id = 11512;
//$id = 6415;
//echo "result= ". process_resume("http://peterburg.jobfine.ru/resume.html?id=$id", $id);
//die();

$filter = "kw=&pt=&pv=1&fd=&action=";
//$filter = "kw=&c=2287&city=2287&region=&m=&pf=&pt=&pv=1&ex=0&af=&at=&sx=0&fd=&ke=&id=0";
$next_page_url = URL ."/search_resume.html?".$filter;
$num_page = 1;
do {
    echo "Загрузка страницы № $num_page со списком резюме ...\n";
    $objHtmlMainPage = new simple_html_dom(get_web_page($next_page_url));
    if (!$objHtmlMainPage) {
        echo "GRABBER ERROR";
        die();
    }
    $resumes_divs = $objHtmlMainPage->find("div.results");
    $count = count($resumes_divs);

    echo "Успешно.\nНа странице $count резюме.\nИдет обработка страницы...\n";
    $statistics['all'] = 0;
    $statistics['false'] = 0;
    $statistics['new'] = 0;
    $statistics['old'] = 0;
    foreach ($resumes_divs as $div) {
        $table = $div->find("table.results_table", 0);
        $href_to_resume = URL . $table->find("tr", 1)->find("a",0)->href;
        unset($resumeID);
        if (preg_match("/[0-9]*$/", $href_to_resume, $resumeID)) {
            $statistics['all']++;
            $resumeID = $resumeID[0];
            if (!isExistResume(URL_ALIAS, $resumeID)) {
                if(process_resume($href_to_resume, $resumeID))
                     $statistics['new']++;
                else
                    $statistics['false']++;
            } else {
                //write_log(URL_ALIAS, "Резюме $href_to_resume УЖЕ есть в БД.\n");
                $statistics['old']++;
            }

         } else {
             write_log(URL_ALIAS, "$href_to_resume: Ошибка определения ID резюме\n");
             $statistics['false']++;
         }
         $table->clear();
         unset($table);
    }

    $statictic = "Обработано {$statistics['all']} резюме.\n";
    if($statistics['false'] > 0)
        $statictic .= "Ошибок -  {$statistics['false']} резюме. Смотри логфайл.\n";
    if ($statistics['old'] > 0)
        $statictic .= "{$statistics['old']} резюме УЖЕ были в БД.\n";
    if($statistics['new'] > 0)
        $statictic .= "В базу записано {$statistics['new']} новых резюме.\n";
    else
        $statictic .= "Новых резюме не обнаружено\n";
    echo $statictic."\n";
    unset($statistics);

    $li = $objHtmlMainPage->find("div.utilites li", -1);
    if ($li) {
        unset($next_page_url);
        $a = $li->find("a", 0);
        if (!$a || !preg_match("/Следующая/", $a->plaintext)) {
            $objHtmlMainPage->clear();
            unset($objHtmlMainPage);
            break;
        }
        $num_page++;
        $next_page_url = URL ."/search_resume.html".$a->href;
    } else {
        $objHtmlMainPage->clear();
        unset($objHtmlMainPage);
        break;
    }
    $objHtmlMainPage->clear();
    unset($objHtmlMainPage);

} while (true);

echo "Конец работы граббера.\n";



function process_resume($href_to_resume, $resumeID) {

    $ResumePage = new simple_html_dom(get_web_page($href_to_resume));

    $resume['jr_grab_src_alias'] = URL_ALIAS;
    $resume['jr_grab_src_id'] = $resumeID;

    $TableHead = new simple_html_dom($ResumePage->find("div.content_inner_resume div.advsearchshadow_head table", 0)->innertext);

    $date_publish = $ResumePage->find("div.date", 0)->plaintext;
    if (preg_match("/сегодня/", $date_publish)) {
        $resume['jr_date_publish'] = time();
    } else {
        $date_publish = trim(preg_replace("/г\./","", $date_publish));
        $date_publish = get_parsed_date($date_publish);
        if($date_publish)
            $resume['jr_date_publish'] = $date_publish;
        else {
            write_log(URL_ALIAS, "Резюме $resumeID: Ошибка парсинга Даты публикации\n");
            return false;
        }
    }

    $result = getFioAgeSalaryTitle($TableHead->innertext);
    if ($result != false)
        $resume = array_merge ($resume, $result);

    $blockHtml = $TableHead->find("td table div.column1", 0)->innertext;
    if (preg_match("/<span class=\"gray1\">Город:<\/span> ([^,]*),/", $blockHtml, $city))
        $resume['jr_city'] = $city[1];
    else {
        write_log(URL_ALIAS, "Резюме $resumeID: Ошибка парсинга Города\n");
        return false;
    }

    $properties = get_properties($TableHead->find("td table div.column2", 0)->innertext);
    $TableHead->clear();
    unset($TableHead);

    if (isset($properties['children']))
        $resume['jr_children'] = $properties['children'];
    if (isset($properties['marriage']))
        $resume['jr_marriage'] = $properties['marriage'];
    if (isset($properties['work_busy']))
        $resume['jr_work_busy'] = $properties['work_busy'];

    foreach (Array("phone", "email") as $value) {
        $href_to_image = "/$value.html?$resumeID";
        if($ResumePage->find("img[src=$href_to_image]", 0)) {
            $resume['jr_grab_has_' . $value] = "Y";
            $image = file_get_contents(URL .$href_to_image);
            if ($image != false) {
                if(file_put_contents(IMG_PATH ."/$value.$resumeID", $image ) == false) {
                    write_log(URL_ALIAS, "Резюме $resumeID: Ошибка сохранения картинок\n");
                    return false;
                }
            } else {
                write_log(URL_ALIAS, "Резюме $resumeID: Ошибка чтения картинки по url\n");
                return false;
            }
            usleep(250000);
        }
    }

    $Description = $ResumePage->find("div.pad60", 0);
    $resume['jr_driver'] = preg_match("'Водительские права'",$Description->plaintext)? "Y": "N";

    foreach ($Description->find("h3") as $h3) {
        $title = trim($h3->plaintext);
        switch ($title) {
            case "Опыт работы":
                $works = getWorks($h3->next_sibling());
                if ($works == false) {
                    write_log(URL_ALIAS, "Резюме $resumeID: Ошибка парсинга Опыта работы\n");
                    return false;
                }
                break;

            case "Образование":
                $educations = getEducation($h3->next_sibling(), $resumeID);
                if ($educations == false) {
                    write_log(URL_ALIAS, "Резюме $resumeID: Ошибка парсинга Образование\n");
                    return false;
                }
                if (isset($educations['jr_edu_plus'])) {
                    $resume['jr_edu_plus'] = $educations['jr_edu_plus'];
                    unset($educations['jr_edu_plus']);
                }
                break;

            case "О себе":
                $about = $h3->parent()->parent()->next_sibling()->find("td", 1);
                if ($about) {
                    if (!isset($resume['jr_about']))
                        $resume['jr_about'] = "";
                    $resume['jr_about'] .= $about->plaintext;
                }
                break;

            default:
                break;

        }
    }
    unset($Description);
//print_r($resume);die();
    $newResumeID = insert_in_db("job_resumes", $resume);

    if (!$newResumeID) {
        echo "Резюме $resumeID - ошибка записи в базу.\n";
        write_log(URL_ALIAS, "Резюме $resumeID: Ошибка записи в базу данных\n");
        return false;
    }
    if (is_array($works)) {
        foreach ($works as $work) {
            $work['jrw_jr_id'] = $newResumeID;
            insert_in_db("job_resumes_works", $work);
        }
    }
    if (is_array($educations)) {
        foreach ($educations as $education) {
            $education['jre_jr_id'] = $newResumeID;
            insert_in_db("job_resumes_education", $education);
        }
    }

    $ResumePage->clear();
    unset($ResumePage);
    return true;
}

function getFioAgeSalaryTitle($table_content) {
    $TableHead = new simple_html_dom($table_content);
    $result['jr_title'] = trim($TableHead->find("td h2", 0)->plaintext);

    $fio_age = split(",", trim($TableHead->find("td h4", 0)->plaintext));

    if (is_array($fio_age)) {
        $result['jr_fio'] = $fio_age[0];
        if (isset($fio_age[1]))
            $result['jr_age'] = intval ($fio_age[1]);
    }
    $html_column1 = $TableHead->find("td table div.column1", 0);
    if ($html_column1) {
        $html_salary = $html_column1->find("div.h3-inner", 0);
        if ($html_salary) {
            $buf = $html_salary->plaintext;
            if (preg_match("/договорная/", $buf))
                $result['jr_about'] = "З/п: договорная.";
            else {
                $span = $html_salary->find("span.float_l", 0);
                if ($span) {
                    $buf = preg_replace("/[^0-9]/", "", $span->plaintext);
                    $result['jr_salary'] = intval($buf);
                }
            }
        }
    }
    $TableHead->clear();
    unset($TableHead);

    if (!is_array($result))
        return false;

    foreach ($result as $key => $row)
        $result[$key] = html_entity_decode ($row);

    return $result;
}

function get_properties($str_html_table) {
    $Table = new simple_html_dom($str_html_table);
    $params = Array(
        'education' => 'Образование:',
        'children' => "Дети:",
        'marriage' => "Семейное положение:",
        'work_busy' => "Тип занятости:",
        'trips' => "Командировки:"
        );
    $props = split("<br>",trim($Table->innertext));
    $Table->clear();
    unset($Table);
    foreach ($props as $row) {
        if (empty($row) || strstr($row, "span") === false)
                continue;
        $TableRow = new simple_html_dom($row);
        $prop_key_rus = trim($TableRow->find("span",0)->plaintext);
        $key = array_search($prop_key_rus, $params);
        if ($key !== false) {
            $buf = split(":",trim($TableRow->plaintext));
            $parsed_prop[$key] = trim($buf[1]);
            unset($buf);
        }
        $TableRow->clear();
        unset($TableRow);
    }
    if (isset($parsed_prop['work_busy'])) {
        $work_busy[1] = "Полная";
        $work_busy[2] = "Частичная";
        $work_busy[4] = "Временная";
        $key = array_search($parsed_prop['work_busy'], $work_busy,true);
        if($key !== false)
            $parsed_prop['work_busy'] = $key;
        unset($work_busy);
    }
    if (isset($parsed_prop['trips']))
        $parsed_prop['trips'] = $parsed_prop['trips'] == "Да" ? "Y": "N";
    if (isset($parsed_prop['children']))
        $parsed_prop['children'] = $parsed_prop['children'] == "Есть" ? "Y": "N";
    if (isset($parsed_prop['marriage']))
        $parsed_prop['marriage'] = $parsed_prop['marriage'] == "В браке" ? "Y": "N";

    if(!is_array($parsed_prop) || empty($parsed_prop)) return false;
    return $parsed_prop;
}

function getWorks($Table) {
    foreach ($Table->find("tr") as $tr) {
        $td_date = $tr->find("td", 0);
        $buf = split("<br>",iconv("CP1251", "UTF-8", $td_date->innertext));
        if (is_array($buf) && !empty($buf)) {
            $date = split(" &ndash; ", trim($buf[0]));
            //print_r($date);
            $date_start = split('\.',$date[0]);
            if (is_array($date_start) && !empty($date_start))
                $work['jrw_date_start'] = date("Y-m-d", mktime(0, 0, 0, intval($date_start[0]), 01, intval($date_start[1])));

            $date_end = trim($date[1]);
            $present_day = "настоящее время";
            if ($date_end != $present_day) {
                $date_end = split('\.',$date_end);
                if (is_array($date_end) && !empty($date_end))
                    $work['jrw_date_fin'] = date("Y-m-d", mktime(0, 0, 0, intval($date_end[0]), 01, intval($date_end[1])));
            }
        }
        $td_info = $tr->find("td", 1);
        $work['jrw_position'] = trim($td_info->find("h4", 0)->plaintext);
        if(preg_match("/<\/h4>([^<]*)</", $td_info->outertext, $match))
            $work['jrw_company'] = trim ($match[1]);

        unset($match);
        if(preg_match("/Выполняемые обязанности:<\/span>(.*)/", $td_info->innertext, $match))
            $work['jrw_responsibility'] = trim ($match[1]);
        //print_r($work);
        if (is_array($work) && !empty($work))
            $works[] = $work;

        unset($work);
    }
    if (!is_array($works)) return true;
    return $works;
}

function getEducation($Table, $resumeID) {
    foreach ($Table->find("tr") as $tr) {
        if (preg_match("/([0-9]{4})/", $tr->find("td", 0)->plaintext, $year)) {
            $year = intval($year[1]);
            $education_type = trim($tr->find("td span", 0)->plaintext);
            $education_type_id = isExistEducationType($education_type);
            if ($education_type_id) {
                $education['jre_jet_id'] = $education_type_id;
                $education['jre_date_fin'] = date("Y-m-d", mktime(0, 0, 0, 1, 1, $year));
            } else {
                write_log(URL_ALIAS, "Резюме $resumeID: Ошибка определения типа образования из строки $education_type \n" );
                return false;
            }
            $education['jre_institution'] = trim($tr->find("td", 1)->plaintext);
        } else {
            if (preg_match("/Дополнительное образование/", $tr->find("td", 0)->plaintext))
                $educations['jr_edu_plus'] = iconv("CP1251", "UTF-8", $tr->find("td", 1)->innertext);
        }

        if (is_array($education) && !empty($education))
            $educations[] = $education;

        unset($education);
    }
    if (!is_array($educations)) return true;
    return $educations;
}

