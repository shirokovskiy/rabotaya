<?php
/*
 * Граббер резюме для http://spb.rabotavgorode.ru/Resumes/Search.aspx?s=1&RegionsIDs=10008%2c162%2c10003%2c10002%2c10001%2c10007%2c10006%2c10005%2c10004
 * Запуск из командной строки:
 * php -f ./grabbers/getResumesFromRabotavgorode.php
 */
require_once __DIR__ .'/grabber_function.php';

#http://spb.rabotavgorode.ru/Resumes/Search.aspx?s=1&RegionsIDs=10008%2c162%2c10003%2c10002%2c10001%2c10007%2c10006%2c10005%2c10004
#http://spb.rabotavgorode.ru/Resumes/Search.aspx?s=1&RegionsIDs=10008%2c162%2c10003%2c10002%2c10001%2c10007%2c10006%2c10005%2c10004&PageID=2

define('URL', "http://spb.rabotavgorode.ru");
define('URL_ALIAS', "rrvg");
define('IMG_PATH', dirname(__DIR__) ."/storage/files/images/resumes/contactimg");
define('COOKIEFILE', __DIR__."/cookie_".URL_ALIAS.".txt");

/**
 * Send Auth data
 */
$login = 'evgeniya2014@list.ru';
$passw = '24642464';

$PostData['DXScript'] = '1_145,1_81,1_137,1_80,1_116,1_99,1_106';
$PostData['__CALLBACKID'] = 'ctl00$cphBody$pcLogin';
$PostData['__CALLBACKPARAM'] = 'c0:Login';
$PostData['__EVENTARGUMENT'] = '';
$PostData['__EVENTTARGET'] = '';
$PostData['__EVENTVALIDATION'] = '/wEdAATmWowrKBSTcP99oCbfUAf/BkmgaiUzjUrIZ0CMsW0mTD7Wce6WxCLZNVQlUZKZ0dZCqQtpmLb7dJmMUJSG1/eYkS0sn28vsdY+5MG0BvpvGCLlrRXJ938CUuVZYfHbbew=';
$PostData['__VIEWSTATE'] = '/wEPDwULLTE0MzMwMDk1MjdkGAEFHl9fQ29udHJvbHNSZXF1aXJlUG9zdEJhY2tLZXlfXxYBBR5jdGwwMCRjcGhCb2R5JHBjTG9naW4kYnRuTG9naW7IW5u9OViNbMduupBOxAOZg5eIk48WLnpwWB88ZAYgwQ==';
$PostData['ctl00$cphBody$pcLogin$hfAuth$I'] = '12|#|#';
$PostData['ctl00$cphBody$pcLogin$txtLogin'] = $login;
$PostData['ctl00$cphBody$pcLogin$txtLogin$CVS'] = '';
$PostData['ctl00$cphBody$pcLogin$txtPassword'] = $pass;
$PostData['ctl00$cphBody$pcLogin$txtPassword$CVS'] = '';

$Url2Auth = URL.'/Include/Login.aspx';

echo "Авторизация ..........................................\n";

$Result = SendPost($Url2Auth, $PostData);
//$fp = fopen(__DIR__.'/AfterAuth.htm', "w");
//fwrite($fp, $Result);
//fclose($fp);

#echo "Загрузка страницы ..........................................\n";

/**
 * Read pages after Auth
 */
//$filter = "s=1&RegionsIDs=10008%2c162%2c10003%2c10002%2c10001%2c10007%2c10006%2c10005%2c10004";
//$filter = "s=1&RegionsIDs=10002";
//$next_page_url = URL ."/Resumes/Search.aspx?".$filter;

//$Result = Read($next_page_url);

//$fp = fopen(__DIR__.'/AfterRead.htm', "w");
//fwrite($fp, $Result);
//fclose($fp);

$Result = file_get_contents(__DIR__.'/AfterRead.htm');

if (!is_dir(IMG_PATH)) {
    if (!mkdir(IMG_PATH, 0775, true)) {
        die('CANT CREATE DIR FOR IMAGES');
    }
}

$file_path = __DIR__ ."/log-".URL_ALIAS .".log";
if (file_exists($file_path))
    unlink($file_path);

//$filter = "s=1&RegionsIDs=10008%2c162%2c10003%2c10002%2c10001%2c10007%2c10006%2c10005%2c10004";
//$next_page_url = URL ."/Resumes/Search.aspx?".$filter;
$num_page = 1;
do {
    echo "Загрузка страницы № $num_page со списком резюме ...\n";
//    $objHtmlMainPage = new simple_html_dom(get_web_page($next_page_url));
    $objHtmlMainPage = new simple_html_dom($Result);
    if (!$objHtmlMainPage) {
        echo "GRABBER ERROR";
        die();
    } else {
//        $objHtmlMainPage->save('ResRvG.'.date('YmdHis').'.htm');
        $count = 0;
        /**
         * Получить ссылки на резюме
         */
        $resumes_links = $objHtmlMainPage->find("table.resumesList a.title");
        if(!empty($resumes_links)) {
            $count = count($resumes_links);
        }
        echo "Успешно.\nНа странице $count резюме.\n";

        $statistics['all'] = 0;
        $statistics['false'] = 0;
        $statistics['new'] = 0;
        $statistics['old'] = 0;

        foreach ($resumes_links as $link) {
            if (preg_match('/http/', $link->href)) {
                echo "Чтение страницы ".$link->href."\n";
                $ResumePage = Read($link->href);

                file_put_contents(__DIR__.'/ResumePage.htm', $ResumePage);

                die();


                $ResumePage = new simple_html_dom(get_web_page($link->href));
                echo $link->href. "\n";

                file_put_contents(__DIR__.'/ResumePage.htm', $ResumePage);

                die();
            }
        }

        //$resumes_links->save('resumes_links.'.date('YmdHis').'.htm');

//        file_put_contents(__DIR__.'/TEST.htm', $resumes_links);

        die();
    }

} while (true);

echo "Конец работы граббера.\n";


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// http://www.rabotavgorode.ru/Include/Login.aspx
function SendPost($url,$PostData){
    $UserAgent = "Mozilla/4.0 (Windows; U; Windows NT 5.0; En; rv:1.8.0.2) Gecko/20070306 Firefox/1.0.0.4";
    $UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0";
    $UserAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36";

    $Headers[] = "Content-Type: application/x-www-form-urlencoded; charset=utf-8";

    $ch = curl_init();
    #if(strtolower((substr($url,0,5))=='https')) { // если соединяемся с https
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    #}
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0); // пустые заголовки
    // откуда пришли на эту страницу
    curl_setopt($ch, CURLOPT_REFERER, $url);
    curl_setopt($ch, CURLOPT_VERBOSE, 0); // 1 = cURL будет выводить подробные сообщения о всех производимых действиях
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // следовать за редиректами
//    curl_setopt($ch, CURLOPT_POSTFIELDS, $PostData);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE=%2FwEPDwULLTE0MzMwMDk1MjdkGAEFHl9fQ29udHJvbHNSZXF1aXJlUG9zdEJhY2tLZXlfXxYBBR5jdGwwMCRjcGhCb2R5JHBjTG9naW4kYnRuTG9naW7IW5u9OViNbMduupBOxAOZg5eIk48WLnpwWB88ZAYgwQ%3D%3D&ctl00%24cphBody%24pcLogin%24hfAuth%24I=12%7C%23%7C%23&ctl00%24cphBody%24pcLogin%24txtLogin=evgeniya2014%40list.ru&ctl00%24cphBody%24pcLogin%24txtLogin%24CVS=&ctl00%24cphBody%24pcLogin%24txtPassword=24642464&ctl00%24cphBody%24pcLogin%24txtPassword%24CVS=&DXScript=1_145%2C1_81%2C1_137%2C1_80%2C1_116%2C1_99%2C1_106&__CALLBACKID=ctl00%24cphBody%24pcLogin&__CALLBACKPARAM=c0%3ALogin&__EVENTVALIDATION=%2FwEdAATmWowrKBSTcP99oCbfUAf%2FBkmgaiUzjUrIZ0CMsW0mTD7Wce6WxCLZNVQlUZKZ0dZCqQtpmLb7dJmMUJSG1%2FeYkS0sn28vsdY%2B5MG0BvpvGCLlrRXJ938CUuVZYfHbbew%3D");
    curl_setopt($ch, CURLOPT_USERAGENT, $UserAgent);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //сохранять полученные COOKIE в файл
    curl_setopt($ch, CURLOPT_COOKIEJAR, COOKIEFILE);
    curl_setopt($ch, CURLOPT_COOKIEFILE, COOKIEFILE);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $Headers);

    $result=curl_exec($ch);
    curl_close($ch);
    return $result;
}


// чтение страницы после авторизации
function Read($url) {
    $UserAgent = "Mozilla/4.0 (Windows; U; Windows NT 5.0; En; rv:1.8.0.2) Gecko/20070306 Firefox/1.0.0.4";
    $UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0";
    $UserAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    // откуда пришли на эту страницу
    curl_setopt($ch, CURLOPT_REFERER, $url);
    //запрещаем делать запрос с помощью POST и соответственно разрешаем с помощью GET
    curl_setopt($ch, CURLOPT_POST, 0);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    //отсылаем серверу COOKIE полученные от него при авторизации
    curl_setopt($ch, CURLOPT_COOKIEJAR, COOKIEFILE);
    curl_setopt($ch, CURLOPT_COOKIEFILE, COOKIEFILE);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($ch, CURLOPT_USERAGENT, $UserAgent);
    curl_setopt($ch, CURLOPT_VERBOSE, 0); // Не выводить доп.инфо
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Вернуть результат в виде строки

    if( ! $result = curl_exec($ch))
    {
        trigger_error(curl_error($ch));
    }

    curl_close($ch);
    return $result;
}
