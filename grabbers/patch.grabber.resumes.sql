ALTER TABLE  `job_resumes` ADD  `jr_grab_src_alias` VARCHAR( 7 ) NULL COMMENT  'Сокращенное название сайта-источника',
ADD  `jr_grab_src_id` VARCHAR( 10 ) NULL COMMENT  'ID резюме на сайте-источнике',
ADD  `jr_grab_has_email` ENUM(  'N',  'Y' ) NULL DEFAULT  'N' COMMENT  'Есть ли картинка с email',
ADD  `jr_grab_has_phone` ENUM(  'N',  'Y' ) NULL DEFAULT  'N' COMMENT  'Есть ли картинка с телефоном';

ALTER TABLE  `job_resumes` ADD UNIQUE (
`jr_grab_src_alias` ,
`jr_grab_src_id`
);

ALTER TABLE  `job_resumes_works` CHANGE  `jrw_date_fin`  `jrw_date_fin` DATE NULL COMMENT  'уволился';

INSERT INTO `job_education_types` (`jet_id` ,`jet_title`) VALUES (NULL ,  'Повышение квалификации, курсы');
