<?php
/*
 * Граббер резюме для http://jobsmarket.ru/
 * Запуск из командной строки:
 * php -f ./grabbers/getResumesFromJobsMarket.php
 *
 * Tips & Tricks
 *      Записать объект в файл $objHtmlMainPage->save('ResRvG.'.date('YmdHis').'.htm');
 *      Если объект simple_html_dom_node то можно сделать ->__toString() чтобы посмотреть код
 *      У объекта ссылки:
 *          свойство $link->href        чтобы узнать адрес
 *          метод $link->text()         чтобы узнать текст ссылки
 *
 * Resume Simple    http://
 * Resume Authed    http://
 * Vacancies        http://
 */

require_once __DIR__ .'/grabber_function.php';
define('URL', "http://www.jobsmarket.ru");
define('URL_ALIAS', "jmkt");
define('COOKIEFILE', __DIR__."/cookie_".URL_ALIAS.".txt");
define('IMG_PATH', dirname(__DIR__) ."/storage/files/images/resumes/contactimg");
define('READ_FLAGS_DIR', __DIR__.'/logs/read_resumes');
define('READ_FLAG_FILE', 'resume.counter');
define('UPPER_LIMIT', 30000000);
define('DIR_PREFIX', 'r');

umask(0002);

/**
 * Создадим директорию для картинок, если ещё нет
 */
if (!is_dir(IMG_PATH)) {
    if (!mkdir(IMG_PATH, 0775, true)) {
        die('CANT CREATE DIR FOR IMAGES');
    }
}

/**
 * Send Auth data
 *
 * Заполнить логин и пароль соответствующими данными с сайта, если есть авторизация
 */
$login = 'gabriella2014';
$passw = 'alleirbag';

    /**
 * Заполнить в соответствии с Firebug POST-данными
 */
$PostData['str_login'] = $login;
$PostData['str_password'] = $passw;
$PostData['input_user_form'] = 1;
$PostData['input_user.x'] = rand(1,58);
$PostData['input_user.y'] = rand(1,18);

$Url1 = URL.'/index.php';
$Url2 = URL.'/index.php?get_page=70&view=8869117&view_mode=print';

//$login = $passw = ''; // DEBUG

/**
 * Процесс проверки авторизации
 */
if (!empty($login) && !empty($passw) && !file_exists(COOKIEFILE)) {
    echo "Запрос авторизации...................................\n";
    $Result = SendPost($Url1, $PostData);
    write_file($Result, '_Url1.htm');
    echo "Ответ получен, см.лог\n";
}

/**
 * Параметры запроса страницы первоначальной
 */
$num_page = UPPER_LIMIT - 1;

/**
 * Определить автоматически на каком месте остановились
 */
if (file_exists(READ_FLAGS_DIR.'/'.READ_FLAG_FILE)) {
    $num_page_string = file_get_contents(READ_FLAGS_DIR.'/'.READ_FLAG_FILE);
    $num_page_string = trim($num_page_string);
    if (!empty($num_page_string) && intval($num_page_string) > 0 && intval($num_page_string) <= UPPER_LIMIT) {
        $num_page = intval($num_page_string);
    }
} else {
    if (!is_dir(READ_FLAGS_DIR)) {
        mkdir(READ_FLAGS_DIR, 0777, true);
    }
    file_put_contents(READ_FLAGS_DIR.'/'.READ_FLAG_FILE, UPPER_LIMIT);
}

$resume = 0;

/**
 * Начать постраничный цикл
 */
do {
    if ($num_page < 0 || $num_page > UPPER_LIMIT) {
        echo "num_page out of date \n";
        break;
    }

    if (isReadBefore($num_page)) {
        $num_page--;
        continue;
    }

    $Url2 = URL.'/?view_resume='.$num_page;
    write_log(URL_ALIAS, "Загрузка страницы $Url2");
    $Result = Read($Url2, $Url1);
    $Result = removeTrash($Result);

    if (!preg_match("/Название Резюме/im", $Result)) {
        if (!file_exists(COOKIEFILE)) {
            if (!AuthPage($Url1, $PostData)) {
                write_log(URL_ALIAS, "Авторизация не получилась\n");
                unlink(COOKIEFILE);
                break;
            }
        }

        // set only for BAD pages
        setIReadIt($num_page);
    } else {
        $Url1 = $Url2;

        if (!is_object($SiteUser)) {
            $SiteUser = new SiteUser();
        }

        /**
         * Обработка резюме
         */
        $objHtmlMainPage = new simple_html_dom($Result);
        if (!$objHtmlMainPage) {
            die("GRABBER ERROR");
        }

        /**
         * Получить только определённые ряды (с двумя ячейками) из таблицы
         */
        $arrRows = $objHtmlMainPage->find("table.fcontent tr.lgrey");
        if (is_array($arrRows) && !empty($arrRows)) {
            $arrDB = $arrWorks = $arrEdu = array();
            $edu = $work = 0;
            $isValidResume = true;
            foreach($arrRows as $Row) {
                $arrTDs = $Row->find("td");
                if (is_array($arrTDs) && !empty($arrTDs)) {
                    # проверить чтобы ячейки было 2 шт.
                    if (count($arrTDs) == 2) {
                        if (is_object($arrTDs[0])) {
                            $strTitle = trim($arrTDs[0]->text());
                            if (!empty($strTitle)) {
                                $strText = trim($arrTDs[1]->text());
                            }

//                            write_log(URL_ALIAS, "Title: ".$strTitle.", Text:".$strText);

                            if (preg_match("/Название Резюме/im", $strTitle)) {
                                if (!empty($strText)) {
                                    $arrDB['title'] = mysql_real_escape_string(ucfirst($strText));
                                } else {
                                    $isValidResume = false;
                                    write_log(URL_ALIAS, "Название Резюме не определено");
                                    break; /**** ОБЯЗАТЕЛЬНОЕ ПОЛЕ ****/
                                }
                            }

                            if (preg_match("/Фамилия/im", $strTitle)) {
                                if (!empty($strText)) {
                                    if (preg_match("/После организации Контакта!/im", $strText)) {
                                        $isValidResume = false;
                                        write_log(URL_ALIAS, "Фамилия не определёна");
                                        break; /**** ОБЯЗАТЕЛЬНОЕ ПОЛЕ ****/
                                    }
                                    $arrDB['fio'] .= mysql_real_escape_string($strText).' ';
                                }
                            }

                            if (preg_match("/Имя/m", $strTitle)) {
                                if (!empty($strText) && !preg_match("/После организации Контакта!/im", $strText)) {
                                    $arrDB['fio'] .= mysql_real_escape_string($strText).' ';
                                }
                            }

                            if (preg_match("/Отчество/m", $strTitle)) {
                                if (!empty($strText) && !preg_match("/После организации Контакта!/im", $strText)) {
                                    $arrDB['fio'] .= mysql_real_escape_string($strText);
                                }
                            }

                            if (preg_match("/Пол/m", $strTitle)) {
                                if (!empty($strText)) {
                                    switch ($strText) {
                                        case 'мужской':
                                            $arrDB['sex'] = 'male';
                                            break;
                                        case 'женский':
                                            $arrDB['sex'] = 'female';
                                            break;
                                        default: break;
                                    }
                                }
                            }

                            if (preg_match("/Возраст/im", $strTitle)) {
                                if (!empty($strText)) {
                                    $arrDB['age'] .= mysql_real_escape_string($strText);
                                }
                            }

                            if (preg_match("/Регион/im", $strTitle)) {
                                if (!empty($strText)) {
                                    $cityId = $SiteUser->getCityId($strText);

                                    if (empty($cityId)) {
                                        $arrDB['city'] = mysql_real_escape_string($strText);
                                    } else {
                                        $arrDB['city_id'] = $cityId;
                                    }
                                }
                            }

                            if (preg_match("/Телефон/im", $strTitle)) {
                                if (!empty($strText) && !preg_match("/После организации Контакта!/im", $strText)) {
                                    $arrDB['phone'] = mysql_real_escape_string($strText);
                                }
                            }

                            if (preg_match("/Е-mail/im", $strTitle)) {
                                if (!empty($strText) && $objUtils->isValidEmail($strText)) {
                                    $arrDB['email'] = mysql_real_escape_string($strText);
                                } else {
                                    $isValidResume = isset($arrDB['phone']);
                                    write_log(URL_ALIAS, "Email не определён");
                                    break; /**** ОБЯЗАТЕЛЬНОЕ ПОЛЕ ****/
                                }
                            }

                            if (preg_match("/Адрес/im", $strTitle)) {
                                if (!empty($strText)) {
                                    $strText = str_replace("\r\n", '', $strText);
                                    $arrDB['about'] .= mysql_real_escape_string($strText)."\n";
                                }
                            }

                            if (preg_match("/Cреднемесячный доход/im", $strTitle)) {
                                if (!empty($strText)) {
                                    if (preg_match("/usd/im", $strText)) {
                                        $strText = preg_replace("/usd/im", '', $strText);
                                        $strText = intval($strText) * 35;
                                    }
                                    $arrDB['salary'] = mysql_real_escape_string($strText);
                                }
                            }

                            if (preg_match("/Учебное заведение/im", $strTitle)) {
                                if (!empty($strText)) {
                                    $arrEdu[$edu]['institution'] = mysql_real_escape_string($strText)."\n";
                                }
                            }

                            if (preg_match("/Тип образования/im", $strTitle)) {
                                if (!empty($strText)) {
                                    $edu++;
                                    switch ($strText) {
                                        case 'Основное образование':
                                            $arrEdu[$edu]['jet_id'] = 3;
                                            break;
                                        case 'Дополнительное образование':
                                            $arrEdu[$edu]['jet_id'] = 5;
                                            break;
                                        default:
                                            write_log(URL_ALIAS, "New education: ".$strText);
                                            break;
                                    }
                                }
                            }

                            if (preg_match("/Факультет/im", $strTitle)) {
                                if (!empty($strText)) {
                                    $arrEdu[$edu]['faculty'] = mysql_real_escape_string($strText)."\n";
                                }
                            }

                            if (preg_match("/Специальность/im", $strTitle)) {
                                if (!empty($strText)) {
                                    $arrEdu[$edu]['spec'] = mysql_real_escape_string($strText)."\n";
                                }
                            }

                            if (preg_match("/Квалификация/im", $strTitle)) {
                                if (!empty($strText)) {
                                    $arrEdu[$edu]['qualification'] = mysql_real_escape_string($strText)."\n";
                                }
                            }

                            if (preg_match("/Период обучения/im", $strTitle)) {
                                if (!empty($strText)) {
                                    $strText = substr($strText, strpos($strText, '('));
                                    if (!empty($strText)) {
                                        $arrDates = explode(' ', $strText);
                                        if (is_array($arrDates) && !empty($arrDates)) {
                                            if (count($arrDates) == 4) {
                                                #
                                                $month = $year = 0;
                                                if (!empty($arrDates[0])) {
                                                    $month = array_search($arrDates[0], $objUtils->arrMonth);
                                                }
                                                if (!empty($arrDates[1])) {
                                                    $year = str_replace('г.','',$arrDates[1]);
                                                }

                                                if ($month && $year) {
                                                    $arrEdu[$edu]['date_start'] = $year.'-'.$month.'-01';
                                                }

                                                $month = $year = 0;
                                                if (!empty($arrDates[2])) {
                                                    $month = array_search($arrDates[2], $objUtils->arrMonth);
                                                }
                                                if (!empty($arrDates[3])) {
                                                    $year = str_replace('г.','',$arrDates[3]);
                                                }

                                                if ($month && $year) {
                                                    $arrEdu[$edu]['date_fin'] = $year.'-'.$month.'-01';
                                                }
                                            } elseif (count($arrDates) == 2) {
                                                #
                                                $month = $year = 0;
                                                if (!empty($arrDates[0])) {
                                                    $month = array_search($arrDates[0], $objUtils->arrMonth);
                                                }
                                                if (!empty($arrDates[1])) {
                                                    $year = str_replace('г.','',$arrDates[1]);
                                                }

                                                if ($month && $year) {
                                                    $arrEdu[$edu]['date_start'] = $year.'-'.$month.'-01';
                                                }
                                            } else {
                                                write_log(URL_ALIAS, "New period of education: ".$strText);
                                            }
                                        }
                                    }
                                }
                            }

                            if (preg_match("/Наименование курса/im", $strTitle)) {
                                if (!empty($strText)) {
                                    $arrEdu[$edu]['coursename'] = mysql_real_escape_string($strText);
                                }
                            }

                            if (preg_match("/Сертификат/im", $strTitle)) {
                                if (!empty($strText)) {
                                    $arrEdu[$edu]['certificate'] = mysql_real_escape_string($strText);
                                }
                            }

                            if (preg_match("/Наименование организации/im", $strTitle)) {
                                if (!empty($strText)) {
                                    $work++;
                                    $arrWorks[$work]['company'] = mysql_real_escape_string($strText);
                                }
                            }

                            if (preg_match("/Должность/im", $strTitle)) {
                                if (!empty($strText)) {
                                    $arrWorks[$work]['position'] = mysql_real_escape_string($strText);
                                }
                            }

                            if (preg_match("/Функциональные обязанности/im", $strTitle)) {
                                if (!empty($strText)) {
                                    $strText = preg_replace("/\.+/", ".", $strText);
                                    $arrWorks[$work]['responsibility'] = mysql_real_escape_string($strText);
                                }
                            }

                            if (preg_match("/Дата начала\/окончания/im", $strTitle)) {
                                if (!empty($strText) && strpos($strText, '-')) {
                                    $arrDates = explode('-', $strText);
                                    if (is_array($arrDates) && !empty($arrDates) && count($arrDates) == 2) {
                                        $arrDateStart = explode(' ', $arrDates[0]);
                                        $month = $year = 0;
                                        if (is_array($arrDateStart) && !empty($arrDateStart) && count($arrDateStart) == 2) {
                                            if (!empty($arrDateStart[0])) {
                                                $month = array_search($arrDateStart[0], $objUtils->arrMonth);
                                            }
                                            if (!empty($arrDateStart[1])) {
                                                $year = str_replace('г.','',$arrDateStart[1]);
                                            }

                                            if ($month && $year) {
                                                $arrWorks[$work]['date_start'] = $year.'-'.$month.'-01';
                                            }
                                        }

                                        $arrDateEnd = explode(' ', $arrDates[0]);
                                        $month = $year = 0;
                                        if (is_array($arrDateEnd) && !empty($arrDateEnd) && count($arrDateEnd) == 2) {
                                            if (!empty($arrDateEnd[0])) {
                                                $month = array_search($arrDateEnd[0], $objUtils->arrMonth);
                                            }
                                            if (!empty($arrDateEnd[1])) {
                                                $year = str_replace('г.','',$arrDateEnd[1]);
                                            }

                                            if ($month && $year) {
                                                $arrWorks[$work]['date_fin'] = $year.'-'.$month.'-01';
                                            }
                                        }
                                    } else {
                                        write_log(URL_ALIAS, "New period of work: ".$strText);
                                    }
                                }
                            }
                        } else {
                            // error
                        }
                    }
                }
            }
        }


        /**
         * Получить только определённые ряды (с двумя ячейками) из таблицы
         */
        $arrRows = $objHtmlMainPage->find("table.fcontent tr.orange");
        if (is_array($arrRows) && !empty($arrRows)) {
            foreach ($arrRows as $Row) {
                $strText = trim($Row->text());

                if (!empty($strText)) {
                    $strText = str_replace("\r\n", ' ', $strText);
                    $arrDB['about'] .= mysql_real_escape_string($strText)."\n";
                }
            }
        }

        if (is_array($arrDB) && !empty($arrDB) && $isValidResume) {
            $strSqlQuery = "SELECT * FROM `job_resumes` WHERE `jr_email` LIKE '".$arrDB['email']."' AND `jr_title` LIKE '".$arrDB['title']."' AND `jr_grab_src_alias` = '".URL_ALIAS."'";
            $arrExistResume = $objDb->fetch($strSqlQuery);

            if (is_array($arrExistResume) && !empty($arrExistResume)) {
                write_log(URL_ALIAS, "Резюме уже в БД");
            } else {
                $strSqlQuery = "INSERT INTO `job_resumes` SET `jr_date_publish` = UNIX_TIMESTAMP('".date('Y-m-d', strtotime('-1 month'))."'),";

                $arrDB['grab_src_id'] = $num_page;
                $arrDB['grab_src_alias'] = URL_ALIAS;
                $arrDB['status'] = 'Y';
                $arrSqlQuery = array();
                foreach ($arrDB as $key => $value) {
                    $arrSqlQuery[] = "`jr_$key` = '$value'";
                }

                $strSqlQuery .= implode(',', $arrSqlQuery);

                if (!$objDb->query($strSqlQuery)) {
                    # sql error
                    echo "MySQL error $strSqlQuery \n";
                } else {
                    $resumeID = $objDb->insert_id();

                    if (is_array($arrEdu) && !empty($arrEdu)) {
                        foreach ($arrEdu as $arr) {
                            #
                            $strSqlQuery = "INSERT INTO `job_resumes_education` SET `jre_jr_id` = $resumeID,";
                            $arrSqlQuery = array();
                            foreach ($arr as $key => $value) {
                                $arrSqlQuery[] = "`jre_$key` = '$value'";
                            }
                            $strSqlQuery .= implode(',', $arrSqlQuery);

                            if (!$objDb->query($strSqlQuery)) {
                                # sql error
                                echo "MySQL error $strSqlQuery \n";
                            }
                        }
                    }

                    if (is_array($arrWorks) && !empty($arrWorks)) {
                        foreach ($arrWorks as $arr) {
                            #
                            $strSqlQuery = "INSERT INTO `job_resumes_works` SET `jrw_jr_id` = $resumeID,";
                            $arrSqlQuery = array();
                            foreach ($arr as $key => $value) {
                                $arrSqlQuery[] = "`jrw_$key` = '$value'";
                            }
                            $strSqlQuery .= implode(',', $arrSqlQuery);

                            if (!$objDb->query($strSqlQuery)) {
                                # sql error
                                echo "MySQL error $strSqlQuery \n";
                            }
                        }
                    }
                }
            }
        }

        if ($isValidResume) {
            $resume++;
            setIReadIt($num_page, true);
            write_log(URL_ALIAS, "Resume #".$resume." with ID: ".$num_page);
            write_log(URL_ALIAS, print_r($arrDB, true));

            if (is_array($arrEdu) && !empty($arrEdu)) {
                write_log(URL_ALIAS, print_r($arrEdu, true));
            }

            if (is_array($arrWorks) && !empty($arrWorks)) {
                write_log(URL_ALIAS, print_r($arrWorks, true));
            }

            write_file($Result, '_Url2_A_'.$num_page.'_'.time().'.htm');
        } else {
            // set only for BAD pages
            setIReadIt($num_page);
        }
    }

    $num_page--;

//    if ($resume > 2) break;
//    $mseconds = rand(100,30000);
//    usleep($mseconds);
} while (true);

echo "Конец работы граббера.", date("H:i:s d.m.Y") ,"\n";

function AuthPage($p1, $p2)
{
    write_log(URL_ALIAS, "Запрос авторизации.....\n");
    $Result = SendPost($p1, $p2);
    $Result = removeTrash($Result);
    write_file($Result, '_Url1_'.date('YmdHis').'.htm');
    write_log(URL_ALIAS, "Ответ получен\n");

    if (!preg_match("/Турандот Виктор Михайлович/im", $Result)) {
        /* авторизация не прошла :( */
        write_log(URL_ALIAS, "Запрос авторизации не удался!!! ***\n");
        return false;
    }

    return true;
}

