ALTER TABLE  `job_vacancies` ADD  `jv_src_url_alias` VARCHAR( 7 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT  'Сокращенное название сайта-источника',
ADD  `jv_src_id` INT UNSIGNED NULL COMMENT  'ID вакансии на сайте-источнике';

ALTER TABLE `job_vacancies` ADD UNIQUE (
  `jv_src_url_alias` ,
  `jv_src_id`
);
