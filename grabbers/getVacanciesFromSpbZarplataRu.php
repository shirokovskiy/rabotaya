<?php
/*
 * Граббер вакансий для http://spb.zarplata.ru/
 * Запуск из командной строки:
 * php -f ./grabbers/getVacanciesFromSpbZarplataRu.php
 */
require_once __DIR__ .'/grabber_function.php';

define('URL', "http://spb.zarplata.ru");
define('URL_ALIAS', "zrplt");
define('COOKIEFILE', __DIR__."/cookie_zarplataru.txt");

$file_path = __DIR__ ."/log-".URL_ALIAS .".log";
if (file_exists($file_path))
    unlink($file_path);

echo "Подключение к сайту ...\n";
$objHtmlMainPage = new simple_html_dom(get_web_page(URL ."/vacancy/"));
if (!$objHtmlMainPage) {
    echo "GRABBER ERROR";
    die();
}
echo "Успешно.\n\n Чтение католога направлений ...\n";
foreach ($objHtmlMainPage->find("form#aspnetForm div.wrap p.cat-list") as $elem_direction) {
    $direction_name_RUS = $elem_direction->find("a",0)->plaintext;
    //if ($direction_name_RUS != "Образование, наука")        continue;
    echo "Обработка направления '$direction_name_RUS' ...\n";
    $IDs = getIDs($direction_name_RUS);
    if ($IDs == false) {
        write_log(URL_ALIAS, "Направление $direction_name_RUS не обработано. Ошибка получения списка ID вакансий");
        continue;
    }

    //echo "Найдено ".count($IDs) ." вакансий.\n Обработка.....\n";
    $count_vacancies = 0;
    $problem_vacancies = 0;

    foreach ($IDs as $ID) {
        if(!isExistVacancy(URL_ALIAS, $ID)) {
            $count_vacancies++;
            if(process_vacancy($ID) == false) {
                $problem_vacancies++;
                $count_vacancies --;
            }
        }
    }
    $statictic = "";
    if($problem_vacancies > 0)
        $statictic .= "Пропущено  $problem_vacancies  вакансий. Смотри логфайл.\n";
    if($count_vacancies > 0)
        $statictic .= "В базу записано $count_vacancies новых вакансий.\n";
    else
        $statictic = "Новых вакансий не обнаружено\n";
    echo $statictic ."\n";

}
if (file_exists(COOKIEFILE))
    unlink (COOKIEFILE);
$objHtmlMainPage->clear();
unset($objHtmlMainPage);

function getIDs($direction_name_RUS) {
    if (file_exists(COOKIEFILE))
        unlink (COOKIEFILE);
    $fp = fopen(COOKIEFILE, "w");
    fwrite($fp,"");
    fclose($fp);

    $get_url = URL ."/workman/result.aspx?keywords=" .
            str_replace("-", ",", rawurlencode(str_replace(",", "-", $direction_name_RUS)));
    send_get($get_url);

    $post_url = URL ."/WebServices/AnnouncementSearch.asmx/GetFavorites";
    $refer = $get_url;
    $headers[] = "Content-Type: application/json; charset=utf-8";
    send_post($post_url, "{}", $refer, $headers);

    $post_url = URL ."/WebServices/AnnouncementSearch.asmx/SearchVacancy1";
    $post['type'] = "vacancy";
    $post['PageSize'] = "30";
    $post['Keywords'] = $direction_name_RUS;
    $post['Locations'] = "2";
    $post['Period'] = "0";
    $post['SortField'] = "SaveDate";
    $post['SortDirection'] = "Descending";
    $post_data = json_encode(Array('query' => http_build_query($post)));
    $response = json_decode(send_post($post_url, $post_data, $refer, $headers), true);
    //unlink(COOKIEFILE);
    if (!isset($response['d']['IDs'])) return false;
    return $response['d']['IDs'];
}

function process_vacancy($vacancy_id) {
    $get_url = URL ."/v$vacancy_id/";
    $html_vacancy = send_get($get_url);
    if($html_vacancy == false) {
        write_log(URL_ALIAS, "($get_url) - Ошибка запроса страницы c вакансией");
        return false;
    }

    $objHtmlVacancy = new simple_html_dom($html_vacancy);
    if (!$objHtmlVacancy) {
        write_log(URL_ALIAS, "($get_url) - Ошибка обработки контента для вакансии");
        return false;
    }
    $vacancy['jv_status'] = "Y";
    $vacancy['jv_src_url_alias'] = URL_ALIAS;
    $vacancy['jv_src_id'] = $vacancy_id;

    $objTitleVac = $objHtmlVacancy->find("#announcementTitle", 0);
    if ($objTitleVac) {
        $vacancy['jv_title'] = $objTitleVac->plaintext;
        $objSalary = $objTitleVac->find("span", 0);
        if ($objSalary) {
            if (preg_match("/^(.+),[\s]+<span/u", $objTitleVac->innertext, $title))
                $vacancy['jv_title'] = preg_replace("/<br\/>$/", "", trim($title[1]));

            $vacancy['jv_salary'] = preg_replace("/[\s]+/u", " ", trim($objSalary->plaintext));
            $salary_parsed = get_parsed_salary($vacancy['jv_salary']);
            if (isset($salary_parsed['from']))
                $vacancy['jv_salary_from'] = $salary_parsed['from'];
            if (isset($salary_parsed['to']))
                $vacancy['jv_salary_to'] = $salary_parsed['to'];
        }
    }    else {
        write_log(URL_ALIAS, "Не обнаружен идентификатор заголовка для $get_url");
        return false;
    }

    $objVacancyProps = $objHtmlVacancy->find("div.candidate-props dl");
    if(!$objVacancyProps) {
        write_log(URL_ALIAS, "($get_url) - Отсутствует перечень основных свойств вакансии");
        return false;
    }

    if ($objHtmlVacancy->find("span#containerEMail", 0))
        $vacancy['jv_email'] = get_contact_fio($vacancy_id, $get_url, $html_vacancy);

    if ($objHtmlVacancy->find("#phoneDetailContainer", 0)) {
        $vacancy['jv_phone'] = trim(str_replace("<br>", ";", $objHtmlVacancy->find("#phoneDetailContainer", 0)->plaintext));
        $vacancy['jv_phone'] = preg_replace("/[\s]+/", " ", $vacancy['jv_phone']);
    }

    foreach ($objVacancyProps as $row) {
        $field = $row->find("dt", 0)->plaintext;
        $htmlDD = $row->find("dd", 0);
        switch ($field) {
            case "Работодатель":
            case "Кадровое агентство":
                $company_title = trim($htmlDD->plaintext);
                $company_id = isExistCompany($company_title);
                if (!$company_id && $htmlDD->find("a", 0)) {
                    $company_href = $htmlDD->find("a", 0)->href;
                    $company_type = $field == "Кадровое агентство" ? "recrut":"direct";
                    $company_id = parse_and_insert_new_company($company_title, $company_href, $company_type);
                    if (!$company_id) {
                        write_log(URL_ALIAS, "($get_url) - Ошибка парсинга работодателя $company_href");
                        return false;
                    }
                }
                if ($company_id)
                    $vacancy['jv_jc_id'] = $company_id;
                break;

            case "Контактное лицо":
                $vacancy['jv_contact_fio'] = trim($htmlDD->plaintext);
                break;

            case "Место работы":
                $htmlDD->plaintext = preg_replace("/:.+/", "", $htmlDD->plaintext);
                $work_place = explode(";", $htmlDD->plaintext);
                if (is_array($work_place) && !empty($work_place))
                    $vacancy['jv_city'] = trim($work_place[0]);
                break;

            case "Образование":
                $vacancy['jv_jet_id'] = isExistEducationType(trim($htmlDD->plaintext));
                if ($vacancy['jv_jet_id'] == false)
                    unset($vacancy['jv_jet_id']);
                break;

            case "Занятость":
                $work_busy = prepare_work_busy_type( trim($htmlDD->plaintext));
                $vacancy['jv_jw_id'] = isExistWorkBusyType($work_busy);
                if ($vacancy['jv_jw_id'] == false)
                    unset($vacancy['jv_jw_id']);
                break;

            case "Размещено":
                $vacancy['jv_date_publish'] = get_parsed_date(preg_replace("/, .+$/", "", trim($htmlDD->plaintext)));
                break;

            default:
                break;
        }
    }

    foreach ($objTitleVac->parent()->find("h3") as $objH3) {
        if ($objH3->plaintext == "Требования" || $objH3->plaintext == "Обязанности" || $objH3->plaintext == "Условия") {
            if (!isset($vacancy['jv_description']))
                $vacancy['jv_description'] = "";
            $vacancy['jv_description'] .= $objH3->plaintext .": ".$objH3->next_sibling()->innertext;
        }
    }
    $objHtmlVacancy->clear();
    unset($objHtmlVacancy);


    if (!isset($vacancy['jv_description'])) {
        write_log(URL_ALIAS, "($get_url) - Не найдено описание");
        return false;
    }
    $vacancy['jv_description'] = trim($vacancy['jv_description']);
    return insert_in_db("job_vacancies", $vacancy);
}

function get_contact_fio($vacancy_id, $get_url, $html_vacancy)
{
    //Пытаемся вцепить хэш
    if (preg_match("/announcementEMail='([a-zA-Z0-9]+)';/", $html_vacancy, $hash)) {
        $hash = $hash[1];
        $post_url = URL ."/WebServices/AnnouncementSearch.asmx/LoadContactEMail";
        $refer = $get_url;
        $headers[] = "Content-Type: application/json; charset=utf-8";
        $post['id'] = $vacancy_id;
        $post['type'] = "v";
        $post['hash'] = $hash;
        $post_data = json_encode($post);
        $response = json_decode(send_post($post_url, $post_data, $refer, $headers), true);
        return $response['d'];
    }
    else
        return false;
}

function parse_and_insert_new_company($company_title, $company_href, $company_type = "direct") {
    $objSHD = new simple_html_dom(get_web_page(URL . $company_href));
    if (!$objSHD) return false;

    $company['jc_type'] = $company_type;
    $company['jc_title'] = $company_title;
    foreach ($objSHD->find("div.candidate-props dl") as $dl) {
        $title = trim($dl->find("dt", 0)->plaintext);
        $value = trim($dl->find("dd", 0)->plaintext);
        switch ($title) {
            case "Адрес":
                $company['jc_address'] = $value;
                if (preg_match("/^[А-Я][а-я]+[\s]?[\-]?[А-Яа-я]+/u", $company['jc_address'], $company_city))//из Адреса получаем город
                    $company['jc_city'] = $company_city[0];
                break;

            case "email":
                $company['jc_email'] = $value;
                break;

            case "Url":
                $company['jc_web'] = $value;
                break;

            case "Телефон":
                $company['jc_phone'] = $value;
                break;

            default:
                break;
        }
    }
    if ($objSHD->find("#firmmapcont", 0))
        $company['jc_description'] = $objSHD->find("#firmmapcont", 0)->next_sibling()->innertext;
    $company['jc_date_create'] = time();
    $company['jc_status'] = "Y";

    return insert_in_db("job_companies", $company);
}

function send_get($get_url, $headers = false)
{
    $uagent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $get_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_COOKIEJAR, COOKIEFILE);
    curl_setopt($ch, CURLOPT_COOKIEFILE, COOKIEFILE);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($ch, CURLOPT_USERAGENT, $uagent);
    curl_setopt($ch, CURLOPT_VERBOSE, 0);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}

function send_post($post_url,$post_data,$refer = false, $headers = false)
{
    $uagent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $post_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_REFERER, $refer);
    curl_setopt($ch, CURLOPT_COOKIEJAR, COOKIEFILE);
    curl_setopt($ch, CURLOPT_COOKIEFILE, COOKIEFILE);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($ch, CURLOPT_USERAGENT, $uagent);
    curl_setopt($ch, CURLOPT_VERBOSE, 0);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}

function prepare_work_busy_type($type_frome_site)
{
    switch ($type_frome_site) {
        case "Временная/Проектная работа":
            $newtype = "временная работа";
            break;

        case "Полная занятость":
            $newtype = "полный рабочий день";
            break;

        default:
            $newtype = $type_frome_site;
            break;
    }
    return $newtype;
}
