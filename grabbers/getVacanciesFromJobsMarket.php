<?php
/*
 * Граббер резюме для http://jobsmarket.ru/
 * Запуск из командной строки:
 * php -f ./grabbers/getResumesFromJobsMarket.php
 *
 * Tips & Tricks
 *      Записать объект в файл $objHtmlMainPage->save('ResRvG.'.date('YmdHis').'.htm');
 *      Если объект simple_html_dom_node то можно сделать ->__toString() чтобы посмотреть код
 *      У объекта ссылки:
 *          свойство $link->href        чтобы узнать адрес
 *          метод $link->text()         чтобы узнать текст ссылки
 *
 * Resume Simple    http://
 * Resume Authed    http://
 * Vacancies        http://
 */

require_once __DIR__ .'/grabber_function.php';
define('URL', "http://www.jobsmarket.ru");
define('URL_ALIAS', "jmkt");
define('COOKIEFILE', __DIR__."/cookie_".URL_ALIAS.".txt");
define('IMG_PATH', dirname(__DIR__) ."/storage/files/images/vacancies/contactimg");
define('READ_FLAGS_DIR', __DIR__.'/logs/read_vacancies');
define('READ_FLAG_FILE', 'vacancy.counter');
define('UPPER_LIMIT', 30000000);
define('KURS_DOLLAR', 35);

umask(0002);

$isSentAboutPhoto = false; // DEBUG

/**
 * Создадим директорию для картинок, если ещё нет
 */
if (!is_dir(IMG_PATH)) {
    if (!mkdir(IMG_PATH, 0775, true)) {
        die('CANT CREATE DIR FOR IMAGES: '.IMG_PATH);
    }
}

/**
 * Send Auth data
 *
 * Заполнить логин и пароль соответствующими данными с сайта, если есть авторизация
 */
$login = 'gabriella2014';
$passw = 'alleirbag';

/**
 * Заполнить в соответствии с Firebug POST-данными
 */
$PostData['str_login'] = $login;
$PostData['str_password'] = $passw;
$PostData['input_user_form'] = 1;
$PostData['input_user.x'] = rand(1,58);
$PostData['input_user.y'] = rand(1,18);

$Url1 = URL.'/index.php';
$Url2 = URL.'/index.php?get_page=082&bydays=1&get_page_num=1';

//$login = $passw = ''; // DEBUG

/**
 * Процесс проверки авторизации
 */
if (!empty($login) && !empty($passw) && (!file_exists(COOKIEFILE) || filesize(COOKIEFILE) < 480)) {
    echo "Запрос авторизации...................................\n";
    $Result = SendPost($Url1, $PostData);
    write_file($Result, '_AuthResult.htm');
    echo "Ответ получен, см.лог\n";
}

/**
 * Параметры запроса страницы первоначальной
 */
$num_page = UPPER_LIMIT - 1;

/**
 * Определить автоматически на каком месте остановились
 */
if (file_exists(READ_FLAGS_DIR.'/'.READ_FLAG_FILE)) {
    $num_page_string = file_get_contents(READ_FLAGS_DIR.'/'.READ_FLAG_FILE);
    $num_page_string = trim($num_page_string);
    if (!empty($num_page_string) && intval($num_page_string) > 0 && intval($num_page_string) <= UPPER_LIMIT) {
        $num_page = intval($num_page_string);
    }
} else {
    if (!is_dir(READ_FLAGS_DIR)) {
        mkdir(READ_FLAGS_DIR, 0777, true);
    }
    file_put_contents(READ_FLAGS_DIR.'/'.READ_FLAG_FILE, UPPER_LIMIT);
}

$vacancy = 0;

//$num_page = 25615664; // DEBUG

echo "Start from ", $num_page, "\n";

/**
 * Начать постраничный цикл по Компаниям
 */
do {
    if ($num_page < 0 || $num_page > UPPER_LIMIT) {
        echo "num_page = $num_page, is out of date \n";
        break;
    }

    if (isReadBefore($num_page)) {
        write_log(URL_ALIAS, "Уже читал ранее $num_page страницу");
        $num_page--;
        continue;
    }

    $Url2 = URL.'/?view_rpage='.$num_page;
    write_log(URL_ALIAS, "Загрузка страницы $Url2");
    $Result = Read($Url2, $Url1);
    $Result = removeTrash($Result);

    if (!empty($Result)) {
        write_file($Result, '_Company_Page_'.$num_page.'_at_'.time().'.htm');
    }

    if (!preg_match("/Работа в компании/im", $Result) || empty($Result)) { // проверим читается ли страница и содержит ли нужную строку
        if (empty($Result)) {write_log(URL_ALIAS, "Пустой результат. Страница пуста.");}
        if (!file_exists(COOKIEFILE)) {
            if (!AuthPage($Url1, $PostData)) {
                write_log(URL_ALIAS, "Авторизация не получилась\n");
                unlink(COOKIEFILE);
                break;
            }
        }

        setIReadIt($num_page);
    } else {
        $Url1 = $Url2; // страницу реферал назначим здесь, Магистр Йода.

        echo "Есть контент ", $Url2, "\n";

        if (!is_object($SiteUser)) {
            $SiteUser = new SiteUser();
        }

        /**
         * Обработка страницы Компании
         */
        $objHtmlMainPage = new simple_html_dom($Result);
        if (!$objHtmlMainPage) {
            die("GRABBER ERROR");
        }

        /**
         * Прочитать данные о Компании
         */
        $strCompanyName = trim($objHtmlMainPage->find("form td.orange table h1", 0)->text());
        $strCompanyName = trim(preg_replace("/Работа в компании/i", '', $strCompanyName));
        $strCompanyCity = trim($objHtmlMainPage->find("form td.orange table table tr.orange td", 0)->text());
        $strCompanyWeb = trim($objHtmlMainPage->find("form td.orange td noindex a", 0)->text());
        $strCompanyInfo = '';
        if (is_object($objHtmlMainPage->find("table.fcontent #prop10 td.one tr.lgrey td", 0))) {
            $strCompanyInfo = trim($objHtmlMainPage->find("table.fcontent #prop10 td.one tr.lgrey td", 0)->text());
        }
        if (is_object($objHtmlMainPage->find("table.fcontent #prop10 td.one tr.lgrey td", 1))) {
            $strCompanyPhoto = trim($objHtmlMainPage->find("table.fcontent #prop10 td.one tr.lgrey td", 1)->text());
        }

        if (!empty($strCompanyPhoto) && !$isSentAboutPhoto) {
            if (mail('jimmy.webstudio@gmail.com', 'Photo for vacancy', print_r($strCompanyPhoto, true), 'From: info@rabota-ya.ru'."\n".'MIME-Version: 1.0'."\n", '-finfo@rabota-ya.ru')) {
                $isSentAboutPhoto = true;
            }
        }

        /**
         * Проверим наличие такой компании, и если нет - запишем
         */
        $companyID = isExistCompany($strCompanyName);
        if(intval($companyID)<=0 && !empty($strCompanyName)) {
            $arrComp['jc_title'] = $strCompanyName;
            $arrComp['jc_web'] = $strCompanyWeb;
            $arrComp['jc_description'] = $strCompanyInfo;
            $arrComp['jc_type'] = 'direct';

            $cityID = null;
            if (!empty($strCompanyCity)) {
                write_log(URL_ALIAS, "City $strCompanyCity");
                $cityID = $SiteUser->getCityId($strCompanyCity);
            }

            if (is_null($cityID) && !empty($strCompanyCity)) {
                // There is no city in database. Let's record.
                $cityID = $SiteUser->saveCity($strCompanyCity);
            }

            $arrComp['jc_city_id'] = is_null($cityID) ? 1 : $cityID;
            $companyID = insert_in_db('job_companies', $arrComp);
            if ($companyID<=0) {
                write_log(URL_ALIAS, "Компания не записана: $strCompanyName");
                // set only for BAD pages
                setIReadIt($num_page);
                continue;
            } else {
                echo "\n Добавил компанию!\n";
                usleep(1000000);
            }
        }

        /**
         * Делаем перебор ссылок на вакансии
         */
        $arrRowsOfLinks = $objHtmlMainPage->find("table.fcontent tr.orange a.blue_link");
        if (is_array($arrRowsOfLinks) && !empty($arrRowsOfLinks)) {
            // есть найденные ссылки
            foreach($arrRowsOfLinks as $Link) {
                $isValidVacancy = true;
                $linkUri = $Link->href;
                $vacancyNum = preg_replace("/[a-z]+|\?|=|_/im", "", $linkUri);
                $strVacancyTitle = $Link->text();
                $vacancyUrl = URL.'/'.$linkUri;
                write_log(URL_ALIAS, "Загрузка страницы вакансии $vacancyUrl");
                $Result = Read($vacancyUrl, $Url1);
//                $Result = removeTrash($Result);

                if (empty($Result)) {
                    write_log(URL_ALIAS, "Страница вакансии пуста.");
                    continue; // пропустить внутренний цикл
                } else {
                    write_file($Result, '_Vacancy_Page_'.$vacancyNum.'_at_'.time().'.htm');
                }

                $objHtmlVacancyPage = new simple_html_dom($Result);
                if (!$objHtmlVacancyPage) {
                    write_log(URL_ALIAS, "Страницу $vacancyUrl не прочитал");
                    continue; // пропустить внутренний цикл
                }

                /**
                 * Получить только определённые ряды (с двумя ячейками) из таблицы
                 */
                $arrRows = $objHtmlVacancyPage->find("table.fcontent tr.lgrey");
                if (is_array($arrRows) && !empty($arrRows)) {
                    $arrDB = array('title' => $strVacancyTitle);
                    write_log(URL_ALIAS, "Есть ряды!");
                    foreach($arrRows as $Row) {
                        $arrTDs = $Row->find("td"); // найти в TR все TD
                        if (is_array($arrTDs) && !empty($arrTDs)) {
                            write_log(URL_ALIAS, "Есть ячейки в ряду!");
                            # проверить чтобы ячейки было 2 шт.
                            if (count($arrTDs) == 2) {
                                if (is_object($arrTDs[0])) {
                                    $strTitle = trim($arrTDs[0]->text());
                                    if (!empty($strTitle)) {
                                        $strText = trim($arrTDs[1]->text());
                                    }

                                    write_log(URL_ALIAS, "Есть данные: $strTitle\n$strText");

                                    if (preg_match("/Регион/im", $strTitle)) {
                                        if (!empty($strText)) {
                                            $cityId = $SiteUser->getCityId($strText);

                                            if (empty($cityId)) {
                                                $arrDB['city'] = mysql_real_escape_string($strText);
                                            } else {
                                                $arrDB['city_id'] = $cityId;
                                            }
                                        }
                                    }

                                    if (preg_match("/Е-mail/im", $strTitle)) {
                                        if (!empty($strText) && $objUtils->isValidEmail($strText)) {
                                            $arrDB['email'] = mysql_real_escape_string($strText);
                                        }
                                    }

                                    if (preg_match("/Телефон/im", $strTitle)) {
                                        if (!empty($strText)) {
                                            $arrDB['phone'] = mysql_real_escape_string($strText);
                                        } else {
                                            $isValidVacancy = isset($arrDB['email']);
                                            write_log(URL_ALIAS, "Телефон не определён");
                                            if (!$isValidVacancy) {
                                                break; /**** ОБЯЗАТЕЛЬНОЕ ПОЛЕ ****/
                                            }
                                        }
                                    }

                                    if (preg_match("/обязанности/im", $strTitle)) {
                                        if (!empty($strText)) {
                                            $arrDB['description'] = mysql_real_escape_string($strText);
                                        }
                                    }

                                    if (preg_match("/компетенции/im", $strTitle)) {
                                        if (!empty($strText)) {
                                            if (isset($arrDB['description']) && !empty($arrDB['description'])) {
                                                $arrDB['description'] .= "\n\n".mysql_real_escape_string($strText);
                                            } else {
                                                $arrDB['description'] = mysql_real_escape_string($strText);
                                            }
                                        }
                                    }

                                    if (preg_match("/среднемесячный доход/im", $strTitle)) {
                                        if (!empty($strText)) {
                                            if (preg_match("/usd/im", $strText)) {
                                                $strText = preg_replace("/usd/im", '', $strText);
                                                $strText = intval($strText) * KURS_DOLLAR;
                                            }
                                            $arrDB['salary'] = mysql_real_escape_string($strText);
                                            $arrDB['salary_from'] = intval($strText);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    try {
                        $strAddOnDescription = trim($objHtmlVacancyPage->find("table.fcontent tr.orange", 2));
                        if (is_object($strAddOnDescription)) {
                            write_log(URL_ALIAS, "Есть описание!");
                            $strAddOnDescription = $strAddOnDescription->find("td.one", 0)->text();

                            if (isset($arrDB['description']) && !empty($arrDB['description'])) {
                                $arrDB['description'] .= "\n\n".mysql_real_escape_string($strAddOnDescription);
                            } else {
                                $arrDB['description'] = mysql_real_escape_string($strAddOnDescription);
                            }
                        }
                    } catch (Exception $ex) {
                        write_log(URL_ALIAS, "Exception: ".$ex->getTraceAsString());
                    }

                    /**
                     * Запишем данные, предварительно проверив нет ли их уже в БД
                     */
                    if (is_array($arrDB) && !empty($arrDB) && $isValidVacancy) {
                        write_log(URL_ALIAS, "Есть что сохранять! Проверяем БД.");
                        $strSqlQuery = "SELECT * FROM `job_vacancies` WHERE ((".(!empty($arrDB['email']) ? "`jv_email` LIKE '".$arrDB['email']."' AND " : '' )
                            .(!empty($arrDB['phone']) ? "`jv_phone` LIKE '".$arrDB['phone']."' AND " : '')." `jv_title` LIKE '".$arrDB['title']."') OR `jv_src_id` = $vacancyNum) AND `jv_jc_id` = $companyID AND `jv_src_url_alias` LIKE '".URL_ALIAS."'";
                        write_log(URL_ALIAS, $strSqlQuery);
                        $arrExistVacancy = $objDb->fetch($strSqlQuery);

                        if (is_array($arrExistVacancy) && !empty($arrExistVacancy)) {
                            $vacancyID = $arrExistVacancy['jv_id'];
                            write_log(URL_ALIAS, "Вакансия уже в БД");
                        } else {
                            $strSqlQuery = "INSERT INTO `job_vacancies` SET `jv_date_publish` = UNIX_TIMESTAMP('".date('Y-m-d', strtotime('-1 month'))."'),";

                            $arrDB['src_id'] = $vacancyNum;
                            $arrDB['src_url_alias'] = URL_ALIAS;
                            $arrDB['status'] = 'Y';
                            $arrDB['jw_id'] = 1;
                            $arrDB['jc_id'] = $companyID;
                            $arrSqlQuery = array();
                            foreach ($arrDB as $key => $value) {
                                $arrSqlQuery[] = "`jv_$key` = '$value'";
                            }

                            $strSqlQuery .= implode(',', $arrSqlQuery);
                            write_log(URL_ALIAS, "Сохраняем данные о вакансии ". $strSqlQuery);
                            if (!$objDb->query($strSqlQuery)) {
                                # sql error
                                write_log(URL_ALIAS, "MySQL error $strSqlQuery");
                            } else {
                                $vacancyID = $objDb->insert_id();

                                write_log(URL_ALIAS, "ID вакансии ". $vacancyID);

                                echo "\n Записал вакансию!\n";
                                usleep(2000000);
                            }
                        }
                    }
                } else {
                    $isValidVacancy = false;
                    write_log(URL_ALIAS, "Ряды не найдены!");
                }

                if ($isValidVacancy) {
                    $vacancy++;
                    write_log(URL_ALIAS, "Vacancy #".$vacancy." with ID: ".$vacancyID);
                    write_log(URL_ALIAS, print_r($arrDB, true));
                } else {
                    write_log(URL_ALIAS, "Что-то не так с вакансией");
                }
            } // end foreach
        } else {
            write_log(URL_ALIAS, "Нет ссылок на вакансии");
        }

        setIReadIt($num_page, true);
    }

    $num_page--;

//    if ($vacancy > 0) {
//        die("\n" . __FILE__ . ":" . __LINE__ . "\n\n");
//    }

//    if ($vacancy > 2) {echo "\nПРЕРВАНО!\n\n";break;} // DEBUG
    $mseconds = rand(100,30000);
//    echo "\n Wait for ".$mseconds." msecs \n";
    usleep($mseconds);
} while (true);

echo "Конец работы граббера.", date("H:i:s d.m.Y") ,"\n";

function AuthPage($p1, $p2)
{
    write_log(URL_ALIAS, "Запрос авторизации.....\n");
    $Result = SendPost($p1, $p2);
    $Result = removeTrash($Result);
    $filename = '_Url1_'.date('YmdHis').'.htm';
    write_file($Result, $filename);
    write_log(URL_ALIAS, "Ответ получен в $filename\n");

    if (!preg_match("/Турандот Виктор Михайлович/im", $Result)) {
        /* авторизация не прошла :( */
        write_log(URL_ALIAS, "Запрос авторизации не удался!!! ***\n");
        return false;
    }

    return true;
}

