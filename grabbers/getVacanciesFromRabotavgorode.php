<?php
/*
 * Граббер вакансий для http://spb.rabotavgorode.ru/
 * Запуск из командной строки:
 * php -f ./grabbers/getVacanciesFromRabotavgorode.php
 */
//Почтаlogin awergix@yandex.ru passw 123456789qaz
//сайт login awergix@yandex.ru passw 1qaz2wsx
require_once dirname(__FILE__).'/grabber_function.php';

define('URL', "http://spb.rabotavgorode.ru");
define('URL_ALIAS', "rbvg");

$file_path = dirname((__FILE__)) ."/log-".URL_ALIAS .".log";
if (file_exists($file_path))
    unlink($file_path);

$objHtmlMainPage = new simple_html_dom(get_web_page(URL ."/Vacancies/"));
foreach ($objHtmlMainPage->find('table.directionsList a') as $elem_direction) { //цикл по направлениям
    $objHtmlListOfPages = new simple_html_dom(get_web_page(URL . $elem_direction->href));
    if (!$objHtmlListOfPages) {
        echo "Ошибка чтения HTML начальной страницы для направления '$elem_direction->plaintext'. Скрипт остановлен\n";
        die();
    }
    echo "Обработка направления '$elem_direction->plaintext' ...\n";
    $count_vacancies = 0;
    $total_vacancies = 0;
    $problem_vacancies = 0;

    foreach ($objHtmlListOfPages->find('table.vacanciesList a.title') as $href_to_vacancy) {//Цикл по ссылкам на вакансии на первом page
        unset($vacancy_id);
        if (preg_match("/[0-9]+$/", $href_to_vacancy->href, $vacancy_id)) {
            $total_vacancies++;
            $vacancy_id = $vacancy_id[0];
            if (!isExistVacancy(URL_ALIAS, $vacancy_id)) {
                if (process_vacancy($href_to_vacancy->href, $vacancy_id))
                    $count_vacancies++;
                else
                    $problem_vacancies++;
            }
        }
    }

    foreach ($objHtmlListOfPages->find('div#cphBody_vacList_pager_pnlCont a') as $href_to_page) {
        $objHtmlListofVacancies = new simple_html_dom(get_web_page(URL . $href_to_page->href));
        foreach ($objHtmlListofVacancies->find('table.vacanciesList a.title') as $href_to_vacancy) {//Цикл по ссылкам на вакансии
            if (preg_match("/[0-9]+$/", $href_to_vacancy->href, $vacancy_id)) {
                $total_vacancies++;
                $vacancy_id = $vacancy_id[0];
                if (!isExistVacancy(URL_ALIAS, $vacancy_id)) {
                    if (process_vacancy($href_to_vacancy->href, $vacancy_id))
                        $count_vacancies++;
                    else
                        $problem_vacancies++;
                }
            }
        }
        $objHtmlListofVacancies->clear();
        unset($objHtmlListofVacancies);
    }
    $objHtmlListOfPages->clear();
    unset($objHtmlListOfPages);
    $statictic = "Просмотрено $total_vacancies вакансий.\n";
    if($problem_vacancies > 0)
        $statictic .= "Пропущено  $problem_vacancies  вакансий. Смотри логфайл.\n";
    if($count_vacancies)
        $statictic .= "В базу записано $count_vacancies новых вакансий.\n";
    else
        $statictic .= "Новых вакансий не обнаружено\n";
    echo $statictic;
}
$objHtmlMainPage->clear();
unset($objHtmlMainPage);


function process_vacancy($vacancy_url, $vacancy_id) {
    global $objDb;
    $vacancy = get_parsed_vacancy($vacancy_url);
    if (isset($vacancy['href_to_company_page'])) {
        $company_id = isExistCompany($vacancy['company_title']);
        if (!$company_id) {
            $company_id = parse_and_insert_new_company($vacancy['href_to_company_page']);
            if (!$company_id) {
                echo "Company {$vacancy['href_to_company_page']} don't parsed for $vacancy_url.\n Script was stoped";
                die();
            }
        }
        $vacancy_filds['jv_jc_id'] = $company_id;
    }
    else {
        write_log(URL_ALIAS, "$vacancy_url - не указан работодатель");
        return 0;
    }

    $vacancy_filds['jv_status'] = "Y";
    $vacancy_filds['jv_src_url_alias'] = URL_ALIAS;
    $vacancy_filds['jv_src_id'] = $vacancy_id;

    //готовим массивы для использования в БД
    if (isset($vacancy['work_busy'])) {
        $vacancy_filds['jv_jw_id'] = isExistWorkBusyType($vacancy['work_busy']);
        if ($vacancy_filds['jv_jw_id'] == false)
            unset($vacancy_filds['jv_jw_id']);
    }

    if (isset($vacancy['education_type'])) {
        $vacancy_filds['jv_jet_id'] = isExistEducationType($vacancy['education_type']);
        if ($vacancy_filds['jv_jet_id'] == false)
            unset($vacancy_filds['jv_jet_id']);
    }

    $vacancy_filds['jv_title'] = $vacancy['title'];
    $vacancy_filds['jv_salary'] = $vacancy['salary'];
    if (isset($vacancy['driver']))
        $vacancy_filds['jv_driver'] = ($vacancy['driver'] == "не требуются") ? "N" : "Y";
    if (isset($vacancy['salary_from']))
        $vacancy_filds['jv_salary_from'] = $vacancy['salary_from'];
    if (isset($vacancy['salary_to']))
        $vacancy_filds['jv_salary_to'] = $vacancy['salary_to'];
    if (isset($vacancy['city']))
        $vacancy_filds['jv_city'] = $vacancy['city'];
    if (isset($vacancy['description']))
        $vacancy_filds['jv_description'] = $vacancy['description'];
    if (isset($vacancy['timestamp_publish']))
        $vacancy_filds['jv_date_publish'] = $vacancy['timestamp_publish'];
    if (isset($vacancy['contact_fio']))
        $vacancy_filds['jv_contact_fio'] = trim($vacancy['contact_fio']);

    return insert_in_db("job_vacancies", $vacancy_filds);
}


function parse_and_insert_new_company($company_url) {
    $objSHD = new simple_html_dom(get_web_page($company_url));
    if(!$objSHD) {
        echo "Ошибка запроса страницы компании \n$company_url";
        die();
    }
    $company['title'] = htmlspecialchars_decode(trim($objSHD->find("div#colCenter h1", 0)->plaintext));
    foreach ($objSHD->find("div#cphRight_Panel1 tr") as $tr) {
        $value = trim($tr->find("td", 1)->plaintext); // значение поля
        $title = trim($tr->find("td img[title]", 0)->title);
        switch ($title) {
            case "Вид компании":
                $company['type'] = "recrut";
                if ($value == "Прямой работодатель")
                    $company['type'] = "direct";
                break;

            case "Адрес":
                $company['address'] = $value;
                if (preg_match("/^[А-Я][а-я]+[\s]?[\-]?[А-Яа-я]+/u", $value, $company_city))//из Адреса получаем город
                    $company['city'] = $company_city[0];
                break;

            case "E-mail":
                $company['email'] = $value;
                break;

            case "Web site":
                $company['web'] = $value;
                break;

            default:
                break;
        }
    }
    $company['description'] = htmlspecialchars_decode($objSHD->find("div.contentContainer div", 0)->plaintext);
    $objSHD->clear();
    unset($objSHD);

    $company_fields['jc_title'] = $company['title'];
    if (isset($company['city']))
        $company_fields['jc_city'] = $company['city'];
    if (isset($company['type']))
        $company_fields['jc_type'] = $company['type'];
    if (isset($company['address']))
        $company_fields['jc_address'] = $company['address'];
    if (isset($company['email']))
        $company_fields['jc_email'] = $company['email'];
    if (isset($company['web']))
        $company_fields['jc_web'] = $company['web'];
    if (isset($company['description']))
        $company_fields['jc_description'] = $company['description'];
    $company_fields['jc_date_create'] = time();
    $company_fields['jc_status'] = "Y";

    return insert_in_db("job_companies", $company_fields);
}

/**
 * Парсинг страницы с вакансией
 * @param type $vacancy_url - url страницы с вакансией
 * @return array
 */
function get_parsed_vacancy($vacancy_url) {
    $objSHD = str_get_html(get_web_page($vacancy_url));
    $vacancy['title'] = trim($objSHD->find("div#colCenter h1", 0)->plaintext);
    if (isset($objSHD->find("a#cphRight_hlCompanyName", 0)->href)) {
        $html_company_info = $objSHD->find("a#cphRight_hlCompanyName", 0);
        $vacancy['href_to_company_page'] = $html_company_info->href;
        $vacancy['company_title'] = htmlspecialchars_decode(trim($html_company_info->find("span", 0)->plaintext));
    }
    else
        return;

    foreach($objSHD->find("div#cphRight_pnlCompanyInfo table span[title]") as $info) {
        if ($info->title == " Контактное лицо") {
            $objHtmlContact = $info;
            break;
        }
    }
    if ($objHtmlContact) {
        $objHtmlFIO = $objHtmlContact->parent()->next_sibling();
        if ($objHtmlFIO)
            $vacancy['contact_fio'] = $objHtmlFIO->plaintext;
    }

    foreach ($objSHD->find("div[id=vacancyView] tr") as $tag_tr) {
        $th = trim($tag_tr->find("th", 0)->plaintext);
        $td = trim($tag_tr->find("td", 0)->plaintext);
        switch ($th) {
            case "Заработная плата:":
                $vacancy['salary'] = $td;
                $salary_parsed = get_parsed_salary($td);
                if (isset($salary_parsed['from']))
                    $vacancy['salary_from'] = $salary_parsed['from'];
                if (isset($salary_parsed['to']))
                    $vacancy['salary_to'] = $salary_parsed['to'];
                break;

            case "Актуальна в городах:":
                $vacancy['city'] = trim(preg_replace("/[\s]+[-][\s]+.+/", "", $td)); //Удаляем всяческие примочки у ГОРОДА
                break;

            case "График работы:":
                $vacancy['work_busy'] = $td;
                break;

            case "Дата размещения:":
                $parsed_date = get_parsed_date($td);
                if ($parsed_date == false)
                    write_log (URL_ALIAS_RBVG, "Для $vacancy_url не смог обработать дату размещения \n$td");
                else
                    $vacancy['timestamp_publish'] = get_parsed_date($td);
                break;

            case "Образование:":
                $vacancy['education_type'] = $td;
                break;

            case "Водительские права:":
                $vacancy['driver'] = $td;
                break;

            case "Условия работы и компенсации:":
            case "Требования:":
            case "Обязанности":
            case "Опыт работы в данной области:":
            case "Условия работы и компенсации:":
                if (!isset($vacancy['description']))
                    $vacancy['description'] = "";
                $vacancy['description'] .= "$th $td\n\r";
                break;

            default:
                break;
        }
    }
    $objSHD->clear();
    unset($objSHD);
    return $vacancy;
}
