<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 8/6/13
 * Time         : 1:05 AM
 * Description  :
 */
include_once "cfg/web.cfg.php";

$arrResult['status'] = 0;
$arrResult['message'] = '';

if (isset($_POST) && $_POST['adv']=='web') {
    $sendFrom = trim($_POST['strEmail']);
    if (!$objUtils->checkEmail($sendFrom)) {
        $arrResult['message'] = "Не правильный формат Email адреса!\n";
    } else {
        // CAPTCHA >>>
        if ( strtoupper($_SESSION['strCptCode']) != strtoupper($_POST['strCodeString']) || empty($_POST['strCodeString']) ) {
            $arrResult['message'] = 'Визуальный код введён неверно';
        // *** CAPTCHA
        } else {
            if ( !is_object( $objMail ) ) {
                include_once( "cls.mail.php" );
                $objMail = new clsMail();
            }

            $sendTo = "info@rabota-ya.ru";
            if ($_SERVER['_IS_DEVELOPER_MODE']==1) {
                $sendTo = 'jimmy.webstudio@gmail.com';
            }

            $sendSubject = "Rabota-Ya.ru | Запрос с сайта";

            $sendMessage = "Email посетителя - ".strip_tags(trim($_POST['strEmail']))."\n";
            $sendMessage .= "Телефон - ".strip_tags(trim($_POST['strPhone']))."\n";
            $sendMessage .= "Контактное лицо - ".strip_tags(trim($_POST['strContact']))."\n";
            $sendMessage .= "Текст обращения - ".strip_tags(trim($_POST['strText']))."\n";
            $sendMessage .= "\n\n===============================================================\n\n";

            $sendHTMLMessage = file_get_contents(SITE_TPL_DIR.'emails/user.request.html');

            $sendHTMLMessage = preg_replace("/\{\%eMail\%\}/", strip_tags(trim($_POST['strEmail'])), $sendHTMLMessage);
            $sendHTMLMessage = preg_replace("/\{\%phone\%\}/", strip_tags(trim($_POST['strPhone'])), $sendHTMLMessage);
            $sendHTMLMessage = preg_replace("/\{\%contact\%\}/", strip_tags(trim($_POST['strContact'])), $sendHTMLMessage);
            $sendHTMLMessage = preg_replace("/\{\%message\%\}/", nl2br(strip_tags(trim($_POST['strText']))), $sendHTMLMessage);

            if (isset($_SERVER['_IS_DEVELOPER_MODE']) && $_SERVER['_IS_DEVELOPER_MODE'] == 1) {
                $sendTo = "jimmy.webstudio@gmail.com"; // DEBUG
            }

            $objMail->new_mail( $sendFrom, $sendTo, $sendSubject, $sendMessage, $sendHTMLMessage );
            if ($sendTo != "jimmy.webstudio@gmail.com") {
                $objMail->add_bcc( "jimmy.webstudio@gmail.com" );
            }

            $objMail->add_attachment( PRJ_SITEIMG.'content/email-bg.jpg', 'email-bg.jpg', 'image' );

            if ( !$objMail->send() ) {
                $arrResult['message'] = "Письмо не отправлено по техническим причинам!\nПопробуйте через пару минут.\nПриносим свои извинения!";
            } else {
                $arrResult['status'] = 1;
            }
        }
    }
}

echo json_encode($arrResult);
