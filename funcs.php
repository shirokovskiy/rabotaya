<?php
function split_string ( $str, $n = 10, $char = ' ' ) {
    $c = 0;
    $nstr = '';
    for ($i=0;$i<strlen($str);$i++) {
        $nstr .= $str[$i];$c++;
        if ($c==$n) {
            $nstr .= $char;
            $c = 0;
        }
    }

    return $nstr;
}

/**
 * Делает первую букву строки кириллицы заглавной
 * @param $word
 * @return string
 */
function mb_ucfirst ($word) {
    return mb_strtoupper(mb_substr($word, 0, 1, 'UTF-8'), 'UTF-8') . mb_substr($word, 1, mb_strlen($word), 'UTF-8');
}

function trace_log($var, $filename = 'debug.log', $noTimeInFilename=true, $pre = '') {
    $dbgTrace = debug_backtrace();
    if (!defined("PRJ_LOGS")) define("PRJ_LOGS", dirname(__FILE__));
    $logDir = PRJ_LOGS;
    if ( !is_dir($logDir) ) {
        mkdir($logDir, 0775, true);
    }
    if (!preg_match("/\.htm.?$/i", $filename) && !preg_match("/\.[log|txt|sql]/i", $filename)) {
        $filename .= '.log';
    }
    $fh = fopen($logDir.($noTimeInFilename==false ? microtime(true).'.' : '').$filename, "a+");

    ob_start();
    $out = '';
    $object = $var;
    if (is_object($object)) {
        $classNameOfObj = get_class($object);
        $out .= $classNameOfObj ."\n";
        $ms = get_class_methods($object);
        sort($ms);
        $out .= print_r($ms, true);
        $out .= "\n\n";
    } else {
        $out .= 'Not object' ."\n";
        if (is_array($object)) {
//            $out .= 'Array:' ."\n";
            foreach ($object as $k => $v) {
                if (is_object($v))
                    $object[$k] = get_class($v);
            }
            $out .= print_r($object, true) ."\n";
        } else {
            ob_start();
            print_r($object);
            $vardump_out = ob_get_contents();
            ob_clean();
            $out .= $vardump_out ."\n";
        }
    }
    echo $out;

    $debug_content = ob_get_contents();
    ob_clean();

    if (preg_match("/\.htm.?$/i", $filename)) {
        $output = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /><title> DEBUG OUTPUT ".date('d.m.Y H:i:s')." [".microtime(true)."]</title></head>\n";
        $output.= "<body><div style='width:800px;color:#333'>".nl2br($debug_content)."</div></body></html>\n";
    } else {
        $output = $pre."DEBUG OUTPUT ".date('d.m.Y H:i:s')." [".microtime(true)."]\n";
        $output.= "Called from: ".(isset($dbgTrace[0]['file']) ? $dbgTrace[0]['file']:'file unknown').':'.(isset($dbgTrace[0]['line']) ? $dbgTrace[0]['line'] : '?')."\n\n";
        $output.= $debug_content." \n\n";
    }

    fwrite($fh, $output, strlen($output));
    fclose($fh);
}

function getDebugBacktrace($NL = "\n") {
    $dbgTrace = debug_backtrace();
    $tab = '';
    $dbgMsg = $NL."Debug backtrace begin:$NL";
    $strArgs = '';
    $arrArgs = array();
    $intArgStringLenght = 30;
    foreach($dbgTrace as $dbgIndex => $dbgInfo) {
        if (is_array($dbgInfo['args'])) {
            foreach($dbgInfo['args'] as $arg) {
                if ( is_string($arg) && strlen($arg) > $intArgStringLenght ) {
                    $intPossibleArgLenght = strpos($arg, "\n");
                    if ($intPossibleArgLenght !== false) {
                        $intArgStringLenght = $intPossibleArgLenght;
                    }
                    $arrArgs[] = substr( $arg, 0, $intArgStringLenght )."... [$intArgStringLenght chars and more text]";
                } else {
                    $arrArgs[] = $arg;
                }
            }
            $strArgs = implode(", ", $arrArgs);
        }
        $dbgMsg .= "$tab ($dbgIndex) ".( isset($dbgInfo['class']) ? 'class '.$dbgInfo['class'].'->' : 'function ' ).$dbgInfo['function']."( $strArgs ), from: ".(isset($dbgInfo['file'])?$dbgInfo['file']:'').":".(isset($dbgInfo['line'])?$dbgInfo['line']:'')."$NL $NL";
        $tab .= "\t";
    }
    $dbgMsg .= "Debug backtrace end".$NL;
    return $dbgMsg;
}

$arrKeyWords = array('работа от а до я', 'вакансии в петербурге', 'резюме петербург и москва', 'вакансии москва', 'вакансии спб', 'работа спб', 'работа москва', 'работа 2014', 'вакансии 2014', 'резюме 2014');
function getUrlContents($url)
{
    $crl = curl_init();
    $timeout = 5;
    curl_setopt ($crl, CURLOPT_URL, $url);
    curl_setopt ($crl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt ($crl, CURLOPT_CONNECTTIMEOUT, $timeout);
    $ret = curl_exec($crl);
    curl_close($crl);
    return $ret;
}
